.class public Lcom/google/android/apps/gmm/car/h/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/h/b;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/car/h/p;

.field private final b:I

.field private final c:Lcom/google/android/apps/gmm/car/h/o;

.field private final d:Z

.field private final e:Z

.field private final f:Lcom/google/android/apps/gmm/shared/c/c/c;

.field private final g:Landroid/content/Context;

.field private final h:Lcom/google/android/libraries/curvular/aq;

.field private i:Lcom/google/android/apps/gmm/map/r/a/ao;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private j:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private k:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private l:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private m:Lcom/google/android/libraries/curvular/aq;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/h/p;ILcom/google/android/apps/gmm/car/h/o;ZZLcom/google/android/apps/gmm/shared/c/c/c;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    sget v0, Lcom/google/android/apps/gmm/d;->aM:I

    sget v1, Lcom/google/android/apps/gmm/d;->y:I

    .line 45
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/k/ab;->a(II)Lcom/google/android/apps/gmm/base/k/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->h:Lcom/google/android/libraries/curvular/aq;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->h:Lcom/google/android/libraries/curvular/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->m:Lcom/google/android/libraries/curvular/aq;

    .line 65
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/h/c;->a:Lcom/google/android/apps/gmm/car/h/p;

    .line 66
    iput p2, p0, Lcom/google/android/apps/gmm/car/h/c;->b:I

    .line 67
    iput-object p3, p0, Lcom/google/android/apps/gmm/car/h/c;->c:Lcom/google/android/apps/gmm/car/h/o;

    .line 68
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/car/h/c;->d:Z

    .line 69
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/car/h/c;->e:Z

    .line 70
    iput-object p6, p0, Lcom/google/android/apps/gmm/car/h/c;->f:Lcom/google/android/apps/gmm/shared/c/c/c;

    .line 71
    iput-object p7, p0, Lcom/google/android/apps/gmm/car/h/c;->g:Landroid/content/Context;

    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v0, p7}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    .line 74
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/h/c;->a(Z)V

    .line 75
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/car/h/c;->b:I

    sget v2, Lcom/google/android/apps/gmm/c;->a:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    array-length v2, v0

    rem-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final a(Z)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 83
    iget v0, p0, Lcom/google/android/apps/gmm/car/h/c;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/h/c;->a:Lcom/google/android/apps/gmm/car/h/p;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/car/h/p;->a()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 84
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/h/c;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    .line 85
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/h/c;->j:Ljava/lang/CharSequence;

    .line 86
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/h/c;->k:Ljava/lang/CharSequence;

    .line 87
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/h/c;->l:Ljava/lang/CharSequence;

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->a:Lcom/google/android/apps/gmm/car/h/p;

    iget v1, p0, Lcom/google/android/apps/gmm/car/h/c;->b:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/h/p;->a(I)Lcom/google/android/apps/gmm/map/r/a/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->a:Lcom/google/android/apps/gmm/car/h/p;

    iget v1, p0, Lcom/google/android/apps/gmm/car/h/c;->b:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/h/p;->b(I)I

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/car/h/c;->e:Z

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/google/android/apps/gmm/car/h/c;->b:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/h/c;->a:Lcom/google/android/apps/gmm/car/h/p;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/car/h/p;->b()I

    move-result v2

    if-eq v1, v2, :cond_5

    move v1, v3

    :goto_1
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/h/c;->a:Lcom/google/android/apps/gmm/car/h/p;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/h/c;->a:Lcom/google/android/apps/gmm/car/h/p;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/car/h/p;->b()I

    move-result v4

    invoke-interface {v2, v4}, Lcom/google/android/apps/gmm/car/h/p;->b(I)I

    move-result v2

    sub-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/h/c;->g:Landroid/content/Context;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;IZ)[Ljava/lang/String;

    move-result-object v0

    aget-object v2, v0, v3

    if-eqz v2, :cond_6

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    aget-object v4, v0, v7

    aput-object v4, v2, v7

    const-string v4, " "

    aput-object v4, v2, v3

    const/4 v4, 0x2

    aget-object v0, v0, v3

    aput-object v0, v2, v4

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->j:Ljava/lang/CharSequence;

    :goto_2
    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->h:Lcom/google/android/libraries/curvular/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->m:Lcom/google/android/libraries/curvular/aq;

    .line 92
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->f:Lcom/google/android/apps/gmm/shared/c/c/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/h/c;->a:Lcom/google/android/apps/gmm/car/h/p;

    iget v2, p0, Lcom/google/android/apps/gmm/car/h/c;->b:I

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/car/h/p;->c(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/h/c;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v4, :cond_8

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v2

    :goto_4
    iget-object v4, v2, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v4, :cond_9

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v2

    :goto_5
    iget v2, v2, Lcom/google/maps/g/a/ai;->d:I

    invoke-static {v2}, Lcom/google/maps/g/a/al;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/maps/g/a/al;->d:Lcom/google/maps/g/a/al;

    :cond_3
    move v4, v3

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZZLcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->k:Ljava/lang/CharSequence;

    .line 94
    if-eqz p1, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_a

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_6
    invoke-virtual {v0}, Lcom/google/maps/g/a/fk;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_b

    :cond_4
    move v0, v3

    :goto_7
    if-eqz v0, :cond_c

    iput-object v5, p0, Lcom/google/android/apps/gmm/car/h/c;->l:Ljava/lang/CharSequence;

    goto/16 :goto_0

    :cond_5
    move v1, v7

    .line 91
    goto/16 :goto_1

    :cond_6
    aget-object v0, v0, v7

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->j:Ljava/lang/CharSequence;

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->a:Lcom/google/android/apps/gmm/car/h/p;

    iget v1, p0, Lcom/google/android/apps/gmm/car/h/c;->b:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/h/p;->d(I)Lcom/google/maps/g/a/fz;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/d;->aM:I

    invoke-static {v0, v1, v7}, Lcom/google/android/apps/gmm/directions/f/d/h;->a(Lcom/google/maps/g/a/fz;IZ)I

    move-result v1

    sget v2, Lcom/google/android/apps/gmm/d;->y:I

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/directions/f/d/h;->a(Lcom/google/maps/g/a/fz;IZ)I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/base/k/ab;->a(II)Lcom/google/android/apps/gmm/base/k/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->m:Lcom/google/android/libraries/curvular/aq;

    goto :goto_3

    .line 92
    :cond_8
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_4

    :cond_9
    iget-object v2, v2, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto :goto_5

    .line 95
    :cond_a
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_6

    :cond_b
    move v0, v7

    goto :goto_7

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->g:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->oB:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v7

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->l:Ljava/lang/CharSequence;

    goto/16 :goto_0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/google/android/apps/gmm/car/h/c;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->j:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/aq;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->m:Lcom/google/android/libraries/curvular/aq;

    return-object v0
.end method

.method public final e()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->k:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->l:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->l:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    if-eqz v0, :cond_5

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v3, :cond_0

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    move-object v4, v0

    :goto_0
    if-nez v4, :cond_1

    move v0, v2

    :goto_1
    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    move-object v4, v0

    goto :goto_0

    :cond_1
    iget-object v0, v4, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    :goto_3
    if-ge v3, v5, :cond_4

    iget-object v0, v4, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    invoke-virtual {v0}, Lcom/google/maps/g/a/da;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v6, "toll"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "p\u00e9age"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "pedaggi"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "Maut"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "paeje"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    if-eqz v0, :cond_5

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v3, :cond_0

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    move-object v4, v0

    :goto_0
    if-nez v4, :cond_1

    move v0, v2

    :goto_1
    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    move-object v4, v0

    goto :goto_0

    :cond_1
    iget-object v0, v4, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    :goto_3
    if-ge v3, v5, :cond_4

    iget-object v0, v4, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    invoke-virtual {v0}, Lcom/google/maps/g/a/da;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v6, "ferry"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "traghetto"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "F\u00e4hrstrecke"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method public final j()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->a:Lcom/google/android/apps/gmm/car/h/p;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/h/p;->b()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/car/h/c;->b:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 219
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/h/c;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final m()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->a:Lcom/google/android/apps/gmm/car/h/p;

    iget v1, p0, Lcom/google/android/apps/gmm/car/h/c;->b:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/h/p;->e(I)V

    .line 225
    const/4 v0, 0x0

    return-object v0
.end method

.method public final n()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->a:Lcom/google/android/apps/gmm/car/h/p;

    iget v1, p0, Lcom/google/android/apps/gmm/car/h/c;->b:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/h/p;->e(I)V

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/c;->c:Lcom/google/android/apps/gmm/car/h/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/h/o;->b()V

    .line 232
    const/4 v0, 0x0

    return-object v0
.end method
