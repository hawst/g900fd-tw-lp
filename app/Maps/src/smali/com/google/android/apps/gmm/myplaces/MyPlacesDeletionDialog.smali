.class public Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;
.super Landroid/app/DialogFragment;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field a:Landroid/app/AlertDialog;

.field b:Lcom/google/android/apps/gmm/myplaces/c/e;

.field public c:Lcom/google/android/apps/gmm/myplaces/a/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private d:Lcom/google/android/apps/gmm/myplaces/c/f;

.field private e:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/myplaces/c/f;)Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;
    .locals 3

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;-><init>()V

    .line 53
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 54
    const-string v2, "myplaces_item"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 55
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->setArguments(Landroid/os/Bundle;)V

    .line 56
    return-object v0
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->c:Lcom/google/android/apps/gmm/myplaces/a/d;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->c:Lcom/google/android/apps/gmm/myplaces/a/d;

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->e:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->iI:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->e:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 148
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->c:Lcom/google/android/apps/gmm/myplaces/a/d;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->c:Lcom/google/android/apps/gmm/myplaces/a/d;

    .line 187
    :cond_0
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    :goto_0
    return-void

    .line 122
    :cond_0
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 129
    :pswitch_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    goto :goto_0

    .line 126
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->b:Lcom/google/android/apps/gmm/myplaces/c/e;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->a()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->a:Landroid/app/AlertDialog;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->a:Landroid/app/AlertDialog;

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->a:Landroid/app/AlertDialog;

    sget v2, Lcom/google/android/apps/gmm/l;->hL:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j_()Lcom/google/android/apps/gmm/myplaces/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->b:Lcom/google/android/apps/gmm/myplaces/c/e;

    new-instance v2, Lcom/google/android/apps/gmm/myplaces/b;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/myplaces/b;-><init>(Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/myplaces/a/a;->a(Lcom/google/android/apps/gmm/myplaces/c/e;Lcom/google/android/apps/gmm/myplaces/a/b;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0

    .line 122
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->e:Lcom/google/android/apps/gmm/base/activities/c;

    .line 70
    if-eqz p1, :cond_1

    .line 71
    :goto_0
    const-string v0, "myplaces_item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    const-string v0, "myplaces_item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/myplaces/c/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->d:Lcom/google/android/apps/gmm/myplaces/c/f;

    .line 75
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 76
    sget v1, Lcom/google/android/apps/gmm/l;->iF:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 77
    sget v1, Lcom/google/android/apps/gmm/l;->iG:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 78
    sget v1, Lcom/google/android/apps/gmm/l;->bg:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 81
    sget v1, Lcom/google/android/apps/gmm/l;->hL:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 83
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->a:Landroid/app/AlertDialog;

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->a:Landroid/app/AlertDialog;

    return-object v0

    .line 70
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 61
    const-string v0, "myplaces_item"

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->d:Lcom/google/android/apps/gmm/myplaces/c/f;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 65
    return-void
.end method

.method public onShow(Landroid/content/DialogInterface;)V
    .locals 4

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->a:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    .line 98
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j_()Lcom/google/android/apps/gmm/myplaces/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->d:Lcom/google/android/apps/gmm/myplaces/c/f;

    new-instance v2, Lcom/google/android/apps/gmm/myplaces/a;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/myplaces/a;-><init>(Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/myplaces/a/a;->a(Lcom/google/android/apps/gmm/myplaces/c/f;Lcom/google/android/apps/gmm/myplaces/a/c;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0
.end method
