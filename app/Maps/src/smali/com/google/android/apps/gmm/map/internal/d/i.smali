.class public abstract Lcom/google/android/apps/gmm/map/internal/d/i;
.super Lcom/google/android/apps/gmm/shared/net/i;
.source "PG"


# static fields
.field private static final a:J


# instance fields
.field private b:I

.field private c:I

.field private d:Lcom/google/android/apps/gmm/map/internal/d/j;

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/j;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final h:[Lcom/google/android/apps/gmm/map/internal/d/p;

.field i:Z

.field j:Z

.field k:Lcom/google/android/apps/gmm/map/internal/d/d;

.field l:I

.field final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/p;",
            ">;>;"
        }
    .end annotation
.end field

.field n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2841
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/map/internal/d/i;->a:J

    return-void
.end method

.method protected constructor <init>(Lcom/google/r/b/a/el;II)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 2889
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/shared/net/i;-><init>(Lcom/google/r/b/a/el;)V

    .line 2847
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->i:Z

    .line 2851
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->j:Z

    .line 2866
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/j;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/gmm/map/internal/d/j;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/b/a/an;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->d:Lcom/google/android/apps/gmm/map/internal/d/j;

    .line 2890
    add-int v0, p2, p3

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/d/p;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 2891
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->b:I

    .line 2892
    if-nez p3, :cond_0

    const/4 v0, -0x1

    :goto_0
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->c:I

    .line 2893
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->e:Ljava/util/Map;

    .line 2894
    if-lez p3, :cond_1

    .line 2895
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->m:Ljava/util/Map;

    .line 2899
    :goto_1
    return-void

    :cond_0
    move v0, p3

    .line 2892
    goto :goto_0

    .line 2897
    :cond_1
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->m:Ljava/util/Map;

    goto :goto_1
.end method


# virtual methods
.method protected final M_()J
    .locals 2

    .prologue
    .line 2913
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->n:Z

    if-eqz v0, :cond_0

    .line 2914
    sget-wide v0, Lcom/google/android/apps/gmm/map/internal/d/i;->a:J

    .line 2916
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/gmm/shared/net/i;->M_()J

    move-result-wide v0

    goto :goto_0
.end method

.method Q_()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3014
    move v0, v1

    :goto_0
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->l:I

    if-ge v0, v3, :cond_2

    .line 3015
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/p;->e:Lcom/google/android/apps/gmm/map/internal/d/n;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/n;->d:Lcom/google/android/apps/gmm/map/internal/d/n;

    if-ne v3, v4, :cond_1

    move v1, v2

    .line 3025
    :cond_0
    :goto_1
    return v1

    .line 3014
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3021
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->b:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->c:I

    if-nez v0, :cond_0

    :cond_3
    move v1, v2

    .line 3022
    goto :goto_1
.end method

.method protected a()I
    .locals 1

    .prologue
    .line 3059
    const/4 v0, -0x1

    return v0
.end method

.method protected abstract a(ILcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/map/internal/c/bo;
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/b/a/an;)Ljava/lang/Integer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "Lcom/google/b/a/an",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .prologue
    .line 3005
    monitor-enter p0

    :try_start_0
    iget-object v0, p2, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 3006
    new-instance v0, Lcom/google/b/a/an;

    iget-object v1, p2, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/b/a/an;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object p2, v0

    .line 3008
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->d:Lcom/google/android/apps/gmm/map/internal/d/j;

    iput-object p1, v0, Lcom/google/android/apps/gmm/map/internal/d/j;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    iput-object p2, v0, Lcom/google/android/apps/gmm/map/internal/d/j;->b:Lcom/google/b/a/an;

    .line 3009
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->e:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->d:Lcom/google/android/apps/gmm/map/internal/d/j;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 3005
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/b/a/an;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/d/p;",
            "Lcom/google/b/a/an",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2944
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 2947
    if-eqz p2, :cond_2

    .line 2948
    iget-object v0, p2, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 2949
    new-instance v0, Lcom/google/b/a/an;

    iget-object v2, p2, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    const-string v3, ""

    invoke-direct {v0, v2, v3}, Lcom/google/b/a/an;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object p2, v0

    .line 2951
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/j;

    invoke-direct {v0, v1, p2}, Lcom/google/android/apps/gmm/map/internal/d/j;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/b/a/an;)V

    .line 2952
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->e:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2953
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x35

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Duplicate tile key: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", already exists in batch for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2957
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->e:Ljava/util/Map;

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2959
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->l:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->l:I

    aput-object p1, v0, v1

    .line 2960
    if-eqz p3, :cond_5

    .line 2961
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->c:I

    .line 2965
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->m:Ljava/util/Map;

    if-eqz v0, :cond_4

    .line 2966
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->m:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2967
    if-nez v0, :cond_3

    .line 2968
    const/4 v0, 0x2

    new-instance v1, Ljava/util/ArrayList;

    const-wide/16 v2, 0x5

    int-to-long v4, v0

    add-long/2addr v2, v4

    const/4 v0, 0x0

    int-to-long v4, v0

    add-long/2addr v2, v4

    const-wide/32 v4, 0x7fffffff

    cmp-long v0, v2, v4

    if-lez v0, :cond_6

    const v0, 0x7fffffff

    :goto_1
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2969
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->m:Ljava/util/Map;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 2971
    :cond_3
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2973
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->n:Z

    iget-boolean v1, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->n:Z

    .line 2974
    return-void

    .line 2963
    :cond_5
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->b:I

    goto :goto_0

    .line 2968
    :cond_6
    const-wide/32 v4, -0x80000000

    cmp-long v0, v2, v4

    if-gez v0, :cond_7

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_7
    long-to-int v0, v2

    goto :goto_1
.end method

.method protected final a(Lcom/google/android/apps/gmm/map/internal/d/p;Z)V
    .locals 1

    .prologue
    .line 2938
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->h:Lcom/google/b/a/an;

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/apps/gmm/map/internal/d/i;->a(Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/b/a/an;Z)V

    .line 2939
    return-void
.end method

.method protected final a(Z)V
    .locals 1

    .prologue
    .line 2925
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->i:Z

    .line 2926
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->j:Z

    .line 2927
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/i;->onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V

    .line 2928
    return-void
.end method

.method protected a(Lcom/google/android/apps/gmm/map/internal/d/p;)Z
    .locals 1

    .prologue
    .line 3098
    const/4 v0, 0x1

    return v0
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 2997
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract b(ILcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/map/internal/c/bt;
.end method

.method protected c(I)[B
    .locals 1

    .prologue
    .line 3073
    const/4 v0, 0x0

    return-object v0
.end method

.method protected d(I)[B
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 3085
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 2904
    if-nez p1, :cond_1

    .line 2905
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->k:Lcom/google/android/apps/gmm/map/internal/d/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v0, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2909
    :cond_0
    :goto_0
    return-void

    .line 2907
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->k:Lcom/google/android/apps/gmm/map/internal/d/d;

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/k;->d:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq p1, v1, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/k;->f:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq p1, v1, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/k;->g:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq p1, v1, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/k;->m:Lcom/google/android/apps/gmm/shared/net/k;

    if-ne p1, v1, :cond_0

    :cond_2
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    const/4 v2, 0x4

    new-instance v3, Lcom/google/b/a/an;

    invoke-direct {v3, p0, p1}, Lcom/google/b/a/an;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
