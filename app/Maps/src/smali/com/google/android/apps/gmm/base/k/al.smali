.class public Lcom/google/android/apps/gmm/base/k/al;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/ax;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/libraries/curvular/cw;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/libraries/curvular/ax;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/libraries/curvular/cw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/libraries/curvular/cw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/libraries/curvular/cw;Lcom/google/android/libraries/curvular/cw;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/k/al;->a:Lcom/google/android/libraries/curvular/cw;

    .line 40
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/k/al;->b:Lcom/google/android/libraries/curvular/cw;

    .line 41
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/base/k/al;Landroid/content/Context;)Lcom/google/android/libraries/curvular/cw;
    .locals 1

    .prologue
    .line 20
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->p_()Lcom/google/android/apps/gmm/q/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/q/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/al;->b:Lcom/google/android/libraries/curvular/cw;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/al;->a:Lcom/google/android/libraries/curvular/cw;

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 45
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/k/al;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/base/k/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/k/al;->a:Lcom/google/android/libraries/curvular/cw;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/al;->a:Lcom/google/android/libraries/curvular/cw;

    .line 46
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/gmm/base/k/al;

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/k/al;->b:Lcom/google/android/libraries/curvular/cw;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/al;->b:Lcom/google/android/libraries/curvular/cw;

    .line 47
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 52
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/al;->a:Lcom/google/android/libraries/curvular/cw;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/al;->b:Lcom/google/android/libraries/curvular/cw;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
