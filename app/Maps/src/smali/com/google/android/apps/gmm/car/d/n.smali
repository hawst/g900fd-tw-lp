.class Lcom/google/android/apps/gmm/car/d/n;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/d/m;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/d/m;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/d/n;->a:Lcom/google/android/apps/gmm/car/d/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/j/p;)V
    .locals 12
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/n;->a:Lcom/google/android/apps/gmm/car/d/m;

    iget-object v6, v0, Lcom/google/android/apps/gmm/car/d/m;->b:Lcom/google/android/apps/gmm/car/d/o;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/p;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v2

    const-wide v8, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v10, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v10

    mul-double/2addr v4, v8

    const-wide v8, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v8

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v8

    invoke-direct {v1, v4, v5, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    const-string v0, "%.7f,%.7f"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v7

    const/4 v4, 0x1

    iget-wide v8, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->d()Lcom/google/android/apps/gmm/map/r/a/aq;

    move-result-object v0

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-boolean v7, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/gmm/car/bm;

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/bm;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/base/g/c;)V

    invoke-interface {v6, v0}, Lcom/google/android/apps/gmm/car/d/o;->a(Lcom/google/android/apps/gmm/car/bm;)V

    .line 70
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/t;)V
    .locals 14
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 58
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/g/a;

    if-eqz v0, :cond_7

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/n;->a:Lcom/google/android/apps/gmm/car/d/m;

    iget-object v8, v0, Lcom/google/android/apps/gmm/car/d/m;->b:Lcom/google/android/apps/gmm/car/d/o;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    check-cast v0, Lcom/google/android/apps/gmm/map/g/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/g/a;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/g/a;->b:Lcom/google/android/apps/gmm/map/internal/c/bb;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/g/a;->b:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bb;->a:Ljava/lang/String;

    :goto_0
    new-instance v4, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/base/g/g;->a(Lcom/google/android/apps/gmm/map/g/a;)Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v4

    iput-boolean v2, v4, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    iput-object v1, v4, Lcom/google/android/apps/gmm/base/g/g;->o:Ljava/lang/String;

    iget-object v1, v4, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/g/i;->h:Z

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/g/a;->g:Z

    iget-object v1, v4, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/g/i;->e:Ljava/lang/Boolean;

    iput-boolean v3, v4, Lcom/google/android/apps/gmm/base/g/g;->i:Z

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v4

    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->d()Lcom/google/android/apps/gmm/map/r/a/aq;

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v4, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    if-nez v6, :cond_0

    iget-object v6, v4, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-virtual {v6}, Lcom/google/r/b/a/ads;->h()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    :cond_0
    iget-object v4, v4, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/bm;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/base/g/c;)V

    :goto_1
    invoke-interface {v8, v0}, Lcom/google/android/apps/gmm/car/d/o;->a(Lcom/google/android/apps/gmm/car/bm;)V

    .line 64
    :cond_1
    :goto_2
    return-void

    .line 59
    :cond_2
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/g/d;->i:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/g/d;->i:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_5

    :cond_4
    move v1, v3

    :goto_3
    if-nez v1, :cond_6

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/g/d;->i:Ljava/lang/String;

    :goto_4
    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->d()Lcom/google/android/apps/gmm/map/r/a/aq;

    move-result-object v1

    iput-object v5, v1, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    iput-object v4, v1, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/g/d;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/q;

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v6, v3

    const-wide v10, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v6, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    move-result-wide v6

    const-wide v12, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v6, v12

    mul-double/2addr v6, v10

    const-wide v10, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v6, v10

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v10

    invoke-direct {v2, v6, v7, v10, v11}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iput-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v3

    new-instance v2, Lcom/google/android/apps/gmm/car/bm;

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/car/bm;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/base/g/c;)V

    move-object v0, v2

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_3

    :cond_6
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/g/d;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->k()Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    .line 61
    :cond_7
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/g/e;

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/n;->a:Lcom/google/android/apps/gmm/car/d/m;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/d/m;->c:Lcom/google/android/apps/gmm/car/d/p;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    check-cast v0, Lcom/google/android/apps/gmm/map/g/e;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/car/d/p;->a(Lcom/google/android/apps/gmm/map/g/e;)V

    goto :goto_2
.end method
