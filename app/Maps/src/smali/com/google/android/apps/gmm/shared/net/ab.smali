.class Lcom/google/android/apps/gmm/shared/net/ab;
.super Ljava/net/HttpURLConnection;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lorg/apache/http/client/HttpClient;

.field private final d:Landroid/content/Context;

.field private e:Lorg/apache/http/client/methods/HttpUriRequest;

.field private f:Lorg/apache/http/HttpResponse;

.field private g:Ljava/io/ByteArrayOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 65
    const-string v0, "GoogleMobile/1.0 (%s %s); gzip"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Landroid/os/Build;->ID:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 66
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/ab;->a:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public constructor <init>(Ljava/net/URL;Lorg/apache/http/client/HttpClient;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0, p1}, Ljava/net/HttpURLConnection;-><init>(Ljava/net/URL;)V

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->b:Ljava/util/List;

    .line 95
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/net/ab;->c:Lorg/apache/http/client/HttpClient;

    .line 96
    iput-object p3, p0, Lcom/google/android/apps/gmm/shared/net/ab;->d:Landroid/content/Context;

    .line 97
    return-void
.end method

.method static a(Lorg/apache/http/client/HttpClient;Landroid/content/Context;)Ljava/net/URLStreamHandler;
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/ac;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/shared/net/ac;-><init>(Lorg/apache/http/client/HttpClient;Landroid/content/Context;)V

    return-object v0
.end method

.method private a()Lorg/apache/http/HttpResponse;
    .locals 3

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->f:Lorg/apache/http/HttpResponse;

    if-nez v0, :cond_1

    .line 206
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/ab;->connect()V

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->g:Ljava/io/ByteArrayOutputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->e:Lorg/apache/http/client/methods/HttpUriRequest;

    instance-of v0, v0, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->e:Lorg/apache/http/client/methods/HttpUriRequest;

    check-cast v0, Lorg/apache/http/HttpEntityEnclosingRequest;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ab;->g:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ab;->d:Landroid/content/Context;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_0
    invoke-static {v2, v1}, Landroid/net/http/AndroidHttpClient;->getCompressedEntity([BLandroid/content/ContentResolver;)Lorg/apache/http/entity/AbstractHttpEntity;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/apache/http/HttpEntityEnclosingRequest;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->c:Lorg/apache/http/client/HttpClient;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ab;->e:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->f:Lorg/apache/http/HttpResponse;

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->f:Lorg/apache/http/HttpResponse;

    return-object v0

    .line 208
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ab;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->connected:Z

    if-eqz v0, :cond_0

    .line 155
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set request property after connection is made"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->b:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    invoke-direct {v1, p1, p2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->f:Lorg/apache/http/HttpResponse;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->f:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_0

    .line 147
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 150
    :cond_0
    return-void
.end method

.method public connect()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->e:Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 106
    :cond_0
    :try_start_0
    const-string v0, "GET"

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ab;->method:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "POST"

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ab;->method:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 107
    new-instance v0, Ljava/net/ProtocolException;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ab;->method:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " is not supported"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    :catchall_0
    move-exception v0

    iput-boolean v3, p0, Lcom/google/android/apps/gmm/shared/net/ab;->connected:Z

    throw v0

    .line 109
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->doOutput:Z

    if-nez v0, :cond_2

    const-string v0, "POST"

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ab;->method:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 110
    :cond_2
    const-string v0, "POST"

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->method:Ljava/lang/String;

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->doOutput:Z

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->url:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ab;->e:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 116
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/Header;

    .line 117
    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/ab;->e:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Lorg/apache/http/Header;)V

    goto :goto_2

    .line 114
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->url:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ab;->e:Lorg/apache/http/client/methods/HttpUriRequest;

    goto :goto_1

    .line 119
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->e:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 120
    const-string v1, "http.connection.timeout"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/ab;->getConnectTimeout()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 121
    const-string v1, "http.socket.timeout"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/ab;->getReadTimeout()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->e:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-static {v0}, Landroid/net/http/AndroidHttpClient;->modifyRequestToAcceptGzipResponse(Lorg/apache/http/HttpRequest;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/shared/net/ab;->connected:Z

    goto/16 :goto_0
.end method

.method public disconnect()V
    .locals 1

    .prologue
    .line 135
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/ab;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :goto_0
    return-void

    .line 137
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 168
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/shared/net/ab;->a()Lorg/apache/http/HttpResponse;

    move-result-object v1

    invoke-interface {v1, p1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 169
    if-nez v1, :cond_0

    .line 171
    :goto_0
    return-object v0

    .line 169
    :cond_0
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 171
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/google/android/apps/gmm/shared/net/ab;->a()Lorg/apache/http/HttpResponse;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-static {v0}, Landroid/net/http/AndroidHttpClient;->getUngzippedContent(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 3

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/ab;->connect()V

    .line 188
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->doOutput:Z

    if-nez v0, :cond_0

    .line 189
    new-instance v0, Ljava/net/ProtocolException;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ab;->method:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " does not support writing"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->f:Lorg/apache/http/HttpResponse;

    if-eqz v0, :cond_1

    .line 191
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "cannot write request body after response has been read"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->g:Ljava/io/ByteArrayOutputStream;

    if-nez v0, :cond_2

    .line 194
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->g:Ljava/io/ByteArrayOutputStream;

    .line 196
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ab;->g:Ljava/io/ByteArrayOutputStream;

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/gmm/shared/net/ab;->a()Lorg/apache/http/HttpResponse;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    return v0
.end method

.method public setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 162
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public usingProxy()Z
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    return v0
.end method
