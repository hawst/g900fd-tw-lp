.class public Lcom/google/android/apps/gmm/car/d/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final u:Ljava/lang/String;


# instance fields
.field final a:Lcom/google/android/libraries/curvular/bd;

.field final b:Lcom/google/android/apps/gmm/car/m/m;

.field final c:Lcom/google/android/apps/gmm/map/t;

.field final d:Lcom/google/android/apps/gmm/shared/c/f;

.field public final e:Lcom/google/android/apps/gmm/map/util/b/a/a;

.field final f:Z

.field public final g:Landroid/widget/FrameLayout;

.field public final h:Landroid/view/View;

.field public final i:Landroid/view/View;

.field public final j:Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;

.field public k:Lcom/google/android/apps/gmm/car/d/i;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public l:Lcom/google/android/apps/gmm/car/ax;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public m:Z

.field n:Z

.field o:Z

.field p:Z

.field q:I

.field r:Lcom/google/android/apps/gmm/car/d/j;

.field final s:Lcom/google/android/apps/gmm/car/d/l;

.field public final t:Ljava/lang/Object;

.field private final v:I

.field private final w:Landroid/view/animation/Interpolator;

.field private final x:Landroid/view/animation/Interpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/google/android/apps/gmm/car/d/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/d/a;->u:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/car/m/m;Lcom/google/android/apps/gmm/map/t;Landroid/widget/FrameLayout;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/util/b/a/a;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    sget-object v0, Lcom/google/android/apps/gmm/car/d/j;->a:Lcom/google/android/apps/gmm/car/d/j;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->r:Lcom/google/android/apps/gmm/car/d/j;

    .line 282
    new-instance v0, Lcom/google/android/apps/gmm/car/d/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/d/d;-><init>(Lcom/google/android/apps/gmm/car/d/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->s:Lcom/google/android/apps/gmm/car/d/l;

    .line 423
    new-instance v0, Lcom/google/android/apps/gmm/car/d/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/d/g;-><init>(Lcom/google/android/apps/gmm/car/d/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->t:Ljava/lang/Object;

    .line 125
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->a:Lcom/google/android/libraries/curvular/bd;

    .line 126
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/car/m/m;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/d/a;->b:Lcom/google/android/apps/gmm/car/m/m;

    .line 127
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move-object v0, p3

    check-cast v0, Lcom/google/android/apps/gmm/map/t;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->c:Lcom/google/android/apps/gmm/map/t;

    .line 128
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move-object v0, p4

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->g:Landroid/widget/FrameLayout;

    .line 129
    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast p5, Lcom/google/android/apps/gmm/shared/c/f;

    iput-object p5, p0, Lcom/google/android/apps/gmm/car/d/a;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 130
    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move-object v0, p6

    check-cast v0, Lcom/google/android/apps/gmm/map/util/b/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->e:Lcom/google/android/apps/gmm/map/util/b/a/a;

    .line 131
    iput-boolean p7, p0, Lcom/google/android/apps/gmm/car/d/a;->f:Z

    .line 133
    const-class v0, Lcom/google/android/apps/gmm/car/d/k;

    invoke-virtual {p1, v0, v3}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/gmm/car/d/k;->m:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->i:Landroid/view/View;

    .line 135
    sget-object v0, Lcom/google/android/apps/gmm/car/d/k;->j:Lcom/google/android/libraries/curvular/b;

    .line 136
    iget-object v1, p1, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/b;->c_(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/car/d/a;->v:I

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/gmm/car/d/k;->a:Lcom/google/android/libraries/curvular/bk;

    .line 138
    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 139
    new-instance v1, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;

    .line 140
    iget-object v2, p1, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-direct {v1, v2, v3, p3, p6}, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/map/util/b/a/a;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/d/a;->j:Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;

    .line 141
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/a;->j:Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->setClickable(Z)V

    .line 142
    iget-object v1, p1, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/e;->k:I

    .line 143
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 144
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 145
    const/16 v1, 0x11

    iput v1, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 146
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/a;->j:Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/a;->j:Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/car/d/a;->a(Landroid/view/View;Landroid/view/View;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/gmm/car/d/k;->e:Lcom/google/android/libraries/curvular/bk;

    .line 151
    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    sget-object v2, Lcom/google/android/apps/gmm/car/d/k;->d:Lcom/google/android/libraries/curvular/bk;

    .line 152
    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v1

    .line 150
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/car/d/a;->a(Landroid/view/View;Landroid/view/View;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/gmm/car/d/k;->g:Lcom/google/android/libraries/curvular/bk;

    .line 154
    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    sget-object v2, Lcom/google/android/apps/gmm/car/d/k;->f:Lcom/google/android/libraries/curvular/bk;

    .line 155
    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v1

    .line 153
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/car/d/a;->a(Landroid/view/View;Landroid/view/View;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/gmm/car/d/k;->c:Lcom/google/android/libraries/curvular/bk;

    .line 157
    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    sget-object v2, Lcom/google/android/apps/gmm/car/d/k;->b:Lcom/google/android/libraries/curvular/bk;

    .line 158
    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v1

    .line 156
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/car/d/a;->a(Landroid/view/View;Landroid/view/View;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/gmm/car/d/k;->l:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 162
    new-instance v1, Lcom/google/android/apps/gmm/car/d/b;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/d/b;-><init>(Lcom/google/android/apps/gmm/car/d/a;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 173
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 174
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->c:Z

    if-eqz v0, :cond_6

    .line 175
    iget-object v0, p1, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    const v1, 0x10c000e

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->w:Landroid/view/animation/Interpolator;

    .line 177
    iget-object v0, p1, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    const v1, 0x10c000f

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->x:Landroid/view/animation/Interpolator;

    .line 184
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    invoke-virtual {p4, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->j:Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->e()V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/a;->s:Lcom/google/android/apps/gmm/car/d/l;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/d/a;->a()V

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->t:Ljava/lang/Object;

    invoke-interface {p6, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->d(Ljava/lang/Object;)V

    .line 191
    return-void

    .line 180
    :cond_6
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->w:Landroid/view/animation/Interpolator;

    .line 181
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->x:Landroid/view/animation/Interpolator;

    goto :goto_0
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 276
    new-instance v0, Landroid/view/TouchDelegate;

    new-instance v1, Landroid/graphics/Rect;

    sget-object v2, Lcom/google/android/apps/gmm/car/d/k;->j:Lcom/google/android/libraries/curvular/b;

    .line 277
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/b;->c_(Landroid/content/Context;)I

    move-result v2

    sget-object v3, Lcom/google/android/apps/gmm/car/d/k;->k:Lcom/google/android/libraries/curvular/b;

    .line 278
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/curvular/b;->c_(Landroid/content/Context;)I

    move-result v3

    invoke-direct {v1, v5, v5, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-direct {v0, v1, p1}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    .line 276
    invoke-virtual {p0, v0}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 280
    return-void
.end method

.method private a(Z)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->r:Lcom/google/android/apps/gmm/car/d/j;

    sget-object v2, Lcom/google/android/apps/gmm/car/d/j;->a:Lcom/google/android/apps/gmm/car/d/j;

    if-eq v0, v2, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 404
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 405
    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->getInterpolator()Landroid/animation/TimeInterpolator;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/d/a;->x:Landroid/view/animation/Interpolator;

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->i:Landroid/view/View;

    .line 406
    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v3

    if-ltz v0, :cond_2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->getStartDelay()J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    .line 421
    :cond_2
    :goto_1
    return-void

    .line 410
    :cond_3
    if-eqz p1, :cond_5

    .line 411
    const-wide/16 v4, 0x1388

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 416
    :goto_2
    iget v0, p0, Lcom/google/android/apps/gmm/car/d/a;->v:I

    .line 417
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v3

    if-ne v3, v1, :cond_4

    .line 418
    neg-int v0, v0

    .line 420
    :cond_4
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/a;->x:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    goto :goto_1

    .line 413
    :cond_5
    invoke-virtual {v2, v6, v7}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_2
.end method

.method private c()V
    .locals 4

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->r:Lcom/google/android/apps/gmm/car/d/j;

    sget-object v1, Lcom/google/android/apps/gmm/car/d/j;->b:Lcom/google/android/apps/gmm/car/d/j;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 378
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 379
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->getInterpolator()Landroid/animation/TimeInterpolator;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/d/a;->w:Landroid/view/animation/Interpolator;

    if-ne v1, v2, :cond_2

    .line 397
    :goto_1
    return-void

    .line 382
    :cond_2
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 383
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/a;->w:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/car/d/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/d/e;-><init>(Lcom/google/android/apps/gmm/car/d/a;)V

    .line 384
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/d/a;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/d/a;->n:Z

    if-eqz v0, :cond_1

    .line 236
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/car/d/j;->b:Lcom/google/android/apps/gmm/car/d/j;

    .line 242
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/a;->r:Lcom/google/android/apps/gmm/car/d/j;

    if-eq v1, v0, :cond_4

    .line 243
    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->r:Lcom/google/android/apps/gmm/car/d/j;

    .line 244
    sget-object v0, Lcom/google/android/apps/gmm/car/d/a;->u:Ljava/lang/String;

    const-string v0, "State changed: "

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/a;->r:Lcom/google/android/apps/gmm/car/d/j;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/d/j;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 245
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/a;->s:Lcom/google/android/apps/gmm/car/d/l;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 246
    sget-object v0, Lcom/google/android/apps/gmm/car/d/h;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/a;->r:Lcom/google/android/apps/gmm/car/d/j;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/d/j;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 257
    new-instance v1, Ljava/lang/AssertionError;

    const-string v2, "Unexpected map buttons state: "

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->r:Lcom/google/android/apps/gmm/car/d/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/d/j;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 237
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/d/a;->o:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/gmm/car/d/a;->q:I

    if-nez v0, :cond_2

    .line 238
    sget-object v0, Lcom/google/android/apps/gmm/car/d/j;->c:Lcom/google/android/apps/gmm/car/d/j;

    goto :goto_0

    .line 240
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/car/d/j;->a:Lcom/google/android/apps/gmm/car/d/j;

    goto :goto_0

    .line 244
    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 248
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/car/d/a;->a(Z)V

    .line 260
    :cond_4
    :goto_3
    return-void

    .line 251
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/car/d/a;->c()V

    goto :goto_3

    .line 254
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/d/a;->b()V

    goto :goto_3

    .line 257
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 246
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method b()V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->r:Lcom/google/android/apps/gmm/car/d/j;

    sget-object v1, Lcom/google/android/apps/gmm/car/d/j;->c:Lcom/google/android/apps/gmm/car/d/j;

    if-eq v0, v1, :cond_0

    .line 374
    :goto_0
    return-void

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/a;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 370
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/car/d/a;->a(Z)V

    goto :goto_0

    .line 372
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/car/d/a;->c()V

    goto :goto_0
.end method
