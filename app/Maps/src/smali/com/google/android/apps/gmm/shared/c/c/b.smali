.class public Lcom/google/android/apps/gmm/shared/c/c/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Lcom/google/android/apps/gmm/shared/c/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/a;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/c/b;->a:Lcom/google/android/apps/gmm/shared/c/f;

    return-void
.end method

.method public static a(IIII)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 190
    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    long-to-int v1, v2

    .line 191
    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, p0

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    int-to-long v4, p1

    add-long/2addr v2, v4

    long-to-int v2, v2

    .line 192
    sget-object v3, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, p2

    .line 193
    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v4

    int-to-long v6, p3

    add-long/2addr v4, v6

    long-to-int v3, v4

    .line 195
    add-int v4, v2, v1

    sub-int/2addr v4, v3

    rem-int/2addr v4, v1

    .line 198
    add-int v5, v3, v1

    sub-int/2addr v5, v2

    rem-int v1, v5, v1

    .line 202
    if-ge v4, v1, :cond_1

    .line 203
    if-ge v2, v3, :cond_0

    const/4 v0, 0x1

    .line 207
    :cond_0
    :goto_0
    return v0

    .line 204
    :cond_1
    if-le v4, v1, :cond_0

    .line 205
    if-le v2, v3, :cond_0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/util/Calendar;)Ljava/lang/String;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x6

    .line 38
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v3

    .line 40
    sget-object v2, Lcom/google/android/apps/gmm/shared/c/c/b;->a:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 41
    invoke-static {p1, v3}, Lcom/google/android/apps/gmm/shared/c/c/b;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v2, v4, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    .line 42
    sget v0, Lcom/google/android/apps/gmm/l;->nz:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 54
    :goto_1
    return-object v0

    :cond_0
    move v2, v1

    .line 41
    goto :goto_0

    .line 45
    :cond_1
    const/4 v2, -0x1

    invoke-virtual {v3, v6, v2}, Ljava/util/Calendar;->add(II)V

    .line 46
    invoke-static {p1, v3}, Lcom/google/android/apps/gmm/shared/c/c/b;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v2, v4, :cond_2

    move v2, v0

    :goto_2
    if-eqz v2, :cond_3

    .line 47
    sget v0, Lcom/google/android/apps/gmm/l;->pf:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v2, v1

    .line 46
    goto :goto_2

    .line 50
    :cond_3
    const/4 v2, 0x2

    invoke-virtual {v3, v6, v2}, Ljava/util/Calendar;->add(II)V

    .line 51
    invoke-static {p1, v3}, Lcom/google/android/apps/gmm/shared/c/c/b;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_4

    :goto_3
    if-eqz v0, :cond_5

    .line 52
    sget v0, Lcom/google/android/apps/gmm/l;->nA:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    move v0, v1

    .line 51
    goto :goto_3

    .line 54
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/util/Calendar;ZI)Ljava/lang/String;
    .locals 8

    .prologue
    .line 70
    if-eqz p2, :cond_0

    .line 71
    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/shared/c/c/b;->a(Landroid/content/Context;Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_0

    .line 83
    :goto_0
    return-object v0

    .line 76
    :cond_0
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 77
    new-instance v1, Ljava/util/Formatter;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v4, 0x32

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 79
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v1, v0, v4}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 83
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    move-wide v4, v2

    move v6, p3

    .line 77
    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 108
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 109
    :cond_0
    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/util/Calendar;ZI)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 91
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/shared/c/c/a;-><init>(Landroid/content/Context;)V

    .line 92
    if-eqz p2, :cond_0

    .line 93
    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/shared/c/c/b;->a(Landroid/content/Context;Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v1

    .line 94
    if-eqz v1, :cond_0

    .line 95
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 98
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {p0, v2, v3, p3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_3

    .line 99
    :cond_1
    :goto_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->a:Ljava/lang/StringBuffer;

    return-object v0

    .line 95
    :cond_2
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_0

    .line 98
    :cond_3
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_1
.end method

.method public static b(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 2

    .prologue
    const/4 v1, 0x6

    .line 120
    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/shared/c/c/b;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 5

    .prologue
    const/16 v4, 0xb

    const/4 v3, 0x6

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 131
    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/shared/c/c/b;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    .line 132
    invoke-virtual {p0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 131
    goto :goto_0

    :cond_1
    move v0, v1

    .line 132
    goto :goto_1
.end method

.method public static d(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 2

    .prologue
    const/16 v1, 0xc

    .line 142
    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/shared/c/c/b;->c(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
