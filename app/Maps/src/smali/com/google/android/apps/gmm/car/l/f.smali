.class Lcom/google/android/apps/gmm/car/l/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/shared/net/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/shared/net/c",
        "<",
        "Lcom/google/r/b/a/aok;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/l/c;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/l/c;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/l/f;->a:Lcom/google/android/apps/gmm/car/l/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 6

    .prologue
    .line 199
    check-cast p1, Lcom/google/r/b/a/aok;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/d;->b()Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/f;->a:Lcom/google/android/apps/gmm/car/l/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/l/c;->e:Lcom/google/android/apps/gmm/car/l/i;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/l/i;->b:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/gmm/car/l/l;->c:Lcom/google/android/apps/gmm/car/l/l;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/l/i;->c:Lcom/google/android/apps/gmm/car/l/l;

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/f;->a:Lcom/google/android/apps/gmm/car/l/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/l/c;->d:Landroid/view/View;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/l/c;->e:Lcom/google/android/apps/gmm/car/l/i;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/r/b/a/aok;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/f;->a:Lcom/google/android/apps/gmm/car/l/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/l/c;->e:Lcom/google/android/apps/gmm/car/l/i;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/f;->a:Lcom/google/android/apps/gmm/car/l/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/l/c;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v2, Lcom/google/android/apps/gmm/l;->nB:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/gmm/car/l/i;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/f;->a:Lcom/google/android/apps/gmm/car/l/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/l/c;->e:Lcom/google/android/apps/gmm/car/l/i;

    const-string v0, ""

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/gmm/car/l/i;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/gmm/car/l/l;->b:Lcom/google/android/apps/gmm/car/l/l;

    iput-object v0, v1, Lcom/google/android/apps/gmm/car/l/i;->c:Lcom/google/android/apps/gmm/car/l/l;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iget-object v1, p1, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/anw;->i()Lcom/google/r/b/a/anw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/anw;

    iget-wide v2, v0, Lcom/google/r/b/a/anw;->b:J

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/l/f;->a:Lcom/google/android/apps/gmm/car/l/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/l/c;->c:Lcom/google/android/apps/gmm/map/g/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/g/e;->a:Lcom/google/android/apps/gmm/map/internal/c/cf;

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/internal/c/cf;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/apps/gmm/car/l/c;->a:Ljava/lang/String;

    iget-wide v0, v0, Lcom/google/r/b/a/anw;->b:J

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x4c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Note: superseded traffic incident ignored.  IncidentId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/l/f;->a:Lcom/google/android/apps/gmm/car/l/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/l/c;->e:Lcom/google/android/apps/gmm/car/l/i;

    invoke-virtual {v0}, Lcom/google/r/b/a/anw;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/gmm/car/l/i;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/gmm/car/l/l;->b:Lcom/google/android/apps/gmm/car/l/l;

    iput-object v0, v1, Lcom/google/android/apps/gmm/car/l/i;->c:Lcom/google/android/apps/gmm/car/l/l;

    goto/16 :goto_0
.end method
