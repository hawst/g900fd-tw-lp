.class public Lcom/google/android/apps/gmm/search/ax;
.super Lcom/google/android/apps/gmm/base/placelists/a/d;
.source "PG"


# instance fields
.field private b:Lcom/google/android/apps/gmm/search/az;

.field private c:Lcom/google/android/apps/gmm/place/j/y;

.field private d:Lcom/google/android/apps/gmm/map/r/b/a;

.field private final e:I

.field private f:Landroid/view/View$OnClickListener;

.field private g:Lcom/google/android/apps/gmm/base/activities/c;

.field private final h:Lcom/google/b/f/t;

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/base/activities/c;IZZLandroid/view/View$OnClickListener;Lcom/google/b/f/t;)V
    .locals 10

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/d;-><init>(Lcom/google/android/apps/gmm/base/g/c;)V

    .line 50
    iput-object p3, p0, Lcom/google/android/apps/gmm/search/ax;->g:Lcom/google/android/apps/gmm/base/activities/c;

    .line 51
    iput-object p2, p0, Lcom/google/android/apps/gmm/search/ax;->d:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 52
    iput p4, p0, Lcom/google/android/apps/gmm/search/ax;->e:I

    .line 53
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ax;->f:Landroid/view/View$OnClickListener;

    .line 54
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ax;->h:Lcom/google/b/f/t;

    .line 55
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/search/ax;->i:Z

    .line 56
    move/from16 v0, p6

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/ax;->j:Z

    .line 57
    new-instance v1, Lcom/google/android/apps/gmm/place/j/y;

    .line 58
    new-instance v4, Lcom/google/android/apps/gmm/search/ay;

    move-object/from16 v0, p7

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/search/ay;-><init>(Landroid/view/View$OnClickListener;)V

    const/4 v5, 0x0

    .line 59
    iget-object v2, p3, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 60
    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/b/f/cq;

    const/4 v6, 0x0

    aput-object p8, v3, v6

    iput-object v3, v2, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v9

    move-object v2, p3

    move-object v3, p1

    move-object v6, p2

    move/from16 v7, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/gmm/place/j/y;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/libraries/curvular/cg;ILcom/google/android/apps/gmm/map/r/b/a;ZLcom/google/android/apps/gmm/directions/a/f;Lcom/google/android/apps/gmm/z/b/l;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/ax;->c:Lcom/google/android/apps/gmm/place/j/y;

    .line 61
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/ax;->i:Z

    if-eqz v0, :cond_0

    .line 118
    sget v0, Lcom/google/android/apps/gmm/h;->ah:I

    .line 120
    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/apps/gmm/h;->ai:I

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)Lcom/google/android/apps/gmm/base/views/a/f;
    .locals 2

    .prologue
    .line 100
    new-instance v0, Lcom/google/android/apps/gmm/search/az;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/search/az;-><init>()V

    .line 101
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/search/ax;->i:Z

    if-eqz v1, :cond_0

    .line 102
    check-cast p1, Lcom/google/android/apps/gmm/base/views/QuCommodityTactilePlaceView;

    iput-object p1, v0, Lcom/google/android/apps/gmm/search/az;->b:Lcom/google/android/apps/gmm/base/views/QuCommodityTactilePlaceView;

    .line 107
    :goto_0
    return-object v0

    .line 104
    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/base/views/QuTactilePlaceView;

    iput-object p1, v0, Lcom/google/android/apps/gmm/search/az;->a:Lcom/google/android/apps/gmm/base/views/QuTactilePlaceView;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/a/f;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 84
    check-cast p1, Lcom/google/android/apps/gmm/search/az;

    iput-object p1, p0, Lcom/google/android/apps/gmm/search/ax;->b:Lcom/google/android/apps/gmm/search/az;

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ax;->g:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ax;->d:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 86
    new-instance v0, Lcom/google/android/apps/gmm/place/j/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/ax;->g:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/ax;->a:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v5, p0, Lcom/google/android/apps/gmm/search/ax;->f:Landroid/view/View$OnClickListener;

    .line 87
    new-instance v3, Lcom/google/android/apps/gmm/search/ay;

    invoke-direct {v3, v5}, Lcom/google/android/apps/gmm/search/ay;-><init>(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/apps/gmm/search/ax;->d:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-boolean v6, p0, Lcom/google/android/apps/gmm/search/ax;->j:Z

    iget-object v7, p0, Lcom/google/android/apps/gmm/search/ax;->g:Lcom/google/android/apps/gmm/base/activities/c;

    .line 88
    iget-object v7, v7, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 89
    invoke-interface {v7}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/gmm/search/ax;->a:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v9, p0, Lcom/google/android/apps/gmm/search/ax;->h:Lcom/google/b/f/t;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v8

    const/4 v10, 0x1

    new-array v10, v10, [Lcom/google/b/f/cq;

    aput-object v9, v10, v4

    iput-object v10, v8, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/place/j/y;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/libraries/curvular/cg;ILcom/google/android/apps/gmm/map/r/b/a;ZLcom/google/android/apps/gmm/directions/a/f;Lcom/google/android/apps/gmm/z/b/l;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ax;->c:Lcom/google/android/apps/gmm/place/j/y;

    .line 90
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/ax;->i:Z

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ax;->b:Lcom/google/android/apps/gmm/search/az;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/az;->b:Lcom/google/android/apps/gmm/base/views/QuCommodityTactilePlaceView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/ax;->c:Lcom/google/android/apps/gmm/place/j/y;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 96
    :goto_0
    return-void

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ax;->b:Lcom/google/android/apps/gmm/search/az;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/az;->a:Lcom/google/android/apps/gmm/base/views/QuTactilePlaceView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/ax;->c:Lcom/google/android/apps/gmm/place/j/y;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;)V
    .locals 4

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/ax;->d:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ax;->b:Lcom/google/android/apps/gmm/search/az;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ax;->c:Lcom/google/android/apps/gmm/place/j/y;

    iput-object p1, v0, Lcom/google/android/apps/gmm/place/j/y;->c:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/j/y;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/j/y;->d:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v1, v2, p1, v3}, Lcom/google/android/apps/gmm/place/j/y;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/x/o;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/j/y;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 136
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/google/android/apps/gmm/search/ax;->e:I

    return v0
.end method
