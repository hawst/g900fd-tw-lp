.class Lcom/google/android/apps/gmm/map/internal/d/c/h;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field a:Lcom/google/e/a/a/a/b;

.field b:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

.field final synthetic c:Lcom/google/android/apps/gmm/map/internal/d/c/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/c/a;Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V
    .locals 2

    .prologue
    .line 941
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/h;->c:Lcom/google/android/apps/gmm/map/internal/d/c/a;

    .line 942
    sget-object v0, Lcom/google/r/b/a/el;->M:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/ah;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 943
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/h;->a:Lcom/google/e/a/a/a/b;

    .line 944
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/c/h;->b:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    .line 945
    return-void
.end method


# virtual methods
.method protected final S_()Lcom/google/b/a/ak;
    .locals 5

    .prologue
    .line 937
    invoke-super {p0}, Lcom/google/android/apps/gmm/shared/net/af;->S_()Lcom/google/b/a/ak;

    move-result-object v2

    const-string v1, "ResourceRequestProto.URL"

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/h;->a:Lcom/google/e/a/a/a/b;

    if-nez v0, :cond_0

    const-string v0, "<NULL>"

    :goto_0
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v0, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/h;->a:Lcom/google/e/a/a/a/b;

    const/4 v3, 0x4

    .line 938
    const/16 v4, 0x1c

    invoke-virtual {v0, v3, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 937
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    return-object v2
.end method

.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 949
    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v2}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_0

    .line 950
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->k:Lcom/google/android/apps/gmm/shared/net/k;

    .line 960
    :goto_0
    return-object v0

    .line 955
    :cond_0
    const/4 v0, 0x0

    const/16 v1, 0x1a

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 956
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/h;->b:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(Lcom/google/e/a/a/a/b;)Z

    move-result v1

    .line 957
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/h;->c:Lcom/google/android/apps/gmm/map/internal/d/c/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/c/a;->f:Lcom/google/android/apps/gmm/map/internal/d/b/q;

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/h;->b:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->c:Z

    if-eqz v1, :cond_1

    .line 958
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/h;->c:Lcom/google/android/apps/gmm/map/internal/d/c/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/c/a;->f:Lcom/google/android/apps/gmm/map/internal/d/b/q;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/q;->a(Lcom/google/e/a/a/a/b;)V

    .line 960
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 970
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->g:Lcom/google/android/apps/gmm/shared/net/k;

    if-ne p1, v0, :cond_0

    .line 971
    const/4 v0, 0x0

    .line 973
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/shared/net/af;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/h;->a:Lcom/google/e/a/a/a/b;

    return-object v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 982
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/h;->b:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    monitor-enter v1

    .line 983
    if-nez p1, :cond_0

    .line 984
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/h;->b:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->g()V

    .line 993
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/h;->b:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(Z)V

    .line 994
    monitor-exit v1

    return-void

    .line 986
    :cond_0
    const/4 v0, 0x1

    .line 987
    sget-object v2, Lcom/google/android/apps/gmm/shared/net/k;->g:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq p1, v2, :cond_1

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/k;->m:Lcom/google/android/apps/gmm/shared/net/k;

    if-ne p1, v2, :cond_2

    .line 989
    :cond_1
    const/4 v0, 0x2

    .line 991
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/h;->b:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(I)V

    goto :goto_0

    .line 994
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
