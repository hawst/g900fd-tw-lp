.class final enum Lcom/google/android/apps/gmm/cardui/d/y;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/cardui/d/y;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/cardui/d/y;

.field public static final enum b:Lcom/google/android/apps/gmm/cardui/d/y;

.field public static final enum c:Lcom/google/android/apps/gmm/cardui/d/y;

.field public static final enum d:Lcom/google/android/apps/gmm/cardui/d/y;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/cardui/d/y;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/google/android/apps/gmm/cardui/d/y;

    const-string v1, "SHOW_NOTHING"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/cardui/d/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/cardui/d/y;->a:Lcom/google/android/apps/gmm/cardui/d/y;

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/cardui/d/y;

    const-string v1, "SHOW_LINE_ICON"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/cardui/d/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/cardui/d/y;->b:Lcom/google/android/apps/gmm/cardui/d/y;

    .line 27
    new-instance v0, Lcom/google/android/apps/gmm/cardui/d/y;

    const-string v1, "SHOW_LINE_NAME"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/cardui/d/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/cardui/d/y;->c:Lcom/google/android/apps/gmm/cardui/d/y;

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/cardui/d/y;

    const-string v1, "SHOW_VEHICLE_ICON"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/cardui/d/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/cardui/d/y;->d:Lcom/google/android/apps/gmm/cardui/d/y;

    .line 24
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/cardui/d/y;

    sget-object v1, Lcom/google/android/apps/gmm/cardui/d/y;->a:Lcom/google/android/apps/gmm/cardui/d/y;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/cardui/d/y;->b:Lcom/google/android/apps/gmm/cardui/d/y;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/cardui/d/y;->c:Lcom/google/android/apps/gmm/cardui/d/y;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/cardui/d/y;->d:Lcom/google/android/apps/gmm/cardui/d/y;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/cardui/d/y;->e:[Lcom/google/android/apps/gmm/cardui/d/y;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/cardui/d/y;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/gmm/cardui/d/y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/cardui/d/y;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/cardui/d/y;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/apps/gmm/cardui/d/y;->e:[Lcom/google/android/apps/gmm/cardui/d/y;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/cardui/d/y;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/cardui/d/y;

    return-object v0
.end method
