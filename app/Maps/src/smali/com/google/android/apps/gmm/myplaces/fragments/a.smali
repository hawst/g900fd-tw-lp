.class Lcom/google/android/apps/gmm/myplaces/fragments/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/myplaces/j;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/google/android/apps/gmm/myplaces/a/a;

.field final synthetic c:I

.field final synthetic d:Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;ZLcom/google/android/apps/gmm/myplaces/a/a;I)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/fragments/a;->d:Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;

    iput-boolean p2, p0, Lcom/google/android/apps/gmm/myplaces/fragments/a;->a:Z

    iput-object p3, p0, Lcom/google/android/apps/gmm/myplaces/fragments/a;->b:Lcom/google/android/apps/gmm/myplaces/a/a;

    iput p4, p0, Lcom/google/android/apps/gmm/myplaces/fragments/a;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ZLjava/lang/String;Lcom/google/e/a/a/a/b;)V
    .locals 5
    .param p3    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/fragments/a;->d:Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/fragments/a;->d:Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 200
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;->a:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/myplaces/fragments/a;->a:Z

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "succeeded = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", popBackStack = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 201
    if-eqz p1, :cond_4

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/fragments/a;->b:Lcom/google/android/apps/gmm/myplaces/a/a;

    invoke-interface {v0, p2}, Lcom/google/android/apps/gmm/myplaces/a/a;->a(Ljava/lang/String;)V

    .line 203
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/myplaces/fragments/a;->a:Z

    if-eqz v0, :cond_2

    .line 207
    if-nez v2, :cond_3

    const/4 v0, 0x0

    .line 208
    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/app/Fragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 209
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 213
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/fragments/a;->b:Lcom/google/android/apps/gmm/myplaces/a/a;

    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/b;->b:Lcom/google/android/apps/gmm/myplaces/c/b;

    .line 214
    invoke-static {v1}, Lcom/google/b/c/dn;->b(Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v1

    const/4 v2, 0x0

    .line 213
    invoke-interface {v0, v1, v2, v4}, Lcom/google/android/apps/gmm/myplaces/a/a;->a(Ljava/util/Set;ZZ)V

    .line 217
    if-eqz p3, :cond_0

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/fragments/a;->d:Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/j/ah;

    invoke-direct {v1, v4, p3}, Lcom/google/android/apps/gmm/map/j/ah;-><init>(ZLcom/google/e/a/a/a/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_0

    .line 207
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    goto :goto_1

    .line 223
    :cond_4
    iget v0, p0, Lcom/google/android/apps/gmm/myplaces/fragments/a;->c:I

    if-nez v0, :cond_5

    .line 224
    sget v0, Lcom/google/android/apps/gmm/l;->gB:I

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 228
    :goto_2
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->b(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 226
    :cond_5
    sget v0, Lcom/google/android/apps/gmm/l;->gC:I

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2
.end method
