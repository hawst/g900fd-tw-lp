.class public Lcom/google/android/apps/gmm/base/views/c/c;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:[I

.field public final b:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/c;->a:[I

    .line 17
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/c;->b:[Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 27
    if-ne p1, p0, :cond_1

    .line 34
    :cond_0
    :goto_0
    return v0

    .line 29
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/base/views/c/c;

    if-eqz v2, :cond_3

    .line 30
    check-cast p1, Lcom/google/android/apps/gmm/base/views/c/c;

    .line 31
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/c;->a:[I

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/views/c/c;->a:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/c;->b:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/views/c/c;->b:[Ljava/lang/String;

    .line 32
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 34
    goto :goto_0
.end method
