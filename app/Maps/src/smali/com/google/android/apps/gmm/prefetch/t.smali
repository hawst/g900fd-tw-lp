.class public Lcom/google/android/apps/gmm/prefetch/t;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/prefetch/s;


# instance fields
.field private a:I

.field private b:Lcom/google/android/apps/gmm/map/c/a;

.field private volatile c:Z

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/prefetch/r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->a:I

    .line 69
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->c:Z

    .line 86
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    .line 87
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 257
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    monitor-enter v1

    .line 258
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 259
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetch/r;

    .line 263
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetch/r;->b()V

    .line 265
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetch/r;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private declared-synchronized d()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 306
    monitor-enter p0

    :try_start_0
    const-string v1, "TileHistoryTracker"

    const-string v2, "Loading tile history"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 307
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 308
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/t;->b:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->w_()Lcom/google/android/apps/gmm/m/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 310
    :try_start_1
    const-string v1, "TILE_HISTORY"

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/m/d;->b(Ljava/lang/String;)[B

    move-result-object v1

    .line 311
    if-eqz v1, :cond_0

    array-length v3, v1

    if-nez v3, :cond_1

    .line 312
    :cond_0
    const-string v0, "TileHistoryTracker"

    const-string v1, "Nothing to load, starting with new history"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 339
    :goto_0
    monitor-exit p0

    return-void

    .line 315
    :cond_1
    :try_start_2
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 316
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 317
    sget-object v3, Lcom/google/r/b/a/b/j;->I:Lcom/google/e/a/a/a/d;

    .line 318
    invoke-static {v3, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/d;Ljava/io/DataInput;)Lcom/google/e/a/a/a/b;

    move-result-object v3

    .line 319
    const/4 v1, 0x1

    iget-object v4, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v4

    move v1, v0

    .line 320
    :goto_1
    if-ge v1, v4, :cond_2

    .line 321
    const/4 v0, 0x1

    .line 322
    const/16 v5, 0x1a

    invoke-virtual {v3, v0, v1, v5}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 323
    iget-object v5, p0, Lcom/google/android/apps/gmm/prefetch/t;->b:Lcom/google/android/apps/gmm/map/c/a;

    .line 324
    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v5

    .line 323
    invoke-static {v0, v5}, Lcom/google/android/apps/gmm/prefetch/r;->a(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/prefetch/r;

    move-result-object v0

    .line 325
    iget-object v5, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    iget-object v6, v0, Lcom/google/android/apps/gmm/prefetch/r;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 327
    :cond_2
    const-string v0, "TileHistoryTracker"

    const-string v1, "Loaded %d entries"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 330
    :catch_0
    move-exception v0

    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 335
    const-string v0, "TILE_HISTORY"

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/m/d;->a(Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 306
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 8

    .prologue
    .line 275
    monitor-enter p0

    :try_start_0
    const-string v0, "TileHistoryTracker"

    const-string v1, "Saving tile history"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 278
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_0

    .line 302
    :goto_0
    monitor-exit p0

    return-void

    .line 282
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetch/t;->c()V

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->b:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v1

    .line 285
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 286
    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 288
    new-instance v4, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/j;->I:Lcom/google/e/a/a/a/d;

    invoke-direct {v4, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 289
    iget-object v5, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    monitor-enter v5
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 290
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetch/r;

    .line 291
    const/4 v7, 0x1

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetch/r;->d()Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-virtual {v4, v7, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    goto :goto_1

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 298
    :catch_0
    move-exception v0

    .line 299
    :try_start_4
    const-string v1, "TileHistoryTracker - error writing tile history"

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 275
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 293
    :cond_1
    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 294
    :try_start_6
    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Ljava/io/DataOutput;Lcom/google/e/a/a/a/b;)V

    .line 295
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const-string v2, "TILE_HISTORY"

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/m/d;->a([BLjava/lang/String;)I

    .line 296
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->a:I

    .line 297
    const-string v0, "TileHistoryTracker"

    const-string v1, "Saved entires: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/b/a/u;ILjava/lang/String;Ljava/lang/Integer;)V
    .locals 7

    .prologue
    .line 209
    monitor-enter p0

    if-nez p1, :cond_0

    .line 232
    :goto_0
    monitor-exit p0

    return-void

    .line 214
    :cond_0
    :try_start_0
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->b(II)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    const/16 v1, 0xe

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(ILcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v1

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetch/r;

    .line 216
    if-nez v0, :cond_1

    .line 217
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/r;

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/t;->b:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/prefetch/r;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 220
    :cond_1
    if-eqz p3, :cond_2

    .line 221
    iput-object p3, v0, Lcom/google/android/apps/gmm/prefetch/r;->c:Ljava/lang/String;

    .line 224
    :cond_2
    if-eqz p4, :cond_3

    .line 225
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/prefetch/r;->b(I)V

    .line 228
    :cond_3
    const-string v2, "TileHistoryTracker"

    const-string v3, "Record usage: %d : %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/bp;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/prefetch/r;->a(I)V

    .line 230
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 1

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->c:Z

    if-nez v0, :cond_0

    .line 102
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetch/t;->b:Lcom/google/android/apps/gmm/map/c/a;

    .line 103
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetch/t;->d()V

    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetch/t;->c()V

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->c:Z

    .line 108
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    :cond_0
    monitor-exit p0

    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Ljava/util/Vector;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetch/t;->c()V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    .line 116
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/apps/gmm/prefetch/r;

    .line 115
    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/prefetch/r;

    .line 117
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 119
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 120
    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 121
    iget-object v4, v4, Lcom/google/android/apps/gmm/prefetch/r;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v2, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 123
    :cond_0
    monitor-exit p0

    return-object v2

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0xa

    .line 355
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 356
    const-string v0, "Tile History"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 357
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/t;->b()Ljava/util/Vector;

    move-result-object v4

    .line 358
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 359
    invoke-virtual {v4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 360
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "coords: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 361
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "score: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/prefetch/r;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/prefetch/r;->c()I

    move-result v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 362
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/apps/gmm/prefetch/t;->d:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetch/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetch/r;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 363
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 358
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 365
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
