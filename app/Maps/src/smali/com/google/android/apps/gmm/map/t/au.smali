.class public Lcom/google/android/apps/gmm/map/t/au;
.super Lcom/google/android/apps/gmm/v/bh;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/v/ak;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/v/ak",
            "<",
            "Lcom/google/android/apps/gmm/map/t/p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/v/bh;-><init>(I)V

    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/v/ak;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/v/ak;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/au;->a:Lcom/google/android/apps/gmm/v/ak;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 4

    .prologue
    .line 41
    check-cast p1, Lcom/google/android/apps/gmm/map/t/p;

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/au;->a:Lcom/google/android/apps/gmm/v/ak;

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/ak;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 43
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 44
    iget-object v1, p1, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Entity "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already in DrawOrderBin"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/v/am;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/v/am;-><init>(Ljava/lang/Object;)V

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/ak;->b:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/ak;->a:Lcom/google/android/apps/gmm/v/am;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/am;->c:Lcom/google/android/apps/gmm/v/am;

    iput-object v2, v1, Lcom/google/android/apps/gmm/v/am;->c:Lcom/google/android/apps/gmm/v/am;

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/ak;->a:Lcom/google/android/apps/gmm/v/am;

    iput-object v2, v1, Lcom/google/android/apps/gmm/v/am;->b:Lcom/google/android/apps/gmm/v/am;

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/ak;->a:Lcom/google/android/apps/gmm/v/am;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/am;->c:Lcom/google/android/apps/gmm/v/am;

    iput-object v1, v2, Lcom/google/android/apps/gmm/v/am;->b:Lcom/google/android/apps/gmm/v/am;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ak;->a:Lcom/google/android/apps/gmm/v/am;

    iput-object v1, v0, Lcom/google/android/apps/gmm/v/am;->c:Lcom/google/android/apps/gmm/v/am;

    const/4 v0, 0x1

    goto :goto_0

    .line 46
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ad;)V
    .locals 6

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/au;->a:Lcom/google/android/apps/gmm/v/ak;

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/ak;->a:Lcom/google/android/apps/gmm/v/am;

    .line 64
    iget-object v0, v3, Lcom/google/android/apps/gmm/v/am;->b:Lcom/google/android/apps/gmm/v/am;

    move-object v2, v0

    .line 65
    :goto_0
    if-eq v2, v3, :cond_2

    .line 66
    iget-object v0, v2, Lcom/google/android/apps/gmm/v/am;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/p;

    .line 67
    iget v1, p0, Lcom/google/android/apps/gmm/map/t/au;->h:I

    iget-byte v4, v0, Lcom/google/android/apps/gmm/v/aa;->u:B

    iget-byte v5, v0, Lcom/google/android/apps/gmm/v/aa;->w:B

    and-int/2addr v4, v5

    and-int/2addr v1, v4

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    .line 72
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/au;->f:Lcom/google/android/apps/gmm/v/n;

    invoke-virtual {v0, p1, p0, v1}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/bh;Lcom/google/android/apps/gmm/v/n;)V

    .line 74
    :cond_0
    iget-object v0, v2, Lcom/google/android/apps/gmm/v/am;->b:Lcom/google/android/apps/gmm/v/am;

    move-object v2, v0

    .line 75
    goto :goto_0

    .line 67
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 76
    :cond_2
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/au;->a:Lcom/google/android/apps/gmm/v/ak;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ak;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/v/ad;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/v/ad;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/au;->a:Lcom/google/android/apps/gmm/v/ak;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/ak;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/p;

    .line 90
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t/p;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 91
    sget-object v3, Lcom/google/android/apps/gmm/v/ab;->d:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v0, p1, v3}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    goto :goto_0

    .line 93
    :cond_0
    sget-object v3, Lcom/google/android/apps/gmm/v/ab;->b:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v0, p1, v3}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    .line 94
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 97
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    .line 98
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/au;->a:Lcom/google/android/apps/gmm/v/ak;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/p;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/v/ak;->a(Ljava/lang/Object;)Z

    goto :goto_1

    .line 101
    :cond_2
    return-object v1
.end method

.method public final b(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 1

    .prologue
    .line 50
    check-cast p1, Lcom/google/android/apps/gmm/map/t/p;

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/au;->a:Lcom/google/android/apps/gmm/v/ak;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/v/ak;->a(Ljava/lang/Object;)Z

    .line 52
    return-void
.end method
