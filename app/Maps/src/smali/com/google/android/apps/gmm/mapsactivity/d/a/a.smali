.class public final Lcom/google/android/apps/gmm/mapsactivity/d/a/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/mapsactivity/a/a;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput p1, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->a:I

    .line 32
    iput p2, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->b:I

    .line 33
    iput p3, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->c:I

    .line 34
    return-void
.end method

.method public static a(Lcom/google/maps/g/co;)Lcom/google/android/apps/gmm/mapsactivity/a/a;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 41
    iget v2, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    .line 42
    :cond_1
    iget v2, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    goto :goto_1

    .line 43
    :cond_3
    iget v2, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    goto :goto_2

    .line 44
    :cond_5
    iget v2, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-nez v2, :cond_7

    move v2, v0

    :goto_4
    if-nez v2, :cond_8

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    goto :goto_3

    :cond_7
    move v2, v1

    goto :goto_4

    .line 45
    :cond_8
    iget v2, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_5
    if-nez v2, :cond_a

    move v2, v0

    :goto_6
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_9
    move v2, v1

    goto :goto_5

    :cond_a
    move v2, v1

    goto :goto_6

    .line 46
    :cond_b
    iget v2, p0, Lcom/google/maps/g/co;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_7
    if-nez v2, :cond_d

    :goto_8
    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_c
    move v2, v1

    goto :goto_7

    :cond_d
    move v0, v1

    goto :goto_8

    .line 47
    :cond_e
    new-instance v0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;

    .line 48
    iget v1, p0, Lcom/google/maps/g/co;->b:I

    .line 49
    iget v2, p0, Lcom/google/maps/g/co;->c:I

    .line 50
    iget v3, p0, Lcom/google/maps/g/co;->d:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;-><init>(III)V

    return-object v0
.end method

.method public static a(Lcom/google/r/b/a/ay;)Lcom/google/android/apps/gmm/mapsactivity/a/a;
    .locals 5

    .prologue
    .line 58
    invoke-static {p0}, Lcom/google/android/apps/gmm/mapsactivity/d/a/b;->a(Lcom/google/r/b/a/ay;)Ljava/util/Calendar;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;-><init>(III)V

    return-object v1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->b:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->c:I

    return v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 18
    check-cast p1, Lcom/google/android/apps/gmm/mapsactivity/a/a;

    invoke-static {}, Lcom/google/b/c/ap;->a()Lcom/google/b/c/ap;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->a:I

    invoke-interface {p1}, Lcom/google/android/apps/gmm/mapsactivity/a/a;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/ap;->a(II)Lcom/google/b/c/ap;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->b:I

    invoke-interface {p1}, Lcom/google/android/apps/gmm/mapsactivity/a/a;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/ap;->a(II)Lcom/google/b/c/ap;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->c:I

    invoke-interface {p1}, Lcom/google/android/apps/gmm/mapsactivity/a/a;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/ap;->a(II)Lcom/google/b/c/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/ap;->b()I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 110
    instance-of v1, p1, Lcom/google/android/apps/gmm/mapsactivity/a/a;

    if-nez v1, :cond_1

    .line 116
    :cond_0
    :goto_0
    return v0

    .line 113
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/mapsactivity/a/a;

    .line 114
    iget v1, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->a:I

    invoke-interface {p1}, Lcom/google/android/apps/gmm/mapsactivity/a/a;->a()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 115
    iget v1, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->b:I

    invoke-interface {p1}, Lcom/google/android/apps/gmm/mapsactivity/a/a;->b()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 116
    iget v1, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->c:I

    invoke-interface {p1}, Lcom/google/android/apps/gmm/mapsactivity/a/a;->c()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 105
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 121
    iget v0, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x23

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "-"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
