.class public Lcom/google/android/apps/gmm/base/k/ap;
.super Landroid/support/v7/widget/bv;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Lcom/google/android/libraries/curvular/bd;

.field private final d:Lcom/google/android/libraries/curvular/c/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/gmm/base/k/ap;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/k/ap;->b:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/libraries/curvular/c/j;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/support/v7/widget/bv;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/k/ap;->c:Lcom/google/android/libraries/curvular/bd;

    .line 31
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/k/ap;->d:Lcom/google/android/libraries/curvular/c/j;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/support/v7/widget/ce;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 43
    if-ltz p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_2

    .line 44
    invoke-super {p0, p1}, Landroid/support/v7/widget/bv;->a(I)Landroid/support/v7/widget/ce;

    move-result-object v0

    .line 53
    :cond_0
    :goto_1
    return-object v0

    :cond_1
    move v1, v2

    .line 43
    goto :goto_0

    .line 47
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/ap;->d:Lcom/google/android/libraries/curvular/c/j;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/c/j;->c:Lcom/google/android/libraries/curvular/bm;

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/curvular/bm;->c(I)Ljava/lang/Class;

    move-result-object v1

    .line 48
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/k/ap;->c:Lcom/google/android/libraries/curvular/bd;

    invoke-virtual {v3, v1, v0, v2}, Lcom/google/android/libraries/curvular/bd;->b(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v1

    .line 49
    if-eqz v1, :cond_0

    .line 53
    iget-object v0, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/g;->cE:I

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    goto :goto_1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/apps/gmm/base/k/ap;->b:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public final a(Landroid/support/v7/widget/ce;)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 69
    invoke-virtual {p1}, Landroid/support/v7/widget/ce;->a()I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 70
    invoke-super {p0, p1}, Landroid/support/v7/widget/bv;->a(Landroid/support/v7/widget/ce;)V

    .line 82
    :goto_1
    return-void

    .line 69
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 80
    :cond_1
    iget-object v0, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/g;->cE:I

    invoke-virtual {v0, v1, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/ap;->c:Lcom/google/android/libraries/curvular/bd;

    iget-object v1, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/bd;->a(Landroid/view/View;)V

    goto :goto_1
.end method
