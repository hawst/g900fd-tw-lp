.class public Lcom/google/android/apps/gmm/base/layout/y;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field final b:Landroid/graphics/Rect;

.field c:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;I)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 5

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, p1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, p2

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, p1

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v4, p2

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 53
    return-void
.end method

.method public final a(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    .line 42
    invoke-static {p1, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 43
    invoke-static {p3, p4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 41
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 45
    return-void
.end method
