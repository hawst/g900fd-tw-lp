.class public Lcom/google/android/apps/gmm/map/t/as;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:I

.field final c:I

.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const v1, 0x7fffffff

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/map/t/as;

    invoke-direct {v0, v1, v1, v1}, Lcom/google/android/apps/gmm/map/t/as;-><init>(III)V

    return-void
.end method

.method public constructor <init>(III)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/apps/gmm/map/t/as;-><init>(IIII)V

    .line 47
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput p1, p0, Lcom/google/android/apps/gmm/map/t/as;->a:I

    .line 51
    iput p2, p0, Lcom/google/android/apps/gmm/map/t/as;->b:I

    .line 52
    iput p3, p0, Lcom/google/android/apps/gmm/map/t/as;->c:I

    .line 53
    iput p4, p0, Lcom/google/android/apps/gmm/map/t/as;->d:I

    .line 54
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/m;)V
    .locals 3

    .prologue
    .line 42
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/m;->b()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/m;->c()I

    move-result v1

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/m;->d()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/as;-><init>(III)V

    .line 43
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/internal/c/m;)Z
    .locals 2

    .prologue
    .line 69
    invoke-interface {p0}, Lcom/google/android/apps/gmm/map/internal/c/m;->b()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/m;->b()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 70
    invoke-interface {p0}, Lcom/google/android/apps/gmm/map/internal/c/m;->c()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/m;->c()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/gmm/map/t/as;
    .locals 4

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/map/t/as;

    iget v1, p0, Lcom/google/android/apps/gmm/map/t/as;->a:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/t/as;->b:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/t/as;->d:I

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/google/android/apps/gmm/map/t/as;-><init>(IIII)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 75
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/as;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/t/as;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/t/as;->d:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/t/as;->c:I

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x2f

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
