.class public Lcom/google/android/apps/gmm/terms/b;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/gmm/terms/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 109
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 110
    const-string v2, "currentKillSwitchContentUrl"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 112
    if-eqz v1, :cond_1

    .line 113
    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    instance-of v2, v2, Lcom/google/android/apps/gmm/terms/KillSwitchFragment;

    if-nez v2, :cond_0

    .line 115
    new-instance v2, Lcom/google/android/apps/gmm/terms/c;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/apps/gmm/terms/c;-><init>(Lcom/google/android/apps/gmm/terms/b;Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/String;)V

    .line 126
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/apps/gmm/terms/KillSwitchFragment;

    if-eqz v1, :cond_0

    .line 132
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    goto :goto_0
.end method


# virtual methods
.method public final Y_()V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 138
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    const-string v3, "gmmActivityPreference"

    .line 141
    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "isFirstStart"

    .line 142
    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 143
    if-eqz v0, :cond_0

    .line 144
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    const-string v4, "gmmActivityPreference"

    .line 145
    invoke-virtual {v3, v4, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 146
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "isFirstStart"

    .line 147
    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 150
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "killSwitchLastFetchedTime"

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v5, "currentKillSwitchContentUrl"

    const/4 v8, 0x0

    invoke-interface {v4, v5, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    sub-long/2addr v4, v6

    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    :cond_1
    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    :cond_2
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->b(Lcom/google/android/apps/gmm/shared/net/ad;)V

    .line 152
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 153
    return-void

    :cond_3
    move v0, v1

    .line 150
    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/gmm/terms/b;->c()V

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/shared/net/a/g;)V
    .locals 6
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 52
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 54
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->b()Lcom/google/r/b/a/wx;

    move-result-object v3

    .line 56
    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 57
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 59
    iget v0, v3, Lcom/google/r/b/a/wx;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v5, 0x2

    if-ne v0, v5, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    .line 60
    iget-object v0, v3, Lcom/google/r/b/a/wx;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    .line 61
    :goto_1
    const-string v1, "currentKillSwitchContentUrl"

    invoke-interface {v4, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 64
    const-string v1, "killSwitchLastFetchedTime"

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    invoke-interface {v4, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 65
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 67
    invoke-direct {p0}, Lcom/google/android/apps/gmm/terms/b;->c()V

    .line 68
    return-void

    :cond_0
    move v0, v1

    .line 59
    goto :goto_0

    .line 60
    :cond_1
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v1, v3, Lcom/google/r/b/a/wx;->c:Ljava/lang/Object;

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->b()V

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 159
    return-void
.end method
