.class Lcom/google/android/apps/gmm/map/internal/d/ad;
.super Lcom/google/android/apps/gmm/shared/net/i;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/internal/d/af;

.field final b:Lcom/google/android/apps/gmm/map/internal/d/af;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/b/cc;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field final e:Lcom/google/android/apps/gmm/map/b/a/ai;

.field private final f:Lcom/google/e/a/a/a/b;

.field private final g:Lcom/google/android/apps/gmm/map/internal/d/ae;

.field private final h:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/af;Lcom/google/android/apps/gmm/map/internal/d/af;Lcom/google/android/apps/gmm/map/b/a/ai;Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/d/ae;ZLcom/google/e/a/a/a/b;)V
    .locals 1
    .param p7    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/d/af;",
            "Lcom/google/android/apps/gmm/map/internal/d/af;",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Lcom/google/android/apps/gmm/map/internal/d/ae;",
            "Z",
            "Lcom/google/e/a/a/a/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    sget-object v0, Lcom/google/r/b/a/el;->ck:Lcom/google/r/b/a/el;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/shared/net/i;-><init>(Lcom/google/r/b/a/el;)V

    .line 64
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->a:Lcom/google/android/apps/gmm/map/internal/d/af;

    .line 65
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->b:Lcom/google/android/apps/gmm/map/internal/d/af;

    .line 66
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->d:Ljava/util/List;

    .line 67
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->e:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 68
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->g:Lcom/google/android/apps/gmm/map/internal/d/ae;

    .line 69
    iput-boolean p6, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->h:Z

    .line 70
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->f:Lcom/google/e/a/a/a/b;

    .line 71
    return-void
.end method


# virtual methods
.method protected final a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 10

    .prologue
    const/16 v9, 0xa

    const/4 v8, 0x2

    const/16 v5, 0x15

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 115
    sget-object v0, Lcom/google/r/b/a/b/bg;->j:Lcom/google/e/a/a/a/d;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/d;Ljava/io/DataInput;)Lcom/google/e/a/a/a/b;

    move-result-object v4

    .line 116
    const/16 v0, 0x1a

    invoke-virtual {v4, v3, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v3, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v1, v6

    invoke-virtual {v0, v8, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    if-eqz v0, :cond_0

    const-string v1, "PerTileDataRequest"

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x28

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Received tile response code: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->j:Lcom/google/android/apps/gmm/shared/net/k;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v4, v8, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->o:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_0

    :cond_1
    iget-object v0, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v9}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v5

    if-ltz v5, :cond_2

    move v0, v3

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->c:Ljava/util/List;

    :goto_2
    if-ge v2, v5, :cond_5

    const/16 v0, 0x1a

    invoke-virtual {v4, v9, v2, v0}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->c:Ljava/util/List;

    invoke-static {}, Lcom/google/maps/b/cc;->d()Lcom/google/maps/b/cc;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_4

    :goto_3
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Ljava/io/DataOutput;)V
    .locals 12

    .prologue
    const/4 v9, 0x2

    const-wide v10, 0x412e848000000000L    # 1000000.0

    const/4 v8, 0x1

    .line 75
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/bg;->c:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/4 v1, 0x5

    int-to-long v2, v8

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->h:Z

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/a;->a(Lcom/google/e/a/a/a/b;Z)V

    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/r/b/a/b/bg;->h:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iget-object v2, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v8, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/16 v0, 0x1e

    int-to-long v2, v0

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v2, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v9, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->f:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->f:Lcom/google/e/a/a/a/b;

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_0
    new-instance v2, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/bg;->i:Lcom/google/e/a/a/a/d;

    invoke-direct {v2, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->b:Lcom/google/android/apps/gmm/map/internal/d/af;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/af;->a:Lcom/google/android/apps/gmm/map/b/a/bc;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->b:Lcom/google/android/apps/gmm/map/internal/d/af;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/d/af;->b:I

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/bc;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/p;->a(Lcom/google/android/apps/gmm/map/b/a/r;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/b/a/u;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/p;->b(Lcom/google/android/apps/gmm/map/b/a/r;)D

    move-result-wide v6

    mul-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v5, v6

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/p;->c(Lcom/google/android/apps/gmm/map/b/a/r;)D

    move-result-wide v6

    mul-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v0, v6

    invoke-static {v4, v5, v0, v3}, Lcom/google/android/apps/gmm/map/b/a/v;->a(Lcom/google/android/apps/gmm/map/b/a/u;III)Lcom/google/e/a/a/a/b;

    move-result-object v0

    iget-object v3, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v8, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v3, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v9, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/16 v0, 0xa

    invoke-virtual {v1, v0, v2}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 76
    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Ljava/io/DataOutput;Lcom/google/e/a/a/a/b;)V

    .line 77
    return-void
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 144
    if-nez p1, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->g:Lcom/google/android/apps/gmm/map/internal/d/ae;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/internal/d/ae;->a(Lcom/google/android/apps/gmm/map/internal/d/ad;)V

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ad;->g:Lcom/google/android/apps/gmm/map/internal/d/ae;

    invoke-interface {v0, p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/ae;->a(Lcom/google/android/apps/gmm/map/internal/d/ad;Lcom/google/android/apps/gmm/shared/net/k;)V

    goto :goto_0
.end method
