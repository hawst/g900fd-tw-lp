.class public Lcom/google/android/apps/gmm/map/internal/vector/gl/j;
.super Lcom/google/android/apps/gmm/v/av;
.source "PG"


# instance fields
.field public a:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/aw;IIII)V
    .locals 2

    .prologue
    .line 132
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/gmm/v/av;-><init>(Lcom/google/android/apps/gmm/v/aw;IIII)V

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    .line 133
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    mul-int/2addr v0, p2

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    .line 134
    if-eqz p3, :cond_0

    .line 135
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    shl-int/lit8 v1, p3, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    .line 137
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;I[SII)V
    .locals 2

    .prologue
    .line 122
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/gmm/v/av;-><init>(Ljava/nio/ByteBuffer;I[SII)V

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    .line 123
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    mul-int/2addr v0, p2

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    .line 124
    if-eqz p3, :cond_0

    .line 125
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    array-length v1, p3

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    .line 128
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 141
    invoke-super {p0}, Lcom/google/android/apps/gmm/v/av;->a()V

    .line 142
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x880

    if-eqz v0, :cond_0

    .line 143
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_4

    .line 144
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->m:I

    .line 145
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    .line 152
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x1100

    if-eqz v0, :cond_1

    .line 153
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_5

    .line 154
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->n:I

    .line 155
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    .line 162
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x2200

    if-eqz v0, :cond_2

    .line 163
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_6

    .line 164
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->o:I

    .line 165
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    .line 172
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x4400

    if-eqz v0, :cond_3

    .line 173
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_7

    .line 174
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->p:I

    .line 175
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    .line 181
    :cond_3
    :goto_3
    return-void

    .line 147
    :cond_4
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->m:I

    .line 148
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    goto :goto_0

    .line 157
    :cond_5
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->n:I

    .line 158
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    goto :goto_1

    .line 167
    :cond_6
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->o:I

    .line 168
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    goto :goto_2

    .line 177
    :cond_7
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->p:I

    .line 178
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    goto :goto_3
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/co;)V
    .locals 12

    .prologue
    const/4 v11, 0x6

    const/4 v0, 0x5

    const/4 v1, 0x4

    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 185
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/v/av;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/co;)V

    .line 186
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v2, v2, 0x880

    if-eqz v2, :cond_0

    .line 188
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_4

    .line 189
    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 190
    const/16 v2, 0x1401

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iget v5, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->m:I

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 199
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x1100

    if-eqz v0, :cond_1

    .line 206
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_5

    .line 207
    invoke-static {v11}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 208
    const/16 v2, 0x1401

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iget v5, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->n:I

    move v0, v11

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 217
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x2200

    if-eqz v0, :cond_2

    .line 224
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_6

    .line 225
    const/4 v0, 0x7

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 226
    const/4 v0, 0x7

    const/16 v2, 0x1401

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iget v5, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->o:I

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 235
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x4400

    if-eqz v0, :cond_3

    .line 242
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_7

    .line 243
    const/16 v0, 0x8

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 244
    const/16 v0, 0x8

    const/16 v2, 0x1401

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iget v5, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->p:I

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 253
    :cond_3
    :goto_3
    return-void

    .line 193
    :cond_4
    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 197
    const/16 v6, 0x1406

    iget v8, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iget v9, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->m:I

    move v4, v0

    move v5, v10

    move v7, v3

    invoke-static/range {v4 .. v9}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    goto :goto_0

    .line 211
    :cond_5
    invoke-static {v11}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 215
    const/16 v6, 0x1406

    iget v8, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iget v9, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->n:I

    move v4, v11

    move v5, v10

    move v7, v3

    invoke-static/range {v4 .. v9}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    goto :goto_1

    .line 229
    :cond_6
    const/4 v0, 0x7

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 233
    const/4 v4, 0x7

    const/16 v6, 0x1406

    iget v8, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iget v9, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->o:I

    move v5, v10

    move v7, v3

    invoke-static/range {v4 .. v9}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    goto :goto_2

    .line 247
    :cond_7
    const/16 v0, 0x8

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 251
    const/16 v0, 0x8

    const/16 v2, 0x1406

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->f:I

    iget v5, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->p:I

    move v1, v10

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    goto :goto_3
.end method

.method public final b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/co;)V
    .locals 1

    .prologue
    .line 262
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/v/av;->b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/co;)V

    .line 264
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x880

    if-eqz v0, :cond_0

    .line 265
    const/4 v0, 0x5

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 268
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x1100

    if-eqz v0, :cond_1

    .line 269
    const/4 v0, 0x6

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 272
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x2200

    if-eqz v0, :cond_2

    .line 273
    const/4 v0, 0x7

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 276
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->d:I

    and-int/lit16 v0, v0, 0x4400

    if-eqz v0, :cond_3

    .line 277
    const/16 v0, 0x8

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 279
    :cond_3
    return-void
.end method

.method protected final b()Z
    .locals 1

    .prologue
    .line 283
    invoke-super {p0}, Lcom/google/android/apps/gmm/v/av;->b()Z

    move-result v0

    return v0
.end method
