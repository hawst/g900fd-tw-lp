.class abstract Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 130
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 127
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->a:Ljava/util/Map;

    .line 131
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 191
    instance-of v0, p1, Landroid/view/ViewStub;

    if-eqz v0, :cond_0

    .line 205
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->a:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/layout/y;

    .line 197
    if-nez v0, :cond_1

    .line 198
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit16 v3, v3, 0xc3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Child "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found in the ViewWrappers (was not created via a newViewWrapper call). If the View is a stub, ensure the newViewWrapper call references the "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "android:inflatedId, not its stub android:id."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :cond_1
    iput-object p1, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    goto :goto_0
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 153
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 154
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->a(Landroid/view/View;)V

    .line 155
    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 141
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->a(Landroid/view/View;)V

    .line 143
    return-void
.end method

.method public addView(Landroid/view/View;II)V
    .locals 0

    .prologue
    .line 159
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 160
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->a(Landroid/view/View;)V

    .line 161
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 135
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 136
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->a(Landroid/view/View;)V

    .line 137
    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 147
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 148
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->a(Landroid/view/View;)V

    .line 149
    return-void
.end method

.method protected addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->a(Landroid/view/View;)V

    .line 166
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    return v0
.end method

.method protected addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z
    .locals 1

    .prologue
    .line 172
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->a(Landroid/view/View;)V

    .line 173
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    move-result v0

    return v0
.end method

.method protected final b(I)Lcom/google/android/apps/gmm/base/layout/y;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)",
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 184
    new-instance v0, Lcom/google/android/apps/gmm/base/layout/y;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/base/layout/y;-><init>(Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;I)V

    .line 185
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/layout/y;

    .line 210
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/base/layout/y;->c:Z

    if-eqz v2, :cond_0

    .line 211
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 214
    :cond_1
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/layout/y;

    .line 219
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 220
    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/gmm/base/layout/y;->a(II)V

    goto :goto_0

    .line 223
    :cond_1
    return-void
.end method
