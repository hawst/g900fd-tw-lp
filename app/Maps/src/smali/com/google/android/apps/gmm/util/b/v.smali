.class public Lcom/google/android/apps/gmm/util/b/v;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/util/b/u;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/util/b/ag;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/r;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/o/h/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/android/apps/gmm/util/b/m;)V
    .locals 7

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p2, p0, Lcom/google/android/apps/gmm/util/b/v;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 38
    iget-object v1, p3, Lcom/google/android/apps/gmm/util/b/m;->a:Ljava/util/List;

    .line 39
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ltz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/b/v;->b:Ljava/util/List;

    .line 40
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/o/h/a/iv;

    .line 42
    invoke-static {p1, p2, v5}, Lcom/google/android/apps/gmm/util/b/af;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/iv;)Lcom/google/b/a/an;

    move-result-object v2

    .line 43
    if-eqz v2, :cond_2

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/util/b/r;

    iget-object v1, v2, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Class;

    iget-object v2, v2, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    .line 54
    invoke-static {v5}, Lcom/google/android/apps/gmm/util/b/af;->a(Lcom/google/o/h/a/iv;)Z

    move-result v3

    .line 55
    invoke-static {v5}, Lcom/google/android/apps/gmm/util/b/af;->b(Lcom/google/o/h/a/iv;)Z

    move-result v4

    .line 56
    invoke-static {v5}, Lcom/google/android/apps/gmm/util/b/af;->c(Lcom/google/o/h/a/iv;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/util/b/r;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;ZZZ)V

    .line 57
    iget-object v1, p0, Lcom/google/android/apps/gmm/util/b/v;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 60
    :cond_3
    iget-object v0, p3, Lcom/google/android/apps/gmm/util/b/m;->b:Lcom/google/o/h/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/b/v;->c:Lcom/google/o/h/a/a;

    .line 61
    iget-object v0, p3, Lcom/google/android/apps/gmm/util/b/m;->c:Lcom/google/o/h/a/a;

    .line 70
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/v;->b:Ljava/util/List;

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/v;->c:Lcom/google/o/h/a/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/v;->c:Lcom/google/o/h/a/a;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/v;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b/ag;->c:Lcom/google/android/apps/gmm/util/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/util/b/v;->c:Lcom/google/o/h/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/util/b/v;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 86
    iget-object v2, v2, Lcom/google/android/apps/gmm/util/b/ag;->a:Lcom/google/o/h/a/br;

    iget-object v3, p0, Lcom/google/android/apps/gmm/util/b/v;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/util/b/b;->a(Lcom/google/o/h/a/br;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v2

    .line 85
    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/util/b/a;->a(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;)V

    .line 88
    :cond_0
    return-object v4

    .line 84
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
