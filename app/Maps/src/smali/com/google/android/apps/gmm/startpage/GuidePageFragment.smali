.class public Lcom/google/android/apps/gmm/startpage/GuidePageFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static final c:I

.field static final d:I

.field static final e:Z

.field private static final f:Ljava/lang/String;


# instance fields
.field private g:Lcom/google/android/apps/gmm/startpage/m;

.field private final m:Lcom/google/android/apps/gmm/startpage/d/d;

.field private n:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->f:Ljava/lang/String;

    .line 54
    const/16 v0, 0x96

    sput v0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->c:I

    .line 56
    const/16 v0, 0xf0

    sput v0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->d:I

    .line 58
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->e:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/startpage/d/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->m:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 72
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/startpage/d/d;)Lcom/google/android/apps/gmm/startpage/GuidePageFragment;
    .locals 4

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;-><init>()V

    .line 78
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 79
    const-string v2, "restoreCameraPositionOnResume"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 80
    const-string v2, "argkey-odelay-state"

    invoke-virtual {p0, v1, v2, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 81
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->setArguments(Landroid/os/Bundle;)V

    .line 83
    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/startpage/a/b;Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/o/h/a/kh;Lcom/google/android/apps/gmm/map/b/a/r;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/s/a;)Lcom/google/android/apps/gmm/startpage/d/d;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 90
    new-instance v4, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/startpage/d/d;-><init>()V

    .line 92
    invoke-virtual {v4, p1}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/iamhere/c/o;)V

    .line 93
    iget v0, p2, Lcom/google/o/h/a/kh;->e:I

    invoke-static {v0}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/dq;)V

    .line 96
    iget v0, p2, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_3

    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/apps/gmm/startpage/d/e;

    .line 97
    invoke-virtual {p2}, Lcom/google/o/h/a/kh;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/e;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    .line 100
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/a/b;->a:Lcom/google/o/h/a/hb;

    .line 102
    iget-object v5, p0, Lcom/google/android/apps/gmm/startpage/a/b;->a:Lcom/google/o/h/a/hb;

    if-eqz v5, :cond_0

    .line 103
    iget-object v5, p0, Lcom/google/android/apps/gmm/startpage/a/b;->c:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/gmm/startpage/d/e;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 105
    :cond_0
    invoke-static {}, Lcom/google/o/h/a/hb;->newBuilder()Lcom/google/o/h/a/hd;

    move-result-object v0

    .line 107
    invoke-static {}, Lcom/google/o/h/a/dt;->newBuilder()Lcom/google/o/h/a/dv;

    move-result-object v5

    sget v6, Lcom/google/android/apps/gmm/d;->v:I

    .line 108
    invoke-virtual {p4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 107
    iget v7, v5, Lcom/google/o/h/a/dv;->a:I

    or-int/lit8 v7, v7, 0x2

    iput v7, v5, Lcom/google/o/h/a/dv;->a:I

    iput v6, v5, Lcom/google/o/h/a/dv;->c:I

    .line 106
    iget-object v6, v0, Lcom/google/o/h/a/hd;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/o/h/a/dv;->g()Lcom/google/n/t;

    move-result-object v5

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v5, 0x0

    iput-object v5, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    iget v5, v0, Lcom/google/o/h/a/hd;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v0, Lcom/google/o/h/a/hd;->a:I

    .line 109
    invoke-virtual {v0}, Lcom/google/o/h/a/hd;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hb;

    .line 111
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/gmm/startpage/a/b;->b:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/hb;Ljava/lang/String;)V

    .line 112
    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Lcom/google/android/apps/gmm/startpage/d/e;)V

    .line 113
    invoke-virtual {v4, p3}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/r;)V

    .line 114
    invoke-virtual {v4, p5}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/map/s/a;)V

    .line 116
    iget v0, p2, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_5

    move v0, v2

    :goto_2
    if-eqz v0, :cond_2

    .line 117
    iget v0, p2, Lcom/google/o/h/a/kh;->h:I

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(I)V

    .line 120
    :cond_2
    return-object v4

    :cond_3
    move v0, v3

    .line 96
    goto :goto_0

    .line 97
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    move-object v1, v0

    goto :goto_1

    :cond_5
    move v0, v3

    .line 116
    goto :goto_2
.end method

.method private a(Landroid/os/Bundle;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 124
    if-nez p1, :cond_0

    move v0, v1

    .line 135
    :goto_0
    return v0

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v2, "argkey-odelay-state"

    invoke-virtual {v0, p1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 128
    instance-of v2, v0, Lcom/google/android/apps/gmm/startpage/d/d;

    if-nez v2, :cond_2

    .line 129
    if-eqz v0, :cond_1

    .line 130
    sget-object v2, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x21

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unknown odelayState class comes: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    move v0, v1

    .line 132
    goto :goto_0

    .line 134
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->m:Lcom/google/android/apps/gmm/startpage/d/d;

    check-cast v0, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/startpage/d/d;)V

    .line 135
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected final b()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 261
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>(Lcom/google/android/apps/gmm/base/views/c/g;)V

    const/4 v0, 0x0

    .line 262
    iput-boolean v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->j:Z

    .line 263
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    return-object v0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 255
    sget-object v0, Lcom/google/b/f/t;->aW:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 140
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onCreate(Landroid/os/Bundle;)V

    .line 141
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->f:Ljava/lang/String;

    .line 144
    :cond_1
    new-instance v3, Lcom/google/android/apps/gmm/startpage/f/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->m:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/startpage/f/b;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/d/d;)V

    .line 145
    new-instance v0, Lcom/google/android/apps/gmm/startpage/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->m:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 146
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v4, 0x0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/m;-><init>(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/f/i;Lcom/google/android/apps/gmm/cardui/a/d;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->g:Lcom/google/android/apps/gmm/startpage/m;

    .line 148
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 155
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/startpage/c/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->g:Lcom/google/android/apps/gmm/startpage/m;

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/startpage/m;->a(Lcom/google/android/libraries/curvular/ag;)V

    .line 158
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 159
    sget-object v1, Lcom/google/android/apps/gmm/startpage/c/a;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v1

    .line 161
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->b:Lcom/google/android/apps/gmm/base/l/ao;

    sget v3, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->c:I

    sget v4, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->d:I

    sget-boolean v5, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->e:Z

    .line 160
    invoke-static {v2, v1, v3, v4, v5}, Lcom/google/android/apps/gmm/base/i/g;->a(Lcom/google/android/apps/gmm/base/l/a/ab;Landroid/view/View;IIZ)V

    .line 164
    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->n:Landroid/util/SparseArray;

    .line 220
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onDestroyView()V

    .line 221
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 243
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onPause()V

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->g:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/m;->d()V

    .line 245
    return-void
.end method

.method public onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 225
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->g:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/m;->b()V

    .line 227
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 228
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 229
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v6, v1, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    const/4 v1, 0x0

    .line 230
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 231
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->a(Landroid/view/View;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 232
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->g:Lcom/google/android/apps/gmm/startpage/m;

    .line 234
    iget-object v1, v1, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    .line 233
    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/p;->a(Lcom/google/android/apps/gmm/z/a;)Lcom/google/android/apps/gmm/base/activities/y;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 235
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 237
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->getView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->n:Landroid/util/SparseArray;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->b:Lcom/google/android/apps/gmm/base/l/ao;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v3

    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->remove(I)V

    invoke-virtual {v0, v4}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/startpage/d;

    invoke-direct {v4, v0, v1, v2}, Lcom/google/android/apps/gmm/startpage/d;-><init>(Landroid/view/View;Landroid/util/SparseArray;Lcom/google/android/apps/gmm/base/l/a/ab;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 238
    :cond_0
    iput-object v6, p0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->n:Landroid/util/SparseArray;

    .line 239
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 249
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "argkey-odelay-state"

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->m:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 251
    return-void
.end method

.method public run()V
    .locals 3

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->m:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->w()Lcom/google/o/h/a/hb;

    move-result-object v0

    .line 270
    if-nez v0, :cond_0

    .line 279
    :goto_0
    return-void

    .line 275
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->b:Lcom/google/android/apps/gmm/base/l/ao;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/l/a/ab;->c()Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>(Lcom/google/android/apps/gmm/base/views/c/g;)V

    const/4 v1, 0x0

    .line 276
    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/views/c/i;->j:Z

    .line 277
    invoke-virtual {v0}, Lcom/google/o/h/a/hb;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    .line 278
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    .line 274
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->a(Lcom/google/android/apps/gmm/base/views/c/g;)V

    goto :goto_0
.end method
