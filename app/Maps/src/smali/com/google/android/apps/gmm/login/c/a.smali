.class public Lcom/google/android/apps/gmm/login/c/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/login/d/b;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/activities/c;

.field final b:Lcom/google/android/apps/gmm/login/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final c:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/login/a/b;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/google/android/apps/gmm/login/c/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 75
    iput-object p2, p0, Lcom/google/android/apps/gmm/login/c/a;->b:Lcom/google/android/apps/gmm/login/a/b;

    .line 76
    iput-object p3, p0, Lcom/google/android/apps/gmm/login/c/a;->c:Ljava/lang/Runnable;

    .line 77
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/login/c/a;)Lcom/google/android/apps/gmm/u/a/a;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/c/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/c/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->B()Lcom/google/android/apps/gmm/u/a/a;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/login/d/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/c/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->k()Ljava/util/List;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_0

    .line 84
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 85
    new-instance v3, Lcom/google/android/apps/gmm/login/c/c;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/gmm/login/c/c;-><init>(Lcom/google/android/apps/gmm/login/c/a;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/c/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v2, Lcom/google/android/apps/gmm/login/c/b;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/gmm/login/c/b;-><init>(Lcom/google/android/apps/gmm/login/c/a;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 89
    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/c/a;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 101
    const/4 v0, 0x0

    return-object v0
.end method
