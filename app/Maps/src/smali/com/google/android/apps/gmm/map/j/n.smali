.class public final enum Lcom/google/android/apps/gmm/map/j/n;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/j/s;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/j/n;",
        ">;",
        "Lcom/google/android/apps/gmm/map/j/s;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/j/n;

.field private static final synthetic b:[Lcom/google/android/apps/gmm/map/j/n;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/map/j/n;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/j/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/j/n;->a:Lcom/google/android/apps/gmm/map/j/n;

    .line 27
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/j/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/j/n;->a:Lcom/google/android/apps/gmm/map/j/n;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/gmm/map/j/n;->b:[Lcom/google/android/apps/gmm/map/j/n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/j/n;
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/map/j/n;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/j/n;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/j/n;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/apps/gmm/map/j/n;->b:[Lcom/google/android/apps/gmm/map/j/n;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/j/n;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/j/n;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/j/r;)Lcom/google/android/apps/gmm/map/j/q;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/map/j/m;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/j/m;-><init>(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/j/r;)V

    return-object v0
.end method
