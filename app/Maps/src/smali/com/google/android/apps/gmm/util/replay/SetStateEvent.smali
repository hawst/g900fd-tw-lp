.class public Lcom/google/android/apps/gmm/util/replay/SetStateEvent;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/util/replay/c;
    a = "set-state"
.end annotation


# instance fields
.field private final crash:Z

.field private final experimentIds:Ljava/lang/String;

.field private final isOffline:Ljava/lang/Boolean;

.field private final updateTraffic:Z


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 3
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "is-offline"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "experiment-ids"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "update-traffic"
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "crash"
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/android/apps/gmm/util/replay/SetStateEvent;->isOffline:Ljava/lang/Boolean;

    .line 70
    iput-object p2, p0, Lcom/google/android/apps/gmm/util/replay/SetStateEvent;->experimentIds:Ljava/lang/String;

    .line 71
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne p3, v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/util/replay/SetStateEvent;->updateTraffic:Z

    .line 72
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne p4, v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/util/replay/SetStateEvent;->crash:Z

    .line 73
    return-void

    :cond_0
    move v0, v2

    .line 71
    goto :goto_0

    :cond_1
    move v1, v2

    .line 72
    goto :goto_1
.end method


# virtual methods
.method public getCrash()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "crash"
    .end annotation

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/util/replay/SetStateEvent;->crash:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getExperimentIds()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "experiment-ids"
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/replay/SetStateEvent;->experimentIds:Ljava/lang/String;

    return-object v0
.end method

.method public getIsOffline()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "is-offline"
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/replay/SetStateEvent;->isOffline:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getUpdateTraffic()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "update-traffic"
    .end annotation

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/util/replay/SetStateEvent;->updateTraffic:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public hasExperimentIds()Z
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/f;
        a = "experiment-ids"
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/replay/SetStateEvent;->experimentIds:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsOffline()Z
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/f;
        a = "is-offline"
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/replay/SetStateEvent;->isOffline:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
