.class Lcom/google/android/apps/gmm/startpage/c;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:J

.field final b:Lcom/google/android/apps/gmm/map/b/a/r;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/map/b/a/l;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final d:F

.field final e:Lcom/google/o/b/a/v;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method constructor <init>(JLcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/b/a/l;FLcom/google/o/b/a/v;)V
    .locals 1
    .param p3    # Lcom/google/android/apps/gmm/map/b/a/r;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/map/b/a/l;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/o/b/a/v;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-wide p1, p0, Lcom/google/android/apps/gmm/startpage/c;->a:J

    .line 57
    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/c;->b:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 58
    iput-object p4, p0, Lcom/google/android/apps/gmm/startpage/c;->c:Lcom/google/android/apps/gmm/map/b/a/l;

    .line 59
    iput p5, p0, Lcom/google/android/apps/gmm/startpage/c;->d:F

    .line 60
    iput-object p6, p0, Lcom/google/android/apps/gmm/startpage/c;->e:Lcom/google/o/b/a/v;

    .line 61
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 65
    iget-wide v2, p0, Lcom/google/android/apps/gmm/startpage/c;->a:J

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/c;->b:Lcom/google/android/apps/gmm/map/b/a/r;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/c;->c:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/startpage/c;->e:Lcom/google/o/b/a/v;

    .line 67
    if-nez v5, :cond_0

    const-string v0, "null"

    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x65

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "<currentTimeInRelativeMillis="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",currentViewport="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",focusedIndoorId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",currentLocation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget v0, v5, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v6, 0x10

    if-ne v0, v6, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    const-string v0, "no-latlng"

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iget-object v0, v5, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/l;->d()Lcom/google/o/b/a/l;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/o/b/a/l;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
