.class public Lcom/google/android/apps/gmm/suggest/d/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:J

.field private e:J

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/suggest/d/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/d/d;->a()V

    .line 68
    return-void
.end method

.method private declared-synchronized b()V
    .locals 1

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    monitor-exit p0

    return-void

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 96
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/suggest/d/d;->c:Z

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    .line 97
    :cond_1
    :try_start_1
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/suggest/d/d;->b:Z

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 98
    :cond_2
    iget-wide v2, p0, Lcom/google/android/apps/gmm/suggest/d/d;->d:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    :goto_1
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 99
    :cond_4
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->e:J

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 101
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/suggest/d/a;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/suggest/d/e;
    .locals 9
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 144
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->c:Z

    if-eqz v0, :cond_0

    .line 146
    const-string v0, "SearchboxSessionLogger"

    const-string v1, "Search log is committed more than once"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    const/4 v1, 0x0

    .line 157
    :goto_0
    monitor-exit p0

    return-object v1

    .line 149
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/suggest/d/d;->b()V

    .line 150
    invoke-direct {p0, p3}, Lcom/google/android/apps/gmm/suggest/d/d;->b(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 151
    new-instance v1, Lcom/google/android/apps/gmm/suggest/d/e;

    iget-wide v2, p0, Lcom/google/android/apps/gmm/suggest/d/d;->d:J

    iget-wide v4, p0, Lcom/google/android/apps/gmm/suggest/d/d;->e:J

    iget-object v7, p0, Lcom/google/android/apps/gmm/suggest/d/d;->f:Ljava/util/List;

    move-object v6, p1

    move-object v8, p3

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/gmm/suggest/d/e;-><init>(JJLcom/google/android/apps/gmm/suggest/d/a;Ljava/util/List;Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 153
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->a:Z

    if-eqz v0, :cond_1

    .line 154
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/gmm/z/b/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/z/a/b;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    .line 156
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->c:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 74
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->a:Z

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->b:Z

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->c:Z

    .line 77
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->d:J

    .line 78
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->e:J

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->f:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    monitor-exit p0

    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 88
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/suggest/d/d;->c:Z

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    .line 89
    :cond_1
    :try_start_1
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/suggest/d/d;->b:Z

    if-nez v2, :cond_2

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 90
    :cond_3
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->d:J

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/suggest/d/g;)V
    .locals 1

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->b:Z

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/d;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    :cond_0
    monitor-exit p0

    return-void

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/suggest/d/d;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    monitor-exit p0

    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
