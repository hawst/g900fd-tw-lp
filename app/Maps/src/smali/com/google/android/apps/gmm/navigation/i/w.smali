.class public final Lcom/google/android/apps/gmm/navigation/i/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/i/v;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Lcom/google/android/apps/gmm/map/c/a;

.field final synthetic d:Lcom/google/android/apps/gmm/map/internal/d/as;

.field final synthetic e:Lcom/google/android/apps/gmm/map/r/b/a;

.field final synthetic f:Lcom/google/android/apps/gmm/map/r/a/w;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/i/v;Landroid/content/Context;Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/internal/d/as;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/r/a/w;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/i/w;->a:Lcom/google/android/apps/gmm/navigation/i/v;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/i/w;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/i/w;->c:Lcom/google/android/apps/gmm/map/c/a;

    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/i/w;->d:Lcom/google/android/apps/gmm/map/internal/d/as;

    iput-object p5, p0, Lcom/google/android/apps/gmm/navigation/i/w;->e:Lcom/google/android/apps/gmm/map/r/b/a;

    iput-object p6, p0, Lcom/google/android/apps/gmm/navigation/i/w;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 22

    .prologue
    .line 82
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/navigation/i/w;->a:Lcom/google/android/apps/gmm/navigation/i/v;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/w;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/w;->c:Lcom/google/android/apps/gmm/map/c/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/i/w;->d:Lcom/google/android/apps/gmm/map/internal/d/as;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/gmm/navigation/i/w;->e:Lcom/google/android/apps/gmm/map/r/b/a;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/navigation/i/w;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    new-instance v16, Lcom/google/android/apps/gmm/map/u/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v0, v16

    invoke-direct {v0, v5, v4, v6}, Lcom/google/android/apps/gmm/map/u/a;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/map/internal/d/as;Z)V

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v17

    new-instance v18, Lcom/google/android/apps/gmm/navigation/i/f;

    iget-object v3, v13, Lcom/google/android/apps/gmm/navigation/i/v;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/navigation/i/f;-><init>(Lcom/google/android/apps/gmm/map/u/a/b;Lcom/google/android/apps/gmm/shared/net/a/n;)V

    invoke-virtual {v14}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v14}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v6

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/f;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v3, v3, Lcom/google/r/b/a/qz;->g:I

    int-to-double v8, v3

    mul-double v10, v6, v8

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/f;->a:Lcom/google/android/apps/gmm/map/u/a/b;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v10, v11, v5}, Lcom/google/android/apps/gmm/map/u/a/b;->a(Lcom/google/android/apps/gmm/map/b/a/y;DLcom/google/android/apps/gmm/map/b/a/ae;)Ljava/util/Iterator;

    move-result-object v5

    const/16 v8, 0x168

    const/4 v3, 0x0

    invoke-virtual {v14}, Lcom/google/android/apps/gmm/map/r/b/a;->hasBearing()Z

    move-result v6

    if-eqz v6, :cond_a

    iget-object v6, v14, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v6, :cond_1

    iget-object v6, v14, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v6, v6, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    if-eqz v6, :cond_1

    const/4 v6, 0x1

    :goto_0
    if-eqz v6, :cond_a

    invoke-virtual {v14}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v3

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/google/android/apps/gmm/navigation/i/f;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    iget-object v6, v6, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v8, v6, Lcom/google/r/b/a/qz;->e:I

    move v6, v3

    :goto_1
    const/4 v3, 0x1

    float-to-double v6, v6

    const/4 v9, 0x0

    const/4 v12, -0x1

    invoke-static/range {v3 .. v12}, Lcom/google/android/apps/gmm/navigation/i/f;->a(ZLcom/google/android/apps/gmm/map/b/a/y;Ljava/util/Iterator;DIIDI)Ljava/util/List;

    move-result-object v19

    const-wide v6, 0x7fefffffffffffffL    # Double.MAX_VALUE

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/navigation/i/e;

    iget-wide v8, v3, Lcom/google/android/apps/gmm/navigation/i/e;->g:D

    cmpg-double v8, v8, v6

    if-gez v8, :cond_0

    iget-wide v6, v3, Lcom/google/android/apps/gmm/navigation/i/e;->g:D

    goto :goto_2

    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    :cond_2
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    mul-double/2addr v6, v8

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v4

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/f;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v3, v3, Lcom/google/r/b/a/qz;->h:I

    int-to-double v8, v3

    mul-double/2addr v4, v8

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    move-object/from16 v0, v19

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/gmm/navigation/i/f;->a(Ljava/util/List;D)V

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    :cond_3
    :goto_3
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v12, v3

    check-cast v12, Lcom/google/android/apps/gmm/navigation/i/e;

    iget-object v3, v12, Lcom/google/android/apps/gmm/navigation/i/e;->c:Lcom/google/android/apps/gmm/map/u/a/e;

    iget v3, v3, Lcom/google/android/apps/gmm/map/u/a/e;->c:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    :goto_4
    if-eqz v3, :cond_3

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/f;->a:Lcom/google/android/apps/gmm/map/u/a/b;

    iget-object v4, v12, Lcom/google/android/apps/gmm/navigation/i/e;->c:Lcom/google/android/apps/gmm/map/u/a/e;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/gmm/map/u/a/b;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/b/a/ae;)Lcom/google/android/apps/gmm/map/u/a/e;

    move-result-object v6

    if-eqz v6, :cond_4

    new-instance v3, Lcom/google/android/apps/gmm/navigation/i/e;

    iget-boolean v4, v12, Lcom/google/android/apps/gmm/navigation/i/e;->a:Z

    iget-object v5, v12, Lcom/google/android/apps/gmm/navigation/i/e;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v7, v12, Lcom/google/android/apps/gmm/navigation/i/e;->d:I

    iget v8, v12, Lcom/google/android/apps/gmm/navigation/i/e;->e:I

    iget-object v9, v12, Lcom/google/android/apps/gmm/navigation/i/e;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-wide v10, v12, Lcom/google/android/apps/gmm/navigation/i/e;->g:D

    iget v12, v12, Lcom/google/android/apps/gmm/navigation/i/e;->h:I

    invoke-direct/range {v3 .. v12}, Lcom/google/android/apps/gmm/navigation/i/e;-><init>(ZLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/u/a/e;IILcom/google/android/apps/gmm/map/b/a/y;DI)V

    move-object/from16 v0, v21

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    goto :goto_4

    :cond_6
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface/range {v19 .. v19}, Ljava/util/Collection;->size()I

    move-result v3

    if-nez v3, :cond_7

    new-instance v2, Lcom/google/android/apps/gmm/navigation/i/a/a;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/gmm/navigation/i/a/b;->b:Lcom/google/android/apps/gmm/navigation/i/a/b;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/navigation/i/a/a;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/navigation/i/a/b;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 84
    :goto_5
    return-void

    .line 82
    :cond_7
    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v15}, Lcom/google/android/apps/gmm/navigation/i/f;->a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/r/a/w;)Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v4

    if-nez v4, :cond_8

    new-instance v2, Lcom/google/android/apps/gmm/navigation/i/a/a;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/gmm/navigation/i/a/b;->b:Lcom/google/android/apps/gmm/navigation/i/a/b;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/navigation/i/a/a;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/navigation/i/a/b;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_5

    :cond_8
    move-object/from16 v0, v19

    invoke-virtual {v13, v0, v3}, Lcom/google/android/apps/gmm/navigation/i/v;->a(Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v4

    iget-object v5, v13, Lcom/google/android/apps/gmm/navigation/i/v;->a:Lcom/google/android/apps/gmm/navigation/i/x;

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v4, v1, v3}, Lcom/google/android/apps/gmm/navigation/i/x;->a(Lcom/google/android/apps/gmm/map/u/a/b;Lcom/google/android/apps/gmm/map/b/a/ae;Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/android/apps/gmm/navigation/i/aa;

    move-result-object v5

    if-nez v5, :cond_9

    new-instance v2, Lcom/google/android/apps/gmm/navigation/i/a/a;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/gmm/navigation/i/a/b;->c:Lcom/google/android/apps/gmm/navigation/i/a/b;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/navigation/i/a/a;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/navigation/i/a/b;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_5

    :cond_9
    const-string v3, "RouteFinder"

    iget-object v6, v5, Lcom/google/android/apps/gmm/navigation/i/aa;->a:Lcom/google/android/apps/gmm/navigation/i/e;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v5, Lcom/google/android/apps/gmm/navigation/i/aa;->b:Lcom/google/android/apps/gmm/navigation/i/e;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x16

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Found path from: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " to: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3, v6, v7}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v14}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v14}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v8

    invoke-direct {v3, v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v6

    move-object/from16 v3, v16

    move-object v7, v15

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/gmm/navigation/i/a;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/u/a/b;Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/navigation/i/aa;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/map/r/a/w;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/navigation/i/a/a;

    sget-object v4, Lcom/google/android/apps/gmm/navigation/i/a/b;->a:Lcom/google/android/apps/gmm/navigation/i/a/b;

    invoke-direct {v3, v2, v4}, Lcom/google/android/apps/gmm/navigation/i/a/a;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/navigation/i/a/b;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto/16 :goto_5

    :cond_a
    move v6, v3

    goto/16 :goto_1
.end method
