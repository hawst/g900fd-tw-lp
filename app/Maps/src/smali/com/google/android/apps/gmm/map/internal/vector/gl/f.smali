.class public Lcom/google/android/apps/gmm/map/internal/vector/gl/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:[I

.field b:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>([I)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/f;->a:[I

    invoke-static {p1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/f;->b:I

    .line 27
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 36
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/f;->a:[I

    check-cast p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/f;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/f;->a:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/f;->b:I

    return v0
.end method
