.class public final enum Lcom/google/android/apps/gmm/cardui/b/b;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/cardui/b/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/cardui/b/b;

.field public static final enum b:Lcom/google/android/apps/gmm/cardui/b/b;

.field public static final enum c:Lcom/google/android/apps/gmm/cardui/b/b;

.field public static final enum d:Lcom/google/android/apps/gmm/cardui/b/b;

.field private static final synthetic h:[Lcom/google/android/apps/gmm/cardui/b/b;


# instance fields
.field public final e:Lcom/google/b/f/t;

.field public final f:Lcom/google/b/f/t;

.field public final g:Lcom/google/b/f/t;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lcom/google/android/apps/gmm/cardui/b/b;

    const-string v1, "MAPS_ACTIVITY"

    sget-object v3, Lcom/google/b/f/t;->fQ:Lcom/google/b/f/t;

    sget-object v4, Lcom/google/b/f/t;->fh:Lcom/google/b/f/t;

    sget-object v5, Lcom/google/b/f/t;->es:Lcom/google/b/f/t;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/cardui/b/b;-><init>(Ljava/lang/String;ILcom/google/b/f/t;Lcom/google/b/f/t;Lcom/google/b/f/t;)V

    sput-object v0, Lcom/google/android/apps/gmm/cardui/b/b;->a:Lcom/google/android/apps/gmm/cardui/b/b;

    .line 14
    new-instance v3, Lcom/google/android/apps/gmm/cardui/b/b;

    const-string v4, "MY_MAPS"

    sget-object v6, Lcom/google/b/f/t;->bz:Lcom/google/b/f/t;

    move v5, v9

    move-object v8, v7

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/cardui/b/b;-><init>(Ljava/lang/String;ILcom/google/b/f/t;Lcom/google/b/f/t;Lcom/google/b/f/t;)V

    sput-object v3, Lcom/google/android/apps/gmm/cardui/b/b;->b:Lcom/google/android/apps/gmm/cardui/b/b;

    .line 17
    new-instance v3, Lcom/google/android/apps/gmm/cardui/b/b;

    const-string v4, "ODELAY"

    sget-object v6, Lcom/google/b/f/t;->am:Lcom/google/b/f/t;

    sget-object v7, Lcom/google/b/f/t;->aQ:Lcom/google/b/f/t;

    sget-object v8, Lcom/google/b/f/t;->er:Lcom/google/b/f/t;

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/cardui/b/b;-><init>(Ljava/lang/String;ILcom/google/b/f/t;Lcom/google/b/f/t;Lcom/google/b/f/t;)V

    sput-object v3, Lcom/google/android/apps/gmm/cardui/b/b;->c:Lcom/google/android/apps/gmm/cardui/b/b;

    .line 20
    new-instance v3, Lcom/google/android/apps/gmm/cardui/b/b;

    const-string v4, "DIRECTORY"

    sget-object v6, Lcom/google/b/f/t;->Z:Lcom/google/b/f/t;

    sget-object v7, Lcom/google/b/f/t;->aa:Lcom/google/b/f/t;

    sget-object v8, Lcom/google/b/f/t;->ep:Lcom/google/b/f/t;

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/cardui/b/b;-><init>(Ljava/lang/String;ILcom/google/b/f/t;Lcom/google/b/f/t;Lcom/google/b/f/t;)V

    sput-object v3, Lcom/google/android/apps/gmm/cardui/b/b;->d:Lcom/google/android/apps/gmm/cardui/b/b;

    .line 10
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/cardui/b/b;

    sget-object v1, Lcom/google/android/apps/gmm/cardui/b/b;->a:Lcom/google/android/apps/gmm/cardui/b/b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/cardui/b/b;->b:Lcom/google/android/apps/gmm/cardui/b/b;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/apps/gmm/cardui/b/b;->c:Lcom/google/android/apps/gmm/cardui/b/b;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/apps/gmm/cardui/b/b;->d:Lcom/google/android/apps/gmm/cardui/b/b;

    aput-object v1, v0, v11

    sput-object v0, Lcom/google/android/apps/gmm/cardui/b/b;->h:[Lcom/google/android/apps/gmm/cardui/b/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/b/f/t;Lcom/google/b/f/t;Lcom/google/b/f/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/f/t;",
            "Lcom/google/b/f/t;",
            "Lcom/google/b/f/t;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput-object p3, p0, Lcom/google/android/apps/gmm/cardui/b/b;->e:Lcom/google/b/f/t;

    .line 33
    iput-object p4, p0, Lcom/google/android/apps/gmm/cardui/b/b;->f:Lcom/google/b/f/t;

    .line 34
    iput-object p5, p0, Lcom/google/android/apps/gmm/cardui/b/b;->g:Lcom/google/b/f/t;

    .line 35
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/cardui/b/b;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/google/android/apps/gmm/cardui/b/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/cardui/b/b;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/cardui/b/b;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/google/android/apps/gmm/cardui/b/b;->h:[Lcom/google/android/apps/gmm/cardui/b/b;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/cardui/b/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/cardui/b/b;

    return-object v0
.end method
