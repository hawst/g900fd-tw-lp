.class public Lcom/google/android/apps/gmm/car/e/ao;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/h/p;


# instance fields
.field a:Lcom/google/android/apps/gmm/map/r/a/ae;

.field b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

.field c:Ljava/util/Collection;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/car/h/q;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/apps/gmm/map/util/b/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/a/a;Lcom/google/android/apps/gmm/map/r/a/ae;[Lcom/google/android/apps/gmm/navigation/g/b/k;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    array-length v1, p3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 42
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/e/ao;->d:Lcom/google/android/apps/gmm/map/util/b/a/a;

    .line 43
    iput-object p2, p0, Lcom/google/android/apps/gmm/car/e/ao;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 44
    iput-object p3, p0, Lcom/google/android/apps/gmm/car/e/ao;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 45
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ao;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/map/r/a/ao;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ao;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/car/h/q;)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ao;->c:Ljava/util/Collection;

    if-nez v0, :cond_0

    .line 88
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/ao;->c:Ljava/util/Collection;

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ao;->c:Ljava/util/Collection;

    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 91
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ao;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    return v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ao;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/car/h/q;)V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ao;->c:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 96
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 97
    :cond_0
    return-void
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ao;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->f:I

    return v0
.end method

.method public final d(I)Lcom/google/maps/g/a/fz;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ao;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->G:Lcom/google/maps/g/a/fz;

    return-object v0
.end method

.method public final e(I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 79
    if-ltz p1, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    .line 80
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/ao;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v2

    if-ge p1, v2, :cond_2

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 81
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ao;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    .line 82
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/ao;->d:Lcom/google/android/apps/gmm/map/util/b/a/a;

    new-instance v3, Lcom/google/android/apps/gmm/navigation/j/a/e;

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/navigation/j/a/e;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;Z)V

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/a/a;->c(Ljava/lang/Object;)V

    .line 83
    return-void
.end method
