.class Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/mapsactivity/oobe/b/a;


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/libraries/curvular/ce;

.field private final c:Lcom/google/android/libraries/curvular/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/b/i",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/libraries/curvular/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/b/i",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/android/libraries/curvular/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/b/i",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/b/i;Lcom/google/android/libraries/curvular/b/i;Lcom/google/android/libraries/curvular/b/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/libraries/curvular/ce;",
            "Lcom/google/android/libraries/curvular/b/i",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/google/android/libraries/curvular/b/i",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/google/android/libraries/curvular/b/i",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput p1, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;->a:I

    .line 21
    iput-object p2, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;->b:Lcom/google/android/libraries/curvular/ce;

    .line 22
    iput-object p3, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;->c:Lcom/google/android/libraries/curvular/b/i;

    .line 23
    iput-object p4, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;->d:Lcom/google/android/libraries/curvular/b/i;

    .line 24
    iput-object p5, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;->e:Lcom/google/android/libraries/curvular/b/i;

    .line 25
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;->c:Lcom/google/android/libraries/curvular/b/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;->b:Lcom/google/android/libraries/curvular/ce;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/curvular/b/i;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;->d:Lcom/google/android/libraries/curvular/b/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;->b:Lcom/google/android/libraries/curvular/ce;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/curvular/b/i;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;->e:Lcom/google/android/libraries/curvular/b/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;->b:Lcom/google/android/libraries/curvular/ce;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/curvular/b/i;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const/4 v0, 0x0

    return-object v0
.end method
