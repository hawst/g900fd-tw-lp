.class public Lcom/google/android/apps/gmm/place/station/ao;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/station/b/f;
.implements Ljava/io/Serializable;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field c:I
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field transient d:Lcom/google/android/libraries/curvular/au;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/Boolean;

.field private final i:Ljava/lang/Boolean;

.field private final j:Lcom/google/android/libraries/curvular/a;

.field private final k:Lcom/google/android/apps/gmm/place/station/ap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/maps/g/a/gu;ILcom/google/android/apps/gmm/place/station/ap;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-virtual {p2}, Lcom/google/maps/g/a/gu;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->e:Ljava/lang/String;

    .line 66
    iget v0, p2, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p2, Lcom/google/maps/g/a/gu;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fo;

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;Lcom/google/maps/g/a/fo;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->a:Ljava/lang/String;

    .line 68
    iget v0, p2, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    .line 69
    iget-object v0, p2, Lcom/google/maps/g/a/gu;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fo;

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;Lcom/google/maps/g/a/fo;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->b:Ljava/lang/String;

    .line 70
    iget v0, p2, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_4
    if-eqz v0, :cond_5

    .line 71
    iget-object v0, p2, Lcom/google/maps/g/a/gu;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fo;

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;Lcom/google/maps/g/a/fo;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->f:Ljava/lang/String;

    .line 72
    iget v0, p2, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_6
    if-eqz v0, :cond_7

    .line 73
    iget-object v0, p2, Lcom/google/maps/g/a/gu;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fo;

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;Lcom/google/maps/g/a/fo;)Ljava/lang/String;

    move-result-object v0

    :goto_7
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->g:Ljava/lang/String;

    .line 75
    iget-object v0, p2, Lcom/google/maps/g/a/gu;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fo;

    iget-object v1, p2, Lcom/google/maps/g/a/gu;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/fo;

    .line 74
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/station/ao;->a(Lcom/google/maps/g/a/fo;Lcom/google/maps/g/a/fo;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->h:Ljava/lang/Boolean;

    .line 77
    iget-object v0, p2, Lcom/google/maps/g/a/gu;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fo;

    iget-object v1, p2, Lcom/google/maps/g/a/gu;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/fo;

    .line 76
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/station/ao;->a(Lcom/google/maps/g/a/fo;Lcom/google/maps/g/a/fo;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->i:Ljava/lang/Boolean;

    .line 78
    new-instance v0, Lcom/google/android/libraries/curvular/a;

    invoke-direct {v0, p3}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->j:Lcom/google/android/libraries/curvular/a;

    .line 79
    if-nez p4, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    .line 66
    goto/16 :goto_0

    .line 67
    :cond_1
    const-string v0, ""

    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 68
    goto/16 :goto_2

    .line 69
    :cond_3
    const-string v0, ""

    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 70
    goto/16 :goto_4

    .line 71
    :cond_5
    const-string v0, ""

    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 72
    goto :goto_6

    .line 73
    :cond_7
    const-string v0, ""

    goto :goto_7

    .line 79
    :cond_8
    check-cast p4, Lcom/google/android/apps/gmm/place/station/ap;

    iput-object p4, p0, Lcom/google/android/apps/gmm/place/station/ao;->k:Lcom/google/android/apps/gmm/place/station/ap;

    .line 80
    return-void
.end method

.method private static a(Lcom/google/maps/g/a/fo;Lcom/google/maps/g/a/fo;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 89
    iget v2, p0, Lcom/google/maps/g/a/fo;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    iget v2, p1, Lcom/google/maps/g/a/fo;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_1
    if-eqz v2, :cond_2

    .line 90
    iget-wide v2, p0, Lcom/google/maps/g/a/fo;->b:J

    iget-wide v4, p1, Lcom/google/maps/g/a/fo;->b:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/16 v4, 0x3c

    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    :goto_2
    return v0

    :cond_0
    move v2, v1

    .line 89
    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 90
    goto :goto_2
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->h:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->i:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/aq;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->j:Lcom/google/android/libraries/curvular/a;

    return-object v0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->k:Lcom/google/android/apps/gmm/place/station/ap;

    sget-object v1, Lcom/google/android/apps/gmm/place/station/ap;->c:Lcom/google/android/apps/gmm/place/station/ap;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->k:Lcom/google/android/apps/gmm/place/station/ap;

    sget-object v1, Lcom/google/android/apps/gmm/place/station/ap;->b:Lcom/google/android/apps/gmm/place/station/ap;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Lcom/google/android/libraries/curvular/au;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const v6, 0xffffff

    .line 146
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->d:Lcom/google/android/libraries/curvular/au;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->c:I

    if-lez v0, :cond_0

    .line 148
    iget v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->c:I

    int-to-double v0, v0

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v0, v1}, Lcom/google/b/g/a;->a(D)Z

    move-result v3

    if-eqz v3, :cond_1

    double-to-int v1, v0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x0

    iput v1, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/ao;->d:Lcom/google/android/libraries/curvular/au;

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ao;->d:Lcom/google/android/libraries/curvular/au;

    return-object v0

    .line 148
    :cond_1
    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    mul-double/2addr v0, v4

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v3}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method
