.class public Lcom/google/android/apps/gmm/util/a/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Landroid/os/Handler;

.field final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/a/e;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/a/d;",
            ">;"
        }
    .end annotation
.end field

.field final e:Lcom/google/android/apps/gmm/shared/c/a/p;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/google/android/apps/gmm/util/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/util/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/a/p;)V
    .locals 2

    .prologue
    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/a/a;->b:Landroid/os/Handler;

    .line 232
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/a/a;->c:Ljava/util/List;

    .line 235
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/a/a;->d:Ljava/util/List;

    .line 241
    iput-object p1, p0, Lcom/google/android/apps/gmm/util/a/a;->e:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 242
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/util/a/a;)V
    .locals 4

    .prologue
    .line 33
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/a;->d:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/util/a/a;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/util/a/d;

    sget-object v1, Lcom/google/android/apps/gmm/util/a/a;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "running deferred action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    if-ne v1, p0, :cond_0

    const/4 v1, 0x1

    :goto_1
    const-string v2, "Not the owner of the action"

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/util/a/d;->run()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/util/a/d;->c()V

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/util/a/d;Lcom/google/android/apps/gmm/util/a/d;)Lcom/google/android/apps/gmm/util/a/d;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/a;->e:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 307
    iget-object v0, p1, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 308
    iget-object v0, p1, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    if-ne v0, p0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Tried to replace action %s which is on list %s, not %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    .line 310
    iget-object v2, p1, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    aput-object v2, v4, v1

    const/4 v1, 0x2

    aput-object p0, v4, v1

    .line 308
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 307
    goto :goto_0

    :cond_1
    move v0, v2

    .line 308
    goto :goto_1

    .line 311
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/util/a/d;->b()V

    .line 313
    :cond_3
    invoke-virtual {p0, p2}, Lcom/google/android/apps/gmm/util/a/a;->a(Lcom/google/android/apps/gmm/util/a/d;)V

    .line 314
    return-object p2
.end method

.method public final a(Lcom/google/android/apps/gmm/util/a/d;)V
    .locals 3

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/a;->e:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 250
    iget-object v0, p1, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Action already pending"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 252
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/util/a/d;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 265
    :goto_1
    return-void

    .line 256
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 257
    sget-object v0, Lcom/google/android/apps/gmm/util/a/a;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "executing action immediately: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/util/a/d;->run()V

    .line 259
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/util/a/d;->c()V

    goto :goto_1

    .line 261
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/util/a/a;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "deferring action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    iput-object p0, p1, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/a;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/util/a/e;)V
    .locals 4

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/a;->e:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 322
    iget-object v0, p1, Lcom/google/android/apps/gmm/util/a/e;->a:Lcom/google/android/apps/gmm/util/a/a;

    if-eqz v0, :cond_1

    .line 324
    sget-object v0, Lcom/google/android/apps/gmm/util/a/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Already blocked; attached stack trace of the original call to block() below this one"

    .line 326
    iget-object v3, p1, Lcom/google/android/apps/gmm/util/a/e;->b:Ljava/lang/Throwable;

    invoke-direct {v1, v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 324
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 329
    iget-object v0, p1, Lcom/google/android/apps/gmm/util/a/e;->a:Lcom/google/android/apps/gmm/util/a/a;

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Already blocked on different list"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 332
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/a;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 333
    iput-object p0, p1, Lcom/google/android/apps/gmm/util/a/e;->a:Lcom/google/android/apps/gmm/util/a/a;

    .line 334
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Original call to block()"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    iput-object v0, p1, Lcom/google/android/apps/gmm/util/a/e;->b:Ljava/lang/Throwable;

    .line 335
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/util/a/e;->c:Z

    if-eqz v0, :cond_2

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/a;->b:Landroid/os/Handler;

    iget-object v1, p1, Lcom/google/android/apps/gmm/util/a/e;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 338
    :cond_2
    return-void
.end method
