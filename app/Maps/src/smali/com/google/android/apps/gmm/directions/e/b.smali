.class Lcom/google/android/apps/gmm/directions/e/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/e/f;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/f/a/a;

.field final synthetic b:Lcom/google/android/apps/gmm/directions/e/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/e/a;Lcom/google/android/apps/gmm/directions/f/a/a;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/e/b;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/i/d;)V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 154
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    monitor-enter v1

    .line 158
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/e/g;->b:Lcom/google/android/apps/gmm/directions/e/f;

    if-eq p0, v0, :cond_1

    .line 159
    monitor-exit v1

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/e/a;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v9

    :goto_1
    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/a/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/a/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 167
    :goto_2
    iget-object v10, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    new-instance v0, Lcom/google/android/apps/gmm/map/i/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    .line 169
    iget-object v2, v1, Lcom/google/android/apps/gmm/directions/e/a;->c:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    .line 171
    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/e/a;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    .line 172
    iget-object v5, v1, Lcom/google/android/apps/gmm/directions/e/a;->e:Lcom/google/android/apps/gmm/map/x;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/b;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    .line 173
    iget-boolean v6, v1, Lcom/google/android/apps/gmm/directions/f/a/a;->k:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    .line 174
    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/e/a;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->j()Z

    move-result v7

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/i/h;-><init>(Lcom/google/android/apps/gmm/map/i/d;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/x;ZZ)V

    .line 167
    iput-object v0, v10, Lcom/google/android/apps/gmm/directions/e/a;->f:Lcom/google/android/apps/gmm/map/i/h;

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/directions/f/a/a;->g:Z

    if-eqz v0, :cond_5

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v10, v0, Lcom/google/android/apps/gmm/directions/e/a;->f:Lcom/google/android/apps/gmm/map/i/h;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/e/a;->b:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v6

    move v7, v8

    :goto_3
    iget-object v0, v10, Lcom/google/android/apps/gmm/map/i/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_5

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/i/h;->a:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/map/i/j;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/i/j;->c:Lcom/google/android/apps/gmm/map/t/q;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/apps/gmm/map/t/q;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/i/j;->b:Lcom/google/android/apps/gmm/map/t/l;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/t/q;-><init>(Lcom/google/android/apps/gmm/map/t/k;)V

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/i/j;->c:Lcom/google/android/apps/gmm/map/t/q;

    :cond_2
    iget-object v11, v1, Lcom/google/android/apps/gmm/map/i/j;->c:Lcom/google/android/apps/gmm/map/t/q;

    new-instance v0, Lcom/google/android/apps/gmm/map/t/y;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/i/j;->d:Lcom/google/android/apps/gmm/map/t/w;

    sget-object v4, Lcom/google/android/apps/gmm/map/j/x;->a:Lcom/google/android/apps/gmm/map/j/x;

    const/4 v2, 0x3

    new-array v5, v2, [Ljava/lang/Object;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/i/j;->e:Lcom/google/android/apps/gmm/map/r/a/w;

    aput-object v2, v5, v8

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/i/j;->f:Ljava/lang/String;

    aput-object v2, v5, v9

    const/4 v2, 0x2

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/i/j;->g:Ljava/lang/String;

    aput-object v1, v5, v2

    move v1, v9

    move v2, v8

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/t/y;-><init>(ZZLcom/google/android/apps/gmm/map/t/x;Lcom/google/android/apps/gmm/map/j/s;[Ljava/lang/Object;Lcom/google/android/apps/gmm/map/util/b/g;)V

    invoke-virtual {v11, v0}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_3

    .line 161
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move v0, v8

    .line 163
    goto/16 :goto_1

    .line 166
    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 178
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/e/a;->f:Lcom/google/android/apps/gmm/map/i/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/b;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/f/a/a;->l:Lcom/google/android/apps/gmm/map/r/a/v;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/e/b;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    .line 179
    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/f/a/a;->m:Lcom/google/android/apps/gmm/map/o/b/h;

    .line 178
    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/i/h;->a(Lcom/google/android/apps/gmm/map/r/a/v;Lcom/google/android/apps/gmm/map/o/b/h;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/e/a;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/e/a;->f:Lcom/google/android/apps/gmm/map/i/h;

    if-nez v1, :cond_9

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->n()V

    .line 182
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/e/a;->d:Lcom/google/android/apps/gmm/map/t;

    .line 183
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/i/d;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/e/b;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/directions/f/a/a;->j:Lcom/google/android/apps/gmm/map/ag;

    .line 182
    invoke-virtual {v1, v2, v3, v9}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/ag;Z)Lcom/google/android/apps/gmm/map/b/b;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/e/a;->h:Lcom/google/android/apps/gmm/map/b/a;

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/i/d;->c:Lcom/google/android/apps/gmm/map/b/a/r;

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/e/a;->g:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/directions/f/a/a;->c:Z

    if-eqz v0, :cond_6

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/b;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/f/a/a;->b:Lcom/google/android/apps/gmm/directions/f/a/c;

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/e/a;->g:Lcom/google/android/apps/gmm/map/b/a/r;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/directions/e/a;->a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/directions/f/a/c;)V

    .line 191
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/a/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/directions/f/a/a;->d:Z

    if-eqz v0, :cond_7

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    .line 193
    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/a/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->x:Lcom/google/maps/g/by;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/indoor/d/f;->a(Lcom/google/maps/g/by;)Lcom/google/android/apps/gmm/map/indoor/d/f;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_7

    .line 195
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/e/a;->b:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/indoor/a/a;->a(Lcom/google/android/apps/gmm/map/indoor/d/f;)V

    .line 200
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    monitor-enter v1

    .line 201
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/e/g;->b:Lcom/google/android/apps/gmm/directions/e/f;

    if-ne p0, v0, :cond_8

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/b;->b:Lcom/google/android/apps/gmm/directions/e/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/directions/e/g;->c:Z

    .line 204
    :cond_8
    monitor-exit v1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 180
    :cond_9
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    goto :goto_4
.end method
