.class public Lcom/google/android/apps/gmm/place/bc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/i/c;


# static fields
.field static final a:Ljava/lang/String;

.field private static c:I


# instance fields
.field b:Landroid/content/Context;

.field private d:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/i/d;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/android/apps/gmm/base/l/a/u;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/apps/gmm/place/bc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/bc;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bc;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->B:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ki;->i()Lcom/google/maps/g/ki;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ki;

    iget-object v0, v0, Lcom/google/maps/g/ki;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x5

    .line 49
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/bc;->b:Landroid/content/Context;

    .line 50
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/bc;->d:Lcom/google/android/apps/gmm/x/o;

    .line 52
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bc;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->B:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ki;->i()Lcom/google/maps/g/ki;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ki;

    iget-object v1, v0, Lcom/google/maps/g/ki;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v2, :cond_0

    iget-object v1, v0, Lcom/google/maps/g/ki;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sput v1, Lcom/google/android/apps/gmm/place/bc;->c:I

    :goto_0
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    sget v1, Lcom/google/android/apps/gmm/place/bc;->c:I

    if-ge v2, v1, :cond_1

    iget-object v1, v0, Lcom/google/maps/g/ki;->h:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/cs;->g()Lcom/google/maps/g/cs;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/cs;

    new-instance v4, Lcom/google/android/apps/gmm/place/d;

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/bc;->b:Landroid/content/Context;

    invoke-direct {v4, v1, v5}, Lcom/google/android/apps/gmm/place/d;-><init>(Lcom/google/maps/g/cs;Landroid/content/Context;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_0
    sput v2, Lcom/google/android/apps/gmm/place/bc;->c:I

    goto :goto_0

    :cond_1
    iput-object v3, p0, Lcom/google/android/apps/gmm/place/bc;->e:Ljava/util/List;

    .line 54
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    .line 57
    new-instance v1, Lcom/google/android/apps/gmm/place/bd;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/place/bd;-><init>(Lcom/google/android/apps/gmm/place/bc;Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/bc;->f:Lcom/google/android/apps/gmm/base/l/a/u;

    .line 90
    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/i/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bc;->e:Ljava/util/List;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bc;->b:Landroid/content/Context;

    const v1, -0x21524111

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/base/l/a/u;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bc;->f:Lcom/google/android/apps/gmm/base/l/a/u;

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 156
    sget v0, Lcom/google/android/apps/gmm/place/bc;->c:I

    if-le v0, v2, :cond_0

    .line 157
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 159
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bc;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 165
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bc;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->B:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ki;->i()Lcom/google/maps/g/ki;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ki;

    iget-object v0, v0, Lcom/google/maps/g/ki;->h:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/cs;->g()Lcom/google/maps/g/cs;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/cs;

    .line 162
    invoke-virtual {v0}, Lcom/google/maps/g/cs;->d()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/place/be;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/place/be;-><init>(Lcom/google/android/apps/gmm/place/bc;)V

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/place/be;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    const v2, -0x21524111

    .line 170
    sget v0, Lcom/google/android/apps/gmm/place/bc;->c:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bc;->b:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 173
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bc;->b:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method
