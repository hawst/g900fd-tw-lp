.class public Lcom/google/android/apps/gmm/map/indoor/c/s;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/b/e;


# instance fields
.field public volatile a:Z

.field public volatile b:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public volatile c:Lcom/google/android/apps/gmm/map/indoor/c/q;

.field private final d:Lcom/google/android/apps/gmm/map/internal/b/e;

.field private e:Lcom/google/android/apps/gmm/map/internal/c/cw;

.field private final f:Lcom/google/android/apps/gmm/map/indoor/c/t;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/b/e;Lcom/google/android/apps/gmm/map/internal/c/cw;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->a:Z

    .line 41
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/c/t;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/indoor/c/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    .line 60
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->d:Lcom/google/android/apps/gmm/map/internal/b/e;

    .line 61
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->e:Lcom/google/android/apps/gmm/map/internal/c/cw;

    .line 62
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/indoor/d/e;)Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 5

    .prologue
    .line 228
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/cd;-><init>()V

    .line 229
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/x;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/internal/c/x;-><init>()V

    .line 230
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 229
    iput-object v2, v1, Lcom/google/android/apps/gmm/map/internal/c/x;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 230
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/x;->a()Lcom/google/android/apps/gmm/map/internal/c/w;

    move-result-object v1

    .line 229
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V

    .line 232
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    return-object v1
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)F
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->d:Lcom/google/android/apps/gmm/map/internal/b/e;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/internal/b/e;->a(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/o;Ljava/util/List;)J
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/f/o;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;)J"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 108
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    monitor-enter v5

    .line 109
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->c:Lcom/google/android/apps/gmm/map/indoor/c/q;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-wide v6, v3, Lcom/google/android/apps/gmm/map/indoor/c/t;->f:J

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v3, v3, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    const/high16 v4, 0x41600000    # 14.0f

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->d:Lcom/google/android/apps/gmm/map/internal/b/e;

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/indoor/c/t;->a:Ljava/util/List;

    invoke-interface {v4, p1, v8}, Lcom/google/android/apps/gmm/map/internal/b/e;->a(Lcom/google/android/apps/gmm/map/f/o;Ljava/util/List;)J

    move-result-wide v8

    iput-wide v8, v3, Lcom/google/android/apps/gmm/map/indoor/c/t;->f:J

    :goto_0
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->a:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-wide v8, v3, Lcom/google/android/apps/gmm/map/indoor/c/t;->f:J

    cmp-long v3, v8, v6

    if-eqz v3, :cond_9

    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->a:Z

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/c/t;->b:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/c/t;->c:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/c/t;->d:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->e:Lcom/google/android/apps/gmm/map/internal/c/cw;

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v3, v4, v6}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v3

    iget v6, v3, Lcom/google/android/apps/gmm/map/internal/c/cv;->a:I

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->c:Lcom/google/android/apps/gmm/map/indoor/c/q;

    monitor-enter v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v4, v2

    :goto_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/indoor/c/t;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/indoor/c/t;->a:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v8

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->c:Lcom/google/android/apps/gmm/map/indoor/c/q;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/indoor/c/q;->d:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/indoor/d/c;

    iget-object v10, v2, Lcom/google/android/apps/gmm/map/indoor/d/c;->b:Lcom/google/android/apps/gmm/map/b/a/d;

    invoke-interface {v10, v8}, Lcom/google/android/apps/gmm/map/b/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v10

    if-eqz v10, :cond_1

    iget-object v10, v2, Lcom/google/android/apps/gmm/map/indoor/d/c;->a:Lcom/google/android/apps/gmm/map/indoor/d/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->c:Lcom/google/android/apps/gmm/map/indoor/c/q;

    invoke-virtual {v2, v10}, Lcom/google/android/apps/gmm/map/indoor/c/q;->a(Lcom/google/android/apps/gmm/map/indoor/d/a;)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v11

    if-eqz v11, :cond_2

    sget-object v2, Lcom/google/android/apps/gmm/map/indoor/d/e;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    invoke-virtual {v2, v11}, Lcom/google/android/apps/gmm/map/indoor/d/e;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v3, v11}, Lcom/google/android/apps/gmm/map/indoor/c/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/indoor/d/e;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v2

    iget v12, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-gt v12, v6, :cond_6

    iget v12, v10, Lcom/google/android/apps/gmm/map/indoor/d/a;->c:I

    invoke-virtual {v10, v12}, Lcom/google/android/apps/gmm/map/indoor/d/a;->a(I)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/google/android/apps/gmm/map/indoor/d/e;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    iget-object v12, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-object v12, v12, Lcom/google/android/apps/gmm/map/indoor/c/t;->c:Ljava/util/Set;

    invoke-interface {v12, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_2
    if-eqz v10, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->c:Lcom/google/android/apps/gmm/map/indoor/c/q;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/indoor/c/q;->b:Lcom/google/android/apps/gmm/map/indoor/d/a;

    invoke-virtual {v10, v2}, Lcom/google/android/apps/gmm/map/indoor/d/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v10, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_3
    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/indoor/d/e;

    iget v13, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-gt v13, v6, :cond_4

    iget v13, v10, Lcom/google/android/apps/gmm/map/indoor/d/a;->c:I

    invoke-virtual {v10, v13}, Lcom/google/android/apps/gmm/map/indoor/d/a;->a(I)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/google/android/apps/gmm/map/indoor/d/e;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_3

    :cond_4
    invoke-virtual {v2, v11}, Lcom/google/android/apps/gmm/map/indoor/d/e;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_3

    iget-object v13, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-object v13, v13, Lcom/google/android/apps/gmm/map/indoor/c/t;->d:Ljava/util/Set;

    invoke-static {v3, v2}, Lcom/google/android/apps/gmm/map/indoor/c/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/indoor/d/e;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v2

    invoke-interface {v13, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :catchall_0
    move-exception v2

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2

    .line 113
    :catchall_1
    move-exception v2

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2

    .line 109
    :cond_5
    :try_start_3
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    const-wide/16 v8, -0x1

    iput-wide v8, v3, Lcom/google/android/apps/gmm/map/indoor/c/t;->f:J

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/c/t;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_0

    :cond_6
    :try_start_4
    iget-object v12, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-object v12, v12, Lcom/google/android/apps/gmm/map/indoor/c/t;->b:Ljava/util/Set;

    invoke-interface {v12, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_1

    :cond_8
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/indoor/c/t;->e:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v2, Lcom/google/android/apps/gmm/map/indoor/c/t;->e:J

    .line 110
    :cond_9
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->clear()V

    .line 111
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/indoor/c/t;->b:Ljava/util/Set;

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 112
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/indoor/c/t;->e:J

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    return-wide v2
.end method

.method public final a(Ljava/util/Collection;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    monitor-enter v1

    .line 127
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->clear()V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/c/t;->c:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/indoor/c/t;->e:J

    monitor-exit v1

    return-wide v2

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->d:Lcom/google/android/apps/gmm/map/internal/b/e;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/b/e;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILcom/google/android/apps/gmm/map/b/a/y;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/util/Collection;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 143
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    monitor-enter v1

    .line 144
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->clear()V

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/c/t;->d:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/s;->f:Lcom/google/android/apps/gmm/map/indoor/c/t;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/indoor/c/t;->e:J

    monitor-exit v1

    return-wide v2

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
