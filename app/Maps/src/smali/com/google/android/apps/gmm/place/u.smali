.class public Lcom/google/android/apps/gmm/place/u;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/am;
.implements Lcom/google/android/apps/gmm/place/b/c;
.implements Lcom/google/android/apps/gmm/search/ao;


# instance fields
.field final a:Lcom/google/android/apps/gmm/search/an;

.field b:Z

.field final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/shared/net/i;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/apps/gmm/base/placelists/a/a;

.field private final e:Lcom/google/android/apps/gmm/place/w;

.field private final f:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;

.field private final g:Lcom/google/android/apps/gmm/base/placelists/o;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/placelists/a/a;Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;Lcom/google/android/apps/gmm/place/w;I)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/u;->c:Ljava/util/Set;

    .line 61
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    .line 62
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/u;->f:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/search/an;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/search/an;-><init>(Lcom/google/android/apps/gmm/search/ao;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/u;->a:Lcom/google/android/apps/gmm/search/an;

    .line 64
    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/o;

    .line 65
    iget-object v0, p2, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/placelists/o;-><init>(Lcom/google/android/apps/gmm/z/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/u;->g:Lcom/google/android/apps/gmm/base/placelists/o;

    .line 66
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/u;->e:Lcom/google/android/apps/gmm/place/w;

    .line 67
    invoke-direct {p0, p4}, Lcom/google/android/apps/gmm/place/u;->b(I)V

    .line 68
    return-void
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 89
    const/4 v0, 0x0

    .line 90
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->e:Lcom/google/android/apps/gmm/place/w;

    sget-object v2, Lcom/google/android/apps/gmm/place/w;->a:Lcom/google/android/apps/gmm/place/w;

    if-ne v1, v2, :cond_2

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->e()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    .line 96
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/c;->e:Z

    if-nez v1, :cond_1

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->f:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->D()Lcom/google/android/apps/gmm/place/b/b;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->f:Lcom/google/maps/g/hy;

    invoke-interface {v2, v0, v1, p0}, Lcom/google/android/apps/gmm/place/b/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/place/b/c;)Lcom/google/android/apps/gmm/shared/net/i;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 100
    :cond_1
    return-void

    .line 92
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->e:Lcom/google/android/apps/gmm/place/w;

    sget-object v2, Lcom/google/android/apps/gmm/place/w;->b:Lcom/google/android/apps/gmm/place/w;

    if-ne v1, v2, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/placelists/a/a;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    return-object v0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/x/o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    sget-object v0, Lcom/google/android/apps/gmm/place/v;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->e:Lcom/google/android/apps/gmm/place/w;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/w;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 123
    const-string v0, "PlacePageSearchViewAdapter"

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->e:Lcom/google/android/apps/gmm/place/w;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "PlaceItemListType "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    :goto_0
    return-object v0

    .line 119
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    goto :goto_0

    .line 121
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    goto :goto_0

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(ILcom/google/android/apps/gmm/place/PlacePageView;)V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->f:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/place/u;->a(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->c:Lcom/google/android/apps/gmm/base/placelists/a;

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/gmm/base/placelists/a;->a(Landroid/view/View;Lcom/google/android/apps/gmm/x/o;)V

    .line 179
    return-void
.end method

.method public final a(IZ)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->f:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    if-eqz p2, :cond_2

    .line 76
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->g:Lcom/google/android/apps/gmm/base/placelists/o;

    sget-object v0, Lcom/google/android/apps/gmm/place/v;->a:[I

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/u;->e:Lcom/google/android/apps/gmm/place/w;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/place/w;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    const-string v0, "PlacePageSearchViewAdapter"

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/u;->e:Lcom/google/android/apps/gmm/place/w;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x21

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "PlaceItemListType "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not supported."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    :goto_1
    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v1, p1, v0, v2}, Lcom/google/android/apps/gmm/base/placelists/o;->a(ILcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 80
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->e:Lcom/google/android/apps/gmm/place/w;

    sget-object v1, Lcom/google/android/apps/gmm/place/w;->a:Lcom/google/android/apps/gmm/place/w;

    if-ne v0, v1, :cond_3

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->e(I)V

    .line 84
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/place/u;->b(I)V

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->f:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/placelists/a/c;->b()Lcom/google/android/apps/gmm/base/placelists/a/a;

    move-result-object v2

    const/4 v0, 0x0

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->g:Lcom/google/android/apps/gmm/place/w;

    sget-object v4, Lcom/google/android/apps/gmm/place/w;->a:Lcom/google/android/apps/gmm/place/w;

    if-ne v3, v4, :cond_4

    invoke-virtual {v2, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->c:Lcom/google/android/apps/gmm/base/placelists/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/placelists/a;->a(Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/mylocation/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->d()V

    goto :goto_0

    .line 76
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    goto :goto_1

    .line 85
    :cond_4
    iget-object v3, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->g:Lcom/google/android/apps/gmm/place/w;

    sget-object v4, Lcom/google/android/apps/gmm/place/w;->b:Lcom/google/android/apps/gmm/place/w;

    if-ne v3, v4, :cond_5

    invoke-virtual {v2, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    goto :goto_2

    :cond_5
    sget-object v2, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->a:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->g:Lcom/google/android/apps/gmm/place/w;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x21

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "CollectionType "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is not supported."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/place/af;)V
    .locals 0

    .prologue
    .line 174
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/search/al;)V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->f:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->g()V

    .line 228
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/i;)V
    .locals 0

    .prologue
    .line 208
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/i;Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 4

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->f:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    if-eqz p2, :cond_0

    .line 189
    sget-object v0, Lcom/google/android/apps/gmm/place/v;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->e:Lcom/google/android/apps/gmm/place/w;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/w;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "PlacePageSearchViewAdapter"

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->e:Lcom/google/android/apps/gmm/place/w;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "PlaceItemListType "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(Lcom/google/android/apps/gmm/base/g/c;)I

    move-result v0

    .line 190
    :goto_1
    if-ltz v0, :cond_0

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->e:Lcom/google/android/apps/gmm/place/w;

    sget-object v2, Lcom/google/android/apps/gmm/place/w;->a:Lcom/google/android/apps/gmm/place/w;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b(ILcom/google/android/apps/gmm/base/g/c;)V

    .line 192
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->f:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->b(I)V

    goto :goto_0

    .line 189
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(Lcom/google/android/apps/gmm/base/g/c;)I

    move-result v0

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b(Lcom/google/android/apps/gmm/base/g/c;)I

    move-result v0

    goto :goto_1

    .line 191
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(ILcom/google/android/apps/gmm/base/g/c;)V

    goto :goto_2

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final aa_()I
    .locals 4

    .prologue
    .line 104
    sget-object v0, Lcom/google/android/apps/gmm/place/v;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->e:Lcom/google/android/apps/gmm/place/w;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/w;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 110
    const-string v0, "PlacePageSearchViewAdapter"

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/u;->e:Lcom/google/android/apps/gmm/place/w;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "PlaceItemListType "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b()I

    move-result v0

    :goto_0
    return v0

    .line 106
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b()I

    move-result v0

    goto :goto_0

    .line 108
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a()I

    move-result v0

    goto :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->h()Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/16 v8, 0x13

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 159
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/u;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->d:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->f:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/place/u;->b:Z

    .line 161
    iget-object v5, p0, Lcom/google/android/apps/gmm/place/u;->f:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;

    iget-object v0, v5, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    new-instance v6, Lcom/google/android/apps/gmm/search/aj;

    iget-object v1, v0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    invoke-direct {v6, v1}, Lcom/google/android/apps/gmm/search/aj;-><init>(Lcom/google/android/apps/gmm/search/ai;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v1

    iput v1, v6, Lcom/google/android/apps/gmm/search/aj;->c:I

    sget-object v1, Lcom/google/b/f/t;->bh:Lcom/google/b/f/t;

    new-instance v7, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    invoke-virtual {v7, v1}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v1}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/hy;

    iput-object v1, v6, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    iget-object v1, v5, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/hotels/a/b;->b()V

    invoke-interface {v1}, Lcom/google/android/apps/gmm/hotels/a/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/apps/gmm/search/aj;->m:Lcom/google/e/a/a/a/b;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/search/aj;->a()Lcom/google/android/apps/gmm/search/ai;

    move-result-object v1

    new-instance v5, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    invoke-direct {v5, v1, v0}, Lcom/google/android/apps/gmm/search/al;-><init>(Lcom/google/android/apps/gmm/search/ai;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->a:Lcom/google/android/apps/gmm/search/an;

    iput-object v0, v5, Lcom/google/android/apps/gmm/search/al;->d:Lcom/google/android/apps/gmm/search/am;

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->f:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v1, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    .line 164
    iget-object v0, v0, Lcom/google/android/apps/gmm/search/aq;->f:Lcom/google/android/apps/gmm/search/ah;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/ah;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 166
    iget-object v0, v5, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    iget-object v0, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v8}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_2

    move v0, v4

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {v5, v8}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v4

    :goto_1
    if-nez v0, :cond_4

    move-object v0, v2

    :goto_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x29

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Sent TactileSearchRequest: LoggingParams="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    :cond_1
    return-void

    :cond_2
    move v0, v3

    .line 166
    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1

    :cond_4
    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v1

    if-eqz v5, :cond_5

    const/16 v0, 0x19

    invoke-virtual {v5, v8, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    :goto_3
    if-eqz v0, :cond_6

    :goto_4
    check-cast v0, Lcom/google/maps/g/hy;

    goto :goto_2

    :cond_5
    move-object v0, v2

    goto :goto_3

    :cond_6
    move-object v0, v1

    goto :goto_4
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/u;->b:Z

    .line 213
    return-void
.end method

.method public isResumed()Z
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/u;->f:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->isResumed()Z

    move-result v0

    return v0
.end method
