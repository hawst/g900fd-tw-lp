.class public Lcom/google/android/apps/gmm/place/aq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/i/a;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/base/activities/c;

.field c:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/android/apps/gmm/place/an;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final e:Lcom/google/android/apps/gmm/place/cj;

.field f:Lcom/google/android/apps/gmm/place/ax;

.field g:Lcom/google/android/apps/gmm/map/r/a/ap;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final h:Ljava/lang/Object;

.field private i:Lcom/google/android/apps/gmm/z/b/l;

.field private j:Lcom/google/android/apps/gmm/z/b/l;

.field private k:Lcom/google/android/apps/gmm/z/b/l;

.field private l:Lcom/google/android/apps/gmm/z/b/l;

.field private m:Lcom/google/android/apps/gmm/z/b/l;

.field private n:Lcom/google/android/apps/gmm/z/b/l;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/google/android/apps/gmm/place/aq;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/aq;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/place/cj;)V
    .locals 1

    .prologue
    .line 305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    .line 448
    new-instance v0, Lcom/google/android/apps/gmm/place/aw;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/aw;-><init>(Lcom/google/android/apps/gmm/place/aq;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->h:Ljava/lang/Object;

    .line 306
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/aq;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 307
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/aq;->e:Lcom/google/android/apps/gmm/place/cj;

    .line 308
    sget-object v0, Lcom/google/android/apps/gmm/place/ax;->c:Lcom/google/android/apps/gmm/place/ax;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->f:Lcom/google/android/apps/gmm/place/ax;

    .line 309
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x3

    const/4 v11, 0x1

    const/4 v4, 0x0

    .line 102
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v13}, Ljava/util/ArrayList;-><init>(I)V

    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/aq;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/aq;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_0
    move v0, v11

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    new-instance v0, Lcom/google/android/apps/gmm/base/l/a;

    sget v1, Lcom/google/android/apps/gmm/f;->eC:I

    .line 105
    sget v2, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->bf:I

    .line 106
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/place/ar;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/place/ar;-><init>(Lcom/google/android/apps/gmm/place/aq;)V

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/aq;->j:Lcom/google/android/apps/gmm/z/b/l;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/l/a;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/bi;Landroid/view/View$OnClickListener;ZLcom/google/android/apps/gmm/z/b/l;)V

    .line 104
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    if-nez v0, :cond_8

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 117
    new-instance v5, Lcom/google/android/apps/gmm/base/l/a;

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_a

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->B:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ki;->i()Lcom/google/maps/g/ki;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ki;

    iget-boolean v0, v0, Lcom/google/maps/g/ki;->b:Z

    if-eqz v0, :cond_a

    move v0, v11

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_b

    sget v0, Lcom/google/android/apps/gmm/f;->eL:I

    sget v1, Lcom/google/android/apps/gmm/d;->aj:I

    .line 119
    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v6

    .line 121
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_c

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->B:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ki;->i()Lcom/google/maps/g/ki;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ki;

    iget-boolean v0, v0, Lcom/google/maps/g/ki;->b:Z

    if-eqz v0, :cond_c

    move v0, v11

    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_d

    sget v0, Lcom/google/android/apps/gmm/l;->mD:I

    .line 122
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v7

    .line 123
    :goto_5
    new-instance v8, Lcom/google/android/apps/gmm/place/as;

    invoke-direct {v8, p0}, Lcom/google/android/apps/gmm/place/as;-><init>(Lcom/google/android/apps/gmm/place/aq;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->f:Lcom/google/android/apps/gmm/place/ax;

    sget-object v1, Lcom/google/android/apps/gmm/place/ax;->d:Lcom/google/android/apps/gmm/place/ax;

    if-ne v0, v1, :cond_e

    move v0, v11

    :goto_6
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    iget-object v10, p0, Lcom/google/android/apps/gmm/place/aq;->k:Lcom/google/android/apps/gmm/z/b/l;

    invoke-direct/range {v5 .. v10}, Lcom/google/android/apps/gmm/base/l/a;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/bi;Landroid/view/View$OnClickListener;ZLcom/google/android/apps/gmm/z/b/l;)V

    .line 117
    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->M()Lcom/google/android/apps/gmm/base/g/f;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/base/g/f;->d:Lcom/google/android/apps/gmm/base/g/f;

    if-ne v1, v2, :cond_f

    :cond_3
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_7
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 134
    new-instance v0, Lcom/google/android/apps/gmm/base/l/a;

    sget v1, Lcom/google/android/apps/gmm/f;->fz:I

    .line 135
    sget v2, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->kD:I

    .line 136
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/place/at;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/place/at;-><init>(Lcom/google/android/apps/gmm/place/aq;)V

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/aq;->m:Lcom/google/android/apps/gmm/z/b/l;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/l/a;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/bi;Landroid/view/View$OnClickListener;ZLcom/google/android/apps/gmm/z/b/l;)V

    .line 134
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_14

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->M()Lcom/google/android/apps/gmm/base/g/f;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/base/g/f;->d:Lcom/google/android/apps/gmm/base/g/f;

    if-ne v0, v1, :cond_14

    :goto_8
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 147
    new-instance v0, Lcom/google/android/apps/gmm/base/l/a;

    sget v1, Lcom/google/android/apps/gmm/f;->cU:I

    .line 148
    sget v2, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->eW:I

    .line 149
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/place/au;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/place/au;-><init>(Lcom/google/android/apps/gmm/place/aq;)V

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/aq;->n:Lcom/google/android/apps/gmm/z/b/l;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/l/a;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/bi;Landroid/view/View$OnClickListener;ZLcom/google/android/apps/gmm/z/b/l;)V

    .line 147
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    :cond_5
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v13, :cond_6

    .line 161
    new-instance v0, Lcom/google/android/apps/gmm/base/l/a;

    sget v1, Lcom/google/android/apps/gmm/f;->eY:I

    .line 162
    sget v2, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->nb:I

    .line 163
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/place/av;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/place/av;-><init>(Lcom/google/android/apps/gmm/place/aq;)V

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/aq;->l:Lcom/google/android/apps/gmm/z/b/l;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/l/a;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/bi;Landroid/view/View$OnClickListener;ZLcom/google/android/apps/gmm/z/b/l;)V

    .line 161
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    :cond_6
    return-object v12

    :cond_7
    move v0, v4

    .line 103
    goto/16 :goto_0

    .line 116
    :cond_8
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->X()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->f:Lcom/google/android/apps/gmm/place/ax;

    sget-object v1, Lcom/google/android/apps/gmm/place/ax;->c:Lcom/google/android/apps/gmm/place/ax;

    if-eq v0, v1, :cond_9

    move v0, v11

    :goto_9
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    :cond_9
    move v0, v4

    goto :goto_9

    :cond_a
    move v0, v4

    .line 118
    goto/16 :goto_2

    .line 119
    :cond_b
    sget v0, Lcom/google/android/apps/gmm/f;->eL:I

    .line 120
    sget v1, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v6

    goto/16 :goto_3

    :cond_c
    move v0, v4

    .line 121
    goto/16 :goto_4

    .line 122
    :cond_d
    sget v0, Lcom/google/android/apps/gmm/l;->mC:I

    .line 123
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v7

    goto/16 :goto_5

    :cond_e
    move v0, v4

    .line 130
    goto/16 :goto_6

    .line 133
    :cond_f
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->x()Lcom/google/maps/g/hg;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/aq;->f()Z

    move-result v1

    if-nez v1, :cond_11

    if-eqz v0, :cond_13

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_10

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_12

    :cond_10
    move v0, v11

    :goto_a
    if-nez v0, :cond_13

    :cond_11
    move v0, v11

    :goto_b
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_7

    :cond_12
    move v0, v4

    goto :goto_a

    :cond_13
    move v0, v4

    goto :goto_b

    :cond_14
    move v11, v4

    .line 146
    goto/16 :goto_8
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 314
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/aq;->d:Lcom/google/android/apps/gmm/place/an;

    .line 315
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    .line 316
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_2

    move v2, v3

    .line 317
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_0
    move-object v0, v1

    .line 318
    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->o:Ljava/lang/String;

    .line 319
    if-eqz v2, :cond_4

    move-object v0, v1

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->p:Ljava/lang/String;

    .line 320
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/aq;->o:Ljava/lang/String;

    .line 321
    iput-object v5, v0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    new-array v5, v3, [Lcom/google/b/f/cq;

    sget-object v6, Lcom/google/b/f/t;->cV:Lcom/google/b/f/t;

    aput-object v6, v5, v4

    .line 322
    iput-object v5, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 323
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->i:Lcom/google/android/apps/gmm/z/b/l;

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    :goto_3
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v5

    new-array v6, v3, [Lcom/google/b/f/cq;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/aq;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/google/b/f/t;->cX:Lcom/google/b/f/t;

    :goto_4
    aput-object v0, v6, v4

    iput-object v6, v5, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->p:Ljava/lang/String;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->j:Lcom/google/android/apps/gmm/z/b/l;

    .line 325
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    new-array v5, v3, [Lcom/google/b/f/cq;

    sget-object v6, Lcom/google/b/f/t;->dx:Lcom/google/b/f/t;

    aput-object v6, v5, v4

    .line 326
    iput-object v5, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/aq;->p:Ljava/lang/String;

    .line 327
    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/aq;->o:Ljava/lang/String;

    .line 328
    iput-object v5, v0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    .line 329
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->k:Lcom/google/android/apps/gmm/z/b/l;

    .line 330
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    new-array v5, v3, [Lcom/google/b/f/cq;

    sget-object v6, Lcom/google/b/f/t;->dG:Lcom/google/b/f/t;

    aput-object v6, v5, v4

    .line 331
    iput-object v5, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/aq;->p:Ljava/lang/String;

    .line 332
    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->o:Ljava/lang/String;

    .line 333
    iput-object v0, v5, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 334
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->x()Lcom/google/maps/g/hg;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 335
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->x()Lcom/google/maps/g/hg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->d()Ljava/lang/String;

    move-result-object v1

    .line 334
    :cond_1
    iput-object v1, v5, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 337
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->m:Lcom/google/android/apps/gmm/z/b/l;

    .line 338
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    new-array v1, v3, [Lcom/google/b/f/cq;

    sget-object v2, Lcom/google/b/f/t;->dy:Lcom/google/b/f/t;

    aput-object v2, v1, v4

    .line 339
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/aq;->p:Ljava/lang/String;

    .line 340
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    .line 341
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->l:Lcom/google/android/apps/gmm/z/b/l;

    .line 342
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    new-array v1, v3, [Lcom/google/b/f/cq;

    sget-object v2, Lcom/google/b/f/t;->de:Lcom/google/b/f/t;

    aput-object v2, v1, v4

    .line 343
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/aq;->p:Ljava/lang/String;

    .line 344
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    .line 345
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->n:Lcom/google/android/apps/gmm/z/b/l;

    .line 347
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->e()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->g:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 348
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/aq;->e()V

    .line 349
    return-void

    :cond_2
    move v2, v4

    .line 316
    goto/16 :goto_0

    .line 318
    :cond_3
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    goto/16 :goto_1

    .line 319
    :cond_4
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v5, :cond_5

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->i:Ljava/lang/String;

    goto/16 :goto_2

    :cond_5
    move-object v0, v1

    goto/16 :goto_2

    .line 324
    :cond_6
    sget-object v0, Lcom/google/b/f/t;->dc:Lcom/google/b/f/t;

    goto/16 :goto_4

    :cond_7
    move-object v0, v1

    goto/16 :goto_3
.end method

.method b()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 208
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/aq;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/h/d;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v4, "android.hardware.telephony"

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 210
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->n()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    move v1, v2

    :goto_0
    if-nez v1, :cond_4

    .line 211
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    :goto_1
    if-nez v0, :cond_4

    move v0, v2

    :goto_2
    return v0

    :cond_2
    move v1, v3

    .line 210
    goto :goto_0

    :cond_3
    move v0, v3

    .line 211
    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_2
.end method

.method c()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 217
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/aq;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/h/d;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v4, "android.hardware.telephony"

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    if-eqz v0, :cond_5

    .line 219
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v1, :cond_2

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/g/a;->n:Ljava/lang/String;

    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    :cond_0
    move v1, v2

    :goto_1
    if-nez v1, :cond_5

    .line 220
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    move v0, v2

    :goto_2
    if-nez v0, :cond_5

    move v0, v2

    :goto_3
    return v0

    .line 219
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    move v0, v3

    .line 220
    goto :goto_2

    :cond_5
    move v0, v3

    goto :goto_3
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->d:Lcom/google/android/apps/gmm/place/an;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->d:Lcom/google/android/apps/gmm/place/an;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/place/an;->d(Lcom/google/android/apps/gmm/x/o;)V

    .line 301
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method e()V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 367
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/c;->e:Z

    if-nez v1, :cond_1

    .line 369
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/place/ax;->c:Lcom/google/android/apps/gmm/place/ax;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->f:Lcom/google/android/apps/gmm/place/ax;

    .line 374
    :goto_0
    return-void

    .line 373
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->B:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ki;->i()Lcom/google/maps/g/ki;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ki;

    iget-boolean v0, v0, Lcom/google/maps/g/ki;->b:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/place/ax;->a:Lcom/google/android/apps/gmm/place/ax;

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->f:Lcom/google/android/apps/gmm/place/ax;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/place/ax;->b:Lcom/google/android/apps/gmm/place/ax;

    goto :goto_1
.end method

.method f()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 390
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 391
    if-eqz v0, :cond_2

    .line 392
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v3, :cond_2

    .line 393
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    return v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public final g()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aq;->i:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method
