.class Lcom/google/android/apps/gmm/startpage/al;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/startpage/ak;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/startpage/ak;)V
    .locals 0

    .prologue
    .line 863
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/al;->a:Lcom/google/android/apps/gmm/startpage/ak;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 866
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/al;->a:Lcom/google/android/apps/gmm/startpage/ak;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/startpage/ak;->i:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/ak;->d:Lcom/google/android/apps/gmm/startpage/j;

    iget-object v1, v1, Lcom/google/android/apps/gmm/startpage/j;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->q:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1, v2, v9}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    .line 867
    :cond_0
    :goto_0
    return-void

    .line 866
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/startpage/ak;->b:Lcom/google/android/apps/gmm/startpage/y;

    iget-object v3, v0, Lcom/google/android/apps/gmm/startpage/ak;->c:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v4, v0, Lcom/google/android/apps/gmm/startpage/ak;->c:Lcom/google/android/apps/gmm/startpage/ag;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/startpage/ag;->a(Landroid/accounts/Account;)Z

    move-result v4

    iget-object v5, v2, Lcom/google/android/apps/gmm/startpage/y;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/gmm/directions/f/d/f;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/maps/g/a/hm;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/gmm/startpage/u;->a(Lcom/google/maps/g/a/hm;)Lcom/google/o/h/a/dq;

    move-result-object v5

    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v6

    sget-object v7, Lcom/google/o/h/a/dq;->b:Lcom/google/o/h/a/dq;

    invoke-virtual {v6, v7}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    move-result-object v6

    sget-object v7, Lcom/google/o/h/a/dq;->j:Lcom/google/o/h/a/dq;

    invoke-virtual {v6, v7}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    move-result-object v6

    if-eqz v5, :cond_2

    invoke-virtual {v6, v5}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :cond_2
    sget-object v5, Lcom/google/o/h/a/dq;->n:Lcom/google/o/h/a/dq;

    invoke-virtual {v6, v5}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    invoke-virtual {v6}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/gmm/startpage/af;->b:Lcom/google/android/apps/gmm/startpage/af;

    sget-object v7, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    iget-object v8, v2, Lcom/google/android/apps/gmm/startpage/y;->c:Landroid/content/Context;

    invoke-static {v8}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v8

    invoke-static {v1, v5, v6, v7, v8}, Lcom/google/android/apps/gmm/startpage/aa;->a(Landroid/accounts/Account;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/startpage/af;Lcom/google/android/apps/gmm/startpage/d/e;Z)Lcom/google/android/apps/gmm/startpage/ac;

    move-result-object v1

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/apps/gmm/startpage/y;->a(Lcom/google/android/apps/gmm/startpage/ac;Lcom/google/android/apps/gmm/startpage/ae;Z)Lcom/google/android/apps/gmm/startpage/aa;

    move-result-object v1

    if-eqz v1, :cond_0

    iput-boolean v9, v0, Lcom/google/android/apps/gmm/startpage/ak;->i:Z

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 870
    const-string v0, "SendPrefetchTask"

    return-object v0
.end method
