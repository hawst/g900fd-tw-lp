.class Lcom/google/android/apps/gmm/streetview/internal/ah;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/streetview/internal/aj;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/streetview/internal/ac;

.field private final b:Lcom/google/android/apps/gmm/streetview/internal/ae;

.field private final c:Lcom/google/android/apps/gmm/streetview/internal/ag;

.field private final d:Ljava/lang/Object;

.field private e:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/streetview/internal/ac;Lcom/google/android/apps/gmm/streetview/internal/ae;Lcom/google/android/apps/gmm/streetview/internal/ag;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->a:Lcom/google/android/apps/gmm/streetview/internal/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->e:I

    .line 190
    iput-object p2, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->b:Lcom/google/android/apps/gmm/streetview/internal/ae;

    .line 191
    iput-object p3, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->c:Lcom/google/android/apps/gmm/streetview/internal/ag;

    .line 192
    iput-object p4, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->d:Ljava/lang/Object;

    .line 193
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->b:Lcom/google/android/apps/gmm/streetview/internal/ae;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->e:I

    if-eq v0, v2, :cond_0

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->b:Lcom/google/android/apps/gmm/streetview/internal/ae;

    const/4 v1, 0x0

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/streetview/internal/ae;->a(ZLcom/google/android/apps/gmm/streetview/internal/t;)V

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->a:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/ac;->a(Ljava/lang/Object;)V

    .line 274
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/streetview/internal/ai;)V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->a:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/ac;->b:Lcom/google/android/apps/gmm/streetview/internal/af;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->a:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/ac;->b:Lcom/google/android/apps/gmm/streetview/internal/af;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/streetview/internal/af;->a(Lcom/google/android/apps/gmm/streetview/internal/ai;)V

    .line 281
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/streetview/internal/t;Lcom/google/e/a/a/a/b;)V
    .locals 3

    .prologue
    .line 199
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->e:I

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->b:Lcom/google/android/apps/gmm/streetview/internal/ae;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->b:Lcom/google/android/apps/gmm/streetview/internal/ae;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/gmm/streetview/internal/ae;->a(ZLcom/google/android/apps/gmm/streetview/internal/t;)V

    .line 210
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/gmm/streetview/internal/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 212
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 213
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {p2, v1}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 219
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->a:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v2, v2, Lcom/google/android/apps/gmm/streetview/internal/ac;->a:Lcom/google/android/apps/gmm/streetview/internal/g;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/gmm/streetview/internal/g;->a([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    :goto_0
    return-void

    .line 223
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 221
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;III[B)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 237
    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/z;

    invoke-direct {v0, p1, p3, p4, p2}, Lcom/google/android/apps/gmm/streetview/internal/z;-><init>(Ljava/lang/String;III)V

    .line 240
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->c:Lcom/google/android/apps/gmm/streetview/internal/ag;

    if-eqz v1, :cond_0

    .line 241
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 242
    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 243
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 245
    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 246
    const/4 v2, 0x0

    array-length v3, p5

    invoke-static {p5, v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 248
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->c:Lcom/google/android/apps/gmm/streetview/internal/ag;

    invoke-interface {v2, v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/ag;->a(Lcom/google/android/apps/gmm/streetview/internal/z;Landroid/graphics/Bitmap;)V

    .line 253
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->a:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/ac;->a:Lcom/google/android/apps/gmm/streetview/internal/g;

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/z;->a:Ljava/lang/String;

    iget v3, v0, Lcom/google/android/apps/gmm/streetview/internal/z;->d:I

    iget v4, v0, Lcom/google/android/apps/gmm/streetview/internal/z;->b:I

    iget v5, v0, Lcom/google/android/apps/gmm/streetview/internal/z;->c:I

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x29

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "tile_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "_"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p5, v2}, Lcom/google/android/apps/gmm/streetview/internal/g;->a([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 262
    :goto_0
    return-void

    .line 255
    :catch_0
    move-exception v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x18

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "PM failed to cache tile "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 257
    :catch_1
    move-exception v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "PM was interrupted caching tile "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->a:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ah;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/ac;->a(Ljava/lang/Object;)V

    .line 286
    return-void
.end method
