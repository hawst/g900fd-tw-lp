.class public final enum Lcom/google/android/apps/gmm/p/b/l;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/p/b/l;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/p/b/l;

.field public static final enum b:Lcom/google/android/apps/gmm/p/b/l;

.field public static final enum c:Lcom/google/android/apps/gmm/p/b/l;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/p/b/l;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/p/b/l;

    const-string v1, "UPDATE_FREQUENCY_NONE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/apps/gmm/p/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/p/b/l;->a:Lcom/google/android/apps/gmm/p/b/l;

    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/p/b/l;

    const-string v1, "UPDATE_FREQUENCY_SLOW"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/p/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/p/b/l;->b:Lcom/google/android/apps/gmm/p/b/l;

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/p/b/l;

    const-string v1, "UPDATE_FREQUENCY_FAST"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/p/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/p/b/l;->c:Lcom/google/android/apps/gmm/p/b/l;

    .line 41
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/p/b/l;

    sget-object v1, Lcom/google/android/apps/gmm/p/b/l;->a:Lcom/google/android/apps/gmm/p/b/l;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/p/b/l;->b:Lcom/google/android/apps/gmm/p/b/l;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/p/b/l;->c:Lcom/google/android/apps/gmm/p/b/l;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/p/b/l;->e:[Lcom/google/android/apps/gmm/p/b/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    if-nez p3, :cond_0

    const-wide/16 v0, 0x0

    .line 59
    :goto_0
    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/p/b/l;->d:I

    .line 60
    return-void

    .line 58
    :cond_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    .line 59
    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v0

    int-to-long v2, p3

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/p/b/l;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/apps/gmm/p/b/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/b/l;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/p/b/l;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/apps/gmm/p/b/l;->e:[Lcom/google/android/apps/gmm/p/b/l;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/p/b/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/p/b/l;

    return-object v0
.end method
