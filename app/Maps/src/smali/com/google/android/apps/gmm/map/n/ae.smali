.class public Lcom/google/android/apps/gmm/map/n/ae;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/b/c;


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/util/b/g;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/v/ad;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/ad;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/n/ae;->b:Lcom/google/android/apps/gmm/v/ad;

    .line 40
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;II)Lcom/google/android/apps/gmm/map/b/d;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lcom/google/android/apps/gmm/map/n/af;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/ae;->b:Lcom/google/android/apps/gmm/v/ad;

    .line 61
    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    invoke-direct {v0, p1, p2, v1, p3}, Lcom/google/android/apps/gmm/map/n/af;-><init>(Landroid/content/res/Resources;ILcom/google/android/apps/gmm/v/ao;I)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/d;)Lcom/google/android/apps/gmm/map/b/g;
    .locals 12

    .prologue
    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v10, -0x41000000    # -0.5f

    const/4 v9, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    .line 66
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/n/af;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/ae;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    const-string v1, "Creating placemark for renderer is initialized!"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/map/n/af;

    .line 70
    new-instance v0, Lcom/google/android/apps/gmm/map/n/ag;

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->J:Lcom/google/android/apps/gmm/map/t/l;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/n/ag;-><init>(Lcom/google/android/apps/gmm/map/t/k;)V

    .line 72
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/ag;->a:Lcom/google/android/apps/gmm/map/t/q;

    .line 73
    const-string v2, "placemarkIcon"

    iput-object v2, v1, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    .line 74
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/n/af;->a:Lcom/google/android/apps/gmm/v/ar;

    .line 75
    new-instance v3, Lcom/google/android/apps/gmm/v/ci;

    .line 76
    iget v4, p1, Lcom/google/android/apps/gmm/map/n/af;->b:I

    invoke-direct {v3, v2, v4}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    .line 75
    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 77
    new-instance v3, Lcom/google/android/apps/gmm/v/m;

    const/16 v4, 0x303

    invoke-direct {v3, v9, v4}, Lcom/google/android/apps/gmm/v/m;-><init>(II)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 78
    new-instance v3, Lcom/google/android/apps/gmm/v/cd;

    iget v4, p1, Lcom/google/android/apps/gmm/map/n/af;->b:I

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/v/cd;-><init>(I)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 79
    sget-object v3, Lcom/google/android/apps/gmm/map/t/r;->b:Lcom/google/android/apps/gmm/map/t/r;

    iget-boolean v4, v1, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v4, :cond_2

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_2
    iput-object v3, v1, Lcom/google/android/apps/gmm/map/t/q;->h:Lcom/google/android/apps/gmm/map/t/r;

    iput-boolean v9, v1, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    .line 80
    iget v3, v2, Lcom/google/android/apps/gmm/v/ar;->d:I

    int-to-float v3, v3

    mul-float/2addr v3, v6

    iget v4, v2, Lcom/google/android/apps/gmm/v/ar;->c:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 81
    iget v4, v2, Lcom/google/android/apps/gmm/v/ar;->c:I

    int-to-float v4, v4

    mul-float/2addr v4, v6

    iget v5, v2, Lcom/google/android/apps/gmm/v/ar;->e:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    .line 82
    iget v5, v2, Lcom/google/android/apps/gmm/v/ar;->d:I

    int-to-float v5, v5

    mul-float/2addr v5, v6

    iget v6, v2, Lcom/google/android/apps/gmm/v/ar;->f:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    .line 83
    const/16 v6, 0x14

    new-array v6, v6, [F

    const/4 v7, 0x0

    aput v10, v6, v7

    aput v3, v6, v9

    const/4 v7, 0x2

    aput v8, v6, v7

    const/4 v7, 0x3

    aput v8, v6, v7

    const/4 v7, 0x4

    aput v8, v6, v7

    const/4 v7, 0x5

    aput v10, v6, v7

    const/4 v7, 0x6

    aput v8, v6, v7

    const/4 v7, 0x7

    aput v8, v6, v7

    const/16 v7, 0x8

    aput v8, v6, v7

    const/16 v7, 0x9

    aput v5, v6, v7

    const/16 v7, 0xa

    aput v11, v6, v7

    const/16 v7, 0xb

    aput v3, v6, v7

    const/16 v3, 0xc

    aput v8, v6, v3

    const/16 v3, 0xd

    aput v4, v6, v3

    const/16 v3, 0xe

    aput v8, v6, v3

    const/16 v3, 0xf

    aput v11, v6, v3

    const/16 v3, 0x10

    aput v8, v6, v3

    const/16 v3, 0x11

    aput v8, v6, v3

    const/16 v3, 0x12

    aput v4, v6, v3

    const/16 v3, 0x13

    aput v5, v6, v3

    .line 89
    new-instance v3, Lcom/google/android/apps/gmm/v/av;

    const/16 v4, 0x11

    const/4 v5, 0x5

    invoke-direct {v3, v6, v4, v5}, Lcom/google/android/apps/gmm/v/av;-><init>([FII)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/co;)V

    .line 100
    iget v3, v2, Lcom/google/android/apps/gmm/v/ar;->c:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    .line 101
    iget v4, v2, Lcom/google/android/apps/gmm/v/ar;->d:I

    int-to-float v4, v4

    iget v2, v2, Lcom/google/android/apps/gmm/v/ar;->c:I

    int-to-float v2, v2

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v2, v5

    sub-float v2, v4, v2

    neg-float v2, v2

    .line 104
    new-instance v4, Lcom/google/android/apps/gmm/map/t/u;

    invoke-direct {v4, v3, v8, v2}, Lcom/google/android/apps/gmm/map/t/u;-><init>(FFF)V

    .line 106
    new-instance v2, Lcom/google/android/apps/gmm/map/t/y;

    sget-object v3, Lcom/google/android/apps/gmm/map/j/ag;->a:Lcom/google/android/apps/gmm/map/j/ag;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/n/ae;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    const-string v6, "Placemark icon"

    invoke-direct {v2, v4, v3, v5, v6}, Lcom/google/android/apps/gmm/map/t/y;-><init>(Lcom/google/android/apps/gmm/map/t/x;Lcom/google/android/apps/gmm/map/j/s;Lcom/google/android/apps/gmm/map/util/b/g;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 110
    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/b;)V
    .locals 4

    .prologue
    .line 48
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/b/f;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/ae;->b:Lcom/google/android/apps/gmm/v/ad;

    check-cast p1, Lcom/google/android/apps/gmm/map/b/f;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/b/f;->a()Lcom/google/android/apps/gmm/v/aa;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 50
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/map/b/b;)V
    .locals 4

    .prologue
    .line 54
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/b/f;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/ae;->b:Lcom/google/android/apps/gmm/v/ad;

    check-cast p1, Lcom/google/android/apps/gmm/map/b/f;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/b/f;->a()Lcom/google/android/apps/gmm/v/aa;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 56
    return-void
.end method
