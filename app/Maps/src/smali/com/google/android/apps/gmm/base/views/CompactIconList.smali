.class public Lcom/google/android/apps/gmm/base/views/CompactIconList;
.super Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;
.source "PG"


# instance fields
.field public a:Z

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/base/views/CompactIconList;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 96
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 99
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompactIconList;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/n;->e:[I

    invoke-virtual {v0, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 103
    sget v1, Lcom/google/android/apps/gmm/n;->g:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/CompactIconList;->a:Z

    if-eq v2, v1, :cond_0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/CompactIconList;->a:Z

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompactIconList;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompactIconList;->invalidate()V

    .line 104
    :cond_0
    sget v1, Lcom/google/android/apps/gmm/n;->f:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/CompactIconList;->e:Z

    if-eq v2, v1, :cond_1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/CompactIconList;->e:Z

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompactIconList;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompactIconList;->invalidate()V

    .line 105
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 106
    return-void
.end method

.method private c()Landroid/view/View;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->c:Lcom/google/b/c/cv;

    .line 218
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method


# virtual methods
.method protected final a(I)I
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/CompactIconList;->e:Z

    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/CompactIconList;->b(I)I

    move-result v0

    .line 148
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->a(I)I

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 160
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->onLayout(ZIIII)V

    .line 162
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/CompactIconList;->a:Z

    if-eqz v2, :cond_8

    .line 163
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompactIconList;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompactIconList;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompactIconList;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->c:Lcom/google/b/c/cv;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_8

    invoke-static {p0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_3

    if-eqz v5, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/CompactIconList;->c()Landroid/view/View;

    move-result-object v0

    :cond_0
    :goto_0
    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    sub-int/2addr v0, v2

    move v2, v0

    :goto_2
    move v3, v1

    :goto_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_8

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    add-int/lit8 v1, v3, 0x1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    mul-int/2addr v1, v2

    div-int/2addr v1, v6

    if-eqz v5, :cond_7

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v6

    sub-int v1, v6, v1

    :goto_4
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v7

    add-int/2addr v7, v1

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v8

    invoke-virtual {v0, v1, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->c:Lcom/google/b/c/cv;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    goto :goto_1

    :cond_3
    if-eqz v5, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->c:Lcom/google/b/c/cv;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_4

    :goto_5
    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    sub-int v0, v3, v0

    move v2, v0

    goto :goto_2

    :cond_4
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_5

    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/CompactIconList;->c()Landroid/view/View;

    move-result-object v0

    goto :goto_5

    :cond_6
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    goto :goto_6

    :cond_7
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v6

    add-int/2addr v1, v6

    goto :goto_4

    .line 165
    :cond_8
    return-void
.end method
