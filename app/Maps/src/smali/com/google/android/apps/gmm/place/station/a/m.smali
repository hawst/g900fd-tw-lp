.class public Lcom/google/android/apps/gmm/place/station/a/m;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/place/station/b/g;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/place/station/a/m;->a:Lcom/google/android/libraries/curvular/bk;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    .line 182
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/place/station/b/g;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 138
    new-instance v0, Lcom/google/android/libraries/curvular/bc;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bc;-><init>()V

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/place/station/a/m;->a(Lcom/google/android/apps/gmm/place/station/b/g;Lcom/google/android/libraries/curvular/bc;)V

    iget-object v2, v0, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    iget-object v0, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/place/station/b/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/f;->i()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    :goto_1
    if-gez v1, :cond_3

    .line 147
    :cond_0
    :goto_2
    return-void

    .line 138
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v1, -0x1

    goto :goto_1

    .line 142
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/place/station/a/m;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {p1, v0}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 144
    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    goto :goto_2
.end method

.method private static a(Lcom/google/android/apps/gmm/place/station/b/g;Lcom/google/android/libraries/curvular/bc;)V
    .locals 2

    .prologue
    .line 102
    invoke-interface {p0}, Lcom/google/android/apps/gmm/place/station/b/g;->a()Lcom/google/android/apps/gmm/place/station/b/f;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 103
    const-class v0, Lcom/google/android/apps/gmm/place/station/a/h;

    .line 104
    invoke-interface {p0}, Lcom/google/android/apps/gmm/place/station/b/g;->a()Lcom/google/android/apps/gmm/place/station/b/f;

    move-result-object v1

    .line 103
    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 106
    :cond_0
    const-class v0, Lcom/google/android/apps/gmm/place/station/a/i;

    .line 107
    invoke-interface {p0}, Lcom/google/android/apps/gmm/place/station/b/g;->c()Ljava/util/List;

    move-result-object v1

    .line 106
    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Ljava/util/List;)V

    .line 108
    invoke-interface {p0}, Lcom/google/android/apps/gmm/place/station/b/g;->b()Lcom/google/android/apps/gmm/place/station/b/f;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 109
    const-class v0, Lcom/google/android/apps/gmm/place/station/a/j;

    invoke-interface {p0}, Lcom/google/android/apps/gmm/place/station/b/g;->b()Lcom/google/android/apps/gmm/place/station/b/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 111
    :cond_1
    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v7, -0x1

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 55
    const/4 v0, 0x6

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 56
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v10

    .line 57
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v11

    .line 58
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v12

    .line 59
    sget v0, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v13

    const/4 v2, 0x4

    const-class v3, Lcom/google/android/apps/gmm/place/station/a/p;

    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    .line 61
    new-instance v4, Lcom/google/android/libraries/curvular/ao;

    const/4 v5, 0x0

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-direct {v4, v3, v0}, Lcom/google/android/libraries/curvular/ao;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ah;)V

    aput-object v4, v1, v2

    const/4 v2, 0x5

    new-array v3, v13, [Lcom/google/android/libraries/curvular/cu;

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/a/m;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v0

    new-array v4, v13, [Lcom/google/android/libraries/curvular/cu;

    sget-object v5, Lcom/google/android/apps/gmm/place/station/a/m;->a:Lcom/google/android/libraries/curvular/bk;

    .line 68
    sget-object v6, Lcom/google/android/libraries/curvular/g;->Z:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v10

    .line 69
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v11

    .line 70
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v12

    .line 66
    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/base/k/ao;->a(Lcom/google/android/libraries/curvular/am;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v3, v10

    const/4 v0, 0x4

    new-array v4, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 74
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v5, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v10

    const/4 v0, -0x2

    .line 75
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v5, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v11

    const-wide/high16 v6, 0x4034000000000000L    # 20.0

    .line 76
    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v6, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v5, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    aput-object v5, v0, v10

    aput-object v5, v0, v11

    aput-object v5, v0, v12

    aput-object v5, v0, v13

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bg:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v12

    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/station/b/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/g;->f()Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    const/16 v6, 0x8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v13

    .line 73
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v4}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v4, "android.widget.ProgressBar"

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v3, v11

    new-array v0, v10, [Lcom/google/android/libraries/curvular/cu;

    .line 79
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/k/aa;->f([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v3, v12

    .line 65
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v3}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.FrameLayout"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v2

    .line 55
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.LinearLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    .line 76
    :cond_0
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v8

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 2

    .prologue
    .line 50
    check-cast p2, Lcom/google/android/apps/gmm/place/station/b/g;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/station/b/g;->f()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/station/b/g;->g()Lcom/google/android/apps/gmm/base/l/a/k;

    move-result-object v0

    if-eqz v0, :cond_1

    const-class v1, Lcom/google/android/apps/gmm/base/f/w;

    invoke-virtual {p4, v1, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p2, p4}, Lcom/google/android/apps/gmm/place/station/a/m;->a(Lcom/google/android/apps/gmm/place/station/b/g;Lcom/google/android/libraries/curvular/bc;)V

    const-class v0, Lcom/google/android/apps/gmm/place/station/a/o;

    new-instance v1, Lcom/google/android/apps/gmm/place/station/a/n;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/place/station/a/n;-><init>(Lcom/google/android/apps/gmm/place/station/a/m;)V

    invoke-virtual {p4, v0, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    const-class v0, Lcom/google/android/apps/gmm/place/station/t;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/station/b/g;->d()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p4, v0, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Ljava/util/List;)V

    goto :goto_0
.end method
