.class Lcom/google/android/apps/gmm/feedback/r;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/google/android/apps/gmm/feedback/r;->a:Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/j/z;)V
    .locals 10
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/r;->a:Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    :goto_0
    return-void

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/r;->a:Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/j/z;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/q;

    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v3

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v8

    mul-double/2addr v4, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v6

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iput-object v2, v0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/r;->a:Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/r;->a:Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/r;->a:Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;

    .line 37
    iget-object v2, v2, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    sget-object v3, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    const/4 v4, 0x1

    .line 36
    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/ag;Z)Lcom/google/android/apps/gmm/map/b/b;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;->f:Lcom/google/android/apps/gmm/map/b/a;

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/r;->a:Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;->a(Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;Z)V

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/r;->a:Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;

    invoke-static {v0}, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;->a(Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;)V

    goto :goto_0
.end method
