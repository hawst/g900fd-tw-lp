.class public Lcom/google/android/apps/gmm/z/b/t;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/gmm/z/b/t;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/z/b/t;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/b/f/aj;
    .locals 8
    .param p0    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 64
    invoke-static {}, Lcom/google/b/f/ah;->newBuilder()Lcom/google/b/f/aj;

    move-result-object v1

    .line 66
    if-eqz p0, :cond_6

    .line 67
    invoke-static {p0}, Lcom/google/android/apps/gmm/z/b/t;->a(Ljava/lang/String;)Lcom/google/b/f/p;

    move-result-object v5

    .line 68
    if-eqz v5, :cond_2

    .line 69
    if-nez v5, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, v1, Lcom/google/b/f/aj;->a:I

    if-eq v0, v3, :cond_1

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v1, Lcom/google/b/f/aj;->b:Ljava/lang/Object;

    iput v3, v1, Lcom/google/b/f/aj;->a:I

    :cond_1
    iget-object v0, v1, Lcom/google/b/f/aj;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v4, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    move v0, v3

    .line 82
    :goto_0
    if-eqz p1, :cond_9

    .line 83
    invoke-static {p1}, Lcom/google/android/apps/gmm/z/b/t;->c(Ljava/lang/String;)Lcom/google/b/f/b;

    move-result-object v4

    .line 84
    if-eqz v4, :cond_9

    .line 85
    if-nez v4, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 72
    :cond_2
    invoke-static {p0}, Lcom/google/android/apps/gmm/z/b/t;->b(Ljava/lang/String;)Lcom/google/b/f/l;

    move-result-object v5

    .line 73
    if-eqz v5, :cond_5

    .line 74
    if-nez v5, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget v0, v1, Lcom/google/b/f/aj;->a:I

    if-eq v0, v6, :cond_4

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v1, Lcom/google/b/f/aj;->b:Ljava/lang/Object;

    iput v6, v1, Lcom/google/b/f/aj;->a:I

    :cond_4
    iget-object v0, v1, Lcom/google/b/f/aj;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v4, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    move v0, v3

    .line 75
    goto :goto_0

    .line 77
    :cond_5
    sget-object v5, Lcom/google/android/apps/gmm/z/b/t;->a:Ljava/lang/String;

    const-string v6, "Invalid string specified for data element EI: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v6, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v5, v0, v6}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_6
    move v0, v4

    goto :goto_0

    :cond_7
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 85
    :cond_8
    iget-object v0, v1, Lcom/google/b/f/aj;->d:Lcom/google/n/ao;

    iget-object v5, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/b/f/aj;->c:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v1, Lcom/google/b/f/aj;->c:I

    move v0, v3

    .line 90
    :cond_9
    if-eqz v0, :cond_a

    move-object v0, v1

    :goto_2
    return-object v0

    :cond_a
    move-object v0, v2

    goto :goto_2
.end method

.method public static a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/b/f/co;
    .locals 7
    .param p0    # Lcom/google/android/apps/gmm/z/b/l;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 99
    if-nez p0, :cond_0

    .line 115
    :goto_0
    return-object v0

    .line 103
    :cond_0
    invoke-static {}, Lcom/google/b/f/cm;->newBuilder()Lcom/google/b/f/co;

    move-result-object v1

    .line 105
    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    array-length v2, v2

    if-gtz v2, :cond_4

    :cond_1
    move-object v2, v0

    .line 106
    :goto_1
    if-eqz v2, :cond_2

    .line 107
    invoke-interface {v2}, Lcom/google/b/f/cq;->a()I

    move-result v2

    iget v3, v1, Lcom/google/b/f/co;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/google/b/f/co;->a:I

    iput v2, v1, Lcom/google/b/f/co;->b:I

    .line 110
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/z/b/t;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/b/f/aj;

    move-result-object v2

    .line 111
    if-eqz v2, :cond_3

    .line 112
    invoke-static {}, Lcom/google/b/f/g;->newBuilder()Lcom/google/b/f/i;

    move-result-object v3

    iget-object v4, v3, Lcom/google/b/f/i;->b:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/b/f/aj;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v4, Lcom/google/n/ao;->d:Z

    iget v2, v3, Lcom/google/b/f/i;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v3, Lcom/google/b/f/i;->a:I

    iget-object v2, v1, Lcom/google/b/f/co;->d:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/b/f/i;->g()Lcom/google/n/t;

    move-result-object v3

    iget-object v4, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/b/f/co;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v1, Lcom/google/b/f/co;->a:I

    :cond_3
    move-object v0, v1

    .line 115
    goto :goto_0

    .line 105
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;)Lcom/google/b/f/p;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 133
    invoke-static {}, Lcom/google/b/f/p;->newBuilder()Lcom/google/b/f/r;

    move-result-object v1

    .line 135
    :try_start_0
    invoke-static {}, Lcom/google/b/e/a;->d()Lcom/google/b/e/a;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/b/e/a;->a(Ljava/lang/CharSequence;)[B

    move-result-object v2

    .line 136
    const/4 v3, 0x0

    array-length v4, v2

    invoke-static {v2, v3, v4}, Lcom/google/n/j;->a([BII)Lcom/google/n/j;

    move-result-object v2

    .line 140
    invoke-virtual {v2}, Lcom/google/n/j;->n()I

    move-result v3

    int-to-long v4, v3

    .line 141
    invoke-virtual {v2}, Lcom/google/n/j;->l()I

    move-result v3

    .line 142
    const-wide v6, 0xffffffffL

    and-long/2addr v4, v6

    const-wide/32 v6, 0xf4240

    mul-long/2addr v4, v6

    int-to-long v6, v3

    add-long/2addr v4, v6

    .line 147
    invoke-virtual {v2}, Lcom/google/n/j;->l()I

    move-result v3

    .line 148
    shr-int/lit8 v6, v3, 0x18

    add-int/lit8 v6, v6, 0xa

    and-int/lit16 v6, v6, 0xff

    .line 149
    shl-int/lit8 v6, v6, 0x18

    const v7, 0xffffff

    and-int/2addr v3, v7

    or-int/2addr v3, v6

    .line 152
    invoke-virtual {v2}, Lcom/google/n/j;->l()I

    move-result v2

    .line 154
    iget v6, v1, Lcom/google/b/f/r;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v1, Lcom/google/b/f/r;->a:I

    iput-wide v4, v1, Lcom/google/b/f/r;->b:J

    .line 155
    iget v4, v1, Lcom/google/b/f/r;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v1, Lcom/google/b/f/r;->a:I

    iput v3, v1, Lcom/google/b/f/r;->c:I

    .line 156
    iget v3, v1, Lcom/google/b/f/r;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v1, Lcom/google/b/f/r;->a:I

    iput v2, v1, Lcom/google/b/f/r;->d:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    invoke-virtual {v1}, Lcom/google/b/f/r;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/p;

    :goto_0
    return-object v0

    .line 160
    :catch_0
    move-exception v1

    goto :goto_0

    .line 158
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static a(Lcom/google/b/f/b;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 123
    if-nez p0, :cond_0

    .line 124
    const-string v0, ""

    .line 127
    :goto_0
    return-object v0

    .line 126
    :cond_0
    const-string v0, "0"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 127
    invoke-static {}, Lcom/google/b/e/a;->d()Lcom/google/b/e/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/e/a;->b()Lcom/google/b/e/a;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/b/f/b;->l()[B

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, v1

    check-cast v0, [B

    const/4 v4, 0x0

    array-length v1, v1

    invoke-virtual {v3, v0, v4, v1}, Lcom/google/b/e/a;->a([BII)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 47
    if-nez p0, :cond_0

    .line 48
    const-string p0, ""

    .line 50
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 52
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Lcom/google/b/f/l;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 168
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 195
    :cond_0
    :goto_0
    return-object v0

    .line 172
    :cond_1
    const-string v1, ":"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 173
    array-length v2, v1

    if-gt v2, v7, :cond_0

    .line 177
    invoke-static {}, Lcom/google/b/f/l;->newBuilder()Lcom/google/b/f/n;

    move-result-object v2

    .line 178
    aget-object v3, v1, v4

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 179
    aget-object v3, v1, v4

    invoke-static {v3}, Lcom/google/android/apps/gmm/z/b/t;->a(Ljava/lang/String;)Lcom/google/b/f/p;

    move-result-object v3

    .line 180
    if-eqz v3, :cond_0

    .line 184
    if-nez v3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v4, v2, Lcom/google/b/f/n;->b:Lcom/google/n/ao;

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v4, Lcom/google/n/ao;->d:Z

    iget v3, v2, Lcom/google/b/f/n;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/b/f/n;->a:I

    .line 187
    :cond_3
    array-length v3, v1

    if-ne v3, v7, :cond_4

    aget-object v3, v1, v6

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 189
    const/4 v3, 0x1

    :try_start_0
    aget-object v1, v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v4, v1

    iget v1, v2, Lcom/google/b/f/n;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v2, Lcom/google/b/f/n;->a:I

    iput-wide v4, v2, Lcom/google/b/f/n;->c:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :cond_4
    invoke-virtual {v2}, Lcom/google/b/f/n;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/l;

    goto :goto_0

    .line 191
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Lcom/google/b/f/b;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x1

    const/4 v0, 0x0

    .line 201
    const-string v1, "1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 202
    invoke-static {}, Lcom/google/b/f/b;->newBuilder()Lcom/google/b/f/d;

    move-result-object v3

    .line 203
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 204
    array-length v5, v4

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_2

    aget-object v6, v4, v1

    .line 205
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x3

    if-lt v7, v8, :cond_0

    invoke-virtual {v6, v9}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x3a

    if-eq v7, v8, :cond_1

    .line 247
    :cond_0
    :goto_1
    return-object v0

    .line 208
    :cond_1
    invoke-virtual {v6, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 209
    const/4 v8, 0x2

    :try_start_0
    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v6

    .line 215
    sparse-switch v7, :sswitch_data_0

    goto :goto_1

    .line 226
    :sswitch_0
    iget v7, v3, Lcom/google/b/f/d;->a:I

    or-int/lit8 v7, v7, 0x10

    iput v7, v3, Lcom/google/b/f/d;->a:I

    iput v6, v3, Lcom/google/b/f/d;->e:I

    .line 204
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 217
    :sswitch_1
    iget v7, v3, Lcom/google/b/f/d;->a:I

    or-int/lit8 v7, v7, 0x1

    iput v7, v3, Lcom/google/b/f/d;->a:I

    iput v6, v3, Lcom/google/b/f/d;->b:I

    goto :goto_2

    .line 220
    :sswitch_2
    iget v7, v3, Lcom/google/b/f/d;->a:I

    or-int/lit8 v7, v7, 0x4

    iput v7, v3, Lcom/google/b/f/d;->a:I

    iput v6, v3, Lcom/google/b/f/d;->c:I

    goto :goto_2

    .line 223
    :sswitch_3
    iget v7, v3, Lcom/google/b/f/d;->a:I

    or-int/lit8 v7, v7, 0x8

    iput v7, v3, Lcom/google/b/f/d;->a:I

    iput v6, v3, Lcom/google/b/f/d;->d:I

    goto :goto_2

    .line 229
    :sswitch_4
    iget v7, v3, Lcom/google/b/f/d;->a:I

    or-int/lit8 v7, v7, 0x20

    iput v7, v3, Lcom/google/b/f/d;->a:I

    iput v6, v3, Lcom/google/b/f/d;->f:I

    goto :goto_2

    .line 232
    :sswitch_5
    iget v7, v3, Lcom/google/b/f/d;->a:I

    or-int/lit8 v7, v7, 0x40

    iput v7, v3, Lcom/google/b/f/d;->a:I

    iput v6, v3, Lcom/google/b/f/d;->g:I

    goto :goto_2

    .line 238
    :cond_2
    invoke-virtual {v3}, Lcom/google/b/f/d;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b;

    goto :goto_1

    .line 240
    :cond_3
    const-string v1, "0"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    const/4 v1, 0x1

    .line 245
    :try_start_1
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 244
    invoke-static {v1}, Lcom/google/b/f/b;->a([B)Lcom/google/b/f/b;
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 247
    :catch_0
    move-exception v1

    goto :goto_1

    .line 213
    :catch_1
    move-exception v1

    goto :goto_1

    .line 215
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_0
        0x69 -> :sswitch_1
        0x72 -> :sswitch_4
        0x73 -> :sswitch_5
        0x74 -> :sswitch_2
        0x79 -> :sswitch_3
    .end sparse-switch
.end method
