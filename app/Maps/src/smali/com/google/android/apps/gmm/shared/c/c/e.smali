.class public Lcom/google/android/apps/gmm/shared/c/c/e;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static b:Ljava/util/regex/Pattern;


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    const-string v0, "%(\\d+\\$)?([-#+ 0,(\\<]*)?(\\d+)?(\\.\\d+)?([tT])?([a-zA-Z%])"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/c/e;->b:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    .line 110
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)Landroid/text/Spannable;
    .locals 5

    .prologue
    .line 657
    new-instance v0, Landroid/text/SpannableStringBuilder;

    const-string v1, "\u00a0"

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 658
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/g;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/shared/c/c/g;-><init>(Landroid/graphics/drawable/Drawable;)V

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 659
    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/c/c/d;Ljava/lang/String;)Landroid/text/Spannable;
    .locals 4

    .prologue
    .line 197
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 198
    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v0, p0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 199
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;FF)Landroid/text/Spannable;
    .locals 5

    .prologue
    .line 173
    const-string v0, "\u00a0"

    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/d;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/gmm/shared/c/c/d;-><init>(Landroid/graphics/drawable/Drawable;FF)V

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v4, 0x21

    invoke-virtual {v2, v1, v3, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v2
.end method

.method public final a(II)Lcom/google/android/apps/gmm/shared/c/c/h;
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 149
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/h;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    return-object v1
.end method
