.class public Lcom/google/android/apps/gmm/map/internal/c/bt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:J

.field public b:J

.field public final c:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public final d:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field public e:I

.field public f:I

.field public final g:Z

.field public final h:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 8

    .prologue
    const-wide/16 v4, -0x1

    .line 34
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/internal/c/bt;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJ)V

    .line 36
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJ)V
    .locals 9

    .prologue
    .line 40
    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/bt;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJZ)V

    .line 42
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJZ)V
    .locals 11

    .prologue
    .line 46
    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/internal/c/bt;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJZI)V

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJZI)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    .line 27
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    .line 52
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/bt;->d:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 53
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 54
    iput-wide p3, p0, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    .line 55
    iput-wide p5, p0, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    .line 56
    iput-boolean p7, p0, Lcom/google/android/apps/gmm/map/internal/c/bt;->g:Z

    .line 57
    iput p8, p0, Lcom/google/android/apps/gmm/map/internal/c/bt;->h:I

    .line 58
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/shared/net/ad;)Z
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    return v0
.end method
