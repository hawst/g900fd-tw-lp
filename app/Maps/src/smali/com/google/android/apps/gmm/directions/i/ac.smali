.class public Lcom/google/android/apps/gmm/directions/i/ac;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/l;


# instance fields
.field private a:Lcom/google/android/apps/gmm/base/activities/c;

.field private b:Lcom/google/android/apps/gmm/directions/i/ad;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/directions/i/ad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/ac;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 32
    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/i/ac;->c:Ljava/lang/String;

    .line 33
    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/i/ac;->d:Ljava/lang/String;

    .line 34
    iput-object p5, p0, Lcom/google/android/apps/gmm/directions/i/ac;->e:Ljava/lang/String;

    .line 35
    iput-object p6, p0, Lcom/google/android/apps/gmm/directions/i/ac;->f:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/i/ac;->b:Lcom/google/android/apps/gmm/directions/i/ad;

    .line 37
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ac;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ac;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ac;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ac;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 72
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ac;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ac;->b:Lcom/google/android/apps/gmm/directions/i/ad;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ac;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 89
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/ac;->b:Lcom/google/android/apps/gmm/directions/i/ad;

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/directions/i/ad;->a(Lcom/google/android/apps/gmm/map/b/a/q;)V

    .line 91
    :cond_0
    return-object v1

    .line 88
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_0
.end method

.method public final k()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/google/b/f/t;->aD:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/google/b/f/t;->aE:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method
