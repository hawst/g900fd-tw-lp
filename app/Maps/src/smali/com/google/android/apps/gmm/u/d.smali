.class public Lcom/google/android/apps/gmm/u/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/people/model/a;


# instance fields
.field private final a:J

.field private final b:Ljava/lang/String;

.field private final c:Z

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Z

.field private final k:Z

.field private final l:J

.field private final m:J

.field private final n:I

.field private final o:J

.field private final p:Z

.field private final q:Z

.field private final r:Z

.field private final s:Z

.field private final t:I

.field private final u:Ljava/lang/String;

.field private final v:Ljava/lang/String;

.field private final w:I

.field private final x:I

.field private final y:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/model/a;)V
    .locals 2

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/u/d;->a:J

    .line 109
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/u/d;->b:Ljava/lang/String;

    .line 110
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/u/d;->c:Z

    .line 111
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/u/d;->d:Ljava/lang/String;

    .line 112
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/u/d;->e:Ljava/lang/String;

    .line 113
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/u/d;->f:Ljava/lang/String;

    .line 114
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/u/d;->g:Ljava/lang/String;

    .line 115
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/u/d;->h:Ljava/lang/String;

    .line 116
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/u/d;->i:Ljava/lang/String;

    .line 117
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/u/d;->j:Z

    .line 118
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->k()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/u/d;->k:Z

    .line 119
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->l()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/u/d;->l:J

    .line 120
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->m()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/u/d;->m:J

    .line 121
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->n()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/u/d;->n:I

    .line 122
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->o()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/u/d;->o:J

    .line 123
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->p()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/u/d;->p:Z

    .line 124
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/u/d;->q:Z

    .line 125
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->r()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/u/d;->r:Z

    .line 126
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->s()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/u/d;->s:Z

    .line 127
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->t()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/u/d;->t:I

    .line 128
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/u/d;->u:Ljava/lang/String;

    .line 129
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/u/d;->v:Ljava/lang/String;

    .line 130
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->w()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/u/d;->w:I

    .line 131
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->x()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/u/d;->x:I

    .line 132
    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/u/d;->y:Ljava/lang/String;

    .line 133
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 138
    iget-wide v0, p0, Lcom/google/android/apps/gmm/u/d;->a:J

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/u/d;->c:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/d;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/d;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/d;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/d;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/d;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/u/d;->j:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/u/d;->k:Z

    return v0
.end method

.method public final l()J
    .locals 2

    .prologue
    .line 203
    iget-wide v0, p0, Lcom/google/android/apps/gmm/u/d;->l:J

    return-wide v0
.end method

.method public final m()J
    .locals 2

    .prologue
    .line 208
    iget-wide v0, p0, Lcom/google/android/apps/gmm/u/d;->m:J

    return-wide v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcom/google/android/apps/gmm/u/d;->n:I

    return v0
.end method

.method public final o()J
    .locals 2

    .prologue
    .line 218
    iget-wide v0, p0, Lcom/google/android/apps/gmm/u/d;->o:J

    return-wide v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/u/d;->p:Z

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 228
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/u/d;->q:Z

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/u/d;->r:Z

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/u/d;->s:Z

    return v0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/google/android/apps/gmm/u/d;->t:I

    return v0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/d;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/d;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 259
    iget v0, p0, Lcom/google/android/apps/gmm/u/d;->w:I

    return v0
.end method

.method public final x()I
    .locals 1

    .prologue
    .line 264
    iget v0, p0, Lcom/google/android/apps/gmm/u/d;->x:I

    return v0
.end method

.method public final y()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/d;->y:Ljava/lang/String;

    return-object v0
.end method
