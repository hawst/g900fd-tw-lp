.class Lcom/google/android/apps/gmm/map/internal/d/b/aj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/internal/d/b/ai;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/b/ai;)V
    .locals 0

    .prologue
    .line 385
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/aj;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ai;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/aj;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ai;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    new-instance v5, Lcom/google/android/apps/gmm/map/internal/d/b/al;

    invoke-direct {v5, v0, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/al;-><init>(Lcom/google/android/apps/gmm/map/internal/d/b/ai;J)V

    move v0, v1

    :goto_0
    iget-object v2, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    if-ge v0, v2, :cond_5

    :try_start_0
    iget-object v2, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    iget-object v2, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aget v2, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_3

    iget-object v2, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    iget-object v2, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    move v2, v1

    :goto_2
    if-ltz v2, :cond_1

    :try_start_1
    iget-object v3, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    iget-boolean v3, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f:Z

    if-nez v3, :cond_4

    iget-object v3, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-lt v0, v3, :cond_6

    :cond_4
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 389
    :cond_5
    return-void

    .line 388
    :catchall_0
    move-exception v0

    iget-object v1, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0

    :cond_6
    const/16 v3, 0x1f4

    :try_start_2
    invoke-virtual {v4, v0, v2, v3, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(IIILcom/google/android/apps/gmm/map/internal/d/b/y;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    iget-object v3, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    invoke-static {}, Ljava/lang/Thread;->yield()V

    goto :goto_2

    :catchall_1
    move-exception v0

    iget-object v1, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
