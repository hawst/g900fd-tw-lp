.class final enum Lcom/google/android/apps/gmm/mylocation/c/d;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/mylocation/c/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/mylocation/c/d;

.field public static final enum b:Lcom/google/android/apps/gmm/mylocation/c/d;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/mylocation/c/d;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/c/d;

    const-string v1, "VISIBLE"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/mylocation/c/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/c/d;->a:Lcom/google/android/apps/gmm/mylocation/c/d;

    new-instance v0, Lcom/google/android/apps/gmm/mylocation/c/d;

    const-string v1, "INVISIBLE"

    invoke-direct {v0, v1, v4, v3}, Lcom/google/android/apps/gmm/mylocation/c/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/c/d;->b:Lcom/google/android/apps/gmm/mylocation/c/d;

    .line 30
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/gmm/mylocation/c/d;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/c/d;->a:Lcom/google/android/apps/gmm/mylocation/c/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/c/d;->b:Lcom/google/android/apps/gmm/mylocation/c/d;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/c/d;->d:[Lcom/google/android/apps/gmm/mylocation/c/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    iput p3, p0, Lcom/google/android/apps/gmm/mylocation/c/d;->c:I

    .line 37
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/mylocation/c/d;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/apps/gmm/mylocation/c/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/c/d;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/mylocation/c/d;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/c/d;->d:[Lcom/google/android/apps/gmm/mylocation/c/d;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/mylocation/c/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/mylocation/c/d;

    return-object v0
.end method
