.class abstract Lcom/google/android/apps/gmm/directions/be;
.super Lcom/google/android/apps/gmm/directions/v;
.source "PG"


# static fields
.field private static final k:Ljava/lang/String;


# instance fields
.field public final j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/google/android/apps/gmm/directions/be;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/be;->k:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/e;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;ZZ)V
    .locals 0

    .prologue
    .line 46
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/gmm/directions/v;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/e;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Z)V

    .line 47
    iput-boolean p6, p0, Lcom/google/android/apps/gmm/directions/be;->j:Z

    .line 48
    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;Landroid/view/View;Lcom/google/android/apps/gmm/map/r/a/w;)Landroid/widget/ListView;
    .locals 8

    .prologue
    .line 85
    sget v0, Lcom/google/android/apps/gmm/g;->ac:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/be;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/directions/c/e;

    invoke-virtual {v0, v1, v7}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 88
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/bb;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/be;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/be;->h:Lcom/google/android/apps/gmm/directions/h/u;

    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/be;->b()Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/i/bb;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/directions/h/u;ZZZ)V

    .line 91
    invoke-static {v7, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 92
    sget v0, Lcom/google/android/apps/gmm/g;->ab:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    return-object v0
.end method

.method protected final a(Landroid/view/View;Lcom/google/android/apps/gmm/map/r/a/w;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/directions/v;->a(Landroid/view/View;Lcom/google/android/apps/gmm/map/r/a/w;)V

    .line 66
    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 68
    sget v0, Lcom/google/android/apps/gmm/g;->dp:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/be;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/directions/c/aq;

    .line 70
    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/be;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/be;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 72
    iget-object v3, p2, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget v4, p2, Lcom/google/android/apps/gmm/map/r/a/w;->b:I

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/directions/i/be;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;I)V

    sget-object v2, Lcom/google/android/apps/gmm/directions/h/x;->b:Lcom/google/android/apps/gmm/directions/h/x;

    .line 73
    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/i/be;->a:Lcom/google/android/apps/gmm/directions/h/x;

    .line 74
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/be;->j:Z

    if-eqz v2, :cond_1

    .line 75
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/be;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v2, p2}, Lcom/google/android/apps/gmm/directions/i/r;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/directions/i/r;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/i/be;->b:Lcom/google/android/apps/gmm/directions/h/y;

    .line 78
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/i/be;->a()Lcom/google/android/apps/gmm/directions/h/w;

    move-result-object v0

    .line 79
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 80
    return-void
.end method

.method protected final a(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 120
    sget v0, Lcom/google/android/apps/gmm/g;->dp:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 121
    if-nez v2, :cond_0

    .line 122
    sget-object v0, Lcom/google/android/apps/gmm/directions/be;->k:Ljava/lang/String;

    const-string v1, "Could not find sheet header content view."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    :goto_0
    return-void

    .line 128
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/h/w;

    .line 129
    if-eqz p2, :cond_1

    sget-object v1, Lcom/google/android/apps/gmm/directions/h/x;->b:Lcom/google/android/apps/gmm/directions/h/x;

    :goto_1
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/directions/h/w;->a(Lcom/google/android/apps/gmm/directions/h/x;)V

    .line 133
    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0

    .line 129
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/directions/h/x;->c:Lcom/google/android/apps/gmm/directions/h/x;

    goto :goto_1
.end method

.method final b(Lcom/google/android/apps/gmm/map/r/a/w;)Z
    .locals 3

    .prologue
    .line 147
    .line 148
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/be;->e:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/be;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 149
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 150
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    .line 147
    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/directions/g/c;->a(Lcom/google/android/apps/gmm/map/r/a/ao;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/p/b/a;)Z

    move-result v0

    return v0
.end method
