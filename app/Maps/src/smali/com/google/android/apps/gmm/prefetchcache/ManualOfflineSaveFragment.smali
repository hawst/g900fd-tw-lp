.class public Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/prefetchcache/api/d;
.implements Lcom/google/android/apps/gmm/prefetchcache/b/g;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:Lcom/google/r/b/a/a/af;

.field c:Z

.field d:Z

.field private e:Landroid/app/AlertDialog;

.field private f:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/prefetchcache/b/d;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/google/android/apps/gmm/prefetchcache/b/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 60
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->c:Z

    .line 61
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->d:Z

    return-void
.end method

.method static a(Lcom/google/r/b/a/a/af;)Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;
    .locals 3

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;-><init>()V

    .line 71
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 72
    const-string v2, "area_proto"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 74
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->setArguments(Landroid/os/Bundle;)V

    .line 75
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;)V
    .locals 2

    .prologue
    .line 46
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->d:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->p()Lcom/google/android/apps/gmm/prefetchcache/api/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/f;->a()Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->c:Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;I)V
    .locals 6

    .prologue
    .line 46
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->g:Lcom/google/android/apps/gmm/prefetchcache/b/e;

    iput p1, v1, Lcom/google/android/apps/gmm/prefetchcache/b/e;->a:I

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    iget-object v0, v1, Lcom/google/android/apps/gmm/prefetchcache/b/e;->b:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/google/android/apps/gmm/prefetchcache/b/e;->b:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->jm:I

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, v1, Lcom/google/android/apps/gmm/prefetchcache/b/e;->b:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/prefetchcache/b/f;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/prefetchcache/b/f;-><init>(Lcom/google/android/apps/gmm/prefetchcache/b/e;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    const-wide/16 v4, 0x7d0

    invoke-interface {v0, v2, v1, v4, v5}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->f:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->g:Lcom/google/android/apps/gmm/prefetchcache/b/e;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method private i()V
    .locals 5

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 200
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 202
    sget v2, Lcom/google/android/apps/gmm/l;->jD:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->jC:I

    .line 203
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->iX:I

    new-instance v4, Lcom/google/android/apps/gmm/prefetchcache/i;

    invoke-direct {v4, p0}, Lcom/google/android/apps/gmm/prefetchcache/i;-><init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;)V

    .line 204
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->pg:I

    new-instance v4, Lcom/google/android/apps/gmm/prefetchcache/h;

    invoke-direct {v4, p0, v0}, Lcom/google/android/apps/gmm/prefetchcache/h;-><init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 213
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 235
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a(Landroid/app/AlertDialog;)V

    .line 236
    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 245
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->b()V

    .line 249
    :goto_0
    return-void

    .line 248
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/r/b/a/av;->p:Lcom/google/r/b/a/av;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/r/b/a/av;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->b:Lcom/google/r/b/a/a/af;

    iget v0, v0, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v0}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_1
    sget-object v1, Lcom/google/r/b/a/a/ai;->c:Lcom/google/r/b/a/a/ai;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->p()Lcom/google/android/apps/gmm/prefetchcache/api/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->b:Lcom/google/r/b/a/a/af;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/api/f;->a(Lcom/google/r/b/a/a/af;)V

    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->c:Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->p()Lcom/google/android/apps/gmm/prefetchcache/api/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->b:Lcom/google/r/b/a/a/af;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/api/f;->b(Lcom/google/r/b/a/a/af;)V

    goto :goto_1
.end method

.method public final a(ILcom/google/android/apps/gmm/prefetchcache/api/e;)V
    .locals 5

    .prologue
    const/16 v4, 0x64

    const/4 v1, 0x1

    .line 291
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 294
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/api/e;->b:Lcom/google/android/apps/gmm/prefetchcache/api/e;

    if-ne p2, v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    .line 295
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x26

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Handled error while an area is saved: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 294
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error in saving: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/prefetchcache/k;

    invoke-direct {v2, p0, p2}, Lcom/google/android/apps/gmm/prefetchcache/k;-><init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;Lcom/google/android/apps/gmm/prefetchcache/api/e;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    move v0, v1

    goto :goto_1

    .line 298
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->c:Z

    if-nez v0, :cond_4

    if-ne p1, v4, :cond_0

    .line 303
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->c:Z

    if-nez v0, :cond_5

    .line 305
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a()V

    goto :goto_0

    .line 309
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->d:Z

    if-eqz v0, :cond_6

    .line 310
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a:Ljava/lang/String;

    goto :goto_0

    .line 314
    :cond_6
    if-ne p1, v4, :cond_7

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v2, Lcom/google/r/b/a/av;->q:Lcom/google/r/b/a/av;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/r/b/a/av;)Ljava/lang/String;

    .line 316
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->d:Z

    .line 319
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/prefetchcache/j;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/prefetchcache/j;-><init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;I)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto/16 :goto_0
.end method

.method a(Landroid/app/AlertDialog;)V
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->e:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->e:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 444
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a:Ljava/lang/String;

    .line 445
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->e:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 447
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->e:Landroid/app/AlertDialog;

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->e:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 449
    return-void

    .line 443
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 3

    .prologue
    .line 404
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->c:Z

    if-eqz v0, :cond_0

    .line 405
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a:Ljava/lang/String;

    const-string v1, "Network failure dialog isn\'t supported after prefetch started"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 440
    :goto_0
    return-void

    .line 408
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/gmm/l;->iT:I

    .line 409
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->kG:I

    .line 410
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->od:I

    new-instance v2, Lcom/google/android/apps/gmm/prefetchcache/f;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/prefetchcache/f;-><init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;)V

    .line 411
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->bg:I

    new-instance v2, Lcom/google/android/apps/gmm/prefetchcache/o;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/prefetchcache/o;-><init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;)V

    .line 420
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/prefetchcache/n;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/prefetchcache/n;-><init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;)V

    .line 429
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 438
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 439
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a(Landroid/app/AlertDialog;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 458
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 463
    :goto_0
    return-void

    .line 462
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->i()V

    goto :goto_0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/google/b/f/t;->cd:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final k()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 179
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->d:Z

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    :goto_0
    move v0, v1

    .line 188
    :goto_1
    return v0

    .line 181
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->p()Lcom/google/android/apps/gmm/prefetchcache/api/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/f;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->i()V

    goto :goto_0

    .line 184
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a:Ljava/lang/String;

    const-string v2, "Unexpected status. prefetchStarted: %b, isManagerBusy: %b"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->c:Z

    .line 185
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->p()Lcom/google/android/apps/gmm/prefetchcache/api/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/f;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v3, v1

    .line 184
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 186
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->k()Z

    move-result v0

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 80
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 81
    if-eqz p1, :cond_0

    .line 82
    :goto_0
    if-eqz p1, :cond_1

    .line 83
    const-string v0, "area_proto"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->b:Lcom/google/r/b/a/a/af;

    .line 84
    const-string v0, "prefetch_started"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->c:Z

    .line 85
    const-string v0, "prefetch_finished"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->d:Z

    .line 89
    :goto_1
    return-void

    .line 81
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0

    .line 87
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a:Ljava/lang/String;

    const-string v1, "state should not be null"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 153
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->p()Lcom/google/android/apps/gmm/prefetchcache/api/f;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/prefetchcache/api/f;->a(Lcom/google/android/apps/gmm/prefetchcache/api/d;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 156
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 93
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->g:Lcom/google/android/apps/gmm/prefetchcache/b/e;

    if-nez v0, :cond_1

    .line 97
    new-instance v0, Lcom/google/android/apps/gmm/prefetchcache/b/e;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/gmm/prefetchcache/b/e;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/prefetchcache/b/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->g:Lcom/google/android/apps/gmm/prefetchcache/b/e;

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/prefetchcache/a/g;

    .line 99
    invoke-virtual {v0, v1, v5}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->f:Lcom/google/android/libraries/curvular/ae;

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->f:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->g:Lcom/google/android/apps/gmm/prefetchcache/b/e;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->p()Lcom/google/android/apps/gmm/prefetchcache/api/f;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/prefetchcache/api/f;->b(Lcom/google/android/apps/gmm/prefetchcache/api/d;)V

    .line 108
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->f:Lcom/google/android/libraries/curvular/ae;

    .line 109
    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v2, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    .line 110
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 111
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v5, v1, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 112
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v5, v1, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 113
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    const/4 v1, 0x0

    .line 114
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->D:Z

    .line 115
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v4, v1, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    const-class v1, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    .line 116
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    .line 117
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 118
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->c()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    new-instance v1, Lcom/google/android/apps/gmm/prefetchcache/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/prefetchcache/e;-><init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;)V

    .line 119
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 143
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 144
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 160
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 161
    const-string v0, "area_proto"

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->b:Lcom/google/r/b/a/a/af;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 162
    const-string v0, "prefetch_started"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 163
    const-string v0, "prefetch_finished"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 164
    return-void
.end method
