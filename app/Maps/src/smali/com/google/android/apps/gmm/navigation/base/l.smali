.class public Lcom/google/android/apps/gmm/navigation/base/l;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/b/f;


# instance fields
.field a:Lcom/google/android/apps/gmm/navigation/j/a/b;

.field private b:Lcom/google/android/apps/gmm/base/a;

.field private c:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 48
    new-instance v0, Lcom/google/android/apps/gmm/navigation/base/m;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/base/m;-><init>(Lcom/google/android/apps/gmm/navigation/base/l;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/l;->c:Ljava/lang/Object;

    return-void
.end method

.method private e()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 148
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/base/l;->a:Lcom/google/android/apps/gmm/navigation/j/a/b;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/base/l;->a:Lcom/google/android/apps/gmm/navigation/j/a/b;

    .line 149
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private i()Lcom/google/android/apps/gmm/navigation/commonui/c;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 158
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/base/l;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    const-class v1, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/commonui/c;

    .line 163
    :goto_0
    return-object v0

    .line 160
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/base/l;->a:Lcom/google/android/apps/gmm/navigation/j/a/b;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/base/l;->a:Lcom/google/android/apps/gmm/navigation/j/a/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/j/a/b;->b:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v2, :cond_1

    move v2, v0

    :goto_1
    if-eqz v2, :cond_2

    :goto_2
    if-eqz v0, :cond_3

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    const-class v1, Lcom/google/android/apps/gmm/navigation/f/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/commonui/c;

    goto :goto_0

    :cond_1
    move v2, v1

    .line 160
    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    .line 163
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final X_()Z
    .locals 2

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/base/l;->i()Lcom/google/android/apps/gmm/navigation/commonui/c;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/c;->isResumed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 90
    :cond_0
    const/4 v0, 0x0

    .line 98
    :goto_0
    return v0

    .line 92
    :cond_1
    new-instance v1, Lcom/google/android/apps/gmm/navigation/base/n;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/navigation/base/n;-><init>(Lcom/google/android/apps/gmm/navigation/base/l;Lcom/google/android/apps/gmm/navigation/commonui/c;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/commonui/c;->a(Ljava/lang/Runnable;)V

    .line 98
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;)Lcom/google/android/apps/gmm/navigation/b/g;
    .locals 1

    .prologue
    .line 143
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    .line 144
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/ad;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/navigation/navui/ad;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 38
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/l;->b:Lcom/google/android/apps/gmm/base/a;

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/l;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/base/l;->c:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 40
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/f;I)V
    .locals 3

    .prologue
    .line 67
    .line 68
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->a(Lcom/google/android/apps/gmm/map/r/a/f;I)Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 70
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 82
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/base/l;->a:Lcom/google/android/apps/gmm/navigation/j/a/b;

    if-eqz v2, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/base/l;->a:Lcom/google/android/apps/gmm/navigation/j/a/b;

    .line 83
    iget-object v2, v3, Lcom/google/android/apps/gmm/navigation/j/a/b;->b:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-nez v2, :cond_0

    iget-object v2, v3, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    :cond_0
    move v2, v0

    :goto_2
    if-eqz v2, :cond_4

    :goto_3
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ag;)Z
    .locals 4
    .param p1    # Lcom/google/android/apps/gmm/map/r/a/ag;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/l;->a:Lcom/google/android/apps/gmm/navigation/j/a/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/l;->a:Lcom/google/android/apps/gmm/navigation/j/a/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    const-class v3, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    .line 120
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->isResumed()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 121
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/navigation/navui/o;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)V

    move v0, v1

    .line 125
    :goto_2
    return v0

    :cond_0
    move v0, v2

    .line 118
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    .line 125
    goto :goto_2
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/base/l;->i()Lcom/google/android/apps/gmm/navigation/commonui/c;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/c;->isResumed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 105
    :cond_0
    const/4 v0, 0x0

    .line 113
    :goto_0
    return v0

    .line 107
    :cond_1
    new-instance v1, Lcom/google/android/apps/gmm/navigation/base/o;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/navigation/base/o;-><init>(Lcom/google/android/apps/gmm/navigation/base/l;Lcom/google/android/apps/gmm/navigation/commonui/c;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/commonui/c;->a(Ljava/lang/Runnable;)V

    .line 113
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/l;->a:Lcom/google/android/apps/gmm/navigation/j/a/b;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/l;->a:Lcom/google/android/apps/gmm/navigation/j/a/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    const-class v3, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    .line 133
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->isResumed()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 134
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->isResumed()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v4, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v3, v4, :cond_0

    sget-object v4, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v3, v4, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    if-nez v2, :cond_2

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/o;->a(Ljava/lang/Float;)V

    :cond_2
    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c()V

    :cond_3
    move v0, v1

    .line 138
    :goto_2
    return v0

    :cond_4
    move v0, v2

    .line 130
    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v2

    .line 138
    goto :goto_2
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 44
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->h()V

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/l;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/base/l;->c:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 46
    return-void
.end method
