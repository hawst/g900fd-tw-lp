.class public abstract Lcom/google/android/apps/gmm/map/internal/d/c/b/d;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:[B

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;[B)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->a:[B

    .line 162
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;F)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1    # Landroid/content/res/Resources;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->b()Ljava/lang/Object;

    move-result-object v0

    .line 228
    if-nez v0, :cond_0

    .line 229
    const/4 v0, 0x0

    .line 231
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->a(Ljava/lang/Object;Landroid/content/res/Resources;F)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract a(Ljava/lang/Object;Landroid/content/res/Resources;F)Landroid/graphics/drawable/Drawable;
    .param p2    # Landroid/content/res/Resources;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/content/res/Resources;",
            "F)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation
.end method

.method protected abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public b()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->b:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 201
    :goto_0
    if-eqz v0, :cond_1

    .line 219
    :goto_1
    return-object v0

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->a:[B

    if-nez v0, :cond_2

    move-object v0, v1

    .line 205
    goto :goto_1

    .line 207
    :cond_2
    monitor-enter p0

    .line 209
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->b:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_3

    move-object v0, v1

    .line 210
    :goto_2
    if-eqz v0, :cond_4

    .line 211
    monitor-exit p0

    goto :goto_1

    .line 220
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 209
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    .line 214
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->a()Ljava/lang/Object;

    move-result-object v0

    .line 215
    if-nez v0, :cond_5

    .line 216
    monitor-exit p0

    move-object v0, v1

    goto :goto_1

    .line 218
    :cond_5
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->b:Ljava/lang/ref/WeakReference;

    .line 219
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
