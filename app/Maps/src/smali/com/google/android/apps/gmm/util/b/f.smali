.class public Lcom/google/android/apps/gmm/util/b/f;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/util/b/h;",
        ">;"
    }
.end annotation


# direct methods
.method public static varargs a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 71
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v1, 0x0

    .line 72
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    .line 73
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v0, v5

    const/4 v1, 0x2

    .line 75
    new-instance v2, Lcom/google/android/libraries/curvular/c/e;

    sget-object v3, Lcom/google/android/libraries/curvular/c/a;->d:Lcom/google/android/libraries/curvular/c/a;

    invoke-direct {v2, v3}, Lcom/google/android/libraries/curvular/c/e;-><init>(Ljava/lang/Enum;)V

    aput-object v2, v0, v1

    .line 71
    new-instance v1, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-class v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    .line 76
    invoke-virtual {v0, p0, v5}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;Z)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/util/b/h;Lcom/google/android/libraries/curvular/bc;)V
    .locals 4

    .prologue
    .line 31
    const/4 v0, 0x0

    .line 32
    invoke-interface {p0}, Lcom/google/android/apps/gmm/util/b/h;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/util/b/q;

    .line 33
    sget-object v3, Lcom/google/android/apps/gmm/util/b/l;->a:Lcom/google/android/apps/gmm/util/b/ac;

    iget-object v3, v3, Lcom/google/android/apps/gmm/util/b/ac;->e:Lcom/google/android/apps/gmm/util/b/w;

    invoke-interface {v3, v1, v0}, Lcom/google/android/apps/gmm/util/b/w;->a(Lcom/google/android/apps/gmm/util/b/q;Lcom/google/android/apps/gmm/util/b/q;)Ljava/lang/Class;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v3, Lcom/google/android/apps/gmm/util/b/d;->a:Lcom/google/android/libraries/curvular/ce;

    invoke-virtual {p1, v1, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 34
    :cond_0
    invoke-interface {v0}, Lcom/google/android/apps/gmm/util/b/q;->b()Lcom/google/o/h/a/bw;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/gmm/util/b/g;->a:[I

    invoke-virtual {v1}, Lcom/google/o/h/a/bw;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    const-class v1, Lcom/google/android/apps/gmm/util/b/c;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    move-object v1, v0

    .line 37
    goto :goto_0

    .line 34
    :pswitch_0
    const-class v1, Lcom/google/android/apps/gmm/util/b/x;

    goto :goto_1

    :pswitch_1
    const-class v1, Lcom/google/android/apps/gmm/util/b/j;

    goto :goto_1

    :pswitch_2
    const-class v1, Lcom/google/android/apps/gmm/util/b/k;

    goto :goto_1

    .line 38
    :cond_1
    return-void

    .line 34
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 4

    .prologue
    .line 66
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v1, 0x0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/util/b/f;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    .line 66
    invoke-static {v0}, Lcom/google/android/apps/gmm/util/b/f;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 0

    .prologue
    .line 21
    check-cast p2, Lcom/google/android/apps/gmm/util/b/h;

    invoke-static {p2, p4}, Lcom/google/android/apps/gmm/util/b/f;->a(Lcom/google/android/apps/gmm/util/b/h;Lcom/google/android/libraries/curvular/bc;)V

    return-void
.end method
