.class public Lcom/google/android/apps/gmm/map/o/h;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/v/bz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/v/bz",
            "<",
            "Lcom/google/android/apps/gmm/map/o/k;",
            ">;"
        }
    .end annotation
.end field

.field public final b:F

.field public final c:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

.field private final d:Lcom/google/android/apps/gmm/map/o/j;


# direct methods
.method public constructor <init>(IILcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/d/c/a/g;F)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/map/o/j;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/h;->d:Lcom/google/android/apps/gmm/map/o/j;

    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/v/bz;

    const/4 v1, 0x6

    invoke-direct {v0, p3, p1, p2, v1}, Lcom/google/android/apps/gmm/v/bz;-><init>(Lcom/google/android/apps/gmm/v/ad;III)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/h;->a:Lcom/google/android/apps/gmm/v/bz;

    .line 59
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/o/h;->c:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    .line 60
    iput p5, p0, Lcom/google/android/apps/gmm/map/o/h;->b:F

    .line 61
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/d/c/a/g;F)V
    .locals 6

    .prologue
    const/16 v1, 0x200

    .line 51
    move-object v0, p0

    move v2, v1

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/o/h;-><init>(IILcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/d/c/a/g;F)V

    .line 53
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/aa;Lcom/google/android/apps/gmm/map/internal/d/c/a/g;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;
    .locals 10
    .param p3    # Lcom/google/android/apps/gmm/map/internal/d/c/b/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v0, :cond_0

    move-object v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v0, :cond_1

    move-object v0, v2

    :goto_1
    invoke-interface {p1, v0, p2, p3}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v0

    .line 135
    :goto_2
    return-object v0

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    goto :goto_0

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    goto :goto_1

    .line 122
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v0, :cond_3

    move-object v0, v2

    :goto_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v0, :cond_4

    move-object v0, v2

    :goto_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [I

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v0, :cond_5

    move-object v0, v2

    :goto_5
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [I

    move v5, v6

    .line 125
    :goto_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v0, :cond_6

    move-object v0, v2

    :goto_7
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_8

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v0, :cond_7

    move-object v0, v2

    :goto_8
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/q;

    .line 127
    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/q;->a:Ljava/lang/String;

    aput-object v7, v1, v5

    .line 128
    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/q;->d:I

    aput v7, v3, v5

    .line 129
    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/q;->e:I

    aput v0, v4, v5

    .line 125
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_6

    .line 122
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    goto :goto_3

    .line 123
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    goto :goto_4

    .line 124
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    goto :goto_5

    .line 125
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    goto :goto_7

    .line 126
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    goto :goto_8

    .line 131
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    if-eqz v0, :cond_a

    move v0, v8

    :goto_9
    if-eqz v0, :cond_b

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bn;->a:I

    .line 133
    :goto_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    if-eqz v0, :cond_9

    move v6, v8

    :cond_9
    if-eqz v6, :cond_c

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bn;->c:I

    int-to-float v6, v0

    .line 135
    :goto_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/c/p;->c:I

    .line 136
    :goto_c
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    move-object v0, p1

    move-object v8, p2

    move-object v9, p3

    .line 135
    invoke-interface/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->a([Ljava/lang/String;I[I[ILjava/lang/String;FILjava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v0

    goto/16 :goto_2

    :cond_a
    move v0, v6

    .line 131
    goto :goto_9

    :cond_b
    move v7, v6

    .line 132
    goto :goto_a

    .line 134
    :cond_c
    const/4 v6, 0x0

    goto :goto_b

    :cond_d
    move v2, v8

    .line 135
    goto :goto_c
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/o/k;Landroid/graphics/Bitmap;IIIIFZ)Lcom/google/android/apps/gmm/v/by;
    .locals 4

    .prologue
    .line 210
    sub-int v0, p5, p3

    .line 211
    sub-int v1, p6, p4

    .line 212
    int-to-float v0, v0

    mul-float/2addr v0, p7

    float-to-int v0, v0

    .line 213
    int-to-float v1, v1

    mul-float/2addr v1, p7

    float-to-int v1, v1

    .line 214
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/h;->d:Lcom/google/android/apps/gmm/map/o/j;

    iput-object p2, v2, Lcom/google/android/apps/gmm/map/o/j;->d:Landroid/graphics/Bitmap;

    .line 215
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/h;->d:Lcom/google/android/apps/gmm/map/o/j;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/o/j;->b:Landroid/graphics/Rect;

    invoke-virtual {v2, p3, p4, p5, p6}, Landroid/graphics/Rect;->set(IIII)V

    .line 216
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/h;->d:Lcom/google/android/apps/gmm/map/o/j;

    iput-boolean p8, v2, Lcom/google/android/apps/gmm/map/o/j;->e:Z

    .line 217
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/h;->a:Lcom/google/android/apps/gmm/v/bz;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/h;->d:Lcom/google/android/apps/gmm/map/o/j;

    invoke-virtual {v2, p1, v0, v1, v3}, Lcom/google/android/apps/gmm/v/bz;->a(Ljava/lang/Object;IILcom/google/android/apps/gmm/v/cc;)Lcom/google/android/apps/gmm/v/by;

    move-result-object v0

    .line 219
    return-object v0
.end method
