.class public Lcom/google/android/apps/gmm/map/legacy/a/b/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:I

.field final b:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/android/apps/gmm/map/internal/b/e;

.field d:Lcom/google/android/apps/gmm/map/b/a/y;

.field e:J

.field private f:Z

.field private g:I

.field private final h:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/google/android/apps/gmm/map/legacy/a/b/b;

.field private final k:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private l:J

.field private m:B

.field private n:Z

.field private o:I

.field private p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/map/legacy/a/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcom/google/android/apps/gmm/map/legacy/a/b/c;


# direct methods
.method public constructor <init>(IIZZ)V
    .locals 2

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->b:Ljava/util/LinkedHashSet;

    .line 138
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->h:Ljava/util/LinkedHashSet;

    .line 151
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->k:Ljava/util/LinkedList;

    .line 163
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    .line 172
    const/4 v0, 0x4

    iput-byte v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B

    .line 178
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->o:I

    .line 184
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->p:Ljava/util/Map;

    .line 190
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->q:Ljava/util/Set;

    .line 200
    iput p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->a:I

    .line 201
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/b/b;

    invoke-direct {v0, p0, p4}, Lcom/google/android/apps/gmm/map/legacy/a/b/b;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/b/a;Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/b;

    .line 203
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->f:Z

    .line 204
    iput p2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->g:I

    .line 205
    return-void
.end method

.method private declared-synchronized b(Lcom/google/android/apps/gmm/map/legacy/a/b/c;Z)Lcom/google/android/apps/gmm/map/legacy/a/b/c;
    .locals 10

    .prologue
    const-wide/16 v8, 0x1

    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 391
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/b/c;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 479
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 395
    :cond_1
    :try_start_1
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->c:J

    .line 396
    iget-byte v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B

    if-nez v5, :cond_2

    .line 397
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->r:Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    iget-wide v6, v5, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->c:J

    cmp-long v5, v2, v6

    if-nez v5, :cond_0

    .line 400
    const/4 v5, 0x1

    iput-byte v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B

    .line 404
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->d()V

    move p2, v4

    .line 408
    :cond_2
    if-eqz p2, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->r:Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    if-eq p1, v5, :cond_3

    .line 409
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->b:Ljava/util/LinkedHashSet;

    iget-object v6, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v5, v6}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    .line 410
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->q:Ljava/util/Set;

    iget-object v6, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 413
    :cond_3
    iget-byte v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B

    if-ne v5, v1, :cond_6

    .line 414
    iget-wide v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    cmp-long v5, v2, v6

    if-nez v5, :cond_0

    .line 419
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->q:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v6}, Ljava/util/LinkedHashSet;->size()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->g:I

    if-ge v5, v6, :cond_4

    .line 421
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->i:Ljava/util/Iterator;

    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 422
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->i:Ljava/util/Iterator;

    .line 423
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    add-long/2addr v2, v8

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/apps/gmm/map/legacy/a/b/c;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;JZ)V

    move-object v0, v1

    goto :goto_0

    .line 429
    :cond_4
    iget-boolean v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->n:Z

    if-eqz v5, :cond_5

    .line 430
    const/4 v1, 0x4

    iput-byte v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 391
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 434
    :cond_5
    const/4 v5, 0x2

    :try_start_2
    iput-byte v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B

    .line 435
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/b;

    const/4 v6, 0x0

    iput v6, v5, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->b:I

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v6}, Ljava/util/LinkedHashSet;->clear()V

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v6}, Ljava/util/LinkedHashSet;->clear()V

    const/4 v6, 0x0

    iput-object v6, v5, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->c:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->d:Ljava/util/LinkedHashSet;

    iget-object v7, v5, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->g:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v6, v7}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v6}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->f:Ljava/util/Iterator;

    .line 438
    :cond_6
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->q:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    iget v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->g:I

    if-ge v5, v6, :cond_7

    move v5, v1

    .line 442
    :goto_1
    if-nez v5, :cond_8

    .line 443
    const/4 v1, 0x4

    iput-byte v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B

    goto/16 :goto_0

    :cond_7
    move v5, v4

    .line 438
    goto :goto_1

    .line 447
    :cond_8
    iget-byte v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B

    const/4 v7, 0x2

    if-ne v6, v7, :cond_d

    .line 448
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v2}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_c

    .line 449
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->c:J

    iget-wide v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    if-eqz v5, :cond_0

    .line 450
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/b;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->c:Lcom/google/android/apps/gmm/map/internal/c/bp;

    if-eqz v3, :cond_a

    iget v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->b:I

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->g:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    iget v5, v5, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->a:I

    if-ge v3, v5, :cond_a

    if-nez p2, :cond_9

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->g:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->c:Lcom/google/android/apps/gmm/map/internal/b/e;

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->c:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v6, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->g:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-interface {v3, v5, v6}, Lcom/google/android/apps/gmm/map/internal/b/e;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v3

    if-eqz v3, :cond_9

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v5, v3}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    :cond_9
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->f:Ljava/util/Iterator;

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_a

    iget v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->b:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->b:I

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->d:Ljava/util/LinkedHashSet;

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->e:Ljava/util/LinkedHashSet;

    iput-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->d:Ljava/util/LinkedHashSet;

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->e:Ljava/util/LinkedHashSet;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->clear()V

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->f:Ljava/util/Iterator;

    :cond_a
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->f:Ljava/util/Iterator;

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->f:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iput-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->c:Lcom/google/android/apps/gmm/map/internal/c/bp;

    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->c:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->g:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    iget-wide v6, v5, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    add-long/2addr v6, v8

    iput-wide v6, v5, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    iget-boolean v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->a:Z

    if-nez v5, :cond_b

    iget v2, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/b;->b:I

    if-eqz v2, :cond_b

    :goto_2
    invoke-direct {v0, v3, v6, v7, v1}, Lcom/google/android/apps/gmm/map/legacy/a/b/c;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;JZ)V

    goto/16 :goto_0

    :cond_b
    move v1, v4

    goto :goto_2

    .line 457
    :cond_c
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    .line 458
    const/4 v1, 0x3

    iput-byte v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B

    .line 469
    :cond_d
    iget-byte v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B

    const/4 v4, 0x3

    if-ne v1, v4, :cond_0

    .line 470
    iget-wide v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    cmp-long v1, v2, v6

    if-nez v1, :cond_0

    .line 473
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->k:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    if-eqz v5, :cond_e

    .line 474
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->k:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    .line 475
    new-instance v2, Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    iget-object v1, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    add-long/2addr v4, v8

    iput-wide v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v2, v1, v4, v5, v0}, Lcom/google/android/apps/gmm/map/legacy/a/b/c;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;JZ)V

    move-object v0, v2

    goto/16 :goto_0

    .line 477
    :cond_e
    const/4 v1, 0x4

    iput-byte v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private declared-synchronized d()V
    .locals 14

    .prologue
    const/16 v1, 0x8

    const/4 v4, 0x0

    const/4 v2, -0x1

    const/4 v13, 0x3

    const/4 v12, 0x2

    .line 291
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->h:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->q:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 295
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->f:Z

    if-eqz v0, :cond_e

    .line 301
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 303
    sget-object v6, Lcom/google/android/apps/gmm/map/internal/c/bv;->d:Lcom/google/android/apps/gmm/map/internal/c/bv;

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v6

    aget-object v6, v7, v6

    if-nez v6, :cond_0

    sget-object v6, Lcom/google/android/apps/gmm/map/internal/c/bv;->e:Lcom/google/android/apps/gmm/map/internal/c/bv;

    .line 304
    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v6

    aget-object v6, v7, v6

    if-nez v6, :cond_0

    .line 305
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 291
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 307
    :cond_0
    :try_start_1
    invoke-virtual {v3, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 313
    :cond_1
    iget-object v9, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->c:Lcom/google/android/apps/gmm/map/internal/b/e;

    iget v10, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->a:I

    iget-object v11, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->o:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->h:Ljava/util/LinkedHashSet;

    if-ltz v6, :cond_2

    if-le v6, v1, :cond_3

    :cond_2
    move v6, v1

    :cond_3
    if-nez v0, :cond_15

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    move-object v8, v0

    :goto_1
    instance-of v0, v3, Ljava/util/Collection;

    if-eqz v0, :cond_c

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-static {v3}, Lcom/google/b/c/an;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    :cond_4
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/b/a;->b(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v3

    invoke-static {v9, v0, v11}, Lcom/google/android/apps/gmm/map/internal/b/a;->a(Lcom/google/android/apps/gmm/map/internal/b/e;Ljava/util/Set;Lcom/google/android/apps/gmm/map/b/a/y;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v9, v3, v11}, Lcom/google/android/apps/gmm/map/internal/b/a;->a(Lcom/google/android/apps/gmm/map/internal/b/e;Ljava/util/Set;Lcom/google/android/apps/gmm/map/b/a/y;)Ljava/util/Set;

    move-result-object v3

    const/4 v0, 0x1

    move v7, v0

    :goto_2
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    if-lez v6, :cond_d

    if-gt v7, v10, :cond_d

    if-ge v4, v13, :cond_d

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    const/16 v5, 0xd

    if-gt v0, v5, :cond_14

    if-gtz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    if-le v0, v12, :cond_7

    :cond_5
    if-ge v4, v12, :cond_6

    const/4 v0, 0x4

    if-ge v7, v0, :cond_7

    :cond_6
    if-ge v4, v13, :cond_14

    const/4 v0, 0x6

    if-lt v7, v0, :cond_14

    :cond_7
    invoke-static {v1, v8, v6}, Lcom/google/android/apps/gmm/map/internal/b/a;->a(Ljava/util/Set;Ljava/util/Set;I)I

    move-result v5

    if-eq v6, v5, :cond_8

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    :cond_8
    :goto_3
    if-eqz v3, :cond_13

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    if-le v0, v12, :cond_9

    if-lt v7, v13, :cond_13

    :cond_9
    invoke-interface {v1, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    const/4 v0, 0x0

    :goto_4
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v3

    if-gt v3, v12, :cond_a

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/internal/b/a;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    :cond_a
    if-eqz v0, :cond_b

    invoke-static {v9, v0, v11}, Lcom/google/android/apps/gmm/map/internal/b/a;->a(Lcom/google/android/apps/gmm/map/internal/b/e;Ljava/util/Set;Lcom/google/android/apps/gmm/map/b/a/y;)Ljava/util/Set;

    move-result-object v0

    :cond_b
    invoke-static {v9, v1, v11}, Lcom/google/android/apps/gmm/map/internal/b/a;->a(Lcom/google/android/apps/gmm/map/internal/b/e;Ljava/util/Set;Lcom/google/android/apps/gmm/map/b/a/y;)Ljava/util/Set;

    move-result-object v3

    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v6, v5

    move-object v1, v3

    move-object v3, v0

    goto :goto_2

    :cond_c
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 318
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->n:Z

    if-eqz v0, :cond_f

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->h:Ljava/util/LinkedHashSet;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->c:Lcom/google/android/apps/gmm/map/internal/b/e;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 323
    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/b/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)Ljava/util/List;

    move-result-object v1

    .line 322
    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 352
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->h:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->i:Ljava/util/Iterator;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 353
    monitor-exit p0

    return-void

    .line 327
    :cond_f
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->h:Ljava/util/LinkedHashSet;

    invoke-static {v0}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 328
    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 329
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 332
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->k:Ljava/util/LinkedList;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v0, v5}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    goto :goto_6

    .line 338
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->h:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :cond_11
    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 339
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 340
    if-ne v1, v2, :cond_12

    .line 342
    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    move v1, v0

    goto :goto_7

    .line 343
    :cond_12
    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-eq v1, v0, :cond_11

    .line 346
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_7

    :cond_13
    move-object v0, v3

    goto/16 :goto_4

    :cond_14
    move v5, v6

    goto/16 :goto_3

    :cond_15
    move-object v8, v0

    goto/16 :goto_1
.end method

.method private declared-synchronized e()Z
    .locals 2

    .prologue
    .line 484
    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/legacy/a/b/c;Z)Lcom/google/android/apps/gmm/map/legacy/a/b/c;
    .locals 2

    .prologue
    .line 364
    monitor-enter p0

    :cond_0
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->b(Lcom/google/android/apps/gmm/map/legacy/a/b/c;Z)Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 366
    const/4 p2, 0x0

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->p:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    .line 370
    if-eqz v0, :cond_1

    .line 371
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->b:Z

    if-nez v0, :cond_0

    .line 372
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->p:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->b:Z

    if-nez v0, :cond_2

    .line 380
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->q:Ljava/util/Set;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    :cond_2
    monitor-exit p0

    return-object p1

    .line 364
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 271
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->n:Z

    if-nez v0, :cond_0

    .line 272
    const/4 v0, 0x4

    iput-byte v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 274
    :cond_0
    monitor-exit p0

    return-void

    .line 271
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/b/e;Lcom/google/android/apps/gmm/map/b/a/y;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;IZ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/b/e;",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x1

    const/4 v0, 0x0

    .line 234
    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    iput-byte v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B

    .line 235
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    add-long/2addr v2, v6

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->l:J

    .line 236
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->c:Lcom/google/android/apps/gmm/map/internal/b/e;

    .line 237
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 239
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/as;->t:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 240
    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-direct {v1, v2, v4, v5, v3}, Lcom/google/android/apps/gmm/map/legacy/a/b/c;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;JZ)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->r:Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    .line 242
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->clear()V

    .line 245
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 246
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->b:Ljava/util/LinkedHashSet;

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 245
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->k:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 250
    if-eqz p4, :cond_1

    .line 251
    invoke-interface {p4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 252
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->k:Ljava/util/LinkedList;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v0, v3}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 234
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 256
    :cond_1
    if-eqz p5, :cond_2

    .line 257
    :try_start_1
    invoke-interface {p5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 258
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->k:Ljava/util/LinkedList;

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v0, v3}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 262
    :cond_2
    iput p6, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->o:I

    .line 263
    iput-boolean p7, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->n:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/legacy/a/b/c;)Z
    .locals 4

    .prologue
    .line 491
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->c:J

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->l:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 4

    .prologue
    .line 283
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->e:J

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->l:J

    .line 284
    const/4 v0, 0x4

    iput-byte v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->m:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    monitor-exit p0

    return-void

    .line 283
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Lcom/google/android/apps/gmm/map/legacy/a/b/c;
    .locals 1

    .prologue
    .line 357
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->r:Lcom/google/android/apps/gmm/map/legacy/a/b/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
