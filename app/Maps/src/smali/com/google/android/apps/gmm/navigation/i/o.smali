.class public Lcom/google/android/apps/gmm/navigation/i/o;
.super Lcom/google/android/apps/gmm/navigation/i/i;
.source "PG"


# static fields
.field private static final d:[I


# instance fields
.field private final c:Lcom/google/maps/g/a/fb;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 235
    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/gmm/l;->en:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/gmm/l;->eo:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/apps/gmm/l;->ep:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/android/apps/gmm/l;->eq:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/google/android/apps/gmm/l;->er:I

    aput v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lcom/google/android/apps/gmm/l;->es:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/o;->d:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/google/maps/g/a/fb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;",
            "Lcom/google/maps/g/a/fb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 245
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/navigation/i/i;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 246
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/i/o;->c:Lcom/google/maps/g/a/fb;

    .line 247
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/g/a/ez;
    .locals 1

    .prologue
    .line 280
    sget-object v0, Lcom/google/maps/g/a/ez;->i:Lcom/google/maps/g/a/ez;

    return-object v0
.end method

.method public final b()Lcom/google/maps/g/a/fb;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/o;->c:Lcom/google/maps/g/a/fb;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 256
    sget-object v0, Lcom/google/maps/g/a/af;->d:Lcom/google/maps/g/a/af;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/i/o;->a(Lcom/google/maps/g/a/af;)Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v0

    .line 257
    if-eqz v0, :cond_0

    .line 258
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/i/o;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->em:I

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 274
    :goto_0
    return-object v0

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/o;->c:Lcom/google/maps/g/a/fb;

    sget-object v2, Lcom/google/maps/g/a/fb;->a:Lcom/google/maps/g/a/fb;

    if-ne v0, v2, :cond_1

    .line 264
    const/4 v0, 0x2

    .line 269
    :goto_1
    sget-object v2, Lcom/google/maps/g/a/af;->c:Lcom/google/maps/g/a/af;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/navigation/i/o;->a(Lcom/google/maps/g/a/af;)Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v2

    .line 270
    if-eqz v2, :cond_2

    .line 271
    add-int/lit8 v0, v0, 0x1

    .line 272
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/i/o;->a:Landroid/content/Context;

    sget-object v4, Lcom/google/android/apps/gmm/navigation/i/o;->d:[I

    aget v0, v4, v0

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 265
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/o;->c:Lcom/google/maps/g/a/fb;

    sget-object v2, Lcom/google/maps/g/a/fb;->b:Lcom/google/maps/g/a/fb;

    if-ne v0, v2, :cond_3

    .line 266
    const/4 v0, 0x4

    goto :goto_1

    .line 274
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/i/o;->a:Landroid/content/Context;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/i/o;->d:[I

    aget v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method
