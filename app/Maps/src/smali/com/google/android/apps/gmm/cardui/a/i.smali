.class public Lcom/google/android/apps/gmm/cardui/a/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/cardui/a/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/cardui/a/i;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 44
    .line 45
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ez;->g()Lcom/google/o/h/a/ez;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ez;

    invoke-virtual {v0}, Lcom/google/o/h/a/ez;->d()Ljava/util/List;

    move-result-object v3

    .line 46
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 47
    sget-object v0, Lcom/google/android/apps/gmm/cardui/a/i;->a:Ljava/lang/String;

    .line 67
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ez;->g()Lcom/google/o/h/a/ez;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ez;

    .line 51
    iget-object v1, v0, Lcom/google/o/h/a/ez;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/et;->d()Lcom/google/o/h/a/et;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/google/o/h/a/et;

    .line 52
    if-nez v4, :cond_2

    move-object v1, v6

    .line 54
    :cond_1
    :goto_1
    const/4 v2, 0x0

    .line 55
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/o/h/a/ph;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->b()Landroid/content/Context;

    move-result-object v5

    .line 54
    invoke-static {v2, v5}, Lcom/google/android/apps/gmm/cardui/f/e;->a(Lcom/google/o/h/a/ph;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v2

    .line 57
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/o/h/a/ph;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->b()Landroid/content/Context;

    move-result-object v5

    .line 56
    invoke-static {v3, v5}, Lcom/google/android/apps/gmm/cardui/f/e;->a(Lcom/google/o/h/a/ph;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v3

    .line 58
    if-nez v4, :cond_3

    move-object v4, v6

    .line 59
    :goto_2
    sget-object v5, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    .line 60
    new-instance v9, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    .line 61
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->g()Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v7

    iget-object v7, v7, Lcom/google/android/apps/gmm/util/b/b;->b:Ljava/lang/String;

    if-eqz v7, :cond_5

    iget-object v8, v9, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    if-nez v7, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 52
    :cond_2
    iget v1, v4, Lcom/google/o/h/a/et;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    goto :goto_1

    .line 58
    :cond_3
    iget-object v4, v4, Lcom/google/o/h/a/et;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v4

    check-cast v4, Lcom/google/r/b/a/afz;

    goto :goto_2

    .line 61
    :cond_4
    iget v10, v8, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v10, v10, 0x2

    iput v10, v8, Lcom/google/maps/g/ia;->a:I

    iput-object v7, v8, Lcom/google/maps/g/ia;->c:Ljava/lang/Object;

    .line 62
    :cond_5
    iget-object v7, v0, Lcom/google/o/h/a/ez;->d:Ljava/lang/Object;

    instance-of v8, v7, Ljava/lang/String;

    if-eqz v8, :cond_6

    move-object v0, v7

    check-cast v0, Ljava/lang/String;

    :goto_3
    invoke-virtual {v9, v0}, Lcom/google/android/apps/gmm/z/b/f;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    .line 63
    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v7

    check-cast v7, Lcom/google/maps/g/hy;

    .line 64
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    move-object v8, v6

    .line 65
    invoke-interface/range {v0 .. v8}, Lcom/google/android/apps/gmm/directions/a/f;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/directions/a/g;Ljava/lang/String;Lcom/google/maps/g/hy;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 62
    :cond_6
    check-cast v7, Lcom/google/n/f;

    invoke-virtual {v7}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7}, Lcom/google/n/f;->e()Z

    move-result v7

    if-eqz v7, :cond_7

    iput-object v8, v0, Lcom/google/o/h/a/ez;->d:Ljava/lang/Object;

    :cond_7
    move-object v0, v8

    goto :goto_3
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    sget-object v0, Lcom/google/o/h/a/g;->d:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 37
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    .line 31
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
