.class public Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/activities/c;

.field private final b:Lcom/google/android/apps/gmm/map/r/a/ag;

.field private final c:Lcom/google/android/apps/gmm/navigation/navui/views/d;

.field private final d:Lcom/google/android/apps/gmm/reportmapissue/a;

.field private e:Z

.field private f:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/reportmapissue/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/google/android/apps/gmm/reportmapissue/d/a;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/navigation/g/b/k;Lcom/google/android/apps/gmm/navigation/navui/views/d;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 41
    iput-object p2, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 42
    iput-object p4, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->c:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    .line 43
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/gmm/reportmapissue/a;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/navigation/g/b/k;)Lcom/google/android/apps/gmm/reportmapissue/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->d:Lcom/google/android/apps/gmm/reportmapissue/a;

    .line 44
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/j/ae;)V
    .locals 7
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->e:Z

    iget-boolean v1, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    if-eq v0, v1, :cond_0

    .line 81
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->e:Z

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->g:Lcom/google/android/apps/gmm/reportmapissue/d/a;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->e:Z

    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/d;

    iget-object v3, v0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->a:Ljava/util/List;

    iget-object v4, v0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->b:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    iget-object v5, v0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->c:Lcom/google/android/apps/gmm/navigation/navui/views/g;

    const/4 v6, 0x1

    invoke-virtual {v5, v1, v6}, Lcom/google/android/apps/gmm/navigation/navui/views/g;->a(ZZ)Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move-result-object v1

    invoke-direct {v2, v3, v4, v1}, Lcom/google/android/apps/gmm/base/views/c/d;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/navigation/navui/views/d;Lcom/google/android/apps/gmm/navigation/navui/views/f;)V

    iput-object v2, v0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->d:Lcom/google/android/apps/gmm/base/views/c/d;

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->f:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->g:Lcom/google/android/apps/gmm/reportmapissue/d/a;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 86
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 48
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->d:Lcom/google/android/apps/gmm/reportmapissue/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->o()Lcom/google/android/apps/gmm/feedback/a/e;

    move-result-object v2

    invoke-interface {v2, v6}, Lcom/google/android/apps/gmm/feedback/a/e;->a(Lcom/google/android/apps/gmm/feedback/a/g;)Lcom/google/android/apps/gmm/feedback/a/f;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/gmm/reportmapissue/a;->c:Lcom/google/android/apps/gmm/feedback/a/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->x_()Lcom/google/android/apps/gmm/util/replay/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/util/replay/a;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/reportmapissue/a;->b:Ljava/lang/String;

    .line 50
    new-instance v0, Lcom/google/android/apps/gmm/reportmapissue/d/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->d:Lcom/google/android/apps/gmm/reportmapissue/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 51
    iget-object v4, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->w:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->c:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/reportmapissue/d/a;-><init>(Landroid/app/DialogFragment;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/reportmapissue/a;Ljava/util/List;Lcom/google/android/apps/gmm/navigation/navui/views/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->g:Lcom/google/android/apps/gmm/reportmapissue/d/a;

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/reportmapissue/b/a;

    invoke-virtual {v0, v1, v6}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->f:Lcom/google/android/libraries/curvular/ae;

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->f:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->g:Lcom/google/android/apps/gmm/reportmapissue/d/a;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 59
    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->a:Lcom/google/android/apps/gmm/base/activities/c;

    const v2, 0x1030076

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 60
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->f:Lcom/google/android/libraries/curvular/ae;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 61
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 74
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 75
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 66
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;->dismiss()V

    .line 69
    return-void
.end method
