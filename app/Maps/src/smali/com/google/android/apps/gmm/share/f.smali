.class public Lcom/google/android/apps/gmm/share/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/share/e;


# static fields
.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/internal/util/Predicate",
            "<",
            "Landroid/content/Intent;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/share/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "com.google.glass.companion.sharing.SharingActivity"

    new-instance v2, Lcom/google/android/apps/gmm/share/g;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/share/g;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sput-object v0, Lcom/google/android/apps/gmm/share/f;->b:Ljava/util/Map;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;[Lcom/google/android/apps/gmm/share/a/a;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-string v0, "share_history.xml"

    .line 39
    invoke-static {p1, v0}, Landroid/support/v7/internal/widget/k;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/k;

    move-result-object v3

    .line 40
    invoke-virtual {v3, p2}, Landroid/support/v7/internal/widget/k;->a(Landroid/content/Intent;)V

    .line 42
    invoke-virtual {v3}, Landroid/support/v7/internal/widget/k;->a()I

    move-result v7

    .line 43
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v8

    .line 45
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_2

    .line 46
    invoke-virtual {v3, v6}, Landroid/support/v7/internal/widget/k;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 47
    iget-object v0, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 49
    sget-object v1, Lcom/google/android/apps/gmm/share/f;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/share/f;->b:Ljava/util/Map;

    .line 50
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/util/Predicate;

    invoke-interface {v0, p2}, Lcom/android/internal/util/Predicate;->apply(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 51
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/share/c;

    move-object v1, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/share/c;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;Landroid/support/v7/internal/widget/k;[Lcom/google/android/apps/gmm/share/a/a;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 45
    :cond_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 61
    :cond_2
    invoke-virtual {v8}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/f;->a:Ljava/util/List;

    .line 62
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/share/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/f;->a:Ljava/util/List;

    return-object v0
.end method
