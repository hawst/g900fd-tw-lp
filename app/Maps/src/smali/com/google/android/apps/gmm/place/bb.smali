.class public Lcom/google/android/apps/gmm/place/bb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/c;


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bb;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/high16 v2, 0x10000

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/bb;->d:Landroid/content/Context;

    .line 27
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    .line 28
    iget v0, v0, Lcom/google/r/b/a/acq;->a:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/bb;->a:Z

    .line 29
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    .line 30
    iget-object v0, v0, Lcom/google/r/b/a/acq;->x:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bb;->b:Ljava/lang/String;

    .line 31
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    .line 32
    iget-object v0, v0, Lcom/google/r/b/a/acq;->x:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bb;->c:Ljava/lang/String;

    .line 33
    return-void

    .line 28
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bb;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bb;->d:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 45
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bb;->c:Ljava/lang/String;

    return-object v0
.end method
