.class public Lcom/google/android/apps/gmm/car/d/q;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/m/l;

.field public final b:Lcom/google/android/apps/gmm/map/util/b/a/a;

.field final c:Lcom/google/android/apps/gmm/car/k/h;

.field final d:Lcom/google/android/apps/gmm/car/d/a;

.field public e:Z

.field f:Z

.field public final g:Ljava/lang/Object;

.field private final h:Lcom/google/android/apps/gmm/map/m/m;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/m/l;Lcom/google/android/apps/gmm/map/util/b/a/a;Lcom/google/android/apps/gmm/car/k/h;Lcom/google/android/apps/gmm/car/d/a;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    new-instance v0, Lcom/google/android/apps/gmm/car/d/r;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/d/r;-><init>(Lcom/google/android/apps/gmm/car/d/q;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/q;->g:Ljava/lang/Object;

    .line 103
    new-instance v0, Lcom/google/android/apps/gmm/car/d/s;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/d/s;-><init>(Lcom/google/android/apps/gmm/car/d/q;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/q;->h:Lcom/google/android/apps/gmm/map/m/m;

    .line 41
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/map/m/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/q;->a:Lcom/google/android/apps/gmm/map/m/l;

    .line 42
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/map/util/b/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/q;->b:Lcom/google/android/apps/gmm/map/util/b/a/a;

    .line 43
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/car/k/h;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/d/q;->c:Lcom/google/android/apps/gmm/car/k/h;

    .line 44
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p4, Lcom/google/android/apps/gmm/car/d/a;

    iput-object p4, p0, Lcom/google/android/apps/gmm/car/d/q;->d:Lcom/google/android/apps/gmm/car/d/a;

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/q;->h:Lcom/google/android/apps/gmm/map/m/m;

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/m/l;->a(Lcom/google/android/apps/gmm/map/m/m;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/q;->g:Ljava/lang/Object;

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->d(Ljava/lang/Object;)V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/d/q;->a()V

    .line 49
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 79
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/d/q;->f:Z

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/q;->a:Lcom/google/android/apps/gmm/map/m/l;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/l;->c(Z)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/q;->a:Lcom/google/android/apps/gmm/map/m/l;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/m/l;->a(Z)V

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/q;->a:Lcom/google/android/apps/gmm/map/m/l;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/m/l;->c(Z)V

    .line 84
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/d/q;->e:Z

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/q;->a:Lcom/google/android/apps/gmm/map/m/l;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/l;->b(Z)V

    goto :goto_0
.end method
