.class public Lcom/google/android/apps/gmm/hotels/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/hotels/a/b;


# static fields
.field private static final d:Lcom/google/maps/g/gn;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/a;

.field b:Z

.field c:Lcom/google/maps/g/gn;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private e:Lcom/google/android/apps/gmm/hotels/g;

.field private f:J

.field private g:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/google/maps/g/gn;->newBuilder()Lcom/google/maps/g/gp;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/gq;->b:Lcom/google/maps/g/gq;

    .line 40
    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/maps/g/gp;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/maps/g/gp;->a:I

    iget v1, v1, Lcom/google/maps/g/gq;->c:I

    iput v1, v0, Lcom/google/maps/g/gp;->d:I

    .line 41
    invoke-virtual {v0}, Lcom/google/n/v;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gn;

    sput-object v0, Lcom/google/android/apps/gmm/hotels/c;->d:Lcom/google/maps/g/gn;

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/hotels/c;->b:Z

    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/hotels/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/hotels/d;-><init>(Lcom/google/android/apps/gmm/hotels/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->g:Ljava/lang/Object;

    .line 70
    iput-object p1, p0, Lcom/google/android/apps/gmm/hotels/c;->a:Lcom/google/android/apps/gmm/base/a;

    .line 71
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/x/o;)Landroid/app/Fragment;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<*>;)",
            "Landroid/app/Fragment;"
        }
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->a:Lcom/google/android/apps/gmm/base/a;

    .line 204
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    .line 203
    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 88
    iget-object v2, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    if-eqz v2, :cond_0

    .line 89
    iget-object v2, p0, Lcom/google/android/apps/gmm/hotels/c;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/gmm/hotels/c;->f:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    .line 90
    iput-object v6, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    .line 105
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v2, v1

    .line 89
    goto :goto_0

    .line 94
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    iget v2, v2, Lcom/google/maps/g/gn;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_2
    if-eqz v2, :cond_0

    .line 97
    iget-object v2, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    invoke-virtual {v2}, Lcom/google/maps/g/gn;->d()Ljava/lang/String;

    move-result-object v2

    .line 98
    invoke-static {v2}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;)J

    move-result-wide v2

    .line 99
    invoke-static {}, Lcom/google/android/apps/gmm/hotels/a/i;->c()Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_4

    .line 100
    :goto_3
    if-eqz v0, :cond_0

    .line 101
    iput-object v6, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    goto :goto_1

    :cond_3
    move v2, v1

    .line 94
    goto :goto_2

    :cond_4
    move v0, v1

    .line 99
    goto :goto_3
.end method

.method public final a(Lcom/google/android/apps/gmm/hotels/a/e;)V
    .locals 3

    .prologue
    .line 75
    invoke-static {}, Lcom/google/maps/g/gn;->newBuilder()Lcom/google/maps/g/gp;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/gmm/hotels/a/e;->a:Lcom/google/android/apps/gmm/hotels/a/i;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/hotels/a/i;->a()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/maps/g/gp;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/maps/g/gp;->a:I

    iput-object v1, v0, Lcom/google/maps/g/gp;->b:Ljava/lang/Object;

    iget-object v1, p1, Lcom/google/android/apps/gmm/hotels/a/e;->a:Lcom/google/android/apps/gmm/hotels/a/i;

    iget-object v2, p1, Lcom/google/android/apps/gmm/hotels/a/e;->b:Lcom/google/android/apps/gmm/hotels/a/i;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;Lcom/google/android/apps/gmm/hotels/a/i;)I

    move-result v1

    const/4 v2, 0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, v0, Lcom/google/maps/g/gp;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/maps/g/gp;->a:I

    iput v1, v0, Lcom/google/maps/g/gp;->c:I

    invoke-virtual {v0}, Lcom/google/maps/g/gp;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gn;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/hotels/c;->a(Lcom/google/maps/g/gn;)V

    .line 76
    return-void
.end method

.method public final a(Lcom/google/maps/g/gn;)V
    .locals 4

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/hotels/c;->f:J

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/hotels/c;->b:Z

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/hotels/a/a;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/hotels/a/a;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 84
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/google/android/apps/gmm/hotels/a/g;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            ">;",
            "Lcom/google/android/apps/gmm/hotels/a/g;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 183
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/hotels/c;->b:Z

    if-nez v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->e:Lcom/google/android/apps/gmm/hotels/g;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->e:Lcom/google/android/apps/gmm/hotels/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/hotels/g;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/hotels/c;->b:Z

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->e:Lcom/google/android/apps/gmm/hotels/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->e:Lcom/google/android/apps/gmm/hotels/g;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/hotels/g;->f()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->e:Lcom/google/android/apps/gmm/hotels/g;

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/hotels/c;->b:Z

    .line 195
    :cond_2
    new-instance v1, Lcom/google/android/apps/gmm/hotels/g;

    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/hotels/c;->d:Lcom/google/maps/g/gn;

    :goto_1
    sget-object v2, Lcom/google/maps/g/b/o;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-direct {v1, p1, v0}, Lcom/google/android/apps/gmm/hotels/g;-><init>(Ljava/util/Collection;Lcom/google/e/a/a/a/b;)V

    .line 196
    iput-object v1, p0, Lcom/google/android/apps/gmm/hotels/c;->e:Lcom/google/android/apps/gmm/hotels/g;

    .line 197
    iget-object v0, v1, Lcom/google/android/apps/gmm/hotels/g;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/c;->e:Lcom/google/android/apps/gmm/hotels/g;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    goto :goto_0

    .line 195
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    goto :goto_1
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    iget v0, v0, Lcom/google/maps/g/gn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 122
    :cond_0
    :goto_1
    return-void

    .line 109
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 113
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    invoke-virtual {v0}, Lcom/google/maps/g/gn;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v0

    .line 114
    invoke-static {v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;)J

    move-result-wide v2

    invoke-static {}, Lcom/google/android/apps/gmm/hotels/a/i;->d()Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    .line 118
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/hotels/c;->b:Z

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/hotels/a/a;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/hotels/a/a;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final c()Lcom/google/e/a/a/a/b;
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    if-nez v0, :cond_0

    .line 139
    sget-object v0, Lcom/google/android/apps/gmm/hotels/c;->d:Lcom/google/maps/g/gn;

    .line 143
    :goto_0
    sget-object v1, Lcom/google/maps/g/b/o;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    return-object v0

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    goto :goto_0
.end method

.method public final d()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/c;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    .line 150
    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aj:Lcom/google/android/apps/gmm/shared/b/c;

    sget-object v3, Lcom/google/maps/g/gn;->PARSER:Lcom/google/n/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;[B)[B

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/ax;)Lcom/google/n/at;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    check-cast v0, Lcom/google/maps/g/gn;

    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    .line 151
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->ak:Lcom/google/android/apps/gmm/shared/b/c;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/hotels/c;->f:J

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/c;->g:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 153
    return-void

    :cond_1
    move-object v0, v1

    .line 150
    goto :goto_0
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    .line 159
    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    if-eqz v1, :cond_2

    .line 160
    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aj:Lcom/google/android/apps/gmm/shared/b/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/hotels/c;->c:Lcom/google/maps/g/gn;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Lcom/google/n/at;)V

    .line 161
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ak:Lcom/google/android/apps/gmm/shared/b/c;

    iget-wide v2, p0, Lcom/google/android/apps/gmm/hotels/c;->f:J

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 166
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/c;->g:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 167
    return-void

    .line 163
    :cond_2
    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aj:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;)V

    .line 164
    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ak:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;)V

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->e:Lcom/google/android/apps/gmm/hotels/g;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->e:Lcom/google/android/apps/gmm/hotels/g;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/hotels/g;->f()V

    .line 211
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/c;->e:Lcom/google/android/apps/gmm/hotels/g;

    .line 212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/hotels/c;->b:Z

    .line 214
    :cond_0
    return-void
.end method
