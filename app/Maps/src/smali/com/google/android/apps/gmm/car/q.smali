.class public final Lcom/google/android/apps/gmm/car/q;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:I


# instance fields
.field final b:Landroid/view/View;

.field public final c:Landroid/view/View;

.field public d:Z

.field public e:I

.field f:Z

.field private final g:Lcom/google/android/apps/gmm/car/d/a;

.field private final h:Landroid/view/animation/Interpolator;

.field private final i:Landroid/view/animation/Interpolator;

.field private final j:Landroid/view/animation/Interpolator;

.field private final k:Lcom/google/android/apps/gmm/car/u;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/q;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/car/d/a;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    new-instance v0, Lcom/google/android/apps/gmm/car/s;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/s;-><init>(Lcom/google/android/apps/gmm/car/q;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/q;->k:Lcom/google/android/apps/gmm/car/u;

    .line 45
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p2, Lcom/google/android/apps/gmm/car/d/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/q;->g:Lcom/google/android/apps/gmm/car/d/a;

    .line 47
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->c:Z

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p1, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    const v1, 0x10c000e

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/q;->h:Landroid/view/animation/Interpolator;

    .line 50
    iget-object v0, p1, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    const v1, 0x10c000d

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/q;->i:Landroid/view/animation/Interpolator;

    .line 52
    iget-object v0, p1, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    const v1, 0x10c000f

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/q;->j:Landroid/view/animation/Interpolator;

    .line 61
    :goto_0
    const-class v0, Lcom/google/android/apps/gmm/car/t;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/q;->b:Landroid/view/View;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/q;->k:Lcom/google/android/apps/gmm/car/u;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->b:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/car/q;->a:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setPivotX(F)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleX(F)V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/gmm/car/r;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/r;-><init>(Lcom/google/android/apps/gmm/car/q;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 79
    return-void

    .line 56
    :cond_1
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/q;->h:Landroid/view/animation/Interpolator;

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->h:Landroid/view/animation/Interpolator;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/q;->i:Landroid/view/animation/Interpolator;

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->h:Landroid/view/animation/Interpolator;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/q;->j:Landroid/view/animation/Interpolator;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 110
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/car/q;->d:Z

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->g:Lcom/google/android/apps/gmm/car/d/a;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/d/a;->m:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/d/a;->a()V

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    move-result v0

    const v1, 0x3f7fbe77    # 0.999f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    .line 122
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/q;->j:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 125
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/car/q;->f:Z

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/q;->k:Lcom/google/android/apps/gmm/car/u;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 127
    return-void

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public a(F)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->g:Lcom/google/android/apps/gmm/car/d/a;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/d/a;->m:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/d/a;->a()V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    const v1, 0x3a83126f    # 0.001f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setScaleX(F)V

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/q;->h:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 143
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    .line 146
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/car/q;->f:Z

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/q;->k:Lcom/google/android/apps/gmm/car/u;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 148
    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/q;->i:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method
