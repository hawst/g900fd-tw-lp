.class public abstract Lcom/google/android/apps/gmm/map/d/a;
.super Lcom/google/android/apps/gmm/v/e;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/f/o;

.field private b:Lcom/google/android/apps/gmm/v/cp;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final g:Lcom/google/android/apps/gmm/v/ad;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/e;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/d/a;->g:Lcom/google/android/apps/gmm/v/ad;

    .line 34
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/d/a;->a:Lcom/google/android/apps/gmm/map/f/o;

    .line 35
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/v/h;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/google/android/apps/gmm/v/h;->e:Lcom/google/android/apps/gmm/v/h;

    return-object v0
.end method

.method public abstract a(IIF)V
.end method

.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/d/a;->a:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->B:Lcom/google/android/apps/gmm/v/cp;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/d/a;->b:Lcom/google/android/apps/gmm/v/cp;

    .line 64
    if-eqz p1, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/d/a;->b:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 67
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/d/a;->b:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/d/a;->a:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/d/a;->a:Lcom/google/android/apps/gmm/map/f/o;

    .line 74
    iget-object v1, v1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/d/a;->a:Lcom/google/android/apps/gmm/map/f/o;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/o;->i:F

    .line 73
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/map/d/a;->a(IIF)V

    .line 75
    return-void
.end method
