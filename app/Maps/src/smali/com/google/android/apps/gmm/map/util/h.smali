.class public final Lcom/google/android/apps/gmm/map/util/h;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/Object;

.field private static final b:Ljava/lang/Object;

.field private static final c:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lcom/google/android/apps/gmm/map/util/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/util/h;->b:Ljava/lang/Object;

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/util/h;->a:Ljava/lang/Object;

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/map/util/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/util/i;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/util/h;->c:Ljava/lang/ThreadLocal;

    .line 58
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/util/h;->d:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public static a(I)Lcom/google/android/apps/gmm/map/util/f;
    .locals 3

    .prologue
    .line 71
    sget-object v0, Lcom/google/android/apps/gmm/map/util/h;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/util/h;->b:Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    .line 72
    const-string v0, "SharedBufferHolder"

    const-string v1, "Attempted to use an unregistered shared buffer"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    const/4 v0, 0x0

    .line 81
    :goto_0
    return-object v0

    .line 75
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/util/h;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/util/f;

    .line 76
    if-nez v0, :cond_1

    .line 77
    new-instance v0, Lcom/google/android/apps/gmm/map/util/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/util/f;-><init>(I)V

    goto :goto_0

    .line 79
    :cond_1
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/map/util/f;->a(I)V

    goto :goto_0
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 102
    sget-object v0, Lcom/google/android/apps/gmm/map/util/h;->c:Ljava/lang/ThreadLocal;

    sget-object v1, Lcom/google/android/apps/gmm/map/util/h;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 103
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/util/f;)V
    .locals 2

    .prologue
    .line 93
    sget-object v0, Lcom/google/android/apps/gmm/map/util/h;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/util/h;->b:Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    .line 99
    :goto_0
    return-void

    .line 97
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/util/f;->c()V

    .line 98
    sget-object v0, Lcom/google/android/apps/gmm/map/util/h;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, p0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/google/android/apps/gmm/map/util/h;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    .line 113
    sget-object v0, Lcom/google/android/apps/gmm/map/util/h;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    .line 114
    return-void
.end method
