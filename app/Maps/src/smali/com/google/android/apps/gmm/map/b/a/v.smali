.class public Lcom/google/android/apps/gmm/map/b/a/v;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/u;
    .locals 6

    .prologue
    const/16 v4, 0x15

    .line 29
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/u;

    const/4 v0, 0x1

    .line 30
    invoke-virtual {p0, v0, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v2, v2

    const/4 v0, 0x2

    .line 31
    invoke-virtual {p0, v0, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v0, v4

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    return-object v1
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/u;)Lcom/google/e/a/a/a/b;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 114
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/q;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 115
    const/4 v1, 0x5

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v6, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 117
    const/16 v1, 0xe

    .line 118
    new-instance v2, Lcom/google/e/a/a/a/b;

    sget-object v3, Lcom/google/r/b/a/b/q;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {v2, v3}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iget v3, p0, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    int-to-long v4, v3

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v6, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v3, 0x2

    iget v4, p0, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v3, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 117
    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 119
    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/u;III)Lcom/google/e/a/a/a/b;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 132
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/q;->g:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 134
    new-instance v1, Lcom/google/e/a/a/a/b;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v4, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v5, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v2, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v4, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 135
    int-to-long v2, p1

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v5, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 136
    const/4 v1, 0x3

    int-to-long v2, p2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 137
    if-lez p3, :cond_0

    .line 138
    const/4 v1, 0x4

    int-to-long v2, p3

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 140
    :cond_0
    return-object v0
.end method

.method public static b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/u;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/16 v2, 0x1a

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/16 v6, 0x15

    .line 48
    if-nez p0, :cond_0

    .line 65
    :goto_0
    return-object v0

    .line 52
    :cond_0
    const/4 v1, 0x0

    invoke-static {p0, v3, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v1

    .line 53
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 59
    :pswitch_1
    invoke-virtual {p0, v7, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/v;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/u;

    move-result-object v0

    goto :goto_0

    .line 55
    :pswitch_2
    const/16 v0, 0xe

    .line 56
    invoke-virtual {p0, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 55
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-virtual {v0, v3, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v1, v4

    invoke-virtual {v0, v7, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v0, v4

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    move-object v0, v2

    goto :goto_0

    .line 62
    :pswitch_3
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v3, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v0, v7, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v1, v4

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v0, v4

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/gmm/map/b/a/u;->a(III)Lcom/google/android/apps/gmm/map/b/a/u;

    move-result-object v0

    goto :goto_0

    .line 53
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
