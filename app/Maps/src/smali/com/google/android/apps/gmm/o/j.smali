.class Lcom/google/android/apps/gmm/o/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/reportmapissue/p;


# instance fields
.field final a:I

.field final b:I

.field final c:Landroid/widget/CompoundButton;

.field final synthetic d:Lcom/google/android/apps/gmm/o/g;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/o/g;IILandroid/widget/CompoundButton;)V
    .locals 0

    .prologue
    .line 742
    iput-object p1, p0, Lcom/google/android/apps/gmm/o/j;->d:Lcom/google/android/apps/gmm/o/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 743
    iput p2, p0, Lcom/google/android/apps/gmm/o/j;->a:I

    .line 744
    iput p3, p0, Lcom/google/android/apps/gmm/o/j;->b:I

    .line 745
    iput-object p4, p0, Lcom/google/android/apps/gmm/o/j;->c:Landroid/widget/CompoundButton;

    .line 746
    return-void
.end method


# virtual methods
.method public final a_(Lcom/google/e/a/a/a/b;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 750
    .line 752
    invoke-static {p1, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->d(Lcom/google/e/a/a/a/b;I)[Lcom/google/e/a/a/a/b;

    move-result-object v4

    move v1, v2

    .line 754
    :goto_0
    array-length v0, v4

    if-ge v1, v0, :cond_5

    .line 755
    aget-object v5, v4, v1

    iget-object v0, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v3}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_2

    move v0, v3

    :goto_1
    if-nez v0, :cond_0

    invoke-virtual {v5, v3}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v3

    :goto_2
    if-eqz v0, :cond_4

    aget-object v0, v4, v1

    .line 756
    const/16 v5, 0x15

    invoke-virtual {v0, v3, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v0, v6

    iget v5, p0, Lcom/google/android/apps/gmm/o/j;->a:I

    if-ne v0, v5, :cond_4

    move v0, v3

    .line 762
    :goto_3
    if-nez v0, :cond_1

    .line 763
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/j;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 764
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/j;->d:Lcom/google/android/apps/gmm/o/g;

    invoke-static {v0}, Lcom/google/android/apps/gmm/o/g;->h(Lcom/google/android/apps/gmm/o/g;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    .line 765
    if-eqz v0, :cond_1

    .line 766
    iget v1, p0, Lcom/google/android/apps/gmm/o/j;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 767
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->a(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/CharSequence;)V

    .line 770
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 755
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    .line 754
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_3
.end method

.method public final c_()V
    .locals 0

    .prologue
    .line 775
    return-void
.end method
