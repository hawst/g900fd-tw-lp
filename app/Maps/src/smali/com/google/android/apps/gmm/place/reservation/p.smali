.class public Lcom/google/android/apps/gmm/place/reservation/p;
.super Lcom/google/android/apps/gmm/place/reservation/a;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/reservation/b/g;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/place/reservation/y;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/apps/gmm/base/activities/c;

.field private final e:Lcom/google/android/apps/gmm/z/b/l;

.field private final f:Lcom/google/android/apps/gmm/base/l/a/y;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/r/b/a/j;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/place/reservation/a;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;)V

    .line 37
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->q()Lcom/google/maps/g/q;

    move-result-object v0

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/reservation/p;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 39
    new-instance v1, Lcom/google/android/apps/gmm/place/reservation/y;

    invoke-direct {v1, p1, p3}, Lcom/google/android/apps/gmm/place/reservation/y;-><init>(Landroid/content/Context;Lcom/google/r/b/a/j;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/p;->b:Lcom/google/android/apps/gmm/place/reservation/y;

    .line 41
    sget v1, Lcom/google/android/apps/gmm/l;->ln:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 42
    invoke-virtual {v0}, Lcom/google/maps/g/q;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p4, v2, v3

    .line 41
    invoke-virtual {p1, v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/p;->c:Ljava/lang/String;

    .line 43
    sget-object v1, Lcom/google/b/f/t;->dR:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/reservation/ae;->a(Lcom/google/maps/g/q;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/p;->e:Lcom/google/android/apps/gmm/z/b/l;

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/base/l/al;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/l/al;-><init>()V

    sget v1, Lcom/google/android/apps/gmm/l;->lo:I

    .line 46
    invoke-virtual {p1, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/l/al;->a:Lcom/google/android/libraries/curvular/ah;

    .line 47
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/l/al;->a()Lcom/google/android/apps/gmm/base/l/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/p;->f:Lcom/google/android/apps/gmm/base/l/a/y;

    .line 48
    return-void
.end method


# virtual methods
.method public final c()Lcom/google/android/apps/gmm/place/reservation/b/i;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/p;->b:Lcom/google/android/apps/gmm/place/reservation/y;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/p;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/p;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->onBackPressed()V

    .line 63
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/p;->e:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/base/l/a/y;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/p;->f:Lcom/google/android/apps/gmm/base/l/a/y;

    return-object v0
.end method
