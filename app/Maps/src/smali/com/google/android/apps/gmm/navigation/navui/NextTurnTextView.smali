.class public Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;
.super Landroid/widget/TextView;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/directions/views/d;

.field public b:I

.field public c:F

.field public d:F

.field public e:F

.field private final f:Lcom/google/android/apps/gmm/shared/c/c/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->b:I

    .line 28
    const v0, 0x3fe66666    # 1.8f

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->c:F

    .line 29
    iput v1, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->d:F

    .line 30
    iput v1, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->e:F

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->f:Lcom/google/android/apps/gmm/shared/c/c/e;

    .line 40
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->a:Lcom/google/android/apps/gmm/directions/views/d;

    if-nez v0, :cond_0

    .line 67
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    :goto_0
    return-void

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->a:Lcom/google/android/apps/gmm/directions/views/d;

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->b:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/views/c;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/directions/views/d;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 70
    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->d:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->e:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->f:Lcom/google/android/apps/gmm/shared/c/c/e;

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->d:F

    iget v3, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->e:F

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/e;->a(Landroid/graphics/drawable/Drawable;FF)Landroid/text/Spannable;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->f:Lcom/google/android/apps/gmm/shared/c/c/e;

    sget v2, Lcom/google/android/apps/gmm/l;->eE:I

    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v4, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    const-string v1, "%s"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->f:Lcom/google/android/apps/gmm/shared/c/c/e;

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->c:F

    const-string v2, "\u00a0"

    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/d;

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/d;-><init>(Landroid/graphics/drawable/Drawable;F)V

    invoke-static {v3, v2}, Lcom/google/android/apps/gmm/shared/c/c/e;->a(Lcom/google/android/apps/gmm/shared/c/c/d;Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_1
.end method
