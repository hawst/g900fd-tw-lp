.class public Lcom/google/android/apps/gmm/map/internal/c/ao;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Lcom/google/android/apps/gmm/map/internal/c/z;

.field static final b:[Lcom/google/android/apps/gmm/map/internal/c/e;


# instance fields
.field A:Lcom/google/android/apps/gmm/map/internal/c/c;

.field public B:Lcom/google/android/apps/gmm/map/internal/c/cf;

.field C:Lcom/google/android/apps/gmm/map/internal/c/cu;

.field public D:Lcom/google/b/f/cq;

.field public E:Ljava/lang/Integer;

.field public F:Z

.field G:Lcom/google/android/apps/gmm/map/internal/c/bb;

.field H:Ljava/lang/String;

.field I:Ljava/lang/String;

.field c:J

.field public d:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field public e:Lcom/google/android/apps/gmm/map/b/a/y;

.field public f:Lcom/google/android/apps/gmm/map/b/a/j;

.field g:I

.field h:I

.field public i:[Lcom/google/android/apps/gmm/map/internal/c/a;

.field public j:Lcom/google/android/apps/gmm/map/internal/c/z;

.field public k:Lcom/google/android/apps/gmm/map/internal/c/z;

.field l:[Lcom/google/android/apps/gmm/map/internal/c/e;

.field m:Ljava/lang/String;

.field n:Lcom/google/android/apps/gmm/map/internal/c/be;

.field o:I

.field p:Ljava/lang/String;

.field public q:I

.field public r:I

.field s:I

.field t:F

.field public u:I

.field v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:Lcom/google/android/apps/gmm/map/internal/c/e;

.field y:[I

.field z:Lcom/google/android/apps/gmm/map/indoor/d/g;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 947
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/z;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/d;->b:Lcom/google/android/apps/gmm/map/internal/c/d;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/z;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/d;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->a:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 949
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/e;

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->b:[Lcom/google/android/apps/gmm/map/internal/c/e;

    return-void
.end method

.method constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 951
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->c:J

    .line 952
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->d:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 953
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 954
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->f:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 955
    iput v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->g:I

    .line 956
    iput v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->h:I

    .line 957
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->i:[Lcom/google/android/apps/gmm/map/internal/c/a;

    .line 958
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->a:Lcom/google/android/apps/gmm/map/internal/c/z;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 959
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->a:Lcom/google/android/apps/gmm/map/internal/c/z;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->k:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 960
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->b:[Lcom/google/android/apps/gmm/map/internal/c/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->l:[Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 961
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->m:Ljava/lang/String;

    .line 962
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->n:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 963
    iput v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->o:I

    .line 964
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->p:Ljava/lang/String;

    .line 965
    iput v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->q:I

    .line 966
    iput v4, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->r:I

    .line 967
    iput v4, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->s:I

    .line 968
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->t:F

    .line 969
    iput v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->u:I

    .line 970
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->v:Ljava/lang/String;

    .line 971
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->w:Ljava/lang/String;

    .line 972
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/e;->c:Lcom/google/android/apps/gmm/map/internal/c/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->x:Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 973
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/an;->a:[I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->y:[I

    .line 974
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->z:Lcom/google/android/apps/gmm/map/indoor/d/g;

    .line 975
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->A:Lcom/google/android/apps/gmm/map/internal/c/c;

    .line 976
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->B:Lcom/google/android/apps/gmm/map/internal/c/cf;

    .line 977
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->C:Lcom/google/android/apps/gmm/map/internal/c/cu;

    .line 978
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->D:Lcom/google/b/f/cq;

    .line 979
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->E:Ljava/lang/Integer;

    .line 980
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->F:Z

    .line 981
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->G:Lcom/google/android/apps/gmm/map/internal/c/bb;

    .line 982
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->H:Ljava/lang/String;

    .line 983
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->I:Ljava/lang/String;

    .line 986
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/an;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1124
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->A:Lcom/google/android/apps/gmm/map/internal/c/c;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ao;->A:Lcom/google/android/apps/gmm/map/internal/c/c;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/c;->c:I

    if-ne v1, v0, :cond_0

    :goto_0
    if-eqz v0, :cond_1

    .line 1125
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/au;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/c/au;-><init>(Lcom/google/android/apps/gmm/map/internal/c/ao;)V

    .line 1127
    :goto_1
    return-object v0

    .line 1124
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1127
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/c/an;-><init>(Lcom/google/android/apps/gmm/map/internal/c/ao;)V

    goto :goto_1
.end method
