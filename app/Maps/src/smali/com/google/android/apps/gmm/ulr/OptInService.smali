.class public Lcom/google/android/apps/gmm/ulr/OptInService;
.super Landroid/app/Service;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/common/c;
.implements Lcom/google/android/gms/common/d;


# instance fields
.field a:Lcom/google/android/gms/location/reporting/c;

.field private b:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 137
    return-void
.end method

.method static a(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 275
    if-nez p0, :cond_1

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    const-string v0, "messenger"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    .line 279
    if-eqz v0, :cond_0

    .line 284
    const/4 v1, 0x0

    invoke-static {v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 286
    :try_start_0
    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 291
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/OptInService;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 109
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 3

    .prologue
    .line 119
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Connection failed, result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/OptInService;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/ulr/OptInService;->b:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-static {v1, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 121
    return-void
.end method

.method public final am_()V
    .locals 3

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/OptInService;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/ulr/OptInService;->b:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 115
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 103
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not a bound service"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 81
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 82
    new-instance v0, Lcom/google/android/apps/gmm/ulr/b;

    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "ULR:OptInService"

    const/16 v3, 0xa

    invoke-direct {v1, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/ulr/b;-><init>(Lcom/google/android/apps/gmm/ulr/OptInService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/ulr/OptInService;->b:Landroid/os/Handler;

    .line 83
    new-instance v0, Lcom/google/android/gms/location/reporting/c;

    invoke-direct {v0, p0, p0, p0}, Lcom/google/android/gms/location/reporting/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/ulr/OptInService;->a:Lcom/google/android/gms/location/reporting/c;

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/OptInService;->a:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/c;->a()V

    .line 85
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/OptInService;->a:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/c;->c()V

    .line 91
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 92
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x25

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onStartCommand() received "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/OptInService;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/ulr/OptInService;->b:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {v1, v3, p3, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 98
    return v3
.end method
