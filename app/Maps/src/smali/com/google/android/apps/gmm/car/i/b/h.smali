.class public final Lcom/google/android/apps/gmm/car/i/b/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/i/b/g;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:[B
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final f:Lcom/google/android/apps/gmm/car/i/b/i;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/apps/gmm/car/i/b/i;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # [B
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/i/b/h;->a:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/google/android/apps/gmm/car/i/b/h;->b:Ljava/lang/String;

    .line 33
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p3, Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/i/b/h;->c:Ljava/lang/String;

    .line 34
    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p4, Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/gmm/car/i/b/h;->d:Ljava/lang/String;

    .line 35
    iput-object p5, p0, Lcom/google/android/apps/gmm/car/i/b/h;->e:[B

    .line 36
    if-nez p6, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p6, Lcom/google/android/apps/gmm/car/i/b/i;

    iput-object p6, p0, Lcom/google/android/apps/gmm/car/i/b/h;->f:Lcom/google/android/apps/gmm/car/i/b/i;

    .line 37
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 89
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-object p0

    .line 92
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 93
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    .line 96
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 97
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 98
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v1

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    move-object p0, v0

    .line 99
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/b/h;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/b/h;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/car/i/b/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/b/h;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/b/h;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/b/h;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/car/i/b/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/b/h;->e:[B

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 56
    iget-object v6, p0, Lcom/google/android/apps/gmm/car/i/b/h;->f:Lcom/google/android/apps/gmm/car/i/b/i;

    iget-object v7, p0, Lcom/google/android/apps/gmm/car/i/b/h;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/b/h;->e:[B

    if-nez v0, :cond_0

    move-object v0, v5

    :goto_0
    invoke-interface {v6, v7, v0}, Lcom/google/android/apps/gmm/car/i/b/i;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/car/bm;)V

    .line 57
    return-object v5

    .line 56
    :cond_0
    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->d()Lcom/google/android/apps/gmm/map/r/a/aq;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/b/h;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/b/h;->e:[B

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->h:[B

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/gmm/car/bm;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/b/h;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/i/b/h;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/i/b/h;->b:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/bm;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/base/g/c;)V

    goto :goto_0
.end method
