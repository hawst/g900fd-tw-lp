.class public final enum Lcom/google/android/apps/gmm/iamhere/c/f;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/iamhere/c/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum b:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum c:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum d:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum e:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum f:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum g:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum h:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum i:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum j:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum k:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum l:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum m:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum n:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum o:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum p:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public static final enum q:Lcom/google/android/apps/gmm/iamhere/c/f;

.field private static final synthetic s:[Lcom/google/android/apps/gmm/iamhere/c/f;


# instance fields
.field public final r:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 212
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "NO_ICON"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->a:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 213
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "BUS_STOP"

    sget v2, Lcom/google/android/apps/gmm/f;->eQ:I

    sget v3, Lcom/google/android/apps/gmm/f;->eR:I

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->b:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 214
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "DOWNLOAD"

    sget v2, Lcom/google/android/apps/gmm/f;->cX:I

    sget v3, Lcom/google/android/apps/gmm/f;->cY:I

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->c:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 215
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "EAT"

    sget v2, Lcom/google/android/apps/gmm/f;->dV:I

    sget v3, Lcom/google/android/apps/gmm/f;->dW:I

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->d:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 216
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "HOTEL"

    sget v2, Lcom/google/android/apps/gmm/f;->dr:I

    sget v3, Lcom/google/android/apps/gmm/f;->ds:I

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->e:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 217
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "LISTS"

    const/4 v2, 0x5

    sget v3, Lcom/google/android/apps/gmm/f;->dv:I

    sget v4, Lcom/google/android/apps/gmm/f;->dx:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->f:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 218
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "PHONE"

    const/4 v2, 0x6

    sget v3, Lcom/google/android/apps/gmm/f;->eC:I

    sget v4, Lcom/google/android/apps/gmm/f;->eD:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->g:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 219
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "PLACE"

    const/4 v2, 0x7

    sget v3, Lcom/google/android/apps/gmm/f;->eE:I

    sget v4, Lcom/google/android/apps/gmm/f;->eI:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->h:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 220
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "SEARCH"

    const/16 v2, 0x8

    sget v3, Lcom/google/android/apps/gmm/f;->eO:I

    sget v4, Lcom/google/android/apps/gmm/f;->eU:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->i:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 221
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "SHARE"

    const/16 v2, 0x9

    sget v3, Lcom/google/android/apps/gmm/f;->eY:I

    sget v4, Lcom/google/android/apps/gmm/f;->eZ:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->j:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 222
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "STAR"

    const/16 v2, 0xa

    sget v3, Lcom/google/android/apps/gmm/f;->eL:I

    sget v4, Lcom/google/android/apps/gmm/f;->eM:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->k:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 223
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "STORE"

    const/16 v2, 0xb

    sget v3, Lcom/google/android/apps/gmm/f;->fj:I

    sget v4, Lcom/google/android/apps/gmm/f;->fk:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->l:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 224
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "TICKET"

    const/16 v2, 0xc

    sget v3, Lcom/google/android/apps/gmm/f;->bS:I

    sget v4, Lcom/google/android/apps/gmm/f;->bT:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->m:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 225
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "TRAIN"

    const/16 v2, 0xd

    sget v3, Lcom/google/android/apps/gmm/f;->eS:I

    sget v4, Lcom/google/android/apps/gmm/f;->eT:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->n:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 226
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "TRANSIT_TIMETABLE"

    const/16 v2, 0xe

    sget v3, Lcom/google/android/apps/gmm/f;->fv:I

    sget v4, Lcom/google/android/apps/gmm/f;->fw:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->o:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 227
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "WEBSITE"

    const/16 v2, 0xf

    sget v3, Lcom/google/android/apps/gmm/f;->fz:I

    sget v4, Lcom/google/android/apps/gmm/f;->fA:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->p:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 228
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    const-string v1, "WORK"

    const/16 v2, 0x10

    sget v3, Lcom/google/android/apps/gmm/f;->fB:I

    sget v4, Lcom/google/android/apps/gmm/f;->fD:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/c/f;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->q:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 211
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/google/android/apps/gmm/iamhere/c/f;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/f;->a:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/f;->b:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/f;->c:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/f;->d:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/f;->e:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/f;->f:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/f;->g:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/f;->h:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/f;->i:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/f;->j:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/f;->k:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/f;->l:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/f;->m:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/f;->n:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/f;->o:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/f;->p:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/f;->q:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->s:[Lcom/google/android/apps/gmm/iamhere/c/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 240
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 241
    iput p3, p0, Lcom/google/android/apps/gmm/iamhere/c/f;->r:I

    .line 242
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/iamhere/c/f;
    .locals 1

    .prologue
    .line 211
    const-class v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/f;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/iamhere/c/f;
    .locals 1

    .prologue
    .line 211
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/f;->s:[Lcom/google/android/apps/gmm/iamhere/c/f;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/iamhere/c/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/iamhere/c/f;

    return-object v0
.end method
