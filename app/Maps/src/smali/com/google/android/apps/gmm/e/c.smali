.class public Lcom/google/android/apps/gmm/e/c;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/common/api/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/base/j/c;",
        "Lcom/google/android/gms/common/api/v",
        "<",
        "Lcom/google/android/gms/common/api/Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/google/android/gms/common/api/o;

.field private c:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/gmm/e/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/e/c;->a:Ljava/lang/String;

    .line 49
    new-instance v0, Lcom/google/android/gms/appdatasearch/e;

    const-string v1, "SsbContext"

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/e;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 51
    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/e;->a(Z)Lcom/google/android/gms/appdatasearch/e;

    move-result-object v0

    const-string v1, "blob"

    .line 52
    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/e;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/e;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/e;->a()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    .line 49
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 64
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/e/c;->c:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 140
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/j/c;->e:Z

    if-nez v0, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->l()Lcom/google/r/b/a/wn;

    move-result-object v0

    .line 145
    iget-boolean v0, v0, Lcom/google/r/b/a/wn;->f:Z

    if-nez v0, :cond_2

    .line 146
    sget-object v0, Lcom/google/android/apps/gmm/e/c;->a:Ljava/lang/String;

    goto :goto_0

    .line 149
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/e/c;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x15

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v1, v3

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "reportSearchContext "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    invoke-static {}, Lcom/google/android/d/b;->newBuilder()Lcom/google/android/d/d;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/e/c;->c:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v4

    iget v0, v1, Lcom/google/android/d/d;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/android/d/d;->a:I

    iput-wide v4, v1, Lcom/google/android/d/d;->b:J

    invoke-static {}, Lcom/google/i/a/k;->newBuilder()Lcom/google/i/a/m;

    move-result-object v0

    if-nez p1, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget v3, v0, Lcom/google/i/a/m;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, v0, Lcom/google/i/a/m;->a:I

    iput-object p1, v0, Lcom/google/i/a/m;->c:Ljava/lang/Object;

    invoke-static {}, Lcom/google/i/a/b;->newBuilder()Lcom/google/i/a/d;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getPackageName()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget v5, v3, Lcom/google/i/a/d;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v3, Lcom/google/i/a/d;->a:I

    iput-object v4, v3, Lcom/google/i/a/d;->b:Ljava/lang/Object;

    if-nez p2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget v4, v3, Lcom/google/i/a/d;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, v3, Lcom/google/i/a/d;->a:I

    iput-object p2, v3, Lcom/google/i/a/d;->c:Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/i/a/m;->b:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/i/a/d;->g()Lcom/google/n/t;

    move-result-object v3

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v7, v4, Lcom/google/n/ao;->d:Z

    iget v3, v0, Lcom/google/i/a/m;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, v0, Lcom/google/i/a/m;->a:I

    invoke-virtual {v0}, Lcom/google/i/a/m;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/k;

    invoke-virtual {v1, v0}, Lcom/google/android/d/d;->a(Lcom/google/i/a/k;)Lcom/google/android/d/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/d/d;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/android/d/b;

    .line 153
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v1

    if-nez v1, :cond_6

    sget-object v0, Lcom/google/android/apps/gmm/e/c;->a:Ljava/lang/String;

    move-object v0, v2

    .line 155
    :goto_1
    if-eqz v0, :cond_0

    .line 158
    new-instance v1, Lcom/google/android/gms/appdatasearch/f;

    invoke-direct {v1}, Lcom/google/android/gms/appdatasearch/f;-><init>()V

    .line 159
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/google/android/gms/appdatasearch/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/f;

    move-result-object v1

    .line 160
    invoke-virtual {v1, v0}, Lcom/google/android/gms/appdatasearch/f;->a(Lcom/google/android/gms/appdatasearch/DocumentContents;)Lcom/google/android/gms/appdatasearch/f;

    move-result-object v0

    .line 161
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/appdatasearch/f;->a(J)Lcom/google/android/gms/appdatasearch/f;

    move-result-object v0

    const/4 v1, 0x4

    .line 162
    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/f;->a(I)Lcom/google/android/gms/appdatasearch/f;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/f;->a()Lcom/google/android/gms/appdatasearch/UsageInfo;

    move-result-object v0

    .line 164
    sget-object v1, Lcom/google/android/gms/appdatasearch/a;->c:Lcom/google/android/gms/appdatasearch/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/e/c;->b:Lcom/google/android/gms/common/api/o;

    new-array v3, v7, [Lcom/google/android/gms/appdatasearch/UsageInfo;

    aput-object v0, v3, v6

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/appdatasearch/g;->a(Lcom/google/android/gms/common/api/o;[Lcom/google/android/gms/appdatasearch/UsageInfo;)Lcom/google/android/gms/common/api/s;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/s;->a(Lcom/google/android/gms/common/api/v;)V

    goto/16 :goto_0

    .line 153
    :cond_6
    new-instance v2, Lcom/google/android/gms/appdatasearch/c;

    invoke-direct {v2}, Lcom/google/android/gms/appdatasearch/c;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/d/b;->l()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/DocumentSection;->a([B)Lcom/google/android/gms/appdatasearch/DocumentSection;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/appdatasearch/c;->a(Lcom/google/android/gms/appdatasearch/DocumentSection;)Lcom/google/android/gms/appdatasearch/c;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/c;->a(Landroid/accounts/Account;)Lcom/google/android/gms/appdatasearch/c;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/gms/appdatasearch/c;->a(Z)Lcom/google/android/gms/appdatasearch/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/c;->a()Lcom/google/android/gms/appdatasearch/DocumentContents;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final Y_()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 89
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 69
    new-instance v0, Lcom/google/android/gms/common/api/p;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/p;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/appdatasearch/a;->b:Lcom/google/android/gms/common/api/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/p;->a()Lcom/google/android/gms/common/api/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/e/c;->b:Lcom/google/android/gms/common/api/o;

    .line 70
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/e/a/a;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 115
    iget-object v1, p1, Lcom/google/android/apps/gmm/e/a/a;->a:Lcom/google/android/apps/gmm/base/g/c;

    .line 116
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "http"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "maps.google.com"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "maps"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "q"

    invoke-static {v2}, Lcom/google/android/apps/gmm/base/g/c;->a(Lcom/google/android/apps/gmm/map/b/a/q;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    :goto_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 117
    const-string v2, "?"

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 120
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "?"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 124
    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    const-string v3, ""

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    :goto_2
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->j()Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/e/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    return-void

    .line 116
    :cond_1
    const-string v2, "ftid"

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/j;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 122
    :cond_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 125
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public a(Lcom/google/android/apps/gmm/search/ak;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p1, Lcom/google/android/apps/gmm/search/ak;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/t;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 190
    iget-object v1, p1, Lcom/google/android/apps/gmm/search/ak;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/e/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/w/a/b;)V
    .locals 0
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 131
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/u;)V
    .locals 3

    .prologue
    .line 35
    check-cast p1, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->b()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/e/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/Exception;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 94
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->b()V

    .line 95
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->f()V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/e/c;->b:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->b()V

    .line 83
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->g()V

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/e/c;->b:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->c()V

    .line 101
    return-void
.end method
