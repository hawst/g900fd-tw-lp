.class public final enum Lcom/google/android/apps/gmm/directions/f/b/p;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/f/b/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/directions/f/b/p;",
        ">;",
        "Lcom/google/android/apps/gmm/directions/f/b/q;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/directions/f/b/p;

.field public static final enum b:Lcom/google/android/apps/gmm/directions/f/b/p;

.field public static final enum c:Lcom/google/android/apps/gmm/directions/f/b/p;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/directions/f/b/p;


# instance fields
.field final d:Lcom/google/maps/g/a/gs;

.field private final e:I

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 15
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/p;

    const-string v1, "DEFAULT_SORT_ORDER"

    sget-object v4, Lcom/google/maps/g/a/gs;->a:Lcom/google/maps/g/a/gs;

    sget v5, Lcom/google/android/apps/gmm/l;->fH:I

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/f/b/p;-><init>(Ljava/lang/String;IILcom/google/maps/g/a/gs;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/b/p;->a:Lcom/google/android/apps/gmm/directions/f/b/p;

    .line 17
    new-instance v3, Lcom/google/android/apps/gmm/directions/f/b/p;

    const-string v4, "FEWER_TRANSFER"

    sget-object v7, Lcom/google/maps/g/a/gs;->b:Lcom/google/maps/g/a/gs;

    sget v8, Lcom/google/android/apps/gmm/l;->fI:I

    move v5, v9

    move v6, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/directions/f/b/p;-><init>(Ljava/lang/String;IILcom/google/maps/g/a/gs;I)V

    sput-object v3, Lcom/google/android/apps/gmm/directions/f/b/p;->b:Lcom/google/android/apps/gmm/directions/f/b/p;

    .line 19
    new-instance v3, Lcom/google/android/apps/gmm/directions/f/b/p;

    const-string v4, "LESS_WALKING"

    sget-object v7, Lcom/google/maps/g/a/gs;->c:Lcom/google/maps/g/a/gs;

    sget v8, Lcom/google/android/apps/gmm/l;->fJ:I

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/directions/f/b/p;-><init>(Ljava/lang/String;IILcom/google/maps/g/a/gs;I)V

    sput-object v3, Lcom/google/android/apps/gmm/directions/f/b/p;->c:Lcom/google/android/apps/gmm/directions/f/b/p;

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/directions/f/b/p;

    sget-object v1, Lcom/google/android/apps/gmm/directions/f/b/p;->a:Lcom/google/android/apps/gmm/directions/f/b/p;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/directions/f/b/p;->b:Lcom/google/android/apps/gmm/directions/f/b/p;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/apps/gmm/directions/f/b/p;->c:Lcom/google/android/apps/gmm/directions/f/b/p;

    aput-object v1, v0, v10

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/b/p;->g:[Lcom/google/android/apps/gmm/directions/f/b/p;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILcom/google/maps/g/a/gs;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/maps/g/a/gs;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 27
    iput p3, p0, Lcom/google/android/apps/gmm/directions/f/b/p;->f:I

    .line 28
    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/f/b/p;->d:Lcom/google/maps/g/a/gs;

    .line 29
    iput p5, p0, Lcom/google/android/apps/gmm/directions/f/b/p;->e:I

    .line 30
    return-void
.end method

.method public static a(I)Lcom/google/android/apps/gmm/directions/f/b/p;
    .locals 5

    .prologue
    .line 41
    invoke-static {}, Lcom/google/android/apps/gmm/directions/f/b/p;->values()[Lcom/google/android/apps/gmm/directions/f/b/p;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 42
    iget v4, v0, Lcom/google/android/apps/gmm/directions/f/b/p;->f:I

    if-ne v4, p0, :cond_0

    .line 46
    :goto_1
    return-object v0

    .line 41
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 46
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/directions/f/b/p;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/android/apps/gmm/directions/f/b/p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/f/b/p;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/directions/f/b/p;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/b/p;->g:[Lcom/google/android/apps/gmm/directions/f/b/p;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/directions/f/b/p;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/directions/f/b/p;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/google/android/apps/gmm/directions/f/b/p;->e:I

    return v0
.end method
