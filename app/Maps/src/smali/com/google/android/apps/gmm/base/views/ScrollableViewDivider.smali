.class public Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/google/android/apps/gmm/base/views/av;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field public a:I

.field public b:Landroid/view/View;

.field public c:Landroid/view/View;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/h;->h:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/n;->Y:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/n;->Z:I

    const/4 v2, -0x1

    .line 45
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->a:I

    .line 46
    sget v0, Lcom/google/android/apps/gmm/g;->dm:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->b:Landroid/view/View;

    .line 47
    sget v0, Lcom/google/android/apps/gmm/g;->as:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->c:Landroid/view/View;

    .line 48
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 120
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->e:Z

    if-ne p1, v0, :cond_0

    .line 126
    :goto_0
    return-void

    .line 123
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->e:Z

    .line 124
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->b:Landroid/view/View;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->c:Landroid/view/View;

    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 124
    goto :goto_1

    :cond_2
    move v2, v1

    .line 125
    goto :goto_2
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    .line 52
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 53
    iget v1, p0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->a:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_0

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 54
    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->a(Z)V

    .line 55
    return-void

    .line 53
    :cond_1
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/AbsListView;

    if-eqz v1, :cond_2

    check-cast v0, Landroid/widget/AbsListView;

    invoke-virtual {v0, p0}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    goto :goto_0

    :cond_2
    instance-of v1, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->j:Lcom/google/android/apps/gmm/base/views/av;

    goto :goto_0

    :cond_3
    instance-of v1, v0, Landroid/widget/ScrollView;

    if-eqz v1, :cond_4

    check-cast v0, Landroid/widget/ScrollView;

    invoke-virtual {v0, p0}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->d:Ljava/lang/String;

    const-string v2, "Target view not supported. Expected instance of AbsListView or ScrollView, got object of class: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    if-nez v0, :cond_5

    const-string v0, "null"

    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 96
    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 97
    if-eqz v1, :cond_1

    .line 98
    if-nez p2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->a(Z)V

    .line 100
    :cond_1
    return-void
.end method

.method public onScrollChanged(IIII)V
    .locals 1

    .prologue
    .line 109
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->a(Z)V

    .line 110
    return-void

    .line 109
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 114
    check-cast p1, Landroid/widget/ScrollView;

    invoke-virtual {p1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->a(Z)V

    .line 115
    return v1

    :cond_0
    move v0, v1

    .line 114
    goto :goto_0
.end method
