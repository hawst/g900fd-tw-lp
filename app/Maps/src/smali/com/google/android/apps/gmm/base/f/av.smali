.class public Lcom/google/android/apps/gmm/base/f/av;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/base/l/v;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method

.method private static varargs a(Lcom/google/android/libraries/curvular/cu;Lcom/google/android/libraries/curvular/cu;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 10

    .prologue
    .line 193
    const/4 v0, 0x2

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const/4 v2, 0x4

    new-array v2, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v3, 0x0

    const/16 v4, 0x14

    .line 196
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v5, v4, v6, v7}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    .line 197
    sget-object v5, Lcom/google/android/libraries/curvular/g;->bs:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const/4 v4, -0x1

    .line 198
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const/4 v4, -0x1

    .line 199
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v2, v3

    .line 195
    new-instance v3, Lcom/google/android/libraries/curvular/cs;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v5, 0x0

    const-class v6, Lcom/google/android/apps/gmm/base/views/WebImageView;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v3, v4}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;Z)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v4, 0x0

    aput-object p0, v2, v4

    .line 200
    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;Z)V

    aput-object v3, v1, v0

    const/4 v2, 0x1

    const/16 v0, 0xb

    new-array v3, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    .line 202
    const-string v4, "com.google.android.apps.gmm:drawable/quantum_gradient_bar_bottom"

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/String;)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    const/16 v4, 0xc

    .line 203
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v5, v4, v6, v7}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    const/4 v4, -0x1

    .line 205
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x3

    const-wide/high16 v6, 0x4048000000000000L    # 48.0

    .line 206
    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v6, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v5, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v4, 0x4

    .line 209
    const-wide/high16 v6, 0x4020000000000000L    # 8.0

    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_1

    double-to-int v6, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v5, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v4, 0x5

    .line 210
    const-wide/high16 v6, 0x4020000000000000L    # 8.0

    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_2

    double-to-int v6, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v5, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bh:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x6

    const/4 v4, 0x5

    .line 212
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bH:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x7

    const/16 v4, 0x50

    .line 213
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x8

    .line 214
    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v5, 0x0

    sget v6, Lcom/google/android/apps/gmm/m;->l:I

    invoke-static {v6}, Lcom/google/android/libraries/curvular/c;->d(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->bJ:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget v6, Lcom/google/android/apps/gmm/k;->ab:I

    invoke-static {v6}, Lcom/google/android/libraries/curvular/c;->e(I)Lcom/google/android/libraries/curvular/bl;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->bT:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, Lcom/google/android/libraries/curvular/ar;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/ar;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x9

    .line 215
    sget v4, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0xa

    aput-object p1, v3, v0

    .line 201
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v3}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.TextView"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v2

    .line 194
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.RelativeLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    .line 218
    invoke-virtual {v0, p2}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    .line 206
    :cond_0
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v8

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    .line 209
    :cond_1
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v8

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    .line 210
    :cond_2
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v8

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 20

    .prologue
    .line 58
    const/4 v2, 0x2

    new-array v3, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v4, 0x0

    .line 59
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    sget-object v5, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v5, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v4, 0x1

    const/4 v2, 0x7

    new-array v5, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 61
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v6, 0x1

    .line 64
    const-wide/high16 v8, 0x4010000000000000L    # 4.0

    const-wide/high16 v10, 0x4010000000000000L    # 4.0

    const-wide/high16 v12, 0x4050000000000000L    # 64.0

    const-wide/high16 v14, 0x4010000000000000L    # 4.0

    new-instance v7, Lcom/google/android/libraries/curvular/as;

    new-instance v16, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_6

    double-to-int v8, v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_0
    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v10, v11}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_7

    double-to-int v9, v10

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v10, 0xffffff

    and-int/2addr v9, v10

    shl-int/lit8 v9, v9, 0x8

    or-int/lit8 v9, v9, 0x1

    iput v9, v2, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v9, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_8

    double-to-int v10, v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v10, v11

    shl-int/lit8 v10, v10, 0x8

    or-int/lit8 v10, v10, 0x1

    iput v10, v2, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v9, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v10, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_9

    double-to-int v11, v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v12, 0xffffff

    and-int/2addr v11, v12

    shl-int/lit8 v11, v11, 0x8

    or-int/lit8 v11, v11, 0x1

    iput v11, v2, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v10, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    move-object/from16 v0, v16

    invoke-direct {v7, v0, v8, v9, v10}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v6, 0x2

    .line 65
    const-wide/high16 v8, 0x4010000000000000L    # 4.0

    const-wide/high16 v10, 0x4010000000000000L    # 4.0

    const-wide/high16 v12, 0x4050000000000000L    # 64.0

    const-wide/high16 v14, 0x4010000000000000L    # 4.0

    new-instance v7, Lcom/google/android/libraries/curvular/as;

    new-instance v16, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_a

    double-to-int v8, v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_4
    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v10, v11}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_b

    double-to-int v9, v10

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v10, 0xffffff

    and-int/2addr v9, v10

    shl-int/lit8 v9, v9, 0x8

    or-int/lit8 v9, v9, 0x1

    iput v9, v2, Landroid/util/TypedValue;->data:I

    :goto_5
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v9, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_c

    double-to-int v10, v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v10, v11

    shl-int/lit8 v10, v10, 0x8

    or-int/lit8 v10, v10, 0x1

    iput v10, v2, Landroid/util/TypedValue;->data:I

    :goto_6
    invoke-direct {v9, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v10, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_d

    double-to-int v11, v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v12, 0xffffff

    and-int/2addr v11, v12

    shl-int/lit8 v11, v11, 0x8

    or-int/lit8 v11, v11, 0x1

    iput v11, v2, Landroid/util/TypedValue;->data:I

    :goto_7
    invoke-direct {v10, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    move-object/from16 v0, v16

    invoke-direct {v7, v0, v8, v9, v10}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ao:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v6, 0x3

    const-wide/high16 v8, 0x4010000000000000L    # 4.0

    .line 66
    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_e

    double-to-int v8, v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_8
    invoke-direct {v7, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->an:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v6, 0x4

    .line 68
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->n()Ljava/lang/Boolean;

    move-result-object v7

    const-wide/high16 v8, 0x406d000000000000L    # 232.0

    .line 69
    new-instance v10, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_f

    double-to-int v8, v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_9
    invoke-direct {v10, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    const-wide/high16 v10, 0x405c000000000000L    # 112.0

    .line 70
    new-instance v9, Lcom/google/android/libraries/curvular/b;

    invoke-static {v10, v11}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_10

    double-to-int v10, v10

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v10, v11

    shl-int/lit8 v10, v10, 0x8

    or-int/lit8 v10, v10, 0x1

    iput v10, v2, Landroid/util/TypedValue;->data:I

    :goto_a
    invoke-direct {v9, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    .line 68
    invoke-static {v7, v8, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v6, 0x5

    .line 75
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->b()Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v2

    sget-object v7, Lcom/google/android/apps/gmm/base/k/j;->aj:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v7, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    .line 76
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->c()Ljava/lang/String;

    move-result-object v2

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    const/16 v2, 0x8

    new-array v9, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v10, 0x0

    .line 77
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->d()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    sget-object v11, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v11, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v9, v10

    const/4 v10, 0x1

    .line 79
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->n()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    sget-object v12, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    const/16 v12, 0x8

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    invoke-static {v2, v11, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v9, v10

    const/4 v10, 0x2

    .line 82
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->q()Lcom/google/android/libraries/curvular/cf;

    move-result-object v11

    const/4 v2, 0x0

    if-eqz v11, :cond_0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v11, v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v11

    new-instance v2, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v2, v11}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_0
    sget-object v11, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v9, v10

    const/4 v10, 0x3

    .line 83
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->p()Ljava/lang/Boolean;

    move-result-object v2

    sget-object v11, Lcom/google/android/libraries/curvular/g;->s:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v9, v10

    const/4 v2, 0x4

    const/4 v10, -0x1

    .line 85
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    sget-object v11, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v9, v2

    const/4 v10, 0x5

    const-wide/16 v12, 0x0

    .line 86
    new-instance v11, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_11

    double-to-int v12, v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v13, 0xffffff

    and-int/2addr v12, v13

    shl-int/lit8 v12, v12, 0x8

    or-int/lit8 v12, v12, 0x1

    iput v12, v2, Landroid/util/TypedValue;->data:I

    :goto_b
    invoke-direct {v11, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v9, v10

    const/4 v2, 0x6

    const/high16 v10, 0x3f800000    # 1.0f

    .line 87
    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    sget-object v11, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v9, v2

    const/4 v10, 0x7

    const-wide/high16 v12, 0x4010000000000000L    # 4.0

    .line 90
    new-instance v11, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_12

    double-to-int v12, v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v13, 0xffffff

    and-int/2addr v12, v13

    shl-int/lit8 v12, v12, 0x8

    or-int/lit8 v12, v12, 0x1

    iput v12, v2, Landroid/util/TypedValue;->data:I

    :goto_c
    invoke-direct {v11, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->an:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v9, v10

    .line 74
    invoke-static {v7, v8, v9}, Lcom/google/android/apps/gmm/base/f/av;->a(Lcom/google/android/libraries/curvular/cu;Lcom/google/android/libraries/curvular/cu;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v6, 0x6

    const/4 v2, 0x7

    new-array v7, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 93
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v2, 0x1

    const/4 v8, -0x1

    .line 94
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v8, 0x2

    const-wide/16 v10, 0x0

    .line 95
    new-instance v9, Lcom/google/android/libraries/curvular/b;

    invoke-static {v10, v11}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_13

    double-to-int v10, v10

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v10, v11

    shl-int/lit8 v10, v10, 0x8

    or-int/lit8 v10, v10, 0x1

    iput v10, v2, Landroid/util/TypedValue;->data:I

    :goto_d
    invoke-direct {v9, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v7, v8

    const/4 v2, 0x3

    const/high16 v8, 0x3f800000    # 1.0f

    .line 96
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v8, 0x4

    .line 103
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->n()Ljava/lang/Boolean;

    move-result-object v9

    .line 104
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->e()Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v2

    sget-object v10, Lcom/google/android/apps/gmm/base/k/j;->aj:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v10, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    .line 105
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->b()Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v2

    sget-object v11, Lcom/google/android/apps/gmm/base/k/j;->aj:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v11, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    .line 103
    invoke-static {v9, v10, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    .line 107
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->n()Ljava/lang/Boolean;

    move-result-object v10

    .line 108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->f()Ljava/lang/String;

    move-result-object v2

    sget-object v11, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    .line 109
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->c()Ljava/lang/String;

    move-result-object v2

    sget-object v12, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    .line 107
    invoke-static {v10, v11, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    const/4 v2, 0x5

    new-array v11, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v12, 0x0

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->n()Ljava/lang/Boolean;

    move-result-object v13

    .line 113
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->s()Lcom/google/android/libraries/curvular/cf;

    move-result-object v14

    const/4 v2, 0x0

    if-eqz v14, :cond_1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v14, v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v14

    new-instance v2, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v2, v14}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_1
    sget-object v14, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->q()Lcom/google/android/libraries/curvular/cf;

    move-result-object v15

    const/4 v2, 0x0

    if-eqz v15, :cond_2

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v15

    new-instance v2, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v2, v15}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_2
    sget-object v15, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    .line 112
    invoke-static {v13, v14, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x1

    .line 116
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->n()Ljava/lang/Boolean;

    move-result-object v13

    .line 117
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->r()Ljava/lang/Boolean;

    move-result-object v2

    sget-object v14, Lcom/google/android/libraries/curvular/g;->s:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    .line 118
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->p()Ljava/lang/Boolean;

    move-result-object v2

    sget-object v15, Lcom/google/android/libraries/curvular/g;->s:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    .line 116
    invoke-static {v13, v14, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x2

    .line 120
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->n()Ljava/lang/Boolean;

    move-result-object v13

    .line 121
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->g()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    sget-object v14, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v14, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    .line 122
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->d()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    sget-object v15, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    .line 120
    invoke-static {v13, v14, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x3

    const-wide/16 v14, 0x0

    .line 125
    new-instance v13, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_14

    double-to-int v14, v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v15, 0xffffff

    and-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x8

    or-int/lit8 v14, v14, 0x1

    iput v14, v2, Landroid/util/TypedValue;->data:I

    :goto_e
    invoke-direct {v13, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x4

    .line 126
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->m()Ljava/lang/Boolean;

    move-result-object v2

    const/high16 v13, 0x3f800000    # 1.0f

    .line 127
    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    const/high16 v14, 0x40000000    # 2.0f

    .line 128
    invoke-static {v14}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    .line 126
    invoke-static {v2, v13, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    .line 102
    invoke-static {v9, v10, v11}, Lcom/google/android/apps/gmm/base/f/av;->a(Lcom/google/android/libraries/curvular/cu;Lcom/google/android/libraries/curvular/cu;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v7, v8

    const/4 v8, 0x5

    .line 136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->n()Ljava/lang/Boolean;

    move-result-object v9

    .line 137
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->h()Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v2

    sget-object v10, Lcom/google/android/apps/gmm/base/k/j;->aj:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v10, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    .line 138
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->e()Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v2

    sget-object v11, Lcom/google/android/apps/gmm/base/k/j;->aj:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v11, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    .line 136
    invoke-static {v9, v10, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    .line 140
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->n()Ljava/lang/Boolean;

    move-result-object v10

    .line 141
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->i()Ljava/lang/String;

    move-result-object v2

    sget-object v11, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    .line 142
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->f()Ljava/lang/String;

    move-result-object v2

    sget-object v12, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    .line 140
    invoke-static {v10, v11, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    const/4 v2, 0x7

    new-array v11, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v12, 0x0

    .line 144
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->n()Ljava/lang/Boolean;

    move-result-object v13

    .line 145
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->u()Lcom/google/android/libraries/curvular/cf;

    move-result-object v14

    const/4 v2, 0x0

    if-eqz v14, :cond_3

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v14, v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v14

    new-instance v2, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v2, v14}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_3
    sget-object v14, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    .line 146
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->s()Lcom/google/android/libraries/curvular/cf;

    move-result-object v15

    const/4 v2, 0x0

    if-eqz v15, :cond_4

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v15

    new-instance v2, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v2, v15}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_4
    sget-object v15, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    .line 144
    invoke-static {v13, v14, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x1

    .line 148
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->n()Ljava/lang/Boolean;

    move-result-object v13

    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->t()Ljava/lang/Boolean;

    move-result-object v2

    sget-object v14, Lcom/google/android/libraries/curvular/g;->s:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    .line 150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->r()Ljava/lang/Boolean;

    move-result-object v2

    sget-object v15, Lcom/google/android/libraries/curvular/g;->s:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    .line 148
    invoke-static {v13, v14, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x2

    .line 152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->n()Ljava/lang/Boolean;

    move-result-object v13

    .line 153
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->j()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    sget-object v14, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v14, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    .line 154
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->g()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    sget-object v15, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    .line 152
    invoke-static {v13, v14, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x3

    .line 157
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->l()Ljava/lang/Boolean;

    move-result-object v2

    const/16 v13, 0x8

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    invoke-static {v2, v13, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x4

    const-wide/16 v14, 0x0

    .line 159
    new-instance v13, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_15

    double-to-int v14, v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v15, 0xffffff

    and-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x8

    or-int/lit8 v14, v14, 0x1

    iput v14, v2, Landroid/util/TypedValue;->data:I

    :goto_f
    invoke-direct {v13, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x5

    .line 161
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->o()Ljava/lang/Boolean;

    move-result-object v2

    const v13, 0x3f99999a    # 1.2f

    .line 162
    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    const/high16 v14, 0x3f800000    # 1.0f

    .line 163
    invoke-static {v14}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    .line 161
    invoke-static {v2, v13, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x6

    const-wide/high16 v14, 0x4010000000000000L    # 4.0

    .line 166
    new-instance v13, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_16

    double-to-int v14, v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v15, 0xffffff

    and-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x8

    or-int/lit8 v14, v14, 0x1

    iput v14, v2, Landroid/util/TypedValue;->data:I

    :goto_10
    invoke-direct {v13, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    .line 135
    invoke-static {v9, v10, v11}, Lcom/google/android/apps/gmm/base/f/av;->a(Lcom/google/android/libraries/curvular/cu;Lcom/google/android/libraries/curvular/cu;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v7, v8

    const/4 v8, 0x6

    .line 170
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->h()Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v2

    sget-object v9, Lcom/google/android/apps/gmm/base/k/j;->aj:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v9, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->i()Ljava/lang/String;

    move-result-object v2

    sget-object v10, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    const/4 v2, 0x7

    new-array v11, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v12, 0x0

    .line 172
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->j()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    sget-object v13, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v13, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x1

    .line 174
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->o()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    const/16 v14, 0x8

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    invoke-static {v2, v13, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x2

    .line 177
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->u()Lcom/google/android/libraries/curvular/cf;

    move-result-object v13

    const/4 v2, 0x0

    if-eqz v13, :cond_5

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v13, v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v13

    new-instance v2, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v2, v13}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_5
    sget-object v13, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x3

    .line 178
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/v;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/v;->t()Ljava/lang/Boolean;

    move-result-object v2

    sget-object v13, Lcom/google/android/libraries/curvular/g;->s:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x4

    const-wide/16 v14, 0x0

    .line 180
    new-instance v13, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_17

    double-to-int v14, v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v15, 0xffffff

    and-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x8

    or-int/lit8 v14, v14, 0x1

    iput v14, v2, Landroid/util/TypedValue;->data:I

    :goto_11
    invoke-direct {v13, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v2, 0x5

    const v12, 0x3f4ccccd    # 0.8f

    .line 181
    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v12, 0x6

    const-wide/high16 v14, 0x4010000000000000L    # 4.0

    .line 184
    new-instance v13, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_18

    double-to-int v14, v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v15, 0xffffff

    and-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x8

    or-int/lit8 v14, v14, 0x1

    iput v14, v2, Landroid/util/TypedValue;->data:I

    :goto_12
    invoke-direct {v13, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    .line 169
    invoke-static {v9, v10, v11}, Lcom/google/android/apps/gmm/base/f/av;->a(Lcom/google/android/libraries/curvular/cu;Lcom/google/android/libraries/curvular/cu;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v7, v8

    .line 92
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v7}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v7, "android.widget.LinearLayout"

    sget-object v8, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v5, v6

    .line 60
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v5}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v5, "android.widget.LinearLayout"

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v3, v4

    .line 58
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v3}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.FrameLayout"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    return-object v2

    .line 64
    :cond_6
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v8, v8, v18

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    :cond_7
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v10, v10, v18

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v10, v11, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v9

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v10, 0xffffff

    and-int/2addr v9, v10

    shl-int/lit8 v9, v9, 0x8

    or-int/lit8 v9, v9, 0x11

    iput v9, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    :cond_8
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v10, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v10, v11, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v10

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v10, v11

    shl-int/lit8 v10, v10, 0x8

    or-int/lit8 v10, v10, 0x11

    iput v10, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    :cond_9
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v12, v14

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v12, v13, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v11

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v12, 0xffffff

    and-int/2addr v11, v12

    shl-int/lit8 v11, v11, 0x8

    or-int/lit8 v11, v11, 0x11

    iput v11, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3

    .line 65
    :cond_a
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v8, v8, v18

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_4

    :cond_b
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v10, v10, v18

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v10, v11, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v9

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v10, 0xffffff

    and-int/2addr v9, v10

    shl-int/lit8 v9, v9, 0x8

    or-int/lit8 v9, v9, 0x11

    iput v9, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_5

    :cond_c
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v10, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v10, v11, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v10

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v10, v11

    shl-int/lit8 v10, v10, 0x8

    or-int/lit8 v10, v10, 0x11

    iput v10, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_6

    :cond_d
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v12, v14

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v12, v13, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v11

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v12, 0xffffff

    and-int/2addr v11, v12

    shl-int/lit8 v11, v11, 0x8

    or-int/lit8 v11, v11, 0x11

    iput v11, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_7

    .line 66
    :cond_e
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v10

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_8

    .line 69
    :cond_f
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_9

    .line 70
    :cond_10
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v10, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v10, v11, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v10

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v10, v11

    shl-int/lit8 v10, v10, 0x8

    or-int/lit8 v10, v10, 0x11

    iput v10, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_a

    .line 86
    :cond_11
    const-wide/high16 v14, 0x4060000000000000L    # 128.0

    mul-double/2addr v12, v14

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v12, v13, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v13, 0xffffff

    and-int/2addr v12, v13

    shl-int/lit8 v12, v12, 0x8

    or-int/lit8 v12, v12, 0x11

    iput v12, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_b

    .line 90
    :cond_12
    const-wide/high16 v14, 0x4060000000000000L    # 128.0

    mul-double/2addr v12, v14

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v12, v13, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v13, 0xffffff

    and-int/2addr v12, v13

    shl-int/lit8 v12, v12, 0x8

    or-int/lit8 v12, v12, 0x11

    iput v12, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_c

    .line 95
    :cond_13
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v10, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v10, v11, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v10

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v10, v11

    shl-int/lit8 v10, v10, 0x8

    or-int/lit8 v10, v10, 0x11

    iput v10, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_d

    .line 125
    :cond_14
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v14, v14, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v14, v15, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v15, 0xffffff

    and-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x8

    or-int/lit8 v14, v14, 0x11

    iput v14, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_e

    .line 159
    :cond_15
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v14, v14, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v14, v15, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v15, 0xffffff

    and-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x8

    or-int/lit8 v14, v14, 0x11

    iput v14, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_f

    .line 166
    :cond_16
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v14, v14, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v14, v15, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v15, 0xffffff

    and-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x8

    or-int/lit8 v14, v14, 0x11

    iput v14, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_10

    .line 180
    :cond_17
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v14, v14, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v14, v15, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v15, 0xffffff

    and-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x8

    or-int/lit8 v14, v14, 0x11

    iput v14, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_11

    .line 184
    :cond_18
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v14, v14, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v14, v15, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v15, 0xffffff

    and-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x8

    or-int/lit8 v14, v14, 0x11

    iput v14, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_12
.end method
