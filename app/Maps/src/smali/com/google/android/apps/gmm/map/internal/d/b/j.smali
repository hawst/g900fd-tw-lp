.class Lcom/google/android/apps/gmm/map/internal/d/b/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/b/k;


# static fields
.field private static final a:Lcom/google/android/apps/gmm/map/internal/d/b/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 504
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/m;

    const/16 v1, 0x1d

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/m;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/j;->a:Lcom/google/android/apps/gmm/map/internal/d/b/d;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 571
    const/4 v0, 0x3

    return v0
.end method

.method public final a([B)Lcom/google/android/apps/gmm/map/internal/d/b/n;
    .locals 10

    .prologue
    const-wide v8, 0xffffffffL

    const/16 v6, 0x20

    const/16 v2, 0x19

    .line 554
    const/4 v0, 0x0

    .line 555
    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    .line 556
    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a([BI)I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 557
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/o;-><init>()V

    .line 558
    iput-object p0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 559
    const/4 v1, 0x5

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v2, v1

    const/16 v1, 0x9

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v4, v1

    and-long/2addr v4, v8

    shl-long/2addr v2, v6

    or-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->c:J

    .line 560
    const/16 v1, 0xd

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v2, v1

    const/16 v1, 0x11

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v4, v1

    and-long/2addr v4, v8

    shl-long/2addr v2, v6

    or-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->b:J

    .line 562
    const/16 v1, 0x15

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->e:I

    .line 564
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a()Lcom/google/android/apps/gmm/map/internal/d/b/n;

    move-result-object v0

    .line 566
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/d/b/n;[B)V
    .locals 8

    .prologue
    const/16 v7, 0x20

    const/16 v6, 0x19

    .line 540
    const/4 v0, 0x0

    const/16 v1, 0x1d

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 542
    const/4 v0, 0x4

    const/4 v1, 0x3

    aput-byte v1, p2, v0

    .line 543
    const/4 v0, 0x5

    .line 544
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    .line 543
    shr-long v4, v2, v7

    long-to-int v1, v4

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    const/16 v0, 0x9

    long-to-int v1, v2

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 545
    const/16 v0, 0xd

    .line 546
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 545
    shr-long v4, v2, v7

    long-to-int v1, v4

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    const/16 v0, 0x11

    long-to-int v1, v2

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 547
    const/16 v0, 0x15

    .line 548
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    .line 547
    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 550
    invoke-static {p2, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a([BI)I

    move-result v0

    .line 549
    invoke-static {p2, v6, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 551
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 576
    const/16 v0, 0x1d

    return v0
.end method

.method public final c()Lcom/google/android/apps/gmm/map/internal/d/b/d;
    .locals 1

    .prologue
    .line 581
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/j;->a:Lcom/google/android/apps/gmm/map/internal/d/b/d;

    return-object v0
.end method
