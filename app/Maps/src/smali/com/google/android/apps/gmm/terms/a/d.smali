.class public Lcom/google/android/apps/gmm/terms/a/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/terms/b/c;


# instance fields
.field final a:Lcom/google/android/apps/gmm/terms/b/d;

.field public b:Z

.field private final c:Lcom/google/android/apps/gmm/base/activities/c;

.field private final d:Lcom/google/android/apps/gmm/shared/b/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/terms/b/d;Lcom/google/android/apps/gmm/shared/b/a;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/gmm/terms/a/d;->c:Lcom/google/android/apps/gmm/base/activities/c;

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/gmm/terms/a/d;->a:Lcom/google/android/apps/gmm/terms/b/d;

    .line 46
    iput-object p3, p0, Lcom/google/android/apps/gmm/terms/a/d;->d:Lcom/google/android/apps/gmm/shared/b/a;

    .line 47
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->c:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->nu:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->c:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->hx:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->c:Lcom/google/android/apps/gmm/base/activities/c;

    sget v4, Lcom/google/android/apps/gmm/l;->kF:I

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->d:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v5, Lcom/google/android/apps/gmm/shared/b/c;->Z:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v5, "KR"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->c:Lcom/google/android/apps/gmm/base/activities/c;

    sget v5, Lcom/google/android/apps/gmm/l;->hy:I

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v7

    aput-object v3, v6, v8

    aput-object v4, v6, v9

    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 62
    :goto_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/gmm/util/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 63
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/gmm/util/q;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 64
    new-instance v7, Landroid/text/SpannableString;

    invoke-direct {v7, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0, v7, v2, v5, v8}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Landroid/text/SpannableString;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0, v7, v4, v6, v8}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Landroid/text/SpannableString;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->d:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->Z:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    const-string v0, "KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->c:Lcom/google/android/apps/gmm/base/activities/c;

    const-string v1, "https://www.google.com/intl/ko/policies/terms/location/"

    invoke-static {v0, v7, v3, v1, v8}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Landroid/text/SpannableString;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 74
    :cond_1
    return-object v7

    :cond_2
    move-object v0, v1

    .line 56
    goto :goto_0

    .line 60
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->c:Lcom/google/android/apps/gmm/base/activities/c;

    sget v5, Lcom/google/android/apps/gmm/l;->hJ:I

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v2, v6, v7

    aput-object v4, v6, v8

    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final b()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 79
    sget v0, Lcom/google/android/apps/gmm/f;->ek:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->c:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->hG:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->c:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->hH:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    .line 86
    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 87
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/util/q;->a(Lcom/google/android/apps/gmm/shared/net/a/b;)Ljava/lang/String;

    move-result-object v0

    .line 89
    invoke-static {v2, v1, v0, v4}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/text/Spannable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 90
    return-object v2
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->c:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->in:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/google/android/apps/gmm/terms/a/d;->c:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->ia:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    .line 97
    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-static {v1}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    .line 100
    new-instance v3, Lcom/google/android/apps/gmm/terms/a/e;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/terms/a/e;-><init>(Lcom/google/android/apps/gmm/terms/a/d;)V

    .line 112
    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 114
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 116
    return-object v2
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/gmm/terms/a/d;->d:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->ae:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->a:Lcom/google/android/apps/gmm/terms/b/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/terms/b/d;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->b:Z

    .line 138
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 140
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->a:Lcom/google/android/apps/gmm/terms/b/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/terms/b/d;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/terms/a/d;->b:Z

    .line 148
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
