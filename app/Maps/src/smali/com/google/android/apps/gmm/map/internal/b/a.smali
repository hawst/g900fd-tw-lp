.class public Lcom/google/android/apps/gmm/map/internal/b/a;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljava/util/Set;Ljava/util/Set;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 100
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 101
    if-eqz p2, :cond_1

    .line 102
    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 108
    :cond_1
    return p2
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/b/e;Ljava/util/Set;Lcom/google/android/apps/gmm/map/b/a/y;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/b/e;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 44
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 45
    invoke-interface {p0, v0, p2}, Lcom/google/android/apps/gmm/map/internal/b/e;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    .line 47
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 50
    :cond_1
    return-object v1
.end method

.method public static a(Ljava/util/Set;)Ljava/util/Set;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    const/4 v13, 0x1

    .line 65
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    .line 66
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 67
    iget v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    shl-int v6, v13, v1

    .line 68
    add-int/lit8 v7, v6, -0x1

    move v3, v2

    .line 69
    :goto_0
    if-gt v3, v13, :cond_0

    move v1, v2

    .line 70
    :goto_1
    if-gt v1, v13, :cond_2

    .line 71
    iget v8, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    add-int/2addr v8, v1

    .line 74
    if-ltz v8, :cond_1

    if-ge v8, v6, :cond_1

    .line 84
    iget v9, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    .line 85
    iget v10, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    add-int/2addr v10, v3

    add-int/2addr v10, v6

    and-int/2addr v10, v7

    .line 83
    new-instance v11, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v12, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v11, v9, v10, v8, v12}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    invoke-interface {v4, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 70
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 69
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 89
    :cond_3
    return-object v4
.end method

.method public static b(Ljava/util/Set;)Ljava/util/Set;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 124
    new-instance v9, Ljava/util/LinkedHashSet;

    invoke-direct {v9}, Ljava/util/LinkedHashSet;-><init>()V

    .line 125
    invoke-interface {p0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v9

    .line 170
    :goto_0
    return-object v0

    .line 128
    :cond_0
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 129
    iget v10, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    .line 130
    shl-int v5, v3, v10

    .line 131
    add-int/lit8 v11, v5, -0x1

    .line 132
    shr-int/lit8 v12, v5, 0x1

    .line 136
    iget v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    .line 137
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v1

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 138
    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v2, v1

    .line 139
    goto :goto_1

    .line 140
    :cond_1
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 141
    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    sub-int/2addr v1, v2

    if-lt v1, v12, :cond_2

    move v2, v3

    .line 152
    :goto_2
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move v3, v5

    move v6, v4

    move v7, v5

    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 153
    iget v8, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    .line 154
    if-eqz v2, :cond_3

    if-ge v8, v12, :cond_3

    .line 155
    add-int/2addr v8, v5

    .line 157
    :cond_3
    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 158
    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 159
    iget v8, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    invoke-static {v3, v8}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 160
    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    goto :goto_3

    .line 165
    :cond_4
    and-int v1, v7, v11

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v2, v10, v1, v3, v5}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    invoke-interface {v9, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 166
    and-int v1, v7, v11

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v2, v10, v1, v4, v5}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    invoke-interface {v9, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 167
    and-int v1, v6, v11

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v2, v10, v1, v3, v5}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    invoke-interface {v9, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 168
    and-int v1, v6, v11

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v2, v10, v1, v4, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    invoke-interface {v9, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object v0, v9

    .line 170
    goto/16 :goto_0

    :cond_5
    move v2, v4

    goto :goto_2
.end method
