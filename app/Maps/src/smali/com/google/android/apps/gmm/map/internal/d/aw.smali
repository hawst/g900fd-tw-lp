.class public abstract Lcom/google/android/apps/gmm/map/internal/d/aw;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final b:Lcom/google/b/c/dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dc",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "Lcom/google/android/apps/gmm/map/internal/d/aw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/b/a/ai;

.field private final c:Lcom/google/android/apps/gmm/map/internal/d/b/ap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 39
    new-instance v0, Lcom/google/b/c/dd;

    invoke-direct {v0}, Lcom/google/b/c/dd;-><init>()V

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 41
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 42
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->d:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->d:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 43
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->e:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/ax;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->e:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/ax;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 44
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/ax;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/ax;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 45
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 46
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 47
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->i:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->i:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 48
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->j:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->j:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 49
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/az;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/az;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 50
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->l:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->l:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 51
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->m:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/ax;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->m:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/ax;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 52
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->n:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/ax;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->n:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/ax;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 53
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 54
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->p:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->p:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 55
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->q:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->q:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 56
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->r:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->r:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 57
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->s:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->s:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 58
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->t:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->t:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 59
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->u:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->u:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 60
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 61
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->w:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->w:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 62
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->x:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/bb;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->x:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/bb;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 63
    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/google/b/c/dd;->a()Lcom/google/b/c/dc;

    move-result-object v0

    .line 67
    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/aw;->b:Lcom/google/b/c/dc;

    invoke-virtual {v0}, Lcom/google/b/c/dc;->size()I

    move-result v0

    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/ai;->a()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "TileStoreConfig init "

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 68
    :cond_1
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/aw;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 82
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/b/a/ai;->E:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/aw;->b()Lcom/google/android/apps/gmm/map/internal/d/b/ap;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/aw;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ap;

    .line 83
    return-void

    .line 82
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/aw;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/aw;->b:Lcom/google/b/c/dc;

    invoke-virtual {v0, p0}, Lcom/google/b/c/dc;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/aw;

    return-object v0
.end method


# virtual methods
.method abstract a()I
.end method

.method abstract a(Lcom/google/android/apps/gmm/map/internal/d/ac;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZLcom/google/android/apps/gmm/map/internal/d/r;Lcom/google/android/apps/gmm/map/internal/d/ag;)Lcom/google/android/apps/gmm/map/internal/d/as;
.end method

.method a(Lcom/google/android/apps/gmm/map/internal/d/ac;)Lcom/google/android/apps/gmm/map/internal/d/b/as;
    .locals 6

    .prologue
    const/16 v5, 0x100

    const/16 v4, 0x40

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/aw;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->x:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v0, v1, :cond_0

    .line 161
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/aw;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 162
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->C()I

    move-result v3

    shr-int/lit8 v3, v3, 0x3

    mul-int/lit8 v3, v3, 0x20

    invoke-static {v5, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/ah;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/map/b/a/ai;I)V

    .line 165
    :goto_0
    return-object v0

    .line 164
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/aq;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/aw;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 165
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->C()I

    move-result v3

    shr-int/lit8 v3, v3, 0x3

    mul-int/lit8 v3, v3, 0x20

    invoke-static {v5, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/aq;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/map/b/a/ai;I)V

    goto :goto_0
.end method

.method final a(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/map/internal/d/b/s;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/aw;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/b/a/ai;->E:Z

    if-nez v1, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-object v0

    .line 102
    :cond_1
    invoke-static {}, Lcom/google/android/apps/gmm/shared/c/h;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;

    if-eqz p3, :cond_2

    const/4 v3, -0x1

    .line 106
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/aw;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ap;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/aw;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;ILcom/google/android/apps/gmm/map/internal/d/b/ap;Lcom/google/android/apps/gmm/map/b/a/ai;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/aw;->a()I

    move-result v3

    goto :goto_1
.end method

.method abstract b()Lcom/google/android/apps/gmm/map/internal/d/b/ap;
.end method

.method public abstract c()B
.end method
