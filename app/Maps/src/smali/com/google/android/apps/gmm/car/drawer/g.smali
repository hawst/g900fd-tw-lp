.class public final Lcom/google/android/apps/gmm/car/drawer/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final d:I

.field public static final e:I

.field public static final f:I

.field public static final g:I

.field public static final h:I


# instance fields
.field final i:Lcom/google/android/apps/gmm/car/ad;

.field j:Z

.field private final k:Lcom/google/android/apps/gmm/car/drawer/o;

.field private final l:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/drawer/g;->a:I

    .line 29
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/drawer/g;->b:I

    .line 30
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/drawer/g;->c:I

    .line 31
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/drawer/g;->d:I

    .line 32
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/drawer/g;->e:I

    .line 33
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/drawer/g;->f:I

    .line 34
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/drawer/g;->g:I

    .line 35
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/drawer/g;->h:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/car/ad;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/drawer/i;-><init>(Lcom/google/android/apps/gmm/car/drawer/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/g;->l:Ljava/lang/Object;

    .line 42
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/car/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    .line 44
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 46
    sget v2, Lcom/google/android/apps/gmm/f;->J:I

    sget v3, Lcom/google/android/apps/gmm/l;->bC:I

    sget v4, Lcom/google/android/apps/gmm/car/drawer/g;->a:I

    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/h;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/drawer/h;-><init>(Lcom/google/android/apps/gmm/car/drawer/g;IIII)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    sget v2, Lcom/google/android/apps/gmm/f;->L:I

    sget v3, Lcom/google/android/apps/gmm/l;->bE:I

    sget v4, Lcom/google/android/apps/gmm/car/drawer/g;->b:I

    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/h;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/drawer/h;-><init>(Lcom/google/android/apps/gmm/car/drawer/g;IIII)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    sget v2, Lcom/google/android/apps/gmm/f;->M:I

    sget v3, Lcom/google/android/apps/gmm/l;->bF:I

    sget v4, Lcom/google/android/apps/gmm/car/drawer/g;->c:I

    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/h;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/drawer/h;-><init>(Lcom/google/android/apps/gmm/car/drawer/g;IIII)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    sget v2, Lcom/google/android/apps/gmm/f;->H:I

    sget v3, Lcom/google/android/apps/gmm/l;->bz:I

    sget v4, Lcom/google/android/apps/gmm/car/drawer/g;->d:I

    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/h;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/drawer/h;-><init>(Lcom/google/android/apps/gmm/car/drawer/g;IIII)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    sget v2, Lcom/google/android/apps/gmm/f;->I:I

    sget v3, Lcom/google/android/apps/gmm/l;->bB:I

    sget v4, Lcom/google/android/apps/gmm/car/drawer/g;->e:I

    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/h;

    move-object v1, p0

    move v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/drawer/h;-><init>(Lcom/google/android/apps/gmm/car/drawer/g;IIII)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    sget v2, Lcom/google/android/apps/gmm/f;->N:I

    sget v3, Lcom/google/android/apps/gmm/l;->bD:I

    sget v4, Lcom/google/android/apps/gmm/car/drawer/g;->f:I

    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/h;

    move-object v1, p0

    move v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/drawer/h;-><init>(Lcom/google/android/apps/gmm/car/drawer/g;IIII)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    sget v2, Lcom/google/android/apps/gmm/f;->G:I

    sget v3, Lcom/google/android/apps/gmm/l;->by:I

    sget v4, Lcom/google/android/apps/gmm/car/drawer/g;->g:I

    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/h;

    move-object v1, p0

    move v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/drawer/h;-><init>(Lcom/google/android/apps/gmm/car/drawer/g;IIII)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    sget v2, Lcom/google/android/apps/gmm/f;->K:I

    sget v3, Lcom/google/android/apps/gmm/l;->bA:I

    sget v4, Lcom/google/android/apps/gmm/car/drawer/g;->h:I

    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/h;

    move-object v1, p0

    move v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/drawer/h;-><init>(Lcom/google/android/apps/gmm/car/drawer/g;IIII)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/o;

    iget-object v1, p1, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/car/drawer/o;-><init>(Lcom/google/android/libraries/curvular/bd;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/g;->k:Lcom/google/android/apps/gmm/car/drawer/o;

    .line 64
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/g;->l:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    .line 74
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->bH:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 73
    iput-object v1, v0, Lcom/google/android/apps/gmm/car/ao;->k:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->b()V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/g;->k:Lcom/google/android/apps/gmm/car/drawer/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/o;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 68
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/g;->k:Lcom/google/android/apps/gmm/car/drawer/o;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/o;->b:Landroid/view/View;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/o;->c:Lcom/google/android/gms/car/support/PagedListView;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/ao;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/ao;->k:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->b()V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/g;->l:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 83
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/google/android/apps/gmm/car/m/e;->b:Lcom/google/android/apps/gmm/car/m/e;

    return-object v0
.end method
