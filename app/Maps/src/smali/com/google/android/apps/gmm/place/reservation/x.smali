.class Lcom/google/android/apps/gmm/place/reservation/x;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/reservation/b/d;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/apps/gmm/place/reservation/s;

.field private final d:Landroid/content/Context;

.field private final e:Ljava/lang/String;

.field private final f:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/reservation/s;Landroid/content/Context;II)V
    .locals 1

    .prologue
    .line 364
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/reservation/x;->c:Lcom/google/android/apps/gmm/place/reservation/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/x;->a:Ljava/lang/String;

    .line 365
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/reservation/x;->d:Landroid/content/Context;

    .line 366
    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/x;->e:Ljava/lang/String;

    .line 367
    iput p4, p0, Lcom/google/android/apps/gmm/place/reservation/x;->f:I

    .line 368
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 396
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/x;->a:Ljava/lang/String;

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/x;->c:Lcom/google/android/apps/gmm/place/reservation/s;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 398
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/x;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/x;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 386
    iget v0, p0, Lcom/google/android/apps/gmm/place/reservation/x;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/x;->c:Lcom/google/android/apps/gmm/place/reservation/s;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/place/reservation/s;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/x;->c:Lcom/google/android/apps/gmm/place/reservation/s;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/place/reservation/s;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/x;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/x;->b:Ljava/lang/String;

    .line 409
    :goto_0
    return-object v0

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/x;->c:Lcom/google/android/apps/gmm/place/reservation/s;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/place/reservation/s;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/x;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/x;->d:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->gI:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 409
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
