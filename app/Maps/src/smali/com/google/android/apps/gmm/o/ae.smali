.class Lcom/google/android/apps/gmm/o/ae;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/o/w;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/o/x;

.field private final b:Lcom/google/android/apps/gmm/o/a/a;

.field private final c:I

.field private final d:Ljava/lang/CharSequence;

.field private final e:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/o/x;Lcom/google/android/apps/gmm/o/a/a;ILjava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;I)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/android/apps/gmm/o/ae;->a:Lcom/google/android/apps/gmm/o/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput-object p2, p0, Lcom/google/android/apps/gmm/o/ae;->b:Lcom/google/android/apps/gmm/o/a/a;

    .line 118
    iput p3, p0, Lcom/google/android/apps/gmm/o/ae;->c:I

    .line 119
    iput-object p4, p0, Lcom/google/android/apps/gmm/o/ae;->d:Ljava/lang/CharSequence;

    .line 120
    iput p6, p0, Lcom/google/android/apps/gmm/o/ae;->e:I

    .line 122
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/ae;->a:Lcom/google/android/apps/gmm/o/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/o/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/ae;->a:Lcom/google/android/apps/gmm/o/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/o/x;->d:Lcom/google/android/apps/gmm/o/g;

    iget v1, p0, Lcom/google/android/apps/gmm/o/ae;->e:I

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/o/g;->a(IZ)V

    .line 153
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/ae;->a:Lcom/google/android/apps/gmm/o/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/o/x;->e:Lcom/google/android/apps/gmm/o/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/ae;->b:Lcom/google/android/apps/gmm/o/a/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/o/a/c;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/ae;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/aw;
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/ae;->a:Lcom/google/android/apps/gmm/o/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/o/x;->e:Lcom/google/android/apps/gmm/o/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/ae;->b:Lcom/google/android/apps/gmm/o/a/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/o/a/c;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/o/ae;->c:I

    sget v1, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    .line 137
    :goto_0
    return-object v0

    .line 136
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/o/ae;->c:I

    .line 137
    sget v1, Lcom/google/android/apps/gmm/d;->ar:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/google/android/apps/gmm/o/ae;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
