.class public Lcom/google/android/apps/gmm/base/views/c/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/j;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/google/android/apps/gmm/navigation/navui/views/d;

.field public final c:Lcom/google/android/apps/gmm/navigation/navui/views/f;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/d;->a:Ljava/util/List;

    .line 24
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/d;->b:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    .line 25
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/d;->c:Lcom/google/android/apps/gmm/navigation/navui/views/f;

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/google/android/apps/gmm/navigation/navui/views/d;Lcom/google/android/apps/gmm/navigation/navui/views/f;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/j;",
            ">;",
            "Lcom/google/android/apps/gmm/navigation/navui/views/d;",
            "Lcom/google/android/apps/gmm/navigation/navui/views/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/c/d;->a:Ljava/util/List;

    .line 33
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/views/c/d;->b:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    .line 34
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/views/c/d;->c:Lcom/google/android/apps/gmm/navigation/navui/views/f;

    .line 35
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 39
    if-ne p1, p0, :cond_0

    move v0, v1

    .line 46
    :goto_0
    return v0

    .line 41
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/views/c/d;

    if-eqz v0, :cond_8

    .line 42
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/c/d;->a:Ljava/util/List;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/base/views/c/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/c/d;->a:Ljava/util/List;

    if-eq v3, v0, :cond_1

    if-eqz v3, :cond_4

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    move v0, v1

    :goto_1
    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/c/d;->b:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/base/views/c/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/c/d;->b:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    .line 43
    if-eq v3, v0, :cond_2

    if-eqz v3, :cond_5

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    move v0, v1

    :goto_2
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/d;->c:Lcom/google/android/apps/gmm/navigation/navui/views/f;

    check-cast p1, Lcom/google/android/apps/gmm/base/views/c/d;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/views/c/d;->c:Lcom/google/android/apps/gmm/navigation/navui/views/f;

    .line 44
    if-eq v0, v3, :cond_3

    if-eqz v0, :cond_6

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_3
    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 42
    goto :goto_1

    :cond_5
    move v0, v2

    .line 43
    goto :goto_2

    :cond_6
    move v0, v2

    .line 44
    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_0

    :cond_8
    move v0, v2

    .line 46
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 51
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/d;->a:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/d;->b:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/d;->c:Lcom/google/android/apps/gmm/navigation/navui/views/f;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
