.class final Lcom/google/android/apps/gmm/base/f/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/h;


# instance fields
.field final synthetic a:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    .prologue
    .line 193
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/f/e;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Z)V
    .locals 6

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/f/e;->a:Z

    if-eqz v0, :cond_0

    const/high16 v0, 0x3f000000    # 0.5f

    .line 197
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/f/e;->a:Z

    if-eqz v1, :cond_1

    const/16 v1, 0xa7

    .line 201
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/f/e;->a:Z

    if-eqz v2, :cond_2

    const/16 v2, 0x7d0

    .line 202
    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    .line 203
    invoke-virtual {v3, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/base/h/a;->a:Landroid/view/animation/Interpolator;

    .line 204
    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    int-to-long v4, v1

    .line 205
    invoke-virtual {v3, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, v2

    .line 206
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/base/f/f;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/apps/gmm/base/f/f;-><init>(Lcom/google/android/apps/gmm/base/f/e;Landroid/view/View;F)V

    .line 207
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 216
    return-void

    .line 196
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 197
    :cond_1
    const/16 v1, 0xfa

    goto :goto_1

    .line 201
    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method
