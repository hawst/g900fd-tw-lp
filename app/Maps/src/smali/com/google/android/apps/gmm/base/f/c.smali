.class final Lcom/google/android/apps/gmm/base/f/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/h;


# instance fields
.field final synthetic a:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    .prologue
    .line 155
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/f/c;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Z)V
    .locals 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 158
    if-eqz p2, :cond_1

    .line 160
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/f/c;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 188
    :goto_1
    return-void

    .line 160
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 163
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/f/c;->a:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 164
    :goto_2
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/f/c;->a:Z

    if-eqz v2, :cond_3

    .line 165
    :goto_3
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/f/c;->a:Z

    if-eqz v2, :cond_4

    const/16 v2, 0xfa

    .line 166
    :goto_4
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    .line 167
    invoke-virtual {v3, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    .line 168
    invoke-virtual {v3, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    .line 169
    invoke-virtual {v3, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/base/h/a;->a:Landroid/view/animation/Interpolator;

    .line 170
    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    int-to-long v4, v2

    .line 171
    invoke-virtual {v3, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/base/f/d;

    invoke-direct {v3, p0, p1, v0, v1}, Lcom/google/android/apps/gmm/base/f/d;-><init>(Lcom/google/android/apps/gmm/base/f/c;Landroid/view/View;FF)V

    .line 172
    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_1

    .line 163
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 164
    :cond_3
    const/high16 v1, 0x3f000000    # 0.5f

    goto :goto_3

    .line 165
    :cond_4
    const/16 v2, 0xa7

    goto :goto_4
.end method
