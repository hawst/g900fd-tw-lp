.class Lcom/google/android/apps/gmm/mylocation/c/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/mylocation/c/e;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/mylocation/c/j;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/mylocation/c/g;Lcom/google/android/apps/gmm/mylocation/c/j;)V
    .locals 0

    .prologue
    .line 78
    iput-object p2, p0, Lcom/google/android/apps/gmm/mylocation/c/h;->a:Lcom/google/android/apps/gmm/mylocation/c/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;Z)V
    .locals 8

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/h;->a:Lcom/google/android/apps/gmm/mylocation/c/j;

    iget-object v1, v0, Lcom/google/android/apps/gmm/mylocation/c/j;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v2, Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->c(Landroid/location/Location;)I

    move-result v4

    int-to-float v4, v4

    invoke-static {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->b(Landroid/location/Location;)I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/mylocation/g/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;FI)V

    iput-object v2, v0, Lcom/google/android/apps/gmm/mylocation/c/j;->c:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/c/j;->c:Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v3

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    if-nez p2, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/c/j;->c:Lcom/google/android/apps/gmm/mylocation/g/a;

    const/4 v3, -0x1

    iput v3, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/mylocation/c/j;->d:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
