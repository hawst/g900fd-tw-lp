.class public Lcom/google/android/apps/gmm/navigation/i/c;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/apps/gmm/map/u/a/e;)F
    .locals 4

    .prologue
    .line 21
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 22
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, -0x2

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 23
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/u/a/e;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    .line 22
    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v3

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v0, v1

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/z;->a(II)F

    move-result v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;)I
    .locals 2

    .prologue
    .line 51
    invoke-static {p0}, Lcom/google/android/apps/gmm/navigation/i/c;->a(Lcom/google/android/apps/gmm/map/u/a/e;)F

    move-result v0

    .line 52
    invoke-static {p1}, Lcom/google/android/apps/gmm/navigation/i/c;->b(Lcom/google/android/apps/gmm/map/u/a/e;)F

    move-result v1

    .line 53
    sub-float v0, v1, v0

    float-to-int v0, v0

    .line 54
    if-gez v0, :cond_0

    .line 55
    add-int/lit16 v0, v0, 0x168

    .line 57
    :cond_0
    const/16 v1, 0xb4

    if-le v0, v1, :cond_1

    add-int/lit16 v0, v0, -0x168

    :cond_1
    return v0
.end method

.method public static b(Lcom/google/android/apps/gmm/map/u/a/e;)F
    .locals 4

    .prologue
    .line 34
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 35
    :goto_0
    return v0

    .line 34
    :cond_0
    const/4 v0, 0x0

    .line 35
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v0, 0x1

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    iget v0, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v0, v3

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int v1, v2, v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/z;->a(II)F

    move-result v0

    goto :goto_0
.end method
