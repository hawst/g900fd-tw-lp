.class public Lcom/google/android/apps/gmm/map/util/g;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12
    const/4 v0, 0x1

    new-array v0, v0, [B

    aput-byte v1, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/util/g;->a:[B

    return-void
.end method

.method public static a([BIILcom/google/android/apps/gmm/map/util/f;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 28
    new-instance v3, Ljava/util/zip/Inflater;

    invoke-direct {v3, v1}, Ljava/util/zip/Inflater;-><init>(Z)V

    .line 30
    :try_start_0
    invoke-virtual {v3, p0, p1, p2}, Ljava/util/zip/Inflater;->setInput([BII)V

    .line 34
    shl-int/lit8 v2, p2, 0x2

    .line 35
    invoke-virtual {p3, v2}, Lcom/google/android/apps/gmm/map/util/f;->b(I)V

    .line 37
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v5

    invoke-virtual {v3, v2, v4, v5}, Ljava/util/zip/Inflater;->inflate([BII)I

    move-result v2

    .line 39
    :goto_0
    invoke-virtual {v3}, Ljava/util/zip/Inflater;->finished()Z

    move-result v4

    if-nez v4, :cond_1

    .line 40
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v4

    shl-int/lit8 v4, v4, 0x1

    invoke-virtual {p3, v4}, Lcom/google/android/apps/gmm/map/util/f;->b(I)V

    .line 42
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v4

    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v5

    sub-int/2addr v5, v2

    .line 41
    invoke-virtual {v3, v4, v2, v5}, Ljava/util/zip/Inflater;->inflate([BII)I

    move-result v4

    .line 43
    if-nez v4, :cond_0

    .line 51
    invoke-virtual {v3}, Ljava/util/zip/Inflater;->needsInput()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 52
    if-nez v0, :cond_1

    .line 53
    sget-object v0, Lcom/google/android/apps/gmm/map/util/g;->a:[B

    invoke-virtual {v3, v0}, Ljava/util/zip/Inflater;->setInput([B)V

    move v0, v1

    .line 67
    :cond_0
    add-int/2addr v2, v4

    .line 68
    goto :goto_0

    .line 71
    :cond_1
    invoke-virtual {p3, v2}, Lcom/google/android/apps/gmm/map/util/f;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    invoke-virtual {v3}, Ljava/util/zip/Inflater;->end()V

    .line 74
    return-void

    .line 73
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/util/zip/Inflater;->end()V

    throw v0
.end method
