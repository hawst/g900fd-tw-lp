.class public Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"


# instance fields
.field c:Lcom/google/android/apps/gmm/photo/s;

.field d:Z

.field e:Landroid/net/Uri;

.field f:Lcom/google/android/apps/gmm/base/g/c;

.field g:Lcom/google/android/apps/gmm/photo/g;

.field h:Landroid/view/View;

.field final i:Ljava/lang/Object;

.field private final j:Lcom/google/android/apps/gmm/photo/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 147
    new-instance v0, Lcom/google/android/apps/gmm/photo/s;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/photo/s;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;-><init>(Lcom/google/android/apps/gmm/photo/s;)V

    .line 148
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/photo/s;)V
    .locals 1

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    .line 93
    new-instance v0, Lcom/google/android/apps/gmm/photo/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/photo/i;-><init>(Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->i:Ljava/lang/Object;

    .line 107
    new-instance v0, Lcom/google/android/apps/gmm/photo/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/photo/j;-><init>(Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->j:Lcom/google/android/apps/gmm/photo/h;

    .line 151
    iput-object p1, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->c:Lcom/google/android/apps/gmm/photo/s;

    .line 152
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Ljava/util/ArrayList;Lcom/google/android/apps/gmm/base/g/c;ZZLcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;
    .locals 3
    .param p5    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/google/android/apps/gmm/base/g/c;",
            "ZZ",
            "Lcom/google/android/apps/gmm/map/b/a/q;",
            ")",
            "Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;"
        }
    .end annotation

    .prologue
    .line 187
    new-instance v0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;-><init>()V

    .line 188
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 189
    const-string v2, "uriList"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 190
    const-string v2, "placemark"

    invoke-virtual {p0, v1, v2, p2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 191
    const-string v2, "popBackDouble"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 192
    const-string v2, "isPanorama"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 193
    if-eqz p5, :cond_0

    .line 194
    const-string v2, "latLon"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 196
    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->setArguments(Landroid/os/Bundle;)V

    .line 197
    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Landroid/net/Uri;Lcom/google/android/apps/gmm/map/b/a/q;)V
    .locals 10
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 156
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 157
    const-string v3, "uriList"

    new-array v4, v1, [Landroid/net/Uri;

    aput-object p1, v4, v0

    if-nez v4, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    array-length v5, v4

    if-ltz v5, :cond_1

    move v0, v1

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    const-wide/16 v6, 0x5

    int-to-long v8, v5

    add-long/2addr v6, v8

    div-int/lit8 v0, v5, 0xa

    int-to-long v8, v0

    add-long/2addr v6, v8

    const-wide/32 v8, 0x7fffffff

    cmp-long v0, v6, v8

    if-lez v0, :cond_4

    const v0, 0x7fffffff

    :goto_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v5, v4}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 158
    const-string v0, "isPanorama"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 160
    if-eqz p2, :cond_3

    .line 161
    const-string v0, "latLon"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 164
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->p()Lcom/google/android/apps/gmm/j/a/a;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/j/a/a;->c(Landroid/os/Bundle;)V

    .line 178
    :goto_1
    return-void

    .line 157
    :cond_4
    const-wide/32 v8, -0x80000000

    cmp-long v0, v6, v8

    if-gez v0, :cond_5

    const/high16 v0, -0x80000000

    goto :goto_0

    :cond_5
    long-to-int v0, v6

    goto :goto_0

    .line 167
    :cond_6
    new-instance v0, Lcom/google/android/apps/gmm/photo/k;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/gmm/photo/k;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Landroid/os/Bundle;)V

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/login/LoginDialog;->a(Landroid/app/Activity;Lcom/google/android/apps/gmm/login/a/b;)V

    goto :goto_1
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 285
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 290
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 306
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x24

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected request code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 292
    :pswitch_0
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293
    new-instance v0, Lcom/google/android/apps/gmm/photo/d;

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/photo/d;-><init>(Landroid/net/Uri;Landroid/content/Context;)V

    .line 294
    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->g:Lcom/google/android/apps/gmm/photo/g;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/photo/g;->a(Lcom/google/android/apps/gmm/photo/d;)V

    goto :goto_0

    .line 298
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->e:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 299
    new-instance v0, Lcom/google/android/apps/gmm/photo/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->e:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/photo/d;-><init>(Landroid/net/Uri;Landroid/content/Context;)V

    .line 300
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/android/apps/gmm/photo/d;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 301
    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->g:Lcom/google/android/apps/gmm/photo/g;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/photo/g;->a(Lcom/google/android/apps/gmm/photo/d;)V

    .line 302
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->e:Landroid/net/Uri;

    goto :goto_0

    .line 290
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 400
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->g:Lcom/google/android/apps/gmm/photo/g;

    .line 402
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 212
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 213
    if-eqz p1, :cond_0

    .line 215
    :goto_0
    if-eqz p1, :cond_7

    .line 216
    const-string v0, "pendingFilename"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->e:Landroid/net/Uri;

    .line 217
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v2, "placemark"

    invoke-virtual {v0, p1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->f:Lcom/google/android/apps/gmm/base/g/c;

    .line 218
    const-string v0, "popBackDouble"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->d:Z

    .line 220
    const-string v0, "isPanorama"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 221
    const-string v0, "uriList"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 223
    if-eqz v3, :cond_a

    .line 225
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v4, :cond_2

    move v0, v4

    :goto_2
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 213
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0

    .line 217
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v5

    .line 225
    goto :goto_2

    .line 227
    :cond_3
    const-string v0, "latLon"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 228
    const-string v0, "latLon"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/q;

    move-object v2, v0

    .line 230
    :goto_3
    new-array v7, v4, [Lcom/google/android/apps/gmm/photo/d;

    new-instance v8, Lcom/google/android/apps/gmm/photo/d;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v8, v0, v2, v6, v4}, Lcom/google/android/apps/gmm/photo/d;-><init>(Landroid/net/Uri;Lcom/google/android/apps/gmm/map/b/a/q;Landroid/content/Context;Z)V

    aput-object v8, v7, v5

    if-nez v7, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    array-length v2, v7

    if-ltz v2, :cond_5

    move v0, v4

    :goto_4
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_5
    move v0, v5

    goto :goto_4

    :cond_6
    const-wide/16 v4, 0x5

    int-to-long v8, v2

    add-long/2addr v4, v8

    div-int/lit8 v0, v2, 0xa

    int-to-long v8, v0

    add-long/2addr v4, v8

    const-wide/32 v8, 0x7fffffff

    cmp-long v0, v4, v8

    if-lez v0, :cond_8

    const v0, 0x7fffffff

    :goto_5
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v4, v7}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 235
    :goto_6
    new-instance v5, Lcom/google/android/apps/gmm/photo/l;

    invoke-direct {v5, p0}, Lcom/google/android/apps/gmm/photo/l;-><init>(Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;)V

    .line 243
    new-instance v0, Lcom/google/android/apps/gmm/photo/b;

    .line 244
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_b

    :goto_7
    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->j:Lcom/google/android/apps/gmm/photo/h;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/photo/b;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/photo/h;ZLjava/util/List;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->g:Lcom/google/android/apps/gmm/photo/g;

    .line 246
    :cond_7
    return-void

    .line 230
    :cond_8
    const-wide/32 v8, -0x80000000

    cmp-long v0, v4, v8

    if-gez v0, :cond_9

    const/high16 v0, -0x80000000

    goto :goto_5

    :cond_9
    long-to-int v0, v4

    goto :goto_5

    .line 232
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/google/android/apps/gmm/photo/d;->a(Ljava/util/List;Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    goto :goto_6

    .line 244
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto :goto_7

    :cond_c
    move-object v2, v1

    goto :goto_3
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 273
    .line 274
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/photo/ad;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v1

    .line 275
    iget-object v2, v1, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->g:Lcom/google/android/apps/gmm/photo/g;

    check-cast v0, Lcom/google/android/apps/gmm/photo/a;

    invoke-interface {v2, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 276
    iget-object v0, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->h:Landroid/view/View;

    .line 279
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->h:Landroid/view/View;

    return-object v0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 256
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onPause()V

    .line 257
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->i:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 258
    return-void

    .line 257
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 250
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onResume()V

    .line 251
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->i:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 252
    return-void

    .line 251
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 262
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 263
    const-string v0, "pendingFilename"

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->e:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 264
    const-string v0, "uriList"

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->g:Lcom/google/android/apps/gmm/photo/g;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/photo/g;->j()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 265
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "placemark"

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->f:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 266
    const-string v0, "popBackDouble"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 267
    const-string v0, "isPanorama"

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->g:Lcom/google/android/apps/gmm/photo/g;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/photo/g;->b()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 268
    return-void

    .line 265
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method
