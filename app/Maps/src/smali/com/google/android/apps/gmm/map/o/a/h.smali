.class public Lcom/google/android/apps/gmm/map/o/a/h;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ">;"
        }
    .end annotation
.end field

.field final b:[I

.field final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ">;>;"
        }
    .end annotation
.end field

.field d:Lcom/google/b/c/hz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/hz",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final g:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final h:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final i:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final j:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final k:Lcom/google/android/apps/gmm/map/b/a/ab;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/b/c/hz",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/ab;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 33
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 82
    invoke-static {p1}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->a:Ljava/util/List;

    .line 83
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/o/a/h;->k:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 86
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Collections;->nCopies(ILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 85
    invoke-static {v0}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->c:Ljava/util/List;

    .line 88
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->b:[I

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->b:[I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 90
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/ab;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 402
    invoke-virtual {p2, p1}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    div-float v3, p3, v0

    .line 406
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    move v2, v3

    .line 408
    :goto_0
    const/16 v4, 0x64

    if-ge v1, v4, :cond_1

    const/high16 v4, 0x3f800000    # 1.0f

    cmpg-float v4, v2, v4

    if-gez v4, :cond_1

    .line 410
    invoke-static {p1, p2, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 411
    mul-float v4, p3, p3

    invoke-direct {p0, v0, v4, p4}, Lcom/google/android/apps/gmm/map/o/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/ab;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 416
    :goto_1
    return-object v0

    .line 409
    :cond_0
    add-int/lit8 v1, v1, 0x1

    add-float/2addr v2, v3

    goto :goto_0

    .line 416
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/ab;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->k:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/o/a/h;->b(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/ab;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 364
    :goto_0
    return v0

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    move v2, v1

    .line 354
    :goto_1
    if-ge v2, v3, :cond_2

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 357
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/h;->k:Lcom/google/android/apps/gmm/map/b/a/ab;

    if-eq v0, v4, :cond_1

    .line 358
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/o/a/h;->b(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/ab;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 359
    goto :goto_0

    .line 354
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 364
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/ab;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 372
    iget-object v1, p3, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v1, v1

    div-int/lit8 v2, v1, 0x3

    move v1, v0

    .line 373
    :goto_0
    if-ge v1, v2, :cond_0

    .line 374
    add-int/lit8 v3, v1, -0x1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/h;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p3, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 375
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a/h;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p3, v1, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 376
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a/h;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/h;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/a/h;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v3, v4, p1, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    .line 377
    cmpg-float v3, v3, p2

    if-gez v3, :cond_1

    .line 378
    const/4 v0, 0x0

    .line 381
    :cond_0
    return v0

    .line 373
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->d:Lcom/google/b/c/hz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->d:Lcom/google/b/c/hz;

    invoke-interface {v0}, Lcom/google/b/c/hz;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 210
    :cond_0
    invoke-static {}, Lcom/google/b/c/cb;->e()Lcom/google/b/c/cb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->d:Lcom/google/b/c/hz;

    .line 211
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->e:Ljava/util/Set;

    .line 212
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->l:Ljava/util/List;

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->e:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/h;->k:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/ab;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    .line 217
    :goto_0
    if-ge v3, v4, :cond_3

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 226
    invoke-static {}, Lcom/google/b/c/cb;->e()Lcom/google/b/c/cb;

    move-result-object v5

    .line 227
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/h;->e:Ljava/util/Set;

    invoke-interface {v5, v1}, Lcom/google/b/c/hz;->addAll(Ljava/util/Collection;)Z

    .line 229
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/h;->d:Lcom/google/b/c/hz;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->d()Ljava/util/List;

    move-result-object v6

    invoke-interface {v1, v6}, Lcom/google/b/c/hz;->addAll(Ljava/util/Collection;)Z

    .line 231
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/h;->k:Lcom/google/android/apps/gmm/map/b/a/ab;

    if-eq v0, v1, :cond_2

    .line 232
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v5, v0}, Lcom/google/b/c/hz;->addAll(Ljava/util/Collection;)Z

    move v1, v2

    .line 234
    :goto_1
    if-ge v1, v3, :cond_2

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 236
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/o/a/h;->k:Lcom/google/android/apps/gmm/map/b/a/ab;

    if-eq v0, v6, :cond_1

    .line 237
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v5, v0}, Lcom/google/b/c/hz;->addAll(Ljava/util/Collection;)Z

    .line 234
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 242
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/h;->l:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 245
    :cond_3
    return-void
.end method

.method a(I)V
    .locals 16

    .prologue
    .line 137
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/o/a/h;->a()V

    .line 139
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 140
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/a/h;->c:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v1, v0, v13}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 142
    const/4 v4, -0x1

    .line 143
    const/4 v6, 0x0

    .line 144
    const/4 v5, -0x1

    .line 146
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/a/h;->a:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 147
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v2, v2

    div-int/lit8 v14, v2, 0x3

    .line 148
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/a/h;->l:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/b/c/hz;

    .line 153
    const/4 v7, 0x0

    :goto_0
    if-ge v7, v14, :cond_11

    .line 154
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/a/h;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1, v7, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 156
    add-int/lit8 v3, v14, -0x1

    if-eq v7, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/a/h;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-interface {v2, v3}, Lcom/google/b/c/hz;->a(Ljava/lang/Object;)I

    move-result v3

    const/4 v8, 0x1

    if-gt v3, v8, :cond_0

    .line 158
    const/4 v3, -0x1

    if-ne v4, v3, :cond_14

    move v3, v5

    move v4, v6

    move v5, v7

    .line 153
    :goto_1
    add-int/lit8 v7, v7, 0x1

    move v6, v4

    move v4, v5

    move v5, v3

    goto :goto_0

    .line 162
    :cond_0
    const/4 v3, -0x1

    if-eq v4, v3, :cond_14

    .line 163
    sub-int v3, v7, v4

    const/4 v8, 0x5

    if-ge v3, v8, :cond_1

    .line 166
    const/4 v4, -0x1

    move v3, v5

    move v5, v4

    move v4, v6

    .line 167
    goto :goto_1

    .line 171
    :cond_1
    sub-int v3, v7, v4

    .line 173
    if-le v3, v6, :cond_2

    .line 174
    div-int/lit8 v5, v3, 0x2

    add-int/2addr v5, v4

    move v6, v3

    .line 178
    :cond_2
    if-lez v4, :cond_3

    .line 182
    add-int/lit8 v4, v4, -0x1

    .line 183
    add-int/lit8 v3, v3, 0x1

    .line 188
    :cond_3
    add-int/2addr v3, v4

    add-int/lit8 v12, v3, -0x1

    const/4 v3, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    move v8, v4

    :goto_2
    if-ge v8, v12, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/a/h;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1, v8, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/a/h;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    const v15, 0x48742400    # 250000.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v15, v1}, Lcom/google/android/apps/gmm/map/o/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/ab;)Z

    move-result v4

    if-nez v4, :cond_5

    add-int/lit8 v4, v8, 0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/o/a/h;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1, v4, v15}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/a/h;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/o/a/h;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v4, v15}, Lcom/google/android/apps/gmm/map/b/a/y;->d(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v4

    const v15, 0x48742400    # 250000.0f

    cmpl-float v4, v4, v15

    if-ltz v4, :cond_4

    const/4 v3, 0x1

    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_5
    if-eqz v3, :cond_13

    if-ge v8, v12, :cond_13

    add-int/lit8 v4, v8, -0x1

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    invoke-virtual {v1, v8}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v10

    const/high16 v15, 0x43fa0000    # 500.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v10, v15, v1}, Lcom/google/android/apps/gmm/map/o/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/ab;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    move-object v10, v4

    move v4, v11

    move v11, v12

    :goto_3
    if-le v11, v8, :cond_7

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/map/o/a/h;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1, v11, v12}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/map/o/a/h;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    const v15, 0x48742400    # 250000.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v15, v1}, Lcom/google/android/apps/gmm/map/o/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/ab;)Z

    move-result v12

    if-nez v12, :cond_7

    add-int/lit8 v12, v11, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/o/a/h;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1, v12, v15}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/map/o/a/h;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/o/a/h;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v12, v15}, Lcom/google/android/apps/gmm/map/b/a/y;->d(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v12

    const v15, 0x48742400    # 250000.0f

    cmpl-float v12, v12, v15

    if-lez v12, :cond_6

    const/4 v4, 0x1

    :cond_6
    add-int/lit8 v11, v11, -0x1

    goto :goto_3

    :cond_7
    if-eqz v4, :cond_8

    if-ge v8, v11, :cond_8

    add-int/lit8 v9, v11, 0x1

    invoke-virtual {v1, v9}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v9

    invoke-virtual {v1, v11}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v12

    const/high16 v15, 0x43fa0000    # 500.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v12, v15, v1}, Lcom/google/android/apps/gmm/map/o/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/ab;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v9

    :cond_8
    if-nez v3, :cond_9

    if-eqz v4, :cond_10

    :cond_9
    sub-int v4, v11, v8

    if-ltz v4, :cond_a

    const/4 v3, 0x1

    :goto_4
    if-nez v3, :cond_b

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_a
    const/4 v3, 0x0

    goto :goto_4

    :cond_b
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v4}, Ljava/util/ArrayList;-><init>(I)V

    if-eqz v10, :cond_c

    invoke-interface {v12, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_c
    move v3, v8

    :goto_5
    if-gt v3, v11, :cond_d

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_d
    if-eqz v9, :cond_e

    invoke-interface {v12, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_e
    invoke-static {v12}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(Ljava/util/List;)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v3

    .line 189
    :goto_6
    iget-object v4, v3, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v4, v4

    div-int/lit8 v4, v4, 0x3

    if-lez v4, :cond_f

    .line 190
    invoke-interface {v13, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    :cond_f
    const/4 v4, -0x1

    move v3, v5

    move v5, v4

    move v4, v6

    goto/16 :goto_1

    .line 188
    :cond_10
    add-int/lit8 v3, v11, 0x1

    invoke-static {v1, v8, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(Lcom/google/android/apps/gmm/map/b/a/ab;II)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v3

    goto :goto_6

    .line 196
    :cond_11
    const/4 v1, -0x1

    if-ne v5, v1, :cond_12

    .line 199
    div-int/lit8 v5, v14, 0x2

    .line 202
    :cond_12
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/a/h;->b:[I

    aput v5, v1, p1

    .line 203
    return-void

    :cond_13
    move v4, v11

    move v11, v12

    goto/16 :goto_3

    :cond_14
    move v3, v5

    move v5, v4

    move v4, v6

    goto/16 :goto_1
.end method
