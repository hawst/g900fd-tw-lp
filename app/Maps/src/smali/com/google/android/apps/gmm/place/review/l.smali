.class public Lcom/google/android/apps/gmm/place/review/l;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field private final a:Lcom/google/e/a/a/a/b;

.field private final b:Lcom/google/android/apps/gmm/place/review/m;

.field private c:Lcom/google/e/a/a/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/apps/gmm/place/review/m;)V
    .locals 4
    .param p5    # Lcom/google/android/apps/gmm/place/review/m;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 45
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/aj;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    invoke-direct {p0, v0, p5}, Lcom/google/android/apps/gmm/place/review/l;-><init>(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/place/review/m;)V

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/l;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/j;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 49
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/l;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 54
    :cond_0
    int-to-float v0, p3

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/l;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x3

    int-to-long v2, p3

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/l;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p4}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 60
    return-void
.end method

.method private constructor <init>(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/place/review/m;)V
    .locals 2
    .param p2    # Lcom/google/android/apps/gmm/place/review/m;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 38
    sget-object v0, Lcom/google/r/b/a/el;->aT:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/aj;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/review/l;->a:Lcom/google/e/a/a/a/b;

    .line 40
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/review/l;->b:Lcom/google/android/apps/gmm/place/review/m;

    .line 41
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 1

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/review/l;->c:Lcom/google/e/a/a/a/b;

    .line 70
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/l;->a:Lcom/google/e/a/a/a/b;

    return-object v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/l;->b:Lcom/google/android/apps/gmm/place/review/m;

    if-eqz v0, :cond_0

    .line 82
    if-eqz p1, :cond_1

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/l;->b:Lcom/google/android/apps/gmm/place/review/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/review/m;->a()V

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/l;->c:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/l;->b:Lcom/google/android/apps/gmm/place/review/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/review/l;->a:Lcom/google/e/a/a/a/b;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/review/l;->c:Lcom/google/e/a/a/a/b;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/place/review/m;->a(Lcom/google/e/a/a/a/b;Lcom/google/e/a/a/a/b;)V

    goto :goto_0
.end method
