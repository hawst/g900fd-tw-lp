.class public abstract Lcom/google/android/apps/gmm/cardui/CardUiListFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/g/c;


# instance fields
.field public c:Lcom/google/android/apps/gmm/cardui/s;

.field public d:Lcom/google/android/apps/gmm/cardui/h/a;

.field public e:Lcom/google/r/b/a/bi;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<",
            "Lcom/google/android/apps/gmm/cardui/g/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 119
    const-class v0, Lcom/google/android/apps/gmm/cardui/d/b;

    return-object v0
.end method

.method public a(Lcom/google/r/b/a/bi;)V
    .locals 0

    .prologue
    .line 231
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 212
    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->e:Lcom/google/r/b/a/bi;

    .line 213
    if-eqz v1, :cond_0

    .line 214
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->e:Lcom/google/r/b/a/bi;

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/cardui/h;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/cardui/h;-><init>(Lcom/google/android/apps/gmm/cardui/CardUiListFragment;Lcom/google/r/b/a/bi;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 224
    :cond_0
    return-void
.end method

.method public f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->i()Lcom/google/android/apps/gmm/cardui/b/b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/b/b;->e:Lcom/google/b/f/t;

    return-object v0
.end method

.method public abstract i()Lcom/google/android/apps/gmm/cardui/b/b;
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 131
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onCreate(Landroid/os/Bundle;)V

    .line 132
    new-instance v0, Lcom/google/android/apps/gmm/cardui/h/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/cardui/h/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->d:Lcom/google/android/apps/gmm/cardui/h/a;

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->getArguments()Landroid/os/Bundle;

    .line 134
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 139
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->a()Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 141
    new-instance v1, Lcom/google/android/apps/gmm/cardui/g;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/cardui/g;-><init>(Lcom/google/android/apps/gmm/cardui/CardUiListFragment;Lcom/google/android/libraries/curvular/ae;)V

    .line 149
    new-instance v2, Lcom/google/android/apps/gmm/cardui/s;

    .line 150
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->i()Lcom/google/android/apps/gmm/cardui/b/b;

    move-result-object v4

    invoke-direct {v2, v3, v4, v5, v5}, Lcom/google/android/apps/gmm/cardui/s;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/android/apps/gmm/cardui/b/a;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    .line 152
    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    iput-object v1, v2, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    .line 154
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    return-object v0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/cardui/s;->d()V

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->d:Lcom/google/android/apps/gmm/cardui/h/a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/cardui/h/a;->c:Lcom/google/android/apps/gmm/cardui/g/c;

    .line 176
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onPause()V

    .line 177
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 167
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/cardui/s;->c()V

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->d:Lcom/google/android/apps/gmm/cardui/h/a;

    iput-object p0, v0, Lcom/google/android/apps/gmm/cardui/h/a;->c:Lcom/google/android/apps/gmm/cardui/g/c;

    .line 170
    return-void
.end method
