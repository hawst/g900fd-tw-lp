.class public Lcom/google/android/apps/gmm/z/k;
.super Lcom/google/android/apps/gmm/z/b/e;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/z/b/n;

.field private final h:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final i:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final j:Lcom/google/b/f/cq;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final k:[Lcom/google/b/f/cq;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final l:Lcom/google/b/f/b/a/dj;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/shared/c/f;Ljava/lang/String;Ljava/lang/String;Lcom/google/b/f/cq;[Lcom/google/b/f/cq;Lcom/google/b/f/b/a/dj;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/z/b/e;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 123
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/k;->a:Lcom/google/android/apps/gmm/z/b/n;

    .line 124
    iput-object p3, p0, Lcom/google/android/apps/gmm/z/k;->h:Ljava/lang/String;

    .line 125
    iput-object p4, p0, Lcom/google/android/apps/gmm/z/k;->i:Ljava/lang/String;

    .line 126
    iput-object p5, p0, Lcom/google/android/apps/gmm/z/k;->j:Lcom/google/b/f/cq;

    .line 127
    iput-object p6, p0, Lcom/google/android/apps/gmm/z/k;->k:[Lcom/google/b/f/cq;

    .line 128
    iput-object p7, p0, Lcom/google/android/apps/gmm/z/k;->l:Lcom/google/b/f/b/a/dj;

    .line 129
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/b/f/b/a/bc;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 189
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/z/b/e;->a(Lcom/google/b/f/b/a/bc;)V

    .line 190
    invoke-static {}, Lcom/google/b/f/b/a/dg;->newBuilder()Lcom/google/b/f/b/a/di;

    move-result-object v2

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/k;->a:Lcom/google/android/apps/gmm/z/b/n;

    if-eqz v0, :cond_5

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/k;->a:Lcom/google/android/apps/gmm/z/b/n;

    iget-object v1, v0, Lcom/google/android/apps/gmm/z/b/n;->a:Lcom/google/r/b/a/a;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/z/b/n;->a:Lcom/google/r/b/a/a;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v3, v2, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/b/f/b/a/di;->a:I

    iget v1, v1, Lcom/google/r/b/a/a;->t:I

    iput v1, v2, Lcom/google/b/f/b/a/di;->b:I

    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/z/b/n;->b:Lcom/google/b/f/cj;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/gmm/z/b/n;->b:Lcom/google/b/f/cj;

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v3, v2, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v2, Lcom/google/b/f/b/a/di;->a:I

    iget v1, v1, Lcom/google/b/f/cj;->I:I

    iput v1, v2, Lcom/google/b/f/b/a/di;->c:I

    :cond_3
    iget-object v1, v0, Lcom/google/android/apps/gmm/z/b/n;->c:Lcom/google/b/f/ch;

    if-eqz v1, :cond_5

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/n;->c:Lcom/google/b/f/ch;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget v1, v2, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v2, Lcom/google/b/f/b/a/di;->a:I

    iget v0, v0, Lcom/google/b/f/ch;->f:I

    iput v0, v2, Lcom/google/b/f/b/a/di;->d:I

    .line 194
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/k;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/k;->h:Ljava/lang/String;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    iget v1, v2, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v2, Lcom/google/b/f/b/a/di;->a:I

    iput-object v0, v2, Lcom/google/b/f/b/a/di;->e:Ljava/lang/Object;

    .line 197
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/k;->i:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 198
    invoke-static {}, Lcom/google/b/f/b/a/do;->newBuilder()Lcom/google/b/f/b/a/dq;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/k;->i:Ljava/lang/String;

    if-nez v1, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget v3, v0, Lcom/google/b/f/b/a/dq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v0, Lcom/google/b/f/b/a/dq;->a:I

    iput-object v1, v0, Lcom/google/b/f/b/a/dq;->b:Ljava/lang/Object;

    iget-object v1, v2, Lcom/google/b/f/b/a/di;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/dq;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v8, v1, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, v2, Lcom/google/b/f/b/a/di;->a:I

    .line 200
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/k;->j:Lcom/google/b/f/cq;

    if-eqz v0, :cond_b

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/k;->j:Lcom/google/b/f/cq;

    .line 202
    invoke-interface {v0}, Lcom/google/b/f/cq;->a()I

    move-result v0

    invoke-static {}, Lcom/google/b/f/cm;->newBuilder()Lcom/google/b/f/co;

    move-result-object v1

    iget v3, v1, Lcom/google/b/f/co;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/google/b/f/co;->a:I

    iput v0, v1, Lcom/google/b/f/co;->b:I

    invoke-virtual {v1}, Lcom/google/b/f/co;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    .line 201
    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    iget-object v1, v2, Lcom/google/b/f/b/a/di;->g:Lcom/google/n/ao;

    iget-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v8, v1, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, v2, Lcom/google/b/f/b/a/di;->a:I

    .line 204
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/k;->k:[Lcom/google/b/f/cq;

    if-eqz v0, :cond_d

    .line 205
    iget-object v3, p0, Lcom/google/android/apps/gmm/z/k;->k:[Lcom/google/b/f/cq;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_d

    aget-object v0, v3, v1

    .line 206
    invoke-interface {v0}, Lcom/google/b/f/cq;->a()I

    move-result v0

    invoke-static {}, Lcom/google/b/f/cm;->newBuilder()Lcom/google/b/f/co;

    move-result-object v5

    iget v6, v5, Lcom/google/b/f/co;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v5, Lcom/google/b/f/co;->a:I

    iput v0, v5, Lcom/google/b/f/co;->b:I

    invoke-virtual {v5}, Lcom/google/b/f/co;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    invoke-virtual {v2}, Lcom/google/b/f/b/a/di;->c()V

    iget-object v5, v2, Lcom/google/b/f/b/a/di;->h:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    invoke-direct {v6}, Lcom/google/n/ao;-><init>()V

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v8, v6, Lcom/google/n/ao;->d:Z

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 209
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/k;->l:Lcom/google/b/f/b/a/dj;

    if-eqz v0, :cond_f

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/k;->l:Lcom/google/b/f/b/a/dj;

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    iget-object v1, v2, Lcom/google/b/f/b/a/di;->i:Lcom/google/n/ao;

    iget-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v8, v1, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/b/f/b/a/di;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, v2, Lcom/google/b/f/b/a/di;->a:I

    .line 212
    :cond_f
    iget-object v0, p1, Lcom/google/b/f/b/a/bc;->c:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/b/f/b/a/di;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v8, v0, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p1, Lcom/google/b/f/b/a/bc;->a:I

    .line 213
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 157
    instance-of v2, p1, Lcom/google/android/apps/gmm/z/k;

    if-eqz v2, :cond_b

    .line 158
    check-cast p1, Lcom/google/android/apps/gmm/z/k;

    .line 159
    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->a:Lcom/google/android/apps/gmm/z/b/n;

    iget-object v3, p1, Lcom/google/android/apps/gmm/z/k;->a:Lcom/google/android/apps/gmm/z/b/n;

    if-eq v2, v3, :cond_0

    if-eqz v2, :cond_5

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/z/k;->h:Ljava/lang/String;

    .line 160
    if-eq v2, v3, :cond_1

    if-eqz v2, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/z/k;->i:Ljava/lang/String;

    .line 161
    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_2
    move v2, v0

    :goto_2
    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->j:Lcom/google/b/f/cq;

    iget-object v3, p1, Lcom/google/android/apps/gmm/z/k;->j:Lcom/google/b/f/cq;

    .line 162
    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_8

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_3
    move v2, v0

    :goto_3
    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->k:[Lcom/google/b/f/cq;

    iget-object v3, p1, Lcom/google/android/apps/gmm/z/k;->k:[Lcom/google/b/f/cq;

    .line 163
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->l:Lcom/google/b/f/b/a/dj;

    .line 164
    invoke-virtual {v2}, Lcom/google/b/f/b/a/dj;->k()Lcom/google/n/f;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/apps/gmm/z/k;->l:Lcom/google/b/f/b/a/dj;

    invoke-virtual {v3}, Lcom/google/b/f/b/a/dj;->k()Lcom/google/n/f;

    move-result-object v3

    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_9

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_4
    move v2, v0

    :goto_4
    if-eqz v2, :cond_a

    .line 166
    :goto_5
    return v0

    :cond_5
    move v2, v1

    .line 159
    goto :goto_0

    :cond_6
    move v2, v1

    .line 160
    goto :goto_1

    :cond_7
    move v2, v1

    .line 161
    goto :goto_2

    :cond_8
    move v2, v1

    .line 162
    goto :goto_3

    :cond_9
    move v2, v1

    .line 164
    goto :goto_4

    :cond_a
    move v0, v1

    goto :goto_5

    :cond_b
    move v0, v1

    .line 166
    goto :goto_5
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 171
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->a:Lcom/google/android/apps/gmm/z/b/n;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->j:Lcom/google/b/f/cq;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->k:[Lcom/google/b/f/cq;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->l:Lcom/google/b/f/b/a/dj;

    .line 172
    invoke-virtual {v2}, Lcom/google/b/f/b/a/dj;->k()Lcom/google/n/f;

    move-result-object v2

    aput-object v2, v0, v1

    .line 171
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 177
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    .line 178
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/b/a/ah;->a:Z

    const-string v1, "action"

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->a:Lcom/google/android/apps/gmm/z/b/n;

    .line 179
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "contentEi"

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->h:Ljava/lang/String;

    .line 180
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "contentVed"

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->i:Ljava/lang/String;

    .line 181
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "clientLeafVe"

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->j:Lcom/google/b/f/cq;

    .line 182
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "clientVeTree"

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/k;->k:[Lcom/google/b/f/cq;

    .line 183
    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
