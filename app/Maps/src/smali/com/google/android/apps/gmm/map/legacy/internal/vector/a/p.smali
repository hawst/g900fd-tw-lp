.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/p;
.super Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/q;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 382
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/q;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;I)V

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/p;->a:[I

    aput v2, v0, v2

    .line 385
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/p;->a:[I

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 386
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;IFLcom/google/android/apps/gmm/map/internal/c/bi;Lcom/google/android/apps/gmm/map/internal/c/be;)V
    .locals 9

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/p;->a:[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v0, v1

    .line 397
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    const/4 v0, 0x4

    if-ge v3, v0, :cond_8

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/p;->a:[I

    const/4 v1, 0x0

    aget v2, v0, v1

    shl-int/lit8 v2, v2, 0x8

    aput v2, v0, v1

    .line 399
    if-ge v3, p2, :cond_6

    .line 400
    iget-object v0, p4, Lcom/google/android/apps/gmm/map/internal/c/bi;->f:[F

    if-nez v0, :cond_5

    iget-object v0, p4, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    int-to-float v0, v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v4

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_1
    iget-object v2, p4, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p4, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v2, v2, v0

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v5, :cond_0

    const/4 v2, 0x0

    :goto_2
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v2, v2

    goto :goto_2

    :cond_1
    new-array v0, v1, [F

    iput-object v0, p4, Lcom/google/android/apps/gmm/map/internal/c/bi;->f:[F

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_2

    iget-object v2, p4, Lcom/google/android/apps/gmm/map/internal/c/bi;->f:[F

    const/4 v5, 0x0

    aput v5, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_2
    const/4 v0, 0x0

    :goto_4
    iget-object v1, p4, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    iget-object v1, p4, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    aget-byte v1, v1, v0

    int-to-float v1, v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v1

    div-float v5, v1, v4

    iget-object v1, p4, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v6, v1, v0

    const/4 v1, 0x0

    :goto_5
    iget-object v2, v6, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v2, :cond_3

    const/4 v2, 0x0

    :goto_6
    if-ge v1, v2, :cond_4

    iget-object v2, p4, Lcom/google/android/apps/gmm/map/internal/c/bi;->f:[F

    iget-object v7, p4, Lcom/google/android/apps/gmm/map/internal/c/bi;->f:[F

    aget v7, v7, v1

    iget-object v8, v6, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v8, v8, v1

    iget v8, v8, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    mul-float/2addr v8, v5

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v7

    aput v7, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_3
    iget-object v2, v6, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v2, v2

    goto :goto_6

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p4, Lcom/google/android/apps/gmm/map/internal/c/bi;->f:[F

    array-length v0, v0

    if-ge v3, v0, :cond_7

    iget-object v0, p4, Lcom/google/android/apps/gmm/map/internal/c/bi;->f:[F

    aget v0, v0, v3

    .line 401
    :goto_7
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_6

    .line 402
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/p;->a:[I

    const/4 v2, 0x0

    aget v4, v1, v2

    .line 403
    iget-object v5, p5, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v5, v5, v3

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    mul-float/2addr v5, p3

    div-float v0, v5, v0

    const/high16 v5, 0x437f0000    # 255.0f

    mul-float/2addr v0, v5

    .line 402
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    or-int/2addr v0, v4

    aput v0, v1, v2

    .line 397
    :cond_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 400
    :cond_7
    const/4 v0, 0x0

    goto :goto_7

    .line 407
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/p;->a:[I

    const/4 v0, 0x0

    :goto_8
    array-length v2, v1

    if-ge v0, v2, :cond_9

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->a:Ljava/nio/ByteBuffer;

    aget v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 408
    :cond_9
    return-void
.end method
