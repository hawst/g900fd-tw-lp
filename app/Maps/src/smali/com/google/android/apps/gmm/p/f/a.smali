.class public Lcom/google/android/apps/gmm/p/f/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:F

.field private b:F

.field private c:J

.field private final d:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 2

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/p/f/a;->b:F

    .line 83
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/f/a;->c:J

    .line 94
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/p/f/a;->d:Z

    .line 95
    return-void
.end method


# virtual methods
.method public final a(JF)F
    .locals 11

    .prologue
    const/high16 v9, 0x3f000000    # 0.5f

    const/high16 v8, 0x41200000    # 10.0f

    const/high16 v7, 0x43b40000    # 360.0f

    const/4 v6, 0x0

    .line 106
    iget-wide v0, p0, Lcom/google/android/apps/gmm/p/f/a;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/gmm/p/f/a;->c:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_2

    .line 107
    :cond_0
    iput p3, p0, Lcom/google/android/apps/gmm/p/f/a;->a:F

    .line 131
    :cond_1
    :goto_0
    iput-wide p1, p0, Lcom/google/android/apps/gmm/p/f/a;->c:J

    .line 134
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/a;->d:Z

    if-eqz v0, :cond_a

    .line 135
    :goto_1
    iget v0, p0, Lcom/google/android/apps/gmm/p/f/a;->a:F

    cmpl-float v0, v0, v7

    if-ltz v0, :cond_9

    .line 136
    iget v0, p0, Lcom/google/android/apps/gmm/p/f/a;->a:F

    sub-float/2addr v0, v7

    iput v0, p0, Lcom/google/android/apps/gmm/p/f/a;->a:F

    goto :goto_1

    .line 110
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/a;->d:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/gmm/p/f/a;->a:F

    sub-float v0, p3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    cmpl-float v0, v0, v7

    if-lez v0, :cond_3

    .line 111
    iget v0, p0, Lcom/google/android/apps/gmm/p/f/a;->a:F

    cmpg-float v0, p3, v0

    if-gez v0, :cond_5

    .line 112
    add-float/2addr p3, v7

    .line 118
    :cond_3
    :goto_2
    iget-wide v0, p0, Lcom/google/android/apps/gmm/p/f/a;->c:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    .line 119
    cmpl-float v1, v0, v8

    if-gtz v1, :cond_4

    cmpg-float v1, v0, v6

    if-gez v1, :cond_6

    .line 121
    :cond_4
    iput p3, p0, Lcom/google/android/apps/gmm/p/f/a;->a:F

    .line 122
    iput v6, p0, Lcom/google/android/apps/gmm/p/f/a;->b:F

    goto :goto_0

    .line 114
    :cond_5
    sub-float/2addr p3, v7

    goto :goto_2

    .line 124
    :cond_6
    :goto_3
    cmpl-float v1, v0, v6

    if-lez v1, :cond_1

    .line 125
    invoke-static {v9, v0}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 126
    iget v2, p0, Lcom/google/android/apps/gmm/p/f/a;->a:F

    sub-float v2, p3, v2

    div-float/2addr v2, v8

    iget v3, p0, Lcom/google/android/apps/gmm/p/f/a;->b:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v4

    mul-float/2addr v4, v2

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/gmm/p/f/a;->b:F

    iget v3, p0, Lcom/google/android/apps/gmm/p/f/a;->b:F

    cmpl-float v3, v3, v6

    if-eqz v3, :cond_7

    mul-float/2addr v2, v8

    iget v3, p0, Lcom/google/android/apps/gmm/p/f/a;->b:F

    div-float/2addr v2, v3

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    neg-float v3, v2

    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v4

    double-to-float v2, v2

    mul-float v3, v2, v1

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_8

    iput v6, p0, Lcom/google/android/apps/gmm/p/f/a;->b:F

    .line 127
    :cond_7
    :goto_4
    sub-float/2addr v0, v9

    .line 128
    goto :goto_3

    .line 126
    :cond_8
    iget v3, p0, Lcom/google/android/apps/gmm/p/f/a;->b:F

    iget v4, p0, Lcom/google/android/apps/gmm/p/f/a;->b:F

    mul-float/2addr v2, v4

    mul-float/2addr v2, v1

    sub-float v2, v3, v2

    iput v2, p0, Lcom/google/android/apps/gmm/p/f/a;->b:F

    iget v2, p0, Lcom/google/android/apps/gmm/p/f/a;->a:F

    iget v3, p0, Lcom/google/android/apps/gmm/p/f/a;->b:F

    mul-float/2addr v1, v3

    add-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/p/f/a;->a:F

    goto :goto_4

    .line 138
    :cond_9
    :goto_5
    iget v0, p0, Lcom/google/android/apps/gmm/p/f/a;->a:F

    cmpg-float v0, v0, v6

    if-gez v0, :cond_a

    .line 139
    iget v0, p0, Lcom/google/android/apps/gmm/p/f/a;->a:F

    add-float/2addr v0, v7

    iput v0, p0, Lcom/google/android/apps/gmm/p/f/a;->a:F

    goto :goto_5

    .line 143
    :cond_a
    iget v0, p0, Lcom/google/android/apps/gmm/p/f/a;->a:F

    return v0
.end method
