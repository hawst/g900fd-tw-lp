.class public Lcom/google/android/apps/gmm/place/cd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/i/i;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/place/g/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/place/g/a/a;)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/cd;->a:Lcom/google/android/apps/gmm/place/g/a/a;

    .line 174
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cd;->a:Lcom/google/android/apps/gmm/place/g/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/a;->c()Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cd;->a:Lcom/google/android/apps/gmm/place/g/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/a;->a()Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cd;->a:Lcom/google/android/apps/gmm/place/g/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/a;->c()Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cd;->a:Lcom/google/android/apps/gmm/place/g/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/a;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/base/l/a/u;
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cd;->a:Lcom/google/android/apps/gmm/place/g/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/a;->d()Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 228
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 234
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()Lcom/google/android/libraries/curvular/aq;
    .locals 1

    .prologue
    .line 239
    sget v0, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 244
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
