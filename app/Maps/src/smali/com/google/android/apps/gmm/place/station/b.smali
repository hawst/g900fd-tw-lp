.class public Lcom/google/android/apps/gmm/place/station/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/station/b/b;


# static fields
.field private static final a:Lcom/google/android/apps/gmm/place/station/c;


# instance fields
.field private b:Lcom/google/android/apps/gmm/place/station/z;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/place/station/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/station/c;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/place/station/b;->a:Lcom/google/android/apps/gmm/place/station/c;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/place/station/z;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/station/z;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/b;->b:Lcom/google/android/apps/gmm/place/station/z;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/b;->b:Lcom/google/android/apps/gmm/place/station/z;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/station/z;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    .line 102
    if-nez p2, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/base/g/c;

    .line 107
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->D_()Lcom/google/android/apps/gmm/map/i/a/a;

    move-result-object v0

    .line 112
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/g/c;->g:Lcom/google/b/c/cv;

    .line 111
    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/i/a/a;->a(Ljava/util/Collection;)V

    .line 114
    new-instance v0, Lcom/google/android/apps/gmm/place/station/z;

    sget-object v2, Lcom/google/android/apps/gmm/place/station/b;->a:Lcom/google/android/apps/gmm/place/station/c;

    .line 117
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v1, v1, Lcom/google/r/b/a/ads;->s:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/wb;->j()Lcom/google/maps/g/wb;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/wb;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 119
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v6

    .line 118
    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/station/z;-><init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/cg;Lcom/google/maps/g/wb;J)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/b;->b:Lcom/google/android/apps/gmm/place/station/z;

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/b;->b:Lcom/google/android/apps/gmm/place/station/z;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/station/z;->e:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/b;->b:Lcom/google/android/apps/gmm/place/station/z;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/station/z;->f:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/b;->b:Lcom/google/android/apps/gmm/place/station/z;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/station/z;->g:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/b;->b:Lcom/google/android/apps/gmm/place/station/z;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/station/z;->h:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/b;->b:Lcom/google/android/apps/gmm/place/station/z;

    sget-object v0, Lcom/google/android/apps/gmm/place/station/z;->b:Lcom/google/android/libraries/curvular/cg;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/directions/h/a;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/b;->b:Lcom/google/android/apps/gmm/place/station/z;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/station/z;->d:Lcom/google/android/apps/gmm/directions/h/a;

    return-object v0
.end method

.method public final h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/b;->b:Lcom/google/android/apps/gmm/place/station/z;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/station/z;->i:Lcom/google/b/c/cv;

    return-object v0
.end method
