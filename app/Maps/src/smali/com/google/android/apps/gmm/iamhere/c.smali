.class Lcom/google/android/apps/gmm/iamhere/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/iamhere/j;


# static fields
.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final d:I


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/activities/c;

.field private e:I

.field private f:Lcom/google/android/apps/gmm/iamhere/c/q;

.field private g:Lcom/google/android/apps/gmm/x/o;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/a/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 30
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c;->b:Ljava/util/List;

    .line 31
    const-string v0, "Starbucks"

    const-string v1, "Cafe"

    const-string v2, "https://lh4.googleusercontent.com/-TZSd8RH-Nx4/UdbeZ-pW9NI/AAAAAAAAQeM/9ITRkbamIVM/s85/Starbucks%25C2%25AE%2BState%2BStreet"

    .line 32
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    const-string v1, "Uniqlo"

    const-string v2, "Shopping"

    const-string v3, "https://lh6.googleusercontent.com/-wwBou541O0c/Ug5MO2OgtRI/AAAAAAAAAF4/6N3f3YIOYhQ/s500-c-k-no/paramus1-big.jpg"

    .line 34
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/iamhere/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v1

    const-string v2, "Harry\'s Bar"

    const-string v3, "Bar"

    const-string v4, "https://lh5.googleusercontent.com/-WUxZMM8nN3c/UFwtwCkAVuI/AAAAAABWn6s/fyOI8CFdeZQ/s250-c-k-no/Harry%2527s%2BBar%2B%2526%2BBurger"

    .line 36
    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v2

    const-string v3, "Gary Danko"

    const-string v4, "Restaurant"

    const-string v5, "https://cbks3.google.com/cbk?output=thumbnail&cb_client=maps_sv&thumb=2&thumbfov=92&panoid=9huYBwJNI_j_odNAdRtgKA&thumbpegman=1&yaw=329.26516203418083&w=300&h=118"

    .line 38
    invoke-static {v3, v4, v5}, Lcom/google/android/apps/gmm/iamhere/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v3

    const-string v4, "Walmart"

    const-string v5, "Supermart"

    const-string v6, "https://lh3.googleusercontent.com/-LdhRbvDZPQ0/UkLaJMsYmkI/AAAAAACfOFw/A1w7_SmkO8Q/s250-c-k-no/photo.jpg"

    .line 41
    invoke-static {v4, v5, v6}, Lcom/google/android/apps/gmm/iamhere/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v4

    const-string v5, "Toho Cinema"

    const-string v6, "Entertainment"

    const-string v7, "https://lh5.googleusercontent.com/-hJ9U3fXGgv4/UgHwusR9d6I/AAAAAAAACQs/aO1qiUVf5Nc/s250-c-k-no/photo.jpg"

    .line 43
    invoke-static {v5, v6, v7}, Lcom/google/android/apps/gmm/iamhere/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v5

    .line 31
    invoke-static/range {v0 .. v5}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    .line 46
    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/iamhere/c;->d:I

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/x/o;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 105
    new-instance v1, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 106
    invoke-static {}, Lcom/google/r/b/a/ads;->newBuilder()Lcom/google/r/b/a/adu;

    move-result-object v0

    .line 107
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/r/b/a/adu;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/r/b/a/adu;->a:I

    iput-object p0, v0, Lcom/google/r/b/a/adu;->d:Ljava/lang/Object;

    .line 108
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {v0}, Lcom/google/r/b/a/adu;->d()V

    iget-object v2, v0, Lcom/google/r/b/a/adu;->f:Lcom/google/n/aq;

    invoke-interface {v2, p1}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    .line 109
    invoke-static {}, Lcom/google/r/b/a/acq;->newBuilder()Lcom/google/r/b/a/acs;

    move-result-object v2

    .line 110
    invoke-static {}, Lcom/google/r/b/a/aje;->newBuilder()Lcom/google/r/b/a/ajg;

    move-result-object v3

    .line 111
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v4, v3, Lcom/google/r/b/a/ajg;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, v3, Lcom/google/r/b/a/ajg;->a:I

    iput-object p2, v3, Lcom/google/r/b/a/ajg;->b:Ljava/lang/Object;

    .line 110
    iget-object v4, v2, Lcom/google/r/b/a/acs;->c:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/r/b/a/ajg;->g()Lcom/google/n/t;

    move-result-object v3

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v4, Lcom/google/n/ao;->d:Z

    iget v3, v2, Lcom/google/r/b/a/acs;->a:I

    const/high16 v4, 0x10000

    or-int/2addr v3, v4

    iput v3, v2, Lcom/google/r/b/a/acs;->a:I

    .line 109
    iget-object v3, v0, Lcom/google/r/b/a/adu;->e:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/r/b/a/acs;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v3, Lcom/google/n/ao;->d:Z

    iget v2, v0, Lcom/google/r/b/a/adu;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, v0, Lcom/google/r/b/a/adu;->a:I

    .line 112
    invoke-virtual {v0}, Lcom/google/r/b/a/adu;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    .line 106
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/g/i;->g:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->v:Z

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    .line 113
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    .line 105
    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized e()V
    .locals 4

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/a/a;

    .line 165
    if-eqz v0, :cond_0

    .line 166
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v3, Lcom/google/android/apps/gmm/iamhere/d;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/gmm/iamhere/d;-><init>(Lcom/google/android/apps/gmm/iamhere/c;Lcom/google/android/apps/gmm/iamhere/a/a;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 174
    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/iamhere/c/q;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/iamhere/c/o;
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/x/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/iamhere/c/q;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)",
            "Lcom/google/android/apps/gmm/iamhere/c/o;"
        }
    .end annotation

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/c;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 98
    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/c;->g:Lcom/google/android/apps/gmm/x/o;

    .line 99
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/c;->e()V

    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/c;->d()Lcom/google/android/apps/gmm/iamhere/c/o;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 81
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c;->g:Lcom/google/android/apps/gmm/x/o;

    .line 82
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->b:Lcom/google/android/apps/gmm/iamhere/c/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 83
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/c;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    monitor-exit p0

    return-void

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/iamhere/a/a;)V
    .locals 1

    .prologue
    .line 154
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/replay/UseCannedStpResponseEvent;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/replay/UseCannedStpResponseEvent;->getStateType()Lcom/google/android/apps/gmm/iamhere/c/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 59
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/replay/UseCannedStpResponseEvent;->getPlaceCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/iamhere/c;->e:I

    .line 60
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/p/d/f;Lcom/google/android/apps/gmm/iamhere/k;)Z
    .locals 1

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/c;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c;->g:Lcom/google/android/apps/gmm/x/o;

    .line 89
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->a:Lcom/google/android/apps/gmm/iamhere/c/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 90
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/c;->e()V

    .line 91
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/iamhere/a/a;)V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 161
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 128
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/iamhere/c/o;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 144
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/c;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 145
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v3, Lcom/google/android/apps/gmm/iamhere/c/q;->a:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-ne v2, v3, :cond_0

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c;->b:Ljava/util/List;

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/c;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v5, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-ne v3, v5, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/c;->g:Lcom/google/android/apps/gmm/x/o;

    :goto_1
    const-string v5, "http://www.google.com"

    .line 148
    invoke-static {v5}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v6

    const/4 v7, 0x1

    move-object v5, v4

    move-object v8, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/iamhere/c/o;-><init>(Lcom/google/android/apps/gmm/iamhere/c/q;Ljava/util/List;Lcom/google/android/apps/gmm/x/o;Lcom/google/o/b/a/v;Lcom/google/r/b/a/amd;Ljava/util/List;ZLcom/google/n/f;)V

    return-object v0

    .line 145
    :cond_0
    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c;->c:Ljava/util/List;

    const/4 v3, 0x0

    sget v5, Lcom/google/android/apps/gmm/iamhere/c;->d:I

    iget v6, p0, Lcom/google/android/apps/gmm/iamhere/c;->e:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-interface {v2, v3, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v2

    goto :goto_0

    :cond_1
    move-object v3, v4

    goto :goto_1
.end method
