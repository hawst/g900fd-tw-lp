.class public final enum Lcom/google/android/apps/gmm/map/r/a/s;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/r/a/s;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/r/a/s;

.field public static final enum b:Lcom/google/android/apps/gmm/map/r/a/s;

.field public static final enum c:Lcom/google/android/apps/gmm/map/r/a/s;

.field public static final enum d:Lcom/google/android/apps/gmm/map/r/a/s;

.field public static final enum e:Lcom/google/android/apps/gmm/map/r/a/s;

.field private static final synthetic i:[Lcom/google/android/apps/gmm/map/r/a/s;


# instance fields
.field public final f:Z

.field public final g:Z

.field public final h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/s;

    const-string v1, "SELECTED_WITH_TRAFFIC"

    move v4, v2

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/r/a/s;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/r/a/s;->a:Lcom/google/android/apps/gmm/map/r/a/s;

    .line 20
    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/s;

    const-string v5, "UNSELECTED_WITH_TRAFFIC"

    move v6, v3

    move v7, v2

    move v8, v2

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/gmm/map/r/a/s;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v4, Lcom/google/android/apps/gmm/map/r/a/s;->b:Lcom/google/android/apps/gmm/map/r/a/s;

    .line 21
    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/s;

    const-string v5, "SELECTED_WITH_ELEVATION"

    move v6, v10

    move v7, v3

    move v8, v2

    move v9, v2

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/gmm/map/r/a/s;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v4, Lcom/google/android/apps/gmm/map/r/a/s;->c:Lcom/google/android/apps/gmm/map/r/a/s;

    .line 23
    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/s;

    const-string v5, "SELECTED_UNIFORM"

    move v6, v11

    move v7, v3

    move v8, v3

    move v9, v2

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/gmm/map/r/a/s;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v4, Lcom/google/android/apps/gmm/map/r/a/s;->d:Lcom/google/android/apps/gmm/map/r/a/s;

    .line 24
    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/s;

    const-string v5, "UNSELECTED_UNIFORM"

    move v6, v12

    move v7, v2

    move v8, v3

    move v9, v2

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/gmm/map/r/a/s;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v4, Lcom/google/android/apps/gmm/map/r/a/s;->e:Lcom/google/android/apps/gmm/map/r/a/s;

    .line 18
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/r/a/s;

    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/s;->a:Lcom/google/android/apps/gmm/map/r/a/s;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/s;->b:Lcom/google/android/apps/gmm/map/r/a/s;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/s;->c:Lcom/google/android/apps/gmm/map/r/a/s;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/s;->d:Lcom/google/android/apps/gmm/map/r/a/s;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/s;->e:Lcom/google/android/apps/gmm/map/r/a/s;

    aput-object v1, v0, v12

    sput-object v0, Lcom/google/android/apps/gmm/map/r/a/s;->i:[Lcom/google/android/apps/gmm/map/r/a/s;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/map/r/a/s;->f:Z

    .line 32
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/map/r/a/s;->g:Z

    .line 33
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/map/r/a/s;->h:Z

    .line 34
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/r/a/s;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/android/apps/gmm/map/r/a/s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/s;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/r/a/s;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/s;->i:[Lcom/google/android/apps/gmm/map/r/a/s;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/r/a/s;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/r/a/s;

    return-object v0
.end method
