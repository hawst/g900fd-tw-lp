.class Lcom/google/android/apps/gmm/cardui/a/ah;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/apps/gmm/cardui/a/ah;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/cardui/a/ah;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 8

    .prologue
    .line 35
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->h()Lcom/google/android/apps/gmm/cardui/c;

    move-result-object v5

    .line 36
    if-nez v5, :cond_0

    .line 37
    sget-object v0, Lcom/google/android/apps/gmm/cardui/a/ah;->a:Ljava/lang/String;

    .line 41
    :cond_0
    iget-object v0, v5, Lcom/google/android/apps/gmm/cardui/c;->f:Lcom/google/o/h/a/nt;

    if-eqz v0, :cond_2

    .line 42
    iget-object v0, v5, Lcom/google/android/apps/gmm/cardui/c;->f:Lcom/google/o/h/a/nt;

    invoke-static {}, Lcom/google/o/h/a/nt;->newBuilder()Lcom/google/o/h/a/nv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/nv;->a(Lcom/google/o/h/a/nt;)Lcom/google/o/h/a/nv;

    move-result-object v0

    move-object v1, v0

    .line 46
    :goto_0
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/st;->d()Lcom/google/o/h/a/st;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/st;

    iget v0, v0, Lcom/google/o/h/a/st;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/ny;->a(I)Lcom/google/o/h/a/ny;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/o/h/a/ny;->a:Lcom/google/o/h/a/ny;

    .line 45
    :cond_1
    invoke-virtual {v1, v0}, Lcom/google/o/h/a/nv;->a(Lcom/google/o/h/a/ny;)Lcom/google/o/h/a/nv;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/google/o/h/a/nv;->g()Lcom/google/n/t;

    move-result-object v4

    check-cast v4, Lcom/google/o/h/a/nt;

    .line 48
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->k()Lcom/google/android/apps/gmm/cardui/b/c;

    move-result-object v0

    .line 50
    iget-object v1, v5, Lcom/google/android/apps/gmm/cardui/c;->h:Lcom/google/android/apps/gmm/cardui/b/b;

    const-string v2, ""

    .line 52
    iget-object v3, v5, Lcom/google/android/apps/gmm/cardui/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ltz v6, :cond_3

    const/4 v3, 0x1

    :goto_1
    if-nez v3, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 43
    :cond_2
    invoke-static {}, Lcom/google/o/h/a/nt;->newBuilder()Lcom/google/o/h/a/nv;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 52
    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    :cond_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v5, v5, Lcom/google/android/apps/gmm/cardui/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/gmm/cardui/f;

    iget-object v5, v5, Lcom/google/android/apps/gmm/cardui/f;->a:Lcom/google/o/h/a/br;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 55
    :cond_5
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->g()Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/apps/gmm/util/b/b;->e:Lcom/google/o/h/a/od;

    .line 54
    new-instance v6, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    iget-object v5, v5, Lcom/google/o/h/a/od;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v5

    check-cast v5, Lcom/google/r/b/a/ads;

    iget-object v7, v6, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v5, v7, Lcom/google/android/apps/gmm/base/g/i;->g:Lcom/google/r/b/a/ads;

    iget-boolean v5, v5, Lcom/google/r/b/a/ads;->v:Z

    iput-boolean v5, v6, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v5

    .line 55
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v5

    .line 49
    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/gmm/cardui/b/c;->a(Lcom/google/android/apps/gmm/cardui/b/b;Ljava/lang/String;Ljava/util/List;Lcom/google/o/h/a/nt;Lcom/google/android/apps/gmm/map/b/a/j;)Landroid/app/Fragment;

    move-result-object v0

    .line 56
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 57
    return-void
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    sget-object v0, Lcom/google/o/h/a/g;->j:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 31
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    .line 25
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
