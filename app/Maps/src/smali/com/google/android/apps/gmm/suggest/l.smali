.class public Lcom/google/android/apps/gmm/suggest/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public a:Ljava/io/Serializable;

.field public b:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/google/b/f/t;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field d:Ljava/lang/Boolean;

.field private e:Lcom/google/android/apps/gmm/suggest/e/c;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Lcom/google/android/apps/gmm/map/b/a/r;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private i:Lcom/google/android/apps/gmm/map/b/a/q;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private j:Lcom/google/android/apps/gmm/suggest/e/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private transient r:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/d;",
            ">;"
        }
    .end annotation
.end field

.field private s:Z

.field private t:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->a:Lcom/google/android/apps/gmm/suggest/e/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->e:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->f:Ljava/lang/String;

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->g:Ljava/lang/String;

    .line 63
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/suggest/l;->k:Z

    .line 68
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/suggest/l;->l:Z

    .line 73
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/suggest/l;->m:Z

    .line 78
    iput v2, p0, Lcom/google/android/apps/gmm/suggest/l;->n:I

    .line 83
    const/high16 v0, 0x12000000

    iput v0, p0, Lcom/google/android/apps/gmm/suggest/l;->o:I

    .line 88
    iput v1, p0, Lcom/google/android/apps/gmm/suggest/l;->p:I

    .line 93
    iput v1, p0, Lcom/google/android/apps/gmm/suggest/l;->q:I

    .line 100
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->r:Lcom/google/b/c/cv;

    .line 114
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/suggest/l;->t:Z

    .line 136
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->d:Ljava/lang/Boolean;

    return-void
.end method

.method private declared-synchronized q()Lcom/google/b/c/cv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->r:Lcom/google/b/c/cv;

    if-nez v0, :cond_0

    .line 321
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 323
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->r:Lcom/google/b/c/cv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 320
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/android/apps/gmm/suggest/e/c;
    .locals 1

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->e:Lcom/google/android/apps/gmm/suggest/e/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 281
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/apps/gmm/suggest/l;->n:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282
    monitor-exit p0

    return-void

    .line 281
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(II)V
    .locals 1

    .prologue
    .line 307
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/apps/gmm/suggest/l;->p:I

    .line 308
    iput p2, p0, Lcom/google/android/apps/gmm/suggest/l;->q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309
    monitor-exit p0

    return-void

    .line 307
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/map/b/a/q;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 236
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/suggest/l;->i:Lcom/google/android/apps/gmm/map/b/a/q;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    monitor-exit p0

    return-void

    .line 236
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/map/b/a/r;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/map/b/a/r;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/suggest/l;->h:Lcom/google/android/apps/gmm/map/b/a/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    monitor-exit p0

    return-void

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/suggest/e/b;)V
    .locals 1

    .prologue
    .line 245
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/suggest/l;->j:Lcom/google/android/apps/gmm/suggest/e/b;

    .line 246
    if-eqz p1, :cond_0

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/suggest/e/b;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    :cond_0
    monitor-exit p0

    return-void

    .line 245
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/suggest/e/c;)V
    .locals 1

    .prologue
    .line 199
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/suggest/l;->e:Lcom/google/android/apps/gmm/suggest/e/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    monitor-exit p0

    return-void

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/suggest/l;)V
    .locals 1

    .prologue
    .line 142
    monitor-enter p0

    if-ne p0, p1, :cond_0

    .line 165
    :goto_0
    monitor-exit p0

    return-void

    .line 145
    :cond_0
    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/l;->e:Lcom/google/android/apps/gmm/suggest/e/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->e:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 146
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/l;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->f:Ljava/lang/String;

    .line 147
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/l;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->g:Ljava/lang/String;

    .line 148
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/l;->h:Lcom/google/android/apps/gmm/map/b/a/r;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->h:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 149
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/l;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 150
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/l;->j:Lcom/google/android/apps/gmm/suggest/e/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->j:Lcom/google/android/apps/gmm/suggest/e/b;

    .line 151
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/suggest/l;->k:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/l;->k:Z

    .line 152
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/suggest/l;->l:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/l;->l:Z

    .line 153
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/suggest/l;->m:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/l;->m:Z

    .line 154
    iget v0, p1, Lcom/google/android/apps/gmm/suggest/l;->n:I

    iput v0, p0, Lcom/google/android/apps/gmm/suggest/l;->n:I

    .line 155
    iget v0, p1, Lcom/google/android/apps/gmm/suggest/l;->o:I

    iput v0, p0, Lcom/google/android/apps/gmm/suggest/l;->o:I

    .line 156
    iget v0, p1, Lcom/google/android/apps/gmm/suggest/l;->p:I

    iput v0, p0, Lcom/google/android/apps/gmm/suggest/l;->p:I

    .line 157
    iget v0, p1, Lcom/google/android/apps/gmm/suggest/l;->q:I

    iput v0, p0, Lcom/google/android/apps/gmm/suggest/l;->q:I

    .line 158
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/l;->r:Lcom/google/b/c/cv;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->r:Lcom/google/b/c/cv;

    .line 159
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/suggest/l;->s:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/l;->s:Z

    .line 160
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/l;->a:Ljava/io/Serializable;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->a:Ljava/io/Serializable;

    .line 161
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/suggest/l;->t:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/l;->t:Z

    .line 162
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/l;->c:Lcom/google/b/f/t;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->c:Lcom/google/b/f/t;

    .line 163
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/l;->b:Lcom/google/android/apps/gmm/x/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->b:Lcom/google/android/apps/gmm/x/o;

    .line 164
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/l;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->d:Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/b/c/cv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 327
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/suggest/l;->r:Lcom/google/b/c/cv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    monitor-exit p0

    return-void

    .line 327
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 207
    monitor-enter p0

    if-nez p1, :cond_1

    :try_start_0
    const-string v0, ""

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->f:Ljava/lang/String;

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->j:Lcom/google/android/apps/gmm/suggest/e/b;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->j:Lcom/google/android/apps/gmm/suggest/e/b;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/suggest/e/b;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    move-object v0, p1

    .line 207
    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 257
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/suggest/l;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    monitor-exit p0

    return-void

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->f:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)V
    .locals 1

    .prologue
    .line 290
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/apps/gmm/suggest/l;->o:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    monitor-exit p0

    return-void

    .line 290
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 218
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    const-string p1, ""

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/suggest/l;->g:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    monitor-exit p0

    return-void

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Z)V
    .locals 1

    .prologue
    .line 265
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/suggest/l;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    monitor-exit p0

    return-void

    .line 265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->g:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Z)V
    .locals 1

    .prologue
    .line 273
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/suggest/l;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 274
    monitor-exit p0

    return-void

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized d()Lcom/google/android/apps/gmm/map/b/a/r;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 223
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->h:Lcom/google/android/apps/gmm/map/b/a/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized d(Z)V
    .locals 1

    .prologue
    .line 331
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/suggest/l;->s:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    monitor-exit p0

    return-void

    .line 331
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 232
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->i:Lcom/google/android/apps/gmm/map/b/a/q;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e(Z)V
    .locals 1

    .prologue
    .line 339
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/suggest/l;->t:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 340
    monitor-exit p0

    return-void

    .line 339
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized f()Lcom/google/android/apps/gmm/suggest/e/b;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 241
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/l;->j:Lcom/google/android/apps/gmm/suggest/e/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Z
    .locals 1

    .prologue
    .line 253
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/l;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()Z
    .locals 1

    .prologue
    .line 261
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/l;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()Z
    .locals 1

    .prologue
    .line 269
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/l;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized j()I
    .locals 1

    .prologue
    .line 277
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/suggest/l;->n:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized k()I
    .locals 1

    .prologue
    .line 286
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/suggest/l;->o:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized l()Z
    .locals 1

    .prologue
    .line 295
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/suggest/l;->p:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized m()I
    .locals 1

    .prologue
    .line 299
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/suggest/l;->p:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized n()I
    .locals 1

    .prologue
    .line 303
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/suggest/l;->q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized o()Lcom/google/b/c/cv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 315
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/l;->s:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/suggest/l;->q()Lcom/google/b/c/cv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized p()Z
    .locals 1

    .prologue
    .line 335
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/l;->t:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 169
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "source"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/l;->e:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 170
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "query"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/l;->f:Ljava/lang/String;

    .line 171
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "hintText"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/l;->g:Ljava/lang/String;

    .line 172
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "viewportBounds"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/l;->h:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 173
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "myLocation"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/l;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 174
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "suggestDirectionsContext"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/l;->j:Lcom/google/android/apps/gmm/suggest/e/b;

    .line 175
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "allowEmptyQuery"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/suggest/l;->k:Z

    .line 176
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "canSubmitQuery"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/suggest/l;->l:Z

    .line 177
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "showSuggestionsForEmptyQuery"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/suggest/l;->m:Z

    .line 178
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "keyboardTransition"

    iget v2, p0, Lcom/google/android/apps/gmm/suggest/l;->n:I

    .line 179
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "imeOptions"

    iget v2, p0, Lcom/google/android/apps/gmm/suggest/l;->o:I

    .line 180
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "omniboxIconResource"

    iget v2, p0, Lcom/google/android/apps/gmm/suggest/l;->p:I

    .line 181
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "omniboxIconContentDescriptionResource"

    iget v2, p0, Lcom/google/android/apps/gmm/suggest/l;->q:I

    .line 182
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "suggestions"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/l;->r:Lcom/google/b/c/cv;

    .line 183
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "suggestionsVisible"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/suggest/l;->s:Z

    .line 184
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "callbackObject"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/l;->a:Ljava/io/Serializable;

    .line 185
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "searchboxLoggingEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/suggest/l;->t:Z

    .line 186
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "queryTextBoxVeType"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/l;->c:Lcom/google/b/f/t;

    .line 187
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "searchRequest"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/l;->b:Lcom/google/android/apps/gmm/x/o;

    .line 188
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "queryUpdated"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/l;->d:Ljava/lang/Boolean;

    .line 189
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 190
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
