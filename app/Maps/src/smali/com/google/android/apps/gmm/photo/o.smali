.class Lcom/google/android/apps/gmm/photo/o;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/apps/gmm/photo/o;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/o;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    :goto_0
    return-void

    .line 148
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 162
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid index in PhotoUploadDialog."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/o;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/o;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;

    iget-object v1, v1, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->b:Lcom/google/android/apps/gmm/photo/s;

    invoke-static {}, Lcom/google/android/apps/gmm/photo/s;->a()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->c:Landroid/net/Uri;

    .line 151
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 152
    const-string v1, "output"

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/o;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;

    iget-object v2, v2, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 153
    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/o;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 156
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 157
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/o;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
