.class public Lcom/google/android/apps/gmm/car/d/z;
.super Lcom/google/android/apps/gmm/map/f/t;
.source "PG"


# static fields
.field private static final a:Lcom/google/android/apps/gmm/map/f/f;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/google/android/apps/gmm/map/f/f;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/f/f;-><init>(Landroid/content/res/Resources;)V

    sput-object v0, Lcom/google/android/apps/gmm/car/d/z;->a:Lcom/google/android/apps/gmm/map/f/f;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/f/t;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 21
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/f/a/a;F)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 6

    .prologue
    .line 24
    new-instance v5, Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-direct {v5, p0}, Lcom/google/android/apps/gmm/map/f/a/c;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;)V

    iget v0, p0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    add-float/2addr v0, p1

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    .line 25
    sget-object v1, Lcom/google/android/apps/gmm/car/d/z;->a:Lcom/google/android/apps/gmm/map/f/f;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/apps/gmm/map/f/f;->a(Lcom/google/android/apps/gmm/map/f/a/c;)V

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/map/f/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/apps/gmm/map/f/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 36
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    move-result v0

    .line 38
    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->a:Lcom/google/android/apps/gmm/map/f/a/d;

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/gmm/car/d/z;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 39
    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->c:Lcom/google/android/apps/gmm/map/f/a/d;

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/gmm/car/d/z;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 40
    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->d:Lcom/google/android/apps/gmm/map/f/a/d;

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/gmm/car/d/z;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 41
    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->e:Lcom/google/android/apps/gmm/map/f/a/d;

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/gmm/car/d/z;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 43
    return v0
.end method
