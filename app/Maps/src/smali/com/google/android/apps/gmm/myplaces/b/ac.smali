.class Lcom/google/android/apps/gmm/myplaces/b/ac;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "PG"


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 435
    const-string v0, "gmm_myplaces.db"

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 436
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 441
    const-string v0, "CREATE TABLE sync_item (corpus INTEGER, key_string TEXT, timestamp BIGINT, merge_key BIGINT, feature_fprint BIGINT, latitude INTEGER, longitude INTEGER, is_local BOOLEAN, sync_item BLOB, PRIMARY KEY (corpus, key_string));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 452
    const-string v0, "CREATE INDEX idx_sync_item_timestamp ON sync_item (corpus, timestamp);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 455
    const-string v0, "CREATE INDEX idx_sync_item_merge_key ON sync_item (corpus, merge_key);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 458
    const-string v0, "CREATE INDEX idx_sync_item_feature_fprint ON sync_item (corpus, feature_fprint);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 461
    const-string v0, "CREATE INDEX idx_sync_item_lat_long ON sync_item (corpus, latitude, longitude);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 466
    const-string v0, "CREATE TABLE sync_corpus (corpus INTEGER PRIMARY KEY, last_sync_time BIGINT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 469
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 487
    const-string v0, "DROP TABLE IF EXISTS sync_item;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS sync_corpus;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 488
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/myplaces/b/ac;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 489
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 480
    const-string v0, "DROP TABLE IF EXISTS sync_item;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS sync_corpus;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 481
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/myplaces/b/ac;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 482
    return-void
.end method
