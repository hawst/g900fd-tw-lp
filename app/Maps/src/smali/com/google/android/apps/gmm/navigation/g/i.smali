.class public Lcom/google/android/apps/gmm/navigation/g/i;
.super Lcom/google/android/apps/gmm/directions/d/a;
.source "PG"


# instance fields
.field d:Z

.field private final e:Z

.field private final f:Lcom/google/android/apps/gmm/shared/net/ad;


# direct methods
.method public constructor <init>(Lcom/google/r/b/a/agd;Lcom/google/android/apps/gmm/shared/net/ad;Z)V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/directions/d/a;-><init>(Lcom/google/r/b/a/agd;Lcom/google/android/apps/gmm/directions/d/b;)V

    .line 60
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/g/i;->f:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 61
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/navigation/g/i;->e:Z

    .line 62
    return-void
.end method

.method static a(Lcom/google/maps/g/a/je;Lcom/google/o/b/a/v;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 95
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/google/maps/g/a/je;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    .line 97
    iget-wide v2, v0, Lcom/google/maps/g/gy;->b:D

    iget-wide v0, v0, Lcom/google/maps/g/gy;->c:D

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    .line 105
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 95
    goto :goto_0

    .line 99
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/je;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/jh;->a(I)Lcom/google/maps/g/a/jh;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/maps/g/a/jh;->e:Lcom/google/maps/g/a/jh;

    :cond_2
    sget-object v3, Lcom/google/maps/g/a/jh;->a:Lcom/google/maps/g/a/jh;

    if-ne v0, v3, :cond_4

    .line 100
    iget v0, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    .line 102
    iget-object v0, p1, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/l;->d()Lcom/google/o/b/a/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/l;

    .line 103
    iget v1, v0, Lcom/google/o/b/a/l;->b:I

    iget v0, v0, Lcom/google/o/b/a/l;->c:I

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(II)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 100
    goto :goto_2

    .line 105
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 2

    .prologue
    .line 154
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->i:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->k:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->l:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq p1, v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/i;->f:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/navigation/g/a/c;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/navigation/g/a/c;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 163
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/directions/d/a;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v0

    return v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 9
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 132
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/navigation/g/i;->d:Z

    .line 133
    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/maps/g/a/z;->e:Lcom/google/maps/g/a/z;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/g/j;->a(Lcom/google/maps/g/a/z;)Lcom/google/android/apps/gmm/navigation/g/j;

    move-result-object v0

    .line 134
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/i;->f:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/navigation/g/l;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/gmm/navigation/g/l;-><init>(Lcom/google/android/apps/gmm/navigation/g/i;Lcom/google/android/apps/gmm/navigation/g/j;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 135
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/directions/d/a;->onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V

    .line 136
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/d/a;->c:Lcom/google/android/apps/gmm/map/r/a/e;

    const-string v1, "directions"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/g/i;->a:Lcom/google/r/b/a/agd;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/i;->f:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/ad;->a()Landroid/content/Context;

    move-result-object v6

    iget-boolean v7, p0, Lcom/google/android/apps/gmm/navigation/g/i;->e:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/i;->a:Lcom/google/r/b/a/agd;

    iget-object v1, v1, Lcom/google/r/b/a/agd;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/afj;->g()Lcom/google/r/b/a/afj;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/afj;

    iget v2, v1, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v8, 0x800

    if-ne v2, v8, :cond_3

    move v2, v3

    :goto_1
    if-eqz v2, :cond_5

    iget-object v2, v1, Lcom/google/r/b/a/afj;->o:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aha;->d()Lcom/google/r/b/a/aha;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/aha;

    iget v2, v2, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_4

    move v2, v3

    :goto_2
    if-eqz v2, :cond_5

    iget-object v1, v1, Lcom/google/r/b/a/afj;->o:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aha;->d()Lcom/google/r/b/a/aha;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/aha;

    iget v1, v1, Lcom/google/r/b/a/aha;->b:I

    invoke-static {v1}, Lcom/google/maps/g/wq;->a(I)Lcom/google/maps/g/wq;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/maps/g/wq;->a:Lcom/google/maps/g/wq;

    :cond_2
    :goto_3
    invoke-static {v5, v0, v6, v7, v1}, Lcom/google/android/apps/gmm/navigation/g/j;->a(Lcom/google/r/b/a/agd;Lcom/google/android/apps/gmm/map/r/a/e;Landroid/content/Context;ZLcom/google/maps/g/wq;)Lcom/google/android/apps/gmm/navigation/g/j;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v2, v4

    goto :goto_1

    :cond_4
    move v2, v4

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    goto :goto_3
.end method
