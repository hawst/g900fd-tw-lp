.class public final Lcom/google/android/apps/gmm/directions/f/d/e;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/apps/gmm/map/r/a/p;)Lcom/google/maps/g/a/fk;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/p;->a:Lcom/google/maps/g/a/ea;

    iget-object v0, v0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v2, v3

    .line 36
    :goto_0
    if-ge v2, v5, :cond_4

    .line 37
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/r/a/p;->a(I)Lcom/google/android/apps/gmm/map/r/a/al;

    move-result-object v1

    .line 38
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget v0, v0, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v6, 0x2

    if-ne v0, v6, :cond_1

    move v0, v4

    :goto_1
    if-eqz v0, :cond_3

    .line 39
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v0, v0, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fk;

    .line 42
    iget v1, v0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_2

    move v1, v4

    :goto_2
    if-eqz v1, :cond_3

    .line 43
    iget v1, v0, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    :cond_0
    sget-object v6, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-ne v1, v6, :cond_3

    .line 47
    :goto_3
    return-object v0

    :cond_1
    move v0, v3

    .line 38
    goto :goto_1

    :cond_2
    move v1, v3

    .line 42
    goto :goto_2

    .line 36
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 47
    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Lcom/google/maps/g/a/fk;)Ljava/lang/String;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 55
    iget v0, p1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 56
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/gy;->d()Lcom/google/maps/g/a/gy;

    move-result-object v0

    .line 57
    :goto_1
    iget v3, v0, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit16 v3, v3, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_2

    move v3, v1

    :goto_2
    if-eqz v3, :cond_4

    .line 58
    iget-object v0, v0, Lcom/google/maps/g/a/gy;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/gu;

    .line 59
    iget v3, v0, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    :goto_3
    if-eqz v1, :cond_4

    .line 60
    iget-object v0, v0, Lcom/google/maps/g/a/gu;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fo;

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;Lcom/google/maps/g/a/fo;)Ljava/lang/String;

    move-result-object v0

    .line 64
    :goto_4
    return-object v0

    :cond_0
    move v0, v2

    .line 55
    goto :goto_0

    .line 56
    :cond_1
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    goto :goto_1

    :cond_2
    move v3, v2

    .line 57
    goto :goto_2

    :cond_3
    move v1, v2

    .line 59
    goto :goto_3

    .line 64
    :cond_4
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public static a(Lcom/google/maps/g/a/fk;)Ljava/lang/String;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 72
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_5

    .line 73
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/gy;->d()Lcom/google/maps/g/a/gy;

    move-result-object v0

    move-object v1, v0

    .line 74
    :goto_1
    iget v0, v1, Lcom/google/maps/g/a/gy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_5

    .line 75
    iget-object v0, v1, Lcom/google/maps/g/a/gy;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/String;

    .line 78
    :goto_3
    return-object v0

    :cond_0
    move v0, v3

    .line 72
    goto :goto_0

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    move-object v1, v0

    goto :goto_1

    :cond_2
    move v0, v3

    .line 74
    goto :goto_2

    .line 75
    :cond_3
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    iput-object v2, v1, Lcom/google/maps/g/a/gy;->b:Ljava/lang/Object;

    :cond_4
    move-object v0, v2

    goto :goto_3

    .line 78
    :cond_5
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static b(Landroid/content/Context;Lcom/google/maps/g/a/fk;)Ljava/lang/String;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 115
    iget v0, p1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 116
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    .line 117
    :goto_1
    iget v3, v0, Lcom/google/maps/g/a/be;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_2

    :goto_2
    if-eqz v1, :cond_3

    .line 118
    iget v0, v0, Lcom/google/maps/g/a/be;->b:I

    .line 119
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 122
    :goto_3
    return-object v0

    :cond_0
    move v0, v2

    .line 115
    goto :goto_0

    .line 116
    :cond_1
    iget-object v0, p1, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    goto :goto_1

    :cond_2
    move v1, v2

    .line 117
    goto :goto_2

    .line 122
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static b(Lcom/google/maps/g/a/fk;)Ljava/lang/String;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 101
    iget v0, p0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_5

    .line 102
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->m:Lcom/google/maps/g/a/cs;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/cs;->d()Lcom/google/maps/g/a/cs;

    move-result-object v0

    move-object v1, v0

    .line 103
    :goto_1
    iget v0, v1, Lcom/google/maps/g/a/cs;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_5

    .line 104
    iget-object v0, v1, Lcom/google/maps/g/a/cs;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/String;

    .line 107
    :goto_3
    return-object v0

    :cond_0
    move v0, v3

    .line 101
    goto :goto_0

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->m:Lcom/google/maps/g/a/cs;

    move-object v1, v0

    goto :goto_1

    :cond_2
    move v0, v3

    .line 103
    goto :goto_2

    .line 104
    :cond_3
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    iput-object v2, v1, Lcom/google/maps/g/a/cs;->c:Ljava/lang/Object;

    :cond_4
    move-object v0, v2

    goto :goto_3

    .line 107
    :cond_5
    const/4 v0, 0x0

    goto :goto_3
.end method
