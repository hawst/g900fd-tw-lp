.class public final enum Lcom/google/android/apps/gmm/iamhere/c/r;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/iamhere/c/r;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/iamhere/c/r;

.field public static final enum b:Lcom/google/android/apps/gmm/iamhere/c/r;

.field public static final enum c:Lcom/google/android/apps/gmm/iamhere/c/r;

.field public static final enum d:Lcom/google/android/apps/gmm/iamhere/c/r;

.field private static final synthetic h:[Lcom/google/android/apps/gmm/iamhere/c/r;


# instance fields
.field private final e:D

.field private final f:D

.field private final g:D


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 21
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/r;

    const-string v1, "NEAR"

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/iamhere/c/r;-><init>(Ljava/lang/String;ILjava/lang/Double;)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/r;->a:Lcom/google/android/apps/gmm/iamhere/c/r;

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/r;

    const-string v1, "MID"

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/iamhere/c/r;-><init>(Ljava/lang/String;ILjava/lang/Double;)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/r;->b:Lcom/google/android/apps/gmm/iamhere/c/r;

    .line 23
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/r;

    const-string v1, "FAR"

    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/gmm/iamhere/c/r;-><init>(Ljava/lang/String;ILjava/lang/Double;)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/r;->c:Lcom/google/android/apps/gmm/iamhere/c/r;

    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/r;

    const-string v1, "VERY_FAR"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/apps/gmm/iamhere/c/r;-><init>(Ljava/lang/String;ILjava/lang/Double;)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/r;->d:Lcom/google/android/apps/gmm/iamhere/c/r;

    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/iamhere/c/r;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/r;->a:Lcom/google/android/apps/gmm/iamhere/c/r;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/r;->b:Lcom/google/android/apps/gmm/iamhere/c/r;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/r;->c:Lcom/google/android/apps/gmm/iamhere/c/r;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/r;->d:Lcom/google/android/apps/gmm/iamhere/c/r;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/r;->h:[Lcom/google/android/apps/gmm/iamhere/c/r;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Double;)V
    .locals 4
    .param p3    # Ljava/lang/Double;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Double;",
            ")V"
        }
    .end annotation

    .prologue
    const-wide/16 v0, 0x0

    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    if-eqz p3, :cond_0

    .line 48
    invoke-virtual {p3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/r;->e:D

    .line 50
    invoke-virtual {p3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/high16 v2, -0x3ff8000000000000L    # -3.0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/iamhere/c/r;->a(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/r;->f:D

    .line 52
    invoke-virtual {p3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/iamhere/c/r;->a(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/r;->g:D

    .line 58
    :goto_0
    return-void

    .line 54
    :cond_0
    iput-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/r;->e:D

    .line 55
    iput-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/r;->f:D

    .line 56
    iput-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/r;->g:D

    goto :goto_0
.end method

.method private static a(DD)D
    .locals 8

    .prologue
    const-wide v6, 0x4044800000000000L    # 41.0

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    .line 96
    invoke-static {p0, p1}, Ljava/lang/Math;->log10(D)D

    move-result-wide v0

    mul-double/2addr v0, v4

    add-double/2addr v0, v6

    add-double/2addr v0, p2

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    sub-double/2addr v0, v6

    div-double/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/iamhere/c/r;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/gmm/iamhere/c/r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/r;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/iamhere/c/r;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/r;->h:[Lcom/google/android/apps/gmm/iamhere/c/r;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/iamhere/c/r;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/iamhere/c/r;

    return-object v0
.end method
