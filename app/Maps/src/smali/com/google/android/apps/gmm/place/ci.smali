.class public Lcom/google/android/apps/gmm/place/ci;
.super Lcom/google/android/apps/gmm/shared/net/i;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public final a:Lcom/google/e/a/a/a/b;

.field private final c:Lcom/google/android/apps/gmm/place/b/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private d:Lcom/google/android/apps/gmm/base/g/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/google/android/apps/gmm/place/ci;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/ci;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/base/activities/o;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/maps/a/a;Lcom/google/android/apps/gmm/place/ck;Lcom/google/e/a/a/a/b;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/place/b/c;)V
    .locals 3
    .param p5    # Lcom/google/android/apps/gmm/place/ck;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Lcom/google/android/apps/gmm/place/b/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 118
    sget-object v0, Lcom/google/r/b/a/el;->bO:Lcom/google/r/b/a/el;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/shared/net/i;-><init>(Lcom/google/r/b/a/el;)V

    .line 119
    invoke-static {p1, p4, p5, p6}, Lcom/google/android/apps/gmm/place/ci;->a(Lcom/google/android/apps/gmm/base/activities/o;Lcom/google/maps/a/a;Lcom/google/android/apps/gmm/place/ck;Lcom/google/e/a/a/a/b;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/ci;->a:Lcom/google/e/a/a/a/b;

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ci;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x2

    iget-object v0, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v1, p2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ci;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x1

    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/b/a/j;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 123
    if-eqz p7, :cond_0

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ci;->a:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/g/b/r;->a:Lcom/google/e/a/a/a/d;

    .line 125
    invoke-static {p7, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v2

    .line 124
    iget-object v0, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 128
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ci;->a:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x11

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v1, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 132
    iput-object p8, p0, Lcom/google/android/apps/gmm/place/ci;->c:Lcom/google/android/apps/gmm/place/b/c;

    .line 133
    return-void
.end method

.method public constructor <init>(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/place/b/c;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/place/b/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 101
    sget-object v0, Lcom/google/r/b/a/el;->bO:Lcom/google/r/b/a/el;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/shared/net/i;-><init>(Lcom/google/r/b/a/el;)V

    .line 102
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/ci;->a:Lcom/google/e/a/a/a/b;

    .line 103
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/ci;->c:Lcom/google/android/apps/gmm/place/b/c;

    .line 104
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/o;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/place/b/c;Lcom/google/android/apps/gmm/place/ck;Lcom/google/e/a/a/a/b;Lcom/google/maps/g/hy;)Lcom/google/android/apps/gmm/place/ci;
    .locals 9
    .param p3    # Lcom/google/android/apps/gmm/place/b/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/place/ck;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 88
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 89
    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/activities/o;->e()Lcom/google/android/apps/gmm/map/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v4

    .line 91
    if-nez v4, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/place/ci;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/place/ci;-><init>(Lcom/google/android/apps/gmm/base/activities/o;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/maps/a/a;Lcom/google/android/apps/gmm/place/ck;Lcom/google/e/a/a/a/b;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/place/b/c;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/o;Lcom/google/maps/a/a;Lcom/google/android/apps/gmm/place/ck;Lcom/google/e/a/a/a/b;)Lcom/google/e/a/a/a/b;
    .locals 12
    .param p2    # Lcom/google/android/apps/gmm/place/ck;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 211
    sget-object v0, Lcom/google/maps/a/a/a;->a:Lcom/google/e/a/a/a/d;

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 212
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/r/b/a/b/ax;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 214
    const/4 v2, 0x3

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 215
    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/activities/o;->e()Lcom/google/android/apps/gmm/map/t;

    move-result-object v2

    .line 216
    const/4 v0, 0x7

    .line 218
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    .line 219
    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/activities/o;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 217
    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/search/bc;->a(Landroid/graphics/Point;Landroid/content/res/Resources;)Lcom/google/e/a/a/a/b;

    move-result-object v3

    .line 216
    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 220
    const/4 v0, 0x4

    sget-object v3, Lcom/google/r/b/a/acy;->m:Lcom/google/r/b/a/acy;

    .line 221
    iget v3, v3, Lcom/google/r/b/a/acy;->t:I

    .line 220
    int-to-long v4, v3

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 222
    const/4 v3, 0x5

    if-eqz p2, :cond_2

    .line 224
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/place/ck;->a()Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 222
    :goto_0
    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v3, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 226
    const/16 v3, 0xe

    sget-object v0, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v3, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 229
    new-instance v3, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/maps/g/b/am;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {v3, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 231
    new-instance v4, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/maps/g/b/am;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v4, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 237
    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/activities/o;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v5, 0x43800000    # 256.0f

    mul-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    const/high16 v5, 0x43800000    # 256.0f

    div-float v5, v0, v5

    .line 238
    const/4 v0, 0x5

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    int-to-long v6, v6

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v6

    iget-object v7, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v7, v0, v6}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 242
    const/4 v6, 0x1

    new-instance v7, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/maps/a/a/a;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {v7, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/4 v0, 0x2

    const/16 v8, 0x67

    .line 244
    int-to-long v8, v8

    invoke-static {v8, v9}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v8

    iget-object v9, v7, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v9, v0, v8}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v8, 0x1

    .line 245
    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/activities/o;->f()Lcom/google/android/apps/gmm/base/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v9

    new-instance v10, Landroid/graphics/Point;

    invoke-direct {v10}, Landroid/graphics/Point;-><init>()V

    const-string v0, "window"

    invoke-virtual {v9, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    invoke-static {v9}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_3

    iget v0, v10, Landroid/graphics/Point;->x:I

    iget v10, v10, Landroid/graphics/Point;->y:I

    invoke-static {v0, v10}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_1
    int-to-float v0, v0

    div-float/2addr v0, v5

    float-to-int v10, v0

    if-eqz v9, :cond_4

    const/16 v0, 0x40

    :goto_2
    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v10, v0

    const/high16 v9, 0x45000000    # 2048.0f

    div-float v5, v9, v5

    float-to-int v5, v5

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-long v10, v0

    invoke-static {v10, v11}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v5, v7, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v8, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 242
    iget-object v0, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v6, v7}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 247
    const/4 v0, 0x2

    sget-object v5, Lcom/google/android/apps/gmm/place/c/w;->a:Lcom/google/maps/g/tm;

    .line 248
    iget v5, v5, Lcom/google/maps/g/tm;->e:I

    .line 247
    int-to-long v6, v5

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v5

    iget-object v6, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v6, v0, v5}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 249
    const/4 v0, 0x3

    const/16 v5, 0xf

    int-to-long v6, v5

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v5

    iget-object v6, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v6, v0, v5}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 252
    iget-object v0, p1, Lcom/google/maps/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    iget-wide v6, v0, Lcom/google/maps/a/e;->d:D

    double-to-float v0, v6

    .line 253
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/f/o;->b(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 254
    const/4 v2, 0x4

    int-to-long v6, v0

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 255
    const/4 v0, 0x2

    invoke-virtual {v3, v0, v4}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 256
    const/16 v0, 0x10

    iget-object v2, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v0, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 260
    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/activities/o;->f()Lcom/google/android/apps/gmm/base/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    .line 261
    if-eqz v0, :cond_0

    .line 262
    const/16 v2, 0xb

    .line 264
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v0

    sget-object v3, Lcom/google/o/b/a/a/a;->f:Lcom/google/e/a/a/a/d;

    .line 263
    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 262
    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 268
    :cond_0
    new-instance v2, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/maps/g/b/ac;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v2, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 269
    if-nez p3, :cond_5

    .line 270
    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/activities/o;->f()Lcom/google/android/apps/gmm/base/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->a()V

    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 271
    :goto_3
    if-eqz v0, :cond_1

    .line 272
    const/4 v0, 0x1

    iget-object v3, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, p3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 274
    :cond_1
    invoke-static {}, Lcom/google/maps/g/pj;->newBuilder()Lcom/google/maps/g/pl;

    move-result-object v0

    const/4 v3, 0x0

    .line 275
    iget v4, v0, Lcom/google/maps/g/pl;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/maps/g/pl;->a:I

    iput-boolean v3, v0, Lcom/google/maps/g/pl;->b:Z

    const/4 v3, 0x1

    .line 276
    iget v4, v0, Lcom/google/maps/g/pl;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v0, Lcom/google/maps/g/pl;->a:I

    iput-boolean v3, v0, Lcom/google/maps/g/pl;->c:Z

    .line 277
    invoke-virtual {v0}, Lcom/google/maps/g/pl;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/pj;

    .line 278
    const/4 v3, 0x5

    sget-object v4, Lcom/google/maps/g/b/ah;->f:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v3, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 280
    const/16 v3, 0x10

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v3, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 283
    const/16 v0, 0xc

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 284
    invoke-static {v1}, Lcom/google/android/apps/gmm/util/a;->b(Lcom/google/e/a/a/a/b;)V

    .line 286
    const/16 v0, 0x12

    .line 288
    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/activities/o;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/base/i/f;->a(Landroid/content/res/Resources;)Lcom/google/r/b/a/ado;

    move-result-object v2

    sget-object v3, Lcom/google/r/b/a/b/aw;->r:Lcom/google/e/a/a/a/d;

    .line 287
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v2

    .line 286
    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 291
    return-object v1

    .line 225
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/place/cl;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/cl;-><init>()V

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v0, Lcom/google/android/apps/gmm/place/cl;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/cl;->a()Lcom/google/android/apps/gmm/place/ck;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/ck;->a()Lcom/google/e/a/a/a/b;

    move-result-object v0

    goto/16 :goto_0

    .line 245
    :cond_3
    iget v0, v10, Landroid/graphics/Point;->x:I

    iget v10, v10, Landroid/graphics/Point;->y:I

    invoke-static {v0, v10}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/16 :goto_1

    :cond_4
    const/4 v0, 0x4

    goto/16 :goto_2

    :cond_5
    move-object v0, p3

    .line 270
    goto :goto_3
.end method


# virtual methods
.method protected final R_()I
    .locals 1

    .prologue
    .line 346
    const/4 v0, 0x0

    return v0
.end method

.method protected final a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 9

    .prologue
    const/16 v8, 0x1c

    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 144
    sget-object v0, Lcom/google/r/b/a/aes;->PARSER:Lcom/google/n/ax;

    .line 147
    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/ax;Ljava/io/DataInput;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aes;

    .line 149
    iget v1, v0, Lcom/google/r/b/a/aes;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    move v1, v2

    :goto_0
    if-nez v1, :cond_1

    .line 150
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->k:Lcom/google/android/apps/gmm/shared/net/k;

    .line 176
    :goto_1
    return-object v0

    :cond_0
    move v1, v3

    .line 149
    goto :goto_0

    .line 152
    :cond_1
    new-instance v4, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 153
    iget-object v1, v0, Lcom/google/r/b/a/aes;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/ads;

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v1, v5, Lcom/google/android/apps/gmm/base/g/i;->g:Lcom/google/r/b/a/ads;

    iget-boolean v1, v1, Lcom/google/r/b/a/ads;->v:Z

    iput-boolean v1, v4, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    .line 154
    new-instance v1, Ljava/util/ArrayList;

    iget-object v5, v0, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ada;->g()Lcom/google/r/b/a/ada;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ada;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    invoke-static {v1}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/g/g;->k:Lcom/google/b/c/cv;

    .line 155
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/ci;->d:Lcom/google/android/apps/gmm/base/g/c;

    .line 156
    sget-object v0, Lcom/google/android/apps/gmm/place/ci;->b:Ljava/lang/String;

    const-string v1, "received a placemark for feature id [%s] and query [%s]"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ci;->a:Lcom/google/e/a/a/a/b;

    .line 173
    invoke-virtual {v0, v2, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ci;->a:Lcom/google/e/a/a/a/b;

    .line 174
    invoke-virtual {v0, v7, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v2

    .line 172
    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 176
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final a(Ljava/io/DataOutput;)V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ci;->a:Lcom/google/e/a/a/a/b;

    instance-of v1, p1, Ljava/io/OutputStream;

    if-eqz v1, :cond_0

    check-cast p1, Ljava/io/OutputStream;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/e/a/a/a/b;->a(Ljava/io/OutputStream;Z)V

    .line 182
    :goto_0
    return-void

    .line 181
    :cond_0
    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Ljava/io/DataOutput;Lcom/google/e/a/a/a/b;)V

    goto :goto_0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ci;->c:Lcom/google/android/apps/gmm/place/b/c;

    if-eqz v0, :cond_0

    .line 336
    if-nez p1, :cond_1

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ci;->c:Lcom/google/android/apps/gmm/place/b/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ci;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/place/b/c;->a(Lcom/google/android/apps/gmm/shared/net/i;Lcom/google/android/apps/gmm/base/g/c;)V

    .line 342
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ci;->c:Lcom/google/android/apps/gmm/place/b/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ci;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/place/b/c;->a(Lcom/google/android/apps/gmm/shared/net/i;)V

    goto :goto_0
.end method
