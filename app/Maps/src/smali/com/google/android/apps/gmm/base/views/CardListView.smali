.class public Lcom/google/android/apps/gmm/base/views/CardListView;
.super Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/views/listview/a;


# instance fields
.field a:Lcom/google/android/apps/gmm/base/i/a;

.field b:Landroid/widget/AbsListView$OnScrollListener;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/AbsListView$OnScrollListener;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;",
            ">;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Landroid/widget/ListAdapter;

.field private final n:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->k:Ljava/util/Map;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->l:Ljava/util/List;

    .line 47
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->n:[I

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->c:Ljava/util/List;

    .line 60
    sget-object v0, Lcom/google/android/apps/gmm/n;->a:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 61
    sget v1, Lcom/google/android/apps/gmm/n;->c:I

    .line 62
    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 63
    sget v2, Lcom/google/android/apps/gmm/n;->d:I

    .line 64
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 65
    sget v3, Lcom/google/android/apps/gmm/n;->b:I

    .line 66
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    .line 67
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 69
    new-instance v0, Lcom/google/android/apps/gmm/base/views/l;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/l;-><init>(Lcom/google/android/apps/gmm/base/views/CardListView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/CardListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 76
    new-instance v0, Lcom/google/android/apps/gmm/base/i/a;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/base/i/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->a:Lcom/google/android/apps/gmm/base/i/a;

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->a:Lcom/google/android/apps/gmm/base/i/a;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/i/a;->g:Z

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/base/i/a;->h:Z

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/i/a;->i:Z

    .line 78
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CardListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->m:Landroid/widget/ListAdapter;

    .line 80
    new-instance v0, Lcom/google/android/apps/gmm/base/views/m;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/m;-><init>(Lcom/google/android/apps/gmm/base/views/CardListView;)V

    invoke-super {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 102
    return-void

    .line 47
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public final a(Landroid/view/View;IJLcom/google/android/apps/gmm/base/views/listview/c;)V
    .locals 7

    .prologue
    .line 133
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/b/b;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/base/b/b;->b(I)Lcom/google/android/libraries/curvular/bk;

    move-result-object v0

    .line 134
    if-eqz v0, :cond_0

    .line 135
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->k:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 136
    instance-of v1, v0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 137
    check-cast v1, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->n:[I

    const/4 v2, 0x1

    aget v3, v0, v2

    move-object v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a(Landroid/view/View;IJLcom/google/android/apps/gmm/base/views/listview/c;)V

    .line 140
    :cond_0
    return-void
.end method

.method public addView(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 146
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 147
    check-cast v0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->a:Lcom/google/android/apps/gmm/base/i/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/b/b;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/base/i/a;->a(Lcom/google/android/apps/gmm/base/b/b;)Lcom/google/android/apps/gmm/base/i/a;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->j:Lcom/google/android/libraries/curvular/bk;

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/i/c;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/i/c;->q:Lcom/google/android/libraries/curvular/bk;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->k:Ljava/util/Map;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->j:Lcom/google/android/libraries/curvular/bk;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->setMaster(Lcom/google/android/apps/gmm/base/views/listview/a;)V

    .line 153
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    return-void

    .line 148
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 149
    check-cast v0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->a:Lcom/google/android/apps/gmm/base/i/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->m:Landroid/widget/ListAdapter;

    new-instance v3, Lcom/google/android/apps/gmm/base/i/c;

    sget-object v4, Lcom/google/android/apps/gmm/base/i/e;->b:Lcom/google/android/apps/gmm/base/i/e;

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/base/i/c;-><init>(Lcom/google/android/apps/gmm/base/i/e;)V

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/i/c;->c:Landroid/widget/ListAdapter;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/i/a;->a(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;)Lcom/google/android/apps/gmm/base/i/a;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->j:Lcom/google/android/libraries/curvular/bk;

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/i/c;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/i/c;->q:Lcom/google/android/libraries/curvular/bk;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->k:Ljava/util/Map;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->j:Lcom/google/android/libraries/curvular/bk;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->setMaster(Lcom/google/android/apps/gmm/base/views/listview/a;)V

    goto :goto_0

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->a:Lcom/google/android/apps/gmm/base/i/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/i/a;->a(Landroid/view/View;)Lcom/google/android/apps/gmm/base/i/a;

    goto :goto_0
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 162
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/CardListView;->addView(Landroid/view/View;)V

    .line 163
    return-void
.end method

.method protected findViewTraversal(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 179
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->findViewTraversal(I)Landroid/view/View;

    move-result-object v0

    .line 180
    if-nez v0, :cond_1

    .line 181
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 182
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 183
    if-eqz v0, :cond_0

    .line 188
    :cond_1
    return-object v0
.end method

.method protected findViewWithTagTraversal(Ljava/lang/Object;)Landroid/view/View;
    .locals 3

    .prologue
    .line 196
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->findViewWithTagTraversal(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 197
    if-nez v0, :cond_1

    .line 198
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 199
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 200
    if-eqz v0, :cond_0

    .line 205
    :cond_1
    return-object v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/b/b;

    return-object v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/b/b;

    return-object v0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->a:Lcom/google/android/apps/gmm/base/i/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/i/a;->a()Lcom/google/android/apps/gmm/base/b/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/CardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 110
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->onFinishInflate()V

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->a:Lcom/google/android/apps/gmm/base/i/a;

    .line 112
    return-void
.end method

.method public performItemClick(Landroid/view/View;IJ)Z
    .locals 3

    .prologue
    .line 121
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/b/b;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/base/b/b;->b(I)Lcom/google/android/libraries/curvular/bk;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_0

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->k:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 124
    instance-of v1, v0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;

    if-eqz v1, :cond_0

    .line 125
    check-cast v0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->n:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {v0, p1, v1, p3, p4}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->performItemClick(Landroid/view/View;IJ)Z

    move-result v0

    .line 128
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sendAccessibilityEvent(I)V
    .locals 1

    .prologue
    .line 168
    const v0, 0x8000

    if-ne p1, v0, :cond_0

    .line 172
    :goto_0
    return-void

    .line 171
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method public final setChildAdapter(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 213
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/b/b;

    .line 214
    if-eqz v0, :cond_0

    .line 215
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->j:Lcom/google/android/libraries/curvular/bk;

    if-eqz p2, :cond_1

    :goto_0
    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/gmm/base/b/b;->b(Lcom/google/android/libraries/curvular/bk;Landroid/widget/ListAdapter;)V

    .line 217
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/CardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 218
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CardListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 219
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 220
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 223
    :cond_0
    return-void

    .line 215
    :cond_1
    iget-object p2, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->m:Landroid/widget/ListAdapter;

    goto :goto_0
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/CardListView;->b:Landroid/widget/AbsListView$OnScrollListener;

    .line 250
    return-void
.end method
