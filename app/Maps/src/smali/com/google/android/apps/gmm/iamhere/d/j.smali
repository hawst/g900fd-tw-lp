.class public Lcom/google/android/apps/gmm/iamhere/d/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/iamhere/d/i;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/gmm/iamhere/c/a;

.field private final c:Lcom/google/android/apps/gmm/iamhere/c/h;

.field private final d:Z

.field private final e:Ljava/text/DateFormat;

.field private final f:Lcom/google/android/apps/gmm/iamhere/d/m;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/iamhere/c/a;Lcom/google/android/apps/gmm/iamhere/c/h;Z)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->a:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->b:Lcom/google/android/apps/gmm/iamhere/c/a;

    .line 35
    iput-object p3, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->c:Lcom/google/android/apps/gmm/iamhere/c/h;

    .line 36
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->d:Z

    .line 37
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->e:Ljava/text/DateFormat;

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/m;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/iamhere/d/m;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->f:Lcom/google/android/apps/gmm/iamhere/d/m;

    .line 39
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->b:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->b:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->b:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    .line 49
    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/d;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 4

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->b:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/a;->c:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->b:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/a;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 55
    :goto_0
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/k;

    sget-object v2, Lcom/google/android/apps/gmm/util/webimageview/b;->c:Lcom/google/android/apps/gmm/util/webimageview/b;

    sget v3, Lcom/google/android/apps/gmm/f;->fK:I

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;I)V

    return-object v1

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    .line 71
    if-nez p1, :cond_1

    sget-object v0, Lcom/google/b/f/t;->bb:Lcom/google/b/f/t;

    move-object v1, v0

    .line 74
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->b:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 75
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->f:Lcom/google/android/apps/gmm/iamhere/d/m;

    sget-object v4, Lcom/google/android/apps/gmm/iamhere/d/o;->b:Lcom/google/android/apps/gmm/iamhere/d/o;

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->b:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    .line 77
    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/d;

    const/4 v5, 0x0

    .line 75
    invoke-virtual {v3, v4, v0, v1, v5}, Lcom/google/android/apps/gmm/iamhere/d/m;->a(Lcom/google/android/apps/gmm/iamhere/d/o;Lcom/google/android/apps/gmm/iamhere/c/d;Lcom/google/b/f/t;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 80
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 71
    :cond_1
    sget-object v0, Lcom/google/b/f/t;->bc:Lcom/google/b/f/t;

    move-object v1, v0

    goto :goto_0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 61
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->b:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->b:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(I)Lcom/google/android/libraries/curvular/aw;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->b:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    if-gez p1, :cond_1

    :cond_0
    move-object v0, v1

    .line 90
    :goto_0
    return-object v0

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->b:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/d;

    .line 89
    invoke-static {v0}, Lcom/google/android/apps/gmm/iamhere/d/a;->a(Lcom/google/android/apps/gmm/iamhere/c/d;)I

    move-result v0

    .line 90
    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 10

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->d:Z

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->c:Lcom/google/android/apps/gmm/iamhere/c/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/h;->d:Lcom/google/android/apps/gmm/iamhere/c/r;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->c:Lcom/google/android/apps/gmm/iamhere/c/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/h;->i:Lcom/google/b/c/dn;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->e:Ljava/text/DateFormat;

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->c:Lcom/google/android/apps/gmm/iamhere/c/h;

    iget-wide v4, v3, Lcom/google/android/apps/gmm/iamhere/c/h;->e:J

    .line 98
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->e:Ljava/text/DateFormat;

    iget-object v4, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->c:Lcom/google/android/apps/gmm/iamhere/c/h;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/iamhere/c/h;->f:J

    .line 99
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->c:Lcom/google/android/apps/gmm/iamhere/c/h;

    iget-wide v6, v0, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->e:Ljava/text/DateFormat;

    iget-object v5, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->c:Lcom/google/android/apps/gmm/iamhere/c/h;

    iget-wide v6, v5, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    .line 101
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/gmm/iamhere/d/j;->c:Lcom/google/android/apps/gmm/iamhere/c/h;

    iget-object v5, v5, Lcom/google/android/apps/gmm/iamhere/c/h;->g:Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x31

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "region "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ", sources "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", start "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", last "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", dismissed-at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 104
    :goto_1
    return-object v0

    .line 101
    :cond_0
    const-string v0, ""

    goto/16 :goto_0

    .line 104
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method
