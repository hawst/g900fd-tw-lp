.class public Lcom/google/android/apps/gmm/base/placelists/n;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/share/a/a;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/r/b/a/ru;

.field private final c:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/gmm/base/placelists/n;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/placelists/n;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/r/b/a/ru;Lcom/google/android/apps/gmm/x/o;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/r/b/a/ru;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/placelists/n;->b:Lcom/google/r/b/a/ru;

    .line 30
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/placelists/n;->c:Lcom/google/android/apps/gmm/x/o;

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/z/a/b;Landroid/content/pm/ResolveInfo;)V
    .locals 11

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/n;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/apps/gmm/base/g/c;

    .line 36
    sget-object v0, Lcom/google/android/apps/gmm/base/placelists/n;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/n;->b:Lcom/google/r/b/a/ru;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x21

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Attempting to log placemark "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    const/4 v0, 0x1

    new-array v9, v0, [Lcom/google/android/apps/gmm/z/b/a;

    const/4 v10, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/n;->b:Lcom/google/r/b/a/ru;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/gmm/z/b/m;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/m;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    move-object v3, v0

    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/z/d;

    iget-object v2, v3, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    iget-object v3, v3, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v7

    const/4 v5, 0x0

    if-eqz v7, :cond_0

    sget-object v8, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v8, v7}, Lcom/google/android/apps/gmm/map/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v5

    :cond_0
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v6

    iget-object v7, p3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {p3, v8}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/z/d;-><init>(Lcom/google/r/b/a/ru;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v0, v9, v10

    invoke-interface {p2, v9}, Lcom/google/android/apps/gmm/z/a/b;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    .line 39
    return-void

    :cond_1
    move-object v3, v0

    goto :goto_0
.end method
