.class public Lcom/google/android/apps/gmm/directions/i/af;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/k;
.implements Ljava/io/Serializable;


# instance fields
.field private final a:Lcom/google/maps/g/a/dd;

.field private final b:Lcom/google/android/libraries/curvular/aw;

.field private final c:Lcom/google/maps/g/a/dl;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lcom/google/maps/g/a/da;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/directions/i/af;-><init>(Lcom/google/maps/g/a/da;Z)V

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/google/maps/g/a/da;Z)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iget v0, p1, Lcom/google/maps/g/a/da;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/dd;->a(I)Lcom/google/maps/g/a/dd;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/dd;->c:Lcom/google/maps/g/a/dd;

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/af;->a:Lcom/google/maps/g/a/dd;

    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/af;->a:Lcom/google/maps/g/a/dd;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/r/a/n;->a(Lcom/google/maps/g/a/dd;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/af;->b:Lcom/google/android/libraries/curvular/aw;

    .line 31
    iget v0, p1, Lcom/google/maps/g/a/da;->c:I

    invoke-static {v0}, Lcom/google/maps/g/a/dl;->a(I)Lcom/google/maps/g/a/dl;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/dl;->a:Lcom/google/maps/g/a/dl;

    :cond_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/af;->c:Lcom/google/maps/g/a/dl;

    .line 32
    invoke-virtual {p1}, Lcom/google/maps/g/a/da;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/af;->d:Ljava/lang/String;

    .line 33
    invoke-virtual {p1}, Lcom/google/maps/g/a/da;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/af;->e:Ljava/lang/String;

    .line 34
    invoke-virtual {p1}, Lcom/google/maps/g/a/da;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/af;->f:Ljava/lang/String;

    .line 35
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/af;->g:Ljava/lang/Boolean;

    .line 36
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/g/a/dd;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/af;->a:Lcom/google/maps/g/a/dd;

    return-object v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 107
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/af;->g:Ljava/lang/Boolean;

    .line 108
    return-void
.end method

.method public final b()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/af;->b:Lcom/google/android/libraries/curvular/aw;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/af;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/af;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/af;->f:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 44
    instance-of v2, p1, Lcom/google/android/apps/gmm/directions/i/af;

    if-nez v2, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v0

    .line 47
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/directions/i/af;

    .line 48
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/af;->a:Lcom/google/maps/g/a/dd;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/i/af;->a:Lcom/google/maps/g/a/dd;

    .line 49
    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_9

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_2
    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/af;->b:Lcom/google/android/libraries/curvular/aw;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/i/af;->b:Lcom/google/android/libraries/curvular/aw;

    .line 50
    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_a

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_3
    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/af;->c:Lcom/google/maps/g/a/dl;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/i/af;->c:Lcom/google/maps/g/a/dl;

    .line 51
    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_b

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_4
    move v2, v1

    :goto_3
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/af;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/i/af;->d:Ljava/lang/String;

    .line 52
    if-eq v2, v3, :cond_5

    if-eqz v2, :cond_c

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    :cond_5
    move v2, v1

    :goto_4
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/af;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/i/af;->e:Ljava/lang/String;

    .line 53
    if-eq v2, v3, :cond_6

    if-eqz v2, :cond_d

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_6
    move v2, v1

    :goto_5
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/af;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/i/af;->f:Ljava/lang/String;

    .line 54
    if-eq v2, v3, :cond_7

    if-eqz v2, :cond_e

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_7
    move v2, v1

    :goto_6
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/af;->g:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/i/af;->g:Ljava/lang/Boolean;

    .line 55
    if-eq v2, v3, :cond_8

    if-eqz v2, :cond_f

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_8
    move v2, v1

    :goto_7
    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_9
    move v2, v0

    .line 49
    goto :goto_1

    :cond_a
    move v2, v0

    .line 50
    goto :goto_2

    :cond_b
    move v2, v0

    .line 51
    goto :goto_3

    :cond_c
    move v2, v0

    .line 52
    goto :goto_4

    :cond_d
    move v2, v0

    .line 53
    goto :goto_5

    :cond_e
    move v2, v0

    .line 54
    goto :goto_6

    :cond_f
    move v2, v0

    .line 55
    goto :goto_7
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 60
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/af;->a:Lcom/google/maps/g/a/dd;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/af;->b:Lcom/google/android/libraries/curvular/aw;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/af;->c:Lcom/google/maps/g/a/dl;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/af;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/af;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/af;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/af;->g:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
