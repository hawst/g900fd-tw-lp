.class public Lcom/google/android/apps/gmm/navigation/a/b/b;
.super Lcom/google/android/apps/gmm/navigation/a/b/a;
.source "PG"


# instance fields
.field public final a:[Lcom/google/android/apps/gmm/navigation/a/b/a;


# direct methods
.method public constructor <init>([Lcom/google/android/apps/gmm/navigation/a/b/a;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/a/b/a;-><init>()V

    .line 167
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/b/b;->a:[Lcom/google/android/apps/gmm/navigation/a/b/a;

    .line 168
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 176
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/b/b;->a:[Lcom/google/android/apps/gmm/navigation/a/b/a;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 177
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/navigation/a/b/a;->a()Z

    move-result v4

    if-nez v4, :cond_1

    .line 181
    :cond_0
    :goto_1
    return v0

    .line 176
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 181
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/b/b;->a:[Lcom/google/android/apps/gmm/navigation/a/b/a;

    array-length v1, v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 195
    instance-of v0, p1, Lcom/google/android/apps/gmm/navigation/a/b/b;

    if-eqz v0, :cond_0

    .line 196
    check-cast p1, Lcom/google/android/apps/gmm/navigation/a/b/b;

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/a/b/b;->a:[Lcom/google/android/apps/gmm/navigation/a/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/b/b;->a:[Lcom/google/android/apps/gmm/navigation/a/b/a;

    invoke-static {v0, v1}, Ljava/util/Arrays;->deepEquals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    .line 198
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/b/b;->a:[Lcom/google/android/apps/gmm/navigation/a/b/a;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/b/b;->a:[Lcom/google/android/apps/gmm/navigation/a/b/a;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 188
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/b/b;->a:[Lcom/google/android/apps/gmm/navigation/a/b/a;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 190
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
