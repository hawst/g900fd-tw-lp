.class public Lcom/google/android/apps/gmm/v/bz;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Key:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/apps/gmm/v/ad;

.field private final b:I

.field private final c:I

.field private final d:Lcom/google/android/apps/gmm/v/cb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/v/bz",
            "<TKey;>.com/google/android/apps/gmm/v/cb;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TKey;",
            "Lcom/google/android/apps/gmm/v/by;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/v/ca",
            "<TKey;>;>;"
        }
    .end annotation
.end field

.field private final g:I

.field private h:Lcom/google/android/apps/gmm/v/ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/v/ca",
            "<TKey;>;"
        }
    .end annotation
.end field

.field private i:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/ad;III)V
    .locals 4

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->e:Ljava/util/Map;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->f:Ljava/util/ArrayList;

    .line 133
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/bz;->a:Lcom/google/android/apps/gmm/v/ad;

    .line 134
    iput p2, p0, Lcom/google/android/apps/gmm/v/bz;->b:I

    .line 135
    iput p3, p0, Lcom/google/android/apps/gmm/v/bz;->c:I

    .line 136
    iput p4, p0, Lcom/google/android/apps/gmm/v/bz;->g:I

    .line 137
    new-instance v0, Lcom/google/android/apps/gmm/v/cb;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/v/cb;-><init>(Lcom/google/android/apps/gmm/v/bz;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->d:Lcom/google/android/apps/gmm/v/cb;

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->d:Lcom/google/android/apps/gmm/v/cb;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 139
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/bz;->e()V

    .line 140
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/v/ca;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/v/ca",
            "<TKey;>;)V"
        }
    .end annotation

    .prologue
    .line 155
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ca;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 156
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/ca;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 157
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/bz;->e:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/by;

    .line 158
    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/v/by;->j:Z

    .line 155
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 160
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/v/ca;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    monitor-exit p0

    return-void

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Lcom/google/android/apps/gmm/v/ca;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/v/ca",
            "<TKey;>;)V"
        }
    .end annotation

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/v/bz;->a(Lcom/google/android/apps/gmm/v/ca;)V

    .line 165
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/bw;->e:Lcom/google/android/apps/gmm/v/bg;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/v/bg;->a:Z

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/bw;->f:Lcom/google/android/apps/gmm/v/aa;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    monitor-exit p0

    return-void

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 173
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bz;->i:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->i:Landroid/graphics/Bitmap;

    .line 175
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/v/bz;->i:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 180
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bz;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/ca;

    .line 182
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/v/ca;->a(Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 183
    if-nez v0, :cond_0

    .line 180
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 195
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/v/bz;->b:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/bz;->c:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 204
    monitor-enter p0

    const/4 v1, 0x0

    .line 205
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v4

    :goto_0
    if-ge v5, v6, :cond_4

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/ca;

    .line 207
    iget v2, v0, Lcom/google/android/apps/gmm/v/bw;->j:I

    if-lez v2, :cond_1

    move v2, v3

    :goto_1
    if-nez v2, :cond_2

    .line 209
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/bz;->a(Lcom/google/android/apps/gmm/v/ca;)V

    .line 214
    :goto_2
    if-nez v0, :cond_0

    .line 216
    new-instance v0, Lcom/google/android/apps/gmm/v/ca;

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/bz;->a:Lcom/google/android/apps/gmm/v/ad;

    iget v3, p0, Lcom/google/android/apps/gmm/v/bz;->b:I

    iget v4, p0, Lcom/google/android/apps/gmm/v/bz;->c:I

    iget v5, p0, Lcom/google/android/apps/gmm/v/bz;->g:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/v/ca;-><init>(Lcom/google/android/apps/gmm/v/bz;Lcom/google/android/apps/gmm/v/ad;III)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bz;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/bw;->f:Lcom/google/android/apps/gmm/v/aa;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    const/4 v4, 0x1

    invoke-direct {v3, v2, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 217
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bz;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 220
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/bz;->d()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 221
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    :try_start_1
    iget-object v2, v0, Lcom/google/android/apps/gmm/v/bw;->h:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->acquire()V

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/v/bw;->l:Z

    iput-object v1, v0, Lcom/google/android/apps/gmm/v/bw;->i:Landroid/graphics/Bitmap;

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/bw;->g:Landroid/graphics/Canvas;

    invoke-virtual {v2, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/v/bw;->l:Z

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    move-exception v1

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 223
    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->h:Lcom/google/android/apps/gmm/v/ca;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 224
    monitor-exit p0

    return-void

    :cond_1
    move v2, v4

    .line 207
    goto :goto_1

    .line 205
    :cond_2
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 222
    :cond_3
    const/4 v1, 0x0

    :try_start_3
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/v/bw;->l:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/bw;->h:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/Object;)Lcom/google/android/apps/gmm/v/by;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)",
            "Lcom/google/android/apps/gmm/v/by;"
        }
    .end annotation

    .prologue
    .line 232
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/by;

    .line 233
    if-eqz v0, :cond_0

    .line 234
    iget-object v1, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget v2, v1, Lcom/google/android/apps/gmm/v/bw;->j:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/android/apps/gmm/v/bw;->j:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    :cond_0
    monitor-exit p0

    return-object v0

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/Object;IILcom/google/android/apps/gmm/v/cc;)Lcom/google/android/apps/gmm/v/by;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;II",
            "Lcom/google/android/apps/gmm/v/cc;",
            ")",
            "Lcom/google/android/apps/gmm/v/by;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 250
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/v/bz;->a(Ljava/lang/Object;)Lcom/google/android/apps/gmm/v/by;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 251
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/v/bz;->b:I

    if-gt p2, v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/gmm/v/bz;->c:I

    if-le p3, v0, :cond_4

    .line 252
    :cond_2
    const/4 v0, 0x0

    .line 279
    :cond_3
    :goto_1
    return-object v0

    .line 256
    :cond_4
    monitor-enter p0

    .line 257
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->h:Lcom/google/android/apps/gmm/v/ca;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/v/ca;->a(Ljava/lang/Object;II)Lcom/google/android/apps/gmm/v/by;

    move-result-object v0

    .line 258
    if-nez v0, :cond_5

    .line 259
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/bz;->e()V

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->h:Lcom/google/android/apps/gmm/v/ca;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/v/ca;->a(Ljava/lang/Object;II)Lcom/google/android/apps/gmm/v/by;

    move-result-object v0

    .line 269
    :cond_5
    if-nez v0, :cond_6

    .line 270
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Failed to allocate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 273
    :cond_6
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bz;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 278
    iget-object v1, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    :try_start_2
    iget-object v2, v1, Lcom/google/android/apps/gmm/v/bw;->h:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->acquire()V

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/v/bw;->l:Z

    iget-boolean v2, v1, Lcom/google/android/apps/gmm/v/bw;->l:Z

    if-nez v2, :cond_7

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    :cond_7
    :try_start_3
    iget-object v2, v1, Lcom/google/android/apps/gmm/v/bw;->i:Landroid/graphics/Bitmap;

    if-nez v2, :cond_8

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_8
    iget-object v2, v1, Lcom/google/android/apps/gmm/v/bw;->g:Landroid/graphics/Canvas;

    invoke-interface {p4, v0, v2}, Lcom/google/android/apps/gmm/v/cc;->a(Lcom/google/android/apps/gmm/v/by;Landroid/graphics/Canvas;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/bw;->l:Z

    if-nez v3, :cond_9

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_9
    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/v/bw;->k:Z

    :cond_a
    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/bw;->l:Z

    if-nez v3, :cond_b

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_b
    const/4 v3, 0x0

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/v/bw;->l:Z

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/bw;->h:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bz;->d:Lcom/google/android/apps/gmm/v/cb;

    iget-object v2, v1, Lcom/google/android/apps/gmm/v/cb;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/google/android/apps/gmm/v/cb;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v3, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1
.end method

.method public final declared-synchronized a()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 306
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 310
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 311
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/ca;

    .line 312
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bz;->h:Lcom/google/android/apps/gmm/v/ca;

    if-eq v0, v1, :cond_0

    .line 313
    iget v1, v0, Lcom/google/android/apps/gmm/v/bw;->j:I

    if-lez v1, :cond_2

    move v1, v2

    :goto_1
    if-nez v1, :cond_1

    .line 317
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/bz;->b(Lcom/google/android/apps/gmm/v/ca;)V

    .line 318
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 319
    :cond_1
    iget v1, v0, Lcom/google/android/apps/gmm/v/bw;->j:I

    if-lez v1, :cond_3

    move v1, v2

    :goto_2
    if-nez v1, :cond_4

    move v1, v2

    .line 325
    :goto_3
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/ca;->a(Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 326
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bz;->i:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 327
    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->i:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 306
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v1, v3

    .line 313
    goto :goto_1

    :cond_3
    move v1, v3

    .line 319
    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_3

    .line 334
    :cond_5
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/v/by;)V
    .locals 2

    .prologue
    .line 355
    monitor-enter p0

    :try_start_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget v0, v1, Lcom/google/android/apps/gmm/v/bw;->j:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    iget v0, v1, Lcom/google/android/apps/gmm/v/bw;->j:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v1, Lcom/google/android/apps/gmm/v/bw;->j:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 356
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b()V
    .locals 3

    .prologue
    .line 340
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bz;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/ca;

    .line 342
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/bz;->b(Lcom/google/android/apps/gmm/v/ca;)V

    .line 340
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 352
    monitor-exit p0

    return-void

    .line 340
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized c()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 359
    monitor-enter p0

    move v4, v2

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_8

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/bw;

    .line 361
    iget v3, v0, Lcom/google/android/apps/gmm/v/bw;->j:I

    if-lez v3, :cond_1

    move v3, v1

    :goto_1
    if-eqz v3, :cond_0

    .line 362
    iget-boolean v3, v0, Lcom/google/android/apps/gmm/v/bw;->k:Z

    if-nez v3, :cond_2

    move v0, v1

    :goto_2
    if-nez v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bz;->d:Lcom/google/android/apps/gmm/v/cb;

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/cb;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/cb;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v5, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v3, v0, v5}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 359
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_1
    move v3, v2

    .line 361
    goto :goto_1

    .line 362
    :cond_2
    iget-object v3, v0, Lcom/google/android/apps/gmm/v/bw;->h:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/v/bw;->l:Z

    move v3, v1

    :goto_3
    if-eqz v3, :cond_7

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/v/bw;->k:Z

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/bw;->e:Lcom/google/android/apps/gmm/v/bg;

    iget-object v5, v0, Lcom/google/android/apps/gmm/v/bw;->d:Lcom/google/android/apps/gmm/v/ad;

    iget-boolean v6, v3, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v6, :cond_3

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_3
    iget-boolean v6, v3, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v6, :cond_4

    iget-object v6, v5, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    iget-object v7, v3, Lcom/google/android/apps/gmm/v/ci;->g:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    iget v8, v3, Lcom/google/android/apps/gmm/v/ci;->t:I

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/gmm/v/aq;->a(II)V

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/ci;->f:Lcom/google/android/apps/gmm/v/ar;

    sget-object v6, Lcom/google/android/apps/gmm/v/ab;->c:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v3, v5, v6}, Lcom/google/android/apps/gmm/v/ar;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    :cond_4
    iget-boolean v3, v0, Lcom/google/android/apps/gmm/v/bw;->l:Z

    if-nez v3, :cond_6

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 359
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    move v3, v2

    .line 362
    goto :goto_3

    :cond_6
    const/4 v3, 0x0

    :try_start_1
    iput-boolean v3, v0, Lcom/google/android/apps/gmm/v/bw;->l:Z

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/bw;->h:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_2

    .line 367
    :cond_8
    monitor-exit p0

    return-void
.end method
