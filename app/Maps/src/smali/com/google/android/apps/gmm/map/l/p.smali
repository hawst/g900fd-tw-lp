.class public Lcom/google/android/apps/gmm/map/l/p;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final f:Lcom/google/android/apps/gmm/map/b/a/y;

.field private static final g:Lcom/google/android/apps/gmm/map/b/a/y;

.field private static final h:[Lcom/google/android/apps/gmm/v/cn;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/v/cn;

.field public final b:Lcom/google/android/apps/gmm/v/cn;

.field public final c:[F

.field public d:[F

.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/l/p;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/l/p;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 86
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/cn;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/v/cn;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/v/cn;-><init>()V

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/l/p;->h:[Lcom/google/android/apps/gmm/v/cn;

    .line 94
    return-void
.end method

.method public constructor <init>([FLcom/google/android/apps/gmm/v/cn;Lcom/google/android/apps/gmm/v/cn;)V
    .locals 1

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    .line 189
    array-length v0, p1

    div-int/lit8 v0, v0, 0x9

    iput v0, p0, Lcom/google/android/apps/gmm/map/l/p;->e:I

    .line 190
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/l/p;->a:Lcom/google/android/apps/gmm/v/cn;

    .line 191
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/l/p;->b:Lcom/google/android/apps/gmm/v/cn;

    .line 193
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/l/p;->a()V

    .line 194
    return-void
.end method

.method private a()V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v1, 0x0

    .line 244
    iget v0, p0, Lcom/google/android/apps/gmm/map/l/p;->e:I

    mul-int/lit8 v0, v0, 0x3

    .line 245
    mul-int/lit8 v4, v0, 0x3

    .line 247
    new-array v0, v4, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/p;->d:[F

    move v2, v1

    move v0, v1

    .line 253
    :goto_0
    if-ge v2, v4, :cond_1

    .line 255
    add-int/lit8 v5, v2, 0x3

    .line 257
    add-int/lit8 v6, v5, 0x3

    .line 259
    add-int/lit8 v3, v6, 0x3

    .line 261
    sget-object v7, Lcom/google/android/apps/gmm/map/l/p;->h:[Lcom/google/android/apps/gmm/v/cn;

    aget-object v7, v7, v1

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    aget v8, v8, v6

    iget-object v9, p0, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    aget v9, v9, v5

    sub-float/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    add-int/lit8 v10, v6, 0x1

    aget v9, v9, v10

    iget-object v10, p0, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    add-int/lit8 v11, v5, 0x1

    aget v10, v10, v11

    sub-float/2addr v9, v10

    iget-object v10, p0, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    add-int/lit8 v6, v6, 0x2

    aget v6, v10, v6

    iget-object v10, p0, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    add-int/lit8 v11, v5, 0x2

    aget v10, v10, v11

    sub-float/2addr v6, v10

    iget-object v10, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aput v8, v10, v1

    iget-object v8, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aput v9, v8, v12

    iget-object v7, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aput v6, v7, v13

    .line 266
    sget-object v6, Lcom/google/android/apps/gmm/map/l/p;->h:[Lcom/google/android/apps/gmm/v/cn;

    aget-object v6, v6, v12

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    aget v7, v7, v2

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    aget v8, v8, v5

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    add-int/lit8 v9, v2, 0x1

    aget v8, v8, v9

    iget-object v9, p0, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    add-int/lit8 v10, v5, 0x1

    aget v9, v9, v10

    sub-float/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    add-int/lit8 v2, v2, 0x2

    aget v2, v9, v2

    iget-object v9, p0, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    add-int/lit8 v5, v5, 0x2

    aget v5, v9, v5

    sub-float/2addr v2, v5

    iget-object v5, v6, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aput v7, v5, v1

    iget-object v5, v6, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aput v8, v5, v12

    iget-object v5, v6, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aput v2, v5, v13

    .line 271
    sget-object v2, Lcom/google/android/apps/gmm/map/l/p;->h:[Lcom/google/android/apps/gmm/v/cn;

    aget-object v2, v2, v1

    sget-object v5, Lcom/google/android/apps/gmm/map/l/p;->h:[Lcom/google/android/apps/gmm/v/cn;

    aget-object v5, v5, v12

    iget-object v6, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v6, v6, v12

    iget-object v7, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v7, v7, v13

    mul-float/2addr v6, v7

    iget-object v7, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v7, v7, v13

    iget-object v8, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v8, v8, v12

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    iget-object v7, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v7, v7, v13

    iget-object v8, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v8, v8, v1

    mul-float/2addr v7, v8

    iget-object v8, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v8, v8, v1

    iget-object v9, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v9, v9, v13

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    iget-object v8, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v8, v8, v1

    iget-object v9, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v9, v9, v12

    mul-float/2addr v8, v9

    iget-object v9, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v9, v9, v12

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v5, v5, v1

    mul-float/2addr v5, v9

    sub-float v5, v8, v5

    iget-object v8, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aput v6, v8, v1

    iget-object v6, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aput v7, v6, v12

    iget-object v6, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aput v5, v6, v13

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/cn;->a()Lcom/google/android/apps/gmm/v/cn;

    move v2, v0

    move v0, v1

    .line 274
    :goto_1
    const/4 v5, 0x3

    if-ge v0, v5, :cond_0

    .line 275
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/l/p;->d:[F

    add-int/lit8 v6, v2, 0x1

    sget-object v7, Lcom/google/android/apps/gmm/map/l/p;->h:[Lcom/google/android/apps/gmm/v/cn;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v7, v7, v1

    aput v7, v5, v2

    .line 276
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/l/p;->d:[F

    add-int/lit8 v5, v6, 0x1

    sget-object v7, Lcom/google/android/apps/gmm/map/l/p;->h:[Lcom/google/android/apps/gmm/v/cn;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v7, v7, v12

    aput v7, v2, v6

    .line 277
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/l/p;->d:[F

    add-int/lit8 v2, v5, 0x1

    sget-object v7, Lcom/google/android/apps/gmm/map/l/p;->h:[Lcom/google/android/apps/gmm/v/cn;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v7, v7, v13

    aput v7, v6, v5

    .line 274
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    move v2, v3

    goto/16 :goto_0

    .line 280
    :cond_1
    return-void
.end method

.method public static a(Ljava/io/DataInput;Ljava/util/List;Ljava/util/List;)Z
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInput;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/l/p;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/l/a;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 312
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    .line 313
    const/4 v2, 0x3

    if-ge v4, v2, :cond_0

    .line 314
    const-string v2, "TriangleMeshUnpacking"

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v5, 0x31

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Malformed TriangleStripMesh, "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " vertices"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v5}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 318
    :cond_0
    mul-int/lit8 v2, v4, 0x3

    new-array v8, v2, [F

    .line 320
    sget-object v2, Lcom/google/android/apps/gmm/map/l/p;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v5, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v6, v2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 322
    const/4 v3, 0x0

    .line 325
    new-instance v9, Lcom/google/android/apps/gmm/v/cn;

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    const v5, 0x7f7fffff    # Float.MAX_VALUE

    const v6, 0x7f7fffff    # Float.MAX_VALUE

    invoke-direct {v9, v2, v5, v6}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    .line 327
    new-instance v10, Lcom/google/android/apps/gmm/v/cn;

    const v2, -0x800001

    const v5, -0x800001

    const v6, -0x800001

    invoke-direct {v10, v2, v5, v6}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    .line 329
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    .line 330
    sget-object v5, Lcom/google/android/apps/gmm/map/l/p;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->b(Ljava/io/DataInput;)I

    move-result v6

    iput v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 331
    sget-object v5, Lcom/google/android/apps/gmm/map/l/p;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->b(Ljava/io/DataInput;)I

    move-result v6

    iput v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 332
    sget-object v5, Lcom/google/android/apps/gmm/map/l/p;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->b(Ljava/io/DataInput;)I

    move-result v6

    iput v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 334
    sget-object v5, Lcom/google/android/apps/gmm/map/l/p;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    sget-object v6, Lcom/google/android/apps/gmm/map/l/p;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->f(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    .line 336
    sget-object v5, Lcom/google/android/apps/gmm/map/l/p;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v5, v5

    sget-object v6, Lcom/google/android/apps/gmm/map/l/p;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v6, v6

    sget-object v7, Lcom/google/android/apps/gmm/map/l/p;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v7, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v7, v7

    invoke-virtual {v9, v5, v6, v7}, Lcom/google/android/apps/gmm/v/cn;->a(FFF)Lcom/google/android/apps/gmm/v/cn;

    .line 337
    sget-object v5, Lcom/google/android/apps/gmm/map/l/p;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v5, v5

    sget-object v6, Lcom/google/android/apps/gmm/map/l/p;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v6, v6

    sget-object v7, Lcom/google/android/apps/gmm/map/l/p;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v7, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v7, v7

    invoke-virtual {v10, v5, v6, v7}, Lcom/google/android/apps/gmm/v/cn;->b(FFF)Lcom/google/android/apps/gmm/v/cn;

    .line 339
    add-int/lit8 v5, v3, 0x1

    sget-object v6, Lcom/google/android/apps/gmm/map/l/p;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v6, v6

    aput v6, v8, v3

    .line 340
    add-int/lit8 v6, v5, 0x1

    sget-object v3, Lcom/google/android/apps/gmm/map/l/p;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v3, v3

    aput v3, v8, v5

    .line 341
    add-int/lit8 v3, v6, 0x1

    sget-object v5, Lcom/google/android/apps/gmm/map/l/p;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v5, v5

    aput v5, v8, v6

    .line 329
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 344
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v6

    .line 345
    if-gtz v6, :cond_2

    .line 346
    const-string v2, "TriangleMeshUnpacking"

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x2f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Malformed TriangleStripMesh, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " strips"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    :cond_2
    const/4 v3, 0x0

    .line 352
    const/4 v2, 0x0

    .line 354
    if-lez v6, :cond_5

    .line 356
    new-array v3, v6, [[I

    .line 358
    const/4 v4, 0x0

    :goto_1
    if-ge v4, v6, :cond_5

    .line 359
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v7

    .line 360
    add-int/lit8 v5, v7, -0x2

    add-int/2addr v5, v2

    .line 361
    if-lez v7, :cond_4

    .line 362
    const/4 v2, 0x3

    if-ge v7, v2, :cond_3

    .line 363
    const-string v2, "TriangleMeshUnpacking"

    new-instance v11, Ljava/lang/StringBuilder;

    const/16 v12, 0x46

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "Malformed TriangleStripMesh, "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " indices for strip "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v2, v11, v12}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 366
    :cond_3
    new-array v2, v7, [I

    aput-object v2, v3, v4

    .line 367
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v7, :cond_4

    .line 368
    aget-object v11, v3, v4

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v12

    aput v12, v11, v2

    .line 367
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 358
    :cond_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v5

    goto :goto_1

    .line 374
    :cond_5
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v11

    .line 376
    if-lez v11, :cond_6

    rem-int/lit8 v4, v11, 0x2

    const/4 v5, 0x1

    if-ne v4, v5, :cond_7

    .line 377
    :cond_6
    const-string v4, "TriangleMeshUnpacking"

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x2e

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Malformed TriangleStripMesh, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " edges"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 381
    :cond_7
    const/4 v4, 0x0

    .line 383
    if-lez v11, :cond_8

    .line 384
    const/4 v6, 0x0

    .line 385
    new-array v4, v11, [I

    .line 386
    const/4 v5, 0x0

    :goto_3
    if-ge v5, v11, :cond_8

    .line 387
    add-int/lit8 v7, v6, 0x1

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v12

    aput v12, v4, v6

    .line 386
    add-int/lit8 v5, v5, 0x1

    move v6, v7

    goto :goto_3

    .line 391
    :cond_8
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v6

    .line 393
    if-lez v6, :cond_9

    .line 395
    mul-int/lit8 v5, v6, 0x3

    new-array v7, v5, [F

    .line 396
    const/4 v5, 0x0

    :goto_4
    if-ge v5, v6, :cond_9

    .line 398
    const/4 v11, 0x0

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v12

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v13

    const v14, -0x3fb6f025

    const v15, 0x3cc9d9b5

    int-to-float v12, v12

    mul-float/2addr v12, v15

    add-float/2addr v12, v14

    const v14, 0x40490fdb    # (float)Math.PI

    int-to-float v13, v13

    mul-float/2addr v13, v14

    float-to-double v14, v12

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    double-to-float v14, v14

    float-to-double v0, v12

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v12, v0

    float-to-double v0, v13

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v15, v0

    float-to-double v0, v13

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v13, v0

    const/16 v16, 0x1

    mul-float/2addr v14, v13

    aput v14, v7, v11

    const/4 v11, 0x2

    mul-float/2addr v12, v13

    aput v12, v7, v16

    aput v15, v7, v11

    .line 397
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 402
    :cond_9
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v6

    .line 404
    if-lez v6, :cond_a

    .line 406
    new-array v7, v6, [I

    .line 407
    const/4 v5, 0x0

    :goto_5
    if-ge v5, v6, :cond_a

    .line 408
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v11

    aput v11, v7, v5

    .line 407
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 412
    :cond_a
    if-eqz v3, :cond_e

    .line 413
    mul-int/lit8 v2, v2, 0x9

    new-array v11, v2, [F

    .line 415
    const/4 v5, 0x0

    .line 417
    const/4 v2, 0x0

    :goto_6
    array-length v6, v3

    if-ge v2, v6, :cond_c

    .line 418
    const/4 v6, 0x0

    :goto_7
    aget-object v7, v3, v2

    array-length v7, v7

    add-int/lit8 v7, v7, -0x2

    if-ge v6, v7, :cond_b

    .line 419
    rem-int/lit8 v7, v6, 0x2

    .line 420
    aget-object v12, v3, v2

    add-int/lit8 v13, v7, 0x1

    mul-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v6

    aget v7, v12, v7

    mul-int/lit8 v7, v7, 0x3

    .line 421
    aget-object v12, v3, v2

    add-int/lit8 v14, v6, 0x1

    aget v12, v12, v14

    mul-int/lit8 v12, v12, 0x3

    .line 422
    aget-object v14, v3, v2

    rem-int/lit8 v13, v13, 0x2

    mul-int/lit8 v13, v13, 0x2

    add-int/2addr v13, v6

    aget v13, v14, v13

    mul-int/lit8 v13, v13, 0x3

    .line 423
    add-int/lit8 v14, v5, 0x1

    add-int/lit8 v15, v7, 0x1

    aget v7, v8, v7

    aput v7, v11, v5

    .line 424
    add-int/lit8 v5, v14, 0x1

    add-int/lit8 v7, v15, 0x1

    aget v15, v8, v15

    aput v15, v11, v14

    .line 425
    add-int/lit8 v14, v5, 0x1

    aget v7, v8, v7

    aput v7, v11, v5

    .line 427
    add-int/lit8 v5, v14, 0x1

    add-int/lit8 v7, v12, 0x1

    aget v12, v8, v12

    aput v12, v11, v14

    .line 428
    add-int/lit8 v12, v5, 0x1

    add-int/lit8 v14, v7, 0x1

    aget v7, v8, v7

    aput v7, v11, v5

    .line 429
    add-int/lit8 v5, v12, 0x1

    aget v7, v8, v14

    aput v7, v11, v12

    .line 431
    add-int/lit8 v7, v5, 0x1

    add-int/lit8 v12, v13, 0x1

    aget v13, v8, v13

    aput v13, v11, v5

    .line 432
    add-int/lit8 v5, v7, 0x1

    add-int/lit8 v13, v12, 0x1

    aget v12, v8, v12

    aput v12, v11, v7

    .line 433
    add-int/lit8 v7, v5, 0x1

    aget v12, v8, v13

    aput v12, v11, v5

    .line 418
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v7

    goto :goto_7

    .line 417
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 437
    :cond_c
    new-instance v2, Lcom/google/android/apps/gmm/map/l/p;

    invoke-direct {v2, v11, v9, v10}, Lcom/google/android/apps/gmm/map/l/p;-><init>([FLcom/google/android/apps/gmm/v/cn;Lcom/google/android/apps/gmm/v/cn;)V

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 440
    if-eqz v4, :cond_d

    .line 441
    new-instance v2, Lcom/google/android/apps/gmm/map/l/a;

    new-instance v3, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v3, v9}, Lcom/google/android/apps/gmm/v/cn;-><init>(Lcom/google/android/apps/gmm/v/cn;)V

    new-instance v5, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v5, v10}, Lcom/google/android/apps/gmm/v/cn;-><init>(Lcom/google/android/apps/gmm/v/cn;)V

    invoke-direct {v2, v8, v4, v3, v5}, Lcom/google/android/apps/gmm/map/l/a;-><init>([F[ILcom/google/android/apps/gmm/v/cn;Lcom/google/android/apps/gmm/v/cn;)V

    .line 443
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 446
    :cond_d
    const/4 v2, 0x1

    .line 449
    :goto_8
    return v2

    :cond_e
    const/4 v2, 0x0

    goto :goto_8
.end method
