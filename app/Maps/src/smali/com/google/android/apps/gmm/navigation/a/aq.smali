.class public Lcom/google/android/apps/gmm/navigation/a/aq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/a/aw;


# static fields
.field static final a:Ljava/lang/String;

.field private static final j:J


# instance fields
.field final b:Lcom/google/android/apps/gmm/navigation/a/d;

.field final c:Lcom/google/android/apps/gmm/map/util/b/g;

.field final d:Lcom/google/android/apps/gmm/navigation/a/b/h;

.field final e:Lcom/google/android/apps/gmm/navigation/a/i;

.field f:Lcom/google/android/apps/gmm/navigation/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final g:Lcom/google/android/apps/gmm/navigation/a/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final h:Z

.field i:Ljava/lang/Runnable;

.field private final k:Lcom/google/android/apps/gmm/shared/c/a/j;

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/gmm/navigation/a/aq;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/aq;->a:Ljava/lang/String;

    .line 36
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/navigation/a/aq;->j:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->k:Lcom/google/android/apps/gmm/shared/c/a/j;

    .line 60
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    .line 61
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 62
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    .line 63
    iput-object p5, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->g:Lcom/google/android/apps/gmm/navigation/a/f;

    .line 64
    iget-object v0, p2, Lcom/google/android/apps/gmm/navigation/a/d;->e:Lcom/google/android/apps/gmm/navigation/a/i;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->e:Lcom/google/android/apps/gmm/navigation/a/i;

    .line 65
    monitor-enter p2

    .line 66
    :try_start_0
    iput-boolean p6, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->h:Z

    .line 69
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/navigation/a/d;->n:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->l:Z

    .line 70
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/navigation/a/d;->o:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->m:Z

    .line 71
    iget-object v0, p2, Lcom/google/android/apps/gmm/navigation/a/d;->c:Landroid/content/Context;

    const-string v2, "vibrator"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->n:Z

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->o:Z

    .line 73
    monitor-exit p2

    return-void

    .line 71
    :cond_0
    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    goto :goto_0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method a()V
    .locals 6

    .prologue
    .line 130
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    monitor-enter v1

    .line 131
    :try_start_0
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/ar;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/a/ar;-><init>(Lcom/google/android/apps/gmm/navigation/a/aq;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->i:Ljava/lang/Runnable;

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->k:Lcom/google/android/apps/gmm/shared/c/a/j;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->i:Ljava/lang/Runnable;

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->ALERT_CONTROLLER:Lcom/google/android/apps/gmm/shared/c/a/p;

    sget-wide v4, Lcom/google/android/apps/gmm/navigation/a/aq;->j:J

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 144
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final a(Z)Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->f:Lcom/google/android/apps/gmm/navigation/a/a;

    if-eqz v0, :cond_0

    move v0, v2

    .line 274
    :goto_0
    return v0

    .line 208
    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->i:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    move v0, v3

    .line 209
    goto :goto_0

    .line 212
    :cond_1
    new-instance v4, Lcom/google/android/apps/gmm/navigation/a/ac;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/navigation/a/ac;-><init>()V

    .line 214
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->l:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->n:Z

    if-eqz v0, :cond_2

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/d;->j:Lcom/google/android/apps/gmm/navigation/a/g;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/navigation/a/g;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;)Lcom/google/android/apps/gmm/navigation/a/a;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v5, v4, Lcom/google/android/apps/gmm/navigation/a/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->m:Z

    if-eqz v0, :cond_b

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/d;->k:Lcom/google/android/apps/gmm/navigation/a/bw;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/navigation/a/bw;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;)Lcom/google/android/apps/gmm/navigation/a/a;

    move-result-object v0

    .line 229
    :goto_1
    if-eqz v0, :cond_3

    iget-object v5, v4, Lcom/google/android/apps/gmm/navigation/a/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/d;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->h:Z

    if-eqz v0, :cond_a

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    .line 232
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/d;->c:Landroid/content/Context;

    const-string v5, "audio"

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v5, 0x3

    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-nez v0, :cond_c

    move v0, v2

    :goto_2
    if-nez v0, :cond_a

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/a/d;->l:Lcom/google/android/apps/gmm/navigation/a/av;

    .line 236
    if-nez v5, :cond_5

    .line 237
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/aq;->a:Ljava/lang/String;

    .line 240
    :cond_5
    if-eqz v5, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->y()Lcom/google/r/b/a/amh;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/r/b/a/amh;->b:Z

    if-eqz v0, :cond_d

    move v0, v2

    :goto_3
    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->h:Z

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/d;->q:Ljava/util/Set;

    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    iget-object v6, v6, Lcom/google/android/apps/gmm/navigation/a/b/h;->c:Lcom/google/android/apps/gmm/navigation/a/b/j;

    invoke-interface {v0, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    :cond_6
    move v0, v2

    :goto_4
    if-eqz v0, :cond_13

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    invoke-interface {v5, v0}, Lcom/google/android/apps/gmm/navigation/a/av;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;)Lcom/google/android/apps/gmm/navigation/a/a;

    move-result-object v0

    .line 243
    if-nez v0, :cond_10

    .line 244
    if-eqz p1, :cond_f

    .line 245
    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/a/d;->d:Lcom/google/android/apps/gmm/navigation/logging/m;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/navigation/logging/m;->b()V

    .line 259
    :goto_5
    if-nez v0, :cond_7

    .line 261
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/aq;->a:Ljava/lang/String;

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/d;->h:Lcom/google/android/apps/gmm/navigation/a/av;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/navigation/a/av;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;)Lcom/google/android/apps/gmm/navigation/a/a;

    move-result-object v0

    .line 267
    :cond_7
    if-eqz v0, :cond_8

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->o:Z

    if-eqz v5, :cond_9

    .line 268
    :cond_8
    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/a/d;->i:Lcom/google/android/apps/gmm/navigation/a/g;

    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    invoke-interface {v5, v6}, Lcom/google/android/apps/gmm/navigation/a/g;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;)Lcom/google/android/apps/gmm/navigation/a/a;

    move-result-object v5

    if-eqz v5, :cond_9

    iget-object v6, v4, Lcom/google/android/apps/gmm/navigation/a/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    :cond_9
    if-eqz v0, :cond_a

    iget-object v5, v4, Lcom/google/android/apps/gmm/navigation/a/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    :cond_a
    iget-object v0, v4, Lcom/google/android/apps/gmm/navigation/a/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    move-object v0, v1

    :goto_6
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->f:Lcom/google/android/apps/gmm/navigation/a/a;

    move v0, v2

    .line 274
    goto/16 :goto_0

    .line 227
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/a/d;->k:Lcom/google/android/apps/gmm/navigation/a/bw;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/bv;

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/a/bw;->d:Lcom/google/android/gms/common/api/o;

    new-array v6, v2, [J

    const-wide/16 v8, 0x0

    aput-wide v8, v6, v3

    invoke-direct {v0, v5, v6}, Lcom/google/android/apps/gmm/navigation/a/bv;-><init>(Lcom/google/android/gms/common/api/o;[J)V

    goto/16 :goto_1

    :cond_c
    move v0, v3

    .line 232
    goto/16 :goto_2

    :cond_d
    move v0, v3

    .line 240
    goto :goto_3

    :cond_e
    move v0, v3

    goto :goto_4

    .line 249
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    sget-object v1, Lcom/google/android/apps/gmm/aa/d/s;->c:Lcom/google/android/apps/gmm/aa/d/s;

    invoke-interface {v5, v0, p0, v1}, Lcom/google/android/apps/gmm/navigation/a/av;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/aw;Lcom/google/android/apps/gmm/aa/d/s;)V

    .line 251
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/aq;->a:Ljava/lang/String;

    move v0, v3

    .line 252
    goto/16 :goto_0

    .line 255
    :cond_10
    sget-object v5, Lcom/google/android/apps/gmm/navigation/a/aq;->a:Ljava/lang/String;

    goto :goto_5

    .line 273
    :cond_11
    iget-object v0, v4, Lcom/google/android/apps/gmm/navigation/a/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v2, :cond_12

    iget-object v0, v4, Lcom/google/android/apps/gmm/navigation/a/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/a;

    goto :goto_6

    :cond_12
    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/ab;

    iget-object v0, v4, Lcom/google/android/apps/gmm/navigation/a/ac;->a:Ljava/util/ArrayList;

    iget-object v3, v4, Lcom/google/android/apps/gmm/navigation/a/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Lcom/google/android/apps/gmm/navigation/a/a;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/navigation/a/a;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/navigation/a/ab;-><init>([Lcom/google/android/apps/gmm/navigation/a/a;)V

    move-object v0, v1

    goto :goto_6

    :cond_13
    move-object v0, v1

    goto/16 :goto_5
.end method

.method final b()V
    .locals 2

    .prologue
    .line 151
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    monitor-enter v1

    .line 152
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->f:Lcom/google/android/apps/gmm/navigation/a/a;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->f:Lcom/google/android/apps/gmm/navigation/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/a;->b()V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->e:Lcom/google/android/apps/gmm/navigation/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/i;->b()V

    .line 156
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final c()V
    .locals 2

    .prologue
    .line 163
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    monitor-enter v1

    .line 164
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->f:Lcom/google/android/apps/gmm/navigation/a/a;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->f:Lcom/google/android/apps/gmm/navigation/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->e:Lcom/google/android/apps/gmm/navigation/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/i;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->f:Lcom/google/android/apps/gmm/navigation/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/a;->c()V

    .line 169
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method d()V
    .locals 3

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->f:Lcom/google/android/apps/gmm/navigation/a/a;

    if-nez v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/d;->b()V

    .line 327
    :goto_0
    return-void

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->f:Lcom/google/android/apps/gmm/navigation/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/a;->a()V

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->k:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/as;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/navigation/a/as;-><init>(Lcom/google/android/apps/gmm/navigation/a/aq;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/aq;->k:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/au;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/navigation/a/au;-><init>(Lcom/google/android/apps/gmm/navigation/a/aq;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->ALERT_CONTROLLER:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 341
    return-void
.end method
