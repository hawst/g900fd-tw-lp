.class public final enum Lcom/google/android/apps/gmm/feedback/a/d;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/feedback/a/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/feedback/a/d;

.field public static final enum b:Lcom/google/android/apps/gmm/feedback/a/d;

.field public static final enum c:Lcom/google/android/apps/gmm/feedback/a/d;

.field public static final enum d:Lcom/google/android/apps/gmm/feedback/a/d;

.field public static final enum e:Lcom/google/android/apps/gmm/feedback/a/d;

.field public static final enum f:Lcom/google/android/apps/gmm/feedback/a/d;

.field public static final enum g:Lcom/google/android/apps/gmm/feedback/a/d;

.field public static final enum h:Lcom/google/android/apps/gmm/feedback/a/d;

.field public static final enum i:Lcom/google/android/apps/gmm/feedback/a/d;

.field public static final enum j:Lcom/google/android/apps/gmm/feedback/a/d;

.field public static final enum k:Lcom/google/android/apps/gmm/feedback/a/d;

.field public static final enum l:Lcom/google/android/apps/gmm/feedback/a/d;

.field public static final enum m:Lcom/google/android/apps/gmm/feedback/a/d;

.field public static final enum n:Lcom/google/android/apps/gmm/feedback/a/d;

.field public static final enum o:Lcom/google/android/apps/gmm/feedback/a/d;

.field private static final synthetic p:[Lcom/google/android/apps/gmm/feedback/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 13
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "MAP"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->a:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 14
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "GEOCODE_PAGE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->b:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 15
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "GEOCODE_PAGE_FULLSCREEN"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->c:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 16
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "BUSINESS_PLACE_PAGE"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->d:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 17
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "BUSINESS_PLACE_PAGE_FULLSCREEN"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->e:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 18
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "STREETVIEW"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "DIRECTION_PAGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->g:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "SUGGEST_PAGE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->h:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 21
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "SETTINGS_MENU"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->i:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "DRAWER_MENU"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->j:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 23
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "PLACE_PICKER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->k:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "PLACE_PICKER_SUGGEST"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->l:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 25
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "RMI_FEATURE_PICKER"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->m:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "LOCATION_QUALITY_FEEDBACK"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->n:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 27
    new-instance v0, Lcom/google/android/apps/gmm/feedback/a/d;

    const-string v1, "CARD_UI_ACTION"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->o:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 12
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/google/android/apps/gmm/feedback/a/d;

    sget-object v1, Lcom/google/android/apps/gmm/feedback/a/d;->a:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/feedback/a/d;->b:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/feedback/a/d;->c:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/feedback/a/d;->d:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/feedback/a/d;->e:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/feedback/a/d;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/feedback/a/d;->g:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/feedback/a/d;->h:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/feedback/a/d;->i:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/gmm/feedback/a/d;->j:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/gmm/feedback/a/d;->k:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/gmm/feedback/a/d;->l:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/gmm/feedback/a/d;->m:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/gmm/feedback/a/d;->n:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/gmm/feedback/a/d;->o:Lcom/google/android/apps/gmm/feedback/a/d;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->p:[Lcom/google/android/apps/gmm/feedback/a/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/google/android/apps/gmm/feedback/a/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/feedback/a/d;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->p:[Lcom/google/android/apps/gmm/feedback/a/d;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/feedback/a/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/feedback/a/d;

    return-object v0
.end method
