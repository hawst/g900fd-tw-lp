.class public final Lcom/google/android/apps/gmm/car/az;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/gms/car/ag;

.field final c:Lcom/google/android/apps/gmm/car/bb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/android/apps/gmm/car/az;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/az;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/ag;Lcom/google/android/apps/gmm/car/bb;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/gms/car/ag;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/az;->b:Lcom/google/android/gms/car/ag;

    .line 29
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/car/bb;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/az;->c:Lcom/google/android/apps/gmm/car/bb;

    .line 30
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/apps/gmm/car/az;->a:Ljava/lang/String;

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/az;->b:Lcom/google/android/gms/car/ag;

    new-instance v1, Lcom/google/android/apps/gmm/car/ba;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/ba;-><init>(Lcom/google/android/apps/gmm/car/az;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/ag;->a(Lcom/google/android/gms/car/ai;)V

    .line 52
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/az;->b:Lcom/google/android/gms/car/ag;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/ag;->a(I)Z
    :try_end_0
    .catch Lcom/google/android/gms/car/ao; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :goto_0
    return-void

    .line 53
    :catch_0
    move-exception v0

    .line 54
    sget-object v1, Lcom/google/android/apps/gmm/car/az;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onStart: car not connected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
