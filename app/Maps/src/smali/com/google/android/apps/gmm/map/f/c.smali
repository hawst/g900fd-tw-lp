.class public final Lcom/google/android/apps/gmm/map/f/c;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:[Lcom/google/android/apps/gmm/map/f/b;

.field public final b:[I

.field public final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/gmm/map/f/b;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/google/android/apps/gmm/map/f/f;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/f/f;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget v0, Lcom/google/android/apps/gmm/map/f/a/a;->a:I

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/f/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    .line 30
    sget v0, Lcom/google/android/apps/gmm/map/f/a/a;->a:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/c;->b:[I

    .line 36
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/c;->c:Ljava/util/HashSet;

    .line 44
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/f/f;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/f/c;->d:Lcom/google/android/apps/gmm/map/f/f;

    .line 45
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/f/b;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    .line 131
    if-nez p1, :cond_1

    .line 148
    :cond_0
    return v0

    .line 136
    :cond_1
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/d;->values()[Lcom/google/android/apps/gmm/map/f/a/d;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 137
    iget v5, v4, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    .line 138
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v6, v6, v5

    if-ne v6, p1, :cond_2

    .line 139
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v6, v6, v5

    invoke-interface {v6, v7, v4}, Lcom/google/android/apps/gmm/map/f/b;->a(Lcom/google/android/apps/gmm/map/f/b;Lcom/google/android/apps/gmm/map/f/a/d;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 140
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v6, v6, v5

    invoke-interface {v6, v7, v4}, Lcom/google/android/apps/gmm/map/f/b;->b(Lcom/google/android/apps/gmm/map/f/b;Lcom/google/android/apps/gmm/map/f/a/d;)V

    .line 141
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aput-object v7, v4, v5

    .line 142
    const/4 v4, 0x1

    shl-int/2addr v4, v5

    or-int/2addr v0, v4

    .line 136
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 144
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/c;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 216
    .line 217
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/d;->values()[Lcom/google/android/apps/gmm/map/f/a/d;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 218
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    iget v6, v4, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    aget-object v5, v5, v6

    if-eqz v5, :cond_0

    .line 219
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    iget v6, v4, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    aget-object v5, v5, v6

    invoke-interface {v5, v4}, Lcom/google/android/apps/gmm/map/f/b;->b(Lcom/google/android/apps/gmm/map/f/a/d;)Ljava/lang/Object;

    move-result-object v5

    .line 220
    if-eqz v5, :cond_0

    .line 221
    invoke-virtual {p1, v4, v5}, Lcom/google/android/apps/gmm/map/f/a/c;->a(Lcom/google/android/apps/gmm/map/f/a/d;Ljava/lang/Object;)Lcom/google/android/apps/gmm/map/f/a/c;

    .line 222
    const/4 v0, 0x1

    .line 217
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 226
    :cond_1
    return v0
.end method
