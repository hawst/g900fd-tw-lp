.class public Lcom/google/android/apps/gmm/startpage/c/d;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/startpage/e/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/startpage/c/d;->a:Lcom/google/android/libraries/curvular/bk;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/l/a/k;Lcom/google/android/libraries/curvular/bc;)V
    .locals 1

    .prologue
    .line 80
    if-eqz p0, :cond_0

    .line 81
    const-class v0, Lcom/google/android/apps/gmm/startpage/c/t;

    invoke-virtual {p1, v0, p0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 83
    :cond_0
    return-void
.end method

.method public static a(ZLcom/google/android/apps/gmm/startpage/e/f;Lcom/google/android/libraries/curvular/bc;)V
    .locals 2

    .prologue
    .line 55
    invoke-interface {p1}, Lcom/google/android/apps/gmm/startpage/e/f;->f()Lcom/google/android/apps/gmm/startpage/e/j;

    move-result-object v1

    .line 56
    if-eqz v1, :cond_0

    .line 57
    if-eqz p0, :cond_3

    const-class v0, Lcom/google/android/apps/gmm/startpage/c/n;

    .line 60
    :goto_0
    invoke-virtual {p2, v0, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 63
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/gmm/startpage/e/f;->g()Lcom/google/android/apps/gmm/base/l/a/t;

    move-result-object v1

    .line 64
    if-eqz v1, :cond_1

    .line 65
    if-eqz p0, :cond_4

    const-class v0, Lcom/google/android/apps/gmm/startpage/c/k;

    .line 68
    :goto_1
    invoke-virtual {p2, v0, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 71
    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/gmm/startpage/e/f;->h()Lcom/google/android/apps/gmm/startpage/e/k;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_2

    .line 73
    const-class v1, Lcom/google/android/apps/gmm/startpage/c/p;

    invoke-virtual {p2, v1, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 75
    :cond_2
    return-void

    .line 57
    :cond_3
    const-class v0, Lcom/google/android/apps/gmm/startpage/c/o;

    goto :goto_0

    .line 65
    :cond_4
    const-class v0, Lcom/google/android/apps/gmm/startpage/c/l;

    goto :goto_1
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 6

    .prologue
    .line 89
    const/4 v0, 0x4

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    sget-object v2, Lcom/google/android/apps/gmm/startpage/c/d;->a:Lcom/google/android/libraries/curvular/bk;

    .line 90
    sget-object v3, Lcom/google/android/libraries/curvular/g;->Z:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x1

    .line 95
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/startpage/e/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/startpage/e/f;->b()Lcom/google/android/apps/gmm/cardui/g/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/cardui/g/b;->c()Ljava/lang/Boolean;

    move-result-object v0

    sget v3, Lcom/google/android/apps/gmm/d;->aR:I

    .line 96
    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/d;->I:I

    .line 97
    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    .line 94
    invoke-static {v0, v3, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x2

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/c/d;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x3

    .line 101
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/startpage/e/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/startpage/e/f;->b()Lcom/google/android/apps/gmm/cardui/g/b;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/c/a;->g:Lcom/google/android/libraries/curvular/c/a;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v2

    .line 89
    invoke-static {v1}, Lcom/google/android/apps/gmm/util/b/f;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 2

    .prologue
    .line 28
    check-cast p2, Lcom/google/android/apps/gmm/startpage/e/f;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/startpage/e/f;->b()Lcom/google/android/apps/gmm/cardui/g/b;

    move-result-object v0

    invoke-static {v0, p4}, Lcom/google/android/apps/gmm/cardui/d/b;->a(Lcom/google/android/apps/gmm/cardui/g/b;Lcom/google/android/libraries/curvular/bc;)V

    invoke-interface {p2}, Lcom/google/android/apps/gmm/startpage/e/f;->b()Lcom/google/android/apps/gmm/cardui/g/b;

    move-result-object v0

    invoke-static {v0, p4}, Lcom/google/android/apps/gmm/cardui/d/b;->b(Lcom/google/android/apps/gmm/cardui/g/b;Lcom/google/android/libraries/curvular/bc;)V

    invoke-interface {p2}, Lcom/google/android/apps/gmm/startpage/e/f;->d()Lcom/google/android/apps/gmm/startpage/e/g;

    move-result-object v0

    if-eqz v0, :cond_0

    const-class v1, Lcom/google/android/apps/gmm/startpage/c/t;

    invoke-virtual {p4, v1, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_0
    return-void
.end method
