.class public Lcom/google/android/apps/gmm/map/r/a/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/maps/g/a/hm;",
            ">;"
        }
    .end annotation
.end field

.field static final serialVersionUID:J = 0x6444745a43b976cL


# instance fields
.field public transient a:Lcom/google/android/apps/gmm/map/r/a/e;

.field final b:Lcom/google/maps/g/a/hm;

.field public final c:Lcom/google/android/apps/gmm/map/r/a/ap;

.field public final d:Lcom/google/android/apps/gmm/map/r/a/ap;

.field public final e:Lcom/google/r/b/a/afz;

.field public final f:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 30
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    sget-object v1, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    sget-object v2, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    sget-object v3, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    invoke-static {v0, v1, v2, v3}, Lcom/google/b/c/dn;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/r/a/f;->g:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/r/a/g;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/g;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    .line 60
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/g;->b:Lcom/google/maps/g/a/hm;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/f;->b:Lcom/google/maps/g/a/hm;

    .line 61
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/g;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/f;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 62
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/g;->d:Lcom/google/android/apps/gmm/map/r/a/ap;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/f;->d:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 63
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/g;->e:Lcom/google/r/b/a/afz;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/g;->e:Lcom/google/r/b/a/afz;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/f;->e:Lcom/google/r/b/a/afz;

    .line 64
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/g;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/f;->f:Ljava/lang/String;

    .line 65
    return-void

    .line 63
    :cond_2
    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v0

    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 157
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 160
    invoke-static {p1}, Lcom/google/r/b/a/agl;->a(Ljava/io/InputStream;)Lcom/google/r/b/a/agl;

    move-result-object v0

    .line 161
    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/e;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/r/a/e;-><init>(Lcom/google/r/b/a/agl;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    .line 162
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 149
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    .line 152
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/e;->a:Lcom/google/r/b/a/agl;

    .line 153
    invoke-virtual {v0, p1}, Lcom/google/r/b/a/agl;->b(Ljava/io/OutputStream;)V

    .line 154
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/r/a/e;)Lcom/google/android/apps/gmm/map/r/a/f;
    .locals 2

    .prologue
    .line 143
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/r/a/g;-><init>(Lcom/google/android/apps/gmm/map/r/a/f;)V

    .line 144
    iput-object p1, v0, Lcom/google/android/apps/gmm/map/r/a/g;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    .line 145
    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/f;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/r/a/f;-><init>(Lcom/google/android/apps/gmm/map/r/a/g;)V

    return-object v1
.end method

.method public final a()Lcom/google/maps/g/a/hm;
    .locals 4

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/f;->e:Lcom/google/r/b/a/afz;

    iget-object v0, v0, Lcom/google/r/b/a/afz;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ho;->d()Lcom/google/maps/g/a/ho;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ho;

    iget v0, v0, Lcom/google/maps/g/a/ho;->c:I

    invoke-static {v0}, Lcom/google/maps/g/a/hr;->a(I)Lcom/google/maps/g/a/hr;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/hr;->a:Lcom/google/maps/g/a/hr;

    :cond_0
    sget-object v1, Lcom/google/maps/g/a/hr;->b:Lcom/google/maps/g/a/hr;

    if-ne v0, v1, :cond_3

    .line 89
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/r/a/f;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v1

    .line 95
    sget-object v0, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne v1, v0, :cond_4

    .line 96
    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v2, v2, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 97
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/r/a/f;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v2

    sget-object v3, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-ne v2, v3, :cond_1

    .line 98
    sget-object v0, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    .line 105
    :goto_1
    if-eqz v0, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/f;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 108
    :goto_2
    return-object v0

    .line 96
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/f;->b:Lcom/google/maps/g/a/hm;

    goto :goto_2

    .line 108
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/f;->b:Lcom/google/maps/g/a/hm;

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(I)Lcom/google/maps/g/a/hm;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 73
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v1, v1, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    if-ltz p1, :cond_2

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v2, v2

    if-gt v2, p1, :cond_3

    :cond_2
    :goto_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_2
    iget v0, v0, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    goto :goto_0

    :cond_3
    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/map/r/a/e;->a(I)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, p1

    goto :goto_1

    :cond_4
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_2
.end method
