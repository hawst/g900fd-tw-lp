.class public Lcom/google/android/apps/gmm/directions/i/au;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/p;
.implements Ljava/io/Serializable;


# instance fields
.field public transient a:Lcom/google/android/apps/gmm/directions/i/av;

.field public transient b:Lcom/google/android/libraries/curvular/ag;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/p;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/directions/i/aw;",
            ">;"
        }
    .end annotation
.end field

.field final d:Lcom/google/android/apps/gmm/directions/i/ay;

.field private final e:Lcom/google/r/b/a/afz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/gmm/directions/i/au;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/r/b/a/afz;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/i/au;->e:Lcom/google/r/b/a/afz;

    .line 257
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/au;->c:Ljava/util/ArrayList;

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/au;->c:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/apps/gmm/directions/i/aw;

    sget v2, Lcom/google/android/apps/gmm/l;->nW:I

    .line 259
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Lcom/google/maps/g/a/iy;

    sget-object v4, Lcom/google/maps/g/a/iy;->a:Lcom/google/maps/g/a/iy;

    aput-object v4, v3, v5

    .line 260
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    sget-object v4, Lcom/google/maps/g/a/iy;->a:Lcom/google/maps/g/a/iy;

    .line 261
    invoke-static {p2, v4}, Lcom/google/android/apps/gmm/directions/i/au;->a(Lcom/google/r/b/a/afz;Lcom/google/maps/g/a/iy;)Z

    move-result v4

    invoke-direct {v1, v2, v3, v4, p0}, Lcom/google/android/apps/gmm/directions/i/aw;-><init>(Ljava/lang/String;Ljava/util/List;ZLcom/google/android/apps/gmm/directions/i/au;)V

    .line 258
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    if-eqz p3, :cond_0

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/au;->c:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/apps/gmm/directions/i/aw;

    sget v2, Lcom/google/android/apps/gmm/l;->fF:I

    .line 265
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Lcom/google/maps/g/a/iy;

    sget-object v4, Lcom/google/maps/g/a/iy;->b:Lcom/google/maps/g/a/iy;

    aput-object v4, v3, v5

    sget-object v4, Lcom/google/maps/g/a/iy;->c:Lcom/google/maps/g/a/iy;

    aput-object v4, v3, v6

    const/4 v4, 0x2

    sget-object v5, Lcom/google/maps/g/a/iy;->d:Lcom/google/maps/g/a/iy;

    aput-object v5, v3, v4

    .line 266
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    sget-object v4, Lcom/google/maps/g/a/iy;->c:Lcom/google/maps/g/a/iy;

    .line 269
    invoke-static {p2, v4}, Lcom/google/android/apps/gmm/directions/i/au;->a(Lcom/google/r/b/a/afz;Lcom/google/maps/g/a/iy;)Z

    move-result v4

    invoke-direct {v1, v2, v3, v4, p0}, Lcom/google/android/apps/gmm/directions/i/aw;-><init>(Ljava/lang/String;Ljava/util/List;ZLcom/google/android/apps/gmm/directions/i/au;)V

    .line 264
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 290
    :goto_0
    new-instance v1, Lcom/google/android/apps/gmm/directions/i/ay;

    iget-object v0, p2, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agt;->d()Lcom/google/r/b/a/agt;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agt;

    invoke-direct {v1, v0, p0}, Lcom/google/android/apps/gmm/directions/i/ay;-><init>(Lcom/google/r/b/a/agt;Lcom/google/android/apps/gmm/directions/i/au;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/i/au;->d:Lcom/google/android/apps/gmm/directions/i/ay;

    .line 291
    return-void

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/au;->c:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/apps/gmm/directions/i/aw;

    sget v2, Lcom/google/android/apps/gmm/l;->nX:I

    .line 273
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Lcom/google/maps/g/a/iy;

    sget-object v4, Lcom/google/maps/g/a/iy;->b:Lcom/google/maps/g/a/iy;

    aput-object v4, v3, v5

    .line 274
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    sget-object v4, Lcom/google/maps/g/a/iy;->b:Lcom/google/maps/g/a/iy;

    .line 275
    invoke-static {p2, v4}, Lcom/google/android/apps/gmm/directions/i/au;->a(Lcom/google/r/b/a/afz;Lcom/google/maps/g/a/iy;)Z

    move-result v4

    invoke-direct {v1, v2, v3, v4, p0}, Lcom/google/android/apps/gmm/directions/i/aw;-><init>(Ljava/lang/String;Ljava/util/List;ZLcom/google/android/apps/gmm/directions/i/au;)V

    .line 272
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/au;->c:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/apps/gmm/directions/i/aw;

    sget v2, Lcom/google/android/apps/gmm/l;->nY:I

    .line 278
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Lcom/google/maps/g/a/iy;

    sget-object v4, Lcom/google/maps/g/a/iy;->c:Lcom/google/maps/g/a/iy;

    aput-object v4, v3, v5

    .line 279
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    sget-object v4, Lcom/google/maps/g/a/iy;->c:Lcom/google/maps/g/a/iy;

    .line 280
    invoke-static {p2, v4}, Lcom/google/android/apps/gmm/directions/i/au;->a(Lcom/google/r/b/a/afz;Lcom/google/maps/g/a/iy;)Z

    move-result v4

    invoke-direct {v1, v2, v3, v4, p0}, Lcom/google/android/apps/gmm/directions/i/aw;-><init>(Ljava/lang/String;Ljava/util/List;ZLcom/google/android/apps/gmm/directions/i/au;)V

    .line 277
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/au;->c:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/apps/gmm/directions/i/aw;

    sget v2, Lcom/google/android/apps/gmm/l;->fG:I

    .line 283
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Lcom/google/maps/g/a/iy;

    sget-object v4, Lcom/google/maps/g/a/iy;->d:Lcom/google/maps/g/a/iy;

    aput-object v4, v3, v5

    .line 285
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    sget-object v4, Lcom/google/maps/g/a/iy;->d:Lcom/google/maps/g/a/iy;

    .line 286
    invoke-static {p2, v4}, Lcom/google/android/apps/gmm/directions/i/au;->a(Lcom/google/r/b/a/afz;Lcom/google/maps/g/a/iy;)Z

    move-result v4

    invoke-direct {v1, v2, v3, v4, p0}, Lcom/google/android/apps/gmm/directions/i/aw;-><init>(Ljava/lang/String;Ljava/util/List;ZLcom/google/android/apps/gmm/directions/i/au;)V

    .line 282
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static a(Lcom/google/r/b/a/afz;Lcom/google/maps/g/a/iy;)Z
    .locals 3

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agt;->d()Lcom/google/r/b/a/agt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agt;

    .line 245
    new-instance v1, Lcom/google/n/ai;

    iget-object v0, v0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/agt;->i:Lcom/google/n/aj;

    invoke-direct {v1, v0, v2}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/iy;

    .line 246
    if-ne v0, p1, :cond_0

    .line 247
    const/4 v0, 0x1

    .line 250
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/b/c/cv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309
    new-instance v0, Lcom/google/b/c/cx;

    invoke-direct {v0}, Lcom/google/b/c/cx;-><init>()V

    .line 310
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/au;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Iterable;)Lcom/google/b/c/cx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/directions/h/q;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/au;->d:Lcom/google/android/apps/gmm/directions/i/ay;

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/au;->e:Lcom/google/r/b/a/afz;

    .line 321
    iget-object v0, v0, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agt;->d()Lcom/google/r/b/a/agt;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agt;

    .line 320
    invoke-static {v0}, Lcom/google/r/b/a/agt;->a(Lcom/google/r/b/a/agt;)Lcom/google/r/b/a/agw;

    move-result-object v3

    .line 322
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, v3, Lcom/google/r/b/a/agw;->f:Ljava/util/List;

    iget v0, v3, Lcom/google/r/b/a/agw;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, v3, Lcom/google/r/b/a/agw;->a:I

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/au;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/i/aw;

    .line 324
    iget-boolean v5, v0, Lcom/google/android/apps/gmm/directions/i/aw;->b:Z

    if-eqz v5, :cond_0

    iget-object v5, v0, Lcom/google/android/apps/gmm/directions/i/aw;->a:[Lcom/google/maps/g/a/iy;

    array-length v6, v5

    move v0, v2

    :goto_0
    if-ge v0, v6, :cond_0

    aget-object v7, v5, v0

    invoke-virtual {v3, v7}, Lcom/google/r/b/a/agw;->a(Lcom/google/maps/g/a/iy;)Lcom/google/r/b/a/agw;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 326
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/i/au;->d:Lcom/google/android/apps/gmm/directions/i/ay;

    sget-object v5, Lcom/google/android/apps/gmm/directions/i/ay;->a:[Lcom/google/android/apps/gmm/directions/i/ax;

    move v0, v2

    :goto_1
    const/4 v2, 0x3

    if-ge v0, v2, :cond_3

    aget-object v2, v5, v0

    iget v6, v2, Lcom/google/android/apps/gmm/directions/i/ax;->d:I

    iget v7, v4, Lcom/google/android/apps/gmm/directions/i/ay;->b:I

    if-ne v6, v7, :cond_2

    iget-object v0, v2, Lcom/google/android/apps/gmm/directions/i/ax;->f:Lcom/google/maps/g/a/gs;

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/r/b/a/agw;->a(Lcom/google/maps/g/a/gs;)Lcom/google/r/b/a/agw;

    .line 327
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/au;->a:Lcom/google/android/apps/gmm/directions/i/av;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/au;->e:Lcom/google/r/b/a/afz;

    invoke-static {v0}, Lcom/google/r/b/a/afz;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v0

    iget-object v4, v0, Lcom/google/r/b/a/agb;->b:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/r/b/a/agw;->g()Lcom/google/n/t;

    move-result-object v3

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v3, 0x1

    iput-boolean v3, v4, Lcom/google/n/ao;->d:Z

    iget v3, v0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/google/r/b/a/agb;->a:I

    invoke-virtual {v0}, Lcom/google/r/b/a/agb;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/directions/i/av;->a(Lcom/google/r/b/a/afz;)V

    .line 328
    return-object v1

    .line 326
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/au;->a:Lcom/google/android/apps/gmm/directions/i/av;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/i/av;->i()V

    .line 334
    const/4 v0, 0x0

    return-object v0
.end method
