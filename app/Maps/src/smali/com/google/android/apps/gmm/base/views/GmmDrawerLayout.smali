.class public Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;
.super Landroid/support/v4/widget/DrawerLayout;
.source "PG"


# instance fields
.field public k:Z

.field private l:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const v2, 0x800003

    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/DrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/f;->aZ:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->l:Landroid/graphics/drawable/Drawable;

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->setDrawerShadow(Landroid/graphics/drawable/Drawable;I)V

    .line 51
    sget v0, Lcom/google/android/apps/gmm/l;->W:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->setDrawerTitle(ILjava/lang/CharSequence;)V

    .line 52
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 64
    invoke-super {p0}, Landroid/support/v4/widget/DrawerLayout;->onFinishInflate()V

    .line 66
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 67
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout$LayoutParams;

    .line 68
    const v2, 0x800003

    iput v2, v0, Landroid/support/v4/widget/DrawerLayout$LayoutParams;->a:I

    .line 69
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 70
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 56
    invoke-super/range {p0 .. p5}, Landroid/support/v4/widget/DrawerLayout;->onLayout(ZIIII)V

    .line 57
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->k:Z

    if-eqz v0, :cond_0

    .line 58
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->c(Landroid/view/View;I)Z

    .line 60
    :cond_0
    return-void
.end method

.method public onRtlPropertiesChanged(I)V
    .locals 3

    .prologue
    const v2, 0x800003

    .line 74
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/base/d/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->l:Landroid/graphics/drawable/Drawable;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/d/e;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->setDrawerShadow(Landroid/graphics/drawable/Drawable;I)V

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->setDrawerShadow(Landroid/graphics/drawable/Drawable;I)V

    goto :goto_0
.end method
