.class Lcom/google/android/apps/gmm/directions/i/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/f;
.implements Ljava/io/Serializable;


# instance fields
.field a:Z

.field final b:Lcom/google/android/apps/gmm/directions/i/j;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/apps/gmm/directions/i/d;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLcom/google/android/apps/gmm/directions/i/j;Lcom/google/android/apps/gmm/directions/i/d;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/i;->c:Ljava/lang/String;

    .line 64
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/directions/i/i;->a:Z

    .line 65
    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/i/i;->b:Lcom/google/android/apps/gmm/directions/i/j;

    .line 66
    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/i/i;->d:Lcom/google/android/apps/gmm/directions/i/d;

    .line 67
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/i/i;->a:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/i/i;->a:Z

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/i;->d:Lcom/google/android/apps/gmm/directions/i/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/d;->b:Lcom/google/android/libraries/curvular/ag;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/d;->b:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 79
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 77
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/i/i;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/i;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
