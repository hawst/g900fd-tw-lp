.class public Lcom/google/android/apps/gmm/invocation/b/j;
.super Lcom/google/android/apps/gmm/invocation/b/e;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/invocation/b/e;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 24
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/r/b/a/gb;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/google/r/b/a/gb;->b:Lcom/google/r/b/a/gb;

    return-object v0
.end method

.method public final b(Lcom/google/e/a/a/a/b;)Ljava/lang/Runnable;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v4, 0x1a

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 29
    const/16 v3, 0xa

    if-eqz p1, :cond_4

    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v3}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_2

    move v0, v2

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p1, v3}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {p1, v3, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    move-object v3, v0

    .line 31
    :goto_2
    if-eqz v3, :cond_7

    iget-object v0, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v2}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_5

    move v0, v2

    :goto_3
    if-nez v0, :cond_1

    invoke-virtual {v3, v2}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    :cond_1
    move v0, v2

    :goto_4
    if-eqz v0, :cond_7

    invoke-virtual {v3, v2, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    move-object v5, v0

    .line 33
    :goto_5
    if-nez v5, :cond_8

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/g;

    const-string v1, "No scene in response."

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/invocation/b/g;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v1

    .line 29
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move-object v3, v6

    goto :goto_2

    :cond_5
    move v0, v1

    .line 31
    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_4

    :cond_7
    move-object v5, v6

    goto :goto_5

    .line 36
    :cond_8
    if-eqz v5, :cond_a

    iget-object v0, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v2}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_b

    move v0, v2

    :goto_6
    if-nez v0, :cond_9

    invoke-virtual {v5, v2}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_c

    :cond_9
    move v0, v2

    :goto_7
    if-eqz v0, :cond_a

    invoke-virtual {v5, v2, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    move-object v6, v0

    .line 38
    :cond_a
    const/4 v0, 0x5

    invoke-static {v5, v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IZ)Z

    move-result v2

    .line 40
    const/4 v0, 0x2

    invoke-static {v5, v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IZ)Z

    move-result v3

    .line 42
    const/4 v0, 0x3

    invoke-static {v5, v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IZ)Z

    move-result v4

    .line 44
    const/4 v0, 0x4

    invoke-static {v5, v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IZ)Z

    move-result v5

    .line 46
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/k;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/invocation/b/k;-><init>(Lcom/google/android/apps/gmm/invocation/b/j;ZZZZLcom/google/e/a/a/a/b;)V

    return-object v0

    :cond_b
    move v0, v1

    .line 36
    goto :goto_6

    :cond_c
    move v0, v1

    goto :goto_7
.end method
