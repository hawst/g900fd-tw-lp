.class public Lcom/google/android/apps/gmm/shared/net/a/o;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/shared/net/a/p;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/r/b/a/rn;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lcom/google/android/apps/gmm/shared/net/a/o;->a(Lcom/google/r/b/a/rn;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/a/o;->a:Ljava/util/List;

    .line 28
    return-void
.end method

.method private static a(Lcom/google/r/b/a/rn;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/r/b/a/rn;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/shared/net/a/p;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 78
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v5

    .line 80
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/r/b/a/rn;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lcom/google/r/b/a/rn;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/rj;->d()Lcom/google/r/b/a/rj;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/rj;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/rj;

    .line 81
    iget v1, v0, Lcom/google/r/b/a/rj;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    move v1, v2

    :goto_2
    if-nez v1, :cond_3

    move-object v0, v4

    .line 82
    :goto_3
    if-eqz v0, :cond_1

    .line 83
    invoke-virtual {v5, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_1

    :cond_2
    move v1, v3

    .line 81
    goto :goto_2

    :cond_3
    iget v7, v0, Lcom/google/r/b/a/rj;->b:I

    iget v1, v0, Lcom/google/r/b/a/rj;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v8, 0x2

    if-ne v1, v8, :cond_4

    move v1, v2

    :goto_4
    if-nez v1, :cond_5

    move-object v0, v4

    goto :goto_3

    :cond_4
    move v1, v3

    goto :goto_4

    :cond_5
    new-instance v1, Lcom/google/android/apps/gmm/shared/net/a/p;

    iget-object v0, v0, Lcom/google/r/b/a/rj;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/aw;->d()Lcom/google/maps/g/aw;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/aw;

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/net/a/s;->a(Lcom/google/maps/g/aw;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {v1, v7, v0}, Lcom/google/android/apps/gmm/shared/net/a/p;-><init>(ILandroid/content/Intent;)V

    move-object v0, v1

    goto :goto_3

    .line 86
    :cond_6
    invoke-virtual {v5}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/shared/net/ad;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/shared/net/ad;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/a/o;->b:Lcom/google/b/c/cv;

    if-nez v0, :cond_2

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/a/o;->a:Ljava/util/List;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/net/a/p;

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/a/p;->b:Landroid/content/Intent;

    const/4 v5, 0x0

    invoke-static {v1, v4, v5}, Lcom/google/android/apps/gmm/shared/c/k;->a(Landroid/content/Context;Landroid/content/Intent;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    iget v0, v0, Lcom/google/android/apps/gmm/shared/net/a/p;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 41
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/a/o;->b:Lcom/google/b/c/cv;

    .line 43
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/a/o;->b:Lcom/google/b/c/cv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 53
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/a/o;->b:Lcom/google/b/c/cv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    monitor-exit p0

    return-void

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 91
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "capabilities"

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/a/o;->a:Ljava/util/List;

    .line 92
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "supportedCapabilities"

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/a/o;->b:Lcom/google/b/c/cv;

    .line 93
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 94
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
