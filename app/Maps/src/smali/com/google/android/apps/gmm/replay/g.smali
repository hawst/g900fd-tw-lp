.class public Lcom/google/android/apps/gmm/replay/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/replay/a/a;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/replay/h;

.field private c:Lcom/google/android/apps/gmm/replay/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private d:Lcom/google/android/apps/gmm/replay/i;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private e:Lcom/google/android/apps/gmm/replay/i;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final f:Lcom/google/android/apps/gmm/base/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/gmm/replay/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/replay/g;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;)V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/replay/h;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/replay/h;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/replay/g;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/replay/h;)V

    .line 56
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/replay/h;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/google/android/apps/gmm/replay/g;->f:Lcom/google/android/apps/gmm/base/a;

    .line 68
    iput-object p2, p0, Lcom/google/android/apps/gmm/replay/g;->b:Lcom/google/android/apps/gmm/replay/h;

    .line 69
    return-void
.end method

.method private declared-synchronized b(Lcom/google/android/apps/gmm/replay/i;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 89
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/replay/g;->d:Lcom/google/android/apps/gmm/replay/i;

    if-eqz v2, :cond_0

    .line 90
    iget-object v2, p0, Lcom/google/android/apps/gmm/replay/g;->d:Lcom/google/android/apps/gmm/replay/i;

    sget-object v3, Lcom/google/android/apps/gmm/replay/i;->a:Ljava/lang/String;

    iget-object v3, v2, Lcom/google/android/apps/gmm/replay/i;->b:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v4, 0x0

    :try_start_1
    iput-boolean v4, v2, Lcom/google/android/apps/gmm/replay/i;->e:Z

    iget-object v2, v2, Lcom/google/android/apps/gmm/replay/i;->b:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 93
    :cond_0
    if-eqz p1, :cond_5

    .line 94
    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/replay/g;->d:Lcom/google/android/apps/gmm/replay/i;

    if-nez v2, :cond_1

    .line 95
    iget-object v2, p0, Lcom/google/android/apps/gmm/replay/g;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/util/b/g;->a()Z

    .line 96
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/replay/g;->d:Lcom/google/android/apps/gmm/replay/i;

    .line 104
    iget-object v2, p0, Lcom/google/android/apps/gmm/replay/g;->d:Lcom/google/android/apps/gmm/replay/i;

    sget-object v3, Lcom/google/android/apps/gmm/replay/i;->a:Ljava/lang/String;

    if-nez p0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 90
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0

    .line 104
    :cond_2
    iget-boolean v3, v2, Lcom/google/android/apps/gmm/replay/i;->e:Z

    if-nez v3, :cond_3

    :goto_0
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/replay/i;->e:Z

    iget-object v0, v2, Lcom/google/android/apps/gmm/replay/i;->c:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/replay/j;

    invoke-direct {v1, v2, p0}, Lcom/google/android/apps/gmm/replay/j;-><init>(Lcom/google/android/apps/gmm/replay/i;Lcom/google/android/apps/gmm/replay/g;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 105
    sget-object v0, Lcom/google/android/apps/gmm/replay/g;->a:Ljava/lang/String;

    const-string v0, "Playback started: "

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 107
    :cond_5
    :goto_1
    monitor-exit p0

    return-void

    .line 105
    :cond_6
    :try_start_5
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/g;->e:Lcom/google/android/apps/gmm/replay/i;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/replay/g;->b(Lcom/google/android/apps/gmm/replay/i;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    monitor-exit p0

    return-void

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/replay/i;)V
    .locals 3

    .prologue
    .line 163
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/g;->d:Lcom/google/android/apps/gmm/replay/i;

    if-ne p1, v0, :cond_0

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/replay/g;->d:Lcom/google/android/apps/gmm/replay/i;

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/g;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/util/b/g;->b()V

    .line 176
    sget-object v0, Lcom/google/android/apps/gmm/replay/g;->a:Ljava/lang/String;

    const-string v0, "Playback stopped: "

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 176
    :cond_1
    :try_start_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/util/replay/SetStateEvent;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 79
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/g;->c:Lcom/google/android/apps/gmm/replay/d;

    if-nez v0, :cond_0

    .line 112
    invoke-static {}, Lcom/google/android/apps/gmm/replay/d;->a()Lcom/google/android/apps/gmm/replay/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/replay/g;->c:Lcom/google/android/apps/gmm/replay/d;

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/g;->b:Lcom/google/android/apps/gmm/replay/h;

    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/g;->f:Lcom/google/android/apps/gmm/base/a;

    new-instance v1, Lcom/google/android/apps/gmm/replay/e;

    iget-object v2, p0, Lcom/google/android/apps/gmm/replay/g;->f:Lcom/google/android/apps/gmm/base/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/replay/g;->c:Lcom/google/android/apps/gmm/replay/d;

    invoke-direct {v1, p1, v2, v3}, Lcom/google/android/apps/gmm/replay/e;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/replay/d;)V

    new-instance v2, Lcom/google/android/apps/gmm/replay/i;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/replay/i;-><init>(Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/replay/a;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/replay/g;->b(Lcom/google/android/apps/gmm/replay/i;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    monitor-exit p0

    return-void

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
