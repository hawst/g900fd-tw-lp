.class public abstract Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/b/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/apps/gmm/base/placelists/a/c;",
        ">",
        "Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;",
        "Lcom/google/android/apps/gmm/place/b/c;"
    }
.end annotation


# instance fields
.field private A:Lcom/google/android/apps/gmm/util/a/d;

.field private B:Lcom/google/android/apps/gmm/base/l/aj;

.field private final C:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final D:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/place/ci;",
            ">;"
        }
    .end annotation
.end field

.field private final E:Ljava/lang/Object;

.field c:Lcom/google/android/apps/gmm/map/util/b/g;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field d:Ljava/lang/String;

.field public e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

.field f:Lcom/google/android/apps/gmm/place/m;

.field g:Z

.field public m:Landroid/view/View;

.field public final n:Lcom/google/android/apps/gmm/hotels/a/g;

.field public o:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<TT;>;"
        }
    .end annotation
.end field

.field public p:Z

.field q:Lcom/google/android/apps/gmm/base/placelists/o;

.field r:Lcom/google/android/apps/gmm/base/placelists/p;

.field public s:Lcom/google/android/apps/gmm/base/placelists/a/a;

.field public t:Z

.field u:Z

.field final v:Lcom/google/android/apps/gmm/base/placelists/a;

.field private w:Landroid/view/View;

.field private final x:Ljava/lang/Object;

.field private y:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    .line 129
    new-instance v0, Lcom/google/android/apps/gmm/base/placelists/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/placelists/b;-><init>(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->x:Ljava/lang/Object;

    .line 151
    new-instance v0, Lcom/google/android/apps/gmm/base/placelists/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/placelists/d;-><init>(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->n:Lcom/google/android/apps/gmm/hotels/a/g;

    .line 288
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->y:Z

    .line 292
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->t:Z

    .line 301
    new-instance v0, Lcom/google/android/apps/gmm/util/a/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/util/a/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->A:Lcom/google/android/apps/gmm/util/a/d;

    .line 306
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->u:Z

    .line 311
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->C:Ljava/util/Set;

    .line 312
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->D:Ljava/util/Set;

    .line 313
    new-instance v0, Lcom/google/android/apps/gmm/base/placelists/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/placelists/a;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->v:Lcom/google/android/apps/gmm/base/placelists/a;

    .line 316
    new-instance v0, Lcom/google/android/apps/gmm/base/placelists/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/placelists/f;-><init>(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->E:Ljava/lang/Object;

    .line 785
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;)V
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->n()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->b(Lcom/google/android/apps/gmm/base/g/c;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;IZ)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->r:Lcom/google/android/apps/gmm/base/placelists/p;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/placelists/p;->e:Z

    move-object v0, p0

    move v2, p1

    move v4, v3

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/base/g/c;IZZZ)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->e(I)V

    iput-boolean v3, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->y:Z

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->b(Lcom/google/android/apps/gmm/base/g/c;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/x/o;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/activities/w;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/activities/w;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/activities/w;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 712
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->a(Ljava/lang/Object;)Lcom/google/android/apps/gmm/place/PlacePageView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageView;->ad_()V

    .line 713
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0, v2, v2, v2, v2}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->setPadding(IIII)V

    .line 715
    iget-object v0, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v5, v0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v0, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 716
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->r()Lcom/google/android/apps/gmm/map/b/a/t;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/google/android/apps/gmm/base/activities/w;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/t;)Lcom/google/android/apps/gmm/base/activities/w;

    .line 718
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->f:Lcom/google/android/apps/gmm/place/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->z:Landroid/view/View;

    .line 719
    sget-object v0, Lcom/google/android/apps/gmm/base/support/c;->b:Lcom/google/android/libraries/curvular/au;

    .line 720
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v0

    .line 719
    iget-object v1, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->v:I

    .line 724
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->i()Landroid/view/View;

    move-result-object v0

    .line 725
    if-nez v0, :cond_1

    .line 727
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->B:Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->f:Lcom/google/android/apps/gmm/base/l/ai;

    .line 735
    :goto_0
    iget-object v0, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 737
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p1, v0, :cond_2

    .line 738
    iget-object v0, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v5, v0, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v0, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 739
    iget-object v1, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    new-instance v0, Lcom/google/android/apps/gmm/base/l/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/l/i;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V

    .line 740
    iget-object v1, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->G:Lcom/google/android/apps/gmm/base/l/j;

    .line 754
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->O:Lcom/google/android/apps/gmm/base/a/a;

    .line 755
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->q()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    .line 756
    iget-object v0, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v4, v0, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    .line 757
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    .line 758
    iget-object v0, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->r:Lcom/google/android/apps/gmm/base/placelists/p;

    .line 759
    iget-object v1, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->m:Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    .line 760
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 761
    return-void

    .line 730
    :cond_1
    iget-object v1, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v2, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    .line 733
    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/base/activities/w;->a(Landroid/view/View;)Lcom/google/android/apps/gmm/base/activities/w;

    goto :goto_0

    .line 743
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->w:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/g;->aM:I

    invoke-virtual {p2, v0, v1}, Lcom/google/android/apps/gmm/base/activities/w;->a(Landroid/view/View;I)Lcom/google/android/apps/gmm/base/activities/w;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->f:Lcom/google/android/apps/gmm/place/m;

    .line 744
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->z:Landroid/view/View;

    .line 745
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p1, v0, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 746
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->H:Z

    .line 747
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->h:Z

    .line 749
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p1, v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 751
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 750
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 748
    :goto_3
    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/base/activities/w;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;)Lcom/google/android/apps/gmm/base/activities/w;

    goto :goto_1

    .line 749
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    goto :goto_2

    .line 750
    :cond_4
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p1, v1, :cond_5

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    goto :goto_3

    :cond_5
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    goto :goto_3
.end method

.method private a(Lcom/google/android/apps/gmm/x/o;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 941
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->v:Lcom/google/android/apps/gmm/base/placelists/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/a;->a(Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/mylocation/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->d()V

    .line 943
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 944
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    .line 946
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 955
    :cond_0
    :goto_0
    return-void

    .line 949
    :cond_1
    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 952
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->C:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/c;->e:Z

    if-nez v1, :cond_0

    .line 953
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/base/g/c;)V

    goto :goto_0
.end method

.method private b(Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 2

    .prologue
    .line 1148
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1149
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->y:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1151
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/l;)V

    .line 1152
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/placelists/a/a;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    return-object v0
.end method

.method a(Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 7

    .prologue
    .line 979
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->j()Ljava/lang/String;

    move-result-object v1

    .line 981
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 980
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/maps/g/hy;

    move-result-object v6

    .line 982
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v0

    .line 984
    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->b()V

    .line 985
    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v5

    .line 989
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 990
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v2

    const/4 v4, 0x0

    move-object v3, p0

    .line 989
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/place/ci;->a(Lcom/google/android/apps/gmm/base/activities/o;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/place/b/c;Lcom/google/android/apps/gmm/place/ck;Lcom/google/e/a/a/a/b;Lcom/google/maps/g/hy;)Lcom/google/android/apps/gmm/place/ci;

    move-result-object v1

    .line 995
    if-eqz v1, :cond_0

    .line 996
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->C:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v2

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 998
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 1005
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->D:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1006
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Sent TactilePlaceDetailsRequest: LoggingParams="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1008
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/g/c;IZZZ)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1119
    if-eqz p3, :cond_1

    .line 1120
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->r:Lcom/google/android/apps/gmm/base/placelists/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/p;->a()V

    .line 1121
    if-eqz p5, :cond_0

    .line 1122
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->v()V

    .line 1124
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->r:Lcom/google/android/apps/gmm/base/placelists/p;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    :goto_0
    move-object v1, p1

    move v3, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/map/b/a/q;IZZZ)V

    .line 1127
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/e/a/a;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/e/a/a;-><init>(Lcom/google/android/apps/gmm/base/g/c;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 1130
    :cond_1
    if-eqz p1, :cond_2

    .line 1131
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->T()Lcom/google/b/c/cv;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1132
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->T()Lcom/google/b/c/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cv;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1135
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v1

    .line 1136
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->T()Lcom/google/b/c/cv;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 1135
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/indoor/a/a;->a(Lcom/google/android/apps/gmm/map/indoor/d/f;)V

    .line 1138
    :cond_2
    return-void

    .line 1124
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public abstract a(Lcom/google/android/apps/gmm/base/l/aj;)V
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/z;)V
    .locals 5

    .prologue
    .line 448
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/z;->a:Lcom/google/android/apps/gmm/v/aa;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 449
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->y:Z

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 452
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v1, v2, :cond_0

    .line 453
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->q:Lcom/google/android/apps/gmm/base/placelists/o;

    new-instance v2, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v3, Lcom/google/r/b/a/a;->h:Lcom/google/r/b/a/a;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    sget-object v3, Lcom/google/b/f/t;->cU:Lcom/google/b/f/t;

    .line 455
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 453
    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/google/android/apps/gmm/base/placelists/o;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 456
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    new-instance v1, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/activities/w;)V

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->r:Lcom/google/android/apps/gmm/base/placelists/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/p;->a()V

    .line 460
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/i;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1079
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1112
    :cond_0
    return-void

    .line 1084
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/l;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/base/placelists/l;-><init>(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1092
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    check-cast p1, Lcom/google/android/apps/gmm/place/ci;

    .line 1093
    iget-object v0, p1, Lcom/google/android/apps/gmm/place/ci;->a:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x1c

    invoke-virtual {v0, v4, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Ljava/util/List;

    move-result-object v0

    .line 1094
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1095
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    .line 1096
    if-eqz v0, :cond_2

    .line 1097
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(Lcom/google/android/apps/gmm/base/g/c;)I

    move-result v2

    .line 1102
    if-ltz v2, :cond_2

    .line 1103
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->f()Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b(ILcom/google/android/apps/gmm/base/g/c;)V

    .line 1104
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v3, Lcom/google/android/apps/gmm/base/placelists/c;

    invoke-direct {v3, p0, v2}, Lcom/google/android/apps/gmm/base/placelists/c;-><init>(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;I)V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/i;Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 4

    .prologue
    .line 1025
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->D:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1026
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1038
    :cond_0
    :goto_0
    return-void

    .line 1029
    :cond_1
    if-eqz p2, :cond_0

    .line 1030
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->C:Ljava/util/Set;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1031
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(Lcom/google/android/apps/gmm/base/g/c;)I

    move-result v0

    .line 1032
    if-ltz v0, :cond_0

    .line 1033
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b(ILcom/google/android/apps/gmm/base/g/c;)V

    .line 1034
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Z)V

    .line 1035
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->b(I)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1048
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/placelists/a/c;->c()Lcom/google/android/apps/gmm/base/placelists/a/e;

    move-result-object v4

    .line 1049
    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 1075
    :cond_0
    :goto_0
    return-void

    .line 1055
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v5

    .line 1057
    iget-object v6, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v7, Lcom/google/j/d/a/w;->c:Lcom/google/j/d/a/w;

    .line 1056
    sget-object v0, Lcom/google/android/apps/gmm/base/placelists/w;->a:[I

    iget-object v1, v4, Lcom/google/android/apps/gmm/base/placelists/a/e;->f:Lcom/google/android/apps/gmm/base/placelists/a/f;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/placelists/a/f;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move-object v1, v2

    :goto_1
    if-nez v1, :cond_a

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iget-object v3, v4, Lcom/google/android/apps/gmm/base/placelists/a/e;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eqz v3, :cond_2

    iget-object v3, v4, Lcom/google/android/apps/gmm/base/placelists/a/e;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_2
    iget-object v0, v4, Lcom/google/android/apps/gmm/base/placelists/a/e;->b:Ljava/lang/String;

    :goto_2
    iget-object v3, v4, Lcom/google/android/apps/gmm/base/placelists/a/e;->c:Ljava/lang/String;

    if-eqz v0, :cond_8

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/g/c;->f()Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v1

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/g/g;->n:Ljava/lang/String;

    move-object v0, v1

    :goto_3
    iget-boolean v1, v4, Lcom/google/android/apps/gmm/base/placelists/a/e;->g:Z

    if-eqz v1, :cond_3

    if-eqz v0, :cond_9

    :goto_4
    iput-boolean v10, v0, Lcom/google/android/apps/gmm/base/g/g;->j:Z

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v2

    :cond_4
    iget-boolean v0, v4, Lcom/google/android/apps/gmm/base/placelists/a/e;->g:Z

    if-eqz v0, :cond_5

    iget-object v0, v6, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v1

    invoke-static {v2}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/iamhere/c/o;

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/b/f/t;->gr:Lcom/google/b/f/t;

    invoke-interface {v0, v2, v7, v1}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 1065
    :cond_5
    if-eqz v2, :cond_0

    .line 1066
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a()I

    move-result v0

    if-le v0, v10, :cond_6

    .line 1067
    const-string v0, "Applied client-side overrides on a list with > 1 item"

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1070
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, v9, v2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(ILcom/google/android/apps/gmm/base/g/c;)V

    .line 1071
    if-eqz p1, :cond_0

    .line 1072
    invoke-virtual {p0, v9}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->b(I)V

    goto/16 :goto_0

    .line 1056
    :pswitch_0
    sget-object v1, Lcom/google/android/apps/gmm/base/g/e;->a:Lcom/google/android/apps/gmm/base/g/e;

    sget v0, Lcom/google/android/apps/gmm/l;->gV:I

    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/g/c;->f()Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v3

    iput-object v1, v3, Lcom/google/android/apps/gmm/base/g/g;->t:Lcom/google/android/apps/gmm/base/g/e;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/g/g;->n:Ljava/lang/String;

    move-object v1, v3

    goto :goto_1

    :pswitch_1
    sget-object v1, Lcom/google/android/apps/gmm/base/g/e;->b:Lcom/google/android/apps/gmm/base/g/e;

    sget v0, Lcom/google/android/apps/gmm/l;->pc:I

    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_7
    move-object v0, v2

    goto :goto_2

    :cond_8
    if-eqz v3, :cond_a

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/g/c;->f()Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->hz:I

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v3, v8, v9

    invoke-virtual {v6, v1, v8}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/g;->q:Ljava/lang/String;

    goto :goto_3

    :cond_9
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/g/c;->f()Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v0

    goto :goto_4

    :cond_a
    move-object v0, v1

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method b(I)V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->b(I)V

    .line 220
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    if-eqz v0, :cond_1

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->y:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 470
    :goto_0
    return v0

    .line 468
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 470
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->y:Z

    goto :goto_0
.end method

.method public final d_()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 641
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->u:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 642
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->n()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c()I

    move-result v2

    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/base/g/c;IZZZ)V

    .line 647
    :cond_0
    new-instance v2, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 649
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 651
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 653
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->n()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    .line 660
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b()I

    move-result v5

    if-lez v5, :cond_4

    if-nez v0, :cond_4

    if-eqz v1, :cond_4

    .line 661
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v1, :cond_3

    move v1, v3

    :goto_0
    if-nez v1, :cond_4

    .line 662
    :goto_1
    if-nez v0, :cond_1

    .line 663
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 666
    :cond_1
    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/i;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/gmm/base/placelists/i;-><init>(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;Z)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v3, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 686
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v1, :cond_2

    .line 690
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 706
    :cond_2
    :goto_2
    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/activities/w;)V

    .line 707
    return-void

    :cond_3
    move v1, v4

    .line 661
    goto :goto_0

    :cond_4
    move v3, v4

    goto :goto_1

    .line 693
    :cond_5
    new-instance v0, Lcom/google/android/apps/gmm/base/placelists/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/placelists/j;-><init>(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;)V

    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 703
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    goto :goto_2
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 775
    const/4 v0, 0x0

    return-object v0
.end method

.method public final n()Lcom/google/android/apps/gmm/base/g/c;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1161
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b()I

    move-result v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    .line 1162
    :goto_1
    if-nez v0, :cond_3

    move-object v0, v1

    :goto_2
    return-object v0

    .line 1161
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->b()I

    move-result v2

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->g:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_3
    sub-int v0, v2, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_3

    :cond_2
    move-object v0, v1

    goto :goto_1

    .line 1162
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    goto :goto_2
.end method

.method public o()V
    .locals 0

    .prologue
    .line 783
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 489
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onAttach(Landroid/app/Activity;)V

    .line 490
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 491
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 495
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onCreate(Landroid/os/Bundle;)V

    .line 497
    if-eqz p1, :cond_0

    .line 498
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "placeItemListProviderRef"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/placelists/a/c;->b()Lcom/google/android/apps/gmm/base/placelists/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    .line 500
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Z)V

    .line 501
    const-string v0, "fromSearchList"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->p:Z

    .line 503
    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/o;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/placelists/o;-><init>(Lcom/google/android/apps/gmm/z/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->q:Lcom/google/android/apps/gmm/base/placelists/o;

    .line 505
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->x:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 506
    return-void

    .line 497
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 517
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->g()V

    .line 518
    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/place/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/place/m;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->f:Lcom/google/android/apps/gmm/place/m;

    .line 519
    new-instance v0, Lcom/google/android/apps/gmm/base/placelists/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->q:Lcom/google/android/apps/gmm/base/placelists/o;

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/google/android/apps/gmm/base/placelists/p;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/place/l;Lcom/google/android/apps/gmm/base/placelists/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->r:Lcom/google/android/apps/gmm/base/placelists/p;

    .line 520
    return-object v4

    .line 517
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/h;->as:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    new-instance v0, Lcom/google/android/apps/gmm/place/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->q:Lcom/google/android/apps/gmm/base/placelists/o;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/gmm/place/ac;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Landroid/app/Fragment;)V

    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/base/placelists/e;-><init>(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/ac;->a:Lcom/google/android/apps/gmm/search/views/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iput-object v0, v1, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->l:Lcom/google/android/apps/gmm/place/an;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/h;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/base/placelists/h;-><init>(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->setAdapter(Lcom/google/android/apps/gmm/place/am;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/f/b;->a(Lcom/google/android/libraries/curvular/bd;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->w:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->w:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/gmm/base/f/b;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->m:Landroid/view/View;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->x:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 511
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onDestroy()V

    .line 512
    return-void
.end method

.method public onLowMemory()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 174
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    if-eqz v0, :cond_1

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->w:Landroid/view/View;

    const/4 v0, 0x1

    .line 176
    :goto_0
    const-string v2, "PlaceCollectionMapFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x2a

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onLowMemory and null out cached view:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 179
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 175
    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 1202
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 1204
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 1205
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->da:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 1207
    const/4 v0, 0x1

    .line 1209
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 896
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onPause()V

    .line 898
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->E:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 900
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->D:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/ci;

    .line 901
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/ci;->f()V

    goto :goto_0

    .line 903
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->D:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 905
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->C:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 907
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->A:Lcom/google/android/apps/gmm/util/a/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    .line 908
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->A:Lcom/google/android/apps/gmm/util/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/util/a/d;->b()V

    .line 911
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->r:Lcom/google/android/apps/gmm/base/placelists/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/p;->a()V

    .line 913
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->g:Z

    .line 915
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->v:Lcom/google/android/apps/gmm/base/placelists/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/a;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/b/a;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->b()V

    goto :goto_2

    :cond_3
    move v0, v1

    .line 907
    goto :goto_1

    .line 916
    :cond_4
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 844
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->u:Z

    .line 845
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 847
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x26

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " placemark(s) will be shown"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 849
    new-instance v2, Lcom/google/android/apps/gmm/base/placelists/k;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/gmm/base/placelists/k;-><init>(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->B:Lcom/google/android/apps/gmm/base/l/aj;

    .line 868
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->B:Lcom/google/android/apps/gmm/base/l/aj;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/base/l/aj;)V

    .line 870
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->v:Lcom/google/android/apps/gmm/base/placelists/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/placelists/a;->a()V

    .line 873
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->h:Lcom/google/android/apps/gmm/util/a/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->A:Lcom/google/android/apps/gmm/util/a/d;

    new-instance v4, Lcom/google/android/apps/gmm/base/placelists/m;

    invoke-direct {v4, p0}, Lcom/google/android/apps/gmm/base/placelists/m;-><init>(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/util/a/a;->a(Lcom/google/android/apps/gmm/util/a/d;Lcom/google/android/apps/gmm/util/a/d;)Lcom/google/android/apps/gmm/util/a/d;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->A:Lcom/google/android/apps/gmm/util/a/d;

    .line 876
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    if-nez v2, :cond_1

    const/4 v0, -0x1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c()I

    move-result v2

    if-eq v0, v2, :cond_3

    .line 877
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->setCurrentItem(I)V

    .line 884
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 885
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/x/o;)V

    .line 888
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->E:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 890
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->d_()V

    .line 891
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->u:Z

    .line 892
    return-void

    .line 876
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->b()I

    move-result v2

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->g:Z

    if-eqz v3, :cond_2

    :goto_2
    sub-int v0, v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2

    .line 879
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->r:Lcom/google/android/apps/gmm/base/placelists/p;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/placelists/p;->e:Z

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 920
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 926
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/x/o;->b(Ljava/io/Serializable;)V

    .line 927
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "placeItemListProviderRef"

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 929
    const-string v0, "fromSearchList"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->p:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 930
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    if-eqz v0, :cond_1

    iput-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iput-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->w:Landroid/view/View;

    move v0, v1

    .line 185
    :goto_0
    const-string v3, "PlaceCollectionMapFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x2f

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "onTrimMemory(%d) and null out cached view:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v3, v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 184
    goto :goto_0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 811
    const/4 v0, 0x0

    return v0
.end method

.method public q()Lcom/google/android/apps/gmm/base/activities/ag;
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 815
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->a()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v2

    .line 816
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a()I

    move-result v3

    if-ne v3, v0, :cond_3

    .line 827
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v3

    .line 828
    new-array v4, v0, [Lcom/google/android/apps/gmm/base/g/c;

    aput-object v3, v4, v1

    if-nez v4, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    array-length v3, v4

    if-ltz v3, :cond_1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x5

    int-to-long v6, v3

    add-long/2addr v0, v6

    div-int/lit8 v3, v3, 0xa

    int-to-long v6, v3

    add-long/2addr v0, v6

    const-wide/32 v6, 0x7fffffff

    cmp-long v3, v0, v6

    if-lez v3, :cond_4

    const v0, 0x7fffffff

    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v1, v4}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/ag;->q:Ljava/util/List;

    .line 830
    :cond_3
    return-object v2

    .line 828
    :cond_4
    const-wide/32 v6, -0x80000000

    cmp-long v3, v0, v6

    if-gez v3, :cond_5

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_5
    long-to-int v0, v0

    goto :goto_1
.end method

.method public abstract r()Lcom/google/android/apps/gmm/map/b/a/t;
    .annotation runtime Lb/a/a;
    .end annotation
.end method

.method public abstract s()Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end method

.method public abstract t()V
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 1144
    const/4 v0, 0x0

    return v0
.end method

.method public v()V
    .locals 0

    .prologue
    .line 1179
    return-void
.end method

.method public w()V
    .locals 4

    .prologue
    .line 1191
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1192
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1193
    instance-of v2, v0, Lcom/google/android/apps/gmm/place/PlacePageView;

    if-eqz v2, :cond_0

    .line 1194
    check-cast v0, Lcom/google/android/apps/gmm/place/PlacePageView;

    .line 1195
    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->r:Lcom/google/android/apps/gmm/base/placelists/p;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/base/placelists/p;->d:Z

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/place/PlacePageView;->a(Z)V

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->v:Lcom/google/android/apps/gmm/base/placelists/a;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/apps/gmm/base/placelists/a;->a(Landroid/view/View;Lcom/google/android/apps/gmm/x/o;)V

    .line 1191
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1198
    :cond_1
    return-void
.end method
