.class public Lcom/google/android/apps/gmm/base/app/GoogleMapsApplication;
.super Landroid/app/Application;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/stitch/a/d;


# instance fields
.field private final a:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/libraries/stitch/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/android/apps/gmm/base/app/b;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/apps/gmm/replay/d;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/google/android/apps/gmm/replay/UseCannedStpResponseEvent;

    aput-object v2, v0, v1

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 61
    new-instance v0, Lcom/google/android/apps/gmm/base/app/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/a;-><init>(Lcom/google/android/apps/gmm/base/app/GoogleMapsApplication;)V

    .line 62
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/GoogleMapsApplication;->a:Lcom/google/b/a/bw;

    .line 61
    return-void

    .line 62
    :cond_0
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/stitch/a/b;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/GoogleMapsApplication;->a:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/stitch/a/b;

    return-object v0
.end method

.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    .line 105
    invoke-static {p0}, Landroid/support/a/a;->a(Landroid/content/Context;)V

    .line 108
    return-void
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 82
    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    new-instance v0, Landroid/os/StrictMode$VmPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->detectAll()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    .line 84
    new-instance v0, Lcom/google/android/apps/gmm/base/app/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/b;-><init>(Landroid/app/Application;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/GoogleMapsApplication;->b:Lcom/google/android/apps/gmm/base/app/b;

    .line 85
    return-void
.end method

.method public onTerminate()V
    .locals 3

    .prologue
    .line 113
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/app/GoogleMapsApplication;->b:Lcom/google/android/apps/gmm/base/app/b;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a()V

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->b()V

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/app/b;->h:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/q/a;

    iget-object v2, v0, Lcom/google/android/apps/gmm/q/a;->a:Lcom/google/android/apps/gmm/q/d;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/q/a;->a:Lcom/google/android/apps/gmm/q/d;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/q/d;->b()V

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/q/a;->a:Lcom/google/android/apps/gmm/q/d;

    :cond_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/app/b;->d:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/d;->c()V

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/app/b;->i:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/e/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/e/a;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 114
    return-void
.end method
