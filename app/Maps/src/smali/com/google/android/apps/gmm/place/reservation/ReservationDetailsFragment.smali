.class public Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/place/reservation/s;

.field b:Lcom/google/android/apps/gmm/base/e/c;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/android/apps/gmm/z/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/q;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/reservation/q;-><init>(Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->c:Ljava/lang/Object;

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/r/b/a/j;)Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;
    .locals 2

    .prologue
    .line 49
    .line 50
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->q()Lcom/google/maps/g/q;

    move-result-object v0

    iget v0, v0, Lcom/google/maps/g/q;->d:I

    invoke-static {v0}, Lcom/google/maps/g/s;->a(I)Lcom/google/maps/g/s;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/s;->a:Lcom/google/maps/g/s;

    :cond_0
    sget-object v1, Lcom/google/maps/g/s;->a:Lcom/google/maps/g/s;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 49
    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 50
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 51
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/ad;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/reservation/ad;-><init>()V

    .line 52
    iput-object p1, v0, Lcom/google/android/apps/gmm/place/reservation/ad;->a:Lcom/google/android/apps/gmm/base/g/c;

    .line 53
    iput-object p2, v0, Lcom/google/android/apps/gmm/place/reservation/ad;->b:Lcom/google/r/b/a/j;

    .line 54
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/reservation/ad;->a()Lcom/google/android/apps/gmm/place/reservation/ac;

    move-result-object v0

    .line 55
    new-instance v1, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;-><init>()V

    .line 56
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/place/reservation/ac;->a(Lcom/google/android/apps/gmm/x/a;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 57
    return-object v1
.end method


# virtual methods
.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lcom/google/b/f/t;->dT:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/reservation/ad;->a(Lcom/google/android/apps/gmm/x/a;Landroid/os/Bundle;)Lcom/google/android/apps/gmm/place/reservation/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/reservation/ad;->a()Lcom/google/android/apps/gmm/place/reservation/ac;

    move-result-object v0

    .line 65
    new-instance v1, Lcom/google/android/apps/gmm/place/reservation/s;

    .line 66
    iget-object v2, v0, Lcom/google/android/apps/gmm/place/reservation/ac;->a:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/reservation/ac;->b:Lcom/google/r/b/a/j;

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/gmm/place/reservation/s;-><init>(Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/r/b/a/j;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->a:Lcom/google/android/apps/gmm/place/reservation/s;

    .line 67
    new-instance v1, Lcom/google/android/apps/gmm/z/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/z/a;-><init>(Lcom/google/android/apps/gmm/z/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->d:Lcom/google/android/apps/gmm/z/a;

    .line 68
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 73
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 75
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    .line 76
    const-class v1, Lcom/google/android/apps/gmm/place/reservation/a/f;

    .line 77
    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 79
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->a:Lcom/google/android/apps/gmm/place/reservation/s;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 80
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->a:Lcom/google/android/apps/gmm/place/reservation/s;

    new-instance v2, Lcom/google/android/apps/gmm/place/reservation/r;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/gmm/place/reservation/r;-><init>(Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;Landroid/view/View;)V

    iput-object v2, v1, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    .line 87
    return-object v0
.end method

.method public onPause()V
    .locals 5

    .prologue
    .line 148
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->c:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aB:Lcom/google/android/apps/gmm/shared/b/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->a:Lcom/google/android/apps/gmm/place/reservation/s;

    iget-object v3, v3, Lcom/google/android/apps/gmm/place/reservation/s;->c:Lcom/google/android/apps/gmm/place/reservation/x;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/reservation/b/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aC:Lcom/google/android/apps/gmm/shared/b/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->a:Lcom/google/android/apps/gmm/place/reservation/s;

    iget-object v3, v3, Lcom/google/android/apps/gmm/place/reservation/s;->d:Lcom/google/android/apps/gmm/place/reservation/x;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/reservation/b/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aD:Lcom/google/android/apps/gmm/shared/b/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->a:Lcom/google/android/apps/gmm/place/reservation/s;

    iget-object v3, v3, Lcom/google/android/apps/gmm/place/reservation/s;->e:Lcom/google/android/apps/gmm/place/reservation/x;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/reservation/b/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aE:Lcom/google/android/apps/gmm/shared/b/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->a:Lcom/google/android/apps/gmm/place/reservation/s;

    iget-object v3, v3, Lcom/google/android/apps/gmm/place/reservation/s;->f:Lcom/google/android/apps/gmm/place/reservation/x;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/reservation/b/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 151
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->d:Lcom/google/android/apps/gmm/z/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/a;->b()V

    .line 152
    return-void
.end method

.method public onResume()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 92
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->c:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v9

    if-eqz v9, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v4

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aB:Lcom/google/android/apps/gmm/shared/b/c;

    const-string v0, ""

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1, v9}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aC:Lcom/google/android/apps/gmm/shared/b/c;

    const-string v1, ""

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v2, v9}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_1
    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->aD:Lcom/google/android/apps/gmm/shared/b/c;

    const-string v2, ""

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v3, v9}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_2
    sget-object v6, Lcom/google/android/apps/gmm/shared/b/c;->aE:Lcom/google/android/apps/gmm/shared/b/c;

    const-string v3, ""

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-static {v6, v9}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->b:Lcom/google/android/apps/gmm/base/e/c;

    if-nez v4, :cond_8

    move-object v4, v5

    :goto_0
    iget-object v6, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->b:Lcom/google/android/apps/gmm/base/e/c;

    if-eqz v6, :cond_c

    iget-object v10, v9, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->b:Lcom/google/android/apps/gmm/base/e/c;

    iget-object v11, v6, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-nez v11, :cond_a

    move-object v6, v5

    :goto_1
    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_b

    :cond_4
    move v6, v7

    :goto_2
    if-nez v6, :cond_c

    move v6, v7

    :goto_3
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_5

    if-eqz v6, :cond_5

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v6, v4

    if-ne v6, v12, :cond_5

    aget-object v0, v4, v8

    aget-object v1, v4, v7

    :cond_5
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v2, v9, Landroid/accounts/Account;->name:Ljava/lang/String;

    if-nez v2, :cond_6

    const-string v2, ""

    :cond_6
    iget-object v4, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->a:Lcom/google/android/apps/gmm/place/reservation/s;

    iget-object v6, v4, Lcom/google/android/apps/gmm/place/reservation/s;->c:Lcom/google/android/apps/gmm/place/reservation/x;

    iput-object v0, v6, Lcom/google/android/apps/gmm/place/reservation/x;->a:Ljava/lang/String;

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/reservation/s;->d:Lcom/google/android/apps/gmm/place/reservation/x;

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/reservation/x;->a:Ljava/lang/String;

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/reservation/s;->e:Lcom/google/android/apps/gmm/place/reservation/x;

    iput-object v2, v0, Lcom/google/android/apps/gmm/place/reservation/x;->a:Ljava/lang/String;

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/reservation/s;->f:Lcom/google/android/apps/gmm/place/reservation/x;

    iput-object v3, v0, Lcom/google/android/apps/gmm/place/reservation/x;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->a:Lcom/google/android/apps/gmm/place/reservation/s;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    if-eqz v1, :cond_7

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 96
    :cond_7
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 97
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v7, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v7, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 99
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v5, v1, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v7, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 100
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v8, v1, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 101
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->d:Lcom/google/android/apps/gmm/z/a;

    .line 102
    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/p;->a(Lcom/google/android/apps/gmm/z/a;)Lcom/google/android/apps/gmm/base/activities/y;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 103
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 104
    return-void

    .line 94
    :cond_8
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/j/b;->B()Lcom/google/android/apps/gmm/u/a/a;

    move-result-object v6

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->b:Lcom/google/android/apps/gmm/base/e/c;

    iget-object v10, v4, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-nez v10, :cond_9

    move-object v4, v5

    :goto_4
    invoke-interface {v6, v4}, Lcom/google/android/apps/gmm/u/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    :cond_9
    iget-object v4, v4, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_4

    :cond_a
    iget-object v6, v6, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto/16 :goto_1

    :cond_b
    move v6, v8

    goto/16 :goto_2

    :cond_c
    move v6, v8

    goto/16 :goto_3
.end method
