.class Lcom/google/android/apps/gmm/place/o;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# instance fields
.field a:I

.field b:Z

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Rect;

.field private final e:Landroid/graphics/drawable/GradientDrawable;

.field private final f:I

.field private final g:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 281
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 271
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/o;->c:Landroid/graphics/Paint;

    .line 272
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/o;->d:Landroid/graphics/Rect;

    .line 273
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/o;->e:Landroid/graphics/drawable/GradientDrawable;

    .line 282
    sget v0, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v0

    .line 289
    const v1, 0xffffff

    and-int/2addr v1, v0

    .line 291
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/o;->c:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 292
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/o;->e:Landroid/graphics/drawable/GradientDrawable;

    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    aput v0, v3, v4

    const/4 v0, 0x1

    aput v1, v3, v0

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setColors([I)V

    .line 293
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/place/o;->f:I

    .line 294
    sget-object v0, Lcom/google/android/apps/gmm/base/support/c;->b:Lcom/google/android/libraries/curvular/au;

    .line 295
    invoke-interface {v0, p1}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/place/o;->g:I

    .line 296
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 306
    iget v0, p0, Lcom/google/android/apps/gmm/place/o;->a:I

    if-nez v0, :cond_0

    .line 316
    :goto_0
    return-void

    .line 312
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    move-result v0

    .line 313
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/o;->d:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/o;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/o;->d:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/o;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/google/android/apps/gmm/place/o;->a:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/o;->d:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 314
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/o;->d:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/o;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/o;->d:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/google/android/apps/gmm/place/o;->g:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/place/o;->b:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/o;->d:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/google/android/apps/gmm/place/o;->f:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/o;->d:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/o;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/place/o;->b:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/o;->d:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/o;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/o;->d:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/google/android/apps/gmm/place/o;->g:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/o;->d:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/o;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/google/android/apps/gmm/place/o;->f:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/o;->e:Landroid/graphics/drawable/GradientDrawable;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/o;->d:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/o;->e:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 315
    :cond_2
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method public getOpacity()I
    .locals 2

    .prologue
    .line 362
    iget v0, p0, Lcom/google/android/apps/gmm/place/o;->a:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/o;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/o;->b:Z

    if-nez v0, :cond_0

    .line 363
    const/4 v0, -0x1

    .line 365
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x3

    goto :goto_0
.end method

.method public setAlpha(I)V
    .locals 0

    .prologue
    .line 352
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 357
    return-void
.end method
