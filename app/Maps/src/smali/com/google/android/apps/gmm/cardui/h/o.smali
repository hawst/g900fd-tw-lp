.class public Lcom/google/android/apps/gmm/cardui/h/o;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/i/i;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/util/b/ag;

.field private final b:Lcom/google/o/h/a/ol;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/ol;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/apps/gmm/cardui/h/o;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 29
    iput-object p2, p0, Lcom/google/android/apps/gmm/cardui/h/o;->b:Lcom/google/o/h/a/ol;

    .line 30
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 49
    if-nez p1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/o;->b:Lcom/google/o/h/a/ol;

    iget v2, v2, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/o;->b:Lcom/google/o/h/a/ol;

    iget v0, v0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/o;->b:Lcom/google/o/h/a/ol;

    iget-object v0, v2, Lcom/google/o/h/a/ol;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v1, v2, Lcom/google/o/h/a/ol;->c:Ljava/lang/Object;

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    const-string v0, ""

    goto :goto_1
.end method

.method public final b(I)Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/o;->b:Lcom/google/o/h/a/ol;

    .line 55
    iget-object v0, v0, Lcom/google/o/h/a/ol;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hv;

    sget v1, Lcom/google/android/apps/gmm/f;->gk:I

    .line 54
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Lcom/google/o/h/a/hv;I)Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/o;->b:Lcom/google/o/h/a/ol;

    iget v0, v0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/o;->b:Lcom/google/o/h/a/ol;

    iget-object v0, v2, Lcom/google/o/h/a/ol;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v1, v2, Lcom/google/o/h/a/ol;->e:Ljava/lang/Object;

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    const-string v0, ""

    goto :goto_1
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/base/l/a/u;
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/z/b/l;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/apps/gmm/z/b/m;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/m;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/h/o;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 71
    iget-object v1, v1, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/h/o;->b:Lcom/google/o/h/a/ol;

    .line 72
    invoke-virtual {v1}, Lcom/google/o/h/a/ol;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 73
    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/h/o;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/util/b/ag;->e:Lcom/google/r/b/a/tf;

    .line 74
    if-eqz v1, :cond_0

    .line 75
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->e:Lcom/google/r/b/a/tf;

    .line 77
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 98
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()Lcom/google/android/libraries/curvular/aq;
    .locals 1

    .prologue
    .line 103
    sget v0, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
