.class public Lcom/google/android/apps/gmm/shared/net/al;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Z

.field private b:Lcom/google/android/apps/gmm/shared/net/k;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final c:Lcom/google/android/apps/gmm/shared/c/f;

.field private final d:J


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;J)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/al;->a:Z

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/al;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 25
    iput-wide p2, p0, Lcom/google/android/apps/gmm/shared/net/al;->d:J

    .line 26
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/android/apps/gmm/shared/net/k;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 46
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/al;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    .line 48
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/al;->a:Z

    if-nez v2, :cond_1

    .line 49
    iget-wide v2, p0, Lcom/google/android/apps/gmm/shared/net/al;->d:J

    add-long/2addr v2, v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/al;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 50
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gtz v4, :cond_0

    .line 51
    sget-object v2, Lcom/google/android/apps/gmm/shared/net/k;->m:Lcom/google/android/apps/gmm/shared/net/k;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/shared/net/al;->a(Lcom/google/android/apps/gmm/shared/net/k;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 54
    :cond_0
    :try_start_1
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v2

    goto :goto_0

    .line 60
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/al;->b:Lcom/google/android/apps/gmm/shared/net/k;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 33
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/al;->a:Z

    if-nez v0, :cond_0

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/al;->a:Z

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/al;->b:Lcom/google/android/apps/gmm/shared/net/k;

    .line 37
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    monitor-exit p0

    return-void

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
