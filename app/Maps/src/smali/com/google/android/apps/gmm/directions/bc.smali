.class public Lcom/google/android/apps/gmm/directions/bc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/aw;


# instance fields
.field private final a:Lcom/google/maps/g/a/bi;

.field private final b:Lcom/google/android/apps/gmm/map/r/a/h;

.field private c:Lcom/google/android/apps/gmm/directions/bb;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private d:Lcom/google/android/apps/gmm/shared/c/c/c;


# direct methods
.method public constructor <init>(Lcom/google/maps/g/a/bi;Lcom/google/android/apps/gmm/map/r/a/h;)V
    .locals 1
    .param p1    # Lcom/google/maps/g/a/bi;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/apps/gmm/map/r/a/h;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/bc;->a:Lcom/google/maps/g/a/bi;

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/bc;->b:Lcom/google/android/apps/gmm/map/r/a/h;

    .line 46
    sget v0, Lcom/google/android/apps/gmm/f;->bD:I

    .line 47
    sget v0, Lcom/google/android/apps/gmm/f;->bC:I

    .line 48
    return-void
.end method


# virtual methods
.method public final d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 18

    .prologue
    .line 52
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->d:Lcom/google/android/apps/gmm/shared/c/c/c;

    if-nez v2, :cond_0

    .line 53
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->d:Lcom/google/android/apps/gmm/shared/c/c/c;

    .line 55
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->c:Lcom/google/android/apps/gmm/directions/bb;

    if-nez v2, :cond_1

    .line 56
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->a:Lcom/google/maps/g/a/bi;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->b:Lcom/google/android/apps/gmm/map/r/a/h;

    if-nez v2, :cond_2

    .line 58
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->c:Lcom/google/android/apps/gmm/directions/bb;

    return-object v2

    .line 56
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v2, Lcom/google/android/apps/gmm/d;->X:I

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    sget v2, Lcom/google/android/apps/gmm/e;->U:I

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    sget v2, Lcom/google/android/apps/gmm/d;->W:I

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    sget v2, Lcom/google/android/apps/gmm/d;->ar:I

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    sget v2, Lcom/google/android/apps/gmm/e;->X:I

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    sget v2, Lcom/google/android/apps/gmm/d;->j:I

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    sget v2, Lcom/google/android/apps/gmm/e;->bA:I

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    sget v2, Lcom/google/android/apps/gmm/e;->W:I

    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    new-instance v15, Lcom/google/android/apps/gmm/directions/b;

    invoke-direct {v15}, Lcom/google/android/apps/gmm/directions/b;-><init>()V

    new-instance v16, Lcom/google/android/apps/gmm/directions/bd;

    invoke-direct/range {v16 .. v16}, Lcom/google/android/apps/gmm/directions/bd;-><init>()V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/bc;->b:Lcom/google/android/apps/gmm/map/r/a/h;

    iget v3, v3, Lcom/google/android/apps/gmm/map/r/a/h;->c:I

    int-to-float v3, v3

    iput v2, v15, Lcom/google/android/apps/gmm/directions/b;->b:F

    iput v3, v15, Lcom/google/android/apps/gmm/directions/b;->c:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->b:Lcom/google/android/apps/gmm/map/r/a/h;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/h;->b:I

    if-lez v2, :cond_5

    const/16 v3, 0xa

    if-ge v2, v3, :cond_5

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->b:Lcom/google/android/apps/gmm/map/r/a/h;

    iget v5, v2, Lcom/google/android/apps/gmm/map/r/a/h;->a:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->a:Lcom/google/maps/g/a/bi;

    iget-object v2, v2, Lcom/google/maps/g/a/bi;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/ai;

    iget v2, v2, Lcom/google/maps/g/a/ai;->d:I

    invoke-static {v2}, Lcom/google/maps/g/a/al;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/maps/g/a/al;->d:Lcom/google/maps/g/a/al;

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/bc;->d:Lcom/google/android/apps/gmm/shared/c/c/c;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/c/c/c;->b:Lcom/google/android/apps/gmm/shared/b/a;

    invoke-static {v4}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/maps/g/a/al;

    move-result-object v4

    if-eqz v4, :cond_6

    move-object v2, v4

    :cond_4
    :goto_2
    sget-object v4, Lcom/google/maps/g/a/al;->a:Lcom/google/maps/g/a/al;

    if-ne v2, v4, :cond_7

    const v2, 0x186a0

    :goto_3
    div-int/lit16 v2, v2, 0x3e8

    sub-int v4, v5, v3

    if-ge v4, v2, :cond_8

    add-int/2addr v2, v3

    :goto_4
    int-to-float v3, v3

    int-to-float v2, v2

    move-object/from16 v0, v16

    iput v3, v0, Lcom/google/android/apps/gmm/directions/b;->b:F

    move-object/from16 v0, v16

    iput v2, v0, Lcom/google/android/apps/gmm/directions/b;->c:F

    new-instance v2, Lcom/google/android/apps/gmm/directions/bb;

    move-object/from16 v0, v16

    invoke-direct {v2, v15, v0}, Lcom/google/android/apps/gmm/directions/bb;-><init>(Lcom/google/android/apps/gmm/directions/b;Lcom/google/android/apps/gmm/directions/b;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->c:Lcom/google/android/apps/gmm/directions/bb;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->c:Lcom/google/android/apps/gmm/directions/bb;

    iget-object v3, v2, Lcom/google/android/apps/gmm/directions/bb;->c:Landroid/graphics/Paint;

    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/directions/bb;->d:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/bb;->d:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->c:Lcom/google/android/apps/gmm/directions/bb;

    const/4 v3, -0x1

    iget-object v4, v2, Lcom/google/android/apps/gmm/directions/bb;->g:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/directions/bb;->f:Landroid/graphics/Paint;

    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/directions/bb;->f:Landroid/graphics/Paint;

    invoke-virtual {v3, v11}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iput v14, v2, Lcom/google/android/apps/gmm/directions/bb;->h:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->c:Lcom/google/android/apps/gmm/directions/bb;

    iget-object v3, v2, Lcom/google/android/apps/gmm/directions/bb;->e:Landroid/graphics/Paint;

    invoke-virtual {v3, v12}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/bb;->e:Landroid/graphics/Paint;

    invoke-virtual {v2, v13}, Landroid/graphics/Paint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->c:Lcom/google/android/apps/gmm/directions/bb;

    float-to-double v4, v14

    float-to-double v8, v11

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v8, v10

    add-double/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    const/4 v4, 0x0

    sget v5, Lcom/google/android/apps/gmm/e;->V:I

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sget v7, Lcom/google/android/apps/gmm/e;->T:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v3, v2, Lcom/google/android/apps/gmm/directions/bb;->j:I

    iput v4, v2, Lcom/google/android/apps/gmm/directions/bb;->k:I

    iput v5, v2, Lcom/google/android/apps/gmm/directions/bb;->l:I

    iput v6, v2, Lcom/google/android/apps/gmm/directions/bb;->m:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/bb;->a()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/bc;->c:Lcom/google/android/apps/gmm/directions/bb;

    invoke-static {}, Lcom/google/android/apps/gmm/util/r;->a()Z

    move-result v3

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/directions/bb;->p:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/bc;->c:Lcom/google/android/apps/gmm/directions/bb;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/bc;->a:Lcom/google/maps/g/a/bi;

    iget-boolean v2, v4, Lcom/google/maps/g/a/bi;->j:Z

    if-nez v2, :cond_9

    const/4 v2, 0x1

    :goto_5
    if-nez v2, :cond_a

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_5
    move v3, v2

    goto/16 :goto_1

    :cond_6
    if-nez v2, :cond_4

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/google/android/apps/gmm/shared/c/c/c;->c:Lcom/google/maps/g/a/al;

    goto/16 :goto_2

    :cond_7
    sget v2, Lcom/google/android/apps/gmm/shared/c/c/c;->a:I

    goto/16 :goto_3

    :cond_8
    move v2, v5

    goto/16 :goto_4

    :cond_9
    const/4 v2, 0x0

    goto :goto_5

    :cond_a
    iput-object v4, v3, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    iget-object v2, v2, Lcom/google/maps/g/a/bi;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/ai;

    invoke-virtual {v2}, Lcom/google/maps/g/a/ai;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    iget-object v2, v2, Lcom/google/maps/g/a/bi;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/ai;

    invoke-virtual {v2}, Lcom/google/maps/g/a/ai;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v5, v3, Lcom/google/android/apps/gmm/directions/bb;->e:Landroid/graphics/Paint;

    const/4 v6, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    iget-object v8, v3, Lcom/google/android/apps/gmm/directions/bb;->b:Landroid/graphics/Rect;

    invoke-virtual {v5, v4, v6, v7, v8}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget-object v4, v3, Lcom/google/android/apps/gmm/directions/bb;->e:Landroid/graphics/Paint;

    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    iget-object v7, v3, Lcom/google/android/apps/gmm/directions/bb;->a:Landroid/graphics/Rect;

    invoke-virtual {v4, v2, v5, v6, v7}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/directions/bb;->a()V

    goto/16 :goto_0
.end method
