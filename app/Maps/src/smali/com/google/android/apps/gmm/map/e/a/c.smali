.class public Lcom/google/android/apps/gmm/map/e/a/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/e/a/b;


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/e/a/a;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    const/4 v2, 0x0

    .line 107
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/e/a/a;->e:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/e/a/d;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/gmm/map/e/a/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/e/a/d;-><init>()V

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    :cond_0
    iget-byte v1, p1, Lcom/google/android/apps/gmm/map/e/a/a;->f:B

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 110
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/e/a/a;->b:J

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/e/a/d;->d:J

    .line 121
    :cond_1
    :goto_0
    return-void

    .line 111
    :cond_2
    iget-byte v1, p1, Lcom/google/android/apps/gmm/map/e/a/a;->f:B

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 112
    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/e/a/d;->b:J

    add-long/2addr v2, v6

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/e/a/d;->b:J

    goto :goto_0

    .line 113
    :cond_3
    iget-byte v1, p1, Lcom/google/android/apps/gmm/map/e/a/a;->f:B

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 115
    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/e/a/d;->d:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 116
    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/e/a/d;->c:J

    add-long/2addr v2, v6

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/e/a/d;->c:J

    .line 117
    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/e/a/d;->a:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/e/a/a;->b:J

    .line 118
    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/e/a/d;->d:J

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    .line 117
    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/e/a/d;->a:J

    goto :goto_0
.end method
