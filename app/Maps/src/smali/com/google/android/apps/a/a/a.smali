.class public abstract Lcom/google/android/apps/a/a/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/a/a/b;


# instance fields
.field public final a:J


# direct methods
.method protected constructor <init>()V
    .locals 6

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 30
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 31
    const-wide/32 v4, 0xf4240

    mul-long/2addr v0, v4

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/a/a/a;->a:J

    .line 32
    return-void
.end method


# virtual methods
.method protected abstract a(JLjava/lang/Iterable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;)V"
        }
    .end annotation
.end method

.method protected abstract a(JLjava/lang/String;Ljava/lang/String;)V
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/google/android/apps/a/a/a;->a:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-virtual {p0, v0, v1, p1}, Lcom/google/android/apps/a/a/a;->a(JLjava/lang/Iterable;)V

    .line 84
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/google/android/apps/a/a/a;->a:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/google/android/apps/a/a/a;->a(JLjava/lang/String;Ljava/lang/String;)V

    .line 44
    return-void
.end method
