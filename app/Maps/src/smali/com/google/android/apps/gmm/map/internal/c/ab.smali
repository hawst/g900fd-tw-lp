.class public Lcom/google/android/apps/gmm/map/internal/c/ab;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/bu;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/e/a/a/a/b;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/gmm/map/internal/c/ab;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ab;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/e/a/a/a/b;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->b:Lcom/google/e/a/a/a/b;

    .line 51
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->c:Ljava/lang/String;

    .line 54
    if-eqz p1, :cond_2

    .line 56
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    .line 57
    if-eqz p2, :cond_0

    .line 58
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    add-int/2addr v0, v1

    .line 63
    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    .line 64
    invoke-virtual {p1}, Lcom/google/e/a/a/a/b;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nAds Token: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->e:Ljava/lang/String;

    .line 73
    :goto_1
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->d:I

    .line 74
    return-void

    .line 60
    :catch_0
    move-exception v1

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    .line 61
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ab;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x38

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not calculate hash code of spotlight description: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_0

    .line 66
    :cond_1
    invoke-virtual {p1}, Lcom/google/e/a/a/a/b;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->e:Ljava/lang/String;

    goto :goto_1

    .line 70
    :cond_2
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/ab;->a:Ljava/lang/String;

    const-string v2, "spotlight description is null"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    const-string v1, ""

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->e:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bv;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->a:Lcom/google/android/apps/gmm/map/internal/c/bv;

    return-object v0
.end method

.method public final a(Lcom/google/e/a/a/a/b;)V
    .locals 3

    .prologue
    .line 154
    const/16 v0, 0x1b

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->b:Lcom/google/e/a/a/a/b;

    iget-object v2, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 155
    const/16 v0, 0x1c

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 156
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ai;)Z
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->x:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->b:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/c/ab;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 24
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/bu;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-ne p0, p1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/ab;

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->d:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/ab;->d:I

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->d:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/ab;->d:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/ab;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127
    if-ne p0, p1, :cond_1

    .line 144
    :cond_0
    :goto_0
    return v0

    .line 130
    :cond_1
    if-nez p1, :cond_2

    .line 131
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->b:Lcom/google/e/a/a/a/b;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 133
    :cond_2
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ab;

    if-nez v0, :cond_3

    move v0, v1

    .line 134
    goto :goto_0

    .line 137
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/ab;

    .line 140
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->d:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/c/ab;->d:I

    if-eq v0, v2, :cond_4

    move v0, v1

    .line 141
    goto :goto_0

    .line 144
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/ab;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->d:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ab;->e:Ljava/lang/String;

    return-object v0
.end method
