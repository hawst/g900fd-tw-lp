.class public Lcom/google/android/apps/gmm/car/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/activities/o;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/a;

.field private final b:Lcom/google/android/apps/gmm/map/t;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 21
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 22
    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/d;->a:Lcom/google/android/apps/gmm/base/a;

    .line 24
    iput-object p2, p0, Lcom/google/android/apps/gmm/car/d;->b:Lcom/google/android/apps/gmm/map/t;

    .line 25
    iput-object p3, p0, Lcom/google/android/apps/gmm/car/d;->c:Landroid/content/res/Resources;

    .line 26
    return-void
.end method


# virtual methods
.method public final e()Lcom/google/android/apps/gmm/map/t;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d;->b:Lcom/google/android/apps/gmm/map/t;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/base/a;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d;->a:Lcom/google/android/apps/gmm/base/a;

    return-object v0
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d;->c:Landroid/content/res/Resources;

    return-object v0
.end method
