.class public Lcom/google/android/apps/gmm/z/b/m;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Lcom/google/b/f/cq;

.field d:Lcom/google/b/f/b/a/dl;

.field public e:Lcom/google/r/b/a/tf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    invoke-static {}, Lcom/google/b/f/b/a/dj;->newBuilder()Lcom/google/b/f/b/a/dl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/b/m;->d:Lcom/google/b/f/b/a/dl;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/z/b/l;
    .locals 6

    .prologue
    .line 226
    new-instance v0, Lcom/google/android/apps/gmm/z/b/l;

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v4, p0, Lcom/google/android/apps/gmm/z/b/m;->d:Lcom/google/b/f/b/a/dl;

    .line 227
    invoke-virtual {v4}, Lcom/google/b/f/b/a/dl;->g()Lcom/google/n/t;

    move-result-object v4

    check-cast v4, Lcom/google/b/f/b/a/dj;

    iget-object v5, p0, Lcom/google/android/apps/gmm/z/b/m;->e:Lcom/google/r/b/a/tf;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/z/b/l;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/google/b/f/cq;Lcom/google/b/f/b/a/dj;Lcom/google/r/b/a/tf;)V

    return-object v0
.end method

.method public final a(Lcom/google/b/f/b/a/ck;)Lcom/google/android/apps/gmm/z/b/m;
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/m;->d:Lcom/google/b/f/b/a/dl;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, v0, Lcom/google/b/f/b/a/dl;->b:Lcom/google/b/f/b/a/ck;

    iget v1, v0, Lcom/google/b/f/b/a/dl;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/b/f/b/a/dl;->a:I

    .line 263
    return-object p0
.end method

.method public final a(Lcom/google/r/b/a/acm;)Lcom/google/android/apps/gmm/z/b/m;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 267
    iget v2, p1, Lcom/google/r/b/a/acm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_0
    if-eqz v2, :cond_0

    .line 268
    invoke-virtual {p1}, Lcom/google/r/b/a/acm;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    .line 270
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/acm;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    :goto_1
    if-eqz v0, :cond_1

    .line 271
    invoke-virtual {p1}, Lcom/google/r/b/a/acm;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 273
    :cond_1
    return-object p0

    :cond_2
    move v2, v1

    .line 267
    goto :goto_0

    :cond_3
    move v0, v1

    .line 270
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 255
    if-eqz p1, :cond_1

    .line 256
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/m;->d:Lcom/google/b/f/b/a/dl;

    invoke-static {}, Lcom/google/b/f/b/a/a;->newBuilder()Lcom/google/b/f/b/a/c;

    move-result-object v0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/b/f/b/a/c;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/b/f/b/a/c;->a:I

    iput-object p1, v0, Lcom/google/b/f/b/a/c;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/c;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/a;

    iput-object v0, v1, Lcom/google/b/f/b/a/dl;->c:Lcom/google/b/f/b/a/a;

    iget v0, v1, Lcom/google/b/f/b/a/dl;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v1, Lcom/google/b/f/b/a/dl;->a:I

    .line 258
    :cond_1
    return-object p0
.end method
