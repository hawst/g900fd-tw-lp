.class public Lcom/google/android/apps/gmm/car/e/v;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# instance fields
.field final a:Lcom/google/android/apps/gmm/car/ad;

.field private final b:Lcom/google/android/apps/gmm/car/e/i;

.field private final c:Lcom/google/android/apps/gmm/car/ax;

.field private final d:Lcom/google/android/apps/gmm/z/b/j;

.field private e:Landroid/view/View;

.field private f:Lcom/google/android/libraries/curvular/ce;

.field private final g:Lcom/google/android/apps/gmm/navigation/commonui/c/d;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/e/i;)V
    .locals 5

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lcom/google/android/apps/gmm/z/b/j;

    sget-object v1, Lcom/google/b/f/t;->ac:Lcom/google/b/f/t;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/b/j;-><init>(Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/v;->d:Lcom/google/android/apps/gmm/z/b/j;

    .line 78
    new-instance v0, Lcom/google/android/apps/gmm/car/e/w;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/w;-><init>(Lcom/google/android/apps/gmm/car/e/v;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/v;->g:Lcom/google/android/apps/gmm/navigation/commonui/c/d;

    .line 34
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/car/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/v;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 35
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/car/e/i;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/e/v;->b:Lcom/google/android/apps/gmm/car/e/i;

    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/car/ax;

    .line 38
    iget-object v1, p1, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    .line 39
    iget-object v2, p1, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    .line 40
    iget-object v3, p1, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    sget v4, Lcom/google/android/apps/gmm/e;->r:I

    iget-object v3, v3, Lcom/google/android/apps/gmm/car/v;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/car/ax;-><init>(Landroid/graphics/Point;Lcom/google/android/apps/gmm/car/v;I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/v;->c:Lcom/google/android/apps/gmm/car/ax;

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/v;->b:Lcom/google/android/apps/gmm/car/e/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/v;->c:Lcom/google/android/apps/gmm/car/ax;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/e/i;->a(Lcom/google/android/apps/gmm/car/ax;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/v;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->s:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/v;->c:Lcom/google/android/apps/gmm/car/ax;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/d/a;->l:Lcom/google/android/apps/gmm/car/ax;

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/v;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/v;->d:Lcom/google/android/apps/gmm/z/b/j;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/o;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/v;->e:Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 6

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/v;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/car/e/u;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/v;->e:Landroid/view/View;

    .line 46
    new-instance v0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/v;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 47
    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->m:Lcom/google/android/apps/gmm/o/a/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/v;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 48
    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/v;->g:Lcom/google/android/apps/gmm/navigation/commonui/c/d;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/e/v;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 50
    iget-boolean v4, v4, Lcom/google/android/apps/gmm/car/ad;->o:Z

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/commonui/c/c;-><init>(Lcom/google/android/apps/gmm/o/a/c;Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/navigation/commonui/c/d;ZZ)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/v;->f:Lcom/google/android/libraries/curvular/ce;

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/v;->e:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/v;->f:Lcom/google/android/libraries/curvular/ce;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 53
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 69
    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/v;->f:Lcom/google/android/libraries/curvular/ce;

    .line 70
    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/v;->e:Landroid/view/View;

    .line 71
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/google/android/apps/gmm/car/m/e;->b:Lcom/google/android/apps/gmm/car/m/e;

    return-object v0
.end method
