.class public Lcom/google/android/apps/gmm/cardui/a/ac;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 26
    .line 27
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->t:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/qb;->d()Lcom/google/o/h/a/qb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/qb;

    .line 28
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29
    iget v1, v0, Lcom/google/o/h/a/qb;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_1

    move v1, v4

    :goto_0
    if-eqz v1, :cond_2

    .line 30
    iget-object v1, v0, Lcom/google/o/h/a/qb;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/a/a;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/maps/a/a;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v1

    move-object v2, v1

    .line 32
    :goto_1
    iget v1, v0, Lcom/google/o/h/a/qb;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v6, 0x2

    if-ne v1, v6, :cond_3

    move v1, v4

    :goto_2
    if-eqz v1, :cond_6

    iget-object v1, v0, Lcom/google/o/h/a/qb;->c:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_4

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    .line 33
    :goto_3
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->u()Lcom/google/android/apps/gmm/prefetchcache/api/a;

    move-result-object v1

    .line 34
    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/prefetchcache/api/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;Ljava/lang/String;)V

    .line 36
    :cond_0
    return-void

    :cond_1
    move v1, v5

    .line 29
    goto :goto_0

    :cond_2
    move-object v2, v3

    .line 30
    goto :goto_1

    :cond_3
    move v1, v5

    .line 32
    goto :goto_2

    :cond_4
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    iput-object v3, v0, Lcom/google/o/h/a/qb;->c:Ljava/lang/Object;

    :cond_5
    move-object v0, v3

    goto :goto_3

    :cond_6
    move-object v0, v3

    goto :goto_3
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    sget-object v0, Lcom/google/o/h/a/g;->v:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 22
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    const/high16 v1, 0x40000

    .line 16
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
