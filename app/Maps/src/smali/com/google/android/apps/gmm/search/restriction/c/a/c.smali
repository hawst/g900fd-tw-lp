.class public Lcom/google/android/apps/gmm/search/restriction/c/a/c;
.super Lcom/google/android/apps/gmm/search/restriction/c/a/a;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/restriction/d/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V
    .locals 3

    .prologue
    .line 23
    sget v0, Lcom/google/android/apps/gmm/l;->lS:I

    .line 24
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->lS:I

    .line 25
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v1

    const/4 v2, 0x0

    .line 23
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/gmm/search/restriction/c/a/a;-><init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/bi;Lcom/google/android/libraries/curvular/bi;Lcom/google/b/f/t;)V

    .line 27
    new-instance v0, Lcom/google/n/ai;

    iget-object v1, p3, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/alh;->l:Lcom/google/n/aj;

    invoke-direct {v0, v1, v2}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    sget-object v1, Lcom/google/maps/g/qj;->c:Lcom/google/maps/g/qj;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->b:Ljava/lang/Boolean;

    .line 28
    iget v0, p2, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->c:Ljava/lang/Boolean;

    .line 29
    return-void

    .line 28
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/qo;)Lcom/google/maps/g/qo;
    .locals 3

    .prologue
    .line 47
    invoke-static {}, Lcom/google/maps/g/ro;->d()Lcom/google/maps/g/ro;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p1, Lcom/google/maps/g/qo;->g:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/maps/g/qo;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p1, Lcom/google/maps/g/qo;->a:I

    return-object p1
.end method

.method public final a(Lcom/google/r/b/a/alm;)Lcom/google/r/b/a/alm;
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/restriction/c/a/c;->ai_()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 40
    sget-object v0, Lcom/google/maps/g/qj;->c:Lcom/google/maps/g/qj;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/r/b/a/alm;->c()V

    iget-object v1, p1, Lcom/google/r/b/a/alm;->c:Ljava/util/List;

    iget v0, v0, Lcom/google/maps/g/qj;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    :cond_1
    return-object p1
.end method

.method public final a(Lcom/google/r/b/a/alh;)V
    .locals 3

    .prologue
    .line 33
    new-instance v0, Lcom/google/n/ai;

    iget-object v1, p1, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/alh;->l:Lcom/google/n/aj;

    invoke-direct {v0, v1, v2}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    sget-object v1, Lcom/google/maps/g/qj;->c:Lcom/google/maps/g/qj;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->b:Ljava/lang/Boolean;

    .line 35
    return-void
.end method
