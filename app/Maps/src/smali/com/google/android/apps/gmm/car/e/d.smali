.class public Lcom/google/android/apps/gmm/car/e/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# instance fields
.field final a:Lcom/google/android/apps/gmm/car/ad;

.field b:Lcom/google/android/libraries/curvular/ae;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/navigation/f/c/a;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/android/apps/gmm/navigation/f/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final d:Lcom/google/android/apps/gmm/car/bi;

.field private final e:Lcom/google/android/apps/gmm/car/d/q;

.field private final f:Lcom/google/android/apps/gmm/z/b/o;

.field private g:Lcom/google/android/apps/gmm/car/d/m;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private h:Landroid/view/View;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final i:Lcom/google/android/apps/gmm/car/bj;

.field private final j:Lcom/google/android/apps/gmm/navigation/f/q;

.field private final k:Lcom/google/android/libraries/curvular/ag;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/navigation/f/c/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/d/q;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/google/android/apps/gmm/z/b/j;

    sget-object v1, Lcom/google/b/f/t;->ab:Lcom/google/b/f/t;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/b/j;-><init>(Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->f:Lcom/google/android/apps/gmm/z/b/o;

    .line 136
    new-instance v0, Lcom/google/android/apps/gmm/car/e/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/f;-><init>(Lcom/google/android/apps/gmm/car/e/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->i:Lcom/google/android/apps/gmm/car/bj;

    .line 149
    new-instance v0, Lcom/google/android/apps/gmm/car/bi;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/d;->i:Lcom/google/android/apps/gmm/car/bj;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/car/bi;-><init>(Lcom/google/android/apps/gmm/car/bj;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->d:Lcom/google/android/apps/gmm/car/bi;

    .line 152
    new-instance v0, Lcom/google/android/apps/gmm/car/e/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/g;-><init>(Lcom/google/android/apps/gmm/car/e/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->j:Lcom/google/android/apps/gmm/navigation/f/q;

    .line 189
    new-instance v0, Lcom/google/android/apps/gmm/car/e/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/h;-><init>(Lcom/google/android/apps/gmm/car/e/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->k:Lcom/google/android/libraries/curvular/ag;

    .line 46
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/car/ad;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 48
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/car/d/q;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/e/d;->e:Lcom/google/android/apps/gmm/car/d/q;

    .line 49
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->d:Lcom/google/android/apps/gmm/car/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bi;->c()V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->h:Lcom/google/android/apps/gmm/car/at;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/car/at;->a(Z)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->e:Lcom/google/android/apps/gmm/car/d/q;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/d/q;->e:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/d/q;->a()V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->g:Lcom/google/android/apps/gmm/car/d/m;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/d/m;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/m;->d:Ljava/lang/Object;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->s:Lcom/google/android/apps/gmm/car/d/a;

    new-instance v1, Lcom/google/android/apps/gmm/car/e/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/e/e;-><init>(Lcom/google/android/apps/gmm/car/e/d;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/d/a;->k:Lcom/google/android/apps/gmm/car/d/i;

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/ao;->e:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->c()V

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/d;->f:Lcom/google/android/apps/gmm/z/b/o;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/o;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->h:Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 17

    .prologue
    .line 53
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/e/d;->d:Lcom/google/android/apps/gmm/car/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/bi;->a()V

    .line 55
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/car/e/c;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/e/d;->b:Lcom/google/android/libraries/curvular/ae;

    .line 56
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/e/d;->b:Lcom/google/android/libraries/curvular/ae;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/e/d;->h:Landroid/view/View;

    .line 58
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 59
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/car/e/d;->j:Lcom/google/android/apps/gmm/navigation/f/q;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 62
    iget-object v14, v1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 63
    iget-object v5, v1, Lcom/google/android/apps/gmm/car/ad;->i:Lcom/google/android/apps/gmm/mylocation/b/f;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 64
    iget-object v4, v1, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/car/e/d;->k:Lcom/google/android/libraries/curvular/ag;

    .line 59
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v14}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    new-instance v9, Lcom/google/android/apps/gmm/navigation/f/s;

    invoke-direct {v9, v13, v2}, Lcom/google/android/apps/gmm/navigation/f/s;-><init>(Lcom/google/android/apps/gmm/navigation/f/q;Lcom/google/android/apps/gmm/map/util/b/g;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v6, Lcom/google/android/apps/gmm/navigation/f/a;

    invoke-direct {v6, v13, v14}, Lcom/google/android/apps/gmm/navigation/f/a;-><init>(Lcom/google/android/apps/gmm/navigation/f/q;Lcom/google/android/apps/gmm/base/a;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/gmm/navigation/c/f;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v7, 0x0

    invoke-interface {v14}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/gmm/navigation/c/f;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/mylocation/b/f;Lcom/google/android/apps/gmm/navigation/d/a;ZLcom/google/android/apps/gmm/shared/net/a/b;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/google/android/apps/gmm/navigation/f/i;

    move-object v8, v13

    move-object v10, v2

    move-object v11, v1

    move-object v12, v4

    invoke-direct/range {v7 .. v12}, Lcom/google/android/apps/gmm/navigation/f/i;-><init>(Lcom/google/android/apps/gmm/navigation/f/q;Lcom/google/android/apps/gmm/navigation/f/r;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/navigation/c/f;Lcom/google/android/apps/gmm/map/t;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/gmm/navigation/f/c/c;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/navigation/f/c/c;-><init>()V

    iput-object v13, v2, Lcom/google/android/apps/gmm/navigation/f/c/c;->a:Lcom/google/android/apps/gmm/navigation/f/q;

    iput-object v9, v2, Lcom/google/android/apps/gmm/navigation/f/c/c;->b:Lcom/google/android/apps/gmm/navigation/f/r;

    iput-object v14, v2, Lcom/google/android/apps/gmm/navigation/f/c/c;->c:Lcom/google/android/apps/gmm/base/a;

    iput-object v15, v2, Lcom/google/android/apps/gmm/navigation/f/c/c;->e:Lcom/google/android/libraries/curvular/ag;

    new-instance v13, Lcom/google/android/apps/gmm/navigation/f/c/b;

    invoke-direct {v13, v2}, Lcom/google/android/apps/gmm/navigation/f/c/b;-><init>(Lcom/google/android/apps/gmm/navigation/f/c/c;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/google/android/apps/gmm/navigation/f/b;

    const/4 v10, 0x0

    move-object/from16 v8, v16

    move-object v11, v6

    move-object v12, v1

    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/gmm/navigation/f/b;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/navigation/f/s;Lcom/google/android/apps/gmm/navigation/f/l;Lcom/google/android/apps/gmm/navigation/f/a;Lcom/google/android/apps/gmm/navigation/c/f;Lcom/google/android/apps/gmm/navigation/f/c/b;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/apps/gmm/car/e/d;->c:Lcom/google/android/apps/gmm/navigation/f/b;

    .line 66
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/e/d;->c:Lcom/google/android/apps/gmm/navigation/f/b;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/navigation/f/b;->a(Landroid/os/Bundle;)V

    .line 68
    new-instance v1, Lcom/google/android/apps/gmm/car/d/m;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 69
    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/car/f/ak;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/car/f/ak;-><init>(Lcom/google/android/apps/gmm/car/ad;)V

    new-instance v4, Lcom/google/android/apps/gmm/car/l/a;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    invoke-direct {v4, v5}, Lcom/google/android/apps/gmm/car/l/a;-><init>(Lcom/google/android/apps/gmm/car/ad;)V

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/car/d/m;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/car/d/o;Lcom/google/android/apps/gmm/car/d/p;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/e/d;->g:Lcom/google/android/apps/gmm/car/d/m;

    .line 72
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/ao;->e:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->c()V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->s:Lcom/google/android/apps/gmm/car/d/a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/d/a;->k:Lcom/google/android/apps/gmm/car/d/i;

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->g:Lcom/google/android/apps/gmm/car/d/m;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/d/m;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/m;->d:Ljava/lang/Object;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->e:Lcom/google/android/apps/gmm/car/d/q;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/d/q;->e:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/d/q;->a()V

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->h:Lcom/google/android/apps/gmm/car/at;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/car/at;->a(Z)V

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->d:Lcom/google/android/apps/gmm/car/bi;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/car/bi;->b:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/bi;->b:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bi;->d()V

    .line 112
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 116
    iput-object v1, p0, Lcom/google/android/apps/gmm/car/e/d;->g:Lcom/google/android/apps/gmm/car/d/m;

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->c:Lcom/google/android/apps/gmm/navigation/f/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/f/b;->g()V

    .line 118
    iput-object v1, p0, Lcom/google/android/apps/gmm/car/e/d;->c:Lcom/google/android/apps/gmm/navigation/f/b;

    .line 119
    iput-object v1, p0, Lcom/google/android/apps/gmm/car/e/d;->b:Lcom/google/android/libraries/curvular/ae;

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/d;->d:Lcom/google/android/apps/gmm/car/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bi;->b()V

    .line 121
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/google/android/apps/gmm/car/m/e;->c:Lcom/google/android/apps/gmm/car/m/e;

    return-object v0
.end method
