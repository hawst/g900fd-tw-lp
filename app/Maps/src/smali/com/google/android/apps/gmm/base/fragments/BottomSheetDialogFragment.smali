.class public abstract Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"


# instance fields
.field private c:Landroid/view/View;

.field private d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Landroid/view/View;
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 29
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 31
    sget v2, Lcom/google/android/apps/gmm/d;->N:I

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 32
    invoke-virtual {v1, v3, v3}, Landroid/view/Window;->setLayout(II)V

    .line 33
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 36
    const v2, 0x1030004

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 37
    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->c:Landroid/view/View;

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    new-instance v1, Lcom/google/android/apps/gmm/base/fragments/a;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/base/fragments/a;-><init>(Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->o:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    return-object v0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 65
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onResume()V

    .line 66
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->c:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setContent(Landroid/view/View;Landroid/view/View;)V

    .line 68
    invoke-static {}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->values()[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v1

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "expandedState"

    sget-object v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    aget-object v1, v1, v2

    .line 72
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingState(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Z)V

    .line 73
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 73
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 75
    return-void

    .line 74
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "expandedState"

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    return-void
.end method
