.class Lcom/google/android/apps/gmm/prefetch/v;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/a/c;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/prefetch/k;

.field private final b:I

.field private final c:Lcom/google/android/apps/gmm/prefetch/a/c;

.field private final d:Lcom/google/android/apps/gmm/prefetch/b;

.field private final e:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private final h:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/prefetch/k;Lcom/google/android/apps/gmm/prefetch/f;Lcom/google/android/apps/gmm/prefetch/b;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetch/v;->a:Lcom/google/android/apps/gmm/prefetch/k;

    .line 36
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->h:Ljava/util/Queue;

    .line 37
    iget-object v0, p2, Lcom/google/android/apps/gmm/prefetch/f;->a:Ljava/util/Queue;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->e:Ljava/util/Queue;

    .line 38
    iget-object v0, p2, Lcom/google/android/apps/gmm/prefetch/f;->b:Ljava/util/Queue;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->f:Ljava/util/Queue;

    .line 39
    iget-object v0, p2, Lcom/google/android/apps/gmm/prefetch/f;->c:Lcom/google/android/apps/gmm/prefetch/a/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->c:Lcom/google/android/apps/gmm/prefetch/a/c;

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->g:I

    .line 41
    iget v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->g:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetch/v;->f:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v1

    :cond_0
    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->g:I

    .line 42
    iget v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->g:I

    iput v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->b:I

    .line 43
    iput-object p3, p0, Lcom/google/android/apps/gmm/prefetch/v;->d:Lcom/google/android/apps/gmm/prefetch/b;

    .line 44
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/prefetch/v;->i:Z

    .line 45
    return-void

    :cond_1
    move v0, v2

    .line 40
    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->h:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/v;->f:Ljava/util/Queue;

    .line 100
    const/4 v0, 0x1

    .line 105
    :goto_0
    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 106
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/v;->h:Ljava/util/Queue;

    invoke-interface {v2, v1}, Ljava/util/Queue;->addAll(Ljava/util/Collection;)Z

    .line 107
    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    .line 108
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/v;->d:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/prefetch/b;->f()Lcom/google/android/apps/gmm/map/internal/d/be;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/v;->h:Ljava/util/Queue;

    invoke-static {v2}, Lcom/google/b/c/es;->b(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/c;->c:Lcom/google/android/apps/gmm/map/internal/d/c;

    invoke-virtual {v1, v2, v3, p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/be;->a(Ljava/util/Queue;Lcom/google/android/apps/gmm/map/internal/d/c;Lcom/google/android/apps/gmm/map/internal/d/a/c;Z)V

    .line 111
    :cond_0
    return-void

    .line 102
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/v;->e:Ljava/util/Queue;

    .line 103
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 57
    :goto_0
    monitor-exit p0

    return-void

    .line 54
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->i:Z

    .line 56
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetch/v;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "I",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 92
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 67
    :cond_1
    const/4 v0, 0x6

    if-ne p2, v0, :cond_3

    .line 68
    :try_start_1
    iget v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->g:I

    .line 69
    iget v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->g:I

    if-lez v0, :cond_2

    .line 71
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetch/v;->b()V

    .line 84
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->c:Lcom/google/android/apps/gmm/prefetch/a/c;

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/d;->b:Lcom/google/android/apps/gmm/prefetch/a/d;

    iget v2, p0, Lcom/google/android/apps/gmm/prefetch/v;->b:I

    iget v3, p0, Lcom/google/android/apps/gmm/prefetch/v;->g:I

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/prefetch/a/c;->a(Lcom/google/android/apps/gmm/prefetch/a/d;II)V

    .line 86
    iget v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->g:I

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->d:Lcom/google/android/apps/gmm/prefetch/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/v;->a:Lcom/google/android/apps/gmm/prefetch/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->c:Lcom/google/android/apps/gmm/prefetch/a/c;

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/d;->b:Lcom/google/android/apps/gmm/prefetch/a/d;

    sget-object v2, Lcom/google/android/apps/gmm/prefetch/a/e;->b:Lcom/google/android/apps/gmm/prefetch/a/e;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/prefetch/a/c;->a(Lcom/google/android/apps/gmm/prefetch/a/d;Lcom/google/android/apps/gmm/prefetch/a/e;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 73
    :cond_3
    if-eqz p2, :cond_2

    const/4 v0, 0x2

    if-eq p2, v0, :cond_2

    .line 76
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->d:Lcom/google/android/apps/gmm/prefetch/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/v;->a:Lcom/google/android/apps/gmm/prefetch/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/prefetch/v;->g:I

    .line 78
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/v;->c:Lcom/google/android/apps/gmm/prefetch/a/c;

    sget-object v2, Lcom/google/android/apps/gmm/prefetch/a/d;->b:Lcom/google/android/apps/gmm/prefetch/a/d;

    const/4 v0, 0x1

    if-ne p2, v0, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->c:Lcom/google/android/apps/gmm/prefetch/a/e;

    :goto_2
    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/prefetch/a/c;->a(Lcom/google/android/apps/gmm/prefetch/a/d;Lcom/google/android/apps/gmm/prefetch/a/e;)V

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->d:Lcom/google/android/apps/gmm/prefetch/a/e;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method
