.class Lcom/google/android/apps/gmm/base/views/aw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/views/MultiColumnListView;)V
    .locals 1

    .prologue
    .line 750
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/aw;->b:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 751
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/aw;->a:Landroid/util/SparseArray;

    return-void
.end method

.method private b(I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 754
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/aw;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 755
    if-nez v0, :cond_0

    .line 756
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 757
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/aw;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 759
    :cond_0
    return-object v0
.end method


# virtual methods
.method final a(I)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 767
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    .line 775
    :cond_0
    :goto_0
    return-object v0

    .line 770
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/aw;->b(I)Ljava/util/List;

    move-result-object v1

    .line 771
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 774
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method

.method final a(ILandroid/view/View;)V
    .locals 2

    .prologue
    .line 781
    const/4 v0, -0x1

    if-eq p1, v0, :cond_3

    .line 782
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/aw;->b(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 783
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    .line 787
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 788
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 789
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 801
    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 802
    instance-of v1, v0, Lcom/google/android/apps/gmm/base/views/ap;

    if-eqz v1, :cond_1

    .line 803
    check-cast v0, Lcom/google/android/apps/gmm/base/views/ap;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/ap;->a:Lcom/google/android/apps/gmm/base/views/ar;

    .line 805
    :cond_1
    return-void

    .line 793
    :cond_2
    const v0, 0x461c4000    # 10000.0f

    invoke-virtual {p2, v0}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0

    .line 796
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 797
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 798
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method
