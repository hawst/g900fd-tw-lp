.class public Lcom/google/android/apps/gmm/prefetchcache/t;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/prefetchcache/api/c;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:J

.field private static final c:J


# instance fields
.field private final d:Lcom/google/r/b/a/a/af;

.field private final e:Lcom/google/android/apps/gmm/map/c/a;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/gmm/prefetchcache/t;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/prefetchcache/t;->a:Ljava/lang/String;

    .line 25
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/prefetchcache/t;->b:J

    .line 28
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x19

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/prefetchcache/t;->c:J

    return-void
.end method

.method public constructor <init>(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->f:I

    .line 37
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->d:Lcom/google/r/b/a/a/af;

    .line 38
    iput-object p2, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->e:Lcom/google/android/apps/gmm/map/c/a;

    .line 39
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/r/b/a/a/af;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->d:Lcom/google/r/b/a/a/af;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->d:Lcom/google/r/b/a/a/af;

    invoke-virtual {v0}, Lcom/google/r/b/a/a/af;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->f:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->f:I

    .line 73
    :goto_0
    if-gez v0, :cond_2

    .line 74
    const/4 v0, 0x0

    .line 77
    :goto_1
    return-object v0

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->e:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->C_()Lcom/google/android/apps/gmm/map/internal/c/cw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/cw;->b()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/t;->a:Ljava/lang/String;

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->d:Lcom/google/r/b/a/a/af;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->d:Lcom/google/r/b/a/a/af;

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->e:Lcom/google/android/apps/gmm/map/c/a;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/c/a;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/internal/c/cv;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->f:I

    iget v0, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->f:I

    goto :goto_0

    .line 76
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->e:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/j;->A:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 77
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->d:Lcom/google/r/b/a/a/af;

    iget v0, v0, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v0}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_0
    sget-object v1, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/t;->e()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 87
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->d:Lcom/google/r/b/a/a/af;

    iget v2, v2, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 88
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/t;->a:Ljava/lang/String;

    const-string v2, "Downloaded date missing"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 87
    goto :goto_0

    .line 92
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->d:Lcom/google/r/b/a/a/af;

    iget-wide v2, v2, Lcom/google/r/b/a/a/af;->f:J

    sget-wide v4, Lcom/google/android/apps/gmm/prefetchcache/t;->b:J

    add-long/2addr v2, v4

    .line 93
    iget-object v4, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->e:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_1
.end method

.method public final f()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 98
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->d:Lcom/google/r/b/a/a/af;

    iget v2, v2, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 99
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/t;->a:Ljava/lang/String;

    const-string v2, "Downloaded date missing"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 98
    goto :goto_0

    .line 102
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->d:Lcom/google/r/b/a/a/af;

    iget-wide v2, v2, Lcom/google/r/b/a/a/af;->f:J

    sget-wide v4, Lcom/google/android/apps/gmm/prefetchcache/t;->c:J

    add-long/2addr v2, v4

    .line 103
    iget-object v4, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->e:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_1
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->d:Lcom/google/r/b/a/a/af;

    iget-boolean v0, v0, Lcom/google/r/b/a/a/af;->g:Z

    return v0
.end method

.method public final h()Lcom/google/android/apps/gmm/prefetchcache/api/c;
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->d:Lcom/google/r/b/a/a/af;

    invoke-static {}, Lcom/google/r/b/a/a/af;->newBuilder()Lcom/google/r/b/a/a/ah;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/a/ah;->a(Lcom/google/r/b/a/a/af;)Lcom/google/r/b/a/a/ah;

    move-result-object v0

    const/4 v1, 0x1

    .line 124
    iget v2, v0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, v0, Lcom/google/r/b/a/a/ah;->a:I

    iput-boolean v1, v0, Lcom/google/r/b/a/a/ah;->f:Z

    .line 125
    invoke-virtual {v0}, Lcom/google/r/b/a/a/ah;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    .line 126
    new-instance v1, Lcom/google/android/apps/gmm/prefetchcache/t;

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->e:Lcom/google/android/apps/gmm/map/c/a;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/prefetchcache/t;-><init>(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/c/a;)V

    .line 127
    return-object v1
.end method

.method public final i()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->e:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 133
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->d:Lcom/google/r/b/a/a/af;

    iget-wide v2, v1, Lcom/google/r/b/a/a/af;->f:J

    sget-wide v4, Lcom/google/android/apps/gmm/prefetchcache/t;->b:J

    add-long/2addr v2, v4

    .line 134
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->e:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 135
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 137
    sget v1, Lcom/google/android/apps/gmm/l;->ja:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 142
    :goto_0
    return-object v0

    .line 140
    :cond_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v2

    long-to-int v1, v2

    add-int/lit8 v1, v1, 0x1

    .line 141
    sget v2, Lcom/google/android/apps/gmm/j;->c:I

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 142
    sget v2, Lcom/google/android/apps/gmm/l;->jb:I

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic j()Lcom/google/android/apps/gmm/prefetchcache/api/c;
    .locals 3

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->d:Lcom/google/r/b/a/a/af;

    invoke-static {}, Lcom/google/r/b/a/a/af;->newBuilder()Lcom/google/r/b/a/a/ah;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/a/ah;->a(Lcom/google/r/b/a/a/af;)Lcom/google/r/b/a/a/ah;

    move-result-object v0

    sget-object v1, Lcom/google/r/b/a/a/ai;->e:Lcom/google/r/b/a/a/ai;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/r/b/a/a/ah;->a:I

    iget v1, v1, Lcom/google/r/b/a/a/ai;->f:I

    iput v1, v0, Lcom/google/r/b/a/a/ah;->d:I

    invoke-virtual {v0}, Lcom/google/r/b/a/a/ah;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    new-instance v1, Lcom/google/android/apps/gmm/prefetchcache/t;

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/t;->e:Lcom/google/android/apps/gmm/map/c/a;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/prefetchcache/t;-><init>(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/c/a;)V

    const/4 v0, 0x0

    iput v0, v1, Lcom/google/android/apps/gmm/prefetchcache/t;->f:I

    return-object v1
.end method
