.class public Lcom/google/android/apps/gmm/map/internal/c/ah;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:[B


# instance fields
.field private b:Lcom/google/e/a/a/a/b;

.field private c:Lcom/google/e/a/a/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ah;->a:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x4ct
        0x54t
        0x49t
        0x50t
        0xat
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a()Lcom/google/e/a/a/a/b;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 70
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ah;->c:Lcom/google/e/a/a/a/b;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ah;->b:Lcom/google/e/a/a/a/b;

    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ah;->b:Lcom/google/e/a/a/a/b;

    .line 71
    iget-object v2, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_3

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {v3, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-eqz v0, :cond_2

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ah;->b:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x1a

    invoke-virtual {v0, v4, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ah;->c:Lcom/google/e/a/a/a/b;

    .line 74
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ah;->c:Lcom/google/e/a/a/a/b;

    return-object v0

    :cond_3
    move v2, v0

    .line 71
    goto :goto_0
.end method

.method public final a([BI)[B
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 47
    .line 51
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/c/ah;->a:[B

    array-length v0, v4

    if-ge p2, v0, :cond_2

    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 53
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/ah;->a:[B

    const/4 v0, 0x4

    new-array v0, v0, [B

    const/4 v1, 0x5

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {p1, v1, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v0}, Lcom/google/b/h/a;->a([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    new-instance v1, Ljava/io/ByteArrayInputStream;

    const/16 v4, 0x9

    invoke-direct {v1, p1, v4, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    if-gez v0, :cond_5

    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    :goto_1
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v4, Lcom/google/r/b/a/b/bg;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v4}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ah;->b:Lcom/google/e/a/a/a/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ah;->b:Lcom/google/e/a/a/a/b;

    const v4, 0x7fffffff

    const/4 v5, 0x1

    new-instance v6, Lcom/google/e/a/a/a/c;

    invoke-direct {v6}, Lcom/google/e/a/a/a/c;-><init>()V

    invoke-virtual {v1, v0, v4, v5, v6}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;IZLcom/google/e/a/a/a/c;)I

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ah;->c:Lcom/google/e/a/a/a/b;

    .line 55
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/ah;->a:[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v3, 0x9

    .line 56
    sub-int v3, p2, v1

    .line 57
    if-gtz v3, :cond_4

    move-object p1, v2

    .line 66
    :cond_1
    :goto_2
    return-object p1

    :cond_2
    move v0, v1

    .line 51
    :goto_3
    array-length v5, v4

    if-ge v0, v5, :cond_3

    aget-byte v5, p1, v0

    aget-byte v6, v4, v0

    if-ne v5, v6, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    move v1, v3

    goto :goto_0

    .line 60
    :cond_4
    :try_start_1
    new-array v0, v3, [B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 61
    const/4 v2, 0x0

    :try_start_2
    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-object p1, v0

    .line 64
    goto :goto_2

    .line 62
    :catch_0
    move-exception v0

    .line 63
    :goto_4
    const-string v1, "IOException reading map tile info"

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 62
    :catch_1
    move-exception v1

    move-object p1, v0

    move-object v0, v1

    goto :goto_4

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method public final b()[Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/c/ah;->a()Lcom/google/e/a/a/a/b;

    move-result-object v3

    .line 82
    if-nez v3, :cond_0

    .line 83
    new-array v0, v0, [Ljava/lang/String;

    .line 93
    :goto_0
    return-object v0

    .line 88
    :cond_0
    iget-object v1, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v4

    .line 89
    new-array v1, v4, [Ljava/lang/String;

    move v2, v0

    .line 90
    :goto_1
    if-ge v2, v4, :cond_1

    .line 91
    const/16 v0, 0x1c

    invoke-virtual {v3, v5, v2, v0}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v1, v2

    .line 90
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 93
    goto :goto_0
.end method

.method public final c()[Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x0

    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/c/ah;->a()Lcom/google/e/a/a/a/b;

    move-result-object v3

    .line 101
    if-nez v3, :cond_0

    .line 102
    new-array v0, v0, [Ljava/lang/String;

    .line 110
    :goto_0
    return-object v0

    .line 105
    :cond_0
    iget-object v1, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v4

    .line 106
    new-array v1, v4, [Ljava/lang/String;

    move v2, v0

    .line 107
    :goto_1
    if-ge v2, v4, :cond_1

    .line 108
    const/16 v0, 0x1c

    invoke-virtual {v3, v5, v2, v0}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v1, v2

    .line 107
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 110
    goto :goto_0
.end method
