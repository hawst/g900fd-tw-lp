.class Lcom/google/android/apps/gmm/car/i/y;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/am;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/i/x;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/i/x;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/i/y;->a:Lcom/google/android/apps/gmm/car/i/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/search/al;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/y;->a:Lcom/google/android/apps/gmm/car/i/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/x;->b:Lcom/google/android/apps/gmm/search/al;

    if-eq p1, v0, :cond_0

    .line 232
    :goto_0
    return-void

    .line 215
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/car/i/x;->a:Ljava/lang/String;

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/y;->a:Lcom/google/android/apps/gmm/car/i/x;

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/i/x;->b:Lcom/google/android/apps/gmm/search/al;

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/y;->a:Lcom/google/android/apps/gmm/car/i/x;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/i/x;->c:Lcom/google/android/apps/gmm/car/i/k;

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/y;->a:Lcom/google/android/apps/gmm/car/i/x;

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/i/x;->c:Lcom/google/android/apps/gmm/car/i/k;

    .line 220
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 221
    iget-object v3, p1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    .line 222
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v4

    .line 223
    const/4 v0, 0x0

    :goto_1
    if-eq v0, v4, :cond_2

    .line 224
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/search/ap;->a(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v5

    .line 225
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 226
    invoke-static {v5}, Lcom/google/android/apps/gmm/car/bm;->a(Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/car/bm;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 228
    :cond_1
    sget-object v5, Lcom/google/android/apps/gmm/car/i/x;->a:Ljava/lang/String;

    goto :goto_2

    .line 231
    :cond_2
    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/car/i/k;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/search/al;Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/y;->a:Lcom/google/android/apps/gmm/car/i/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/x;->b:Lcom/google/android/apps/gmm/search/al;

    if-eq p1, v0, :cond_0

    .line 245
    :goto_0
    return-void

    .line 239
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/car/i/x;->a:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xf

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Search failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/y;->a:Lcom/google/android/apps/gmm/car/i/x;

    iput-object v3, v0, Lcom/google/android/apps/gmm/car/i/x;->b:Lcom/google/android/apps/gmm/search/al;

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/y;->a:Lcom/google/android/apps/gmm/car/i/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/x;->c:Lcom/google/android/apps/gmm/car/i/k;

    .line 242
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/y;->a:Lcom/google/android/apps/gmm/car/i/x;

    iput-object v3, v1, Lcom/google/android/apps/gmm/car/i/x;->c:Lcom/google/android/apps/gmm/car/i/k;

    .line 244
    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/i/k;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/search/al;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/y;->a:Lcom/google/android/apps/gmm/car/i/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/x;->b:Lcom/google/android/apps/gmm/search/al;

    if-eq p1, v0, :cond_0

    .line 258
    :goto_0
    return-void

    .line 252
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/car/i/x;->a:Ljava/lang/String;

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/y;->a:Lcom/google/android/apps/gmm/car/i/x;

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/i/x;->b:Lcom/google/android/apps/gmm/search/al;

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/y;->a:Lcom/google/android/apps/gmm/car/i/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/x;->c:Lcom/google/android/apps/gmm/car/i/k;

    .line 255
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/y;->a:Lcom/google/android/apps/gmm/car/i/x;

    iput-object v2, v1, Lcom/google/android/apps/gmm/car/i/x;->c:Lcom/google/android/apps/gmm/car/i/k;

    .line 257
    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/i/k;->a()V

    goto :goto_0
.end method
