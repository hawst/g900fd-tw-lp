.class public Lcom/google/android/apps/gmm/map/util/b/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/util/b/g;


# instance fields
.field public final a:Lcom/google/b/c/jo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/jo",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/android/apps/gmm/map/util/b/l;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/map/util/b/l;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/locks/ReadWriteLock;

.field private final d:Lcom/google/android/apps/gmm/map/util/b/n;

.field private final e:Lcom/google/android/apps/gmm/map/util/b/b/c;

.field private final f:Lcom/google/android/apps/gmm/map/util/b/o;

.field private final g:Lcom/google/android/apps/gmm/util/replay/a;

.field private final h:Lcom/google/android/apps/gmm/map/util/b/e;

.field private final i:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/gmm/map/util/b/k;",
            ">;>;"
        }
    .end annotation
.end field

.field private final j:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private volatile k:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/util/replay/a;)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {}, Lcom/google/b/c/ca;->n()Lcom/google/b/c/ca;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->a:Lcom/google/b/c/jo;

    .line 54
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->b:Ljava/util/Map;

    .line 65
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 78
    new-instance v0, Lcom/google/android/apps/gmm/map/util/b/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/i;-><init>(Lcom/google/android/apps/gmm/map/util/b/h;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->i:Ljava/lang/ThreadLocal;

    .line 86
    new-instance v0, Lcom/google/android/apps/gmm/map/util/b/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/j;-><init>(Lcom/google/android/apps/gmm/map/util/b/h;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->j:Ljava/lang/ThreadLocal;

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->k:Z

    .line 109
    new-instance v0, Lcom/google/android/apps/gmm/map/util/b/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/util/b/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->h:Lcom/google/android/apps/gmm/map/util/b/e;

    .line 110
    new-instance v0, Lcom/google/android/apps/gmm/map/util/b/n;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/n;-><init>(Lcom/google/android/apps/gmm/map/util/b/h;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->d:Lcom/google/android/apps/gmm/map/util/b/n;

    .line 111
    new-instance v0, Lcom/google/android/apps/gmm/map/util/b/b/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/b/c;-><init>(Lcom/google/android/apps/gmm/map/util/b/h;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->e:Lcom/google/android/apps/gmm/map/util/b/b/c;

    .line 112
    new-instance v0, Lcom/google/android/apps/gmm/map/util/b/o;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/gmm/map/util/b/o;-><init>(Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/map/util/b/h;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->f:Lcom/google/android/apps/gmm/map/util/b/o;

    .line 113
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/util/b/h;->g:Lcom/google/android/apps/gmm/util/replay/a;

    .line 114
    return-void
.end method

.method static b(Ljava/lang/Object;Lcom/google/android/apps/gmm/map/util/b/l;)V
    .locals 2

    .prologue
    .line 313
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/util/b/l;->b:Z

    if-eqz v0, :cond_0

    .line 326
    :goto_0
    return-void

    .line 317
    :cond_0
    :try_start_0
    invoke-virtual {p1, p0}, Lcom/google/android/apps/gmm/map/util/b/l;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 318
    :catch_0
    move-exception v0

    .line 321
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 322
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_1

    .line 323
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 325
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private c()V
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->j:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    :goto_0
    return-void

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->j:Ljava/lang/ThreadLocal;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 213
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->i:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 215
    :goto_1
    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/util/b/k;

    if-eqz v1, :cond_1

    .line 216
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/util/b/k;->a:Ljava/lang/Object;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/b/k;->b:Lcom/google/android/apps/gmm/map/util/b/l;

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/map/util/b/h;->b(Ljava/lang/Object;Lcom/google/android/apps/gmm/map/util/b/l;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 219
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/b/h;->j:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->remove()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->j:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->g:Lcom/google/android/apps/gmm/util/replay/a;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/util/replay/a;->a(Ljava/lang/Object;)V

    .line 155
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/b/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/map/util/b/d;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/b/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 160
    :try_start_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/util/b/d;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 165
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/b/h;->d:Lcom/google/android/apps/gmm/map/util/b/n;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/util/b/d;->b:Ljava/util/Set;

    invoke-virtual {v1, p1, v2}, Lcom/google/android/apps/gmm/map/util/b/n;->a(Ljava/lang/Object;Ljava/util/Set;)V

    .line 168
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/b/d;->a:Ljava/util/Set;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/map/util/b/h;->a(Ljava/lang/Object;Ljava/util/Set;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/b/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 173
    if-nez v0, :cond_1

    instance-of v0, p1, Lcom/google/b/d/b;

    if-nez v0, :cond_1

    .line 174
    new-instance v0, Lcom/google/b/d/b;

    invoke-direct {v0, p0, p1}, Lcom/google/b/d/b;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/util/b/h;->c(Ljava/lang/Object;)V

    .line 177
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/util/b/h;->c()V

    .line 178
    return-void

    .line 170
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/b/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method final a(Ljava/lang/Object;Lcom/google/android/apps/gmm/map/util/b/l;)V
    .locals 4

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->f:Lcom/google/android/apps/gmm/map/util/b/o;

    iget-object v1, p2, Lcom/google/android/apps/gmm/map/util/b/l;->a:Lcom/google/android/apps/gmm/shared/c/a/p;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/util/b/o;->a:Lcom/google/android/apps/gmm/shared/c/a/j;

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Lcom/google/android/apps/gmm/shared/c/a/p;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 197
    :goto_1
    return-void

    .line 193
    :cond_0
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/util/b/o;->a:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v3, Lcom/google/android/apps/gmm/map/util/b/p;

    invoke-direct {v3, v0, p1, p2}, Lcom/google/android/apps/gmm/map/util/b/p;-><init>(Lcom/google/android/apps/gmm/map/util/b/o;Ljava/lang/Object;Lcom/google/android/apps/gmm/map/util/b/l;)V

    invoke-interface {v2, v3, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    const/4 v0, 0x1

    goto :goto_0

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->i:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    new-instance v1, Lcom/google/android/apps/gmm/map/util/b/k;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/gmm/map/util/b/k;-><init>(Ljava/lang/Object;Lcom/google/android/apps/gmm/map/util/b/l;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final declared-synchronized a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 294
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/util/b/h;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 295
    const/4 v0, 0x0

    .line 298
    :goto_0
    monitor-exit p0

    return v0

    .line 297
    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/util/b/h;->k:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 294
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/util/Set;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<*>;>;)Z"
        }
    .end annotation

    .prologue
    .line 132
    const/4 v0, 0x0

    .line 134
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 137
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/util/b/h;->a:Lcom/google/b/c/jo;

    invoke-interface {v3, v0}, Lcom/google/b/c/jo;->e(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/b/h;->a:Lcom/google/b/c/jo;

    invoke-interface {v1, v0}, Lcom/google/b/c/jo;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 140
    const/4 v1, 0x1

    .line 141
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/util/b/l;

    .line 142
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/map/util/b/h;->a(Ljava/lang/Object;Lcom/google/android/apps/gmm/map/util/b/l;)V

    goto :goto_1

    :cond_0
    move v0, v1

    move v1, v0

    .line 145
    goto :goto_0

    .line 146
    :cond_1
    return v1
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 303
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 304
    monitor-exit p0

    return-void

    .line 303
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 182
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/b/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/map/util/b/d;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/b/h;->d:Lcom/google/android/apps/gmm/map/util/b/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/b/d;->b:Ljava/util/Set;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/gmm/map/util/b/n;->b(Ljava/lang/Object;Ljava/util/Set;)V

    .line 184
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->k:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/gmm/util/replay/j;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 122
    :goto_1
    return-void

    .line 118
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 121
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/util/b/h;->a(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final d(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->h:Lcom/google/android/apps/gmm/map/util/b/e;

    invoke-static {p1}, Lcom/google/android/apps/gmm/map/util/b/e;->a(Ljava/lang/Object;)Lcom/google/b/c/hu;

    move-result-object v0

    .line 231
    invoke-interface {v0}, Lcom/google/b/c/hu;->h()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v1

    .line 233
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/util/b/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 235
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/util/b/h;->b:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 236
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Tried to register "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " twice."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/b/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 238
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/util/b/h;->b:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/util/b/h;->a:Lcom/google/b/c/jo;

    invoke-interface {v2, v0}, Lcom/google/b/c/jo;->a(Lcom/google/b/c/hu;)Z

    .line 240
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/util/b/h;->f:Lcom/google/android/apps/gmm/map/util/b/o;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/google/android/apps/gmm/map/util/b/o;->a(Ljava/lang/Class;Ljava/util/Collection;)Lcom/google/android/apps/gmm/shared/c/a/p;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    if-eq v1, v3, :cond_1

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/util/b/o;->a:Lcom/google/android/apps/gmm/shared/c/a/j;

    invoke-interface {v2, v1, p1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Lcom/google/android/apps/gmm/shared/c/a/p;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "No executor registered for %s while registering %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object p1, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 243
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/b/h;->d:Lcom/google/android/apps/gmm/map/util/b/n;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/n;->a(Lcom/google/b/c/hu;)V

    .line 244
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/b/h;->e:Lcom/google/android/apps/gmm/map/util/b/b/c;

    invoke-interface {v0}, Lcom/google/b/c/hu;->l()Ljava/util/Set;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/util/b/b/c;->a:Lcom/google/android/apps/gmm/map/util/b/b/e;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/b/c;->a(Ljava/util/Set;)Lcom/google/android/apps/gmm/map/util/b/b/e;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/b/e;->a(Lcom/google/android/apps/gmm/map/util/b/c/a;)V

    sget-object v2, Lcom/google/android/apps/gmm/map/util/b/b/b;->a:Lcom/google/android/apps/gmm/map/util/b/b/b;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/map/util/b/b/c;->a(Ljava/util/Set;Lcom/google/android/apps/gmm/map/util/b/b/b;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/b/c;->b(Ljava/util/Set;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 251
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/util/b/h;->c()V

    .line 252
    return-void
.end method

.method public final e(Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 258
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/cv;

    .line 259
    if-nez v0, :cond_0

    .line 260
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "missing event handler for an annotated method. Is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " registered?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 274
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/b/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 263
    :cond_0
    :try_start_1
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 264
    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/util/b/l;

    .line 265
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/util/b/a/b;->c:Ljava/lang/reflect/Method;

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v2

    const/4 v7, 0x0

    aget-object v2, v2, v7

    .line 266
    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 267
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/util/b/h;->a:Lcom/google/b/c/jo;

    invoke-interface {v7, v2, v1}, Lcom/google/b/c/jo;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 268
    :cond_1
    iget-boolean v2, v1, Lcom/google/android/apps/gmm/map/util/b/l;->b:Z

    if-nez v2, :cond_2

    move v2, v3

    :goto_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    move v2, v4

    goto :goto_1

    :cond_3
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/map/util/b/l;->b:Z

    goto :goto_0

    .line 271
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/b/h;->f:Lcom/google/android/apps/gmm/map/util/b/o;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/map/util/b/o;->a(Ljava/lang/Class;Ljava/util/Collection;)Lcom/google/android/apps/gmm/shared/c/a/p;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    if-eq v0, v2, :cond_5

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/b/o;->a:Lcom/google/android/apps/gmm/shared/c/a/j;

    invoke-interface {v1, v0, p1}, Lcom/google/android/apps/gmm/shared/c/a/j;->b(Lcom/google/android/apps/gmm/shared/c/a/p;Ljava/lang/Object;)V

    .line 272
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->e:Lcom/google/android/apps/gmm/map/util/b/b/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/util/b/b/c;->a:Lcom/google/android/apps/gmm/map/util/b/b/e;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/util/b/b/c;->a(Ljava/util/Set;)Lcom/google/android/apps/gmm/map/util/b/b/e;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/b/e;->b(Lcom/google/android/apps/gmm/map/util/b/c/a;)V

    sget-object v1, Lcom/google/android/apps/gmm/map/util/b/b/b;->b:Lcom/google/android/apps/gmm/map/util/b/b/b;

    invoke-virtual {v0, v5, v1}, Lcom/google/android/apps/gmm/map/util/b/b/c;->a(Ljava/util/Set;Lcom/google/android/apps/gmm/map/util/b/b/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 279
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/util/b/h;->c()V

    .line 280
    return-void
.end method
