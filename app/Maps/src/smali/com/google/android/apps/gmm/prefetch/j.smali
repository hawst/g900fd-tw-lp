.class final Lcom/google/android/apps/gmm/prefetch/j;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/prefetch/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/prefetch/b;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 421
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    .line 422
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 423
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    .line 427
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 467
    :goto_0
    return-void

    .line 429
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetch/b;->c()V

    goto :goto_0

    .line 433
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/prefetch/b;->a(Lcom/google/android/apps/gmm/prefetch/b;)V

    goto :goto_0

    .line 439
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/prefetch/b;->b(Lcom/google/android/apps/gmm/prefetch/b;)V

    goto :goto_0

    .line 443
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/prefetch/h;

    .line 445
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/prefetch/b;->a(Lcom/google/android/apps/gmm/prefetch/b;Lcom/google/android/apps/gmm/prefetch/h;)V

    goto :goto_0

    .line 448
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    const/4 v1, 0x4

    sget-wide v2, Lcom/google/android/apps/gmm/prefetch/b;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/prefetch/j;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 452
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetch/b;->stopSelf()V

    goto :goto_0

    .line 457
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/b/a/an;

    .line 459
    iget-object v1, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    move-object v6, v1

    check-cast v6, Ljava/util/Queue;

    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    move-object v7, v0

    check-cast v7, Lcom/google/android/apps/gmm/prefetch/a/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/prefetch/b;->c(Lcom/google/android/apps/gmm/prefetch/b;)Lcom/google/android/apps/gmm/shared/net/a/r;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/r;->a:Lcom/google/r/b/a/ti;

    iget-boolean v0, v0, Lcom/google/r/b/a/ti;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->E()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/d;->a:Lcom/google/android/apps/gmm/prefetch/a/d;

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/e;->e:Lcom/google/android/apps/gmm/prefetch/a/e;

    invoke-interface {v7, v0, v1}, Lcom/google/android/apps/gmm/prefetch/a/c;->a(Lcom/google/android/apps/gmm/prefetch/a/d;Lcom/google/android/apps/gmm/prefetch/a/e;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/l;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/prefetch/b;->d:Lcom/google/android/apps/gmm/map/h/d;

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/prefetch/b;->f()Lcom/google/android/apps/gmm/map/internal/d/be;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    iget-object v3, v3, Lcom/google/android/apps/gmm/prefetch/b;->f:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v4, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/c;->c:Lcom/google/android/apps/gmm/map/internal/d/c;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/prefetch/l;-><init>(Lcom/google/android/apps/gmm/map/h/d;Lcom/google/android/apps/gmm/map/internal/d/be;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/prefetch/b;Lcom/google/android/apps/gmm/map/internal/d/c;)V

    new-instance v1, Lcom/google/android/apps/gmm/prefetch/g;

    invoke-direct {v1, v6}, Lcom/google/android/apps/gmm/prefetch/g;-><init>(Ljava/util/Queue;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-static {v2, v1, v0, v7}, Lcom/google/android/apps/gmm/prefetch/b;->a(Lcom/google/android/apps/gmm/prefetch/b;Lcom/google/android/apps/gmm/prefetch/q;Lcom/google/android/apps/gmm/prefetch/m;Lcom/google/android/apps/gmm/prefetch/a/c;)V

    goto/16 :goto_0

    .line 463
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/prefetch/f;

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    sget-object v3, Lcom/google/android/apps/gmm/prefetch/k;->b:Lcom/google/android/apps/gmm/prefetch/k;

    iput-object v3, v2, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    new-instance v2, Lcom/google/android/apps/gmm/prefetch/v;

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetch/j;->a:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-direct {v2, v1, v0, v3}, Lcom/google/android/apps/gmm/prefetch/v;-><init>(Lcom/google/android/apps/gmm/prefetch/k;Lcom/google/android/apps/gmm/prefetch/f;Lcom/google/android/apps/gmm/prefetch/b;)V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/prefetch/v;->a()V

    goto/16 :goto_0

    .line 427
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
