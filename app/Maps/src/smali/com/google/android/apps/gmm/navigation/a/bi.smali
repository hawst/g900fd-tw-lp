.class Lcom/google/android/apps/gmm/navigation/a/bi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# instance fields
.field final synthetic a:Landroid/speech/tts/TextToSpeech$OnInitListener;

.field final synthetic b:Lcom/google/android/apps/gmm/navigation/a/bh;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/navigation/a/bh;Landroid/speech/tts/TextToSpeech$OnInitListener;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/bi;->b:Lcom/google/android/apps/gmm/navigation/a/bh;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/bi;->a:Landroid/speech/tts/TextToSpeech$OnInitListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInit(I)V
    .locals 4

    .prologue
    .line 81
    const-string v0, "TextToSpeechProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x26

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "tts.OnInitListener.onInit("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    if-nez p1, :cond_0

    .line 86
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bi;->b:Lcom/google/android/apps/gmm/navigation/a/bh;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/bi;->b:Lcom/google/android/apps/gmm/navigation/a/bh;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/navigation/a/bh;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/navigation/a/bh;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/a/bh;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bi;->a:Landroid/speech/tts/TextToSpeech$OnInitListener;

    invoke-interface {v0, p1}, Landroid/speech/tts/TextToSpeech$OnInitListener;->onInit(I)V

    .line 94
    if-nez p1, :cond_1

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bi;->b:Lcom/google/android/apps/gmm/navigation/a/bh;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/a/bh;->a:Lcom/google/android/apps/gmm/navigation/logging/m;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bi;->b:Lcom/google/android/apps/gmm/navigation/a/bh;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/bh;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bi;->b:Lcom/google/android/apps/gmm/navigation/a/bh;

    .line 96
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/bh;->a()Ljava/util/Locale;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    .line 95
    :goto_1
    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/navigation/logging/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_1
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 88
    const-string v1, "TextToSpeechProxy"

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Exception after TTS reported init SUCCESS"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 90
    const/4 p1, -0x1

    goto :goto_0

    .line 96
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bi;->b:Lcom/google/android/apps/gmm/navigation/a/bh;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/bh;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
