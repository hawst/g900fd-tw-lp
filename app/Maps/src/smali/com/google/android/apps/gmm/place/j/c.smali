.class public Lcom/google/android/apps/gmm/place/j/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/ab;
.implements Lcom/google/android/apps/gmm/place/br;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/activities/c;

.field final b:Lcom/google/android/apps/gmm/place/ch;

.field private c:Lcom/google/android/apps/gmm/base/views/c/g;

.field private d:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/place/ch;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 42
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/j/c;->b:Lcom/google/android/apps/gmm/place/ch;

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    new-instance v1, Lcom/google/android/libraries/curvular/a;

    invoke-direct {v1, v4}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    .line 44
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->g:Lcom/google/android/libraries/curvular/aq;

    sget v1, Lcom/google/android/apps/gmm/l;->ba:I

    .line 46
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    sget v3, Lcom/google/android/apps/gmm/l;->mG:I

    invoke-virtual {p1, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;[Ljava/lang/Object;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v1

    .line 45
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->d:Lcom/google/android/libraries/curvular/bi;

    new-instance v1, Lcom/google/android/apps/gmm/place/j/d;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/place/j/d;-><init>(Lcom/google/android/apps/gmm/place/j/c;Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 47
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    .line 63
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/j/c;->c:Lcom/google/android/apps/gmm/base/views/c/g;

    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/j/c;->a()V

    .line 66
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/c;->c:Lcom/google/android/apps/gmm/base/views/c/g;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>(Lcom/google/android/apps/gmm/base/views/c/g;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 83
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/f;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->mG:I

    .line 84
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->a:Ljava/lang/CharSequence;

    sget v2, Lcom/google/android/apps/gmm/f;->eO:I

    .line 85
    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->c:Lcom/google/android/libraries/curvular/aw;

    const/4 v2, 0x2

    .line 86
    iput v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->e:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->mG:I

    .line 87
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->b:Ljava/lang/CharSequence;

    new-instance v2, Lcom/google/android/apps/gmm/place/j/e;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/place/j/e;-><init>(Lcom/google/android/apps/gmm/place/j/c;)V

    .line 88
    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->d:Ljava/lang/Runnable;

    .line 95
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/e;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/base/views/c/e;-><init>(Lcom/google/android/apps/gmm/base/views/c/f;)V

    .line 83
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/c;->d:Lcom/google/android/apps/gmm/x/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/c;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->H:Z

    if-eqz v0, :cond_0

    .line 98
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/f;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->jk:I

    .line 99
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->a:Ljava/lang/CharSequence;

    new-instance v2, Lcom/google/android/apps/gmm/place/j/f;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/place/j/f;-><init>(Lcom/google/android/apps/gmm/place/j/c;)V

    .line 100
    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->d:Ljava/lang/Runnable;

    .line 108
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/e;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/base/views/c/e;-><init>(Lcom/google/android/apps/gmm/base/views/c/f;)V

    .line 98
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/c;->d:Lcom/google/android/apps/gmm/x/o;

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/c;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    .line 114
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/c;->c:Lcom/google/android/apps/gmm/base/views/c/g;

    .line 115
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    .line 76
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/j/c;->d:Lcom/google/android/apps/gmm/x/o;

    .line 77
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/j/c;->a()V

    .line 78
    return-void
.end method

.method public final c()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/c;->c:Lcom/google/android/apps/gmm/base/views/c/g;

    return-object v0
.end method
