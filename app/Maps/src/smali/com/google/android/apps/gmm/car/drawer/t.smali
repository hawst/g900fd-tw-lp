.class public final Lcom/google/android/apps/gmm/car/drawer/t;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I


# instance fields
.field final d:Lcom/google/android/apps/gmm/car/ad;

.field final e:Landroid/os/Handler;

.field f:Z

.field final g:Ljava/lang/Runnable;

.field private final h:Lcom/google/android/apps/gmm/car/drawer/o;

.field private final i:Lcom/google/android/apps/gmm/car/drawer/s;

.field private final j:Lcom/google/android/apps/gmm/car/drawer/s;

.field private final k:Lcom/google/android/apps/gmm/car/drawer/s;

.field private final l:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/drawer/t;->a:I

    .line 35
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/drawer/t;->b:I

    .line 36
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/drawer/t;->c:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/car/ad;)V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->e:Landroid/os/Handler;

    .line 97
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/u;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/drawer/u;-><init>(Lcom/google/android/apps/gmm/car/drawer/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->i:Lcom/google/android/apps/gmm/car/drawer/s;

    .line 157
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/drawer/v;-><init>(Lcom/google/android/apps/gmm/car/drawer/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->j:Lcom/google/android/apps/gmm/car/drawer/s;

    .line 212
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/w;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/drawer/w;-><init>(Lcom/google/android/apps/gmm/car/drawer/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->k:Lcom/google/android/apps/gmm/car/drawer/s;

    .line 299
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/x;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/drawer/x;-><init>(Lcom/google/android/apps/gmm/car/drawer/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->l:Ljava/lang/Object;

    .line 350
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/y;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/drawer/y;-><init>(Lcom/google/android/apps/gmm/car/drawer/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->g:Ljava/lang/Runnable;

    .line 50
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/car/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 54
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/t;->i:Lcom/google/android/apps/gmm/car/drawer/s;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/t;->j:Lcom/google/android/apps/gmm/car/drawer/s;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/t;->k:Lcom/google/android/apps/gmm/car/drawer/s;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    new-instance v1, Lcom/google/android/apps/gmm/car/drawer/o;

    iget-object v2, p1, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/car/drawer/o;-><init>(Lcom/google/android/libraries/curvular/bd;Ljava/util/List;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/t;->h:Lcom/google/android/apps/gmm/car/drawer/o;

    .line 59
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/car/drawer/t;Z)Z
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->m:Lcom/google/android/apps/gmm/o/a/c;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/o/a/c;->a(Z)Z

    move-result v0

    if-eq v0, p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/z/b/n;

    if-eqz p1, :cond_2

    sget-object v0, Lcom/google/r/b/a/a;->b:Lcom/google/r/b/a/a;

    :goto_1
    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    sget-object v0, Lcom/google/b/f/t;->al:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->F:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/r/b/a/a;->c:Lcom/google/r/b/a/a;

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/car/drawer/t;Z)V
    .locals 3

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->f:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/gmm/car/drawer/t;->f:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->h:Lcom/google/android/apps/gmm/car/drawer/o;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/o;->b:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/gmm/car/drawer/p;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/car/drawer/p;-><init>(Lcom/google/android/apps/gmm/car/drawer/o;)V

    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->f:Z

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->h:Lcom/google/android/apps/gmm/car/drawer/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/o;->a()Landroid/view/View;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/t;->l:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 71
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    .line 72
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v2}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->bI:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 71
    iput-object v2, v1, Lcom/google/android/apps/gmm/car/ao;->k:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/ao;->b()V

    .line 73
    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/ao;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/ao;->k:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->b()V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/t;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/t;->l:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/t;->h:Lcom/google/android/apps/gmm/car/drawer/o;

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/drawer/o;->b:Landroid/view/View;

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/drawer/o;->c:Lcom/google/android/gms/car/support/PagedListView;

    .line 82
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/google/android/apps/gmm/car/m/e;->c:Lcom/google/android/apps/gmm/car/m/e;

    return-object v0
.end method
