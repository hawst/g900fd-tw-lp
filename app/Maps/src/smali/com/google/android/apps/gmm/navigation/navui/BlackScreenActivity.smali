.class public Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;
.super Landroid/app/Activity;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field final a:Landroid/content/BroadcastReceiver;

.field private b:Z

.field private c:Lcom/google/android/apps/gmm/base/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 61
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/a;-><init>(Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->a:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public static a(Landroid/app/Activity;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 187
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 188
    const-string v1, "sip"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 189
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 190
    invoke-virtual {p0, v3, v3}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 191
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/navigation/g/a/f;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 171
    sget-object v0, Lcom/google/b/f/b/a/t;->f:Lcom/google/b/f/b/a/t;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->a(Lcom/google/b/f/b/a/t;)V

    .line 172
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/g;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 166
    sget-object v0, Lcom/google/b/f/b/a/t;->e:Lcom/google/b/f/b/a/t;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->a(Lcom/google/b/f/b/a/t;)V

    .line 167
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 159
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/g/b/f;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 160
    :cond_2
    sget-object v0, Lcom/google/b/f/b/a/t;->d:Lcom/google/b/f/b/a/t;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->a(Lcom/google/b/f/b/a/t;)V

    .line 162
    :cond_3
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/navui/PretendToBePluggedInEventForTesting;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->b:Z

    if-nez v0, :cond_0

    .line 178
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->b:Z

    .line 181
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/navigation/navui/PretendToBePluggedInEventForTesting;->isPluggedIn()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    sget-object v0, Lcom/google/b/f/b/a/t;->c:Lcom/google/b/f/b/a/t;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->a(Lcom/google/b/f/b/a/t;)V

    .line 184
    :cond_1
    return-void
.end method

.method a(Lcom/google/b/f/b/a/t;)V
    .locals 2
    .param p1    # Lcom/google/b/f/b/a/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    :goto_0
    return-void

    .line 107
    :cond_0
    if-eqz p1, :cond_1

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/b/f/b/a/t;)V

    .line 110
    :cond_1
    const/high16 v0, -0x40800000    # -1.0f

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->c:Lcom/google/android/apps/gmm/base/a;

    .line 80
    const v0, 0x3b83126f    # 0.004f

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 83
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 84
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 85
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->setContentView(Landroid/view/View;)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "sip"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->b:Z

    .line 88
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 126
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 128
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->b:Z

    if-nez v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 132
    :cond_0
    const/4 v1, 0x0

    .line 133
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 134
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_1

    .line 135
    sget-object v0, Lcom/google/b/f/b/a/t;->a:Lcom/google/b/f/b/a/t;

    .line 138
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->a(Lcom/google/b/f/b/a/t;)V

    .line 139
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 116
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 118
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->b:Z

    if-nez v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->a:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 122
    :cond_0
    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    .prologue
    .line 153
    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    .line 154
    sget-object v0, Lcom/google/b/f/b/a/t;->b:Lcom/google/b/f/b/a/t;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->a(Lcom/google/b/f/b/a/t;)V

    .line 155
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 143
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 144
    if-nez p1, :cond_0

    .line 147
    sget-object v0, Lcom/google/b/f/b/a/t;->b:Lcom/google/b/f/b/a/t;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/BlackScreenActivity;->a(Lcom/google/b/f/b/a/t;)V

    .line 149
    :cond_0
    return-void
.end method
