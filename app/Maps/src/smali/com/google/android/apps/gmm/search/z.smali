.class Lcom/google/android/apps/gmm/search/z;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/hotels/a/g;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/search/SearchListFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/search/SearchListFragment;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/z;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/z;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 335
    if-eqz v0, :cond_0

    .line 337
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/z;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    sget v2, Lcom/google/android/apps/gmm/l;->iU:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/search/SearchListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 336
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->a(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/CharSequence;)V

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/z;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/SearchListFragment;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/hotels/a/h;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/hotels/a/h;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 341
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            "Lcom/google/e/a/a/a/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/z;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/search/ap;->a(Ljava/util/Map;)V

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/z;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/SearchListFragment;->e:Lcom/google/android/apps/gmm/base/views/a/a;

    .line 325
    if-eqz v0, :cond_0

    .line 326
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/a/a;->notifyDataSetChanged()V

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/z;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/SearchListFragment;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/hotels/a/h;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/hotels/a/h;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 330
    return-void
.end method
