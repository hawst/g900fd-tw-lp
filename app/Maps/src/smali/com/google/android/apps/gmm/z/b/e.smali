.class public Lcom/google/android/apps/gmm/z/b/e;
.super Lcom/google/android/apps/gmm/z/b/a;
.source "PG"


# instance fields
.field public final b:Lcom/google/b/f/b/a/bc;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/z/b/a;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 31
    invoke-static {}, Lcom/google/b/f/b/a/ba;->newBuilder()Lcom/google/b/f/b/a/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/b/e;->b:Lcom/google/b/f/b/a/bc;

    .line 32
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/b/f/b/a/ba;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/z/b/a;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 39
    invoke-static {p2}, Lcom/google/b/f/b/a/ba;->a(Lcom/google/b/f/b/a/ba;)Lcom/google/b/f/b/a/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/b/e;->b:Lcom/google/b/f/b/a/bc;

    .line 40
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/b/f/b/a/e;)Lcom/google/android/apps/gmm/z/b/e;
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/e;->b:Lcom/google/b/f/b/a/bc;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, v0, Lcom/google/b/f/b/a/bc;->h:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, v0, Lcom/google/b/f/b/a/bc;->a:I

    .line 61
    return-object p0
.end method

.method public a(Lcom/google/b/f/b/a/bc;)V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method protected a(Lcom/google/r/b/a/apf;)V
    .locals 3

    .prologue
    .line 91
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/z/b/a;->a(Lcom/google/r/b/a/apf;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/e;->b:Lcom/google/b/f/b/a/bc;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/z/b/e;->a(Lcom/google/b/f/b/a/bc;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/e;->b:Lcom/google/b/f/b/a/bc;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/bc;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ba;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p1, Lcom/google/r/b/a/apf;->b:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/r/b/a/apf;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/google/r/b/a/apf;->a:I

    .line 94
    return-void
.end method
