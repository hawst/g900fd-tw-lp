.class public Lcom/google/android/apps/gmm/cardui/a/o;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/apps/gmm/cardui/a/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/cardui/a/o;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    return-void
.end method

.method public static a(Lcom/google/o/h/a/kh;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/o/h/a/od;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/android/apps/gmm/startpage/a/b;)V
    .locals 14
    .param p3    # Lcom/google/o/h/a/od;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/cardui/a/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/android/apps/gmm/iamhere/c/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/android/apps/gmm/startpage/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 87
    if-eqz p5, :cond_2

    move-object/from16 v3, p5

    .line 94
    :goto_0
    if-eqz p6, :cond_3

    move-object/from16 v2, p6

    .line 87
    :goto_1
    new-instance v6, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/startpage/d/d;-><init>()V

    iget v4, p0, Lcom/google/o/h/a/kh;->e:I

    invoke-static {v4}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v4

    invoke-virtual {v6, v4}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/dq;)V

    const/4 v4, 0x0

    invoke-virtual {v6, v4}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Z)V

    iget v4, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v4, v4, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_4

    const/4 v4, 0x1

    :goto_2
    if-eqz v4, :cond_0

    iget v4, p0, Lcom/google/o/h/a/kh;->h:I

    invoke-virtual {v6, v4}, Lcom/google/android/apps/gmm/startpage/d/d;->a(I)V

    :cond_0
    iget v4, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v4, v4, 0x1

    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    const/4 v4, 0x1

    :goto_3
    if-eqz v4, :cond_6

    new-instance v4, Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {p0}, Lcom/google/o/h/a/kh;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/apps/gmm/startpage/d/e;-><init>(Ljava/lang/String;)V

    :goto_4
    invoke-virtual {v6, v4}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Lcom/google/android/apps/gmm/startpage/d/e;)V

    iget-object v4, p0, Lcom/google/o/h/a/kh;->c:Ljava/lang/Object;

    instance-of v5, v4, Ljava/lang/String;

    if-eqz v5, :cond_7

    check-cast v4, Ljava/lang/String;

    :goto_5
    invoke-virtual {v6, v4}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v4

    invoke-virtual {v6, v4}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/r;)V

    sget-object v5, Lcom/google/android/apps/gmm/cardui/a/p;->a:[I

    iget v4, p0, Lcom/google/o/h/a/kh;->f:I

    invoke-static {v4}, Lcom/google/o/h/a/kk;->a(I)Lcom/google/o/h/a/kk;

    move-result-object v4

    if-nez v4, :cond_1

    sget-object v4, Lcom/google/o/h/a/kk;->a:Lcom/google/o/h/a/kk;

    :cond_1
    invoke-virtual {v4}, Lcom/google/o/h/a/kk;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    move-object/from16 v0, p4

    invoke-static {v6, v0}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->b(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/cardui/a/d;)Lcom/google/android/apps/gmm/startpage/OdelayListFragment;

    move-result-object v2

    .line 97
    :goto_6
    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v3

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v2

    invoke-virtual {p1, v3, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 98
    return-void

    .line 94
    :cond_2
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/iamhere/a/b;->b(Z)Lcom/google/android/apps/gmm/iamhere/c/o;

    move-result-object v3

    goto/16 :goto_0

    :cond_3
    sget-object v2, Lcom/google/android/apps/gmm/startpage/a/b;->d:Lcom/google/android/apps/gmm/startpage/a/b;

    goto/16 :goto_1

    .line 87
    :cond_4
    const/4 v4, 0x0

    goto :goto_2

    :cond_5
    const/4 v4, 0x0

    goto :goto_3

    :cond_6
    sget-object v4, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    goto :goto_4

    :cond_7
    check-cast v4, Lcom/google/n/f;

    invoke-virtual {v4}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/google/n/f;->e()Z

    move-result v4

    if-eqz v4, :cond_8

    iput-object v5, p0, Lcom/google/o/h/a/kh;->c:Ljava/lang/Object;

    :cond_8
    move-object v4, v5

    goto :goto_5

    :pswitch_0
    iget-object v2, p0, Lcom/google/o/h/a/kh;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mr;->h()Lcom/google/o/h/a/mr;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/o/h/a/mr;

    if-eqz p3, :cond_e

    new-instance v4, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/o/h/a/od;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/r/b/a/ads;

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v3, v5, Lcom/google/android/apps/gmm/base/g/i;->g:Lcom/google/r/b/a/ads;

    iget-boolean v3, v3, Lcom/google/r/b/a/ads;->v:Z

    iput-boolean v3, v4, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/cardui/f/d;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/cardui/f/d;-><init>()V

    iget-object v5, v4, Lcom/google/android/apps/gmm/cardui/f/d;->a:Lcom/google/android/apps/gmm/cardui/f/c;

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/google/android/apps/gmm/base/g/c;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    if-nez v7, :cond_9

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_9
    array-length v8, v7

    if-ltz v8, :cond_a

    const/4 v3, 0x1

    :goto_7
    if-nez v3, :cond_b

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_a
    const/4 v3, 0x0

    goto :goto_7

    :cond_b
    const-wide/16 v10, 0x5

    int-to-long v12, v8

    add-long/2addr v10, v12

    div-int/lit8 v3, v8, 0xa

    int-to-long v8, v3

    add-long/2addr v8, v10

    const-wide/32 v10, 0x7fffffff

    cmp-long v3, v8, v10

    if-lez v3, :cond_c

    const v3, 0x7fffffff

    :goto_8
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v8, v7}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-virtual {v5, v8}, Lcom/google/android/apps/gmm/cardui/f/c;->a(Ljava/util/List;)V

    invoke-static {}, Lcom/google/o/h/a/nt;->newBuilder()Lcom/google/o/h/a/nv;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/o/h/a/nv;->a(Lcom/google/o/h/a/mr;)Lcom/google/o/h/a/nv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/o/h/a/nv;->g()Lcom/google/n/t;

    move-result-object v2

    check-cast v2, Lcom/google/o/h/a/nt;

    iput-object v2, v4, Lcom/google/android/apps/gmm/cardui/f/d;->b:Lcom/google/o/h/a/nt;

    iget-object v2, v4, Lcom/google/android/apps/gmm/cardui/f/d;->a:Lcom/google/android/apps/gmm/cardui/f/c;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/cardui/f/c;->e(I)V

    invoke-static {v4}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-static {v2, v0, v3, v6}, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/startpage/d/d;)Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;

    move-result-object v2

    goto/16 :goto_6

    :cond_c
    const-wide/32 v10, -0x80000000

    cmp-long v3, v8, v10

    if-gez v3, :cond_d

    const/high16 v3, -0x80000000

    goto :goto_8

    :cond_d
    long-to-int v3, v8

    goto :goto_8

    :cond_e
    invoke-static {v6, v2}, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->a(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/o/h/a/mr;)Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;

    move-result-object v2

    goto/16 :goto_6

    :pswitch_1
    iget-object v4, p1, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v4, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/mylocation/b/i;->d()Lcom/google/android/apps/gmm/mylocation/b/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/mylocation/b/f;->b()Lcom/google/android/apps/gmm/map/s/a;

    move-result-object v7

    move-object v4, p0

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->a(Lcom/google/android/apps/gmm/startpage/a/b;Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/o/h/a/kh;Lcom/google/android/apps/gmm/map/b/a/r;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/s/a;)Lcom/google/android/apps/gmm/startpage/d/d;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v2

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/startpage/d/d;)Lcom/google/android/apps/gmm/startpage/GuidePageFragment;

    move-result-object v2

    goto/16 :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 52
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kh;

    .line 53
    sget-object v1, Lcom/google/android/apps/gmm/cardui/a/o;->a:Ljava/lang/String;

    const-string v1, "LoadOdelayActionHandler.execute() with requestToken="

    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 54
    :goto_0
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->h()Lcom/google/android/apps/gmm/cardui/c;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v2, Lcom/google/android/apps/gmm/cardui/b/b;->c:Lcom/google/android/apps/gmm/cardui/b/b;

    .line 57
    :goto_1
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->g()Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v1

    iget-object v3, v1, Lcom/google/android/apps/gmm/util/b/b;->e:Lcom/google/o/h/a/od;

    .line 60
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    .line 63
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->f()Lcom/google/android/apps/gmm/cardui/a/d;

    move-result-object v4

    move-object v6, v5

    .line 59
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/cardui/a/o;->a(Lcom/google/o/h/a/kh;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/o/h/a/od;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/android/apps/gmm/startpage/a/b;)V

    .line 66
    return-void

    .line 53
    :cond_0
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->h()Lcom/google/android/apps/gmm/cardui/c;

    move-result-object v1

    iget-object v2, v1, Lcom/google/android/apps/gmm/cardui/c;->h:Lcom/google/android/apps/gmm/cardui/b/b;

    goto :goto_1
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    sget-object v0, Lcom/google/o/h/a/g;->e:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 71
    sget-object v0, Lcom/google/o/h/a/g;->f:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 72
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    .line 47
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
