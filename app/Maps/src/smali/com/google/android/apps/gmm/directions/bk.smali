.class Lcom/google/android/apps/gmm/directions/bk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/bj;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/bj;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/bk;->a:Lcom/google/android/apps/gmm/directions/bj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 110
    const-string v0, "TrafficUpdate.run()"

    .line 111
    new-instance v6, Lcom/google/android/apps/gmm/directions/bm;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bk;->a:Lcom/google/android/apps/gmm/directions/bj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/bj;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v6, v0}, Lcom/google/android/apps/gmm/directions/bm;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bk;->a:Lcom/google/android/apps/gmm/directions/bj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/bj;->d:[Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v5, Lcom/google/android/apps/gmm/directions/bn;

    invoke-direct {v5, v6, v6}, Lcom/google/android/apps/gmm/directions/bn;-><init>(Lcom/google/android/apps/gmm/directions/bm;Lcom/google/android/apps/gmm/directions/bm;)V

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, v6, Lcom/google/android/apps/gmm/directions/bm;->b:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 113
    :cond_2
    :goto_1
    return-void

    .line 112
    :cond_3
    iget-object v0, v6, Lcom/google/android/apps/gmm/directions/bm;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l_()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/r/a/w;->K:Lcom/google/n/f;

    if-eqz v7, :cond_4

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iput-object v4, v6, Lcom/google/android/apps/gmm/directions/bm;->b:Ljava/util/List;

    iget-object v2, v6, Lcom/google/android/apps/gmm/directions/bm;->b:Ljava/util/List;

    iget-object v0, v6, Lcom/google/android/apps/gmm/directions/bm;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s()Lcom/google/android/apps/gmm/car/a/g;

    move-result-object v0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/car/a/g;->a:Z

    new-instance v7, Lcom/google/android/apps/gmm/directions/f/b;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/directions/f/b;-><init>()V

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    iput-object v0, v7, Lcom/google/android/apps/gmm/directions/f/b;->a:Lcom/google/maps/g/a/hm;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->y:Lcom/google/r/b/a/afz;

    iput-object v0, v7, Lcom/google/android/apps/gmm/directions/f/b;->b:Lcom/google/r/b/a/afz;

    iput-boolean v4, v7, Lcom/google/android/apps/gmm/directions/f/b;->i:Z

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/directions/f/b;->a()Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v1

    invoke-static {}, Lcom/google/r/b/a/aha;->newBuilder()Lcom/google/r/b/a/ahc;

    move-result-object v4

    sget-object v0, Lcom/google/maps/g/wq;->d:Lcom/google/maps/g/wq;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    iget v7, v4, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit8 v7, v7, 0x1

    iput v7, v4, Lcom/google/r/b/a/ahc;->a:I

    iget v0, v0, Lcom/google/maps/g/wq;->h:I

    iput v0, v4, Lcom/google/r/b/a/ahc;->b:I

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->K:Lcom/google/n/f;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    invoke-virtual {v4}, Lcom/google/r/b/a/ahc;->c()V

    iget-object v7, v4, Lcom/google/r/b/a/ahc;->d:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_8
    new-instance v0, Lcom/google/android/apps/gmm/directions/d/a;

    invoke-virtual {v4}, Lcom/google/r/b/a/ahc;->g()Lcom/google/n/t;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/aha;

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/d/a;-><init>(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/r/b/a/aha;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/directions/d/b;)V

    iget-object v1, v6, Lcom/google/android/apps/gmm/directions/bm;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    goto/16 :goto_1
.end method
