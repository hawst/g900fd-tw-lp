.class Lcom/google/android/apps/gmm/navigation/a/y;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field final a:Ljava/util/Locale;

.field final b:Lcom/google/maps/g/a/al;

.field final synthetic c:Lcom/google/android/apps/gmm/navigation/a/u;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/google/android/apps/gmm/navigation/a/w;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/a/u;Ljava/util/Locale;Lcom/google/maps/g/a/al;Ljava/lang/String;Lcom/google/android/apps/gmm/navigation/a/w;)V
    .locals 7

    .prologue
    .line 354
    invoke-static {p3}, Lcom/google/android/apps/gmm/navigation/a/u;->a(Lcom/google/maps/g/a/al;)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/navigation/a/y;-><init>(Lcom/google/android/apps/gmm/navigation/a/u;Ljava/util/Locale;Lcom/google/maps/g/a/al;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/navigation/a/w;)V

    .line 355
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/navigation/a/u;Ljava/util/Locale;Lcom/google/maps/g/a/al;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/navigation/a/w;)V
    .locals 4

    .prologue
    .line 358
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/y;->c:Lcom/google/android/apps/gmm/navigation/a/u;

    .line 359
    const/16 v0, 0x27

    sget-object v1, Lcom/google/r/b/a/b/ah;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(ILcom/google/e/a/a/a/d;)V

    .line 364
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/a/u;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/aa/b/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    invoke-virtual {p2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    .line 367
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/y;->a:Ljava/util/Locale;

    .line 368
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/a/y;->b:Lcom/google/maps/g/a/al;

    .line 369
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/a/y;->d:Ljava/lang/String;

    .line 370
    iput-object p5, p0, Lcom/google/android/apps/gmm/navigation/a/y;->e:Ljava/lang/String;

    .line 371
    iput-object p6, p0, Lcom/google/android/apps/gmm/navigation/a/y;->f:Lcom/google/android/apps/gmm/navigation/a/w;

    .line 372
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 392
    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_0

    .line 393
    const-string v0, "CannedSpeechManager"

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/y;->a:Ljava/util/Locale;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Empty response for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " voice bundle"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 394
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->k:Lcom/google/android/apps/gmm/shared/net/k;

    .line 425
    :goto_0
    return-object v0

    .line 399
    :cond_0
    const/16 v0, 0x1a

    invoke-virtual {p1, v1, v8, v0}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 401
    const/4 v1, 0x3

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v1, v2

    .line 402
    const-string v2, "CannedSpeechManager"

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/y;->a:Ljava/util/Locale;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/a/y;->b:Lcom/google/maps/g/a/al;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x33

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "readResponseData status: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " locale="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " units="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 404
    const/16 v2, 0xc8

    if-ne v1, v2, :cond_1

    .line 405
    const-string v1, "CannedSpeechManager"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/y;->a:Ljava/util/Locale;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x17

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Got a voice bundle for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 407
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/y;->a:Ljava/util/Locale;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/y;->b:Lcom/google/maps/g/a/al;

    const/4 v1, 0x4

    .line 408
    const/16 v4, 0x13

    invoke-virtual {v0, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/y;->c:Lcom/google/android/apps/gmm/navigation/a/u;

    iget-object v10, v1, Lcom/google/android/apps/gmm/navigation/a/u;->b:Ljava/lang/String;

    .line 407
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/w;

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    move-wide v8, v6

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/gmm/navigation/a/w;-><init>(Ljava/lang/String;Lcom/google/maps/g/a/al;JJJLjava/lang/String;)V

    .line 410
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/y;->c:Lcom/google/android/apps/gmm/navigation/a/u;

    const/4 v3, 0x6

    const/16 v4, 0x19

    invoke-virtual {v0, v3, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-virtual {v2, p0, v1, v0}, Lcom/google/android/apps/gmm/navigation/a/u;->a(Lcom/google/android/apps/gmm/navigation/a/y;Lcom/google/android/apps/gmm/navigation/a/w;[B)V

    .line 425
    :goto_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 411
    :cond_1
    const/16 v0, 0x130

    if-ne v1, v0, :cond_3

    .line 412
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/y;->f:Lcom/google/android/apps/gmm/navigation/a/w;

    if-eqz v0, :cond_2

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/y;->f:Lcom/google/android/apps/gmm/navigation/a/w;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/gmm/navigation/a/w;->e:J

    .line 414
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/y;->c:Lcom/google/android/apps/gmm/navigation/a/u;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/u;->a()V

    goto :goto_1

    .line 416
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/y;->c:Lcom/google/android/apps/gmm/navigation/a/u;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/navigation/a/u;->b(Lcom/google/android/apps/gmm/navigation/a/y;)V

    goto :goto_1

    .line 418
    :cond_3
    const/16 v0, 0x194

    if-ne v1, v0, :cond_4

    .line 419
    const-string v0, "CannedSpeechManager"

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/y;->a:Ljava/util/Locale;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Failed to get voice bundle for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/y;->c:Lcom/google/android/apps/gmm/navigation/a/u;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/navigation/a/u;->a(Lcom/google/android/apps/gmm/navigation/a/y;)V

    goto :goto_1

    .line 422
    :cond_4
    const-string v0, "CannedSpeechManager"

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/y;->a:Ljava/util/Locale;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Failed to get voice bundle for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 423
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/y;->c:Lcom/google/android/apps/gmm/navigation/a/u;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/navigation/a/u;->b(Lcom/google/android/apps/gmm/navigation/a/y;)V

    goto/16 :goto_1
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 5

    .prologue
    .line 376
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/ah;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/y;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 378
    const-string v2, "CannedSpeechManager"

    const-string v3, "referer is null for filename="

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/y;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 380
    :cond_0
    const/4 v2, 0x4

    const-string v3, "/file/"

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/y;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 381
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/y;->e:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/y;->f:Lcom/google/android/apps/gmm/navigation/a/w;

    if-eqz v0, :cond_1

    .line 384
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/y;->f:Lcom/google/android/apps/gmm/navigation/a/w;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/navigation/a/w;->c:J

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 387
    :cond_1
    return-object v1

    .line 378
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 380
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
