.class public Lcom/google/android/apps/gmm/base/activities/MapActivity;
.super Landroid/app/Activity;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/c/a;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/google/android/apps/gmm/map/MapFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/google/android/apps/gmm/base/activities/MapActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/activities/MapActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public final A()Lcom/google/android/apps/gmm/map/c/c;
    .locals 1

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->A()Lcom/google/android/apps/gmm/map/c/c;

    move-result-object v0

    return-object v0
.end method

.method public final A_()Lcom/google/android/apps/gmm/map/internal/b/f;
    .locals 1

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->A_()Lcom/google/android/apps/gmm/map/internal/b/f;

    move-result-object v0

    return-object v0
.end method

.method public final B()Lcom/google/android/apps/gmm/map/internal/c/o;
    .locals 1

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v0

    return-object v0
.end method

.method public final B_()Landroid/accounts/Account;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 218
    const/4 v0, 0x0

    return-object v0
.end method

.method public final C_()Lcom/google/android/apps/gmm/map/internal/c/cw;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->C_()Lcom/google/android/apps/gmm/map/internal/c/cw;

    move-result-object v0

    return-object v0
.end method

.method public final D_()Lcom/google/android/apps/gmm/map/i/a/a;
    .locals 1

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->D_()Lcom/google/android/apps/gmm/map/i/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final E_()Lcom/google/android/apps/gmm/shared/c/a/j;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    return-object v0
.end method

.method public final a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/shared/net/r;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/shared/b/a;
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/util/b/g;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/shared/c/f;
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/internal/d/bd;
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/google/android/apps/gmm/map/indoor/a/a;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/google/android/apps/gmm/map/q/b;
    .locals 1

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->n()Lcom/google/android/apps/gmm/map/q/b;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 62
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/MapActivity;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v0, "GMM_MAP_FRAGMENT"

    invoke-virtual {v4, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/MapFragment;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/MapActivity;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/MapActivity;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    if-nez v0, :cond_4

    move v3, v1

    :goto_1
    if-nez p1, :cond_5

    move v0, v1

    :goto_2
    if-eq v3, v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/base/activities/MapActivity;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/activities/MapActivity;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x25

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "mapFragment = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "; savedInstanceState = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v5}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/MapActivity;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/apps/gmm/map/MapFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/MapFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/MapActivity;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    move v0, v1

    :goto_3
    if-eqz v0, :cond_3

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/MapActivity;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    const-string v2, "GMM_MAP_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_3
    invoke-virtual {v4}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 64
    return-void

    :cond_4
    move v3, v2

    .line 63
    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_3
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 69
    sget v0, Lcom/google/android/apps/gmm/h;->g:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->setContentView(I)V

    .line 70
    sget v0, Lcom/google/android/apps/gmm/g;->bF:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    .line 71
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/MapActivity;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->a(Lcom/google/android/apps/gmm/map/MapFragment;)V

    .line 72
    return-void
.end method

.method public final q()Lcom/google/android/apps/gmm/map/e/a;
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    return-object v0
.end method

.method public final r()Lcom/google/android/apps/gmm/map/c/a/a;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->r()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final t()Lcom/google/android/apps/gmm/shared/net/a/b;
    .locals 1

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    return-object v0
.end method

.method public final t_()Lcom/google/android/apps/gmm/map/util/a/b;
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v0

    return-object v0
.end method

.method public final u()Lcom/google/android/apps/gmm/map/h/d;
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    return-object v0
.end method

.method public final u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v0

    return-object v0
.end method

.method public final v_()Lcom/google/android/apps/gmm/map/legacy/a/a/a;
    .locals 1

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->v_()Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final w_()Lcom/google/android/apps/gmm/m/d;
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v0

    return-object v0
.end method

.method public final x_()Lcom/google/android/apps/gmm/util/replay/a;
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return-object v0
.end method

.method public final y_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->y_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final z()Lcom/google/android/apps/gmm/map/t/d;
    .locals 1

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->z()Lcom/google/android/apps/gmm/map/t/d;

    move-result-object v0

    return-object v0
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/MapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->z_()Z

    move-result v0

    return v0
.end method
