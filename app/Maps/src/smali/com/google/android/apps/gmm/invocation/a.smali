.class public Lcom/google/android/apps/gmm/invocation/a;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/invocation/c;

.field b:Lcom/google/e/a/a/a/b;

.field private c:Lcom/google/e/a/a/a/b;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 332
    sget-object v0, Lcom/google/r/b/a/el;->cd:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/n;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 334
    return-void
.end method


# virtual methods
.method protected final M_()J
    .locals 4

    .prologue
    .line 320
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 1

    .prologue
    .line 298
    iput-object p1, p0, Lcom/google/android/apps/gmm/invocation/a;->c:Lcom/google/e/a/a/a/b;

    .line 299
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 325
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->g:Lcom/google/android/apps/gmm/shared/net/k;

    if-ne p1, v0, :cond_0

    .line 326
    const/4 v0, 0x0

    .line 328
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/shared/net/af;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v0

    goto :goto_0
.end method

.method public final g_()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/apps/gmm/invocation/a;->b:Lcom/google/e/a/a/a/b;

    return-object v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/invocation/a;->b:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    .line 311
    if-nez p1, :cond_0

    .line 312
    iget-object v1, p0, Lcom/google/android/apps/gmm/invocation/a;->a:Lcom/google/android/apps/gmm/invocation/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/invocation/a;->c:Lcom/google/e/a/a/a/b;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/invocation/c;->a(Ljava/lang/String;Lcom/google/e/a/a/a/b;)V

    .line 316
    :goto_0
    return-void

    .line 314
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/invocation/a;->a:Lcom/google/android/apps/gmm/invocation/c;

    invoke-interface {v1, v0, p0, p1}, Lcom/google/android/apps/gmm/invocation/c;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/invocation/a;Lcom/google/android/apps/gmm/shared/net/k;)V

    goto :goto_0
.end method
