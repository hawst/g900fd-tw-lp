.class public Lcom/google/android/apps/gmm/map/o/b/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/internal/c/be;

.field public final b:Lcom/google/android/apps/gmm/map/internal/c/be;

.field public final c:Lcom/google/android/apps/gmm/map/o/b/g;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bn;Lcom/google/android/apps/gmm/map/internal/c/bn;Lcom/google/android/apps/gmm/map/o/b/g;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/be;->b()Lcom/google/android/apps/gmm/map/internal/c/bh;

    move-result-object v0

    .line 79
    iput-object p1, v0, Lcom/google/android/apps/gmm/map/internal/c/bh;->h:Lcom/google/android/apps/gmm/map/internal/c/bn;

    .line 80
    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bh;->b:I

    .line 81
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/internal/c/be;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bh;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/o/b/e;->a:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 83
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/be;->b()Lcom/google/android/apps/gmm/map/internal/c/bh;

    move-result-object v0

    .line 84
    iput-object p2, v0, Lcom/google/android/apps/gmm/map/internal/c/bh;->h:Lcom/google/android/apps/gmm/map/internal/c/bn;

    .line 85
    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bh;->b:I

    .line 86
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/internal/c/be;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bh;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/o/b/e;->b:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 88
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/o/b/e;->c:Lcom/google/android/apps/gmm/map/o/b/g;

    .line 89
    return-void
.end method

.method public static final a(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/o/b/h;Z)Lcom/google/android/apps/gmm/map/o/b/e;
    .locals 9

    .prologue
    .line 55
    new-instance v8, Lcom/google/android/apps/gmm/map/o/b/e;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bn;

    if-eqz p2, :cond_0

    sget v1, Lcom/google/android/apps/gmm/d;->ao:I

    .line 57
    :goto_0
    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/gmm/map/o/b/h;->b:Lcom/google/android/apps/gmm/map/o/b/h;

    if-ne p1, v3, :cond_1

    const/16 v3, 0x18

    :goto_1
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/gmm/map/o/b/h;->b:Lcom/google/android/apps/gmm/map/o/b/h;

    if-ne p1, v6, :cond_2

    const/16 v6, 0x40

    :goto_2
    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/c/bn;-><init>(IIIFFI)V

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bn;

    sget v2, Lcom/google/android/apps/gmm/d;->ar:I

    .line 66
    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/gmm/map/o/b/h;->b:Lcom/google/android/apps/gmm/map/o/b/h;

    if-ne p1, v4, :cond_3

    const/16 v4, 0x18

    :goto_3
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/apps/gmm/map/o/b/h;->b:Lcom/google/android/apps/gmm/map/o/b/h;

    if-ne p1, v7, :cond_4

    const/16 v7, 0x40

    :goto_4
    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/internal/c/bn;-><init>(IIIFFI)V

    if-eqz p2, :cond_5

    sget-object v2, Lcom/google/android/apps/gmm/map/o/b/g;->p:Lcom/google/android/apps/gmm/map/o/b/g;

    :goto_5
    invoke-direct {v8, v0, v1, v2}, Lcom/google/android/apps/gmm/map/o/b/e;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bn;Lcom/google/android/apps/gmm/map/internal/c/bn;Lcom/google/android/apps/gmm/map/o/b/g;)V

    return-object v8

    .line 55
    :cond_0
    sget v1, Lcom/google/android/apps/gmm/d;->ar:I

    goto :goto_0

    .line 57
    :cond_1
    const/16 v3, 0xe

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    goto :goto_2

    .line 66
    :cond_3
    const/16 v4, 0xe

    goto :goto_3

    :cond_4
    const/4 v7, 0x0

    goto :goto_4

    :cond_5
    sget-object v2, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    goto :goto_5
.end method
