.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;
.super Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;
.source "PG"


# instance fields
.field private A:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

.field private B:Lcom/google/android/apps/gmm/map/o/f;

.field private C:Lcom/google/android/apps/gmm/v/by;

.field private D:[F

.field private E:F

.field private F:F

.field private G:F

.field private H:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

.field private I:Z

.field private J:Z

.field private K:Z

.field private L:J

.field private M:J

.field private final a:Lcom/google/android/apps/gmm/map/o/b/i;

.field private final b:Lcom/google/android/apps/gmm/map/b/a/y;

.field private c:F

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Lcom/google/android/apps/gmm/map/internal/c/aa;

.field private g:Lcom/google/android/apps/gmm/map/b/a/ab;

.field private h:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

.field private z:Lcom/google/android/apps/gmm/map/o/h;


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 186
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;-><init>()V

    .line 94
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/b/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->a:Lcom/google/android/apps/gmm/map/o/b/i;

    .line 100
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 181
    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->L:J

    .line 184
    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->M:J

    .line 187
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/o/ap;Lcom/google/android/apps/gmm/map/f/o;)Z
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 417
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    const/high16 v4, 0x41680000    # 14.5f

    cmpl-float v0, v0, v4

    if-lez v0, :cond_1

    move v0, v1

    .line 418
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->u:Lcom/google/android/apps/gmm/map/o/ap;

    if-ne v4, p1, :cond_2

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->I:Z

    if-ne v4, v0, :cond_2

    .line 467
    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 417
    goto :goto_0

    .line 422
    :cond_2
    iget v4, p2, Lcom/google/android/apps/gmm/map/f/o;->i:F

    .line 424
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->u:Lcom/google/android/apps/gmm/map/o/ap;

    if-eq v5, p1, :cond_0

    .line 427
    iget-object v5, p1, Lcom/google/android/apps/gmm/map/o/ap;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    iput-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->H:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    .line 428
    iget v5, p1, Lcom/google/android/apps/gmm/map/o/ap;->h:F

    .line 429
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->l:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget v7, p1, Lcom/google/android/apps/gmm/map/o/ap;->i:I

    iget v8, p1, Lcom/google/android/apps/gmm/map/o/ap;->j:I

    invoke-static {v6, v5, v7, v8, v4}, Lcom/google/android/apps/gmm/map/o/an;->a(Lcom/google/android/apps/gmm/map/internal/c/be;FIIF)F

    move-result v4

    .line 435
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->u:Lcom/google/android/apps/gmm/map/o/ap;

    .line 436
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->I:Z

    .line 438
    iget v0, p1, Lcom/google/android/apps/gmm/map/o/ap;->k:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->G:F

    .line 441
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 443
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->e:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 444
    iput v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->E:F

    .line 445
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->h:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->e:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->H:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->l:Lcom/google/android/apps/gmm/map/internal/c/be;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->l:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 446
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    :goto_2
    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->E:F

    .line 445
    invoke-virtual {v4, v6, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a(Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;Lcom/google/android/apps/gmm/map/internal/c/bn;)V

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    iget v3, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->e:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    add-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->F:F

    .line 455
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->C:Lcom/google/android/apps/gmm/v/by;

    if-eqz v0, :cond_5

    .line 456
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->C:Lcom/google/android/apps/gmm/v/by;

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/bw;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/v/bz;->a(Lcom/google/android/apps/gmm/v/by;)V

    .line 457
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->C:Lcom/google/android/apps/gmm/v/by;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 460
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 462
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->F:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_0

    .line 463
    const-string v0, "GLLineLabel"

    const-string v3, "Label text length is 0: Text: %s FeatureID: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->e:Ljava/lang/String;

    aput-object v5, v4, v2

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->j:Lcom/google/android/apps/gmm/map/internal/c/m;

    .line 464
    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/internal/c/m;->e()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v5

    aput-object v5, v4, v1

    .line 463
    invoke-static {v0, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v2

    .line 465
    goto/16 :goto_1

    :cond_6
    move-object v0, v3

    .line 446
    goto :goto_2

    .line 447
    :cond_7
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->f:Lcom/google/android/apps/gmm/map/internal/c/aa;

    if-eqz v0, :cond_3

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->z:Lcom/google/android/apps/gmm/map/o/h;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->f:Lcom/google/android/apps/gmm/map/internal/c/aa;

    const/high16 v4, 0x3f800000    # 1.0f

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->A:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    new-instance v6, Lcom/google/android/apps/gmm/map/o/c/b;

    invoke-direct {v6, v0, v3, v4, v5}, Lcom/google/android/apps/gmm/map/o/c/b;-><init>(Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/internal/c/aa;FLcom/google/android/apps/gmm/map/internal/d/c/b/f;)V

    .line 450
    invoke-virtual {v6, p1}, Lcom/google/android/apps/gmm/map/o/c/a;->a(Lcom/google/android/apps/gmm/map/o/ap;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 451
    iget v0, v6, Lcom/google/android/apps/gmm/map/o/c/a;->f:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->E:F

    .line 452
    iget v0, v6, Lcom/google/android/apps/gmm/map/o/c/a;->e:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->F:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 460
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method private a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;FFFLcom/google/android/apps/gmm/map/l/b;)Z
    .locals 9

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->g:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v1, v0, 0x3

    .line 366
    const/4 v0, 0x0

    iput v0, p6, Lcom/google/android/apps/gmm/map/l/b;->d:I

    const/4 v0, 0x0

    iput-boolean v0, p6, Lcom/google/android/apps/gmm/map/l/b;->c:Z

    .line 367
    invoke-virtual {p6, v1}, Lcom/google/android/apps/gmm/map/l/b;->a(I)V

    .line 369
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/o/au;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 370
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/o/au;->f:[F

    .line 371
    iget-object v4, p1, Lcom/google/android/apps/gmm/map/o/au;->d:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 372
    const/4 v0, 0x0

    const/4 v5, 0x0

    iput v0, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 373
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-eqz v0, :cond_0

    .line 374
    iget v0, p2, Lcom/google/android/apps/gmm/map/f/o;->i:F

    .line 375
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->g:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->e()F

    move-result v6

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->e()F

    move-result v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    cmpg-float v8, v7, v0

    if-gez v8, :cond_2

    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->g(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    :goto_0
    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v5, v5

    iput v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v0, v0

    iput v0, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v0

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v6, v6

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 380
    neg-float v0, p3

    iget v5, p2, Lcom/google/android/apps/gmm/map/f/o;->i:F

    mul-float/2addr v0, v5

    const/high16 v5, 0x3f800000    # 1.0f

    iget v6, p2, Lcom/google/android/apps/gmm/map/f/o;->g:F

    mul-float/2addr v5, v6

    iget v6, p2, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v7, p2, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v6, v7

    div-float/2addr v5, v6

    mul-float/2addr v0, v5

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v5, v0

    iput v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v0, v5

    iput v0, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->u:Lcom/google/android/apps/gmm/map/o/ap;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->u:Lcom/google/android/apps/gmm/map/o/ap;

    iget v0, v0, Lcom/google/android/apps/gmm/map/o/ap;->h:F

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v5, v0

    iput v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v0, v5

    iput v0, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 385
    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_4

    .line 386
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->g:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v5, v0, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 387
    const/4 v5, 0x0

    cmpl-float v5, p3, v5

    if-eqz v5, :cond_1

    .line 388
    iget v5, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v5, v5

    iget v6, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    add-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 389
    iget v5, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v5, v5

    iget v6, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    add-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 391
    :cond_1
    invoke-virtual {p2, v2, v3}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v5

    if-nez v5, :cond_3

    .line 394
    const/4 v0, 0x0

    .line 413
    :goto_2
    return v0

    .line 375
    :cond_2
    add-float v8, v7, v0

    div-float/2addr v8, v6

    sub-float v0, v7, v0

    div-float/2addr v0, v6

    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v7, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v5, v8, v6}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(FLcom/google/android/apps/gmm/map/b/a/y;)I

    invoke-virtual {v5, v0, v7}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(FLcom/google/android/apps/gmm/map/b/a/y;)I

    invoke-virtual {v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->g(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    goto/16 :goto_0

    .line 396
    :cond_3
    const/4 v5, 0x0

    aget v5, v3, v5

    const/4 v6, 0x1

    aget v6, v3, v6

    invoke-virtual {p6, v5, v6}, Lcom/google/android/apps/gmm/map/l/b;->a(FF)V

    .line 385
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 399
    :cond_4
    const v0, 0x3f866666    # 1.05f

    mul-float v2, p4, v0

    .line 401
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->d:I

    .line 402
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_8

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v4, :cond_5

    iget v5, p6, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v5, v5, -0x1

    invoke-static {v0, v5}, Lcom/google/b/a/aq;->a(II)I

    invoke-virtual {p6}, Lcom/google/android/apps/gmm/map/l/b;->b()V

    iget-object v5, p6, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    add-int/lit8 v6, v0, 0x1

    aget v5, v5, v6

    iget-object v6, p6, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    aget v6, v6, v0

    sub-float/2addr v5, v6

    add-float/2addr v1, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {p6, v4, v0}, Lcom/google/android/apps/gmm/map/l/b;->a(ILcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/o/au;->f:[F

    invoke-virtual {p2, v3, v4}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v5

    if-nez v5, :cond_7

    const-string v0, "GLLineLabel"

    const-string v1, "computeLengthToAnchor (%f %f %f) had no valid screen proj."

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v5

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 405
    :goto_4
    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 406
    add-float v1, v0, v2

    .line 408
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/o/au;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/o/au;->c:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v5, p6, Lcom/google/android/apps/gmm/map/l/b;->d:I

    const/4 v6, 0x2

    if-lt v5, v6, :cond_6

    cmpg-float v5, v1, v0

    if-gtz v5, :cond_a

    .line 409
    :cond_6
    :goto_5
    iget v0, p6, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v3, v0, -0x1

    const/4 v0, 0x2

    if-ge v3, v0, :cond_d

    const/4 v0, 0x0

    :goto_6
    if-eqz v0, :cond_12

    .line 410
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 402
    :cond_7
    const/4 v3, 0x0

    aget v3, v4, v3

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    sub-float/2addr v3, v5

    const/4 v5, 0x1

    aget v4, v4, v5

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    sub-float v0, v4, v0

    mul-float/2addr v3, v3

    mul-float/2addr v0, v0

    add-float/2addr v0, v3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v0, v4

    add-float/2addr v0, v1

    goto :goto_4

    :cond_8
    iget v0, p6, Lcom/google/android/apps/gmm/map/l/b;->d:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_9

    const/4 v0, 0x0

    :goto_7
    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    goto :goto_4

    :cond_9
    invoke-virtual {p6}, Lcom/google/android/apps/gmm/map/l/b;->b()V

    iget-object v0, p6, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    iget v1, p6, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_7

    .line 408
    :cond_a
    invoke-virtual {p6, v0, v2}, Lcom/google/android/apps/gmm/map/l/b;->b(FLcom/google/android/apps/gmm/map/b/a/ay;)I

    move-result v5

    invoke-virtual {p6, v1, v3}, Lcom/google/android/apps/gmm/map/l/b;->b(FLcom/google/android/apps/gmm/map/b/a/ay;)I

    move-result v0

    invoke-virtual {p6, v0, v4}, Lcom/google/android/apps/gmm/map/l/b;->a(ILcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/gmm/map/b/a/ay;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    add-int/lit8 v0, v0, 0x1

    :cond_b
    iget-object v1, p6, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    const/4 v4, 0x0

    iget v6, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    aput v6, v1, v4

    iget-object v1, p6, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    const/4 v4, 0x1

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    aput v2, v1, v4

    const/4 v2, 0x1

    add-int/lit8 v1, v5, 0x1

    :goto_8
    if-ge v1, v0, :cond_c

    iget-object v4, p6, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    shl-int/lit8 v5, v2, 0x1

    iget-object v6, p6, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    shl-int/lit8 v7, v1, 0x1

    aget v6, v6, v7

    aput v6, v4, v5

    iget-object v4, p6, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    shl-int/lit8 v5, v2, 0x1

    add-int/lit8 v5, v5, 0x1

    iget-object v6, p6, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    shl-int/lit8 v7, v1, 0x1

    add-int/lit8 v7, v7, 0x1

    aget v6, v6, v7

    aput v6, v4, v5

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p6, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    shl-int/lit8 v1, v2, 0x1

    iget v4, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    aput v4, v0, v1

    iget-object v0, p6, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    shl-int/lit8 v1, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    aput v3, v0, v1

    add-int/lit8 v0, v2, 0x1

    iput v0, p6, Lcom/google/android/apps/gmm/map/l/b;->d:I

    const/4 v0, 0x0

    iput-boolean v0, p6, Lcom/google/android/apps/gmm/map/l/b;->c:Z

    goto/16 :goto_5

    .line 409
    :cond_d
    const/4 v0, 0x0

    invoke-virtual {p6, v0}, Lcom/google/android/apps/gmm/map/l/b;->b(I)F

    move-result v1

    const/4 v0, 0x1

    move v2, v0

    move v0, v1

    :goto_9
    if-ge v2, v3, :cond_11

    iget v1, p6, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v2, v1}, Lcom/google/b/a/aq;->a(II)I

    invoke-virtual {p6}, Lcom/google/android/apps/gmm/map/l/b;->b()V

    iget-object v1, p6, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    add-int/lit8 v4, v2, 0x1

    aget v1, v1, v4

    iget-object v4, p6, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    aget v4, v4, v2

    sub-float/2addr v1, v4

    cmpg-float v1, v1, p5

    if-gez v1, :cond_e

    add-int/lit8 v1, v3, -0x1

    if-lt v2, v1, :cond_10

    :cond_e
    invoke-virtual {p6, v2}, Lcom/google/android/apps/gmm/map/l/b;->b(I)F

    move-result v1

    sub-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v4, v0

    const-wide v6, 0x3ff0c152382d7365L    # 1.0471975511965976

    cmpl-double v0, v4, v6

    if-lez v0, :cond_f

    const-wide v6, 0x4014f1a6c638d03fL    # 5.235987755982989

    cmpg-double v0, v4, v6

    if-gez v0, :cond_f

    const/4 v0, 0x1

    goto/16 :goto_6

    :cond_f
    move v0, v1

    :cond_10
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_9

    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 413
    :cond_12
    const/4 v0, 0x1

    goto/16 :goto_2
.end method

.method private a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;Z)Z
    .locals 12

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->g:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/o/au;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p2, v1}, Lcom/google/android/apps/gmm/map/f/o;->b(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 472
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/f/o;->A:Lcom/google/android/apps/gmm/map/f/j;

    .line 473
    iget-boolean v2, v1, Lcom/google/android/apps/gmm/map/f/j;->g:Z

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->L:J

    .line 474
    iget-object v4, v1, Lcom/google/android/apps/gmm/map/f/j;->a:Lcom/google/android/apps/gmm/map/f/k;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/f/k;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 477
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->a:Lcom/google/android/apps/gmm/map/o/b/i;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v1, Lcom/google/android/apps/gmm/map/f/j;->b:F

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/j;->c:F

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v1, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v1, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    neg-float v1, v1

    iput v1, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v1, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v1, v1

    iput v1, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/map/o/b/i;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Z)V

    .line 540
    :goto_0
    iget-wide v0, p2, Lcom/google/android/apps/gmm/map/f/o;->t:J

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->L:J

    .line 542
    if-eqz p3, :cond_0

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->a:Lcom/google/android/apps/gmm/map/o/b/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->r:Lcom/google/android/apps/gmm/map/o/b/i;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->r:Lcom/google/android/apps/gmm/map/o/b/i;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/o/b/i;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/b/i;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v5, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/o/b/i;->b:D

    iput-wide v4, v2, Lcom/google/android/apps/gmm/map/o/b/i;->b:D

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/o/b/i;->c:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/b/i;->c:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v5, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/o/b/i;->d:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/b/i;->d:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v5, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/o/b/i;->e:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/b/i;->e:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v5, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/o/b/i;->f:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/b/i;->f:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v5, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/o/b/i;->g:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/b/i;->g:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v5, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/o/b/i;->h:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/b/i;->h:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v5, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/o/b/i;->i:Lcom/google/android/apps/gmm/map/o/b/a;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/b/i;->i:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v3, v3, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/b/i;->i:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v4, v4, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/o/b/i;->i:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v5, v5, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/b/i;->i:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    invoke-virtual {v2, v3, v4, v5, v0}, Lcom/google/android/apps/gmm/map/o/b/a;->a(FFFF)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 545
    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 479
    :cond_1
    iget-object v6, p1, Lcom/google/android/apps/gmm/map/o/au;->g:Lcom/google/android/apps/gmm/map/l/b;

    .line 480
    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->F:F

    mul-float v4, v1, v0

    .line 481
    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->E:F

    mul-float v5, v1, v0

    .line 482
    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->c:F

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;FFFLcom/google/android/apps/gmm/map/l/b;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 484
    const/4 v0, 0x0

    goto :goto_1

    .line 487
    :cond_2
    iget v2, v6, Lcom/google/android/apps/gmm/map/l/b;->d:I

    .line 489
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 495
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->c:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    const/high16 v0, 0x3f800000    # 1.0f

    move v1, v0

    .line 497
    :goto_2
    const/4 v0, 0x2

    if-ne v2, v0, :cond_5

    .line 498
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {v6, v0, v3}, Lcom/google/android/apps/gmm/map/l/b;->a(FLcom/google/android/apps/gmm/map/b/a/ay;)I

    .line 499
    iget v0, v6, Lcom/google/android/apps/gmm/map/l/b;->d:I

    const/4 v2, 0x2

    if-ge v0, v2, :cond_4

    const/4 v0, 0x0

    :goto_3
    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v0, v2

    .line 500
    mul-float/2addr v1, v5

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float v7, v1, v2

    .line 537
    :goto_4
    iget v1, v6, Lcom/google/android/apps/gmm/map/l/b;->d:I

    const/4 v2, 0x2

    if-ge v1, v2, :cond_6

    const/4 v1, 0x0

    move v4, v1

    .line 538
    :goto_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->a:Lcom/google/android/apps/gmm/map/o/b/i;

    iget v2, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v4, v4

    const/4 v8, 0x1

    move v6, v0

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/o/b/i;->a(FFDFFZ)V

    goto/16 :goto_0

    .line 495
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->G:F

    move v1, v0

    goto :goto_2

    .line 499
    :cond_4
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/l/b;->b()V

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    iget v2, v6, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v2, v2, -0x1

    aget v0, v0, v2

    goto :goto_3

    .line 511
    :cond_5
    iget-object v4, p1, Lcom/google/android/apps/gmm/map/o/au;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 512
    iget-object v7, p1, Lcom/google/android/apps/gmm/map/o/au;->c:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 513
    iget-object v8, p1, Lcom/google/android/apps/gmm/map/o/au;->d:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 515
    const/4 v0, 0x0

    invoke-virtual {v6, v0, v4}, Lcom/google/android/apps/gmm/map/l/b;->a(ILcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 516
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {v6, v0, v7}, Lcom/google/android/apps/gmm/map/l/b;->a(ILcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 517
    div-int/lit8 v0, v2, 0x2

    invoke-virtual {v6, v0, v8}, Lcom/google/android/apps/gmm/map/l/b;->a(ILcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 518
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-static {v4, v7, v0, v3}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;FLcom/google/android/apps/gmm/map/b/a/ay;)V

    .line 521
    invoke-virtual {v7, v4}, Lcom/google/android/apps/gmm/map/b/a/ay;->b(Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 522
    iget v0, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v2, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v0, v2

    iget v2, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v9, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v2, v9

    add-float/2addr v0, v2

    float-to-double v10, v0

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v2, v10

    .line 523
    const/high16 v0, 0x3f000000    # 0.5f

    mul-float/2addr v0, v2

    .line 526
    invoke-virtual {v8, v4}, Lcom/google/android/apps/gmm/map/b/a/ay;->b(Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 527
    iget v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v9, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v4, v9

    iget v9, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v10, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v9, v10

    add-float/2addr v4, v9

    mul-float/2addr v2, v2

    div-float v2, v4, v2

    .line 528
    iget v4, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v4, v2

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v2, v4

    iput v2, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 531
    invoke-virtual {v8, v7}, Lcom/google/android/apps/gmm/map/b/a/ay;->b(Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v2

    const/high16 v4, 0x3f000000    # 0.5f

    iget v7, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v7, v4

    iput v7, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v7, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v4, v7

    iput v4, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 534
    iget v2, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v2, v4

    iget v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v7, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v4, v7

    add-float/2addr v2, v4

    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v2, v8

    add-float/2addr v2, v5

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float v7, v1, v2

    goto/16 :goto_4

    .line 537
    :cond_6
    iget-object v1, v6, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    const/4 v4, 0x1

    aget v2, v2, v4

    iget-object v4, v6, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    iget v5, v6, Lcom/google/android/apps/gmm/map/l/b;->d:I

    shl-int/lit8 v5, v5, 0x1

    add-int/lit8 v5, v5, -0x2

    aget v4, v4, v5

    iget-object v5, v6, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    iget v6, v6, Lcom/google/android/apps/gmm/map/l/b;->d:I

    shl-int/lit8 v6, v6, 0x1

    add-int/lit8 v6, v6, -0x1

    aget v5, v5, v6

    sub-float v2, v5, v2

    float-to-double v8, v2

    sub-float v1, v4, v1

    float-to-double v4, v1

    invoke-static {v8, v9, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v1, v4

    move v4, v1

    goto/16 :goto_5

    .line 543
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/map/internal/c/m;Ljava/lang/Integer;Lcom/google/android/apps/gmm/map/o/ak;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/aa;ZFIFLcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/b/a/y;ILcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)V
    .locals 10

    .prologue
    .line 208
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 209
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/o/af;->a(Lcom/google/android/apps/gmm/map/internal/c/m;)Lcom/google/android/apps/gmm/map/o/af;

    move-result-object v8

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p5

    move/from16 v6, p10

    move/from16 v7, p9

    move-object/from16 v9, p14

    .line 208
    invoke-super/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a(Lcom/google/android/apps/gmm/map/internal/c/m;ILcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/internal/c/be;FILcom/google/android/apps/gmm/map/o/af;Lcom/google/android/apps/gmm/map/t/l;)V

    .line 210
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->e:Ljava/lang/String;

    .line 211
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->f:Lcom/google/android/apps/gmm/map/internal/c/aa;

    .line 212
    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->K:Z

    .line 213
    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->c:F

    .line 214
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->h:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    .line 215
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->z:Lcom/google/android/apps/gmm/map/o/h;

    .line 216
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->A:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    .line 217
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->g:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 218
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p12

    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, p12

    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, p12

    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 219
    move/from16 v0, p13

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->d:I

    .line 220
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/b;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 253
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 255
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->C:Lcom/google/android/apps/gmm/v/by;

    if-eqz v1, :cond_2

    .line 256
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->t:Lcom/google/android/apps/gmm/map/t/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, p1, :cond_0

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 284
    :goto_0
    return-void

    .line 259
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->C:Lcom/google/android/apps/gmm/v/by;

    iget-object v2, v1, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/bw;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/v/bz;->a(Lcom/google/android/apps/gmm/v/by;)V

    .line 260
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->C:Lcom/google/android/apps/gmm/v/by;

    .line 262
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->t:Lcom/google/android/apps/gmm/map/t/b;

    .line 264
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->e:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 265
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->l:Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-static {v1, p1}, Lcom/google/android/apps/gmm/map/o/an;->b(Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/t/b;)I

    move-result v5

    .line 266
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->l:Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-static {v1, p1}, Lcom/google/android/apps/gmm/map/o/an;->a(Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/t/b;)I

    move-result v6

    .line 267
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->l:Lcom/google/android/apps/gmm/map/internal/c/be;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->l:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    .line 268
    :cond_3
    if-eqz v6, :cond_4

    move v10, v0

    .line 270
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->h:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->H:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    iget v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->E:F

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;Lcom/google/android/apps/gmm/map/internal/c/bn;FIII)Lcom/google/android/apps/gmm/v/by;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->C:Lcom/google/android/apps/gmm/v/by;

    .line 272
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->h:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->H:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->E:F

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    new-array v5, v5, [F

    invoke-virtual {v4, v1, v3}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a(Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;Lcom/google/android/apps/gmm/map/internal/c/bn;)V

    iget-object v1, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v1, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v0, v5}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    iget v0, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->e:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v3, v0

    if-eqz v10, :cond_5

    int-to-float v0, v3

    :goto_2
    move v1, v0

    move v0, v9

    :goto_3
    array-length v2, v5

    if-ge v0, v2, :cond_6

    aget v2, v5, v0

    add-float/2addr v2, v1

    aput v1, v5, v0

    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_3

    :cond_4
    move v10, v9

    .line 268
    goto :goto_1

    :cond_5
    move v0, v8

    .line 272
    goto :goto_2

    :cond_6
    if-eqz v10, :cond_7

    const/4 v0, 0x0

    aget v1, v5, v0

    int-to-float v2, v3

    sub-float/2addr v1, v2

    aput v1, v5, v0

    array-length v0, v5

    add-int/lit8 v0, v0, -0x1

    aget v1, v5, v0

    int-to-float v2, v3

    add-float/2addr v1, v2

    aput v1, v5, v0

    :cond_7
    array-length v0, v5

    add-int/lit8 v0, v0, -0x1

    aget v1, v5, v0

    iget v2, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->f:F

    mul-float/2addr v1, v2

    aput v1, v5, v0

    iput-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->D:[F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 283
    :cond_8
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto/16 :goto_0

    .line 274
    :cond_9
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->f:Lcom/google/android/apps/gmm/map/internal/c/aa;

    if-eqz v0, :cond_8

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->z:Lcom/google/android/apps/gmm/map/o/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->f:Lcom/google/android/apps/gmm/map/internal/c/aa;

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->A:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    new-instance v4, Lcom/google/android/apps/gmm/map/o/c/b;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/o/c/b;-><init>(Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/internal/c/aa;FLcom/google/android/apps/gmm/map/internal/d/c/b/f;)V

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->u:Lcom/google/android/apps/gmm/map/o/ap;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/o/c/a;->a(Lcom/google/android/apps/gmm/map/o/ap;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 278
    invoke-virtual {v4, p1}, Lcom/google/android/apps/gmm/map/o/c/a;->a(Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/v/by;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->C:Lcom/google/android/apps/gmm/v/by;

    .line 279
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, v4, Lcom/google/android/apps/gmm/map/o/c/a;->e:F

    aput v2, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->D:[F
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 283
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/bc;Z)Z
    .locals 26

    .prologue
    .line 290
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->n:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 292
    const/4 v2, 0x1

    .line 349
    :goto_0
    return v2

    .line 295
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v2

    if-nez v2, :cond_1

    .line 297
    const/4 v2, 0x0

    goto :goto_0

    .line 301
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->C:Lcom/google/android/apps/gmm/v/by;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_2

    .line 302
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    const/4 v2, 0x1

    goto :goto_0

    .line 305
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->g:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/au;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v4, v4

    div-int/lit8 v4, v4, 0x3

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/f/o;->b(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v18

    .line 306
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/o;->A:Lcom/google/android/apps/gmm/map/f/j;

    .line 307
    iget-boolean v3, v2, Lcom/google/android/apps/gmm/map/f/j;->g:Z

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->M:J

    .line 308
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/f/j;->a:Lcom/google/android/apps/gmm/map/f/k;

    iget-wide v6, v3, Lcom/google/android/apps/gmm/map/f/k;->d:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    .line 311
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->B:Lcom/google/android/apps/gmm/map/o/f;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v5, v2, Lcom/google/android/apps/gmm/map/f/j;->b:F

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/j;->c:F

    iput v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v2, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v2, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    neg-float v2, v2

    iput v2, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v2, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v2, v2

    iput v2, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/o/f;->a:[Lcom/google/android/apps/gmm/map/o/g;

    array-length v5, v3

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v5, :cond_8

    aget-object v6, v3, v2

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/o/g;->d:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {v6, v4}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 313
    :cond_3
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/o/au;->g:Lcom/google/android/apps/gmm/map/l/b;

    .line 314
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->c:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->F:F

    mul-float v6, v18, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->E:F

    mul-float v7, v18, v2

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;FFFLcom/google/android/apps/gmm/map/l/b;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_4

    .line 316
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    const/4 v2, 0x1

    goto/16 :goto_0

    .line 320
    :cond_4
    :try_start_2
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 321
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/au;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 322
    const/4 v4, 0x0

    invoke-virtual {v8, v4, v2}, Lcom/google/android/apps/gmm/map/l/b;->a(ILcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 323
    iget v4, v8, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v8, v4, v3}, Lcom/google/android/apps/gmm/map/l/b;->a(ILcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 324
    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    sub-float v2, v3, v2

    .line 325
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->K:Z

    if-nez v3, :cond_9

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->J:Z

    if-eqz v3, :cond_5

    const/high16 v3, 0x41700000    # 15.0f

    cmpg-float v3, v2, v3

    if-ltz v3, :cond_6

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->J:Z

    if-nez v3, :cond_9

    const/high16 v3, -0x3e900000    # -15.0f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_9

    .line 328
    :cond_6
    const/4 v2, 0x0

    :goto_2
    iget v3, v8, Lcom/google/android/apps/gmm/map/l/b;->d:I

    div-int/lit8 v3, v3, 0x2

    if-ge v2, v3, :cond_7

    shl-int/lit8 v3, v2, 0x1

    iget v4, v8, Lcom/google/android/apps/gmm/map/l/b;->d:I

    sub-int/2addr v4, v2

    add-int/lit8 v4, v4, -0x1

    shl-int/lit8 v4, v4, 0x1

    iget-object v5, v8, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    aget v5, v5, v3

    iget-object v6, v8, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    add-int/lit8 v7, v3, 0x1

    aget v6, v6, v7

    iget-object v7, v8, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    iget-object v9, v8, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    aget v9, v9, v4

    aput v9, v7, v3

    iget-object v7, v8, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    add-int/lit8 v3, v3, 0x1

    iget-object v9, v8, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    add-int/lit8 v10, v4, 0x1

    aget v9, v9, v10

    aput v9, v7, v3

    iget-object v3, v8, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    aput v5, v3, v4

    iget-object v3, v8, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    add-int/lit8 v4, v4, 0x1

    aput v6, v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_7
    const/4 v2, 0x0

    iput-boolean v2, v8, Lcom/google/android/apps/gmm/map/l/b;->c:Z

    .line 329
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->J:Z

    .line 333
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->C:Lcom/google/android/apps/gmm/v/by;

    iget v3, v2, Lcom/google/android/apps/gmm/v/by;->d:I

    iget v2, v2, Lcom/google/android/apps/gmm/v/by;->b:I

    sub-int v2, v3, v2

    int-to-float v2, v2

    .line 334
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->B:Lcom/google/android/apps/gmm/map/o/f;

    if-nez v3, :cond_a

    .line 335
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->D:[F

    move/from16 v0, v18

    invoke-static {v8, v3, v2, v0}, Lcom/google/android/apps/gmm/map/o/f;->b(Lcom/google/android/apps/gmm/map/l/b;[FFF)Lcom/google/android/apps/gmm/map/o/f;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->B:Lcom/google/android/apps/gmm/map/o/f;

    .line 340
    :cond_8
    :goto_4
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/f/o;->t:J

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->M:J

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->B:Lcom/google/android/apps/gmm/map/o/f;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->C:Lcom/google/android/apps/gmm/v/by;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->v:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->q:Lcom/google/android/apps/gmm/map/t/l;

    const/4 v4, 0x0

    cmpg-float v4, v18, v4

    if-lez v4, :cond_b

    iget-object v4, v2, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/bw;->e:Lcom/google/android/apps/gmm/v/bg;

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v3}, Lcom/google/android/apps/gmm/map/o/bc;->a(Lcom/google/android/apps/gmm/v/bg;Lcom/google/android/apps/gmm/map/t/l;)Lcom/google/android/apps/gmm/map/o/be;

    move-result-object v16

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v3

    int-to-float v14, v3

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v3

    int-to-float v15, v3

    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, v2, Lcom/google/android/apps/gmm/v/by;->i:I

    int-to-float v4, v4

    div-float v20, v3, v4

    iget v10, v2, Lcom/google/android/apps/gmm/v/by;->f:F

    iget v12, v2, Lcom/google/android/apps/gmm/v/by;->h:F

    iget v9, v2, Lcom/google/android/apps/gmm/v/by;->e:F

    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/f;->a:[Lcom/google/android/apps/gmm/map/o/g;

    array-length v0, v2

    move/from16 v21, v0

    const/4 v2, 0x0

    move/from16 v17, v2

    :goto_5
    move/from16 v0, v17

    move/from16 v1, v21

    if-ge v0, v1, :cond_b

    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/f;->a:[Lcom/google/android/apps/gmm/map/o/g;

    aget-object v2, v2, v17

    iget v3, v2, Lcom/google/android/apps/gmm/map/o/g;->a:F

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    mul-float v3, v3, v18

    iget v4, v2, Lcom/google/android/apps/gmm/map/o/g;->b:F

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v4, v5

    mul-float v4, v4, v18

    iget-wide v0, v2, Lcom/google/android/apps/gmm/map/o/g;->c:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    double-to-float v5, v6

    mul-float/2addr v5, v3

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    double-to-float v6, v6

    mul-float/2addr v6, v3

    const-wide v24, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double v24, v24, v22

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->cos(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-float v3, v0

    mul-float v7, v4, v3

    const-wide v24, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sin(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v3, v0

    mul-float v8, v4, v3

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/o/g;->d:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/o/g;->d:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v2, v2, Lcom/google/android/apps/gmm/map/o/g;->a:F

    mul-float v2, v2, v20

    add-float v11, v9, v2

    move-object/from16 v2, p3

    invoke-virtual/range {v2 .. v16}, Lcom/google/android/apps/gmm/map/o/bc;->a(FFFFFFFFFFFFFLcom/google/android/apps/gmm/map/o/be;)V

    add-int/lit8 v2, v17, 0x1

    move/from16 v17, v2

    move v9, v11

    goto :goto_5

    .line 331
    :cond_9
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->J:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_3

    .line 346
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    throw v2

    .line 337
    :cond_a
    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->B:Lcom/google/android/apps/gmm/map/o/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->D:[F

    move/from16 v0, v18

    invoke-virtual {v3, v8, v4, v2, v0}, Lcom/google/android/apps/gmm/map/o/f;->a(Lcom/google/android/apps/gmm/map/l/b;[FFF)V

    goto/16 :goto_4

    .line 342
    :cond_b
    if-eqz p4, :cond_c

    .line 343
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->r:Lcom/google/android/apps/gmm/map/o/b/i;

    monitor-enter v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->r:Lcom/google/android/apps/gmm/map/o/b/i;

    sget-object v4, Lcom/google/android/apps/gmm/map/t/l;->D:Lcom/google/android/apps/gmm/map/t/l;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v4}, Lcom/google/android/apps/gmm/map/o/bc;->a(Lcom/google/android/apps/gmm/map/o/b/b;Lcom/google/android/apps/gmm/map/t/l;)V

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 346
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    .line 349
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 343
    :catchall_1
    move-exception v2

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/o/ap;Lcom/google/android/apps/gmm/map/f/o;Z)Z
    .locals 1

    .prologue
    .line 551
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->a(Lcom/google/android/apps/gmm/map/o/ap;Lcom/google/android/apps/gmm/map/f/o;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 552
    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->o:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final bridge synthetic g()Lcom/google/android/apps/gmm/map/o/b/b;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->a:Lcom/google/android/apps/gmm/map/o/b/i;

    return-object v0
.end method

.method protected final k()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const v0, -0x31fab6fc    # -5.5903872E8f

    const/4 v2, 0x0

    .line 224
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->e:Ljava/lang/String;

    .line 225
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->f:Lcom/google/android/apps/gmm/map/internal/c/aa;

    .line 226
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->g:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 227
    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->F:F

    .line 228
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->h:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    .line 229
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->z:Lcom/google/android/apps/gmm/map/o/h;

    .line 230
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->A:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    .line 231
    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->E:F

    .line 232
    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->G:F

    .line 233
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->H:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->C:Lcom/google/android/apps/gmm/v/by;

    if-eqz v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->C:Lcom/google/android/apps/gmm/v/by;

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/bw;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/v/bz;->a(Lcom/google/android/apps/gmm/v/by;)V

    .line 236
    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->C:Lcom/google/android/apps/gmm/v/by;

    .line 238
    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->B:Lcom/google/android/apps/gmm/map/o/f;

    .line 239
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->D:[F

    .line 240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->J:Z

    .line 241
    iput-wide v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->M:J

    .line 242
    iput-wide v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->L:J

    .line 243
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->k()V

    .line 244
    return-void
.end method
