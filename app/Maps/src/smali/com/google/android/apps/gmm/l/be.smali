.class Lcom/google/android/apps/gmm/l/be;
.super Lcom/google/android/apps/gmm/l/o;
.source "PG"


# instance fields
.field private final b:Lcom/google/android/apps/gmm/l/i;

.field private final c:Lcom/google/android/apps/gmm/aa/a/a/a;

.field private final d:Lcom/google/android/apps/gmm/base/activities/c;

.field private final e:Lcom/google/android/apps/gmm/l/u;


# direct methods
.method constructor <init>(Landroid/content/Intent;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/l/i;)V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/l/bg;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/l/bg;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/l/be;-><init>(Landroid/content/Intent;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/l/i;Lcom/google/android/apps/gmm/l/bg;)V

    .line 27
    return-void
.end method

.method private constructor <init>(Landroid/content/Intent;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/l/i;Lcom/google/android/apps/gmm/l/bg;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/l/o;-><init>(Landroid/content/Intent;)V

    .line 33
    iput-object p2, p0, Lcom/google/android/apps/gmm/l/be;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 34
    iput-object p3, p0, Lcom/google/android/apps/gmm/l/be;->b:Lcom/google/android/apps/gmm/l/i;

    .line 35
    invoke-virtual {p4, p1}, Lcom/google/android/apps/gmm/l/bg;->b(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/be;->e:Lcom/google/android/apps/gmm/l/u;

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/be;->e:Lcom/google/android/apps/gmm/l/u;

    sget-object v1, Lcom/google/android/apps/gmm/l/u;->i:Lcom/google/android/apps/gmm/l/u;

    if-eq v0, v1, :cond_0

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/be;->e:Lcom/google/android/apps/gmm/l/u;

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/u;->g:Lcom/google/android/apps/gmm/aa/a/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/be;->c:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 41
    :goto_0
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/be;->c:Lcom/google/android/apps/gmm/aa/a/a/a;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/be;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/be;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/be;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/be;->c:Lcom/google/android/apps/gmm/aa/a/a/a;

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->s:Lcom/google/android/apps/gmm/aa/a/a/a;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/be;->c:Lcom/google/android/apps/gmm/aa/a/a/a;

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->r:Lcom/google/android/apps/gmm/aa/a/a/a;

    if-ne v1, v2, :cond_1

    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 47
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/be;->b:Lcom/google/android/apps/gmm/l/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/o;->a:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/l/i;->c(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/u;

    .line 48
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/be;->c:Lcom/google/android/apps/gmm/aa/a/a/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/be;->c:Lcom/google/android/apps/gmm/aa/a/a/a;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    goto :goto_0
.end method
