.class public Lcom/google/android/apps/gmm/v/cd;
.super Lcom/google/android/apps/gmm/v/bp;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/r;


# static fields
.field private static final c:[Lcom/google/android/apps/gmm/v/aj;


# instance fields
.field private a:[F

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 47
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/aj;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->b:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->c:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->d:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->e:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->f:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->g:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->h:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->i:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/v/cd;->c:[Lcom/google/android/apps/gmm/v/aj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/google/android/apps/gmm/v/ch;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/bp;-><init>(Ljava/lang/Class;)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    .line 63
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/v/cd;->b:I

    .line 64
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/google/android/apps/gmm/v/ch;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/bp;-><init>(Ljava/lang/Class;)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    .line 68
    iput p1, p0, Lcom/google/android/apps/gmm/v/cd;->b:I

    .line 69
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 78
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/v/cd;-><init>(III)V

    .line 79
    return-void
.end method

.method private constructor <init>(III)V
    .locals 4

    .prologue
    const/high16 v3, 0x437f0000    # 255.0f

    .line 100
    const/4 v0, 0x4

    new-array v0, v0, [F

    const/4 v1, 0x0

    .line 101
    invoke-static {p3}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x1

    .line 102
    invoke-static {p3}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x2

    .line 103
    invoke-static {p3}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x3

    .line 104
    invoke-static {p3}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 100
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/v/cd;-><init>(II[F)V

    .line 106
    return-void
.end method

.method private constructor <init>(II[F)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 109
    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid blend mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-class v0, Lcom/google/android/apps/gmm/v/cg;

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/bp;-><init>(Ljava/lang/Class;)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    .line 110
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    .line 111
    iput p2, p0, Lcom/google/android/apps/gmm/v/cd;->b:I

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    invoke-static {p3, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 113
    return-void

    .line 109
    :pswitch_1
    const-class v0, Lcom/google/android/apps/gmm/v/ch;

    goto :goto_0

    :pswitch_2
    const-class v0, Lcom/google/android/apps/gmm/v/cf;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public constructor <init>(Ljava/lang/Class;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/v/bo;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/v/bp;-><init>(Ljava/lang/Class;)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    .line 117
    iput p2, p0, Lcom/google/android/apps/gmm/v/cd;->b:I

    .line 118
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;II)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/v/bo;",
            ">;II)V"
        }
    .end annotation

    .prologue
    const/high16 v3, 0x437f0000    # 255.0f

    .line 83
    const/4 v0, 0x4

    new-array v0, v0, [F

    const/4 v1, 0x0

    .line 84
    invoke-static {p3}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x1

    .line 85
    invoke-static {p3}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x2

    .line 86
    invoke-static {p3}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x3

    .line 87
    invoke-static {p3}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 83
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/v/cd;-><init>(Ljava/lang/Class;I[F)V

    .line 89
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;I[F)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/v/bo;",
            ">;I[F)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 93
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/v/bp;-><init>(Ljava/lang/Class;)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    .line 94
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    .line 95
    iput p2, p0, Lcom/google/android/apps/gmm/v/cd;->b:I

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    invoke-static {p3, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 97
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/cd;->p:Z

    if-eqz v0, :cond_0

    .line 181
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    if-nez v0, :cond_1

    .line 184
    const/4 v0, 0x4

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    const/4 v1, 0x3

    aput p1, v0, v1

    .line 187
    return-void

    .line 184
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final a(FFFF)V
    .locals 2

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/cd;->p:Z

    if-eqz v0, :cond_0

    .line 167
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    if-nez v0, :cond_1

    .line 170
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    const/4 v1, 0x3

    aput p4, v0, v1

    .line 176
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/high16 v3, 0x437f0000    # 255.0f

    .line 147
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/cd;->p:Z

    if-eqz v0, :cond_0

    .line 148
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    if-nez v0, :cond_1

    .line 151
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    const/4 v1, 0x1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    const/4 v1, 0x2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    const/4 v1, 0x3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 157
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 192
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/v/bp;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/v/ce;

    iget v0, v0, Lcom/google/android/apps/gmm/v/ce;->c:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/cd;->b:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 196
    sget-object v0, Lcom/google/android/apps/gmm/v/cd;->c:[Lcom/google/android/apps/gmm/v/aj;

    iget v1, p0, Lcom/google/android/apps/gmm/v/cd;->b:I

    aget-object v0, v0, v1

    invoke-virtual {p1, v0, p4}, Lcom/google/android/apps/gmm/v/aa;->b(Lcom/google/android/apps/gmm/v/aj;I)Lcom/google/android/apps/gmm/v/ai;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/ci;

    .line 203
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cd;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v1, Lcom/google/android/apps/gmm/v/ce;

    iget v1, v1, Lcom/google/android/apps/gmm/v/ce;->d:I

    const/4 v2, 0x1

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ci;->u:Lcom/google/android/apps/gmm/v/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    invoke-static {v1, v2, v3, v0, v3}, Landroid/opengl/GLES20;->glUniformMatrix3fv(IIZ[FI)V

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    if-eqz v0, :cond_0

    .line 208
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cd;->a:[F

    invoke-static {v0, v1, v3}, Landroid/opengl/GLES20;->glVertexAttrib4fv(I[FI)V

    .line 210
    :cond_0
    return-void
.end method
