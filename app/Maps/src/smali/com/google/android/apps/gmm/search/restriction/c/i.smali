.class public final enum Lcom/google/android/apps/gmm/search/restriction/c/i;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/search/restriction/c/i;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/search/restriction/c/i;

.field public static final enum b:Lcom/google/android/apps/gmm/search/restriction/c/i;

.field public static final enum c:Lcom/google/android/apps/gmm/search/restriction/c/i;

.field public static final enum d:Lcom/google/android/apps/gmm/search/restriction/c/i;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/search/restriction/c/i;


# instance fields
.field final e:Lcom/google/maps/g/pz;

.field final f:Lcom/google/b/f/t;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/i;

    const-string v1, "ANY"

    sget-object v2, Lcom/google/b/f/t;->K:Lcom/google/b/f/t;

    sget-object v3, Lcom/google/maps/g/pz;->a:Lcom/google/maps/g/pz;

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/apps/gmm/search/restriction/c/i;-><init>(Ljava/lang/String;ILcom/google/b/f/t;Lcom/google/maps/g/pz;)V

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/i;->a:Lcom/google/android/apps/gmm/search/restriction/c/i;

    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/i;

    const-string v1, "TWO_PLUS"

    sget-object v2, Lcom/google/b/f/t;->N:Lcom/google/b/f/t;

    sget-object v3, Lcom/google/maps/g/pz;->b:Lcom/google/maps/g/pz;

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/gmm/search/restriction/c/i;-><init>(Ljava/lang/String;ILcom/google/b/f/t;Lcom/google/maps/g/pz;)V

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/i;->b:Lcom/google/android/apps/gmm/search/restriction/c/i;

    .line 33
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/i;

    const-string v1, "THREE_PLUS"

    sget-object v2, Lcom/google/b/f/t;->M:Lcom/google/b/f/t;

    sget-object v3, Lcom/google/maps/g/pz;->c:Lcom/google/maps/g/pz;

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/gmm/search/restriction/c/i;-><init>(Ljava/lang/String;ILcom/google/b/f/t;Lcom/google/maps/g/pz;)V

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/i;->c:Lcom/google/android/apps/gmm/search/restriction/c/i;

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/i;

    const-string v1, "FOUR_PLUS"

    sget-object v2, Lcom/google/b/f/t;->L:Lcom/google/b/f/t;

    sget-object v3, Lcom/google/maps/g/pz;->d:Lcom/google/maps/g/pz;

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/apps/gmm/search/restriction/c/i;-><init>(Ljava/lang/String;ILcom/google/b/f/t;Lcom/google/maps/g/pz;)V

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/i;->d:Lcom/google/android/apps/gmm/search/restriction/c/i;

    .line 30
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/search/restriction/c/i;

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/i;->a:Lcom/google/android/apps/gmm/search/restriction/c/i;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/i;->b:Lcom/google/android/apps/gmm/search/restriction/c/i;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/i;->c:Lcom/google/android/apps/gmm/search/restriction/c/i;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/i;->d:Lcom/google/android/apps/gmm/search/restriction/c/i;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/i;->g:[Lcom/google/android/apps/gmm/search/restriction/c/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/b/f/t;Lcom/google/maps/g/pz;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/f/t;",
            "Lcom/google/maps/g/pz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 65
    iput-object p3, p0, Lcom/google/android/apps/gmm/search/restriction/c/i;->f:Lcom/google/b/f/t;

    .line 66
    iput-object p4, p0, Lcom/google/android/apps/gmm/search/restriction/c/i;->e:Lcom/google/maps/g/pz;

    .line 67
    return-void
.end method

.method public static a(Lcom/google/maps/g/pz;)Lcom/google/android/apps/gmm/search/restriction/c/i;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 46
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/i;->values()[Lcom/google/android/apps/gmm/search/restriction/c/i;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 47
    iget-object v4, v3, Lcom/google/android/apps/gmm/search/restriction/c/i;->e:Lcom/google/maps/g/pz;

    invoke-virtual {v4, p0}, Lcom/google/maps/g/pz;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 48
    return-object v3

    .line 46
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 52
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x45

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "The rating "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not one of the options available as a SelectionOption."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/search/restriction/c/i;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/apps/gmm/search/restriction/c/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/restriction/c/i;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/search/restriction/c/i;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/apps/gmm/search/restriction/c/i;->g:[Lcom/google/android/apps/gmm/search/restriction/c/i;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/search/restriction/c/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/search/restriction/c/i;

    return-object v0
.end method
