.class public Lcom/google/android/apps/gmm/map/util/a/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/util/a/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Key:",
        "Ljava/lang/Object;",
        "Value:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/map/util/a/a",
        "<TKey;TValue;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/google/android/apps/gmm/map/util/a/b;

.field private b:La/a/a/a/d/ac;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/d/ac",
            "<TKey;>;"
        }
    .end annotation
.end field

.field public c:La/a/a/a/d/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/d/am",
            "<TKey;TValue;>;"
        }
    .end annotation
.end field

.field public final d:I

.field private e:I

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    .line 89
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/map/util/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, La/a/a/a/d/am;

    invoke-direct {v0}, La/a/a/a/d/am;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    .line 77
    new-instance v0, La/a/a/a/d/ac;

    invoke-direct {v0}, La/a/a/a/d/ac;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->b:La/a/a/a/d/ac;

    .line 78
    iput p1, p0, Lcom/google/android/apps/gmm/map/util/a/e;->d:I

    .line 79
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/util/a/e;->a:Lcom/google/android/apps/gmm/map/util/a/b;

    .line 80
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/util/a/e;->f:Ljava/lang/String;

    .line 81
    if-eqz p3, :cond_0

    .line 82
    iget-object v1, p3, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    invoke-interface {v0, p0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    .line 84
    :cond_0
    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;Z)TValue;"
        }
    .end annotation

    .prologue
    .line 214
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-virtual {v0, p1}, La/a/a/a/d/am;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 215
    if-eqz v0, :cond_0

    .line 216
    iget v1, p0, Lcom/google/android/apps/gmm/map/util/a/e;->e:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/util/a/e;->b:La/a/a/a/d/ac;

    invoke-virtual {v2, p1}, La/a/a/a/d/ac;->a(Ljava/lang/Object;)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/map/util/a/e;->e:I

    .line 217
    if-eqz p2, :cond_0

    .line 219
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    :cond_0
    monitor-exit p0

    return-object v0

    .line 214
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(I)V
    .locals 4

    .prologue
    .line 256
    monitor-enter p0

    if-nez p1, :cond_3

    .line 258
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->b:La/a/a/a/d/ac;

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-virtual {v0}, La/a/a/a/d/am;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    .line 261
    iget-object v1, v0, La/a/a/a/d/am;->i:La/a/a/a/d/ba;

    if-nez v1, :cond_0

    new-instance v1, La/a/a/a/d/at;

    invoke-direct {v1, v0}, La/a/a/a/d/at;-><init>(La/a/a/a/d/am;)V

    iput-object v1, v0, La/a/a/a/d/am;->i:La/a/a/a/d/ba;

    :cond_0
    iget-object v0, v0, La/a/a/a/d/am;->i:La/a/a/a/d/ba;

    invoke-interface {v0}, La/a/a/a/d/ba;->c()La/a/a/a/d/bg;

    move-result-object v1

    .line 262
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 263
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/a/a/d/ax;

    .line 264
    invoke-interface {v0}, La/a/a/a/d/ax;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, La/a/a/a/d/ax;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 265
    invoke-interface {v0}, La/a/a/a/d/ax;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, La/a/a/a/d/ax;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 256
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 268
    :cond_1
    :try_start_1
    new-instance v0, La/a/a/a/d/am;

    invoke-direct {v0}, La/a/a/a/d/am;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    .line 269
    new-instance v0, La/a/a/a/d/ac;

    invoke-direct {v0}, La/a/a/a/d/ac;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->b:La/a/a/a/d/ac;

    .line 270
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->e:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 280
    :cond_2
    monitor-exit p0

    return-void

    .line 273
    :cond_3
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-virtual {v0}, La/a/a/a/d/am;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->e:I

    if-le v0, p1, :cond_2

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-virtual {v0}, La/a/a/a/d/am;->firstKey()Ljava/lang/Object;

    move-result-object v0

    .line 275
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 277
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a(F)I
    .locals 7

    .prologue
    const/high16 v0, 0x3f000000    # 0.5f

    .line 284
    monitor-enter p0

    const/4 v1, 0x0

    cmpg-float v1, p1, v1

    if-gez v1, :cond_1

    .line 285
    :try_start_0
    const-string v1, "LRUCache"

    const-string v2, "fraction %f < 0"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v0

    .line 288
    :goto_0
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    .line 289
    const-string v2, "LRUCache"

    const-string v3, "fraction %f > 1"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 292
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/util/a/e;->e()I

    move-result v1

    .line 293
    int-to-float v2, v1

    mul-float/2addr v2, v0

    float-to-int v2, v2

    .line 294
    const-string v3, "LRUCache"

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/util/a/e;->f:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x47

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Trimming "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'s current size "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", or "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v0, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 296
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/map/util/a/e;->a(I)V

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-virtual {v0}, La/a/a/a/d/am;->h()Z

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->b:La/a/a/a/d/ac;

    invoke-virtual {v0}, La/a/a/a/d/ac;->d()Z

    .line 303
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/util/a/e;->e()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    sub-int v0, v1, v0

    monitor-exit p0

    return v0

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    move v1, p1

    goto :goto_0
.end method

.method public declared-synchronized a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)TValue;"
        }
    .end annotation

    .prologue
    .line 187
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    iget-object v2, v1, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    iget-object v3, v1, La/a/a/a/d/am;->c:[Z

    iget v4, v1, La/a/a/a/d/am;->g:I

    if-nez p1, :cond_1

    const v0, 0x87fcd5c

    :goto_0
    and-int/2addr v0, v4

    :goto_1
    aget-boolean v5, v3, v0

    if-eqz v5, :cond_4

    if-nez p1, :cond_2

    aget-object v5, v2, v0

    if-nez v5, :cond_3

    :cond_0
    invoke-virtual {v1, v0}, La/a/a/a/d/am;->a(I)V

    iget-object v1, v1, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aget-object v0, v1, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_2
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_0

    :cond_2
    aget-object v5, v2, v0

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    if-nez v5, :cond_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    and-int/2addr v0, v4

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;TValue;)V"
        }
    .end annotation

    .prologue
    .line 148
    return-void
.end method

.method public final declared-synchronized b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)TValue;"
        }
    .end annotation

    .prologue
    .line 182
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-virtual {v0, p1}, La/a/a/a/d/am;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;TValue;)V"
        }
    .end annotation

    .prologue
    .line 136
    return-void
.end method

.method public final declared-synchronized c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)TValue;"
        }
    .end annotation

    .prologue
    .line 209
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;Z)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;TValue;)V"
        }
    .end annotation

    .prologue
    .line 152
    monitor-enter p0

    if-nez p2, :cond_1

    .line 154
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 155
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 158
    :cond_1
    const/4 v0, 0x1

    .line 162
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-virtual {v1, p1, p2}, La/a/a/a/d/am;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 166
    iget v2, p0, Lcom/google/android/apps/gmm/map/util/a/e;->e:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/util/a/e;->b:La/a/a/a/d/ac;

    invoke-virtual {v3, p1, v0}, La/a/a/a/d/ac;->a(Ljava/lang/Object;I)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/gmm/map/util/a/e;->e:I

    .line 167
    if-eqz v1, :cond_2

    .line 168
    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 169
    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 173
    :goto_1
    iget v1, p0, Lcom/google/android/apps/gmm/map/util/a/e;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->e:I

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->a:Lcom/google/android/apps/gmm/map/util/a/b;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->a:Lcom/google/android/apps/gmm/map/util/a/b;

    goto :goto_0

    .line 171
    :cond_2
    iget v1, p0, Lcom/google/android/apps/gmm/map/util/a/e;->d:I

    sub-int/2addr v1, v0

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    .prologue
    .line 92
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    monitor-exit p0

    return-void

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()I
    .locals 1

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()I
    .locals 1

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-virtual {v0}, La/a/a/a/d/am;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Z
    .locals 1

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-virtual {v0}, La/a/a/a/d/am;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TValue;"
        }
    .end annotation

    .prologue
    .line 198
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-virtual {v0}, La/a/a/a/d/am;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    const/4 v0, 0x0

    .line 201
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-virtual {v0}, La/a/a/a/d/am;->lastKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TValue;>;"
        }
    .end annotation

    .prologue
    .line 243
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-virtual {v1}, La/a/a/a/d/am;->b()La/a/a/a/d/be;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    monitor-exit p0

    return-object v0

    .line 243
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
