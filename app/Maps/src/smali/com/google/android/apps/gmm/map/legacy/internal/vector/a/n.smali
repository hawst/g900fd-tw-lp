.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/nio/ByteBuffer;

.field public b:I

.field public c:I

.field public d:I

.field public e:Lcom/google/android/apps/gmm/shared/c/j;

.field public f:I

.field final synthetic g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Ljava/nio/ByteBuffer;III)V
    .locals 2

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/j;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->e:Lcom/google/android/apps/gmm/shared/c/j;

    .line 100
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->f:I

    .line 103
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->a:Ljava/nio/ByteBuffer;

    .line 104
    iput p3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->b:I

    .line 105
    iput p4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->c:I

    .line 106
    iput p5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->d:I

    .line 107
    return-void
.end method


# virtual methods
.method public final a([I)V
    .locals 14

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->o:Lcom/google/android/apps/gmm/map/internal/vector/gl/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/e;->a([I)[B

    move-result-object v0

    .line 159
    if-nez v0, :cond_2

    .line 161
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->c:I

    new-array v7, v0, [B

    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_3

    :cond_0
    move v0, v1

    :goto_0
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->c:I

    if-ge v0, v2, :cond_1

    const/4 v2, -0x1

    aput-byte v2, v7, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v0, v7

    .line 164
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->o:Lcom/google/android/apps/gmm/map/internal/vector/gl/e;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/vector/gl/e;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    new-instance v3, Lcom/google/android/apps/gmm/map/internal/vector/gl/f;

    invoke-direct {v3, p1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/f;-><init>([I)V

    invoke-virtual {v2, v3, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 168
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->e:Lcom/google/android/apps/gmm/shared/c/j;

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->f:I

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/c/j;->a:[I

    aget v2, v2, v3

    .line 169
    :goto_2
    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->c:I

    if-ge v1, v3, :cond_e

    .line 170
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->a:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    aget-byte v5, v0, v1

    invoke-virtual {v4, v5, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->a(BI)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 169
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    move v2, v1

    .line 161
    :goto_3
    array-length v3, p1

    if-ge v0, v3, :cond_4

    aget v3, p1, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    const/high16 v0, 0x41000000    # 8.0f

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    iget v3, v3, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->p:F

    div-float v11, v0, v3

    move v6, v1

    :goto_4
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->c:I

    if-ge v6, v0, :cond_d

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->c:I

    if-ge v6, v0, :cond_5

    move v0, v5

    :goto_5
    const-string v3, "Invalid texel index"

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move v0, v1

    goto :goto_5

    :cond_6
    array-length v0, p1

    rem-int/lit8 v3, v0, 0x2

    if-ne v3, v5, :cond_f

    shl-int/lit8 v0, v0, 0x1

    shl-int/lit8 v3, v2, 0x1

    :goto_6
    int-to-float v8, v6

    int-to-float v3, v3

    mul-float/2addr v3, v8

    iget v8, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->c:I

    int-to-float v8, v8

    div-float v12, v3, v8

    move v8, v1

    move v3, v5

    move v9, v1

    :goto_7
    if-ge v8, v0, :cond_c

    array-length v10, p1

    rem-int v10, v8, v10

    aget v10, p1, v10

    add-int/2addr v10, v9

    int-to-float v13, v10

    cmpg-float v13, v12, v13

    if-gtz v13, :cond_a

    int-to-float v0, v9

    sub-float v0, v12, v0

    int-to-float v8, v10

    sub-float/2addr v8, v12

    cmpl-float v9, v0, v11

    if-lez v9, :cond_8

    cmpl-float v9, v8, v11

    if-lez v9, :cond_8

    if-eqz v3, :cond_7

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_8
    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-byte v0, v0

    aput-byte v0, v7, v6

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_4

    :cond_7
    const/4 v0, 0x0

    goto :goto_8

    :cond_8
    invoke-static {v0, v8}, Ljava/lang/Math;->min(FF)F

    move-result v0

    div-float v8, v0, v11

    if-eqz v3, :cond_9

    move v0, v4

    :goto_9
    mul-float/2addr v0, v8

    add-float/2addr v0, v4

    goto :goto_8

    :cond_9
    const/high16 v0, -0x41000000    # -0.5f

    goto :goto_9

    :cond_a
    if-nez v3, :cond_b

    move v3, v5

    :goto_a
    add-int/lit8 v8, v8, 0x1

    move v9, v10

    goto :goto_7

    :cond_b
    move v3, v1

    goto :goto_a

    :cond_c
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Couldn\'t find dash"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    move-object v0, v7

    goto/16 :goto_1

    .line 172
    :cond_e
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->f:I

    .line 173
    return-void

    :cond_f
    move v3, v2

    goto :goto_6
.end method
