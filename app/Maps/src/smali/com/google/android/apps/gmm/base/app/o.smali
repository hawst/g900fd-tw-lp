.class Lcom/google/android/apps/gmm/base/app/o;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/b/a/bw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/b/a/bw",
        "<",
        "Lcom/google/android/apps/gmm/search/av;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/app/b;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/app/b;)V
    .locals 0

    .prologue
    .line 341
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/app/o;->a:Lcom/google/android/apps/gmm/base/app/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/o;->a:Lcom/google/android/apps/gmm/base/app/b;

    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.google.android.ssb.action.SSB_SERVICE"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.google.android.googlequicksearchbox"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/app/b;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    if-nez v4, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/base/app/b;->a:Ljava/lang/String;

    move v0, v2

    :goto_0
    if-eqz v0, :cond_7

    sget-object v0, Lcom/google/android/apps/gmm/base/app/b;->a:Ljava/lang/String;

    new-instance v1, Lcom/google/android/apps/gmm/search/av;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/o;->a:Lcom/google/android/apps/gmm/base/app/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/app/b;->b:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/app/o;->a:Lcom/google/android/apps/gmm/base/app/b;

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v4

    invoke-direct {v1, v0, v4}, Lcom/google/android/apps/gmm/search/av;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/util/b/g;)V

    iget-object v4, v1, Lcom/google/android/apps/gmm/search/av;->b:Lcom/google/android/d/a/b;

    iget-object v0, v1, Lcom/google/android/apps/gmm/search/av;->e:Lcom/google/android/d/a/a;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    if-ne v5, v6, :cond_0

    move v2, v3

    :cond_0
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {v4, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/base/app/b;->a:Ljava/lang/String;

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-nez v1, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/base/app/b;->a:Ljava/lang/String;

    move v0, v2

    goto :goto_0

    :cond_4
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/google/android/gms/common/g;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_5
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Lcom/google/android/d/a/a;

    iput-object v0, v4, Lcom/google/android/d/a/b;->b:Lcom/google/android/d/a/a;

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.ssb.action.SSB_SERVICE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.googlequicksearchbox"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v2, v4, Lcom/google/android/d/a/b;->f:Landroid/content/Context;

    iget-object v4, v4, Lcom/google/android/d/a/b;->e:Lcom/google/android/d/a/d;

    invoke-virtual {v2, v0, v4, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    sget-object v2, Lcom/google/android/apps/gmm/search/av;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "ssbServiceClient.Connect: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/base/app/b;->a:Ljava/lang/String;

    const/4 v0, 0x0

    goto :goto_1
.end method
