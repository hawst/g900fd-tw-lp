.class public Lcom/google/android/apps/gmm/map/o/b/c;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/b/a/ab;

.field public final b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ab;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/b/c;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 30
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/ab;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/b/c;->b:I

    .line 31
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/o/b/e;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/z;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 45
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 48
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/b/e;->a:Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    if-eqz p2, :cond_0

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    const/16 v1, 0x8

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(ILcom/google/android/apps/gmm/map/internal/c/p;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bi;ILjava/lang/String;F)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/b/e;->b:Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-direct {v0, p2, v1}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/z;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/d;->b:Lcom/google/android/apps/gmm/map/internal/c/d;

    invoke-direct {v0, v9, v1}, Lcom/google/android/apps/gmm/map/internal/c/z;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/d;)V

    return-object v0
.end method
