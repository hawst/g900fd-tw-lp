.class public Lcom/google/android/apps/gmm/cardui/h/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/g/f;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/util/b/ag;

.field private final b:Lcom/google/o/h/a/mb;

.field private final c:Lcom/google/android/apps/gmm/place/station/b/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/mb;Lcom/google/android/apps/gmm/place/station/b/a;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/apps/gmm/cardui/h/m;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 26
    iput-object p2, p0, Lcom/google/android/apps/gmm/cardui/h/m;->b:Lcom/google/o/h/a/mb;

    .line 27
    iput-object p3, p0, Lcom/google/android/apps/gmm/cardui/h/m;->c:Lcom/google/android/apps/gmm/place/station/b/a;

    .line 28
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/place/station/b/a;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/m;->c:Lcom/google/android/apps/gmm/place/station/b/a;

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/m;->b:Lcom/google/o/h/a/mb;

    iget v0, v0, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4

    .prologue
    .line 32
    new-instance v2, Lcom/google/android/apps/gmm/z/b/m;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/z/b/m;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/m;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 33
    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/cardui/h/m;->b:Lcom/google/o/h/a/mb;

    .line 34
    iget-object v0, v3, Lcom/google/o/h/a/mb;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    :goto_0
    iput-object v0, v2, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/m;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b/ag;->e:Lcom/google/r/b/a/tf;

    .line 36
    if-eqz v0, :cond_0

    .line 37
    iput-object v0, v2, Lcom/google/android/apps/gmm/z/b/m;->e:Lcom/google/r/b/a/tf;

    .line 39
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0

    .line 34
    :cond_1
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v1, v3, Lcom/google/o/h/a/mb;->d:Ljava/lang/Object;

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/m;->b:Lcom/google/o/h/a/mb;

    iget v0, v0, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/m;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b/ag;->c:Lcom/google/android/apps/gmm/util/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/m;->b:Lcom/google/o/h/a/mb;

    iget-object v0, v0, Lcom/google/o/h/a/mb;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/m;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 51
    iget-object v2, v2, Lcom/google/android/apps/gmm/util/b/ag;->a:Lcom/google/o/h/a/br;

    iget-object v3, p0, Lcom/google/android/apps/gmm/cardui/h/m;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/util/b/b;->a(Lcom/google/o/h/a/br;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v2

    .line 50
    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/util/b/a;->a(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;)V

    .line 53
    :cond_0
    return-object v4

    .line 49
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
