.class public final enum Lcom/google/android/apps/gmm/feedback/c/b;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/feedback/c/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/feedback/c/b;

.field public static final enum b:Lcom/google/android/apps/gmm/feedback/c/b;

.field public static final enum c:Lcom/google/android/apps/gmm/feedback/c/b;

.field public static final enum d:Lcom/google/android/apps/gmm/feedback/c/b;

.field public static final enum e:Lcom/google/android/apps/gmm/feedback/c/b;

.field public static final enum f:Lcom/google/android/apps/gmm/feedback/c/b;

.field public static final enum g:Lcom/google/android/apps/gmm/feedback/c/b;

.field private static final synthetic h:[Lcom/google/android/apps/gmm/feedback/c/b;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    new-instance v0, Lcom/google/android/apps/gmm/feedback/c/b;

    const-string v1, "LESS_500"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/feedback/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/c/b;->a:Lcom/google/android/apps/gmm/feedback/c/b;

    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/feedback/c/b;

    const-string v1, "MORE_500"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/feedback/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/c/b;->b:Lcom/google/android/apps/gmm/feedback/c/b;

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/feedback/c/b;

    const-string v1, "WRONG_DIRECTION"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/feedback/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/c/b;->c:Lcom/google/android/apps/gmm/feedback/c/b;

    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/feedback/c/b;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/feedback/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/c/b;->d:Lcom/google/android/apps/gmm/feedback/c/b;

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/feedback/c/b;

    const-string v1, "STALE"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/feedback/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/c/b;->e:Lcom/google/android/apps/gmm/feedback/c/b;

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/feedback/c/b;

    const-string v1, "WRONG_PLACE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/c/b;->f:Lcom/google/android/apps/gmm/feedback/c/b;

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/feedback/c/b;

    const-string v1, "OTHER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/c/b;->g:Lcom/google/android/apps/gmm/feedback/c/b;

    .line 16
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/apps/gmm/feedback/c/b;

    sget-object v1, Lcom/google/android/apps/gmm/feedback/c/b;->a:Lcom/google/android/apps/gmm/feedback/c/b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/feedback/c/b;->b:Lcom/google/android/apps/gmm/feedback/c/b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/feedback/c/b;->c:Lcom/google/android/apps/gmm/feedback/c/b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/feedback/c/b;->d:Lcom/google/android/apps/gmm/feedback/c/b;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/feedback/c/b;->e:Lcom/google/android/apps/gmm/feedback/c/b;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/feedback/c/b;->f:Lcom/google/android/apps/gmm/feedback/c/b;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/feedback/c/b;->g:Lcom/google/android/apps/gmm/feedback/c/b;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/feedback/c/b;->h:[Lcom/google/android/apps/gmm/feedback/c/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/feedback/c/b;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/android/apps/gmm/feedback/c/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/feedback/c/b;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/feedback/c/b;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/android/apps/gmm/feedback/c/b;->h:[Lcom/google/android/apps/gmm/feedback/c/b;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/feedback/c/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/feedback/c/b;

    return-object v0
.end method
