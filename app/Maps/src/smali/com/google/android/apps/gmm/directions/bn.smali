.class Lcom/google/android/apps/gmm/directions/bn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/d/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/bm;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/bm;Lcom/google/android/apps/gmm/directions/bm;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/bn;->a:Lcom/google/android/apps/gmm/directions/bm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/directions/d/a;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 49
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/bn;->a:Lcom/google/android/apps/gmm/directions/bm;

    iget-object v5, v4, Lcom/google/android/apps/gmm/directions/bm;->b:Ljava/util/List;

    iget-object v6, p1, Lcom/google/android/apps/gmm/directions/d/a;->c:Lcom/google/android/apps/gmm/map/r/a/e;

    if-nez v6, :cond_1

    move-object v1, v2

    :goto_0
    if-eqz v1, :cond_0

    iget-object v0, v4, Lcom/google/android/apps/gmm/directions/bm;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/gmm/directions/b/d;

    invoke-direct {v3, v1}, Lcom/google/android/apps/gmm/directions/b/d;-><init>(Ljava/util/Map;)V

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    :cond_0
    iput-object v2, v4, Lcom/google/android/apps/gmm/directions/bm;->b:Ljava/util/List;

    .line 50
    return-void

    .line 49
    :cond_1
    iget-object v1, v6, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v1, v1, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    if-eq v1, v3, :cond_2

    const-string v1, "TrafficFetcher"

    const-string v3, "Wrong number of routes returned from traffic update. Expected %d but received %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v0

    const/4 v0, 0x1

    iget-object v5, v6, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v5, v5, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v0

    invoke-static {v1, v3, v7}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v2

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v3

    move v1, v0

    :goto_1
    iget-object v0, v6, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fw;

    invoke-interface {v3, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move-object v1, v3

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/directions/d/a;)V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bn;->a:Lcom/google/android/apps/gmm/directions/bm;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/bm;->b:Ljava/util/List;

    .line 55
    return-void
.end method
