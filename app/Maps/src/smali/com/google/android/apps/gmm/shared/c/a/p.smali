.class public final enum Lcom/google/android/apps/gmm/shared/c/a/p;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/shared/c/a/p;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum ALERT_CONTROLLER:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum ANDROID_INTERNAL_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum ANDROID_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum BOGUS_DATA_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum DATA_REQUEST_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum DIRECTIONS_DETAILS_ROUTE_TRAFFIC_UPDATE_FETCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum FUSED_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum GL_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum GMM_PICKER:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum GMM_STORAGE:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum INDOOR_BUILDING_STORE:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum INTERPRETER_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum LABELING_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum LOCATION_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum LOCATION_PROVIDER_MONITOR:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum MEMORY_MONITOR_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum NETWORK_THREADS:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum NETWORK_TTS_SYNTHESIS:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum OFFLINE_REGION_MANAGEMENT:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum PARTICLE_FILTER_HANDLER:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum PARTICLE_FILTER_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum REDIRECT_FETCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

.field public static final enum WEARABLE_DATA:Lcom/google/android/apps/gmm/shared/c/a/p;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 29
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "UI_THREAD"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "CURRENT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 48
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "BACKGROUND_THREADPOOL"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "NETWORK_THREADS"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->NETWORK_THREADS:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "LOCATION_DISPATCHER"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->LOCATION_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 61
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "INDOOR_BUILDING_STORE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->INDOOR_BUILDING_STORE:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 67
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "GL_THREAD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->GL_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 70
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "GMM_PICKER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_PICKER:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 81
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "NAVIGATION_INTERNAL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 88
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "WEARABLE_DATA"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->WEARABLE_DATA:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 91
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "GMM_STORAGE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_STORAGE:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 94
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "DATA_REQUEST_DISPATCHER"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->DATA_REQUEST_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 97
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "REDIRECT_FETCHER"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->REDIRECT_FETCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 105
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "DIRECTIONS_DETAILS_ROUTE_TRAFFIC_UPDATE_FETCHER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->DIRECTIONS_DETAILS_ROUTE_TRAFFIC_UPDATE_FETCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 108
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "ANDROID_INTERNAL_LOCATION_PROVIDER"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->ANDROID_INTERNAL_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 111
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "FUSED_LOCATION_PROVIDER"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->FUSED_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 114
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "ANDROID_LOCATION_PROVIDER"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->ANDROID_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 117
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "BOGUS_DATA_LOCATION_PROVIDER"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BOGUS_DATA_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 123
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "PARTICLE_FILTER_HANDLER"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->PARTICLE_FILTER_HANDLER:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 126
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "PARTICLE_FILTER_LOCATION_PROVIDER"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->PARTICLE_FILTER_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 132
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "LOCATION_PROVIDER_MONITOR"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->LOCATION_PROVIDER_MONITOR:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 135
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "LABELING_THREAD"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->LABELING_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 138
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "INTERPRETER_THREAD"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->INTERPRETER_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 144
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "MEMORY_MONITOR_THREAD"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->MEMORY_MONITOR_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 149
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "OFFLINE_REGION_MANAGEMENT"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->OFFLINE_REGION_MANAGEMENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 162
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "ALERT_CONTROLLER"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->ALERT_CONTROLLER:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 168
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    const-string v1, "NETWORK_TTS_SYNTHESIS"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->NETWORK_TTS_SYNTHESIS:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 27
    const/16 v0, 0x1b

    new-array v0, v0, [Lcom/google/android/apps/gmm/shared/c/a/p;

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->NETWORK_THREADS:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->LOCATION_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->INDOOR_BUILDING_STORE:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->GL_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_PICKER:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->WEARABLE_DATA:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_STORAGE:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->DATA_REQUEST_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->REDIRECT_FETCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->DIRECTIONS_DETAILS_ROUTE_TRAFFIC_UPDATE_FETCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->ANDROID_INTERNAL_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->FUSED_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->ANDROID_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BOGUS_DATA_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->PARTICLE_FILTER_HANDLER:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->PARTICLE_FILTER_LOCATION_PROVIDER:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->LOCATION_PROVIDER_MONITOR:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->LABELING_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->INTERPRETER_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->MEMORY_MONITOR_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->OFFLINE_REGION_MANAGEMENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->ALERT_CONTROLLER:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->NETWORK_TTS_SYNTHESIS:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->$VALUES:[Lcom/google/android/apps/gmm/shared/c/a/p;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 263
    return-void
.end method

.method public static a()Lcom/google/android/apps/gmm/shared/c/a/p;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 180
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 181
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 182
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 187
    :goto_0
    return-object v0

    .line 184
    :cond_0
    instance-of v1, v0, Lcom/google/android/apps/gmm/shared/c/a/r;

    if-eqz v1, :cond_1

    .line 185
    check-cast v0, Lcom/google/android/apps/gmm/shared/c/a/r;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/a/r;->P_()Lcom/google/android/apps/gmm/shared/c/a/p;

    move-result-object v0

    goto :goto_0

    .line 187
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/shared/c/a/p;
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/shared/c/a/p;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->$VALUES:[Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/shared/c/a/p;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/shared/c/a/p;

    return-object v0
.end method


# virtual methods
.method public a(Z)V
    .locals 8

    .prologue
    .line 212
    if-nez p1, :cond_1

    .line 217
    new-instance v1, Ljava/lang/IllegalStateException;

    if-eqz p1, :cond_0

    const-string v0, "Should"

    :goto_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 218
    invoke-static {}, Lcom/google/android/apps/gmm/shared/c/a/p;->a()Lcom/google/android/apps/gmm/shared/c/a/p;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 219
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x2d

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " be running on "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", but we\'re on "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " (Java Thread "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 217
    :cond_0
    const-string v0, "Should not"

    goto :goto_0

    .line 221
    :cond_1
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 192
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    if-ne p0, v0, :cond_0

    move v0, v1

    .line 205
    :goto_0
    return v0

    .line 195
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 199
    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    if-ne p0, v3, :cond_2

    .line 200
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v3

    if-ne v0, v3, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 202
    :cond_2
    instance-of v3, v0, Lcom/google/android/apps/gmm/shared/c/a/r;

    if-eqz v3, :cond_4

    .line 203
    check-cast v0, Lcom/google/android/apps/gmm/shared/c/a/r;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/a/r;->P_()Lcom/google/android/apps/gmm/shared/c/a/p;

    move-result-object v0

    if-ne p0, v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    .line 205
    goto :goto_0
.end method
