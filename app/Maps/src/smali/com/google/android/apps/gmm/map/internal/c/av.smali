.class public Lcom/google/android/apps/gmm/map/internal/c/av;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/bu;


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Z


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->a:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->b:Ljava/lang/String;

    .line 40
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->c:Z

    .line 41
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/android/apps/gmm/map/internal/c/av;
    .locals 3

    .prologue
    .line 170
    const-string v0, ""

    .line 171
    const-string v0, ""

    .line 174
    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->bf:Lcom/google/android/apps/gmm/shared/b/c;

    const-string v0, ""

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 180
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/aw;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/internal/c/aw;-><init>()V

    .line 181
    const-string v2, ""

    iput-object v2, v1, Lcom/google/android/apps/gmm/map/internal/c/aw;->a:Ljava/lang/String;

    .line 182
    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/aw;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->aa:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v2, 0x0

    .line 183
    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/map/internal/c/aw;->c:Z

    .line 184
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/aw;->a()Lcom/google/android/apps/gmm/map/internal/c/av;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bv;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->f:Lcom/google/android/apps/gmm/map/internal/c/bv;

    return-object v0
.end method

.method public final a(Lcom/google/e/a/a/a/b;)V
    .locals 3

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    .line 206
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->b:Ljava/lang/String;

    if-gez v0, :cond_2

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 202
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 206
    :cond_2
    iget-object v2, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 208
    :cond_3
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ai;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 189
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->c:Z

    if-eqz v2, :cond_6

    .line 192
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    :cond_1
    move v2, v0

    :goto_1
    if-nez v2, :cond_5

    .line 194
    :cond_2
    :goto_2
    return v0

    :cond_3
    move v2, v1

    .line 192
    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2

    .line 194
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->a:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v2, v0

    :goto_3
    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v2, v0

    :goto_4
    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_2

    :cond_9
    move v2, v1

    goto :goto_3

    :cond_a
    move v2, v1

    goto :goto_4
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/c/av;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/bu;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/c/av;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    if-ne p0, p1, :cond_0

    move v0, v1

    .line 95
    :goto_0
    return v0

    .line 90
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/internal/c/av;

    if-nez v0, :cond_1

    move v0, v2

    .line 91
    goto :goto_0

    .line 94
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->a:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/av;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/av;->a:Ljava/lang/String;

    if-eq v3, v0, :cond_2

    if-eqz v3, :cond_4

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->b:Ljava/lang/String;

    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/av;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/av;->b:Ljava/lang/String;

    .line 95
    if-eq v0, v3, :cond_3

    if-eqz v0, :cond_5

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    move v0, v1

    :goto_2
    if-eqz v0, :cond_6

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 94
    goto :goto_1

    :cond_5
    move v0, v2

    .line 95
    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 81
    :goto_1
    add-int/2addr v0, v1

    return v0

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/av;->b:Ljava/lang/String;

    .line 81
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
