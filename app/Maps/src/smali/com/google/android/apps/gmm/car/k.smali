.class public Lcom/google/android/apps/gmm/car/k;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Landroid/app/Service;

.field final c:Lcom/google/android/apps/gmm/car/f;

.field final d:Lcom/google/android/apps/gmm/car/e;

.field final e:Lcom/google/android/gms/common/api/o;

.field f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/apps/gmm/car/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/k;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Service;Lcom/google/android/apps/gmm/car/e;Lcom/google/android/apps/gmm/car/f;)V
    .locals 3

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/k;->b:Landroid/app/Service;

    .line 33
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/car/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/k;->d:Lcom/google/android/apps/gmm/car/e;

    .line 34
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p3, Lcom/google/android/apps/gmm/car/f;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/k;->c:Lcom/google/android/apps/gmm/car/f;

    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/car/l;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/l;-><init>(Lcom/google/android/apps/gmm/car/k;)V

    new-instance v1, Lcom/google/android/apps/gmm/car/m;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/m;-><init>(Lcom/google/android/apps/gmm/car/k;)V

    new-instance v2, Lcom/google/android/apps/gmm/car/n;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/car/n;-><init>(Lcom/google/android/apps/gmm/car/k;)V

    invoke-interface {p2, p1, v0, v1, v2}, Lcom/google/android/apps/gmm/car/e;->a(Landroid/content/Context;Lcom/google/android/gms/common/api/q;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/car/e;)Lcom/google/android/gms/common/api/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/k;->e:Lcom/google/android/gms/common/api/o;

    .line 70
    return-void
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/google/android/apps/gmm/car/k;->a:Ljava/lang/String;

    .line 79
    return-void
.end method


# virtual methods
.method b()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 94
    sget-object v0, Lcom/google/android/apps/gmm/car/k;->a:Ljava/lang/String;

    .line 95
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/k;->f:Z

    if-eqz v0, :cond_2

    .line 97
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/car/k;->f:Z

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/k;->c:Lcom/google/android/apps/gmm/car/f;

    sget-object v1, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/f;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/f;->q:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/f;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/f;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v3, Lcom/google/android/apps/gmm/car/a/d;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/car/a/d;-><init>(Z)V

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->s()Lcom/google/android/apps/gmm/car/a/g;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, v2, Lcom/google/android/apps/gmm/car/a/g;->c:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iput-object v3, v2, Lcom/google/android/apps/gmm/car/a/g;->b:Lcom/google/android/apps/gmm/car/a/h;

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/f;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v3, Lcom/google/android/apps/gmm/car/a/e;

    sget-object v4, Lcom/google/android/apps/gmm/car/a/f;->a:Lcom/google/android/apps/gmm/car/a/f;

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/car/a/e;-><init>(Lcom/google/android/apps/gmm/car/a/f;)V

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/b/c;->a()V

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/f;->l:Lcom/google/android/apps/gmm/car/i/ab;

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/i/ab;->e:Lcom/google/android/apps/gmm/car/w;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/car/w;->b()V

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/i/ab;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/apps/gmm/car/i/ab;->k:Ljava/lang/Object;

    invoke-interface {v3, v4}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/car/i/ab;->g:Z

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/car/i/ab;->h:Z

    sget-object v2, Lcom/google/android/apps/gmm/car/i/ab;->a:Ljava/lang/String;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/f;->l:Lcom/google/android/apps/gmm/car/i/ab;

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/f;->m:Lcom/google/android/apps/gmm/car/bc;

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0

    :cond_0
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/f;->m:Lcom/google/android/apps/gmm/car/bc;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/f;->m:Lcom/google/android/apps/gmm/car/bc;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/bc;->b()V

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/f;->m:Lcom/google/android/apps/gmm/car/bc;

    :cond_1
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/f;->k:Lcom/google/android/apps/gmm/car/az;

    sget-object v3, Lcom/google/android/apps/gmm/car/az;->a:Ljava/lang/String;

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/az;->b:Lcom/google/android/gms/car/ag;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/gms/car/ag;->b(I)V

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/az;->b:Lcom/google/android/gms/car/ag;

    invoke-virtual {v2}, Lcom/google/android/gms/car/ag;->a()V

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/f;->k:Lcom/google/android/apps/gmm/car/az;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/f;->g:Lcom/google/android/gms/car/ag;

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/f;->j:Lcom/google/android/apps/gmm/car/aa;

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/aa;->b:Lcom/google/android/gms/car/aq;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/aa;->d:Lcom/google/android/gms/car/as;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/car/aq;->a(Lcom/google/android/gms/car/as;)V

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/f;->j:Lcom/google/android/apps/gmm/car/aa;

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/f;->i:Lcom/google/android/apps/gmm/car/p;

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/p;->b:Lcom/google/android/gms/car/aq;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/car/aq;->a(Lcom/google/android/gms/car/as;)V

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/f;->i:Lcom/google/android/apps/gmm/car/p;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/f;->h:Lcom/google/android/gms/car/aq;

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 100
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/k;->e:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->c()V

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/k;->b:Landroid/app/Service;

    invoke-virtual {v0}, Landroid/app/Service;->stopSelf()V

    .line 102
    return-void
.end method
