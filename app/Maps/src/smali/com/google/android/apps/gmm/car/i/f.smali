.class Lcom/google/android/apps/gmm/car/i/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/i/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/car/i/k",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/apps/gmm/suggest/e/d;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/i/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/i/a;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/i/f;->a:Lcom/google/android/apps/gmm/car/i/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/f;->a:Lcom/google/android/apps/gmm/car/i/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/a;->b:Lcom/google/android/apps/gmm/car/i/l;

    const-class v1, Lcom/google/android/apps/gmm/car/i/a/b;

    sget v2, Lcom/google/android/apps/gmm/l;->bM:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/car/i/l;->a(Ljava/lang/Class;I)V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/f;->a:Lcom/google/android/apps/gmm/car/i/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/i/a;->d:Landroid/view/View;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/a;->h:Lcom/google/android/apps/gmm/car/i/b/a;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 207
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 184
    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/f;->a:Lcom/google/android/apps/gmm/car/i/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/a;->b:Lcom/google/android/apps/gmm/car/i/l;

    const-class v1, Lcom/google/android/apps/gmm/car/i/a/b;

    sget v2, Lcom/google/android/apps/gmm/l;->bt:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/car/i/l;->a(Ljava/lang/Class;I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/f;->a:Lcom/google/android/apps/gmm/car/i/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/i/a;->d:Landroid/view/View;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/a;->h:Lcom/google/android/apps/gmm/car/i/b/a;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    return-void

    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/apps/gmm/suggest/e/d;

    new-instance v0, Lcom/google/android/apps/gmm/car/i/b/h;

    iget-object v1, v5, Lcom/google/android/apps/gmm/suggest/e/d;->e:Ljava/lang/String;

    iget-object v2, v5, Lcom/google/android/apps/gmm/suggest/e/d;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/i/f;->a:Lcom/google/android/apps/gmm/car/i/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/car/i/a;->e:Ljava/lang/String;

    iget-object v4, v5, Lcom/google/android/apps/gmm/suggest/e/d;->d:Ljava/lang/String;

    iget-object v5, v5, Lcom/google/android/apps/gmm/suggest/e/d;->m:[B

    iget-object v6, p0, Lcom/google/android/apps/gmm/car/i/f;->a:Lcom/google/android/apps/gmm/car/i/a;

    iget-object v6, v6, Lcom/google/android/apps/gmm/car/i/a;->f:Lcom/google/android/apps/gmm/car/i/b/i;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/car/i/b/h;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/apps/gmm/car/i/b/i;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/f;->a:Lcom/google/android/apps/gmm/car/i/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/a;->b:Lcom/google/android/apps/gmm/car/i/l;

    const-class v1, Lcom/google/android/apps/gmm/car/i/a/f;

    invoke-virtual {v0, v1, v7}, Lcom/google/android/apps/gmm/car/i/l;->a(Ljava/lang/Class;Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 212
    return-void
.end method
