.class Lcom/google/android/apps/gmm/map/internal/d/h;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/internal/d/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/d;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 876
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/h;->a:Lcom/google/android/apps/gmm/map/internal/d/d;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 20

    .prologue
    .line 879
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/h;->a:Lcom/google/android/apps/gmm/map/internal/d/d;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Landroid/os/Message;)Z

    move-result v2

    .line 880
    if-eqz v2, :cond_1

    .line 927
    :cond_0
    :goto_0
    return-void

    .line 886
    :cond_1
    const-string v2, "DashServerTileStore.handleMessage"

    .line 888
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 890
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/h;->a:Lcom/google/android/apps/gmm/map/internal/d/d;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/p;

    invoke-static {v3, v2}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/d;Lcom/google/android/apps/gmm/map/internal/d/p;)V

    goto :goto_0

    .line 893
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/h;->a:Lcom/google/android/apps/gmm/map/internal/d/d;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/d;)V

    goto :goto_0

    .line 896
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/h;->a:Lcom/google/android/apps/gmm/map/internal/d/d;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/i;

    invoke-static {v3, v2}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/d;Lcom/google/android/apps/gmm/map/internal/d/i;)V

    goto :goto_0

    .line 900
    :pswitch_4
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/google/b/a/an;

    .line 902
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/h;->a:Lcom/google/android/apps/gmm/map/internal/d/d;

    iget-object v3, v2, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/apps/gmm/map/internal/d/i;

    iget-object v2, v2, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/apps/gmm/shared/net/k;

    invoke-static {v4, v3, v2}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/d;Lcom/google/android/apps/gmm/map/internal/d/i;Lcom/google/android/apps/gmm/shared/net/k;)V

    goto :goto_0

    .line 905
    :pswitch_5
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    monitor-enter v3

    .line 906
    :try_start_0
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 907
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 908
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/h;->a:Lcom/google/android/apps/gmm/map/internal/d/d;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/d/d;->b(Lcom/google/android/apps/gmm/map/internal/d/d;)V

    goto :goto_0

    .line 907
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 911
    :pswitch_6
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/q;

    .line 912
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/internal/d/h;->a:Lcom/google/android/apps/gmm/map/internal/d/d;

    iget-object v9, v2, Lcom/google/android/apps/gmm/map/internal/d/q;->a:Ljava/util/Queue;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/q;->c:Lcom/google/android/apps/gmm/map/internal/d/c;

    iget-object v6, v2, Lcom/google/android/apps/gmm/map/internal/d/q;->b:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    iget-boolean v10, v2, Lcom/google/android/apps/gmm/map/internal/d/q;->d:Z

    const/4 v2, 0x0

    invoke-static {v3, v2}, Lcom/google/android/apps/gmm/map/internal/d/aj;->a(Lcom/google/android/apps/gmm/map/internal/d/c;Z)I

    move-result v11

    iget-object v2, v8, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v12

    invoke-interface {v9}, Ljava/util/Queue;->size()I

    move-result v13

    invoke-interface {v9}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/bp;

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/internal/c/bp;

    if-eqz v3, :cond_7

    iget-object v4, v8, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v4, :cond_5

    iget-object v14, v8, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v10, :cond_4

    move-object v5, v6

    :goto_2
    const/4 v4, 0x0

    move v7, v4

    :goto_3
    iget-object v4, v14, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v7, v4, :cond_5

    iget-object v4, v14, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v15

    if-eqz v15, :cond_3

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v4

    invoke-interface {v4, v3, v5, v11}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;I)V

    :cond_3
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    move-object v5, v4

    goto :goto_2

    :cond_5
    if-nez v10, :cond_2

    if-eqz v12, :cond_6

    iget-object v4, v8, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v3

    invoke-interface {v12, v3, v6, v11}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;I)V

    goto :goto_1

    :cond_6
    const/4 v2, 0x1

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    invoke-interface {v6, v3, v2, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/a/c;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    goto/16 :goto_0

    :cond_7
    if-lez v13, :cond_0

    const/4 v3, 0x6

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    invoke-interface {v6, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/a/c;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    iget-object v2, v8, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/au;->b(Z)V

    iget-object v2, v8, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v2, :cond_0

    iget-object v2, v8, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/r;->a(Z)V

    goto/16 :goto_0

    .line 917
    :pswitch_7
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/google/b/a/an;

    .line 919
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/h;->a:Lcom/google/android/apps/gmm/map/internal/d/d;

    iget-object v3, v2, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/apps/gmm/map/b/a/bc;

    iget-object v2, v2, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/d/r;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/d/d;->p:Lcom/google/android/apps/gmm/map/internal/d/ag;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/d/ag;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/d/d;->p:Lcom/google/android/apps/gmm/map/internal/d/ag;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v5, v4, v3, v2}, Lcom/google/android/apps/gmm/map/internal/d/ag;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/b/a/bc;Ljava/util/List;)V

    goto/16 :goto_0

    .line 922
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/d/h;->a:Lcom/google/android/apps/gmm/map/internal/d/d;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/ad;

    iget-object v10, v2, Lcom/google/android/apps/gmm/map/internal/d/ad;->c:Ljava/util/List;

    if-eqz v10, :cond_0

    iget-object v3, v9, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/d/r;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v11

    if-eqz v11, :cond_0

    iget-object v3, v9, Lcom/google/android/apps/gmm/map/internal/d/d;->p:Lcom/google/android/apps/gmm/map/internal/d/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/ag;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v11, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/ad;->d:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v12, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v2, v9, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    invoke-interface {v2, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/au;

    const/4 v3, 0x0

    move v6, v3

    :goto_4
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v3

    if-ge v6, v3, :cond_0

    invoke-interface {v10, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/google/maps/b/cc;

    iget-object v3, v4, Lcom/google/maps/b/cc;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/h/e;->d()Lcom/google/maps/h/e;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/h/e;

    new-instance v13, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v5, v3, Lcom/google/maps/h/e;->b:I

    iget v7, v3, Lcom/google/maps/h/e;->c:I

    iget v3, v3, Lcom/google/maps/h/e;->d:I

    invoke-direct {v13, v5, v7, v3, v12}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    iget-object v3, v4, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_11

    iget v14, v4, Lcom/google/maps/b/cc;->d:I

    div-int v15, v3, v14

    const/4 v3, 0x0

    move v7, v3

    :goto_5
    if-ge v7, v15, :cond_10

    const/4 v3, 0x0

    move v8, v3

    :goto_6
    if-ge v8, v14, :cond_f

    new-instance v16, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v3, v13, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v5, v13, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    add-int/2addr v5, v8

    iget v0, v13, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    move/from16 v17, v0

    add-int v17, v17, v7

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v0, v3, v5, v1, v12}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    mul-int v3, v7, v14

    add-int/2addr v3, v8

    iget-object v5, v4, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v17

    const/4 v3, -0x1

    move/from16 v0, v17

    if-ne v0, v3, :cond_a

    const/4 v3, 0x0

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v5, :cond_8

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    move-object/from16 v0, v16

    invoke-interface {v5, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v5

    if-eqz v5, :cond_8

    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v5

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v5, v0, :cond_8

    const/4 v3, 0x1

    :cond_8
    move-object/from16 v0, v16

    invoke-virtual {v9, v11, v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Z)V

    :cond_9
    :goto_7
    iget-object v0, v9, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    monitor-enter v18

    const/4 v5, 0x0

    :goto_8
    :try_start_2
    iget-object v3, v9, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v5, v3, :cond_e

    iget-object v3, v9, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/internal/d/at;

    if-eqz v3, :cond_d

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v3, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/at;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v3, v5

    :goto_9
    add-int/lit8 v5, v3, 0x1

    goto :goto_8

    :cond_a
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v3, :cond_c

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    move-object/from16 v0, v16

    invoke-interface {v3, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v3

    if-eqz v3, :cond_c

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v5

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v18, v0

    sget-object v19, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    move/from16 v0, v17

    iput v0, v5, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    :cond_b
    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v3

    iget-object v5, v9, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v0, v9, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    move-object/from16 v18, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v5, v1}, Lcom/google/android/apps/gmm/map/b/a/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    iput-wide v0, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    :cond_c
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v3

    if-eqz v3, :cond_9

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v3, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;I)V

    goto :goto_7

    :cond_d
    :try_start_3
    iget-object v0, v9, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    add-int/lit8 v3, v5, -0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_9

    :catchall_1
    move-exception v2

    monitor-exit v18
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    :cond_e
    :try_start_4
    monitor-exit v18
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    add-int/lit8 v3, v8, 0x1

    move v8, v3

    goto/16 :goto_6

    :cond_f
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    goto/16 :goto_5

    :cond_10
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/au;->b(Z)V

    :cond_11
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto/16 :goto_4

    .line 925
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/h;->a:Lcom/google/android/apps/gmm/map/internal/d/d;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v3, v2}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/d;Z)V

    goto/16 :goto_0

    .line 888
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
