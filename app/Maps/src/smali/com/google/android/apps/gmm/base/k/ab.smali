.class public Lcom/google/android/apps/gmm/base/k/ab;
.super Lcom/google/android/libraries/curvular/c;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/ax;


# instance fields
.field private final d:I

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/curvular/f;II)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/curvular/c;-><init>(Lcom/google/android/libraries/curvular/f;I)V

    .line 21
    iput p3, p0, Lcom/google/android/apps/gmm/base/k/ab;->d:I

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/ab;->e:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public static a(II)Lcom/google/android/apps/gmm/base/k/ab;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/base/k/ab;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->a:Lcom/google/android/libraries/curvular/f;

    invoke-direct {v0, v1, p0, p1}, Lcom/google/android/apps/gmm/base/k/ab;-><init>(Lcom/google/android/libraries/curvular/f;II)V

    return-object v0
.end method


# virtual methods
.method protected final a_(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 49
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->p_()Lcom/google/android/apps/gmm/q/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/q/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget v0, p0, Lcom/google/android/apps/gmm/base/k/ab;->d:I

    .line 53
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/curvular/c;->a_(Landroid/content/Context;)I

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 33
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/k/ab;

    if-eqz v0, :cond_0

    .line 34
    invoke-super {p0, p1}, Lcom/google/android/libraries/curvular/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/gmm/base/k/ab;

    iget v0, p1, Lcom/google/android/apps/gmm/base/k/ab;->d:I

    iget v1, p0, Lcom/google/android/apps/gmm/base/k/ab;->d:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/libraries/curvular/c;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/ab;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
