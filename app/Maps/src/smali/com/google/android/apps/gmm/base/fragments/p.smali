.class final Lcom/google/android/apps/gmm/base/fragments/p;
.super Landroid/text/style/ClickableSpan;
.source "PG"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Z

.field final synthetic c:Z

.field final synthetic d:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method constructor <init>(Ljava/lang/String;ZZLcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/fragments/p;->a:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/android/apps/gmm/base/fragments/p;->b:Z

    iput-boolean p3, p0, Lcom/google/android/apps/gmm/base/fragments/p;->c:Z

    iput-object p4, p0, Lcom/google/android/apps/gmm/base/fragments/p;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/p;->a:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/fragments/p;->b:Z

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a(Ljava/lang/String;Z)Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "fullScreen"

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/base/fragments/p;->c:Z

    .line 109
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 110
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/p;->d:Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 111
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 114
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 116
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/p;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/d;->e:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 118
    return-void
.end method
