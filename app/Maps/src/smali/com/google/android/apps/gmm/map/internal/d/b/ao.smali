.class final enum Lcom/google/android/apps/gmm/map/internal/d/b/ao;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/internal/d/b/ao;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

.field public static final enum b:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

.field public static final enum c:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/map/internal/d/b/ao;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 174
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    const-string v1, "INSERT_TYPE_DEFAULT"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    .line 175
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    const-string v1, "INSERT_TYPE_REFCOUNT_CHANGE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->b:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    .line 176
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    const-string v1, "INSERT_TYPE_HEADER_UPDATE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    .line 173
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->b:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->d:[Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 173
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/d/b/ao;
    .locals 1

    .prologue
    .line 173
    const-class v0, Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/internal/d/b/ao;
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->d:[Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/internal/d/b/ao;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    return-object v0
.end method
