.class public Lcom/google/android/apps/gmm/prefetch/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/prefetch/q;


# static fields
.field private static b:[I

.field private static c:[I


# instance fields
.field private final a:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/apps/gmm/map/internal/c/cd;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x9

    .line 44
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/gmm/prefetch/w;->b:[I

    .line 45
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/gmm/prefetch/w;->c:[I

    return-void

    .line 44
    nop

    :array_0
    .array-data 4
        0x2
        0x4
        0x6
        0x8
        0xa
        0xb
        0xc
        0xd
        0xe
    .end array-data

    .line 45
    :array_1
    .array-data 4
        0x2
        0x2
        0x2
        0x2
        0x3
        0x4
        0x4
        0x6
        0x8
    .end array-data
.end method

.method public constructor <init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Lcom/google/android/apps/gmm/map/c/a;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->B_()Landroid/accounts/Account;

    move-result-object v0

    .line 55
    if-eqz v0, :cond_0

    .line 56
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/cd;

    new-array v2, v5, [Lcom/google/android/apps/gmm/map/internal/c/bu;

    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/b;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/map/internal/c/b;-><init>(Landroid/accounts/Account;)V

    aput-object v3, v2, v4

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/cd;-><init>([Lcom/google/android/apps/gmm/map/internal/c/bu;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/prefetch/w;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    .line 61
    :goto_0
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/w;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 62
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 65
    invoke-static {p1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 66
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 68
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->c()Lcom/google/android/apps/gmm/shared/net/a/r;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/r;->a:Lcom/google/r/b/a/ti;

    iget v2, v0, Lcom/google/r/b/a/ti;->b:I

    .line 69
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 70
    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/prefetch/w;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/Set;I)V

    goto :goto_1

    .line 59
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/cd;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/w;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    goto :goto_0

    .line 72
    :cond_1
    const-string v0, "VerticalTileExpander"

    const-string v1, "Total tiles in queue = %d"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetch/w;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/Set;I)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 130
    const-string v2, "VerticalTileExpander"

    const-string v3, "Adding tiles for location %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    const/4 v3, 0x0

    .line 134
    sget-object v2, Lcom/google/android/apps/gmm/prefetch/w;->c:[I

    array-length v2, v2

    sget-object v4, Lcom/google/android/apps/gmm/prefetch/w;->b:[I

    array-length v4, v4

    if-ne v2, v4, :cond_0

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 135
    :cond_1
    const/4 v2, 0x0

    :goto_1
    sget-object v4, Lcom/google/android/apps/gmm/prefetch/w;->b:[I

    array-length v4, v4

    if-ge v2, v4, :cond_6

    .line 136
    sget-object v4, Lcom/google/android/apps/gmm/prefetch/w;->b:[I

    aget v10, v4, v2

    .line 137
    sget-object v4, Lcom/google/android/apps/gmm/prefetch/w;->c:[I

    aget v6, v4, v2

    .line 138
    const/4 v5, 0x0

    .line 139
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    sub-int v7, v10, v4

    .line 140
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    int-to-double v8, v4

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    int-to-double v14, v7

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    mul-double/2addr v8, v12

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-int v4, v8

    sub-int/2addr v4, v6

    add-int/lit8 v4, v4, 0x1

    .line 141
    move-object/from16 v0, p1

    iget v8, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    int-to-double v8, v8

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    int-to-double v14, v7

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    mul-double/2addr v8, v12

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-int v8, v8

    add-int/2addr v8, v6

    add-int/lit8 v11, v8, -0x1

    .line 142
    move-object/from16 v0, p1

    iget v8, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    int-to-double v8, v8

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    int-to-double v14, v7

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    mul-double/2addr v8, v12

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-int v8, v8

    sub-int/2addr v8, v6

    add-int/lit8 v8, v8, 0x1

    .line 143
    move-object/from16 v0, p1

    iget v9, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    int-to-double v12, v9

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    int-to-double v0, v7

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->floor(D)D

    move-result-wide v12

    double-to-int v7, v12

    add-int/2addr v6, v7

    add-int/lit8 v12, v6, -0x1

    .line 144
    const/4 v6, 0x1

    shl-int v13, v6, v10

    move v9, v4

    move v4, v5

    .line 146
    :goto_2
    if-gt v9, v11, :cond_5

    move v7, v8

    move v5, v3

    .line 147
    :goto_3
    if-gt v7, v12, :cond_4

    .line 148
    if-ltz v9, :cond_2

    rem-int v3, v9, v13

    move v6, v3

    .line 149
    :goto_4
    if-ltz v7, :cond_3

    rem-int v3, v7, v13

    .line 151
    :goto_5
    new-instance v14, Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/prefetch/w;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v14, v10, v6, v3, v15}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    .line 152
    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    invoke-interface/range {p2 .. p2}, Ljava/util/Set;->size()I

    move-result v3

    move/from16 v0, p3

    if-ge v3, v0, :cond_7

    .line 153
    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 154
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/prefetch/w;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3, v14}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 155
    add-int/lit8 v5, v5, 0x1

    .line 156
    add-int/lit8 v3, v4, 0x1

    move v4, v5

    .line 147
    :goto_6
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    move v5, v4

    move v4, v3

    goto :goto_3

    .line 148
    :cond_2
    rem-int v3, v9, v13

    add-int/2addr v3, v13

    move v6, v3

    goto :goto_4

    .line 149
    :cond_3
    rem-int v3, v7, v13

    add-int/2addr v3, v13

    goto :goto_5

    .line 146
    :cond_4
    add-int/lit8 v3, v9, 0x1

    move v9, v3

    move v3, v5

    goto :goto_2

    .line 160
    :cond_5
    const-string v5, "VerticalTileExpander"

    const-string v6, "Added %d tiles on level %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v8

    const/4 v4, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v5, v6, v7}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 135
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 163
    :cond_6
    const-string v2, "VerticalTileExpander"

    const-string v4, "Tiles added to queue - %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v6

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 164
    return-void

    :cond_7
    move v3, v4

    move v4, v5

    goto :goto_6
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/w;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/w;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    return v0
.end method
