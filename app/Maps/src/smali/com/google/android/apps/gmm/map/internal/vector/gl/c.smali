.class public final enum Lcom/google/android/apps/gmm/map/internal/vector/gl/c;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/internal/vector/gl/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

.field public static final enum b:Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

.field public static final enum c:Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

.field private static final synthetic f:[Lcom/google/android/apps/gmm/map/internal/vector/gl/c;


# instance fields
.field d:Z

.field e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    const-string v1, "EXTRUDE_BOTH"

    invoke-direct {v0, v1, v3, v2, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    .line 29
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    const-string v1, "EXTRUDE_LEFT"

    invoke-direct {v0, v1, v2, v2, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    const-string v1, "EXTRUDE_RIGHT"

    invoke-direct {v0, v1, v4, v3, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    .line 24
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->f:[Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->d:Z

    .line 42
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->e:Z

    .line 43
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/vector/gl/c;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/internal/vector/gl/c;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->f:[Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    return-object v0
.end method
