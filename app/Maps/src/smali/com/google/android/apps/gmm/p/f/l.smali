.class public Lcom/google/android/apps/gmm/p/f/l;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final g:J


# instance fields
.field final a:Lcom/google/android/apps/gmm/shared/c/f;

.field final b:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/google/android/apps/gmm/p/b/j;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field c:Z

.field d:I

.field e:F

.field f:F

.field private final h:Lcom/google/android/apps/gmm/p/b/k;

.field private i:J

.field private j:F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 28
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/p/f/l;->g:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/p/b/k;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/l;->b:Ljava/util/WeakHashMap;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/p/f/l;->d:I

    .line 57
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/p/f/l;->e:F

    .line 62
    const/high16 v0, -0x3b860000    # -1000.0f

    iput v0, p0, Lcom/google/android/apps/gmm/p/f/l;->f:F

    .line 78
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/p/b/k;

    iput-object p1, p0, Lcom/google/android/apps/gmm/p/f/l;->h:Lcom/google/android/apps/gmm/p/b/k;

    .line 79
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/shared/c/f;

    iput-object p2, p0, Lcom/google/android/apps/gmm/p/f/l;->a:Lcom/google/android/apps/gmm/shared/c/f;

    .line 80
    return-void
.end method


# virtual methods
.method public final a(F)F
    .locals 8

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/l;->a:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    iget-wide v0, p0, Lcom/google/android/apps/gmm/p/f/l;->i:J

    sub-long v0, v4, v0

    sget-wide v2, Lcom/google/android/apps/gmm/p/f/l;->g:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/l;->h:Lcom/google/android/apps/gmm/p/b/k;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/k;->a()Landroid/location/Location;

    move-result-object v3

    if-eqz v3, :cond_0

    iput-wide v4, p0, Lcom/google/android/apps/gmm/p/f/l;->i:J

    new-instance v0, Landroid/hardware/GeomagneticField;

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    double-to-float v1, v6

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    double-to-float v2, v6

    invoke-virtual {v3}, Landroid/location/Location;->getAltitude()D

    move-result-wide v6

    double-to-float v3, v6

    invoke-direct/range {v0 .. v5}, Landroid/hardware/GeomagneticField;-><init>(FFFJ)V

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getDeclination()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/p/f/l;->j:F

    .line 148
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/p/f/l;->j:F

    add-float/2addr v0, p1

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/s;->b(F)F

    move-result v0

    return v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/l;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/b/j;

    .line 112
    iget v2, p0, Lcom/google/android/apps/gmm/p/f/l;->d:I

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/p/b/j;->a(I)V

    goto :goto_0

    .line 114
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/l;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/b/j;

    .line 142
    iget v2, p0, Lcom/google/android/apps/gmm/p/f/l;->e:F

    iget v3, p0, Lcom/google/android/apps/gmm/p/f/l;->f:F

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/gmm/p/b/j;->a(FF)V

    goto :goto_0

    .line 144
    :cond_0
    return-void
.end method
