.class Lcom/google/android/apps/gmm/base/layout/g;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PG"


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/google/android/apps/gmm/base/activities/p;

.field final synthetic c:Lcom/google/android/apps/gmm/base/activities/z;

.field final synthetic d:Lcom/google/android/apps/gmm/base/layout/MainLayout;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;Landroid/view/View;Lcom/google/android/apps/gmm/base/activities/p;Lcom/google/android/apps/gmm/base/activities/z;)V
    .locals 0

    .prologue
    .line 1816
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/layout/g;->d:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iput-object p2, p0, Lcom/google/android/apps/gmm/base/layout/g;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/apps/gmm/base/layout/g;->b:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p4, p0, Lcom/google/android/apps/gmm/base/layout/g;->c:Lcom/google/android/apps/gmm/base/activities/z;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1819
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/g;->a:Landroid/view/View;

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1820
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/g;->b:Lcom/google/android/apps/gmm/base/activities/p;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->M:Z

    if-eqz v0, :cond_0

    .line 1833
    :goto_0
    return-void

    .line 1826
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/g;->a:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1827
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/g;->d:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->setAllowLayoutDuringAnimation(Z)V

    .line 1828
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/g;->d:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/g;->b:Lcom/google/android/apps/gmm/base/activities/p;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/layout/MainLayout;Lcom/google/android/apps/gmm/base/activities/p;Landroid/view/View;)Z

    .line 1829
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/g;->d:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->setAllowLayoutDuringAnimation(Z)V

    .line 1832
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/g;->c:Lcom/google/android/apps/gmm/base/activities/z;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/g;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/z;->c(Landroid/view/View;)V

    goto :goto_0
.end method
