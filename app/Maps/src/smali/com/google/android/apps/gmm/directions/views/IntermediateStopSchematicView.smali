.class public Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;
.super Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;
.source "PG"


# instance fields
.field public final f:F

.field public g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;->e:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/e;->ab:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;->f:F

    .line 36
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;->g:I

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;->a(Landroid/graphics/Canvas;FFI)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;->f:F

    iget v2, p0, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;->g:I

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;->b(Landroid/graphics/Canvas;FFI)V

    .line 51
    return-void
.end method
