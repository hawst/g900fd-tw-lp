.class public Lcom/google/android/apps/gmm/map/internal/c/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/bu;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/f;->a:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bv;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->d:Lcom/google/android/apps/gmm/map/internal/c/bv;

    return-object v0
.end method

.method public final a(Lcom/google/e/a/a/a/b;)V
    .locals 3

    .prologue
    .line 96
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/f;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 97
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ai;)Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/f;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/c/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 15
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/bu;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/c/f;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 62
    if-ne p0, p1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v0

    .line 65
    :cond_1
    if-nez p1, :cond_2

    .line 66
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/f;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 68
    :cond_2
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/internal/c/f;

    if-nez v2, :cond_3

    move v0, v1

    .line 69
    goto :goto_0

    .line 72
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/f;->a:Ljava/lang/String;

    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/f;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/f;->a:Ljava/lang/String;

    if-eq v2, v3, :cond_0

    if-eqz v2, :cond_4

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/f;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/f;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/f;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/f;->a:Ljava/lang/String;

    goto :goto_0
.end method
