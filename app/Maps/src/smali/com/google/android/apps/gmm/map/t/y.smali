.class public Lcom/google/android/apps/gmm/map/t/y;
.super Lcom/google/android/apps/gmm/v/bb;
.source "PG"


# static fields
.field private static final d:[Ljava/lang/Object;


# instance fields
.field final a:Z

.field final b:Z

.field final c:Lcom/google/android/apps/gmm/map/t/x;

.field private final e:Lcom/google/android/apps/gmm/map/j/s;

.field private final f:[Ljava/lang/Object;

.field private final g:Lcom/google/android/apps/gmm/map/util/b/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lcom/google/android/apps/gmm/map/t/y;->d:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/t/x;Lcom/google/android/apps/gmm/map/j/s;Lcom/google/android/apps/gmm/map/util/b/g;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 70
    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v5, Lcom/google/android/apps/gmm/map/t/y;->d:[Ljava/lang/Object;

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/t/y;-><init>(ZZLcom/google/android/apps/gmm/map/t/x;Lcom/google/android/apps/gmm/map/j/s;[Ljava/lang/Object;Lcom/google/android/apps/gmm/map/util/b/g;)V

    .line 72
    return-void
.end method

.method public constructor <init>(ZZLcom/google/android/apps/gmm/map/t/x;Lcom/google/android/apps/gmm/map/j/s;[Ljava/lang/Object;Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/bb;-><init>()V

    .line 56
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/t/y;->a:Z

    .line 57
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/map/t/y;->b:Z

    .line 58
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/t/y;->c:Lcom/google/android/apps/gmm/map/t/x;

    .line 59
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/t/y;->e:Lcom/google/android/apps/gmm/map/j/s;

    .line 60
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/t/y;->f:[Ljava/lang/Object;

    .line 61
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/t/y;->g:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 62
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/map/t/y;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 87
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/google/android/apps/gmm/map/j/r;->a:Lcom/google/android/apps/gmm/map/j/r;

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/t/y;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/j/r;)V

    .line 97
    return-void
.end method

.method final a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/j/r;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/y;->e:Lcom/google/android/apps/gmm/map/j/s;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/j/s;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/j/r;)Lcom/google/android/apps/gmm/map/j/q;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/y;->f:[Ljava/lang/Object;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/j/q;->c:[Ljava/lang/Object;

    .line 102
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/y;->g:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 103
    return-void
.end method
