.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/h;
.super Lcom/google/android/apps/gmm/map/t/an;
.source "PG"


# instance fields
.field public a:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 142
    const-string v0, "uniform mat4 uMVPMatrix;\nuniform mat3 uTextureMatrix;\nuniform highp int uTextureMode;\nuniform float cameraZoom;\nattribute vec4 aPosition;\nattribute vec2 aTextureCoord;\nattribute vec4 aColor;\nattribute float relativeZoom;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nvoid main() {\n  float offsetScale = 1.5;\n  float zoom = relativeZoom;\n  float upSlope = (cameraZoom - zoom * offsetScale) * 2.0;\n  zoom = min(1.0, max(0.0, upSlope));\n  gl_Position = uMVPMatrix * aPosition;\n  vTextureCoord = (uTextureMatrix * vec3(aTextureCoord, 1.0)).xy;\n  float zoomAlpha = aColor.a;\n  vColor = vec4(aColor.rgb, zoomAlpha * max(1.0, zoom));\n}\n"

    const-string v1, "precision mediump float;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nuniform sampler2D sTexture0;\nuniform float brightnessScale;\nvoid main() {\n  float t = texture2D(sTexture0, vTextureCoord).a * vColor.a;\n  gl_FragColor = vec4(brightnessScale * vColor.rgb, t);\n}\n"

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/t/an;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 2

    .prologue
    .line 147
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/map/t/an;->a(I)V

    .line 149
    const-string v0, "cameraZoom"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/h;->a:I

    .line 150
    const-string v0, "OneWayRoadShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/h;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 152
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get cameraZoom handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_0
    return-void
.end method

.method protected final b(I)V
    .locals 2

    .prologue
    .line 158
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/map/t/an;->b(I)V

    .line 159
    const/4 v0, 0x5

    const-string v1, "relativeZoom"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 161
    return-void
.end method
