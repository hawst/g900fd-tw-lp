.class Lcom/google/android/apps/gmm/car/bd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/bc;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/bc;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/bd;->a:Lcom/google/android/apps/gmm/car/bc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 21
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 144
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/bd;->a:Lcom/google/android/apps/gmm/car/bc;

    iget-object v12, v2, Lcom/google/android/apps/gmm/car/bc;->e:Ljava/lang/Object;

    monitor-enter v12

    .line 145
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/car/bd;->a:Lcom/google/android/apps/gmm/car/bc;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    iget-wide v2, v13, Lcom/google/android/apps/gmm/car/bc;->r:J

    iget v4, v13, Lcom/google/android/apps/gmm/car/bc;->s:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    cmp-long v2, v14, v2

    if-ltz v2, :cond_d

    iget-boolean v3, v13, Lcom/google/android/apps/gmm/car/bc;->h:Z

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    if-eq v3, v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, v13, Lcom/google/android/apps/gmm/car/bc;->h:Z

    sget-object v2, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    iget-boolean v2, v13, Lcom/google/android/apps/gmm/car/bc;->h:Z

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x1f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "navigationRunningState => "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    iget-boolean v2, v13, Lcom/google/android/apps/gmm/car/bc;->h:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v13, v2}, Lcom/google/android/apps/gmm/car/bc;->a(I)V

    iput-wide v14, v13, Lcom/google/android/apps/gmm/car/bc;->r:J

    :cond_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_d

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v2, :cond_5

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 146
    :catchall_0
    move-exception v2

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 145
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    const/4 v2, 0x2

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    :cond_5
    :try_start_1
    check-cast v2, Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v4, v3, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v3, v3, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v3, v4, v3

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v3, :cond_d

    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v4, v3, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v3, v3, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v16, v4, v3

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    iget-object v5, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->e:Lcom/google/maps/g/a/fd;

    sget-object v6, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    invoke-virtual {v6, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_e

    sget-object v5, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    invoke-virtual {v5, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move v11, v4

    :goto_4
    iget-object v4, v13, Lcom/google/android/apps/gmm/car/bc;->g:Lcom/google/android/apps/gmm/navigation/util/b;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/gmm/navigation/util/b;->a(Lcom/google/android/apps/gmm/navigation/g/b/f;)V

    iget-object v2, v13, Lcom/google/android/apps/gmm/car/bc;->g:Lcom/google/android/apps/gmm/navigation/util/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/util/b;->j:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v17

    iget v0, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->f:I

    move/from16 v18, v0

    iget v0, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->g:I

    move/from16 v19, v0

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    sget-object v4, Lcom/google/android/apps/gmm/car/bf;->b:[I

    invoke-virtual {v2}, Lcom/google/maps/g/a/fb;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    sget-object v4, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2f

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "convertStepTurnSideToInt: illegal stepTurnSide "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    move v10, v2

    :goto_5
    iget v2, v13, Lcom/google/android/apps/gmm/car/bc;->k:I

    if-ne v11, v2, :cond_6

    iget-object v2, v13, Lcom/google/android/apps/gmm/car/bc;->l:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget v2, v13, Lcom/google/android/apps/gmm/car/bc;->m:I

    move/from16 v0, v18

    if-ne v2, v0, :cond_6

    iget v2, v13, Lcom/google/android/apps/gmm/car/bc;->n:I

    move/from16 v0, v19

    if-ne v2, v0, :cond_6

    iget v2, v13, Lcom/google/android/apps/gmm/car/bc;->p:I

    if-eq v2, v10, :cond_9

    :cond_6
    const/4 v7, 0x0

    iget v2, v13, Lcom/google/android/apps/gmm/car/bc;->t:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_8

    iget v2, v13, Lcom/google/android/apps/gmm/car/bc;->v:I

    if-lez v2, :cond_8

    iget v2, v13, Lcom/google/android/apps/gmm/car/bc;->u:I

    if-lez v2, :cond_8

    iget v2, v13, Lcom/google/android/apps/gmm/car/bc;->w:I

    if-lez v2, :cond_8

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->e:Lcom/google/maps/g/a/fd;

    aput-object v5, v2, v4

    const/4 v4, 0x1

    iget-object v5, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    aput-object v5, v2, v4

    const/4 v4, 0x2

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v2}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    iget-object v2, v13, Lcom/google/android/apps/gmm/car/bc;->f:Landroid/util/SparseArray;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_7

    iget v2, v13, Lcom/google/android/apps/gmm/car/bc;->w:I

    sparse-switch v2, :sswitch_data_0

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :goto_6
    iget-object v2, v13, Lcom/google/android/apps/gmm/car/bc;->d:Landroid/content/Context;

    const/4 v4, -0x1

    iget v5, v13, Lcom/google/android/apps/gmm/car/bc;->v:I

    iget v6, v13, Lcom/google/android/apps/gmm/car/bc;->u:I

    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v9, 0x64

    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/gmm/directions/views/c;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ag;IIILandroid/graphics/Bitmap$Config;Landroid/graphics/Bitmap$CompressFormat;I)[B

    move-result-object v2

    iget-object v3, v13, Lcom/google/android/apps/gmm/car/bc;->f:Landroid/util/SparseArray;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_7
    iget-object v2, v13, Lcom/google/android/apps/gmm/car/bc;->f:Landroid/util/SparseArray;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    move-object v7, v2

    :cond_8
    move-object v2, v13

    move v3, v11

    move-object/from16 v4, v17

    move/from16 v5, v18

    move/from16 v6, v19

    move v8, v10

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/apps/gmm/car/bc;->a(ILjava/lang/String;II[BI)V

    iput-object v7, v13, Lcom/google/android/apps/gmm/car/bc;->o:[B

    iput-wide v14, v13, Lcom/google/android/apps/gmm/car/bc;->r:J

    :cond_9
    move-object/from16 v0, v16

    iget v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    move-object/from16 v0, v16

    iget v3, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->e:I

    iget v4, v13, Lcom/google/android/apps/gmm/car/bc;->i:I

    if-ne v2, v4, :cond_a

    iget v4, v13, Lcom/google/android/apps/gmm/car/bc;->j:I

    if-eq v3, v4, :cond_c

    :cond_a
    iget-boolean v4, v13, Lcom/google/android/apps/gmm/car/bc;->q:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v4, :cond_b

    :try_start_2
    iget-object v4, v13, Lcom/google/android/apps/gmm/car/bc;->c:Lcom/google/android/gms/car/ak;

    invoke-virtual {v4, v2, v3}, Lcom/google/android/gms/car/ak;->a(II)Z
    :try_end_2
    .catch Lcom/google/android/gms/car/ao; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_b
    :goto_7
    :try_start_3
    iput-wide v14, v13, Lcom/google/android/apps/gmm/car/bc;->r:J

    :cond_c
    iput v2, v13, Lcom/google/android/apps/gmm/car/bc;->i:I

    iput v3, v13, Lcom/google/android/apps/gmm/car/bc;->j:I

    iput v11, v13, Lcom/google/android/apps/gmm/car/bc;->k:I

    move-object/from16 v0, v17

    iput-object v0, v13, Lcom/google/android/apps/gmm/car/bc;->l:Ljava/lang/String;

    move/from16 v0, v19

    iput v0, v13, Lcom/google/android/apps/gmm/car/bc;->n:I

    move/from16 v0, v18

    iput v0, v13, Lcom/google/android/apps/gmm/car/bc;->m:I

    iput v10, v13, Lcom/google/android/apps/gmm/car/bc;->p:I

    .line 146
    :cond_d
    monitor-exit v12

    return-void

    .line 145
    :cond_e
    sget-object v6, Lcom/google/maps/g/a/ez;->f:Lcom/google/maps/g/a/ez;

    if-ne v4, v6, :cond_f

    sget-object v4, Lcom/google/android/apps/gmm/car/bf;->a:[I

    invoke-virtual {v5}, Lcom/google/maps/g/a/fd;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    const/4 v4, 0x4

    move v11, v4

    goto/16 :goto_4

    :pswitch_0
    const/4 v4, 0x3

    move v11, v4

    goto/16 :goto_4

    :pswitch_1
    const/4 v4, 0x5

    move v11, v4

    goto/16 :goto_4

    :cond_f
    sget-object v5, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x35

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "convertManeuverTypeToTurnEvent: illegal maneuverType "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    move v11, v4

    goto/16 :goto_4

    :pswitch_2
    const/4 v2, 0x1

    move v10, v2

    goto/16 :goto_5

    :pswitch_3
    const/4 v2, 0x2

    move v10, v2

    goto/16 :goto_5

    :pswitch_4
    const/4 v2, 0x3

    move v10, v2

    goto/16 :goto_5

    :sswitch_0
    sget-object v7, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    goto/16 :goto_6

    :sswitch_1
    sget-object v7, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    goto/16 :goto_6

    :catch_0
    move-exception v4

    sget-object v4, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_7

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x10 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
