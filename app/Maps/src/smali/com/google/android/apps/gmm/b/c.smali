.class public Lcom/google/android/apps/gmm/b/c;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/b/d;

.field b:Ljava/lang/String;

.field c:Lcom/google/e/a/a/a/b;

.field d:Lcom/google/e/a/a/a/b;

.field e:Z

.field f:Lcom/google/android/apps/gmm/b/f;

.field private h:Lcom/google/android/apps/gmm/b/g;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 118
    sget-object v0, Lcom/google/r/b/a/el;->ca:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/ac;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 119
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 4
    .param p1    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 138
    if-eqz p1, :cond_0

    const/4 v2, 0x3

    .line 139
    iget-object v3, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_0

    .line 140
    iget-object v2, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_2

    :goto_1
    if-nez v0, :cond_3

    .line 141
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->j:Lcom/google/android/apps/gmm/shared/net/k;

    .line 144
    :goto_2
    return-object v0

    :cond_1
    move v2, v1

    .line 139
    goto :goto_0

    :cond_2
    move v0, v1

    .line 140
    goto :goto_1

    .line 143
    :cond_3
    new-instance v0, Lcom/google/android/apps/gmm/b/g;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/b/g;-><init>(Lcom/google/e/a/a/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/b/c;->h:Lcom/google/android/apps/gmm/b/g;

    .line 144
    const/4 v0, 0x0

    goto :goto_2
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 4

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/b/c;->d:Lcom/google/e/a/a/a/b;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/b/c;->a:Lcom/google/android/apps/gmm/b/d;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 127
    :cond_1
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/ac;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/b/c;->a:Lcom/google/android/apps/gmm/b/d;

    .line 128
    iget v2, v2, Lcom/google/android/apps/gmm/b/d;->c:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/android/apps/gmm/b/c;->b:Ljava/lang/String;

    .line 129
    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/b/c;->c:Lcom/google/e/a/a/a/b;

    .line 130
    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/b/c;->d:Lcom/google/e/a/a/a/b;

    .line 131
    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v2, 0x6

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/b/c;->e:Z

    .line 132
    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    :goto_0
    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    return-object v1

    :cond_2
    sget-object v0, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/b/c;->f:Lcom/google/android/apps/gmm/b/f;

    if-eqz v0, :cond_0

    .line 151
    if-nez p1, :cond_1

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/b/c;->f:Lcom/google/android/apps/gmm/b/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/b/c;->h:Lcom/google/android/apps/gmm/b/g;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/b/f;->a(Lcom/google/android/apps/gmm/b/g;)V

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/b/c;->f:Lcom/google/android/apps/gmm/b/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/b/f;->e()V

    goto :goto_0
.end method
