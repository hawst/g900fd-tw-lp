.class public Lcom/google/android/apps/gmm/place/QuPlacePageView;
.super Lcom/google/android/apps/gmm/place/PlacePageView;
.source "PG"


# instance fields
.field private c:Lcom/google/android/apps/gmm/place/r;

.field private d:Lcom/google/android/apps/gmm/place/q;

.field private e:Lcom/google/android/apps/gmm/place/riddler/c;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/place/PlacePageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->c:Lcom/google/android/apps/gmm/place/r;

    .line 27
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->d:Lcom/google/android/apps/gmm/place/q;

    .line 28
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->e:Lcom/google/android/apps/gmm/place/riddler/c;

    .line 33
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    .line 34
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    .line 36
    const-class v2, Lcom/google/android/apps/gmm/place/c/k;

    invoke-virtual {v0, v2, p0}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/place/bq;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/place/bq;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/place/r;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    .line 39
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/place/af;->L()Lcom/google/android/apps/gmm/base/l/a/q;

    move-result-object v2

    invoke-direct {v0, v2, v1, p0}, Lcom/google/android/apps/gmm/place/r;-><init>(Lcom/google/android/apps/gmm/base/l/a/q;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/place/PlacePageView;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->c:Lcom/google/android/apps/gmm/place/r;

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/place/q;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    iget-object v2, v2, Lcom/google/android/apps/gmm/place/af;->b:Lcom/google/android/apps/gmm/base/l/j;

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/place/q;-><init>(Lcom/google/android/apps/gmm/base/l/j;Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->d:Lcom/google/android/apps/gmm/place/q;

    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/place/riddler/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/place/riddler/c;-><init>(Lcom/google/android/apps/gmm/place/af;Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->e:Lcom/google/android/apps/gmm/place/riddler/c;

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 47
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/QuPlacePageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/apps/gmm/place/af;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V

    .line 71
    sget-object v0, Lcom/google/android/apps/gmm/place/c/f;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v1

    .line 73
    if-eqz v1, :cond_0

    .line 74
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    .line 75
    sget v2, Lcom/google/android/apps/gmm/l;->ce:I

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/place/g;->a(Landroid/view/View;Ljava/lang/CharSequence;I)V

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->c:Lcom/google/android/apps/gmm/place/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/r;->a()V

    .line 82
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/place/PlacePageView;->a(Z)V

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/place/af;->a(Z)V

    .line 88
    sget-object v0, Lcom/google/android/apps/gmm/place/c/f;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_0

    .line 90
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/af;->J()V

    .line 93
    :cond_0
    return-void

    .line 87
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ad_()V
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/android/apps/gmm/place/t;->a:Lcom/google/android/libraries/curvular/bk;

    .line 64
    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    .line 65
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/view/View;)V

    .line 66
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/google/android/apps/gmm/place/c/f;->b:Lcom/google/android/libraries/curvular/bk;

    .line 52
    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 53
    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->f:I

    .line 58
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->f:I

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 97
    invoke-super {p0}, Lcom/google/android/apps/gmm/place/PlacePageView;->onAttachedToWindow()V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->c:Lcom/google/android/apps/gmm/place/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/r;->b()V

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->d:Lcom/google/android/apps/gmm/place/q;

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/q;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;)V

    .line 100
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->e:Lcom/google/android/apps/gmm/place/riddler/c;

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/riddler/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;)V

    .line 103
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/place/PlacePageView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->c:Lcom/google/android/apps/gmm/place/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/r;->c()V

    .line 119
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 107
    invoke-super {p0}, Lcom/google/android/apps/gmm/place/PlacePageView;->onDetachedFromWindow()V

    .line 108
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->c:Lcom/google/android/apps/gmm/place/r;

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/r;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 109
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->d:Lcom/google/android/apps/gmm/place/q;

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/q;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 110
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/QuPlacePageView;->e:Lcom/google/android/apps/gmm/place/riddler/c;

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/riddler/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 113
    return-void
.end method

.method public setVisibility(I)V
    .locals 0

    .prologue
    .line 123
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/place/PlacePageView;->setVisibility(I)V

    .line 130
    return-void
.end method
