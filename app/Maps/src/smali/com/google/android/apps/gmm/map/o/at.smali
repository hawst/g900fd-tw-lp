.class public Lcom/google/android/apps/gmm/map/o/at;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private A:Lcom/google/android/apps/gmm/map/o/a/e;

.field private B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ">;"
        }
    .end annotation
.end field

.field final a:Lcom/google/android/apps/gmm/map/o/o;

.field final b:Lcom/google/android/apps/gmm/map/o/z;

.field final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/d;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/f;",
            ">;"
        }
    .end annotation
.end field

.field e:La/a/a/a/b/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/b/s",
            "<",
            "Lcom/google/android/apps/gmm/map/o/d;",
            ">;"
        }
    .end annotation
.end field

.field f:La/a/a/a/b/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/b/s",
            "<",
            "Lcom/google/android/apps/gmm/map/o/d;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/apps/gmm/map/internal/c/an;",
            ">;"
        }
    .end annotation
.end field

.field h:Lcom/google/android/apps/gmm/map/o/a;

.field i:Lcom/google/android/apps/gmm/map/o/ap;

.field j:Lcom/google/android/apps/gmm/map/t/b;

.field k:Lcom/google/android/apps/gmm/map/o/az;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/o/az",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/o/d;",
            ">;"
        }
    .end annotation
.end field

.field final l:Lcom/google/android/apps/gmm/map/o/av;

.field final m:Lcom/google/android/apps/gmm/v/cp;

.field final n:Lcom/google/android/apps/gmm/map/o/ab;

.field final o:Lcom/google/android/apps/gmm/map/q/a;

.field final p:Lcom/google/android/apps/gmm/shared/net/a/b;

.field final q:Lcom/google/android/apps/gmm/map/util/b/g;

.field final r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/j/g;",
            ">;"
        }
    .end annotation
.end field

.field s:Lcom/google/android/apps/gmm/map/indoor/a/a;

.field final t:Lcom/google/android/apps/gmm/map/o/a/c;

.field u:Lcom/google/android/apps/gmm/map/o/a/i;

.field v:Lcom/google/android/apps/gmm/map/o/a/e;

.field volatile w:Z

.field volatile x:Z

.field y:Lcom/google/android/apps/gmm/map/o/au;

.field z:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/q/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)V
    .locals 7

    .prologue
    .line 236
    new-instance v6, Lcom/google/android/apps/gmm/map/o/av;

    .line 237
    invoke-interface {p3}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    invoke-direct {v6, p1, p2}, Lcom/google/android/apps/gmm/map/o/av;-><init>(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/v/ad;)V

    new-instance v0, Lcom/google/android/apps/gmm/map/o/z;

    .line 239
    iget v5, p1, Lcom/google/android/apps/gmm/map/f/o;->i:F

    move-object v1, p2

    move-object v2, p3

    move-object v3, p5

    move-object v4, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/o/z;-><init>(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;F)V

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, v6

    move-object v6, v0

    .line 236
    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/o/at;-><init>(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/q/a;Lcom/google/android/apps/gmm/map/o/av;Lcom/google/android/apps/gmm/map/o/z;)V

    .line 240
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/q/a;Lcom/google/android/apps/gmm/map/o/av;Lcom/google/android/apps/gmm/map/o/z;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->c:Ljava/util/List;

    .line 132
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->d:Ljava/util/Set;

    .line 147
    new-instance v0, La/a/a/a/b/s;

    invoke-direct {v0}, La/a/a/a/b/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->e:La/a/a/a/b/s;

    .line 157
    new-instance v0, La/a/a/a/b/s;

    invoke-direct {v0}, La/a/a/a/b/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->f:La/a/a/a/b/s;

    .line 165
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->g:Ljava/util/Map;

    .line 169
    sget-object v0, Lcom/google/android/apps/gmm/map/o/ap;->o:Lcom/google/android/apps/gmm/map/o/ap;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->i:Lcom/google/android/apps/gmm/map/o/ap;

    .line 171
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->j:Lcom/google/android/apps/gmm/map/t/b;

    .line 197
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->r:Ljava/util/Set;

    .line 201
    new-instance v0, Lcom/google/android/apps/gmm/map/o/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->t:Lcom/google/android/apps/gmm/map/o/a/c;

    .line 209
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->B:Ljava/util/List;

    .line 232
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/at;->w:Z

    .line 246
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/o/at;->l:Lcom/google/android/apps/gmm/map/o/av;

    .line 247
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/o/at;->o:Lcom/google/android/apps/gmm/map/q/a;

    .line 248
    new-instance v0, Lcom/google/android/apps/gmm/v/cr;

    invoke-direct {v0, p4}, Lcom/google/android/apps/gmm/v/cr;-><init>(Lcom/google/android/apps/gmm/v/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->m:Lcom/google/android/apps/gmm/v/cp;

    .line 249
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/o/at;->b:Lcom/google/android/apps/gmm/map/o/z;

    .line 250
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->s:Lcom/google/android/apps/gmm/map/indoor/a/a;

    .line 251
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->p:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 252
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ab;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/at;->p:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/map/o/ab;-><init>(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/net/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->n:Lcom/google/android/apps/gmm/map/o/ab;

    .line 254
    new-instance v0, Lcom/google/android/apps/gmm/map/o/a/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->t:Lcom/google/android/apps/gmm/map/o/a/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/o/a/i;-><init>(Lcom/google/android/apps/gmm/map/o/a/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->u:Lcom/google/android/apps/gmm/map/o/a/i;

    .line 255
    new-instance v0, Lcom/google/android/apps/gmm/map/o/a/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->t:Lcom/google/android/apps/gmm/map/o/a/c;

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/apps/gmm/map/o/a/e;-><init>(Lcom/google/android/apps/gmm/map/o/a/c;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/ab;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->v:Lcom/google/android/apps/gmm/map/o/a/e;

    .line 256
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->q:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 258
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v1

    .line 259
    new-instance v0, Lcom/google/android/apps/gmm/map/o/au;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/au;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->y:Lcom/google/android/apps/gmm/map/o/au;

    .line 260
    new-instance v0, Lcom/google/android/apps/gmm/map/o/o;

    const/16 v2, 0x60

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/map/o/o;-><init>(ILcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->a:Lcom/google/android/apps/gmm/map/o/o;

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->p:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 262
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->d()Lcom/google/android/apps/gmm/shared/net/a/t;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget v0, v0, Lcom/google/r/b/a/aqg;->n:I

    invoke-static {v0}, Lcom/google/r/b/a/aqj;->a(I)Lcom/google/r/b/a/aqj;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/r/b/a/aqj;->a:Lcom/google/r/b/a/aqj;

    :cond_0
    sget-object v2, Lcom/google/r/b/a/aqj;->a:Lcom/google/r/b/a/aqj;

    invoke-virtual {v0, v2}, Lcom/google/r/b/a/aqj;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/at;->z:Z

    .line 263
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ba;

    const-string v2, "PooledListMultimap"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/o/ba;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/gmm/map/o/az;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/o/az;-><init>(Lcom/google/android/apps/gmm/map/util/a/i;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->k:Lcom/google/android/apps/gmm/map/o/az;

    .line 264
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v1, Lcom/google/android/apps/gmm/v/af;

    const/4 v2, 0x1

    invoke-direct {v1, p4, v2}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 265
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/map/internal/c/an;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/o/ak;)I
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 995
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->o:Lcom/google/android/apps/gmm/map/internal/c/cf;

    if-eqz v0, :cond_1

    move v0, v4

    :goto_0
    if-eqz v0, :cond_2

    .line 996
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->o:Lcom/google/android/apps/gmm/map/internal/c/cf;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cf;->a:J

    .line 997
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 1064
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v3

    .line 995
    goto :goto_0

    .line 1006
    :cond_2
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->i:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 1007
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    move v5, v3

    move v2, v3

    :goto_2
    if-ge v5, v8, :cond_9

    .line 1008
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    .line 1009
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-eqz v6, :cond_3

    move v6, v4

    :goto_3
    if-eqz v6, :cond_1d

    .line 1010
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v6, :cond_4

    move-object v6, v1

    :goto_4
    if-eqz v6, :cond_6

    .line 1011
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v6, :cond_5

    move-object v0, v1

    :goto_5
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v0, v2

    .line 1007
    :goto_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v0

    goto :goto_2

    :cond_3
    move v6, v3

    .line 1009
    goto :goto_3

    .line 1010
    :cond_4
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    goto :goto_4

    .line 1011
    :cond_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    goto :goto_5

    .line 1012
    :cond_6
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v6, :cond_7

    move-object v6, v1

    :goto_7
    if-eqz v6, :cond_1d

    .line 1013
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v6, :cond_8

    move-object v0, v1

    :goto_8
    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    xor-int/2addr v0, v2

    goto :goto_6

    .line 1012
    :cond_7
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    goto :goto_7

    .line 1013
    :cond_8
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    goto :goto_8

    .line 1018
    :cond_9
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 1019
    if-eqz v7, :cond_10

    .line 1020
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    move v5, v3

    :goto_9
    if-ge v5, v8, :cond_10

    .line 1021
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    .line 1022
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-eqz v6, :cond_a

    move v6, v4

    :goto_a
    if-eqz v6, :cond_1c

    .line 1023
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v6, :cond_b

    move-object v6, v1

    :goto_b
    if-eqz v6, :cond_d

    .line 1024
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v6, :cond_c

    move-object v0, v1

    :goto_c
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v0, v2

    .line 1020
    :goto_d
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v0

    goto :goto_9

    :cond_a
    move v6, v3

    .line 1022
    goto :goto_a

    .line 1023
    :cond_b
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    goto :goto_b

    .line 1024
    :cond_c
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    goto :goto_c

    .line 1025
    :cond_d
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v6, :cond_e

    move-object v6, v1

    :goto_e
    if-eqz v6, :cond_1c

    .line 1026
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v6, :cond_f

    move-object v0, v1

    :goto_f
    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    xor-int/2addr v0, v2

    goto :goto_d

    .line 1025
    :cond_e
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    goto :goto_e

    .line 1026
    :cond_f
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    goto :goto_f

    .line 1034
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eqz v0, :cond_11

    .line 1035
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->hashCode()I

    move-result v0

    xor-int/2addr v2, v0

    .line 1041
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    if-eqz v0, :cond_17

    .line 1042
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bb;->a:Ljava/lang/String;

    if-eqz v0, :cond_12

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_16

    :cond_12
    move v0, v4

    :goto_10
    if-nez v0, :cond_17

    .line 1043
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bb;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v2, v0

    .line 1053
    :cond_13
    :goto_11
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->t:Ljava/lang/String;

    if-eqz v0, :cond_14

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_19

    :cond_14
    move v0, v4

    :goto_12
    if-nez v0, :cond_1b

    .line 1054
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->u:Ljava/lang/String;

    if-eqz v0, :cond_15

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1a

    :cond_15
    move v0, v4

    :goto_13
    if-nez v0, :cond_1b

    .line 1055
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->t:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v0, v2

    .line 1056
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->u:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int v2, v0, v1

    move v0, v2

    .line 1061
    :goto_14
    if-eqz p2, :cond_0

    .line 1062
    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    goto/16 :goto_1

    :cond_16
    move v0, v3

    .line 1042
    goto :goto_10

    .line 1044
    :cond_17
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_18

    .line 1046
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v2, v0

    goto :goto_11

    .line 1047
    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    if-eqz v0, :cond_13

    .line 1048
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    const/16 v1, 0xd

    if-le v0, v1, :cond_13

    .line 1049
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/y;->hashCode()I

    move-result v0

    xor-int/2addr v2, v0

    goto :goto_11

    :cond_19
    move v0, v3

    .line 1053
    goto :goto_12

    :cond_1a
    move v0, v3

    .line 1054
    goto :goto_13

    :cond_1b
    move v0, v2

    goto :goto_14

    :cond_1c
    move v0, v2

    goto/16 :goto_d

    :cond_1d
    move v0, v2

    goto/16 :goto_6
.end method


# virtual methods
.method a(I)Lcom/google/android/apps/gmm/map/o/d;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->e:La/a/a/a/b/s;

    invoke-virtual {v0, p1}, La/a/a/a/b/s;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/d;

    .line 307
    if-eqz v0, :cond_0

    .line 308
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/o/d;->a(I)V

    .line 312
    :goto_0
    return-object v0

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->a:Lcom/google/android/apps/gmm/map/o/o;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/apps/gmm/map/o/o;->a(JI)Lcom/google/android/apps/gmm/map/o/d;

    move-result-object v0

    goto :goto_0
.end method

.method a()V
    .locals 2

    .prologue
    .line 277
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->e:La/a/a/a/b/s;

    invoke-virtual {v0}, La/a/a/a/b/s;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->e:La/a/a/a/b/s;

    invoke-virtual {v0}, La/a/a/a/b/s;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/d;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V

    goto :goto_0

    .line 280
    :cond_0
    return-void
.end method

.method a(Lcom/google/android/apps/gmm/map/internal/c/m;F)V
    .locals 2

    .prologue
    .line 793
    const/high16 v0, 0x41600000    # 14.0f

    cmpg-float v0, p2, v0

    if-gez v0, :cond_1

    .line 809
    :cond_0
    :goto_0
    return-void

    .line 797
    :cond_1
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/internal/c/an;

    if-eqz v0, :cond_0

    .line 801
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/an;

    .line 803
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->s:Lcom/google/android/apps/gmm/map/indoor/a/a;

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/c/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/indoor/c/c;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 804
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->m:Lcom/google/android/apps/gmm/map/indoor/d/g;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    .line 805
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/c/an;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 806
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->d:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 804
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->m:Lcom/google/android/apps/gmm/map/indoor/d/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/g;->a:Ljava/util/List;

    goto :goto_1
.end method

.method a(Lcom/google/android/apps/gmm/map/o/ag;Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 4
    .param p2    # Lcom/google/android/apps/gmm/map/internal/c/m;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 317
    if-nez p1, :cond_1

    .line 337
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    const/4 v0, 0x0

    instance-of v1, p2, Lcom/google/android/apps/gmm/map/internal/c/an;

    if-eqz v1, :cond_2

    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/an;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/internal/c/an;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/o/ak;)I

    move-result v0

    :cond_2
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/o/ag;->b:Lcom/google/android/apps/gmm/map/internal/c/aa;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/o/ag;->b:Lcom/google/android/apps/gmm/map/internal/c/aa;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/aa;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 321
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->f:La/a/a/a/b/s;

    invoke-virtual {v1, v0}, La/a/a/a/b/s;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 326
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/o/at;->a(I)Lcom/google/android/apps/gmm/map/o/d;

    move-result-object v1

    .line 327
    if-nez v1, :cond_6

    .line 328
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->b:Lcom/google/android/apps/gmm/map/o/z;

    invoke-virtual {p1, v0, v1, p2}, Lcom/google/android/apps/gmm/map/o/ag;->a(ILcom/google/android/apps/gmm/map/o/z;Lcom/google/android/apps/gmm/map/internal/c/m;)Lcom/google/android/apps/gmm/map/o/d;

    move-result-object v0

    .line 329
    if-eqz v0, :cond_4

    .line 330
    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/o/d;->a(I)V

    .line 333
    :cond_4
    :goto_1
    if-eqz v0, :cond_0

    .line 334
    invoke-virtual {p0, p3, v0}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/d;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 335
    :goto_2
    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V

    goto :goto_0

    .line 334
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->a:Lcom/google/android/apps/gmm/map/o/o;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/o/o;->a(Lcom/google/android/apps/gmm/map/o/d;)V

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method a(Lcom/google/android/apps/gmm/map/o/d;)V
    .locals 1

    .prologue
    .line 716
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/o/d;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 717
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->n:Lcom/google/android/apps/gmm/map/o/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/o/ab;->b(Lcom/google/android/apps/gmm/map/o/d;)V

    .line 720
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->l:Lcom/google/android/apps/gmm/map/o/av;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/o/av;->b(Lcom/google/android/apps/gmm/map/o/d;)V

    .line 724
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->a:Lcom/google/android/apps/gmm/map/o/o;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/o/o;->b(Lcom/google/android/apps/gmm/map/o/d;)V

    .line 725
    return-void
.end method

.method a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/d;)Z
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 632
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/o/d;->c()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 659
    :cond_0
    :goto_0
    return v3

    .line 637
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->y:Lcom/google/android/apps/gmm/map/o/au;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->i:Lcom/google/android/apps/gmm/map/o/ap;

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/map/o/at;->x:Z

    invoke-interface {p2, v0, v1, p1, v5}, Lcom/google/android/apps/gmm/map/o/d;->a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/o/ap;Lcom/google/android/apps/gmm/map/f/o;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 642
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/o/d;->g()Lcom/google/android/apps/gmm/map/o/b/b;

    move-result-object v5

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/at;->z:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    if-eqz v0, :cond_7

    invoke-interface {p2, v2}, Lcom/google/android/apps/gmm/map/o/d;->a(Z)V

    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/o/d;->h()Lcom/google/android/apps/gmm/map/o/b/b;

    move-result-object v0

    :goto_2
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/o/d;->e()Lcom/google/android/apps/gmm/map/o/af;

    move-result-object v6

    if-nez v0, :cond_9

    iget-boolean v0, v6, Lcom/google/android/apps/gmm/map/o/af;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->h:Lcom/google/android/apps/gmm/map/o/a;

    iget-boolean v1, v6, Lcom/google/android/apps/gmm/map/o/af;->b:Z

    invoke-virtual {v0, v5, v1}, Lcom/google/android/apps/gmm/map/o/a;->a(Lcom/google/android/apps/gmm/map/o/b/b;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_2
    move v0, v2

    :goto_3
    if-eqz v0, :cond_0

    .line 643
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->j:Lcom/google/android/apps/gmm/map/t/b;

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/map/o/d;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    .line 644
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->e:La/a/a/a/b/s;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/o/d;->f()I

    move-result v1

    invoke-virtual {v0, v1}, La/a/a/a/b/s;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/d;

    .line 645
    if-eqz v0, :cond_13

    .line 646
    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V

    .line 654
    :cond_3
    :goto_4
    invoke-interface {p2, v4}, Lcom/google/android/apps/gmm/map/o/d;->a(I)V

    .line 655
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->f:La/a/a/a/b/s;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/o/d;->f()I

    move-result v1

    invoke-virtual {v0, v1, p2}, La/a/a/a/b/s;->a(ILjava/lang/Object;)Ljava/lang/Object;

    move v3, v2

    .line 656
    goto :goto_0

    .line 642
    :cond_4
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/o/d;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    if-eqz v0, :cond_5

    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/o/d;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/an;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    if-nez v0, :cond_6

    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v3

    goto :goto_5

    :cond_6
    move v0, v3

    goto :goto_1

    :cond_7
    invoke-interface {p2, v3}, Lcom/google/android/apps/gmm/map/o/d;->a(Z)V

    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    move v0, v3

    goto :goto_3

    :cond_9
    iget-boolean v1, v6, Lcom/google/android/apps/gmm/map/o/af;->c:Z

    if-nez v1, :cond_d

    iget-boolean v1, v6, Lcom/google/android/apps/gmm/map/o/af;->b:Z

    if-eqz v1, :cond_b

    iget-boolean v1, v6, Lcom/google/android/apps/gmm/map/o/af;->d:Z

    if-nez v1, :cond_b

    move v1, v2

    :goto_6
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/at;->h:Lcom/google/android/apps/gmm/map/o/a;

    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/gmm/map/o/a;->a(Lcom/google/android/apps/gmm/map/o/b/b;Z)Z

    move-result v0

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/map/o/d;->a(Z)V

    if-nez v0, :cond_a

    iget-boolean v0, v6, Lcom/google/android/apps/gmm/map/o/af;->d:Z

    if-eqz v0, :cond_c

    :cond_a
    move v0, v2

    goto :goto_3

    :cond_b
    move v1, v3

    goto :goto_6

    :cond_c
    move v0, v3

    goto :goto_3

    :cond_d
    iget-boolean v1, v6, Lcom/google/android/apps/gmm/map/o/af;->b:Z

    if-eqz v1, :cond_f

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/o/at;->h:Lcom/google/android/apps/gmm/map/o/a;

    iget-boolean v1, v6, Lcom/google/android/apps/gmm/map/o/af;->d:Z

    if-nez v1, :cond_e

    move v1, v2

    :goto_7
    invoke-virtual {v7, v0, v1}, Lcom/google/android/apps/gmm/map/o/a;->a(Lcom/google/android/apps/gmm/map/o/b/b;Z)Z

    move-result v0

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/map/o/d;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->h:Lcom/google/android/apps/gmm/map/o/a;

    invoke-virtual {v0, v5, v2}, Lcom/google/android/apps/gmm/map/o/a;->a(Lcom/google/android/apps/gmm/map/o/b/b;Z)Z

    move v0, v2

    goto/16 :goto_3

    :cond_e
    move v1, v3

    goto :goto_7

    :cond_f
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->h:Lcom/google/android/apps/gmm/map/o/a;

    iget-boolean v6, v6, Lcom/google/android/apps/gmm/map/o/af;->d:Z

    iget-object v7, v1, Lcom/google/android/apps/gmm/map/o/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    iget-object v7, v1, Lcom/google/android/apps/gmm/map/o/a;->d:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    iget-object v7, v1, Lcom/google/android/apps/gmm/map/o/a;->b:Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/o/b/b;->a()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v7

    if-eqz v7, :cond_14

    iget-object v7, v1, Lcom/google/android/apps/gmm/map/o/a;->a:Lcom/google/android/apps/gmm/map/o/b;

    iget-object v8, v1, Lcom/google/android/apps/gmm/map/o/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v7, v5, v2, v8}, Lcom/google/android/apps/gmm/map/o/a;->a(Lcom/google/android/apps/gmm/map/o/b;Lcom/google/android/apps/gmm/map/o/b/b;ZLjava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_14

    iget-object v7, v1, Lcom/google/android/apps/gmm/map/o/a;->b:Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/o/b/b;->a()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v7

    if-eqz v7, :cond_10

    iget-object v7, v1, Lcom/google/android/apps/gmm/map/o/a;->a:Lcom/google/android/apps/gmm/map/o/b;

    iget-object v8, v1, Lcom/google/android/apps/gmm/map/o/a;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v7, v0, v2, v8}, Lcom/google/android/apps/gmm/map/o/a;->a(Lcom/google/android/apps/gmm/map/o/b;Lcom/google/android/apps/gmm/map/o/b/b;ZLjava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_10

    iget-object v6, v1, Lcom/google/android/apps/gmm/map/o/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v6, v5}, Lcom/google/android/apps/gmm/map/o/a;->a(Ljava/util/List;Lcom/google/android/apps/gmm/map/o/b/b;)V

    iget-object v5, v1, Lcom/google/android/apps/gmm/map/o/a;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v5, v0}, Lcom/google/android/apps/gmm/map/o/a;->a(Ljava/util/List;Lcom/google/android/apps/gmm/map/o/b/b;)V

    move v0, v4

    :goto_8
    iget-object v5, v1, Lcom/google/android/apps/gmm/map/o/a;->e:Lcom/google/android/apps/gmm/map/o/c;

    iget v6, v5, Lcom/google/android/apps/gmm/map/o/c;->c:I

    rsub-int/lit8 v7, v0, 0x2

    add-int/2addr v6, v7

    iput v6, v5, Lcom/google/android/apps/gmm/map/o/c;->c:I

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/o/a;->e:Lcom/google/android/apps/gmm/map/o/c;

    iget v5, v1, Lcom/google/android/apps/gmm/map/o/c;->a:I

    add-int/2addr v5, v0

    iput v5, v1, Lcom/google/android/apps/gmm/map/o/c;->a:I

    if-ne v0, v4, :cond_11

    move v1, v2

    :goto_9
    invoke-interface {p2, v1}, Lcom/google/android/apps/gmm/map/o/d;->a(Z)V

    if-lez v0, :cond_12

    move v0, v2

    goto/16 :goto_3

    :cond_10
    if-eqz v6, :cond_14

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/o/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v5}, Lcom/google/android/apps/gmm/map/o/a;->a(Ljava/util/List;Lcom/google/android/apps/gmm/map/o/b/b;)V

    move v0, v2

    goto :goto_8

    :cond_11
    move v1, v3

    goto :goto_9

    :cond_12
    move v0, v3

    goto/16 :goto_3

    .line 648
    :cond_13
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->l:Lcom/google/android/apps/gmm/map/o/av;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/map/o/av;->a(Lcom/google/android/apps/gmm/map/o/d;)V

    .line 649
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/o/d;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 650
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->n:Lcom/google/android/apps/gmm/map/o/ab;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/map/o/ab;->a(Lcom/google/android/apps/gmm/map/o/d;)V

    goto/16 :goto_4

    :cond_14
    move v0, v3

    goto :goto_8
.end method

.method a(Lcom/google/android/apps/gmm/map/o/a/a;Ljava/util/List;Lcom/google/android/apps/gmm/map/f/o;)Z
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/o/a/a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/al;",
            ">;",
            "Lcom/google/android/apps/gmm/map/f/o;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 556
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 557
    const/4 v12, 0x0

    .line 616
    :cond_0
    return v12

    .line 563
    :cond_1
    move-object/from16 v0, p3

    iget v1, v0, Lcom/google/android/apps/gmm/map/f/o;->e:I

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    move v12, v1

    .line 565
    :goto_0
    const/4 v1, 0x0

    move v13, v1

    :goto_1
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v13, v1, :cond_0

    .line 566
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/o/al;

    .line 567
    iget v2, v1, Lcom/google/android/apps/gmm/map/o/al;->d:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/o/at;->a(I)Lcom/google/android/apps/gmm/map/o/d;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;

    .line 569
    if-eqz v2, :cond_3

    .line 570
    iget v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->e:I

    iget v4, v1, Lcom/google/android/apps/gmm/map/o/al;->e:I

    if-eq v3, v4, :cond_2

    .line 571
    iget-object v3, v1, Lcom/google/android/apps/gmm/map/o/al;->c:Lcom/google/android/apps/gmm/map/internal/c/z;

    iget v4, v1, Lcom/google/android/apps/gmm/map/o/al;->e:I

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->a(Lcom/google/android/apps/gmm/map/internal/c/z;I)V

    .line 574
    :cond_2
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->c:Lcom/google/android/apps/gmm/map/o/b/g;

    iget-object v4, v1, Lcom/google/android/apps/gmm/map/o/al;->b:Lcom/google/android/apps/gmm/map/o/b/e;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/o/b/e;->c:Lcom/google/android/apps/gmm/map/o/b/g;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/o/b/g;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 575
    iget-object v3, v1, Lcom/google/android/apps/gmm/map/o/al;->b:Lcom/google/android/apps/gmm/map/o/b/e;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/o/b/e;->c:Lcom/google/android/apps/gmm/map/o/b/g;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v4}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    :try_start_0
    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->c:Lcom/google/android/apps/gmm/map/o/b/g;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 579
    :cond_3
    if-nez v2, :cond_8

    .line 580
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/o/at;->b:Lcom/google/android/apps/gmm/map/o/z;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/o/al;->a:Lcom/google/android/apps/gmm/map/o/b/c;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/o/al;->b:Lcom/google/android/apps/gmm/map/o/b/e;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/o/b/e;->c:Lcom/google/android/apps/gmm/map/o/b/g;

    iget-object v4, v1, Lcom/google/android/apps/gmm/map/o/al;->c:Lcom/google/android/apps/gmm/map/internal/c/z;

    iget v5, v1, Lcom/google/android/apps/gmm/map/o/al;->d:I

    iget v6, v1, Lcom/google/android/apps/gmm/map/o/al;->e:I

    iget-object v7, v1, Lcom/google/android/apps/gmm/map/o/al;->f:Lcom/google/android/apps/gmm/map/o/ak;

    iget-object v1, v8, Lcom/google/android/apps/gmm/map/o/z;->g:Lcom/google/android/apps/gmm/map/legacy/a/c/b/m;

    iget-object v9, v8, Lcom/google/android/apps/gmm/map/o/z;->d:Lcom/google/android/apps/gmm/map/o/h;

    iget-object v10, v8, Lcom/google/android/apps/gmm/map/o/z;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v11, v8, Lcom/google/android/apps/gmm/map/o/z;->a:Landroid/content/res/Resources;

    move-object/from16 v8, p3

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/m;->a(Lcom/google/android/apps/gmm/map/o/b/c;Lcom/google/android/apps/gmm/map/o/b/g;Lcom/google/android/apps/gmm/map/internal/c/z;IILcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Landroid/content/res/Resources;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;

    move-result-object v2

    .line 588
    if-eqz v2, :cond_4

    .line 589
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->a(I)V

    .line 592
    :cond_4
    :goto_2
    if-eqz v2, :cond_6

    .line 598
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->y:Lcom/google/android/apps/gmm/map/o/au;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/at;->i:Lcom/google/android/apps/gmm/map/o/ap;

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/map/o/at;->x:Z

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->a(Lcom/google/android/apps/gmm/map/o/ap;)Z

    move-result v3

    if-eqz v3, :cond_9

    move-object/from16 v0, p3

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_5

    .line 603
    if-eqz v12, :cond_c

    .line 604
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->t:Lcom/google/android/apps/gmm/map/o/a/c;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/o/a/c;->a(Lcom/google/android/apps/gmm/map/o/b/b;)Z

    move-result v4

    if-nez v4, :cond_a

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/o/a/c;->a:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    :goto_4
    if-eqz v1, :cond_5

    .line 605
    move-object/from16 v0, p3

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/d;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 613
    :cond_5
    :goto_5
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->b(I)V

    .line 565
    :cond_6
    add-int/lit8 v1, v13, 0x1

    move v13, v1

    goto/16 :goto_1

    .line 563
    :cond_7
    const/4 v1, 0x0

    move v12, v1

    goto/16 :goto_0

    .line 575
    :catchall_0
    move-exception v1

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    throw v1

    .line 591
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/at;->f:La/a/a/a/b/s;

    iget v1, v1, Lcom/google/android/apps/gmm/map/o/al;->d:I

    invoke-virtual {v3, v1}, La/a/a/a/b/s;->b(I)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_2

    .line 598
    :cond_9
    const/4 v1, 0x0

    goto :goto_3

    .line 604
    :cond_a
    const/4 v1, 0x0

    goto :goto_4

    .line 605
    :cond_b
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->a:Lcom/google/android/apps/gmm/map/o/o;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/o/o;->a(Lcom/google/android/apps/gmm/map/o/d;)V

    goto :goto_5

    .line 607
    :cond_c
    move-object/from16 v0, p3

    invoke-interface {p1, v2, v0}, Lcom/google/android/apps/gmm/map/o/a/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;Lcom/google/android/apps/gmm/map/f/o;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 608
    move-object/from16 v0, p3

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/d;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->a:Lcom/google/android/apps/gmm/map/o/o;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/o/o;->a(Lcom/google/android/apps/gmm/map/o/d;)V

    goto :goto_5
.end method

.method a(Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/map/f/o;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/al;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ">;",
            "Lcom/google/android/apps/gmm/map/f/o;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 523
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 551
    :cond_0
    :goto_0
    return v2

    .line 527
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    move v1, v2

    .line 531
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 532
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/al;

    .line 533
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/at;->B:Ljava/util/List;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/al;->a:Lcom/google/android/apps/gmm/map/o/b/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/b/c;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 531
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 536
    :cond_2
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 537
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->B:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 538
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->B:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 544
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->A:Lcom/google/android/apps/gmm/map/o/a/e;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->A:Lcom/google/android/apps/gmm/map/o/a/e;

    .line 545
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/o/a/e;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/at;->B:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->A:Lcom/google/android/apps/gmm/map/o/a/e;

    .line 546
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/o/a/e;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 547
    :cond_4
    new-instance v1, Lcom/google/android/apps/gmm/map/o/a/e;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/at;->t:Lcom/google/android/apps/gmm/map/o/a/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/at;->B:Ljava/util/List;

    .line 548
    invoke-static {v3}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/o/a/e;-><init>(Lcom/google/android/apps/gmm/map/o/a/c;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/ab;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/o/at;->A:Lcom/google/android/apps/gmm/map/o/a/e;

    .line 551
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/at;->A:Lcom/google/android/apps/gmm/map/o/a/e;

    invoke-virtual {p0, v0, p1, p3}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/o/a/a;Ljava/util/List;Lcom/google/android/apps/gmm/map/f/o;)Z

    move-result v2

    goto :goto_0
.end method
