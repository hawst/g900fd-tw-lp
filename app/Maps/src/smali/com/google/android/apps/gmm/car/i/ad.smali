.class Lcom/google/android/apps/gmm/car/i/ad;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/ac;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/i/ab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/i/ab;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/i/ad;->a:Lcom/google/android/apps/gmm/car/i/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/bm;)V
    .locals 5

    .prologue
    .line 292
    sget-object v0, Lcom/google/android/apps/gmm/car/i/ab;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "fetchCompleteCallback: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ad;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/bm;

    .line 297
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bm;->a()Lcom/google/android/apps/gmm/car/bn;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/car/bn;->b:Lcom/google/android/apps/gmm/car/bn;

    if-eq v2, v3, :cond_0

    .line 298
    sget-object v1, Lcom/google/android/apps/gmm/car/i/ab;->a:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/bm;->b:Ljava/lang/String;

    .line 299
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bm;->a()Lcom/google/android/apps/gmm/car/bn;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Got directions, but \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    :goto_0
    return-void

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ad;->a:Lcom/google/android/apps/gmm/car/i/ab;

    invoke-static {v0}, Lcom/google/android/apps/gmm/car/i/ab;->a(Lcom/google/android/apps/gmm/car/i/ab;)V

    goto :goto_0
.end method
