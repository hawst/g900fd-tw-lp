.class Lcom/google/android/apps/gmm/base/activities/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 359
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/activities/f;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/base/e/c;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 391
    iget v0, p1, Lcom/google/android/apps/gmm/base/e/c;->b:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 392
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/f;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/f;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 393
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    .line 392
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/login/a;->a(Landroid/app/Activity;Lcom/google/android/apps/gmm/login/a/a;)V

    .line 397
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/f;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/j/a;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/j/a;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 398
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/base/fragments/k;)V
    .locals 5
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 369
    .line 370
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/fragments/k;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "state"

    .line 371
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/fragments/k;->b:Lcom/google/android/apps/gmm/base/fragments/l;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 372
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    .line 369
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 407
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/f;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V

    .line 408
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/z;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 380
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/z;->a:Lcom/google/android/apps/gmm/v/aa;

    if-nez v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/f;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->b:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 382
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Fragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/f;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 386
    :cond_0
    return-void
.end method

.method public a(Lcom/google/b/d/b;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 363
    iget-object v0, p1, Lcom/google/b/d/b;->a:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "GmmActivity.logDeadEvent(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    return-void
.end method
