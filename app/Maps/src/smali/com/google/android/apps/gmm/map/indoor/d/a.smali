.class public Lcom/google/android/apps/gmm/map/indoor/d/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/b/a/l;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/e;",
            ">;"
        }
    .end annotation
.end field

.field public final c:I

.field public final d:Lcom/google/android/apps/gmm/map/b/a/y;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public e:Z


# direct methods
.method private constructor <init>(Lcom/google/android/apps/gmm/map/b/a/l;Ljava/util/List;IZLcom/google/android/apps/gmm/map/b/a/y;JLcom/google/android/apps/gmm/map/indoor/d/b;)V
    .locals 3
    .param p5    # Lcom/google/android/apps/gmm/map/b/a/y;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/l;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/e;",
            ">;IZ",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "J",
            "Lcom/google/android/apps/gmm/map/indoor/d/b;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    .line 88
    invoke-static {p2}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    .line 89
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->e:Z

    .line 90
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 91
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->e:Z

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/indoor/d/e;->a(Lcom/google/android/apps/gmm/map/b/a/l;)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 103
    iput v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->c:I

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_0
    iput p3, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->c:I

    goto :goto_0
.end method

.method public static a(Lcom/google/e/a/a/a/b;J)Lcom/google/android/apps/gmm/map/indoor/d/a;
    .locals 13

    .prologue
    const/4 v11, 0x5

    const/4 v4, 0x2

    const/16 v10, 0x1a

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 117
    const/16 v0, 0x1c

    invoke-virtual {p0, v7, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/l;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/l;

    move-result-object v1

    .line 119
    if-nez v1, :cond_1

    .line 120
    const-string v1, "INDOOR"

    const-string v2, "malformed building id: "

    const/16 v0, 0x1c

    invoke-virtual {p0, v7, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    const/4 v0, 0x0

    .line 154
    :goto_1
    return-object v0

    .line 120
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v5

    .line 125
    if-ltz v5, :cond_2

    move v0, v7

    :goto_2
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v0, v6

    goto :goto_2

    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    move v3, v6

    .line 126
    :goto_3
    if-ge v3, v5, :cond_5

    .line 127
    invoke-virtual {p0, v4, v3, v10}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 128
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/indoor/d/e;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v0

    .line 129
    if-eqz v0, :cond_4

    .line 130
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 134
    :cond_5
    const/4 v0, 0x4

    const/16 v3, 0x18

    invoke-virtual {p0, v0, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 136
    const/4 v0, 0x3

    const/16 v3, 0x15

    invoke-virtual {p0, v0, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    long-to-int v3, v8

    .line 137
    if-ltz v3, :cond_6

    if-lt v3, v5, :cond_7

    :cond_6
    move v3, v6

    .line 142
    :cond_7
    if-nez v5, :cond_8

    .line 143
    const/4 v3, -0x1

    .line 146
    :cond_8
    const/4 v5, 0x0

    .line 147
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v11}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_b

    move v0, v7

    :goto_4
    if-nez v0, :cond_9

    invoke-virtual {p0, v11}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_c

    :cond_9
    move v0, v7

    :goto_5
    if-eqz v0, :cond_a

    .line 148
    invoke-virtual {p0, v11, v10}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    .line 151
    :cond_a
    const/4 v0, 0x7

    .line 152
    invoke-virtual {p0, v0, v10}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/indoor/d/b;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/indoor/d/b;

    move-result-object v8

    .line 154
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/d/a;

    move-wide v6, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/indoor/d/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/l;Ljava/util/List;IZLcom/google/android/apps/gmm/map/b/a/y;JLcom/google/android/apps/gmm/map/indoor/d/b;)V

    goto/16 :goto_1

    :cond_b
    move v0, v6

    .line 147
    goto :goto_4

    :cond_c
    move v0, v6

    goto :goto_5
.end method

.method public static a(Lcom/google/maps/b/a/ab;J)Lcom/google/android/apps/gmm/map/indoor/d/a;
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 167
    invoke-virtual {p0}, Lcom/google/maps/b/a/ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/l;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/l;

    move-result-object v1

    .line 169
    if-nez v1, :cond_1

    .line 170
    const-string v1, "INDOOR"

    const-string v2, "malformed building id: "

    invoke-virtual {p0}, Lcom/google/maps/b/a/ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 204
    :goto_1
    return-object v5

    .line 170
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/google/maps/b/a/ab;->a:Lcom/google/maps/b/a/cs;

    iget v6, v0, Lcom/google/maps/b/a/cs;->b:I

    .line 175
    if-ltz v6, :cond_2

    move v0, v4

    :goto_2
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v0, v3

    goto :goto_2

    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v3

    .line 176
    :goto_3
    if-ge v0, v6, :cond_5

    .line 177
    invoke-virtual {p0, v0}, Lcom/google/maps/b/a/ab;->b(I)Lcom/google/maps/b/a/ae;

    move-result-object v7

    .line 178
    invoke-static {v7}, Lcom/google/android/apps/gmm/map/indoor/d/e;->a(Lcom/google/maps/b/a/ae;)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v7

    .line 179
    if-eqz v7, :cond_4

    .line 180
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 184
    :cond_5
    iget-object v0, p0, Lcom/google/maps/b/a/ab;->c:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v0, :cond_9

    .line 186
    :goto_4
    iget-object v0, p0, Lcom/google/maps/b/a/ab;->b:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 187
    if-ltz v0, :cond_6

    if-lt v0, v6, :cond_a

    .line 192
    :cond_6
    :goto_5
    if-nez v6, :cond_7

    .line 193
    const/4 v3, -0x1

    .line 197
    :cond_7
    iget-object v0, p0, Lcom/google/maps/b/a/ab;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v0, :cond_8

    .line 198
    invoke-virtual {p0}, Lcom/google/maps/b/a/ab;->b()Lcom/google/maps/b/a/bm;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/maps/b/a/bm;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    .line 202
    :cond_8
    invoke-virtual {p0}, Lcom/google/maps/b/a/ab;->d()Lcom/google/maps/b/a/aa;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/indoor/d/b;->a(Lcom/google/maps/b/a/aa;)Lcom/google/android/apps/gmm/map/indoor/d/b;

    move-result-object v8

    .line 204
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/d/a;

    move-wide v6, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/indoor/d/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/l;Ljava/util/List;IZLcom/google/android/apps/gmm/map/b/a/y;JLcom/google/android/apps/gmm/map/indoor/d/b;)V

    move-object v5, v0

    goto :goto_1

    :cond_9
    move v4, v3

    .line 184
    goto :goto_4

    :cond_a
    move v3, v0

    goto :goto_5
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/indoor/d/e;)I
    .locals 2

    .prologue
    .line 277
    if-nez p1, :cond_0

    .line 278
    const/4 v0, -0x1

    .line 284
    :goto_0
    return v0

    .line 279
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/indoor/d/e;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    if-ne p1, v0, :cond_1

    .line 280
    const v0, 0x7fffffff

    goto :goto_0

    .line 283
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/indoor/d/a;->a(Lcom/google/android/apps/gmm/map/b/a/l;)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v0

    .line 284
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/map/indoor/d/e;
    .locals 1

    .prologue
    .line 308
    const v0, 0x7fffffff

    if-ne p1, v0, :cond_0

    .line 309
    sget-object v0, Lcom/google/android/apps/gmm/map/indoor/d/e;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    .line 313
    :goto_0
    return-object v0

    .line 310
    :cond_0
    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 311
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 313
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/e;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/l;)Lcom/google/android/apps/gmm/map/indoor/d/e;
    .locals 4

    .prologue
    .line 252
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/e;

    .line 254
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {p1, v3}, Lcom/google/android/apps/gmm/map/b/a/l;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 258
    :goto_1
    return-object v0

    .line 252
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 258
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 365
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/indoor/d/a;

    if-nez v2, :cond_1

    .line 379
    :cond_0
    :goto_0
    return v0

    .line 369
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 370
    goto :goto_0

    .line 373
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/indoor/d/a;

    .line 375
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/indoor/d/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_4

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    .line 379
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/l;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/indoor/d/a;->c:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->e:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/map/indoor/d/a;->e:Z

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_4
    move v2, v0

    .line 375
    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 386
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "[Building: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
