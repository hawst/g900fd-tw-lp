.class public Lcom/google/android/apps/gmm/shared/net/a/e;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# static fields
.field static final a:J


# instance fields
.field final b:Lcom/google/android/apps/gmm/shared/net/ad;

.field final c:Ljava/lang/String;

.field final d:Lcom/google/android/apps/gmm/shared/net/a/b;

.field private final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 41
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/shared/net/a/e;->a:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/ad;Ljava/lang/String;ZLcom/google/android/apps/gmm/shared/net/a/b;)V
    .locals 2

    .prologue
    .line 54
    sget-object v0, Lcom/google/r/b/a/el;->aw:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/h;->c:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 55
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->b:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 56
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->c:Ljava/lang/String;

    .line 57
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->e:Z

    .line 58
    iput-object p4, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 59
    return-void
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 129
    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    monitor-enter v2

    .line 130
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    const/4 v3, 0x0

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/shared/net/a/b;->e:Z

    .line 134
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/shared/net/a/b;->d:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->b:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/ad;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/shared/net/a/f;

    invoke-direct {v4, p0, v0}, Lcom/google/android/apps/gmm/shared/net/a/f;-><init>(Lcom/google/android/apps/gmm/shared/net/a/e;Z)V

    sget-object v5, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/b;->d:Z

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x0

    :goto_0
    invoke-interface {v3, v4, v5, v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 145
    monitor-exit v2

    return-void

    .line 135
    :cond_1
    sget-wide v0, Lcom/google/android/apps/gmm/shared/net/a/e;->a:J

    goto :goto_0

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 7

    .prologue
    .line 85
    iget-object v6, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    monitor-enter v6

    .line 86
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 87
    invoke-static {}, Lcom/google/r/b/a/dr;->g()Lcom/google/r/b/a/dr;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    if-eqz v1, :cond_1

    :goto_0
    check-cast v1, Lcom/google/r/b/a/dr;

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->b:Lcom/google/android/apps/gmm/shared/net/ad;

    const/4 v3, 0x0

    const/4 v4, 0x1

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->e:Z

    .line 86
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/shared/net/a/b;->a(Lcom/google/r/b/a/dr;Lcom/google/android/apps/gmm/shared/net/ad;ZZZ)Z

    move-result v0

    .line 93
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->b:Lcom/google/android/apps/gmm/shared/net/ad;

    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->c:Ljava/lang/String;

    const/4 v4, 0x1

    iput-boolean v4, v1, Lcom/google/android/apps/gmm/shared/net/a/b;->f:Z

    iget-object v4, v1, Lcom/google/android/apps/gmm/shared/net/a/b;->i:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    iput-wide v4, v1, Lcom/google/android/apps/gmm/shared/net/a/b;->h:J

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    if-eqz v0, :cond_0

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/net/ad;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->H()Lcom/google/r/b/a/dr;

    move-result-object v2

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/h;->a(Landroid/content/Context;Lcom/google/n/at;Ljava/lang/String;)Z

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/m/g;

    iget-object v2, v1, Lcom/google/android/apps/gmm/shared/net/a/b;->j:Lcom/google/android/apps/gmm/m/d;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/m/g;-><init>(Lcom/google/android/apps/gmm/m/d;)V

    const-string v2, "client_parameters_timestamp"

    iget-wide v4, v1, Lcom/google/android/apps/gmm/shared/net/a/b;->h:J

    invoke-virtual {v0, v2, v4, v5}, Lcom/google/android/apps/gmm/m/g;->a(Ljava/lang/String;J)V

    .line 94
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    invoke-direct {p0}, Lcom/google/android/apps/gmm/shared/net/a/e;->g()V

    .line 97
    const/4 v0, 0x0

    return-object v0

    :cond_1
    move-object v1, v2

    .line 87
    goto :goto_0

    .line 94
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x0

    const/4 v2, 0x1

    .line 105
    invoke-static {}, Lcom/google/r/b/a/dn;->newBuilder()Lcom/google/r/b/a/dp;

    move-result-object v4

    .line 108
    iget-object v5, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    monitor-enter v5

    .line 110
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->H()Lcom/google/r/b/a/dr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/dr;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/rd;

    .line 112
    invoke-static {}, Lcom/google/r/b/a/rd;->newBuilder()Lcom/google/r/b/a/rf;

    move-result-object v7

    .line 113
    iget v1, v0, Lcom/google/r/b/a/rd;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    move v1, v2

    :goto_1
    if-eqz v1, :cond_3

    .line 114
    iget v1, v0, Lcom/google/r/b/a/rd;->c:I

    invoke-static {v1}, Lcom/google/r/b/a/rg;->a(I)Lcom/google/r/b/a/rg;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/r/b/a/rg;->a:Lcom/google/r/b/a/rg;

    :cond_0
    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v1, v3

    .line 113
    goto :goto_1

    .line 114
    :cond_2
    :try_start_1
    iget v8, v7, Lcom/google/r/b/a/rf;->a:I

    or-int/lit8 v8, v8, 0x1

    iput v8, v7, Lcom/google/r/b/a/rf;->a:I

    iget v1, v1, Lcom/google/r/b/a/rg;->Q:I

    iput v1, v7, Lcom/google/r/b/a/rf;->c:I

    .line 116
    :cond_3
    iget v1, v0, Lcom/google/r/b/a/rd;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v8, 0x2

    if-ne v1, v8, :cond_5

    move v1, v2

    :goto_2
    if-eqz v1, :cond_4

    .line 117
    iget-wide v0, v0, Lcom/google/r/b/a/rd;->d:J

    iget v8, v7, Lcom/google/r/b/a/rf;->a:I

    or-int/lit8 v8, v8, 0x2

    iput v8, v7, Lcom/google/r/b/a/rf;->a:I

    iput-wide v0, v7, Lcom/google/r/b/a/rf;->d:J

    .line 119
    :cond_4
    invoke-virtual {v4}, Lcom/google/r/b/a/dp;->c()V

    iget-object v0, v4, Lcom/google/r/b/a/dp;->b:Ljava/util/List;

    invoke-virtual {v7}, Lcom/google/r/b/a/rf;->g()Lcom/google/n/t;

    move-result-object v1

    new-instance v7, Lcom/google/n/ao;

    invoke-direct {v7}, Lcom/google/n/ao;-><init>()V

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v7, Lcom/google/n/ao;->d:Z

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    move v1, v3

    .line 116
    goto :goto_2

    .line 121
    :cond_6
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 122
    invoke-static {}, Lcom/google/r/b/a/dj;->newBuilder()Lcom/google/r/b/a/dl;

    move-result-object v1

    iget v0, v1, Lcom/google/r/b/a/dl;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/r/b/a/dl;->a:I

    iput-boolean v2, v1, Lcom/google/r/b/a/dl;->b:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/b;->c:Z

    iget v3, v1, Lcom/google/r/b/a/dl;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v1, Lcom/google/r/b/a/dl;->a:I

    iput-boolean v0, v1, Lcom/google/r/b/a/dl;->c:Z

    iget v0, v1, Lcom/google/r/b/a/dl;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v1, Lcom/google/r/b/a/dl;->a:I

    iput-boolean v2, v1, Lcom/google/r/b/a/dl;->d:Z

    invoke-static {}, Lcom/google/maps/b/ag;->newBuilder()Lcom/google/maps/b/ai;

    move-result-object v0

    const-string v3, "GMM"

    if-nez v3, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget v5, v0, Lcom/google/maps/b/ai;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, v0, Lcom/google/maps/b/ai;->a:I

    iput-object v3, v0, Lcom/google/maps/b/ai;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/maps/b/ai;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/ag;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget-object v3, v1, Lcom/google/r/b/a/dl;->e:Lcom/google/n/ao;

    iget-object v5, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v3, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/r/b/a/dl;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v1, Lcom/google/r/b/a/dl;->a:I

    invoke-virtual {v1}, Lcom/google/r/b/a/dl;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/dj;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    iget-object v1, v4, Lcom/google/r/b/a/dp;->c:Lcom/google/n/ao;

    iget-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v1, Lcom/google/n/ao;->d:Z

    iget v0, v4, Lcom/google/r/b/a/dp;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v4, Lcom/google/r/b/a/dp;->a:I

    .line 124
    invoke-virtual {v4}, Lcom/google/r/b/a/dp;->g()Lcom/google/n/t;

    move-result-object v0

    sget-object v1, Lcom/google/r/b/a/b/h;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    return-object v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 69
    if-eqz p1, :cond_0

    .line 71
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    monitor-enter v1

    .line 72
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/a/e;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 73
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/gmm/shared/net/a/e;->g()V

    .line 76
    :cond_0
    return-void

    .line 73
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
