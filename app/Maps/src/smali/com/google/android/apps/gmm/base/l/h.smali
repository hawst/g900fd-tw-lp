.class public Lcom/google/android/apps/gmm/base/l/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;
.implements Lcom/google/android/apps/gmm/base/l/a/i;


# instance fields
.field public a:Ljava/lang/Runnable;

.field public b:Ljava/util/Date;

.field public c:Ljava/util/Date;

.field public d:Ljava/util/Date;

.field private final e:Landroid/content/Context;

.field private final f:Ljava/util/Calendar;

.field private final g:Lcom/google/android/libraries/curvular/bi;

.field private final h:Lcom/google/b/f/t;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Calendar;Ljava/util/Date;Lcom/google/android/libraries/curvular/bi;Ljava/lang/Boolean;Lcom/google/b/f/t;)V
    .locals 0
    .param p4    # Lcom/google/android/libraries/curvular/bi;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/h;->e:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/l/h;->f:Ljava/util/Calendar;

    .line 55
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/l/h;->b:Ljava/util/Date;

    .line 56
    iput-object p4, p0, Lcom/google/android/apps/gmm/base/l/h;->g:Lcom/google/android/libraries/curvular/bi;

    .line 57
    iput-object p6, p0, Lcom/google/android/apps/gmm/base/l/h;->h:Lcom/google/b/f/t;

    .line 59
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/h;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/h;->b:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const/16 v1, 0x12

    invoke-static {v0, v2, v3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/bi;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/h;->g:Lcom/google/android/libraries/curvular/bi;

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/h;->f:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/h;->b:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 107
    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/h;->e:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/h;->f:Ljava/util/Calendar;

    .line 110
    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/h;->f:Ljava/util/Calendar;

    const/4 v4, 0x2

    .line 111
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/h;->f:Ljava/util/Calendar;

    const/4 v5, 0x5

    .line 112
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 113
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/DatePicker;->setSpinnersShown(Z)V

    .line 115
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/h;->c:Ljava/util/Date;

    if-eqz v1, :cond_0

    .line 116
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/h;->c:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/DatePicker;->setMinDate(J)V

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/h;->d:Ljava/util/Date;

    if-eqz v1, :cond_1

    .line 120
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/h;->d:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/DatePicker;->setMaxDate(J)V

    .line 123
    :cond_1
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->show()V

    .line 125
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/h;->h:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/h;->f:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/h;->f:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/h;->f:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p4}, Ljava/util/Calendar;->set(II)V

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/h;->f:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/h;->b:Ljava/util/Date;

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/h;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/h;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 139
    :cond_0
    return-void
.end method
