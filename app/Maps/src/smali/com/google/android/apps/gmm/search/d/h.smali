.class public final enum Lcom/google/android/apps/gmm/search/d/h;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/search/d/h;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/search/d/h;

.field public static final enum b:Lcom/google/android/apps/gmm/search/d/h;

.field public static final enum c:Lcom/google/android/apps/gmm/search/d/h;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/search/d/h;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/search/d/h;

    const-string v1, "ORGANIC"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/gmm/search/d/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/search/d/h;->a:Lcom/google/android/apps/gmm/search/d/h;

    .line 35
    new-instance v0, Lcom/google/android/apps/gmm/search/d/h;

    const-string v1, "AD"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/apps/gmm/search/d/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/search/d/h;->b:Lcom/google/android/apps/gmm/search/d/h;

    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/search/d/h;

    const-string v1, "AD_WITHOUT_LOCATION"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/android/apps/gmm/search/d/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/search/d/h;->c:Lcom/google/android/apps/gmm/search/d/h;

    .line 33
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/search/d/h;

    sget-object v1, Lcom/google/android/apps/gmm/search/d/h;->a:Lcom/google/android/apps/gmm/search/d/h;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/search/d/h;->b:Lcom/google/android/apps/gmm/search/d/h;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/search/d/h;->c:Lcom/google/android/apps/gmm/search/d/h;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/search/d/h;->e:[Lcom/google/android/apps/gmm/search/d/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/google/android/apps/gmm/search/d/h;->d:I

    .line 42
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/search/d/h;
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/gmm/search/d/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/d/h;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/search/d/h;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/apps/gmm/search/d/h;->e:[Lcom/google/android/apps/gmm/search/d/h;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/search/d/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/search/d/h;

    return-object v0
.end method
