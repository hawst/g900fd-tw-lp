.class Lcom/google/android/apps/gmm/photo/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/support/v4/view/bz;


# instance fields
.field a:Lcom/google/android/apps/gmm/photo/e;

.field final synthetic b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;)V
    .locals 0

    .prologue
    .line 596
    iput-object p1, p0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 602
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 650
    :goto_0
    return-void

    .line 605
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iput p1, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->q:I

    .line 606
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->b:Lcom/google/android/apps/gmm/base/views/HeaderView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/HeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 607
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->d:Lcom/google/android/apps/gmm/util/h;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/h;->a:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/h;->a:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/h;->a:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 610
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v2

    .line 611
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->n:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    .line 612
    iget v0, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->g:I

    if-ge v2, v0, :cond_1

    add-int/lit8 v0, v2, -0x2

    if-lt p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/w;->a:Lcom/google/android/apps/gmm/photo/e;

    if-nez v0, :cond_1

    .line 615
    new-instance v0, Lcom/google/android/apps/gmm/photo/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->n:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    const/16 v3, 0x14

    iget-object v4, p0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    .line 616
    iget v4, v4, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->o:I

    iget-object v5, p0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget v5, v5, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->p:I

    new-instance v6, Lcom/google/android/apps/gmm/photo/x;

    invoke-direct {v6, p0}, Lcom/google/android/apps/gmm/photo/x;-><init>(Lcom/google/android/apps/gmm/photo/w;)V

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/photo/e;-><init>(Lcom/google/android/apps/gmm/map/b/a/j;IIIILcom/google/android/apps/gmm/photo/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/w;->a:Lcom/google/android/apps/gmm/photo/e;

    .line 647
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/w;->a:Lcom/google/android/apps/gmm/photo/e;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 649
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->K_()V

    goto :goto_0
.end method

.method public final a(IF)V
    .locals 0

    .prologue
    .line 655
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 660
    return-void
.end method
