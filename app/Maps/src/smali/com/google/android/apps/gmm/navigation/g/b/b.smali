.class public Lcom/google/android/apps/gmm/navigation/g/b/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/r/b/a;

.field public final b:Z

.field public final c:Z


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/navigation/g/b/c;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/c;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/b;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 22
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/c;->b:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/b;->b:Z

    .line 23
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/c;->c:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/b;->c:Z

    .line 24
    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/b/a/ah;
    .locals 3

    .prologue
    .line 99
    .line 100
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "myLocation"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/b;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 101
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "dataConnectionReady"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/b;->b:Z

    .line 102
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "gpsReady"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/b;->c:Z

    .line 103
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 95
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "myLocation"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/b;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "dataConnectionReady"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/b;->b:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "gpsReady"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/b;->c:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
