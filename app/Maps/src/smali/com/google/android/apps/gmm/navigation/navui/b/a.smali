.class public Lcom/google/android/apps/gmm/navigation/navui/b/a;
.super Lcom/google/android/apps/gmm/navigation/commonui/b/a;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final e:Ljava/lang/String;


# instance fields
.field public final f:Lcom/google/android/apps/gmm/navigation/g/b/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final g:Z

.field public final h:Lcom/google/android/apps/gmm/map/r/a/ag;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final i:Z

.field public final j:Z

.field public final k:Lcom/google/android/apps/gmm/base/g/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final l:Lcom/google/android/apps/gmm/navigation/navui/b/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->e:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/b/b;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/navigation/commonui/b/a;-><init>(Lcom/google/android/apps/gmm/navigation/commonui/b/b;)V

    .line 55
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 56
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/b;->f:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->g:Z

    .line 57
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/b;->i:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->j:Z

    .line 58
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 59
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/b;->h:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->i:Z

    .line 60
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/b;->j:Lcom/google/android/apps/gmm/base/g/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->k:Lcom/google/android/apps/gmm/base/g/c;

    .line 61
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/b;->k:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->l:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    .line 62
    return-void
.end method


# virtual methods
.method public final a(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->l:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/navui/b/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->l:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/navui/b/c;->b:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    if-ne v1, v2, :cond_0

    if-nez p1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v1, :cond_2

    .line 127
    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-boolean v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/f;->e:Z

    if-nez v2, :cond_1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iget v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    const/16 v2, 0x2ee0

    if-le v1, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/b/a;->a()Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "navState"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 137
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "useNightMode"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->g:Z

    .line 138
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "headerStep"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 139
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "showDestinationInfo"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->j:Z

    .line 140
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "arrivedAtPlacemark"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->k:Lcom/google/android/apps/gmm/base/g/c;

    .line 141
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "powerSavingsMode"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->l:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    .line 142
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 143
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
