.class public Lcom/google/android/apps/gmm/car/l/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/car/ad;

.field c:Lcom/google/android/apps/gmm/map/g/e;

.field d:Landroid/view/View;

.field e:Lcom/google/android/apps/gmm/car/l/i;

.field f:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final g:Lcom/google/android/apps/gmm/car/ax;

.field private final h:Lcom/google/android/apps/gmm/z/b/o;

.field private i:Lcom/google/android/apps/gmm/car/d/m;

.field private j:Lcom/google/android/apps/gmm/shared/net/b;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<",
            "Lcom/google/r/b/a/aog;",
            "Lcom/google/r/b/a/aok;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/google/android/apps/gmm/car/l/k;

.field private final l:Lcom/google/android/apps/gmm/shared/net/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/c",
            "<",
            "Lcom/google/r/b/a/aok;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcom/google/android/apps/gmm/car/d/p;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/apps/gmm/car/l/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/l/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/g/e;Lcom/google/android/apps/gmm/car/ad;)V
    .locals 5

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/z/b/j;

    sget-object v1, Lcom/google/b/f/t;->ak:Lcom/google/b/f/t;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/b/j;-><init>(Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->h:Lcom/google/android/apps/gmm/z/b/o;

    .line 184
    new-instance v0, Lcom/google/android/apps/gmm/car/l/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/l/e;-><init>(Lcom/google/android/apps/gmm/car/l/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->k:Lcom/google/android/apps/gmm/car/l/k;

    .line 197
    new-instance v0, Lcom/google/android/apps/gmm/car/l/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/l/f;-><init>(Lcom/google/android/apps/gmm/car/l/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->l:Lcom/google/android/apps/gmm/shared/net/c;

    .line 223
    new-instance v0, Lcom/google/android/apps/gmm/car/l/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/l/g;-><init>(Lcom/google/android/apps/gmm/car/l/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->m:Lcom/google/android/apps/gmm/car/d/p;

    .line 61
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/map/g/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->c:Lcom/google/android/apps/gmm/map/g/e;

    .line 62
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/car/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 64
    sget-object v0, Lcom/google/android/apps/gmm/car/l/c;->a:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/g/e;->a:Lcom/google/android/apps/gmm/map/internal/c/cf;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    new-instance v0, Lcom/google/android/apps/gmm/car/ax;

    .line 69
    iget-object v1, p2, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    .line 70
    iget-object v2, p2, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    .line 71
    iget-object v3, p2, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    sget-object v4, Lcom/google/android/apps/gmm/car/n/b;->e:Lcom/google/android/libraries/curvular/b;

    iget-object v3, v3, Lcom/google/android/apps/gmm/car/v;->a:Landroid/content/Context;

    invoke-virtual {v4, v3}, Lcom/google/android/libraries/curvular/b;->c_(Landroid/content/Context;)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/car/ax;-><init>(Landroid/graphics/Point;Lcom/google/android/apps/gmm/car/v;I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->g:Lcom/google/android/apps/gmm/car/ax;

    .line 72
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/car/l/c;)V
    .locals 0

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/l/c;->e()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/car/l/b;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->d:Landroid/view/View;

    .line 81
    new-instance v0, Lcom/google/android/apps/gmm/car/l/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/l/c;->k:Lcom/google/android/apps/gmm/car/l/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/l/c;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 82
    iget-boolean v2, v2, Lcom/google/android/apps/gmm/car/ad;->o:Z

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/l/c;->c:Lcom/google/android/apps/gmm/map/g/e;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/g/d;->i:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/car/l/i;-><init>(Lcom/google/android/apps/gmm/car/l/k;ZLjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->e:Lcom/google/android/apps/gmm/car/l/i;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/l/c;->e:Lcom/google/android/apps/gmm/car/l/i;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/l/c;->f()V

    .line 87
    new-instance v0, Lcom/google/android/apps/gmm/car/d/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/l/c;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 88
    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/car/f/ak;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/l/c;->b:Lcom/google/android/apps/gmm/car/ad;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/car/f/ak;-><init>(Lcom/google/android/apps/gmm/car/ad;)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/l/c;->m:Lcom/google/android/apps/gmm/car/d/p;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/car/d/m;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/car/d/o;Lcom/google/android/apps/gmm/car/d/p;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->i:Lcom/google/android/apps/gmm/car/d/m;

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->i:Lcom/google/android/apps/gmm/car/d/m;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/d/m;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/m;->d:Ljava/lang/Object;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/l/c;->e()V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 95
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/l/c;->g:Lcom/google/android/apps/gmm/car/ax;

    .line 94
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->k:Lcom/google/android/apps/gmm/map/f/e;

    if-eqz v0, :cond_0

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ax;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/e;->a(Landroid/graphics/Rect;)V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->s:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/l/c;->g:Lcom/google/android/apps/gmm/car/ax;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/d/a;->l:Lcom/google/android/apps/gmm/car/ax;

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/l/c;->h:Lcom/google/android/apps/gmm/z/b/o;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/o;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->d:Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V
    .locals 4

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 169
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 170
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/l/c;->e:Lcom/google/android/apps/gmm/car/l/i;

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 169
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d()V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 170
    check-cast v0, Landroid/graphics/drawable/Drawable;

    iput-object v0, v2, Lcom/google/android/apps/gmm/car/l/i;->d:Landroid/graphics/drawable/Drawable;

    .line 171
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->k:Lcom/google/android/apps/gmm/map/f/e;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/f/e;->a(Landroid/graphics/Rect;)V

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->i:Lcom/google/android/apps/gmm/car/d/m;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/d/m;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/m;->d:Ljava/lang/Object;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 105
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/l/c;->i:Lcom/google/android/apps/gmm/car/d/m;

    .line 107
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/l/c;->f:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->j:Lcom/google/android/apps/gmm/shared/net/b;

    if-eqz v0, :cond_1

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->j:Lcom/google/android/apps/gmm/shared/net/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/b;->a()Lcom/google/android/apps/gmm/shared/net/b;

    .line 112
    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/l/c;->e:Lcom/google/android/apps/gmm/car/l/i;

    .line 113
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/l/c;->d:Landroid/view/View;

    .line 114
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 118
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lcom/google/android/apps/gmm/car/m/e;->b:Lcom/google/android/apps/gmm/car/m/e;

    return-object v0
.end method

.method e()V
    .locals 5

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->j:Lcom/google/android/apps/gmm/shared/net/b;

    if-nez v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 132
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    .line 133
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    const-class v1, Lcom/google/r/b/a/aog;

    .line 135
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/l/c;->l:Lcom/google/android/apps/gmm/shared/net/c;

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 136
    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->j:Lcom/google/android/apps/gmm/shared/net/b;

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->j:Lcom/google/android/apps/gmm/shared/net/b;

    invoke-static {}, Lcom/google/r/b/a/aog;->newBuilder()Lcom/google/r/b/a/aoi;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/l/c;->c:Lcom/google/android/apps/gmm/map/g/e;

    .line 139
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/g/e;->a:Lcom/google/android/apps/gmm/map/internal/c/cf;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/internal/c/cf;->a:J

    invoke-virtual {v1}, Lcom/google/r/b/a/aoi;->c()V

    iget-object v4, v1, Lcom/google/r/b/a/aoi;->b:Ljava/util/List;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    invoke-virtual {v1}, Lcom/google/r/b/a/aoi;->g()Lcom/google/n/t;

    move-result-object v1

    .line 138
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 143
    new-instance v0, Lcom/google/android/apps/gmm/car/l/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/l/d;-><init>(Lcom/google/android/apps/gmm/car/l/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->f:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->c:Lcom/google/android/apps/gmm/map/g/e;

    .line 156
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/g/e;->a:Lcom/google/android/apps/gmm/map/internal/c/cf;

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/aa;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cf;->d:Lcom/google/android/apps/gmm/map/internal/c/p;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(Lcom/google/android/apps/gmm/map/internal/c/p;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 157
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/car/l/c;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "#requestTrafficIncidentDetails"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/l/c;->f:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    .line 155
    invoke-static {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/map/o/h;->a(Lcom/google/android/apps/gmm/map/internal/c/aa;Lcom/google/android/apps/gmm/map/internal/d/c/a/g;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_1

    .line 160
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/l/c;->a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->e:Lcom/google/android/apps/gmm/car/l/i;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/l/i;->b:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/gmm/car/l/l;->a:Lcom/google/android/apps/gmm/car/l/l;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/l/i;->c:Lcom/google/android/apps/gmm/car/l/l;

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/l/c;->e:Lcom/google/android/apps/gmm/car/l/i;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 165
    return-void
.end method

.method f()V
    .locals 8

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->c:Lcom/google/android/apps/gmm/map/g/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/g/d;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v2

    const-wide v4, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    const-wide v6, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v2, v6

    mul-double/2addr v2, v4

    const-wide v4, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v2, v4

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/c;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    .line 176
    const/high16 v2, 0x41700000    # 15.0f

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/l/c;->g:Lcom/google/android/apps/gmm/car/ax;

    .line 180
    iget-object v3, v3, Lcom/google/android/apps/gmm/car/ax;->c:Landroid/graphics/Rect;

    .line 177
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/map/c;->b(Lcom/google/android/apps/gmm/map/b/a/q;FLandroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v1

    const/4 v2, -0x1

    .line 180
    iput v2, v1, Lcom/google/android/apps/gmm/map/a;->a:I

    const/4 v2, 0x0

    .line 176
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 182
    return-void
.end method
