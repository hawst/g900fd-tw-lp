.class public abstract Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;
.super Landroid/widget/ImageView;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field public static final b:Lcom/google/android/apps/gmm/util/webimageview/g;

.field static final c:Ljava/util/regex/Pattern;


# instance fields
.field d:I

.field public e:Z

.field public f:Lcom/google/android/apps/gmm/util/webimageview/f;

.field private final g:Lcom/google/android/apps/gmm/util/webimageview/j;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/google/android/apps/gmm/util/webimageview/b;

.field private k:Lcom/google/android/apps/gmm/util/webimageview/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->a:Ljava/lang/String;

    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/util/webimageview/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/util/webimageview/a;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->b:Lcom/google/android/apps/gmm/util/webimageview/g;

    .line 113
    const-string v0, "\\$(.)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/google/android/apps/gmm/util/webimageview/j;)V
    .locals 1

    .prologue
    .line 201
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 120
    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/b;->c:Lcom/google/android/apps/gmm/util/webimageview/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->j:Lcom/google/android/apps/gmm/util/webimageview/b;

    .line 136
    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->b:Lcom/google/android/apps/gmm/util/webimageview/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->k:Lcom/google/android/apps/gmm/util/webimageview/g;

    .line 202
    iput-object p3, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->g:Lcom/google/android/apps/gmm/util/webimageview/j;

    .line 203
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 352
    new-instance v0, Lcom/google/android/apps/gmm/util/webimageview/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->k:Lcom/google/android/apps/gmm/util/webimageview/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->i:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/gmm/util/webimageview/f;-><init>(Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;Lcom/google/android/apps/gmm/util/webimageview/g;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->f:Lcom/google/android/apps/gmm/util/webimageview/f;

    .line 354
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->g:Lcom/google/android/apps/gmm/util/webimageview/j;

    iget-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->f:Lcom/google/android/apps/gmm/util/webimageview/f;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/util/webimageview/j;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/k;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->f:Lcom/google/android/apps/gmm/util/webimageview/f;

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/util/webimageview/f;->a:Z

    .line 358
    return-void

    .line 357
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->f:Lcom/google/android/apps/gmm/util/webimageview/f;

    iput-boolean v4, v1, Lcom/google/android/apps/gmm/util/webimageview/f;->a:Z

    throw v0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 29
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;)V
    .locals 6

    .prologue
    .line 207
    iput-object p2, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->j:Lcom/google/android/apps/gmm/util/webimageview/b;

    .line 209
    iput-object p1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->h:Ljava/lang/String;

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 211
    iget-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->h:Ljava/lang/String;

    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->e:Z

    move-object v0, p2

    .line 211
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/util/webimageview/b;->a(Ljava/lang/String;IILandroid/widget/ImageView$ScaleType;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->i:Ljava/lang/String;

    .line 216
    :goto_0
    return-void

    .line 214
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 435
    iput-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->h:Ljava/lang/String;

    .line 436
    iput-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->i:Ljava/lang/String;

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->f:Lcom/google/android/apps/gmm/util/webimageview/f;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->f:Lcom/google/android/apps/gmm/util/webimageview/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/util/webimageview/k;->e:Z

    .line 442
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->b:Lcom/google/android/apps/gmm/util/webimageview/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->k:Lcom/google/android/apps/gmm/util/webimageview/g;

    .line 443
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/gmm/util/webimageview/g;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Drawable;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/util/webimageview/g;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 325
    iput-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->h:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->i:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->f:Lcom/google/android/apps/gmm/util/webimageview/f;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->f:Lcom/google/android/apps/gmm/util/webimageview/f;

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/util/webimageview/k;->e:Z

    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->b:Lcom/google/android/apps/gmm/util/webimageview/g;

    iput-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->k:Lcom/google/android/apps/gmm/util/webimageview/g;

    .line 327
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;)V

    .line 329
    if-eqz p4, :cond_3

    :goto_0
    iput-object p4, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->k:Lcom/google/android/apps/gmm/util/webimageview/g;

    .line 330
    iput p5, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->d:I

    .line 333
    invoke-super {p0, p3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 334
    iget-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->k:Lcom/google/android/apps/gmm/util/webimageview/g;

    .line 337
    iget-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->i:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_4

    :cond_1
    :goto_1
    if-eqz v0, :cond_5

    .line 345
    :cond_2
    :goto_2
    return-void

    .line 329
    :cond_3
    sget-object p4, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->b:Lcom/google/android/apps/gmm/util/webimageview/g;

    goto :goto_0

    .line 337
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 342
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->getWidth()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->getHeight()I

    move-result v0

    if-lez v0, :cond_2

    .line 343
    invoke-direct {p0}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->a()V

    goto :goto_2
.end method

.method public onSizeChanged(IIII)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 408
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 411
    iget-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->f:Lcom/google/android/apps/gmm/util/webimageview/f;

    if-eqz v1, :cond_0

    .line 412
    iget-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->f:Lcom/google/android/apps/gmm/util/webimageview/f;

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/util/webimageview/k;->e:Z

    .line 416
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->j:Lcom/google/android/apps/gmm/util/webimageview/b;

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;)V

    .line 419
    iget-object v1, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->i:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    :goto_0
    if-eqz v0, :cond_4

    .line 427
    :cond_2
    :goto_1
    return-void

    .line 419
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 424
    :cond_4
    if-ne p1, p3, :cond_5

    if-eq p2, p4, :cond_2

    .line 425
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->a()V

    goto :goto_1
.end method

.method public abstract setFadingImage(Lcom/google/android/apps/gmm/util/webimageview/f;Landroid/graphics/Bitmap;)V
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 457
    invoke-direct {p0}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->b()V

    .line 458
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 459
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 451
    invoke-direct {p0}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->b()V

    .line 452
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 453
    return-void
.end method

.method public setImageResource(I)V
    .locals 0

    .prologue
    .line 463
    invoke-direct {p0}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->b()V

    .line 464
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 465
    return-void
.end method

.method public final setTransitionDrawable(Lcom/google/android/apps/gmm/util/webimageview/f;Landroid/graphics/drawable/TransitionDrawable;)V
    .locals 1

    .prologue
    .line 399
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/util/webimageview/k;->e:Z

    if-eqz v0, :cond_0

    .line 404
    :goto_0
    return-void

    .line 402
    :cond_0
    invoke-super {p0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 403
    iget v0, p0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->d:I

    invoke-virtual {p2, v0}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    goto :goto_0
.end method
