.class public Lcom/google/android/apps/gmm/z/m;
.super Lcom/google/android/apps/gmm/z/b/e;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/z/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/z/g;",
            ">;",
            "Lcom/google/android/apps/gmm/shared/c/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/z/b/e;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/m;->a:Ljava/util/List;

    .line 37
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/b/f/b/a/bc;)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 41
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/z/b/e;->a(Lcom/google/b/f/b/a/bc;)V

    .line 43
    invoke-static {}, Lcom/google/b/f/b/a/dr;->newBuilder()Lcom/google/b/f/b/a/dt;

    move-result-object v5

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/m;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 51
    new-instance v1, Lcom/google/android/apps/gmm/z/n;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/z/n;-><init>(Lcom/google/android/apps/gmm/z/m;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 58
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/z/g;

    .line 59
    iget-object v7, v0, Lcom/google/android/apps/gmm/z/g;->a:Lcom/google/android/apps/gmm/z/b/l;

    .line 61
    invoke-static {}, Lcom/google/b/f/b/a/du;->newBuilder()Lcom/google/b/f/b/a/dw;

    move-result-object v8

    .line 62
    iget-object v1, v7, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 63
    iget-object v1, v7, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v9, v8, Lcom/google/b/f/b/a/dw;->a:I

    or-int/lit8 v9, v9, 0x1

    iput v9, v8, Lcom/google/b/f/b/a/dw;->a:I

    iput-object v1, v8, Lcom/google/b/f/b/a/dw;->b:Ljava/lang/Object;

    .line 65
    :cond_1
    iget-object v1, v7, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    if-eqz v1, :cond_2

    iget-object v1, v7, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    array-length v1, v1

    if-gtz v1, :cond_6

    :cond_2
    move-object v1, v2

    :goto_1
    if-eqz v1, :cond_4

    .line 66
    iget-object v1, v7, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    if-eqz v1, :cond_3

    iget-object v1, v7, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    array-length v1, v1

    if-gtz v1, :cond_7

    :cond_3
    move-object v1, v2

    :goto_2
    invoke-interface {v1}, Lcom/google/b/f/cq;->a()I

    move-result v1

    iget v9, v8, Lcom/google/b/f/b/a/dw;->a:I

    or-int/lit8 v9, v9, 0x8

    iput v9, v8, Lcom/google/b/f/b/a/dw;->a:I

    iput v1, v8, Lcom/google/b/f/b/a/dw;->e:I

    .line 68
    :cond_4
    iget-object v1, v7, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_8

    :cond_5
    move v1, v4

    :goto_3
    if-nez v1, :cond_a

    .line 69
    invoke-static {}, Lcom/google/b/f/b/a/do;->newBuilder()Lcom/google/b/f/b/a/dq;

    move-result-object v1

    iget-object v7, v7, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    if-nez v7, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 65
    :cond_6
    iget-object v1, v7, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    aget-object v1, v1, v3

    goto :goto_1

    .line 66
    :cond_7
    iget-object v1, v7, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    aget-object v1, v1, v3

    goto :goto_2

    :cond_8
    move v1, v3

    .line 68
    goto :goto_3

    .line 69
    :cond_9
    iget v9, v1, Lcom/google/b/f/b/a/dq;->a:I

    or-int/lit8 v9, v9, 0x2

    iput v9, v1, Lcom/google/b/f/b/a/dq;->a:I

    iput-object v7, v1, Lcom/google/b/f/b/a/dq;->b:Ljava/lang/Object;

    invoke-virtual {v8}, Lcom/google/b/f/b/a/dw;->c()V

    iget-object v7, v8, Lcom/google/b/f/b/a/dw;->c:Ljava/util/List;

    invoke-virtual {v1}, Lcom/google/b/f/b/a/dq;->g()Lcom/google/n/t;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_a
    iget-wide v0, v0, Lcom/google/android/apps/gmm/z/g;->b:J

    iget v7, v8, Lcom/google/b/f/b/a/dw;->a:I

    or-int/lit8 v7, v7, 0x4

    iput v7, v8, Lcom/google/b/f/b/a/dw;->a:I

    iput-wide v0, v8, Lcom/google/b/f/b/a/dw;->d:J

    .line 72
    invoke-virtual {v5}, Lcom/google/b/f/b/a/dt;->c()V

    iget-object v0, v5, Lcom/google/b/f/b/a/dt;->a:Ljava/util/List;

    invoke-virtual {v8}, Lcom/google/b/f/b/a/dw;->g()Lcom/google/n/t;

    move-result-object v1

    new-instance v7, Lcom/google/n/ao;

    invoke-direct {v7}, Lcom/google/n/ao;-><init>()V

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v7, Lcom/google/n/ao;->d:Z

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 75
    :cond_b
    iget-object v0, p1, Lcom/google/b/f/b/a/bc;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/b/f/b/a/dt;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p1, Lcom/google/b/f/b/a/bc;->a:I

    .line 76
    return-void
.end method
