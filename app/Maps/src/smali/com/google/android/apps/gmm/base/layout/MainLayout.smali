.class public abstract Lcom/google/android/apps/gmm/base/layout/MainLayout;
.super Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;
.implements Lcom/google/android/apps/gmm/base/views/expandingscrollview/q;


# static fields
.field static final synthetic V:Z

.field private static final W:Ljava/lang/String;


# instance fields
.field public A:I

.field public B:Lcom/google/android/apps/gmm/base/activities/z;

.field public C:Z

.field public D:Z

.field public E:Z

.field public F:Z

.field public G:Z

.field public H:Z

.field public I:Z

.field public J:Z

.field public K:Lcom/google/android/apps/gmm/base/activities/c;

.field public L:Lcom/google/android/apps/gmm/base/activities/p;

.field public M:Lcom/google/android/apps/gmm/base/l/ai;

.field N:Landroid/animation/AnimatorSet;

.field public O:Z

.field public P:Z

.field Q:Landroid/animation/AnimatorSet;

.field public R:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field public final S:Ljava/lang/Runnable;

.field public T:Lcom/google/android/apps/gmm/base/views/MapViewContainer;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final U:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field public a:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Lcom/google/android/apps/gmm/base/views/MapViewContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final aa:Landroid/view/animation/Animation;

.field private final ab:Landroid/view/animation/Animation;

.field private ac:Lcom/google/android/apps/gmm/base/layout/w;

.field private ad:Z

.field private ae:Lcom/google/android/apps/gmm/base/l/r;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final af:Lcom/google/android/apps/gmm/util/a/e;

.field private ag:Z

.field private ah:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field private ai:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field private aj:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private ak:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final al:Ljava/lang/Object;

.field public b:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Lcom/google/android/apps/gmm/prefetchcache/api/NextButton;",
            ">;"
        }
    .end annotation
.end field

.field public q:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public s:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public v:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field public w:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public x:Lcom/google/android/apps/gmm/base/layout/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<*>;>;"
        }
    .end annotation
.end field

.field public z:Lcom/google/android/apps/gmm/base/activities/x;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    const-class v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->V:Z

    .line 112
    const-class v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->W:Ljava/lang/String;

    return-void

    .line 103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 482
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 123
    sget v2, Lcom/google/android/apps/gmm/g;->bF:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a:Lcom/google/android/apps/gmm/base/layout/y;

    .line 124
    sget v2, Lcom/google/android/apps/gmm/g;->aH:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    .line 125
    sget v2, Lcom/google/android/apps/gmm/g;->aN:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->c:Lcom/google/android/apps/gmm/base/layout/y;

    .line 126
    sget v2, Lcom/google/android/apps/gmm/g;->Q:I

    .line 127
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->d:Lcom/google/android/apps/gmm/base/layout/y;

    .line 128
    sget v2, Lcom/google/android/apps/gmm/g;->bX:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    .line 129
    sget v2, Lcom/google/android/apps/gmm/g;->x:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->e:Lcom/google/android/apps/gmm/base/layout/y;

    .line 130
    sget v2, Lcom/google/android/apps/gmm/g;->aD:I

    .line 131
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f:Lcom/google/android/apps/gmm/base/layout/y;

    .line 132
    sget v2, Lcom/google/android/apps/gmm/g;->aE:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->g:Lcom/google/android/apps/gmm/base/layout/y;

    .line 138
    sget v2, Lcom/google/android/apps/gmm/g;->F:I

    .line 139
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->h:Lcom/google/android/apps/gmm/base/layout/y;

    .line 140
    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    .line 141
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    .line 142
    sget v2, Lcom/google/android/apps/gmm/g;->cY:I

    .line 143
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/layout/y;->c:Z

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->j:Lcom/google/android/apps/gmm/base/layout/y;

    .line 144
    const v2, -0x21524111

    .line 145
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/layout/y;->c:Z

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->k:Lcom/google/android/apps/gmm/base/layout/y;

    .line 148
    sget v2, Lcom/google/android/apps/gmm/g;->eD:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->l:Lcom/google/android/apps/gmm/base/layout/y;

    .line 149
    sget v2, Lcom/google/android/apps/gmm/g;->cB:I

    .line 150
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->m:Lcom/google/android/apps/gmm/base/layout/y;

    .line 151
    sget v2, Lcom/google/android/apps/gmm/g;->aW:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    .line 152
    sget v2, Lcom/google/android/apps/gmm/g;->aC:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->o:Lcom/google/android/apps/gmm/base/layout/y;

    .line 158
    sget v2, Lcom/google/android/apps/gmm/g;->bU:I

    .line 159
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->p:Lcom/google/android/apps/gmm/base/layout/y;

    .line 162
    sget v2, Lcom/google/android/apps/gmm/g;->eo:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->q:Lcom/google/android/apps/gmm/base/layout/y;

    .line 165
    iput-object v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->r:Lcom/google/android/apps/gmm/base/layout/y;

    .line 167
    sget v2, Lcom/google/android/apps/gmm/g;->bY:I

    .line 168
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->s:Lcom/google/android/apps/gmm/base/layout/y;

    .line 170
    sget v2, Lcom/google/android/apps/gmm/g;->dd:I

    .line 171
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    .line 174
    sget v2, Lcom/google/android/apps/gmm/g;->bE:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->u:Lcom/google/android/apps/gmm/base/layout/y;

    .line 175
    sget v2, Lcom/google/android/apps/gmm/g;->s:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    .line 176
    sget v2, Lcom/google/android/apps/gmm/g;->dI:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->v:Lcom/google/android/apps/gmm/base/layout/y;

    .line 177
    sget v2, Lcom/google/android/apps/gmm/g;->dr:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->w:Lcom/google/android/apps/gmm/base/layout/y;

    .line 186
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a:Lcom/google/android/apps/gmm/base/layout/y;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/layout/y;->c:Z

    .line 193
    sget v2, Lcom/google/android/apps/gmm/g;->bG:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(I)Lcom/google/android/apps/gmm/base/layout/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->x:Lcom/google/android/apps/gmm/base/layout/y;

    .line 224
    sget-object v2, Lcom/google/android/apps/gmm/base/layout/w;->c:Lcom/google/android/apps/gmm/base/layout/w;

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ac:Lcom/google/android/apps/gmm/base/layout/w;

    .line 229
    iput v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->A:I

    .line 236
    iput-object v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->B:Lcom/google/android/apps/gmm/base/activities/z;

    .line 257
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ad:Z

    .line 296
    new-instance v2, Lcom/google/android/apps/gmm/util/a/e;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/util/a/e;-><init>()V

    .line 297
    iput-boolean v1, v2, Lcom/google/android/apps/gmm/util/a/e;->c:Z

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->af:Lcom/google/android/apps/gmm/util/a/e;

    .line 300
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ag:Z

    .line 319
    iput-object v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->aj:Lcom/google/android/apps/gmm/base/layout/y;

    .line 324
    iput-object v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ak:Lcom/google/android/apps/gmm/base/layout/y;

    .line 328
    new-instance v2, Lcom/google/android/apps/gmm/base/layout/a;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/base/layout/a;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->R:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 335
    new-instance v2, Lcom/google/android/apps/gmm/base/layout/l;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/base/layout/l;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->S:Ljava/lang/Runnable;

    .line 342
    new-instance v2, Lcom/google/android/apps/gmm/base/layout/o;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/base/layout/o;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->al:Ljava/lang/Object;

    .line 459
    new-instance v2, Lcom/google/android/apps/gmm/base/layout/q;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/base/layout/q;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->U:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 484
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    .line 485
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v2

    const/4 v3, 0x6

    new-array v3, v3, [Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    aput-object v4, v3, v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->j:Lcom/google/android/apps/gmm/base/layout/y;

    aput-object v4, v3, v0

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->u:Lcom/google/android/apps/gmm/base/layout/y;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->v:Lcom/google/android/apps/gmm/base/layout/y;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->m:Lcom/google/android/apps/gmm/base/layout/y;

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->l:Lcom/google/android/apps/gmm/base/layout/y;

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/b/c/cx;->a([Ljava/lang/Object;)Lcom/google/b/c/cx;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->l:Lcom/google/android/apps/gmm/base/layout/y;

    invoke-virtual {v2, v3}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :cond_0
    invoke-virtual {v2}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->y:Ljava/util/List;

    .line 487
    sget v2, Lcom/google/android/apps/gmm/b;->d:I

    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->aa:Landroid/view/animation/Animation;

    .line 489
    sget v2, Lcom/google/android/apps/gmm/b;->e:I

    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ab:Landroid/view/animation/Animation;

    .line 492
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->D:Z

    .line 493
    return-void

    :cond_1
    move v2, v1

    .line 492
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/base/layout/y;Z)Landroid/animation/Animator;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/ViewGroup;",
            ">;Z)",
            "Landroid/animation/Animator;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2769
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBottom()I

    move-result v1

    .line 2770
    if-eqz p2, :cond_1

    .line 2772
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2775
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2776
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    const-string v2, "translationY"

    const/4 v3, 0x2

    new-array v3, v3, [F

    neg-int v1, v1

    int-to-float v1, v1

    aput v1, v3, v4

    aput v6, v3, v5

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2788
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->R:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2789
    new-instance v1, Lcom/google/android/apps/gmm/base/layout/m;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/gmm/base/layout/m;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;ZLcom/google/android/apps/gmm/base/layout/y;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2797
    return-object v0

    .line 2782
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    const-string v1, "translationY"

    new-array v2, v5, [F

    aput v6, v2, v4

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    goto :goto_0

    .line 2786
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    const-string v2, "translationY"

    new-array v3, v5, [F

    neg-int v1, v1

    int-to-float v1, v1

    aput v1, v3, v4

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Z)Landroid/graphics/Rect;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2316
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    .line 2317
    invoke-static {p0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;)Z

    move-result v3

    .line 2318
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2319
    sget v5, Lcom/google/android/apps/gmm/e;->ay:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 2320
    sget v6, Lcom/google/android/apps/gmm/e;->bv:I

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 2321
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    .line 2322
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2323
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v6

    .line 2325
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 2327
    iget-object v0, v6, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v0

    iget-object v8, v6, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v8

    invoke-virtual {v7, v2, v2, v0, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 2330
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->r()Z

    move-result v8

    .line 2333
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    .line 2334
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->f:Lcom/google/android/apps/gmm/base/l/ai;

    if-eqz v0, :cond_2

    .line 2335
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->F:Z

    if-eqz v0, :cond_1

    if-nez v8, :cond_2

    .line 2337
    :cond_1
    iget v9, v7, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBottom()I

    move-result v0

    invoke-static {v9, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v7, Landroid/graphics/Rect;->top:I

    .line 2343
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ai:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v9, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    invoke-virtual {p0, v0, v9}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/activities/p;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->G:Z

    if-eqz v0, :cond_3

    if-nez v8, :cond_4

    .line 2345
    :cond_3
    iget v0, v7, Landroid/graphics/Rect;->top:I

    iget-object v8, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v8, v8, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getBottom()I

    move-result v8

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v7, Landroid/graphics/Rect;->top:I

    .line 2349
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 2350
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->q()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2351
    iget-boolean v8, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->F:Z

    if-eqz v8, :cond_9

    .line 2352
    if-eqz v3, :cond_8

    .line 2353
    iget v8, v7, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getLeft()I

    move-result v0

    invoke-static {v8, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v7, Landroid/graphics/Rect;->right:I

    .line 2364
    :cond_5
    :goto_0
    if-nez p1, :cond_6

    .line 2365
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->m:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v8, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    .line 2366
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    .line 2367
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_a

    move v0, v1

    :goto_1
    if-eqz v0, :cond_b

    .line 2369
    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {v8}, Landroid/view/View;->getTranslationY()F

    move-result v8

    float-to-int v8, v8

    add-int/2addr v0, v8

    .line 2370
    iget v8, v7, Landroid/graphics/Rect;->bottom:I

    invoke-static {v8, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    .line 2384
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-eqz v0, :cond_d

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->E:Z

    if-eqz v0, :cond_7

    .line 2385
    if-eqz v3, :cond_e

    .line 2386
    iget v0, v7, Landroid/graphics/Rect;->right:I

    iget-object v1, v6, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v1

    sub-int/2addr v1, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v7, Landroid/graphics/Rect;->right:I

    .line 2392
    :cond_7
    :goto_4
    invoke-virtual {v7, v5, v5}, Landroid/graphics/Rect;->inset(II)V

    .line 2393
    return-object v7

    .line 2355
    :cond_8
    iget v8, v7, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getRight()I

    move-result v0

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v7, Landroid/graphics/Rect;->left:I

    goto :goto_0

    .line 2358
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getHeight()I

    move-result v8

    iget-object v9, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v9

    aget v0, v0, v9

    sub-int v0, v8, v0

    .line 2359
    iget v8, v7, Landroid/graphics/Rect;->bottom:I

    invoke-static {v8, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    :cond_a
    move v0, v2

    .line 2367
    goto :goto_1

    .line 2371
    :cond_b
    if-eqz v3, :cond_c

    .line 2373
    invoke-virtual {v8}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {v8}, Landroid/view/View;->getTranslationX()F

    move-result v8

    float-to-int v8, v8

    add-int/2addr v0, v8

    .line 2374
    iget v8, v7, Landroid/graphics/Rect;->left:I

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v7, Landroid/graphics/Rect;->left:I

    goto :goto_2

    .line 2377
    :cond_c
    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {v8}, Landroid/view/View;->getTranslationX()F

    move-result v8

    float-to-int v8, v8

    add-int/2addr v0, v8

    .line 2378
    iget v8, v7, Landroid/graphics/Rect;->right:I

    invoke-static {v8, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v7, Landroid/graphics/Rect;->right:I

    goto :goto_2

    :cond_d
    move v0, v2

    .line 2384
    goto :goto_3

    .line 2388
    :cond_e
    iget v0, v7, Landroid/graphics/Rect;->left:I

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v7, Landroid/graphics/Rect;->left:I

    goto :goto_4
.end method

.method private a(Lcom/google/android/apps/gmm/base/activities/p;Z)V
    .locals 7

    .prologue
    const/4 v2, -0x1

    .line 1209
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/base/activities/p;->c:Z

    if-nez v0, :cond_1

    .line 1232
    :cond_0
    :goto_0
    return-void

    .line 1213
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v3, Landroid/view/ViewGroup;

    .line 1214
    sget-boolean v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->V:Z

    if-nez v0, :cond_2

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1216
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/p;->b:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    .line 1217
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eq v1, v0, :cond_0

    .line 1222
    invoke-virtual {v3}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1223
    if-eqz v0, :cond_3

    .line 1224
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1226
    invoke-virtual {v3, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1229
    :cond_3
    if-eqz p2, :cond_0

    .line 1230
    iget-object v6, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    iget v0, p1, Lcom/google/android/apps/gmm/base/activities/p;->P:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/google/android/apps/gmm/base/activities/p;->P:I

    new-instance v1, Lcom/google/android/apps/gmm/base/activities/t;

    const-wide/16 v4, 0x3e8

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/base/activities/t;-><init>(Lcom/google/android/apps/gmm/base/activities/p;Landroid/view/View;JLcom/google/android/apps/gmm/base/activities/c;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/base/layout/y;Landroid/graphics/Rect;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/layout/y",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/graphics/Rect;",
            ")V"
        }
    .end annotation

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 956
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 957
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 963
    :goto_0
    return-void

    .line 959
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 960
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {p1, v0, v2, v1, v2}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 961
    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget v1, p2, Landroid/graphics/Rect;->top:I

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p1, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2661
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v3

    .line 2662
    iget-object v2, v3, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    if-eqz v2, :cond_2

    .line 2663
    iget-object v2, v3, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->o()Lcom/google/android/apps/gmm/map/m/l;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2665
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->q()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p1, v2, :cond_3

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p1, v2, :cond_3

    move v2, v1

    :goto_0
    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->F:Z

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 2666
    :cond_1
    iget-object v1, v3, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->o()Lcom/google/android/apps/gmm/map/m/l;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/m/l;->c(Z)V

    .line 2668
    :cond_2
    return-void

    :cond_3
    move v2, v0

    .line 2665
    goto :goto_0
.end method

.method private static a(Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2175
    instance-of v2, p0, Landroid/view/SurfaceView;

    if-eqz v2, :cond_1

    .line 2185
    :cond_0
    :goto_0
    return v0

    .line 2177
    :cond_1
    instance-of v2, p0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    .line 2178
    check-cast p0, Landroid/view/ViewGroup;

    .line 2179
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    .line 2180
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Landroid/view/View;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2179
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2185
    goto :goto_0
.end method

.method public static a(Landroid/view/ViewGroup;Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1363
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 1374
    :goto_0
    return v0

    .line 1368
    :cond_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1370
    if-eqz p1, :cond_1

    .line 1371
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1374
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/base/activities/p;Landroid/view/View;)Z
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1963
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1964
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 1966
    sget v4, Lcom/google/android/apps/gmm/g;->aG:I

    .line 1967
    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    .line 1968
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    sget v4, Lcom/google/android/apps/gmm/g;->aG:I

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-ne v1, p2, :cond_0

    move v0, v2

    .line 2009
    :goto_0
    return v0

    .line 1974
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->bringToFront()V

    invoke-virtual {v1}, Landroid/view/ViewGroup;->invalidate()V

    invoke-virtual {v1}, Landroid/view/ViewGroup;->requestLayout()V

    .line 1980
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 1981
    iget-object v4, p1, Lcom/google/android/apps/gmm/base/activities/p;->f:Lcom/google/android/apps/gmm/base/l/ai;

    if-eqz v4, :cond_4

    .line 1982
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1983
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/base/f/bp;->a(Z)I

    move-result v4

    .line 1982
    invoke-static {v2, v4}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1993
    :cond_1
    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1995
    sget v1, Lcom/google/android/apps/gmm/g;->aG:I

    .line 1996
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1997
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1998
    if-eqz p2, :cond_3

    .line 1999
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 2000
    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    .line 2001
    sget-object v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->W:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x29

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "fullscreen view ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ") already has a parent: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2002
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2004
    :cond_2
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2006
    invoke-virtual {v0, p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_3
    move v0, v3

    .line 2009
    goto/16 :goto_0

    .line 1984
    :cond_4
    iget v4, p1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    if-ne v4, v3, :cond_5

    .line 1985
    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    goto :goto_1

    .line 1986
    :cond_5
    iget v2, p1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_1

    .line 1991
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/activities/p;->b:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a()I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/base/layout/MainLayout;Lcom/google/android/apps/gmm/base/activities/p;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/activities/p;Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method private b(F)F
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 887
    .line 890
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->g:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    .line 891
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->I:Z

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 892
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getHeight()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTop()I

    move-result v3

    sub-int/2addr v1, v3

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v0

    sub-float v0, v1, v0

    move v1, v0

    .line 895
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->F:Z

    if-nez v0, :cond_0

    .line 896
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 897
    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v3

    aget v0, v0, v3

    int-to-float v0, v0

    mul-float/2addr v0, p1

    .line 900
    :goto_1
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 901
    return v0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public static b(Landroid/view/ViewGroup;Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1705
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1706
    if-eqz v1, :cond_2

    .line 1707
    if-ne v1, p1, :cond_1

    .line 1709
    const/4 v0, 0x0

    .line 1721
    :cond_0
    :goto_0
    return v0

    .line 1712
    :cond_1
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1715
    :cond_2
    if-eqz p1, :cond_0

    .line 1716
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1718
    invoke-virtual {p0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private j()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, -0x80000000

    .line 829
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 854
    :goto_0
    return-void

    .line 833
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 834
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getMeasuredWidth()I

    move-result v1

    .line 836
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 837
    sget v2, Lcom/google/android/apps/gmm/e;->av:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 838
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->l:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->l:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    .line 839
    invoke-virtual {v4}, Landroid/view/View;->getTranslationY()F

    move-result v4

    float-to-int v4, v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v4

    mul-int/lit8 v4, v0, 0x2

    sub-int/2addr v3, v4

    .line 838
    invoke-virtual {v2, v1, v5, v3, v5}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 841
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->l:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 842
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v3, v4

    sub-int/2addr v3, v0

    .line 843
    invoke-static {p0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;)Z

    move-result v4

    .line 841
    if-eqz v4, :cond_1

    sub-int v0, v1, v0

    iget-object v1, v2, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, v2, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v4, v2, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v4, v0

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v3

    invoke-virtual {v1, v0, v3, v4, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 853
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    invoke-virtual {v0, v6, v6}, Lcom/google/android/apps/gmm/base/layout/y;->a(II)V

    goto :goto_0

    .line 841
    :cond_1
    iget-object v1, v2, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v4, v2, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v4, v0

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v3

    invoke-virtual {v1, v0, v3, v4, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1

    .line 845
    :cond_2
    sget v2, Lcom/google/android/apps/gmm/e;->aW:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 846
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->v:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->v:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/FrameLayout;

    .line 847
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTranslationY()F

    move-result v0

    float-to-int v0, v0

    add-int/2addr v0, v4

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v4

    mul-int/lit8 v4, v2, 0x2

    sub-int/2addr v0, v4

    .line 846
    invoke-virtual {v3, v1, v5, v0, v5}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 849
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->v:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 850
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v3, v4

    sub-int/2addr v3, v2

    .line 851
    invoke-static {p0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;)Z

    move-result v4

    .line 849
    if-eqz v4, :cond_3

    sub-int/2addr v1, v2

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v4, v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int/2addr v0, v3

    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1

    :cond_3
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v4, v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int/2addr v0, v3

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_1
.end method

.method private k()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1780
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->E:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->F:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    .line 1781
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1782
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/e;->bv:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1784
    invoke-static {p0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1785
    neg-int v0, v0

    move v1, v0

    .line 1787
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->u:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    int-to-float v2, v1

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 1788
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->v:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/FrameLayout;

    int-to-float v2, v1

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setTranslationX(F)V

    .line 1789
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/LinearLayout;

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setTranslationX(F)V

    .line 1795
    :goto_1
    return-void

    .line 1791
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->u:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 1792
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->v:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setTranslationX(F)V

    .line 1793
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setTranslationX(F)V

    goto :goto_1

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method private l()Landroid/widget/EditText;
    .locals 2

    .prologue
    .line 2042
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    if-nez v0, :cond_1

    .line 2043
    :cond_0
    const/4 v0, 0x0

    .line 2045
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/gmm/base/f/bp;->c:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    goto :goto_0
.end method

.method private m()Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2050
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2051
    sget v1, Lcom/google/android/apps/gmm/g;->aG:I

    .line 2052
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2053
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private n()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2403
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-nez v2, :cond_1

    .line 2407
    :cond_0
    :goto_0
    return v0

    .line 2406
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ai:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ai:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    .line 2407
    iget-object v3, v3, Lcom/google/android/apps/gmm/base/activities/p;->g:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget v2, v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->g:F

    iget v3, v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->g:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    move v2, v1

    :goto_1
    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method private o()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2455
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->C:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->E:Z

    if-nez v2, :cond_1

    .line 2461
    :cond_0
    :goto_0
    return v0

    .line 2458
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->q()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2459
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->F:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ah:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2461
    goto :goto_0
.end method

.method private p()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2507
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v0, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    .line 2508
    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/p;->t:Lcom/google/android/apps/gmm/base/activities/a/a;

    sget-object v3, Lcom/google/android/apps/gmm/base/activities/a/a;->a:Lcom/google/android/apps/gmm/base/activities/a/a;

    if-eq v2, v3, :cond_1

    .line 2509
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->x:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    .line 2510
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->x:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2511
    return-void

    :cond_0
    move v2, v1

    .line 2507
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2508
    goto :goto_1

    .line 2510
    :cond_2
    const/16 v1, 0x8

    goto :goto_2
.end method

.method private q()Z
    .locals 2

    .prologue
    .line 2585
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 2586
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2593
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->E:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-eqz v2, :cond_3

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->F:Z

    if-eqz v2, :cond_2

    .line 2594
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->q()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0

    :cond_3
    move v2, v0

    .line 2593
    goto :goto_0
.end method

.method private s()V
    .locals 3

    .prologue
    .line 2645
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-nez v0, :cond_0

    .line 2657
    :goto_0
    return-void

    .line 2649
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2650
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->E:Z

    if-eqz v1, :cond_1

    .line 2651
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/e;->bv:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 2653
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->b(Landroid/view/View;I)V

    goto :goto_0

    .line 2655
    :cond_1
    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->b(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private t()V
    .locals 4

    .prologue
    .line 2731
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-nez v0, :cond_0

    .line 2764
    :goto_0
    return-void

    .line 2736
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->N:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    .line 2737
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->N:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    .line 2738
    invoke-virtual {v0}, Landroid/animation/Animator;->removeAllListeners()V

    goto :goto_1

    .line 2742
    :cond_1
    const/4 v0, 0x2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2745
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 2746
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->c:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ai:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    .line 2747
    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/activities/p;)Z

    move-result v2

    .line 2746
    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/layout/y;Z)Landroid/animation/Animator;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2749
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->f:Lcom/google/android/apps/gmm/base/l/ai;

    if-eqz v0, :cond_4

    .line 2750
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n()Z

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/layout/y;Z)Landroid/animation/Animator;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2755
    :cond_3
    :goto_2
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->N:Landroid/animation/AnimatorSet;

    .line 2756
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->N:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 2757
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->N:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/google/android/apps/gmm/base/layout/k;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/base/layout/k;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2763
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->N:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 2751
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->b:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    if-eqz v0, :cond_3

    .line 2752
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f:Lcom/google/android/apps/gmm/base/layout/y;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n()Z

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/layout/y;Z)Landroid/animation/Animator;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/base/activities/p;)Landroid/animation/ObjectAnimator;
    .locals 6

    .prologue
    .line 1317
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->c:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    .line 1320
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBottom()I

    move-result v1

    .line 1321
    const-string v2, "translationY"

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v3, v4

    const/4 v4, 0x1

    neg-int v1, v1

    int-to-float v1, v1

    aput v1, v3, v4

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1322
    new-instance v2, Lcom/google/android/apps/gmm/base/layout/t;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/apps/gmm/base/layout/t;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;Lcom/google/android/apps/gmm/base/activities/p;Landroid/view/ViewGroup;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1331
    return-object v1
.end method

.method public final a()V
    .locals 5

    .prologue
    .line 2708
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    .line 2709
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v2

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    .line 2710
    iget-object v4, v1, Lcom/google/android/apps/gmm/base/activities/c;->c:Lcom/google/b/f/t;

    aput-object v4, v0, v3

    iput-object v0, v2, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 2714
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->c:Lcom/google/b/f/t;

    sget-object v3, Lcom/google/b/f/t;->el:Lcom/google/b/f/t;

    if-eq v0, v3, :cond_2

    .line 2715
    invoke-static {}, Lcom/google/b/f/b/a/ck;->newBuilder()Lcom/google/b/f/b/a/cm;

    move-result-object v0

    sget-object v3, Lcom/google/b/f/b/a/cn;->b:Lcom/google/b/f/b/a/cn;

    .line 2716
    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v4, v0, Lcom/google/b/f/b/a/cm;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v0, Lcom/google/b/f/b/a/cm;->a:I

    iget v3, v3, Lcom/google/b/f/b/a/cn;->d:I

    iput v3, v0, Lcom/google/b/f/b/a/cm;->c:I

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 2718
    invoke-static {v3}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/b/f/b/a/cp;

    move-result-object v3

    .line 2717
    invoke-virtual {v0, v3}, Lcom/google/b/f/b/a/cm;->a(Lcom/google/b/f/b/a/cp;)Lcom/google/b/f/b/a/cm;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 2720
    invoke-static {v3}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/b/f/b/a/cp;

    move-result-object v3

    .line 2719
    if-nez v3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v4, v0, Lcom/google/b/f/b/a/cm;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v0, Lcom/google/b/f/b/a/cm;->a:I

    iget v3, v3, Lcom/google/b/f/b/a/cp;->e:I

    iput v3, v0, Lcom/google/b/f/b/a/cm;->d:I

    .line 2721
    invoke-virtual {v0}, Lcom/google/b/f/b/a/cm;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ck;

    .line 2715
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/z/b/m;->a(Lcom/google/b/f/b/a/ck;)Lcom/google/android/apps/gmm/z/b/m;

    .line 2723
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 2724
    return-void
.end method

.method protected final a(F)V
    .locals 3

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->g:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 476
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 477
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    const/4 v2, 0x0

    .line 476
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V

    .line 479
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 1511
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/layout/y;

    .line 1512
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    if-ne v0, v2, :cond_0

    .line 1513
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b()V

    goto :goto_0

    .line 1515
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1518
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->requestLayout()V

    .line 1519
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 2

    .prologue
    .line 2243
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ag:Z

    .line 2244
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->h:Lcom/google/android/apps/gmm/util/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->af:Lcom/google/android/apps/gmm/util/a/e;

    .line 2245
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/util/a/a;->a(Lcom/google/android/apps/gmm/util/a/e;)V

    .line 2246
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2256
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->setAllowLayoutDuringAnimation(Z)V

    .line 2257
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ah:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 2258
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i()V

    .line 2259
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->setAllowLayoutDuringAnimation(Z)V

    .line 2261
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->O:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v0, :cond_0

    cmpl-float v0, p3, v2

    if-nez v0, :cond_0

    .line 2262
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->O:Z

    .line 2263
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->P:Z

    .line 2264
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0, v3, v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setContent(Landroid/view/View;Landroid/view/View;)V

    .line 2267
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V

    .line 2272
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ag:Z

    if-eqz v0, :cond_2

    .line 2279
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 2280
    cmpl-float v1, p3, v2

    if-lez v1, :cond_1

    .line 2281
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object p2

    .line 2282
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ai:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p2, v0, :cond_2

    .line 2283
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ai:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 2284
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t()V

    .line 2287
    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2193
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ag:Z

    .line 2195
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ai:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_0

    .line 2196
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ai:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 2197
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t()V

    .line 2200
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v0, :cond_3

    .line 2205
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->setAllowLayoutDuringAnimation(Z)V

    .line 2206
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ah:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 2207
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i()V

    .line 2208
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->setAllowLayoutDuringAnimation(Z)V

    .line 2213
    :cond_1
    :goto_0
    invoke-direct {p0, p3}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 2214
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->p:Landroid/view/View;

    instance-of v4, v0, Lcom/google/android/apps/gmm/place/l;

    if-eqz v4, :cond_2

    check-cast v0, Lcom/google/android/apps/gmm/place/l;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/l;->ab_()Ljava/lang/CharSequence;

    move-result-object v4

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v0, :cond_7

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_5

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_5

    move v0, v2

    :goto_1
    if-eqz v0, :cond_7

    if-nez v4, :cond_6

    move-object v0, v1

    :goto_2
    new-instance v2, Lcom/google/android/apps/gmm/a/a/a;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/a/a/a;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v2}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    invoke-static {p0}, Lcom/google/android/apps/gmm/a/a/b;->a(Landroid/view/View;)V

    invoke-virtual {p0, v1}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 2215
    :cond_2
    :goto_3
    return-void

    .line 2209
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_4

    move v0, v2

    :goto_4
    if-eqz v0, :cond_1

    .line 2210
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/gmm/base/layout/v;->a()Lcom/google/android/apps/gmm/base/layout/v;

    move-result-object v4

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    move v0, v3

    .line 2209
    goto :goto_4

    :cond_5
    move v0, v3

    .line 2214
    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v5, Lcom/google/android/apps/gmm/l;->ai:I

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v4, v2, v3

    invoke-virtual {v0, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p3, v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p2, v0, :cond_8

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p2, v0, :cond_8

    move v0, v2

    :goto_5
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v2, Lcom/google/android/apps/gmm/l;->ah:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/a/a/a;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/a/a/a;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v2}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    invoke-static {p0}, Lcom/google/android/apps/gmm/a/a/b;->a(Landroid/view/View;)V

    invoke-virtual {p0, v1}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_3

    :cond_8
    move v0, v3

    goto :goto_5
.end method

.method a(Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;Lcom/google/android/apps/gmm/map/indoor/d/a;Lcom/google/android/apps/gmm/map/indoor/d/e;I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1614
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ae:Lcom/google/android/apps/gmm/base/l/r;

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/l/r;->d:Lcom/google/android/apps/gmm/map/indoor/d/a;

    if-eq v0, p2, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_4

    iput-object p2, v2, Lcom/google/android/apps/gmm/base/l/r;->d:Lcom/google/android/apps/gmm/map/indoor/d/a;

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/l/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/l/r;->d:Lcom/google/android/apps/gmm/map/indoor/d/a;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/l/r;->d:Lcom/google/android/apps/gmm/map/indoor/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/e;

    iget-object v4, v2, Lcom/google/android/apps/gmm/base/l/r;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/gmm/base/l/p;

    iget-object v6, v2, Lcom/google/android/apps/gmm/base/l/r;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v5, v6, v0, v2}, Lcom/google/android/apps/gmm/base/l/p;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/indoor/d/e;Lcom/google/android/apps/gmm/base/l/r;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/l/r;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/l/r;->d()V

    :cond_3
    if-nez p3, :cond_5

    move v0, v1

    :goto_2
    if-nez v0, :cond_4

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/l/r;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/indoor/a/a;->b()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/l/r;->a(Ljava/util/Set;)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/l/r;->c:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v0, v2}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 1617
    :cond_4
    return-void

    .line 1614
    :cond_5
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/l/r;->a(Lcom/google/android/apps/gmm/map/indoor/d/f;)Z

    move-result v0

    goto :goto_2
.end method

.method public final a(Ljava/util/List;Lcom/google/android/apps/gmm/base/activities/p;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/animation/Animator;",
            ">;",
            "Lcom/google/android/apps/gmm/base/activities/p;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1144
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    .line 1145
    iget v1, p2, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 1147
    iget-object v4, p2, Lcom/google/android/apps/gmm/base/activities/p;->f:Lcom/google/android/apps/gmm/base/l/ai;

    if-eqz v4, :cond_1

    .line 1149
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1203
    :cond_0
    :goto_0
    return-void

    .line 1153
    :cond_1
    if-ne v1, v2, :cond_3

    .line 1154
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    .line 1155
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBottom()I

    move-result v1

    .line 1156
    const-string v4, "translationY"

    new-array v2, v2, [F

    neg-int v1, v1

    int-to-float v1, v1

    aput v1, v2, v3

    .line 1157
    invoke-static {v0, v4, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1158
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->R:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1159
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1162
    :cond_2
    invoke-direct {p0, p2, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/activities/p;Z)V

    goto :goto_0

    .line 1164
    :cond_3
    if-ne v1, v7, :cond_0

    .line 1165
    invoke-direct {p0, p2, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/activities/p;Z)V

    .line 1166
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->D:Z

    if-eqz v1, :cond_5

    .line 1167
    iget-boolean v1, p2, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    if-eqz v1, :cond_5

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->J:Z

    .line 1171
    iget-object v1, p2, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 1172
    iget-object v4, p2, Lcom/google/android/apps/gmm/base/activities/p;->g:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget v1, v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->g:F

    iget v4, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->g:F

    cmpl-float v1, v1, v4

    if-lez v1, :cond_6

    move v1, v2

    .line 1173
    :goto_2
    if-nez v1, :cond_7

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->J:Z

    if-nez v1, :cond_7

    move v1, v2

    .line 1176
    :goto_3
    if-nez v1, :cond_8

    .line 1177
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1178
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBottom()I

    .line 1181
    :goto_4
    iget-object v1, p2, Lcom/google/android/apps/gmm/base/activities/p;->b:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    .line 1185
    const/4 v0, 0x0

    .line 1187
    if-eqz v1, :cond_4

    .line 1188
    iget v4, p2, Lcom/google/android/apps/gmm/base/activities/p;->e:I

    .line 1189
    if-ne v4, v2, :cond_9

    .line 1190
    invoke-virtual {v1, v5}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(F)Landroid/animation/Animator;

    move-result-object v0

    move v3, v2

    .line 1199
    :cond_4
    :goto_5
    if-eqz v0, :cond_0

    .line 1200
    if-eqz v3, :cond_a

    iput v5, v1, Lcom/google/android/apps/gmm/base/views/FloatingBar;->P:F

    goto :goto_0

    :cond_5
    move v1, v3

    .line 1167
    goto :goto_1

    :cond_6
    move v1, v3

    .line 1172
    goto :goto_2

    :cond_7
    move v1, v3

    .line 1173
    goto :goto_3

    .line 1180
    :cond_8
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_4

    .line 1192
    :cond_9
    if-ne v4, v7, :cond_4

    .line 1193
    invoke-virtual {v1, v6}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(F)Landroid/animation/Animator;

    move-result-object v0

    goto :goto_5

    .line 1200
    :cond_a
    iput v6, v1, Lcom/google/android/apps/gmm/base/views/FloatingBar;->P:F

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/activities/p;)Z
    .locals 5
    .param p1    # Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1295
    if-eqz p2, :cond_0

    iget-object v2, p2, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 1313
    :cond_1
    :goto_0
    return v0

    .line 1298
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->G:Z

    if-eqz v2, :cond_3

    .line 1299
    iget-object v2, p2, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    sget-object v3, Lcom/google/android/apps/gmm/base/activities/ac;->b:Lcom/google/android/apps/gmm/base/activities/ac;

    if-eq v2, v3, :cond_1

    .line 1304
    :cond_3
    iget-object v2, p2, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    sget-object v3, Lcom/google/android/apps/gmm/base/activities/ac;->f:Lcom/google/android/apps/gmm/base/activities/ac;

    if-eq v2, v3, :cond_1

    .line 1305
    iget-object v2, p2, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    sget-object v3, Lcom/google/android/apps/gmm/base/activities/ac;->e:Lcom/google/android/apps/gmm/base/activities/ac;

    if-eq v2, v3, :cond_1

    .line 1310
    iget-object v2, p2, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    sget-object v3, Lcom/google/android/apps/gmm/base/activities/ac;->d:Lcom/google/android/apps/gmm/base/activities/ac;

    if-ne v2, v3, :cond_4

    move v3, v0

    .line 1311
    :goto_1
    if-eqz p1, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    .line 1312
    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/p;->g:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 1311
    iget v4, p1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->g:F

    iget v2, v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->g:F

    cmpl-float v2, v4, v2

    if-lez v2, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    move v2, v0

    .line 1313
    :goto_3
    if-nez v2, :cond_7

    :goto_4
    xor-int/2addr v0, v3

    goto :goto_0

    :cond_4
    move v3, v1

    .line 1310
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1311
    goto :goto_2

    :cond_6
    move v2, v1

    goto :goto_3

    :cond_7
    move v0, v1

    .line 1313
    goto :goto_4
.end method

.method public bridge synthetic addView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic addView(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 103
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public bridge synthetic addView(Landroid/view/View;II)V
    .locals 0

    .prologue
    .line 103
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->addView(Landroid/view/View;II)V

    return-void
.end method

.method public bridge synthetic addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 103
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public bridge synthetic addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 103
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final b()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x4

    const/4 v1, 0x0

    .line 1530
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/indoor/a/a;->a()Lcom/google/android/apps/gmm/map/indoor/d/a;

    move-result-object v3

    .line 1531
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/indoor/a/a;->a(Lcom/google/android/apps/gmm/map/indoor/d/a;)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v4

    .line 1536
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v8

    .line 1538
    if-eqz v3, :cond_5

    .line 1539
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v5, 0x2

    if-lt v0, v5, :cond_4

    move v0, v7

    :goto_0
    if-eqz v0, :cond_5

    move v5, v1

    .line 1542
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v9

    .line 1546
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    sget v10, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v10}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    .line 1552
    sget v10, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v10}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v10, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v10, :cond_6

    sget-object v10, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v10, :cond_6

    move v0, v7

    :goto_2
    if-eqz v0, :cond_7

    move v0, v6

    .line 1562
    :goto_3
    if-ne v9, v5, :cond_0

    if-eq v8, v0, :cond_1

    :cond_0
    move v1, v7

    .line 1566
    :cond_1
    if-ne v8, v6, :cond_9

    if-nez v0, :cond_9

    .line 1569
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v6, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->aa:Landroid/view/animation/Animation;

    new-instance v0, Lcom/google/android/apps/gmm/base/layout/d;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/layout/d;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;Lcom/google/android/apps/gmm/map/indoor/d/a;Lcom/google/android/apps/gmm/map/indoor/d/e;I)V

    invoke-virtual {v6}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v6}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/animation/Animation;->cancel()V

    :cond_2
    invoke-virtual {v7, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v6, v7}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1610
    :cond_3
    :goto_4
    return-void

    :cond_4
    move v0, v1

    .line 1539
    goto :goto_0

    :cond_5
    move v5, v6

    goto :goto_1

    :cond_6
    move v0, v1

    .line 1552
    goto :goto_2

    .line 1558
    :cond_7
    if-nez v5, :cond_8

    move v0, v1

    goto :goto_3

    :cond_8
    move v0, v6

    goto :goto_3

    .line 1587
    :cond_9
    if-nez v8, :cond_b

    if-ne v0, v6, :cond_b

    .line 1590
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->n:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v6, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ab:Landroid/view/animation/Animation;

    new-instance v0, Lcom/google/android/apps/gmm/base/layout/e;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/layout/e;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;Lcom/google/android/apps/gmm/map/indoor/d/a;Lcom/google/android/apps/gmm/map/indoor/d/e;I)V

    invoke-virtual {v6}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v6}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/animation/Animation;->cancel()V

    :cond_a
    invoke-virtual {v7, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v6, v7}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_4

    .line 1605
    :cond_b
    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;Lcom/google/android/apps/gmm/map/indoor/d/a;Lcom/google/android/apps/gmm/map/indoor/d/e;I)V

    .line 1606
    if-eqz v1, :cond_3

    .line 1607
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->requestLayout()V

    goto :goto_4
.end method

.method public final b(Lcom/google/android/apps/gmm/base/activities/p;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1910
    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/p;->t:Lcom/google/android/apps/gmm/base/activities/a/a;

    .line 1913
    sget-object v0, Lcom/google/android/apps/gmm/base/activities/a/a;->b:Lcom/google/android/apps/gmm/base/activities/a/a;

    if-ne v3, v0, :cond_1

    .line 1914
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->x:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/d;->am:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1920
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_5

    .line 1921
    sget-object v0, Lcom/google/android/apps/gmm/base/activities/a/a;->a:Lcom/google/android/apps/gmm/base/activities/a/a;

    if-eq v3, v0, :cond_3

    move v0, v1

    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->x:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    const-string v4, "alpha"

    new-array v5, v1, [F

    if-eqz v0, :cond_4

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_3
    aput v1, v5, v2

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/base/layout/i;

    invoke-direct {v2, p0, v3, v0}, Lcom/google/android/apps/gmm/base/layout/i;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;Landroid/view/View;Z)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1922
    sget v0, Lcom/google/android/apps/gmm/base/activities/p;->a:I

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1924
    invoke-virtual {v1}, Landroid/animation/Animator;->start()V

    .line 1928
    :goto_4
    return-void

    .line 1915
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/base/activities/a/a;->c:Lcom/google/android/apps/gmm/base/activities/a/a;

    if-ne v3, v0, :cond_0

    .line 1916
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->x:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/d;->P:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1920
    goto :goto_1

    :cond_3
    move v0, v2

    .line 1921
    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    .line 1926
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->x:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method

.method public final b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 1

    .prologue
    .line 2250
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ag:Z

    .line 2251
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->af:Lcom/google/android/apps/gmm/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/util/a/e;->a()V

    .line 2252
    return-void
.end method

.method public b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V
    .locals 5

    .prologue
    .line 2802
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v0, :cond_0

    move v1, p3

    .line 2803
    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(F)F

    move-result v3

    .line 2805
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 2806
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->y:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/layout/y;

    .line 2807
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    neg-float v4, v3

    invoke-virtual {v0, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 2805
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2802
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    move v1, v0

    goto :goto_0

    .line 2809
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->h:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    neg-float v2, v3

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 2813
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->P:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->F:Z

    if-nez v0, :cond_2

    .line 2814
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 2815
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v0

    aget v0, v2, v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    .line 2817
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2816
    invoke-static {v1, p1, p2, p3}, Lcom/google/android/apps/gmm/base/l/l;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)F

    move-result v1

    sub-float/2addr v0, v1

    .line 2820
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x4

    .line 2819
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 2821
    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v1, v0

    .line 2837
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->m:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/FrameLayout;

    neg-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    .line 2839
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->j()V

    .line 2840
    return-void

    .line 2822
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->s:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 2823
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->s:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/FrameLayout;

    neg-float v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    .line 2826
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->s:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 2827
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v1, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->s:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getScaleY()F

    move-result v0

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    move v1, v0

    goto :goto_2

    .line 2828
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->h:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 2835
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->h:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v3

    move v1, v0

    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_2
.end method

.method public final b(Ljava/util/List;Lcom/google/android/apps/gmm/base/activities/p;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/animation/Animator;",
            ">;",
            "Lcom/google/android/apps/gmm/base/activities/p;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1804
    iget-object v3, p2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    .line 1806
    if-nez v3, :cond_4

    .line 1807
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1808
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v12, :cond_3

    .line 1810
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->B:Lcom/google/android/apps/gmm/base/activities/z;

    .line 1811
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v8, :cond_1

    :goto_0
    if-nez v8, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/z;->a:Lcom/google/android/apps/gmm/base/activities/z;

    .line 1812
    :cond_0
    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/z;->b(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v2

    .line 1813
    invoke-virtual {v2, v0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 1815
    invoke-virtual {v0, v11, v10}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1816
    new-instance v3, Lcom/google/android/apps/gmm/base/layout/g;

    invoke-direct {v3, p0, v0, p2, v1}, Lcom/google/android/apps/gmm/base/layout/g;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;Landroid/view/View;Lcom/google/android/apps/gmm/base/activities/p;Lcom/google/android/apps/gmm/base/activities/z;)V

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1835
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1836
    iput-boolean v9, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->C:Z

    .line 1837
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i()V

    .line 1889
    :goto_2
    iget-object v0, p2, Lcom/google/android/apps/gmm/base/activities/p;->u:Lcom/google/android/apps/gmm/base/activities/z;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->B:Lcom/google/android/apps/gmm/base/activities/z;

    .line 1890
    return-void

    :cond_1
    move v8, v9

    .line 1811
    goto :goto_0

    :cond_2
    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/z;->b:Lcom/google/android/apps/gmm/base/activities/z;

    goto :goto_1

    .line 1841
    :cond_3
    invoke-direct {p0, p2, v10}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/activities/p;Landroid/view/View;)Z

    goto :goto_2

    .line 1847
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    move v7, v8

    .line 1849
    :goto_3
    invoke-direct {p0, p2, v3}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/activities/p;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1850
    iget-object v6, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    iget v0, p2, Lcom/google/android/apps/gmm/base/activities/p;->P:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/google/android/apps/gmm/base/activities/p;->P:I

    new-instance v1, Lcom/google/android/apps/gmm/base/activities/t;

    const-wide/16 v4, 0x3e8

    move-object v2, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/base/activities/t;-><init>(Lcom/google/android/apps/gmm/base/activities/p;Landroid/view/View;JLcom/google/android/apps/gmm/base/activities/c;)V

    .line 1854
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1860
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->D:Z

    if-eqz v1, :cond_7

    iget-boolean v1, p2, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    if-eqz v1, :cond_7

    move v1, v8

    :goto_4
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->E:Z

    .line 1861
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->s()V

    .line 1864
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v12}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1866
    if-eqz v7, :cond_8

    .line 1867
    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_6
    move v7, v9

    .line 1847
    goto :goto_3

    :cond_7
    move v1, v9

    .line 1860
    goto :goto_4

    .line 1871
    :cond_8
    iget-object v1, p2, Lcom/google/android/apps/gmm/base/activities/p;->u:Lcom/google/android/apps/gmm/base/activities/z;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v8, :cond_a

    :goto_5
    if-nez v8, :cond_b

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/z;->a:Lcom/google/android/apps/gmm/base/activities/z;

    .line 1872
    :cond_9
    :goto_6
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/z;->a(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v1

    .line 1873
    invoke-virtual {v1, v0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 1875
    invoke-virtual {v0, v11, v10}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1876
    new-instance v2, Lcom/google/android/apps/gmm/base/layout/h;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/gmm/base/layout/h;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1886
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_a
    move v8, v9

    .line 1871
    goto :goto_5

    :cond_b
    if-nez v1, :cond_9

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/z;->b:Lcom/google/android/apps/gmm/base/activities/z;

    goto :goto_6
.end method

.method protected final c()V
    .locals 4

    .prologue
    .line 1758
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->z:Lcom/google/android/apps/gmm/base/activities/x;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1762
    :goto_0
    if-eqz v0, :cond_1

    .line 1764
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/e;->bd:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 1765
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->e:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    .line 1766
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v3

    .line 1767
    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    float-to-int v0, v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    .line 1768
    sub-int/2addr v0, v2

    .line 1773
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->e:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 1774
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->e:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1776
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->j()V

    .line 1777
    return-void

    .line 1758
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->z:Lcom/google/android/apps/gmm/base/activities/x;

    .line 1761
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/x;->a:Landroid/view/View;

    goto :goto_0

    .line 1770
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v0

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_1
.end method

.method public final c(Lcom/google/android/apps/gmm/base/activities/p;)V
    .locals 0

    .prologue
    .line 2079
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    .line 2080
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i()V

    .line 2081
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 2016
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->l()Landroid/widget/EditText;

    move-result-object v1

    .line 2017
    if-nez v1, :cond_0

    .line 2024
    :goto_0
    return-void

    .line 2020
    :cond_0
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 2021
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2023
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0
.end method

.method public final d(Lcom/google/android/apps/gmm/base/activities/p;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2085
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->k()V

    .line 2088
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 2089
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    .line 2091
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/p;->m:Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    if-eqz v1, :cond_0

    .line 2092
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/p;->m:Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;)V

    .line 2096
    :cond_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/p;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 2097
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    if-eqz v3, :cond_1

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    if-eqz v3, :cond_1

    .line 2099
    iget-boolean v1, p1, Lcom/google/android/apps/gmm/base/activities/p;->h:Z

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 2104
    :cond_1
    :goto_0
    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/p;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 2103
    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingStateTransition(Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;)V

    .line 2108
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v1

    .line 2109
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ai:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 2111
    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-nez v3, :cond_2

    .line 2113
    if-eqz v2, :cond_5

    .line 2115
    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingState(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Z)V

    .line 2116
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ah:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 2122
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t()V

    .line 2124
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->r()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2126
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->w:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2128
    :cond_3
    return-void

    .line 2099
    :cond_4
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->g:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    goto :goto_0

    .line 2118
    :cond_5
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingState(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Z)V

    .line 2119
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V

    goto :goto_1
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 2030
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->l()Landroid/widget/EditText;

    move-result-object v1

    .line 2031
    if-nez v1, :cond_0

    .line 2039
    :goto_0
    return-void

    .line 2034
    :cond_0
    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    .line 2035
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2037
    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method public final e(Lcom/google/android/apps/gmm/base/activities/p;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2133
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2134
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2135
    sget v3, Lcom/google/android/apps/gmm/g;->aG:I

    .line 2136
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2137
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2139
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2140
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->C:Z

    .line 2141
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i()V

    .line 2144
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/MapFragment;->getView()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/view/SurfaceView;

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2145
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->a(Landroid/view/View;)V

    .line 2149
    :cond_0
    iget v0, p1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    .line 2150
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 2153
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2154
    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/activities/p;Z)V

    .line 2157
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 2160
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0, v5, v5}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setContent(Landroid/view/View;Landroid/view/View;)V

    .line 2161
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->P:Z

    .line 2164
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->r()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2165
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->w:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2167
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 2144
    goto :goto_0
.end method

.method public final f()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 2300
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Z)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public final g()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 2312
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Z)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public gatherTransparentRegion(Landroid/graphics/Region;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 518
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/MapFragment;->getView()Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Landroid/view/SurfaceView;

    if-eqz v1, :cond_0

    move v1, v0

    :goto_0
    if-eqz v1, :cond_1

    .line 520
    invoke-virtual {p1}, Landroid/graphics/Region;->setEmpty()V

    .line 523
    :goto_1
    return v0

    .line 518
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 523
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->gatherTransparentRegion(Landroid/graphics/Region;)Z

    move-result v0

    goto :goto_1
.end method

.method public final h()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2429
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->m()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2430
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 2433
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    sget-object v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 2434
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v0

    if-ne v4, v0, :cond_0

    move v0, v1

    .line 2435
    :goto_0
    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v3, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v3, v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    .line 2436
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/d;->a:[I

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_2
    move v0, v1

    .line 2440
    :goto_3
    return v0

    :cond_0
    move v0, v2

    .line 2434
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2435
    goto :goto_1

    .line 2436
    :pswitch_0
    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingState(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Z)V

    goto :goto_2

    :cond_2
    move v0, v2

    .line 2440
    goto :goto_3

    .line 2436
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected final i()V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2468
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    .line 2469
    if-nez v4, :cond_0

    .line 2500
    :goto_0
    return-void

    .line 2473
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ad:Z

    if-eqz v0, :cond_2

    .line 2474
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->T:Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    if-nez v0, :cond_4

    move v0, v2

    :goto_1
    if-eqz v0, :cond_8

    .line 2475
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->w:Z

    if-eqz v0, :cond_2

    .line 2476
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->T:Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->a(Lcom/google/android/apps/gmm/map/MapFragment;)V

    .line 2483
    :cond_2
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->o()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->T:Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    if-nez v0, :cond_9

    move v0, v1

    .line 2484
    :goto_3
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ac:Lcom/google/android/apps/gmm/base/layout/w;

    sget-object v6, Lcom/google/android/apps/gmm/base/layout/w;->a:Lcom/google/android/apps/gmm/base/layout/w;

    if-eq v5, v6, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ac:Lcom/google/android/apps/gmm/base/layout/w;

    sget-object v6, Lcom/google/android/apps/gmm/base/layout/w;->c:Lcom/google/android/apps/gmm/base/layout/w;

    if-ne v5, v6, :cond_a

    if-eqz v0, :cond_a

    .line 2486
    :cond_3
    :goto_4
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/o;->g(Z)V

    .line 2488
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->o()Z

    move-result v1

    if-eqz v1, :cond_b

    const/4 v1, 0x4

    :goto_5
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->setVisibility(I)V

    .line 2492
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-nez v0, :cond_c

    .line 2493
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(I)V

    goto :goto_0

    .line 2474
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-eqz v0, :cond_5

    move v0, v1

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v5, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;

    if-eqz v5, :cond_7

    check-cast v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getView()Landroid/view/View;

    move-result-object v5

    new-instance v6, Lcom/google/android/apps/gmm/base/fragments/e;

    invoke-direct {v6, v0}, Lcom/google/android/apps/gmm/base/fragments/e;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;)V

    invoke-static {v5, v6}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/view/View;Lcom/google/b/a/ar;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    if-eqz v0, :cond_7

    move v0, v1

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_6

    :cond_7
    move v0, v2

    goto :goto_1

    .line 2479
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->a(Lcom/google/android/apps/gmm/map/MapFragment;)V

    goto :goto_2

    :cond_9
    move v0, v2

    .line 2483
    goto :goto_3

    :cond_a
    move v1, v2

    .line 2484
    goto :goto_4

    :cond_b
    move v1, v2

    .line 2488
    goto :goto_5

    .line 2494
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_d

    .line 2495
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(I)V

    goto/16 :goto_0

    .line 2497
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/p;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2498
    :goto_7
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(I)V

    goto/16 :goto_0

    :cond_e
    move v2, v3

    .line 2497
    goto :goto_7
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 2599
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->onAttachedToWindow()V

    .line 2600
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->al:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 2602
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ae:Lcom/google/android/apps/gmm/base/l/r;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 2605
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 2606
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2672
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2673
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 2674
    :goto_1
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->D:Z

    if-ne v3, v0, :cond_2

    .line 2704
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 2673
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 2677
    :cond_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->D:Z

    .line 2678
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v3, :cond_a

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    .line 2679
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    if-eqz v0, :cond_a

    .line 2680
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->E:Z

    .line 2681
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->F:Z

    .line 2682
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    if-eqz v0, :cond_6

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->G:Z

    .line 2683
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->z:Landroid/view/View;

    if-eqz v0, :cond_7

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->H:Z

    .line 2684
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->A:Landroid/view/View;

    if-eqz v0, :cond_8

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->I:Z

    .line 2685
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    .line 2686
    iget v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_9

    :goto_8
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->J:Z

    .line 2695
    :goto_9
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->s()V

    .line 2696
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->k()V

    .line 2697
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->p()V

    .line 2698
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->w:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->r()Z

    move-result v1

    if-eqz v1, :cond_b

    :goto_a
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2699
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    if-eqz v0, :cond_3

    .line 2700
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 2703
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t()V

    goto :goto_2

    :cond_4
    move v0, v2

    .line 2680
    goto :goto_3

    :cond_5
    move v0, v2

    .line 2681
    goto :goto_4

    :cond_6
    move v0, v2

    .line 2682
    goto :goto_5

    :cond_7
    move v0, v2

    .line 2683
    goto :goto_6

    :cond_8
    move v0, v2

    .line 2684
    goto :goto_7

    :cond_9
    move v1, v2

    .line 2686
    goto :goto_8

    .line 2688
    :cond_a
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->E:Z

    .line 2689
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->F:Z

    .line 2690
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->G:Z

    .line 2691
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->H:Z

    .line 2692
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->I:Z

    .line 2693
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->J:Z

    goto :goto_9

    .line 2698
    :cond_b
    const/16 v2, 0x8

    goto :goto_a
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 2610
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 2612
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ae:Lcom/google/android/apps/gmm/base/l/r;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 2616
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->al:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 2617
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->T:Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    .line 2618
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->onDetachedFromWindow()V

    .line 2619
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 529
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->onFinishInflate()V

    .line 532
    new-instance v0, Lcom/google/android/apps/gmm/base/layout/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/layout/j;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;)V

    new-instance v1, Lcom/google/android/apps/gmm/base/l/r;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/base/l/r;-><init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/ag;)V

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ae:Lcom/google/android/apps/gmm/base/l/r;

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/base/f/b;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->s:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 546
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->p()V

    .line 548
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    sget v1, Lcom/google/android/apps/gmm/f;->bd:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->q:Landroid/graphics/drawable/Drawable;

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;)V

    .line 550
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->o:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 552
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 553
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->l:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 554
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->x:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/gmm/base/layout/r;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/base/layout/r;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 562
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->y:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v2, 0x0

    .line 563
    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    .line 564
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/high16 v5, -0x40800000    # -1.0f

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1012
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->onLayout(ZIIII)V

    .line 1013
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->c()V

    .line 1014
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-nez v0, :cond_1

    .line 1027
    :cond_0
    :goto_0
    return-void

    .line 1018
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 1019
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a:Lcom/google/b/c/cv;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 1020
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 1021
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a:Lcom/google/b/c/cv;

    sget-object v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 1022
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    if-eqz v3, :cond_0

    .line 1023
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_2
    if-nez v0, :cond_3

    if-eqz v4, :cond_0

    .line 1027
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->h:Z

    if-eqz v0, :cond_6

    sget-object v0, Lcom/google/android/apps/gmm/base/layout/x;->b:Lcom/google/android/apps/gmm/base/layout/x;

    :goto_3
    sget-object v3, Lcom/google/android/apps/gmm/base/layout/n;->c:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/layout/x;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported SliderSizing option."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v2

    .line 1022
    goto :goto_1

    :cond_5
    move v0, v2

    .line 1023
    goto :goto_2

    .line 1027
    :cond_6
    sget-object v0, Lcom/google/android/apps/gmm/base/layout/x;->a:Lcom/google/android/apps/gmm/base/layout/x;

    goto :goto_3

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3fe38e39

    div-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getHeight()I

    move-result v1

    sub-int/2addr v1, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->l:[F

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v4

    aput v5, v3, v4

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExposurePixelsInternal(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;I)V

    goto/16 :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v1, :cond_7

    move v3, v1

    :goto_4
    if-nez v3, :cond_8

    :goto_5
    if-nez v4, :cond_9

    if-eqz v1, :cond_9

    const/high16 v1, 0x40a00000    # 5.0f

    :goto_6
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v1

    float-to-int v1, v1

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getHeight()I

    move-result v3

    sub-int v1, v3, v1

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->l:[F

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v4

    aput v5, v3, v4

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExposurePixelsInternal(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;I)V

    goto/16 :goto_0

    :cond_7
    move v3, v2

    goto :goto_4

    :cond_8
    move v1, v2

    goto :goto_5

    :cond_9
    const/high16 v1, 0x40000000    # 2.0f

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 27

    .prologue
    .line 568
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    if-ne v3, v4, :cond_0

    const/4 v3, 0x1

    :goto_0
    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 569
    :cond_1
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    if-ne v3, v4, :cond_2

    const/4 v3, 0x1

    :goto_1
    if-nez v3, :cond_3

    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 571
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_f

    const/4 v3, 0x1

    move v4, v3

    .line 572
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v9

    .line 574
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 575
    sget v3, Lcom/google/android/apps/gmm/e;->bp:I

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    div-int/lit8 v11, v3, 0x2

    .line 576
    if-eqz v4, :cond_10

    sget v3, Lcom/google/android/apps/gmm/e;->M:I

    :goto_3
    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v12

    .line 580
    sget v3, Lcom/google/android/apps/gmm/e;->au:I

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v13

    .line 581
    sget v3, Lcom/google/android/apps/gmm/e;->av:I

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v14

    .line 582
    sget v3, Lcom/google/android/apps/gmm/e;->bk:I

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    .line 583
    sget v3, Lcom/google/android/apps/gmm/e;->bo:I

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 584
    sget v3, Lcom/google/android/apps/gmm/e;->aE:I

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 585
    sget v3, Lcom/google/android/apps/gmm/e;->aC:I

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 586
    sget v3, Lcom/google/android/apps/gmm/e;->bv:I

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    .line 588
    sget v3, Lcom/google/android/apps/gmm/e;->aw:I

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 589
    sget v3, Lcom/google/android/apps/gmm/e;->ax:I

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v20

    .line 590
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/base/f/b;->a(Landroid/content/Context;)I

    move-result v21

    .line 591
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 592
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v22

    .line 593
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v7, v1}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->setMeasuredDimension(II)V

    .line 595
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    if-eqz v3, :cond_4

    .line 597
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, 0x40000000    # 2.0f

    move/from16 v0, v22

    invoke-virtual {v3, v7, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 598
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v5, 0x0

    const/4 v8, 0x0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Rect;->width()I

    move-result v24

    add-int v24, v24, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v5, v8, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 601
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, 0x40000000    # 2.0f

    move/from16 v0, v22

    invoke-virtual {v3, v7, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 602
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v5, 0x0

    const/4 v8, 0x0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Rect;->width()I

    move-result v24

    add-int v24, v24, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v5, v8, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 604
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;)Z

    move-result v23

    .line 610
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->F:Z

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, 0x40000000    # 2.0f

    move/from16 v0, v22

    invoke-virtual {v3, v6, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 612
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/base/activities/c;

    sget v5, Lcom/google/android/apps/gmm/g;->aD:I

    invoke-virtual {v3, v5}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    if-nez v3, :cond_12

    const/4 v3, 0x0

    :goto_5
    check-cast v3, Lcom/google/android/apps/gmm/base/views/FloatingBar;

    .line 613
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->J:Z

    if-eqz v5, :cond_13

    .line 614
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v6, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 621
    :goto_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v5, 0x0

    const/4 v8, 0x0

    if-eqz v23, :cond_16

    sub-int v5, v7, v5

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Rect;->width()I

    move-result v24

    sub-int v5, v5, v24

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v25

    add-int v25, v25, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v5, v8, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 623
    :goto_7
    if-eqz v4, :cond_17

    .line 624
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v7, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 639
    :goto_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v5, 0x0

    const/4 v8, 0x0

    if-eqz v23, :cond_1b

    sub-int v5, v7, v5

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Rect;->width()I

    move-result v24

    sub-int v5, v5, v24

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v25

    add-int v25, v25, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v5, v8, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 641
    :goto_9
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->G:Z

    if-eqz v3, :cond_1c

    .line 642
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->c:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v6, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 643
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->d:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v6, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 649
    :goto_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->c:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v5, 0x0

    const/4 v8, 0x0

    if-eqz v23, :cond_1d

    sub-int v5, v7, v5

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Rect;->width()I

    move-result v24

    sub-int v5, v5, v24

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v25

    add-int v25, v25, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v5, v8, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 655
    :goto_b
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->H:Z

    if-eqz v3, :cond_1e

    .line 656
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->d:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v6, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 661
    :goto_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->d:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v5, 0x0

    const/4 v8, 0x0

    if-eqz v23, :cond_1f

    sub-int v5, v7, v5

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Rect;->width()I

    move-result v24

    sub-int v5, v5, v24

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v25

    add-int v25, v25, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v5, v8, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 663
    :goto_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->g:Lcom/google/android/apps/gmm/base/layout/y;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->I:Z

    if-eqz v3, :cond_20

    move v3, v6

    :goto_e
    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v24, -0x80000000

    move/from16 v0, v22

    move/from16 v1, v24

    invoke-virtual {v5, v3, v8, v0, v1}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 666
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->g:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->g:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v8, v8, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    sub-int v8, v22, v8

    if-eqz v23, :cond_21

    sub-int v5, v7, v5

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Rect;->width()I

    move-result v24

    sub-int v5, v5, v24

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v25

    add-int v25, v25, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v5, v8, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 668
    :goto_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->q:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, 0x40000000    # 2.0f

    move/from16 v0, v22

    invoke-virtual {v3, v7, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 669
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->q:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v5, 0x0

    const/4 v8, 0x0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v25

    add-int v25, v25, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v5, v8, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 671
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->o:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, 0x40000000    # 2.0f

    move/from16 v0, v22

    invoke-virtual {v3, v7, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 673
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->o:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v5, 0x0

    const/4 v8, 0x0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v25

    add-int v25, v25, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v5, v8, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 676
    const/4 v3, 0x0

    .line 684
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    if-nez v5, :cond_22

    :cond_5
    const/4 v5, 0x0

    :goto_10
    if-eqz v5, :cond_6

    .line 685
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 688
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v5, :cond_37

    .line 689
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget v5, v5, Lcom/google/android/apps/gmm/base/activities/p;->v:I

    sub-int/2addr v3, v5

    move v5, v3

    .line 691
    :goto_11
    sub-int v24, v22, v5

    .line 696
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->p:Landroid/view/View;

    .line 697
    const/4 v8, 0x0

    .line 698
    instance-of v0, v3, Lcom/google/android/apps/gmm/place/l;

    move/from16 v25, v0

    if-eqz v25, :cond_36

    .line 699
    check-cast v3, Lcom/google/android/apps/gmm/place/l;

    .line 700
    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/l;->c()I

    move-result v8

    .line 701
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->d:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v3

    if-lez v3, :cond_24

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->d:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v3

    .line 702
    :goto_12
    sub-int v3, v8, v3

    add-int/lit8 v3, v3, 0x0

    .line 703
    if-gez v3, :cond_7

    .line 704
    const/4 v3, 0x0

    .line 707
    :cond_7
    :goto_13
    add-int v8, v24, v3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->F:Z

    move/from16 v24, v0

    if-eqz v24, :cond_26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    move-object/from16 v24, v0

    const/high16 v25, 0x40000000    # 2.0f

    const/high16 v26, 0x40000000    # 2.0f

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v6, v1, v8, v2}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 709
    :goto_14
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    const/16 v24, 0x0

    sub-int v3, v5, v3

    if-eqz v23, :cond_27

    sub-int v5, v7, v24

    iget-object v0, v8, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Rect;->width()I

    move-result v24

    sub-int v5, v5, v24

    iget-object v0, v8, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    iget-object v0, v8, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v25

    add-int v25, v25, v5

    iget-object v8, v8, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    add-int/2addr v8, v3

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v5, v3, v1, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 711
    :goto_15
    if-nez v9, :cond_8

    .line 712
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->l:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, -0x80000000

    const/high16 v8, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v7, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 713
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->l:Lcom/google/android/apps/gmm/base/layout/y;

    sub-int v5, v22, v14

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->l:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v8, v8, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 714
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    sub-int/2addr v5, v8

    .line 713
    if-eqz v23, :cond_28

    sub-int v8, v7, v14

    iget-object v14, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v14

    sub-int/2addr v8, v14

    iget-object v14, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Rect;->width()I

    move-result v24

    add-int v24, v24, v8

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v5

    move/from16 v0, v24

    invoke-virtual {v14, v8, v5, v0, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 717
    :cond_8
    :goto_16
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(F)F

    move-result v3

    float-to-int v3, v3

    .line 718
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->h:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v14, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v5, v7, v8, v0, v14}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 719
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->h:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->h:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v14, v14, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 720
    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v14

    sub-int v14, v22, v14

    sub-int v3, v14, v3

    .line 719
    iget-object v14, v5, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v0, v5, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Rect;->width()I

    move-result v24

    add-int v24, v24, v8

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    add-int/2addr v5, v3

    move/from16 v0, v24

    invoke-virtual {v14, v8, v3, v0, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 722
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->s:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, -0x80000000

    const/high16 v8, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v7, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 723
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->s:Lcom/google/android/apps/gmm/base/layout/y;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->s:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 724
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    sub-int v5, v7, v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->s:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v8, v8, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 725
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    sub-int v8, v22, v8

    .line 723
    if-eqz v23, :cond_29

    sub-int v5, v7, v5

    iget-object v14, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v14

    sub-int/2addr v5, v14

    iget-object v14, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Rect;->width()I

    move-result v24

    add-int v24, v24, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move/from16 v0, v24

    invoke-virtual {v14, v5, v8, v0, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 730
    :goto_17
    mul-int/lit8 v3, v21, 0x2

    sub-int v3, v7, v3

    move/from16 v0, v18

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 732
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->m:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v8, -0x80000000

    const/high16 v14, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v5, v3, v8, v0, v14}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 734
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->m:Lcom/google/android/apps/gmm/base/layout/y;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->m:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 735
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    sub-int v5, v7, v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->m:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v8, v8, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 736
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    sub-int v8, v22, v8

    .line 734
    if-eqz v23, :cond_2a

    sub-int v5, v7, v5

    iget-object v14, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v14

    sub-int/2addr v5, v14

    iget-object v14, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v18

    add-int v18, v18, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move/from16 v0, v18

    invoke-virtual {v14, v5, v8, v0, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 739
    :goto_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->m:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v3, Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v3

    .line 742
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/google/android/apps/gmm/base/f/b;->a(Landroid/content/Context;I)I

    move-result v3

    sub-int v3, v7, v3

    .line 745
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->e:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v8, -0x80000000

    const/high16 v14, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v5, v7, v8, v0, v14}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 746
    if-eqz v4, :cond_2c

    .line 747
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->e:Lcom/google/android/apps/gmm/base/layout/y;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->e:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 748
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v3, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v12

    .line 747
    if-eqz v23, :cond_2b

    sub-int v3, v7, v3

    iget-object v8, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v3, v8

    iget-object v8, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v12, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v12

    add-int/2addr v12, v3

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v5

    invoke-virtual {v8, v3, v5, v12, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 756
    :goto_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->p:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v5, 0x40000000    # 2.0f

    move/from16 v0, v17

    invoke-virtual {v3, v7, v4, v0, v5}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 757
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->p:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v4, 0x0

    iget-object v5, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v8, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    add-int/2addr v8, v4

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int v3, v3, v22

    move/from16 v0, v22

    invoke-virtual {v5, v4, v0, v8, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 758
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->j()V

    .line 760
    const/4 v4, 0x0

    .line 761
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->h:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v3

    if-nez v3, :cond_35

    .line 762
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->h:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    .line 766
    :goto_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->j:Lcom/google/android/apps/gmm/base/layout/y;

    add-int v5, v15, v16

    const/high16 v8, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->m:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v12, v12, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 767
    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v12

    const/high16 v14, 0x40000000    # 2.0f

    .line 766
    invoke-virtual {v4, v5, v8, v12, v14}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 768
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->j:Lcom/google/android/apps/gmm/base/layout/y;

    sub-int v5, v7, v21

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->m:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v8, v8, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v5, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->j:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v8, v8, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 769
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v5, v8

    sub-int v8, v22, v20

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->j:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v12, v12, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 770
    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v12

    sub-int/2addr v8, v12

    sub-int v3, v8, v3

    .line 768
    if-eqz v23, :cond_2e

    sub-int v5, v7, v5

    iget-object v8, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v5, v8

    iget-object v8, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v12, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v12

    add-int/2addr v12, v5

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {v8, v5, v3, v12, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 773
    :goto_1b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->m:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    shl-int/lit8 v4, v11, 0x1

    add-int v4, v4, v19

    sub-int/2addr v4, v13

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->j:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    if-lt v7, v3, :cond_2f

    const/4 v3, 0x1

    .line 775
    :goto_1c
    if-eqz v3, :cond_30

    .line 776
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->j:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v3, Lcom/google/android/apps/gmm/base/views/ScalebarView;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/google/android/apps/gmm/base/views/ScalebarView;->l:Z

    .line 781
    :goto_1d
    if-eqz v9, :cond_9

    .line 782
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->u:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v4, -0x80000000

    const/high16 v5, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v7, v4, v0, v5}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 783
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->u:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->u:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int v5, v22, v5

    if-eqz v23, :cond_31

    sub-int v4, v7, v4

    iget-object v8, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v4, v8

    iget-object v8, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v9, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    add-int/2addr v9, v4

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v5

    invoke-virtual {v8, v4, v5, v9, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 785
    :goto_1e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->v:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v4, -0x80000000

    const/high16 v5, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v7, v4, v0, v5}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 786
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->v:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->v:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int v5, v22, v5

    if-eqz v23, :cond_32

    sub-int v4, v7, v4

    iget-object v8, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v4, v8

    iget-object v8, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v9, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    add-int/2addr v9, v4

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v5

    invoke-virtual {v8, v4, v5, v9, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 789
    :cond_9
    :goto_1f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->x:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v5, 0x40000000    # 2.0f

    move/from16 v0, v22

    invoke-virtual {v3, v7, v4, v0, v5}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 790
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->x:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v8, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v9, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    add-int/2addr v9, v4

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v5

    invoke-virtual {v8, v4, v5, v9, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 792
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->k:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    if-eqz v3, :cond_a

    .line 794
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->k:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v3, v4, v5, v8, v9}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 795
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->k:Lcom/google/android/apps/gmm/base/layout/y;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->k:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    .line 796
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    sub-int v4, v7, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->e:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    .line 795
    if-eqz v23, :cond_33

    sub-int v4, v7, v4

    iget-object v8, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v4, v8

    iget-object v8, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v9, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    add-int/2addr v9, v4

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v5

    invoke-virtual {v8, v4, v5, v9, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 799
    :cond_a
    :goto_20
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->w:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_b

    .line 800
    sget v3, Lcom/google/android/apps/gmm/e;->bq:I

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 801
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->w:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, 0x40000000    # 2.0f

    move/from16 v0, v22

    invoke-virtual {v4, v3, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 802
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->w:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v4, 0x0

    if-eqz v23, :cond_34

    sub-int v5, v7, v6

    iget-object v6, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v7, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    add-int/2addr v7, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v4

    invoke-virtual {v6, v5, v4, v7, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 806
    :cond_b
    :goto_21
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/base/activities/c;

    .line 807
    iget-object v3, v3, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    .line 808
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    .line 809
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v3

    .line 810
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/f/o;->k:Lcom/google/android/apps/gmm/map/f/e;

    .line 811
    if-eqz v3, :cond_c

    .line 812
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Z)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/f/e;->a(Landroid/graphics/Rect;)V

    .line 815
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->aj:Lcom/google/android/apps/gmm/base/layout/y;

    if-eqz v3, :cond_d

    .line 816
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->aj:Lcom/google/android/apps/gmm/base/layout/y;

    .line 817
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Z)Landroid/graphics/Rect;

    move-result-object v4

    .line 816
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/layout/y;Landroid/graphics/Rect;)V

    .line 820
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ak:Lcom/google/android/apps/gmm/base/layout/y;

    if-eqz v3, :cond_e

    .line 821
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ak:Lcom/google/android/apps/gmm/base/layout/y;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Z)Landroid/graphics/Rect;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/layout/y;Landroid/graphics/Rect;)V

    .line 823
    :cond_e
    return-void

    .line 571
    :cond_f
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_2

    .line 576
    :cond_10
    sget v3, Lcom/google/android/apps/gmm/e;->L:I

    goto/16 :goto_3

    .line 610
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, 0x40000000    # 2.0f

    move/from16 v0, v22

    invoke-virtual {v3, v7, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    goto/16 :goto_4

    .line 612
    :cond_12
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_5

    .line 615
    :cond_13
    if-nez v4, :cond_14

    if-eqz v3, :cond_15

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v5, Lcom/google/android/apps/gmm/base/views/w;->c:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v3, v5, :cond_15

    .line 616
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v7, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    goto/16 :goto_6

    .line 618
    :cond_15
    invoke-virtual {v10}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 619
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v24, -0x80000000

    move/from16 v0, v22

    move/from16 v1, v24

    invoke-virtual {v5, v3, v8, v0, v1}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    goto/16 :goto_6

    .line 621
    :cond_16
    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v25

    add-int v25, v25, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v5, v8, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_7

    .line 625
    :cond_17
    if-eqz v9, :cond_18

    .line 626
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v6, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    goto/16 :goto_8

    .line 630
    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v3, Landroid/view/ViewGroup;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v3

    if-nez v3, :cond_19

    const/4 v3, 0x1

    .line 631
    :goto_22
    if-eqz v3, :cond_1a

    .line 632
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v7, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    goto/16 :goto_8

    .line 630
    :cond_19
    const/4 v3, 0x0

    goto :goto_22

    .line 636
    :cond_1a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    move/from16 v0, v22

    move/from16 v1, v22

    invoke-virtual {v3, v0, v5, v1, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    goto/16 :goto_8

    .line 639
    :cond_1b
    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v25

    add-int v25, v25, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v5, v8, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_9

    .line 646
    :cond_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->c:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v7, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    .line 647
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->d:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v7, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    goto/16 :goto_a

    .line 649
    :cond_1d
    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v25

    add-int v25, v25, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v5, v8, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_b

    .line 659
    :cond_1e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->d:Lcom/google/android/apps/gmm/base/layout/y;

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    move/from16 v0, v22

    invoke-virtual {v3, v7, v5, v0, v8}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    goto/16 :goto_c

    .line 661
    :cond_1f
    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v25

    add-int v25, v25, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v5, v8, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_d

    :cond_20
    move v3, v7

    .line 663
    goto/16 :goto_e

    .line 666
    :cond_21
    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v25

    add-int v25, v25, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v5, v8, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_f

    .line 684
    :cond_22
    sget-object v5, Lcom/google/android/apps/gmm/base/layout/n;->b:[I

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->L:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v8, v8, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/base/activities/ac;->ordinal()I

    move-result v8

    aget v5, v5, v8

    packed-switch v5, :pswitch_data_0

    const/4 v5, 0x0

    goto/16 :goto_10

    :pswitch_0
    const/4 v5, 0x0

    goto/16 :goto_10

    :pswitch_1
    const/4 v5, 0x1

    goto/16 :goto_10

    :pswitch_2
    const/4 v5, 0x1

    goto/16 :goto_10

    :pswitch_3
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->G:Z

    if-eqz v5, :cond_23

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->F:Z

    if-eqz v5, :cond_23

    const/4 v5, 0x1

    goto/16 :goto_10

    :cond_23
    const/4 v5, 0x0

    goto/16 :goto_10

    .line 701
    :cond_24
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->c:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v3

    if-lez v3, :cond_25

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->c:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v3

    goto/16 :goto_12

    :cond_25
    const/4 v3, 0x0

    goto/16 :goto_12

    .line 707
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    move-object/from16 v24, v0

    const/high16 v25, 0x40000000    # 2.0f

    const/high16 v26, 0x40000000    # 2.0f

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v7, v1, v8, v2}, Lcom/google/android/apps/gmm/base/layout/y;->a(IIII)V

    goto/16 :goto_14

    .line 709
    :cond_27
    iget-object v5, v8, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v0, v8, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v25

    add-int v25, v25, v24

    iget-object v8, v8, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    add-int/2addr v8, v3

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v5, v0, v3, v1, v8}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_15

    .line 713
    :cond_28
    iget-object v8, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Rect;->width()I

    move-result v24

    add-int v24, v24, v14

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v5

    move/from16 v0, v24

    invoke-virtual {v8, v14, v5, v0, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_16

    .line 723
    :cond_29
    iget-object v14, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Rect;->width()I

    move-result v24

    add-int v24, v24, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move/from16 v0, v24

    invoke-virtual {v14, v5, v8, v0, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_17

    .line 734
    :cond_2a
    iget-object v14, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v18

    add-int v18, v18, v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v8

    move/from16 v0, v18

    invoke-virtual {v14, v5, v8, v0, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_18

    .line 747
    :cond_2b
    iget-object v8, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v12, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v12

    add-int/2addr v12, v3

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v5

    invoke-virtual {v8, v3, v5, v12, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_19

    .line 751
    :cond_2c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->e:Lcom/google/android/apps/gmm/base/layout/y;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->e:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    .line 752
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v3, v5

    .line 751
    if-eqz v23, :cond_2d

    sub-int v3, v7, v3

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    sub-int/2addr v3, v5

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v8, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    add-int/2addr v8, v3

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v12

    invoke-virtual {v5, v3, v12, v8, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_19

    :cond_2d
    iget-object v5, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v8, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    add-int/2addr v8, v3

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v12

    invoke-virtual {v5, v3, v12, v8, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_19

    .line 768
    :cond_2e
    iget-object v8, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v12, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v12

    add-int/2addr v12, v5

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {v8, v5, v3, v12, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_1b

    .line 773
    :cond_2f
    const/4 v3, 0x0

    goto/16 :goto_1c

    .line 778
    :cond_30
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->j:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v3, Lcom/google/android/apps/gmm/base/views/ScalebarView;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/android/apps/gmm/base/views/ScalebarView;->l:Z

    goto/16 :goto_1d

    .line 783
    :cond_31
    iget-object v8, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v9, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    add-int/2addr v9, v4

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v5

    invoke-virtual {v8, v4, v5, v9, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_1e

    .line 786
    :cond_32
    iget-object v8, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v9, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    add-int/2addr v9, v4

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v5

    invoke-virtual {v8, v4, v5, v9, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_1f

    .line 795
    :cond_33
    iget-object v8, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v9, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    add-int/2addr v9, v4

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v5

    invoke-virtual {v8, v4, v5, v9, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_20

    .line 802
    :cond_34
    iget-object v5, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    iget-object v7, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    add-int/2addr v7, v6

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/layout/y;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v4

    invoke-virtual {v5, v6, v4, v7, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_21

    :cond_35
    move v3, v4

    goto/16 :goto_1a

    :cond_36
    move v3, v8

    goto/16 :goto_13

    :cond_37
    move v5, v3

    goto/16 :goto_11

    .line 684
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 967
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->y:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 968
    const/4 v0, 0x0

    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    .line 970
    :cond_0
    return-void
.end method

.method public requestLayout()V
    .locals 0

    .prologue
    .line 2555
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/layout/ManualLayoutViewGroup;->requestLayout()V

    .line 2563
    return-void
.end method

.method public final setAllowLayoutDuringAnimation(Z)V
    .locals 2

    .prologue
    .line 2544
    iget v1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->A:I

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->A:I

    .line 2545
    iget v0, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->A:I

    if-gez v0, :cond_0

    .line 2546
    sget-object v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->W:Ljava/lang/String;

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2548
    :cond_0
    return-void

    .line 2544
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final setMapRenderingMode(Lcom/google/android/apps/gmm/base/layout/w;)V
    .locals 0

    .prologue
    .line 2415
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->ac:Lcom/google/android/apps/gmm/base/layout/w;

    .line 2420
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i()V

    .line 2421
    return-void
.end method
