.class public final enum Lcom/google/android/apps/gmm/map/internal/d/c;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/internal/d/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/internal/d/c;

.field public static final enum b:Lcom/google/android/apps/gmm/map/internal/d/c;

.field public static final enum c:Lcom/google/android/apps/gmm/map/internal/d/c;

.field public static final enum d:Lcom/google/android/apps/gmm/map/internal/d/c;

.field public static final enum e:Lcom/google/android/apps/gmm/map/internal/d/c;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/map/internal/d/c;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v3, 0x1

    .line 184
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/map/internal/d/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/c;->a:Lcom/google/android/apps/gmm/map/internal/d/c;

    .line 190
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/apps/gmm/map/internal/d/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/c;->b:Lcom/google/android/apps/gmm/map/internal/d/c;

    .line 197
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c;

    const-string v1, "PREFETCH_OFFLINE_MAP"

    invoke-direct {v0, v1, v6, v4}, Lcom/google/android/apps/gmm/map/internal/d/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/c;->c:Lcom/google/android/apps/gmm/map/internal/d/c;

    .line 204
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c;

    const-string v1, "PREFETCH_ROUTE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/apps/gmm/map/internal/d/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/c;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    .line 213
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c;

    const-string v1, "PREFETCH_AREA"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/internal/d/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/c;->e:Lcom/google/android/apps/gmm/map/internal/d/c;

    .line 183
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/d/c;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/c;->a:Lcom/google/android/apps/gmm/map/internal/d/c;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/c;->b:Lcom/google/android/apps/gmm/map/internal/d/c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/c;->c:Lcom/google/android/apps/gmm/map/internal/d/c;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/c;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/c;->e:Lcom/google/android/apps/gmm/map/internal/d/c;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/c;->g:[Lcom/google/android/apps/gmm/map/internal/d/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 217
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 218
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/d/c;->f:I

    .line 219
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/d/c;
    .locals 1

    .prologue
    .line 183
    const-class v0, Lcom/google/android/apps/gmm/map/internal/d/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/c;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/internal/d/c;
    .locals 1

    .prologue
    .line 183
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/c;->g:[Lcom/google/android/apps/gmm/map/internal/d/c;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/internal/d/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/d/c;

    return-object v0
.end method
