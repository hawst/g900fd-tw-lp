.class public abstract Lcom/google/android/apps/gmm/shared/c/u;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 25
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/gmm/shared/c/u;-><init>(Landroid/view/View;J)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/view/View;J)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/v;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/shared/c/v;-><init>(Lcom/google/android/apps/gmm/shared/c/u;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/u;->b:Ljava/lang/Runnable;

    .line 45
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/u;->a:Landroid/os/Handler;

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/u;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/u;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 49
    :cond_0
    invoke-virtual {p1, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 50
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/u;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/u;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/u;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 65
    :cond_0
    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/c/u;->a()V

    .line 67
    return-void
.end method
