.class public Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/navigation/base/d;

.field b:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 20
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/navigation/base/d;Z)Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;-><init>()V

    .line 45
    iput-object p0, v0, Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;->a:Lcom/google/android/apps/gmm/navigation/base/d;

    .line 47
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 48
    const-string v2, "isChecked"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 49
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;->setArguments(Landroid/os/Bundle;)V

    .line 50
    return-object v0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;->a:Lcom/google/android/apps/gmm/navigation/base/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/base/d;->a()V

    .line 90
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 59
    sget v0, Lcom/google/android/apps/gmm/h;->al:I

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 61
    sget v0, Lcom/google/android/apps/gmm/g;->bR:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;->b:Landroid/widget/CheckBox;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;->b:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "isChecked"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 64
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 65
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->ct:I

    new-instance v2, Lcom/google/android/apps/gmm/navigation/base/c;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/navigation/base/c;-><init>(Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;)V

    .line 66
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->bg:I

    new-instance v2, Lcom/google/android/apps/gmm/navigation/base/b;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/navigation/base/b;-><init>(Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;)V

    .line 72
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
