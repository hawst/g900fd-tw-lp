.class public final Lcom/google/android/apps/gmm/feedback/z;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/feedback/y;


# static fields
.field static a:F


# instance fields
.field private final b:Lcom/google/android/apps/gmm/map/util/a/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/i",
            "<",
            "Lcom/google/android/apps/gmm/feedback/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/android/apps/gmm/feedback/b;

.field private final d:Lcom/google/android/apps/gmm/base/a;

.field private final e:Landroid/hardware/SensorManager;

.field private final f:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/gmm/feedback/a;",
            ">;"
        }
    .end annotation
.end field

.field private g:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const v0, 0x3f4ccccd    # 0.8f

    sput v0, Lcom/google/android/apps/gmm/feedback/z;->a:F

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/base/a;Landroid/hardware/SensorManager;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/google/android/apps/gmm/feedback/b;

    new-array v1, v2, [F

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/feedback/b;-><init>([F)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->c:Lcom/google/android/apps/gmm/feedback/b;

    .line 75
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->f:Ljava/util/LinkedList;

    .line 80
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->g:[F

    .line 85
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->d:Lcom/google/android/apps/gmm/base/a;

    .line 86
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Landroid/hardware/SensorManager;

    iput-object p2, p0, Lcom/google/android/apps/gmm/feedback/z;->e:Landroid/hardware/SensorManager;

    .line 87
    new-instance v0, Lcom/google/android/apps/gmm/feedback/aa;

    .line 88
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v1

    const-string v2, "ShakeGestureRecognizerImpl"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/gmm/feedback/aa;-><init>(Lcom/google/android/apps/gmm/feedback/z;Lcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->b:Lcom/google/android/apps/gmm/map/util/a/i;

    .line 94
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;)Lcom/google/android/apps/gmm/feedback/z;
    .locals 2

    .prologue
    .line 104
    const-string v0, "sensor"

    .line 105
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 106
    new-instance v1, Lcom/google/android/apps/gmm/feedback/z;

    invoke-direct {v1, p1, v0}, Lcom/google/android/apps/gmm/feedback/z;-><init>(Lcom/google/android/apps/gmm/base/a;Landroid/hardware/SensorManager;)V

    .line 108
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->e:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-nez v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->e:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/z;->e:Landroid/hardware/SensorManager;

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->e:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->e:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/z;->e:Landroid/hardware/SensorManager;

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 141
    :cond_0
    return-void
.end method

.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 146
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 10

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 156
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->g:[F

    sget v4, Lcom/google/android/apps/gmm/feedback/z;->a:F

    iget-object v5, p0, Lcom/google/android/apps/gmm/feedback/z;->g:[F

    aget v5, v5, v3

    mul-float/2addr v4, v5

    sget v5, Lcom/google/android/apps/gmm/feedback/z;->a:F

    sub-float v5, v7, v5

    aget v6, v1, v3

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    aput v4, v0, v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->g:[F

    sget v4, Lcom/google/android/apps/gmm/feedback/z;->a:F

    iget-object v5, p0, Lcom/google/android/apps/gmm/feedback/z;->g:[F

    aget v5, v5, v2

    mul-float/2addr v4, v5

    sget v5, Lcom/google/android/apps/gmm/feedback/z;->a:F

    sub-float v5, v7, v5

    aget v6, v1, v2

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    aput v4, v0, v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->g:[F

    sget v4, Lcom/google/android/apps/gmm/feedback/z;->a:F

    iget-object v5, p0, Lcom/google/android/apps/gmm/feedback/z;->g:[F

    aget v5, v5, v9

    mul-float/2addr v4, v5

    sget v5, Lcom/google/android/apps/gmm/feedback/z;->a:F

    sub-float v5, v7, v5

    aget v6, v1, v9

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    aput v4, v0, v9

    aget v0, v1, v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/feedback/z;->g:[F

    aget v4, v4, v3

    sub-float/2addr v0, v4

    aput v0, v1, v3

    aget v0, v1, v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/feedback/z;->g:[F

    aget v4, v4, v2

    sub-float/2addr v0, v4

    aput v0, v1, v2

    aget v0, v1, v9

    iget-object v4, p0, Lcom/google/android/apps/gmm/feedback/z;->g:[F

    aget v4, v4, v9

    sub-float/2addr v0, v4

    aput v0, v1, v9

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->c:Lcom/google/android/apps/gmm/feedback/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/feedback/b;->a([F)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->c:Lcom/google/android/apps/gmm/feedback/b;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/feedback/b;->b:D

    sget-wide v6, Lcom/google/android/apps/gmm/map/util/c;->h:D

    cmpl-double v0, v4, v6

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->d:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->b:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/feedback/a;

    iget-object v6, p0, Lcom/google/android/apps/gmm/feedback/z;->g:[F

    iget-object v7, v0, Lcom/google/android/apps/gmm/feedback/a;->a:Lcom/google/android/apps/gmm/feedback/b;

    invoke-virtual {v7, v1}, Lcom/google/android/apps/gmm/feedback/b;->a([F)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/feedback/a;->b:Lcom/google/android/apps/gmm/feedback/b;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/gmm/feedback/b;->a([F)V

    iput-wide v4, v0, Lcom/google/android/apps/gmm/feedback/a;->c:J

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/z;->f:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    const-wide/16 v0, 0x3e8

    sub-long/2addr v4, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/feedback/a;

    iget-wide v6, v0, Lcom/google/android/apps/gmm/feedback/a;->c:J

    cmp-long v6, v6, v4

    if-gez v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    iget-object v6, p0, Lcom/google/android/apps/gmm/feedback/z;->b:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v4, 0x5

    cmp-long v0, v0, v4

    if-ltz v0, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/feedback/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/z;->f:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/feedback/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/a;->b:Lcom/google/android/apps/gmm/feedback/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/feedback/a;->b:Lcom/google/android/apps/gmm/feedback/b;

    iget-object v4, v0, Lcom/google/android/apps/gmm/feedback/b;->a:[F

    array-length v4, v4

    invoke-static {v9, v4}, Lcom/google/b/a/aq;->a(II)I

    iget-object v4, v0, Lcom/google/android/apps/gmm/feedback/b;->a:[F

    iget-object v5, v1, Lcom/google/android/apps/gmm/feedback/b;->a:[F

    aget v6, v4, v3

    aget v7, v5, v3

    mul-float/2addr v6, v7

    aget v7, v4, v2

    aget v8, v5, v2

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    aget v4, v4, v9

    aget v5, v5, v9

    mul-float/2addr v4, v5

    add-float/2addr v4, v6

    float-to-double v4, v4

    iget-wide v6, v0, Lcom/google/android/apps/gmm/feedback/b;->b:D

    iget-wide v0, v1, Lcom/google/android/apps/gmm/feedback/b;->b:D

    mul-double/2addr v0, v6

    div-double v0, v4, v0

    const-wide v4, 0x3fe3333340000000L    # 0.6000000238418579

    cmpg-double v0, v0, v4

    if-gez v0, :cond_5

    move v0, v2

    :goto_2
    if-nez v0, :cond_6

    move v0, v2

    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/z;->f:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    :goto_4
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/z;->d:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/feedback/x;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/feedback/x;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 157
    :cond_2
    return-void

    :cond_3
    move v0, v3

    .line 156
    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_1

    :cond_5
    move v0, v3

    goto :goto_2

    :cond_6
    move v0, v3

    goto :goto_3

    :cond_7
    move v0, v3

    goto :goto_4
.end method
