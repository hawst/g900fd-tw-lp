.class public Lcom/google/android/apps/gmm/base/l/x;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/r;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/activities/c;

.field private final b:Lcom/google/android/apps/gmm/prefetchcache/api/c;

.field private final c:Lcom/google/android/apps/gmm/base/l/a/u;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/prefetchcache/api/c;Lcom/google/android/apps/gmm/base/l/a/u;Z)V
    .locals 0
    .param p3    # Lcom/google/android/apps/gmm/base/l/a/u;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 31
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/l/x;->b:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    .line 32
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/l/x;->c:Lcom/google/android/apps/gmm/base/l/a/u;

    .line 33
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/base/l/x;->d:Z

    .line 34
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/x;->b:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->u()Lcom/google/android/apps/gmm/prefetchcache/api/a;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    .line 86
    :goto_1
    return-object v1

    :cond_0
    move-object v0, v1

    .line 76
    goto :goto_0

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/x;->b:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->d()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_4

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->u()Lcom/google/android/apps/gmm/prefetchcache/api/a;

    move-result-object v0

    :goto_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/x;->b:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/prefetchcache/api/a;->b(Lcom/google/android/apps/gmm/prefetchcache/api/c;)V

    goto :goto_1

    .line 80
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 81
    goto :goto_3

    .line 84
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->u()Lcom/google/android/apps/gmm/prefetchcache/api/a;

    move-result-object v0

    :goto_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/x;->b:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/prefetchcache/api/a;->a(Lcom/google/android/apps/gmm/prefetchcache/api/c;)V

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_4
.end method

.method public final b()Lcom/google/android/apps/gmm/base/l/a/s;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/x;->b:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/base/l/a/s;->a:Lcom/google/android/apps/gmm/base/l/a/s;

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/base/l/a/s;->b:Lcom/google/android/apps/gmm/base/l/a/s;

    goto :goto_1
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 71
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/e;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/x;->b:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->d()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/x;->b:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/gmm/l;->ja:I

    :goto_1
    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v3, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/apps/gmm/d;->aH:I

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/h;->a(I)Lcom/google/android/apps/gmm/shared/c/c/i;

    move-result-object v0

    const-string v2, "  \u2022  "

    const-string v3, "%s"

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iput-object v3, v0, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    sget v2, Lcom/google/android/apps/gmm/l;->jc:I

    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v4, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    const-string v1, "%s"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    const-string v2, "%s"

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iput-object v1, v0, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    const-string v1, "%s"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    :cond_0
    :goto_2
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/apps/gmm/l;->jd:I

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/x;->b:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->i()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/x;->b:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_0

    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/x;->b:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    sget v1, Lcom/google/android/apps/gmm/d;->aH:I

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(I)Lcom/google/android/apps/gmm/shared/c/c/i;

    :cond_4
    if-eqz v0, :cond_5

    const-string v1, "  \u2022  "

    const-string v2, "%s"

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iput-object v2, v3, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    const-string v1, "%s"

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iput-object v1, v3, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    :cond_5
    const-string v0, "%s"

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_2
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/base/l/a/u;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/x;->c:Lcom/google/android/apps/gmm/base/l/a/u;

    return-object v0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/x;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()Lcom/google/android/libraries/curvular/aw;
    .locals 2

    .prologue
    .line 21
    sget-object v1, Lcom/google/android/apps/gmm/base/l/y;->a:[I

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/x;->b:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/base/l/a/s;->a:Lcom/google/android/apps/gmm/base/l/a/s;

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/l/a/s;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_2
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/base/l/a/s;->b:Lcom/google/android/apps/gmm/base/l/a/s;

    goto :goto_1

    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/f;->cX:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_2

    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/x;->d:Z

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/gmm/f;->ed:I

    :goto_3
    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_2

    :cond_2
    sget v0, Lcom/google/android/apps/gmm/f;->ej:I

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
