.class Lcom/google/android/apps/gmm/startpage/q;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/o/h/a/a;

.field final synthetic b:Lcom/google/android/apps/gmm/startpage/m;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/startpage/m;Lcom/google/o/h/a/a;)V
    .locals 0

    .prologue
    .line 876
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/q;->b:Lcom/google/android/apps/gmm/startpage/m;

    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/q;->a:Lcom/google/o/h/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 879
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/q;->b:Lcom/google/android/apps/gmm/startpage/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/q;->a:Lcom/google/o/h/a/a;

    iget v0, v1, Lcom/google/o/h/a/a;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/startpage/m;->a:Ljava/lang/String;

    .line 880
    :goto_1
    return-void

    .line 879
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, v1, Lcom/google/o/h/a/a;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kh;

    iget v1, v0, Lcom/google/o/h/a/kh;->f:I

    invoke-static {v1}, Lcom/google/o/h/a/kk;->a(I)Lcom/google/o/h/a/kk;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/o/h/a/kk;->a:Lcom/google/o/h/a/kk;

    :cond_2
    sget-object v3, Lcom/google/o/h/a/kk;->b:Lcom/google/o/h/a/kk;

    if-eq v1, v3, :cond_4

    sget-object v1, Lcom/google/android/apps/gmm/startpage/m;->a:Ljava/lang/String;

    const-string v1, "LoadOdelayAction.page_style should be CURRENT_PAGE.Ignoring specified parameter: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget v1, v0, Lcom/google/o/h/a/kh;->f:I

    invoke-static {v1}, Lcom/google/o/h/a/kk;->a(I)Lcom/google/o/h/a/kk;

    move-result-object v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/google/o/h/a/kk;->a:Lcom/google/o/h/a/kk;

    :cond_3
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    new-instance v1, Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/e;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/startpage/m;->a(Lcom/google/android/apps/gmm/startpage/d/e;)V

    goto :goto_1
.end method
