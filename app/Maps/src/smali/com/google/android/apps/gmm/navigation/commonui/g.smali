.class public Lcom/google/android/apps/gmm/navigation/commonui/g;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/navigation/commonui/d;

.field private final b:Lcom/google/android/apps/gmm/map/t;

.field private final c:Lcom/google/android/apps/gmm/navigation/commonui/h;

.field private d:Lcom/google/android/apps/gmm/map/b/a/y;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/commonui/d;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/navigation/commonui/h;)V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const-string v0, "host"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/navigation/commonui/d;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/commonui/g;->a:Lcom/google/android/apps/gmm/navigation/commonui/d;

    .line 66
    const-string v0, "mapContainer"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/map/t;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/commonui/g;->b:Lcom/google/android/apps/gmm/map/t;

    .line 67
    const-string v0, "listener"

    if-nez p3, :cond_2

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/navigation/commonui/h;

    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/commonui/g;->c:Lcom/google/android/apps/gmm/navigation/commonui/h;

    .line 68
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/j/ab;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/g;->a:Lcom/google/android/apps/gmm/navigation/commonui/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/d;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/g;->b:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    .line 82
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/j/ab;->a:Lcom/google/android/apps/gmm/map/j/ac;

    sget-object v2, Lcom/google/android/apps/gmm/map/j/ac;->a:Lcom/google/android/apps/gmm/map/j/ac;

    if-ne v1, v2, :cond_2

    .line 84
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_0

    .line 85
    :cond_2
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/j/ab;->a:Lcom/google/android/apps/gmm/map/j/ac;

    sget-object v2, Lcom/google/android/apps/gmm/map/j/ac;->b:Lcom/google/android/apps/gmm/map/j/ac;

    if-eq v1, v2, :cond_3

    .line 86
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/j/ab;->a:Lcom/google/android/apps/gmm/map/j/ac;

    sget-object v2, Lcom/google/android/apps/gmm/map/j/ac;->c:Lcom/google/android/apps/gmm/map/j/ac;

    if-ne v1, v2, :cond_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/commonui/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v1, :cond_0

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/commonui/g;->c:Lcom/google/android/apps/gmm/navigation/commonui/h;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/commonui/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, v0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    mul-float/2addr v3, v4

    iget v4, v0, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v5, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    div-float/2addr v3, v4

    div-float/2addr v2, v3

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/o;->i:F

    div-float v0, v2, v0

    const/high16 v2, 0x42200000    # 40.0f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/navigation/commonui/h;->a(Z)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/ad;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/g;->a:Lcom/google/android/apps/gmm/navigation/commonui/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/d;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/g;->c:Lcom/google/android/apps/gmm/navigation/commonui/h;

    iget v1, p1, Lcom/google/android/apps/gmm/map/j/ad;->a:F

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/commonui/h;->a(F)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/w;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/g;->a:Lcom/google/android/apps/gmm/navigation/commonui/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/d;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/g;->c:Lcom/google/android/apps/gmm/navigation/commonui/h;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/navigation/commonui/h;->a(Lcom/google/android/apps/gmm/map/j/w;)V

    goto :goto_0
.end method
