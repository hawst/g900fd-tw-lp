.class Lcom/google/android/apps/gmm/z/c/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/z/c/b;

.field private b:I

.field private c:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/z/c/b;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/c/d;->a:Lcom/google/android/apps/gmm/z/c/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 45
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/z/c/d;->c:Z

    .line 46
    iget v0, p0, Lcom/google/android/apps/gmm/z/c/d;->b:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/z/c/d;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/d;->a:Lcom/google/android/apps/gmm/z/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/c/b;->b:Lcom/google/android/apps/gmm/z/c/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/c/k;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    :cond_0
    monitor-exit p0

    return-void

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/z/c/d;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 58
    const/4 v0, 0x0

    .line 61
    :goto_0
    monitor-exit p0

    return v0

    .line 60
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/z/c/d;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61
    const/4 v0, 0x1

    goto :goto_0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 65
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/z/c/d;->c:Z

    .line 66
    iget v0, p0, Lcom/google/android/apps/gmm/z/c/d;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/z/c/d;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    monitor-exit p0

    return-void

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/z/c/d;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/z/c/d;->b:I

    .line 71
    iget v0, p0, Lcom/google/android/apps/gmm/z/c/d;->b:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/z/c/d;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/d;->a:Lcom/google/android/apps/gmm/z/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/c/b;->b:Lcom/google/android/apps/gmm/z/c/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/c/k;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    :cond_0
    monitor-exit p0

    return-void

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
