.class Lcom/google/android/apps/gmm/reportmapissue/d/j;
.super Lcom/google/android/apps/gmm/base/l/d;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/reportmapissue/d/h;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/reportmapissue/d/h;)V
    .locals 3

    .prologue
    .line 230
    iput-object p1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 231
    iget-object v0, p1, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->la:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 232
    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->lh:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 231
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/base/l/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->a:Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Ljava/lang/Runnable;)V

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->a:Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 241
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 245
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/h;->a:Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->isResumed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 263
    :goto_0
    return-void

    .line 248
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->l:Lcom/google/android/apps/gmm/reportmapissue/d/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/b;->b:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->l:Lcom/google/android/apps/gmm/reportmapissue/d/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/b;->b:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v3, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v3, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v0

    :goto_1
    if-eqz v1, :cond_8

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->m:Lcom/google/android/apps/gmm/reportmapissue/d/g;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/g;->a:Lcom/google/android/apps/gmm/reportmapissue/a/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/e;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_5

    move v1, v0

    :goto_2
    if-nez v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->q:Lcom/google/android/apps/gmm/reportmapissue/a/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->n:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_6

    :cond_1
    move v1, v0

    :goto_3
    if-nez v1, :cond_7

    :cond_2
    :goto_4
    if-eqz v0, :cond_17

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->r:Landroid/app/ProgressDialog;

    if-nez v0, :cond_3

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v1, v2, v6}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->r:Landroid/app/ProgressDialog;

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->r:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->mP:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 254
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->r:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->p:Lcom/google/android/apps/gmm/reportmapissue/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/h;->q:Lcom/google/android/apps/gmm/reportmapissue/a/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 256
    iget-object v3, v3, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v4, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 257
    iget-object v4, v4, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 258
    iget-object v5, v5, Lcom/google/android/apps/gmm/reportmapissue/d/h;->a:Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    iget-object v7, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 255
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/reportmapissue/f;->a(Lcom/google/android/apps/gmm/reportmapissue/a/c;Lcom/google/android/apps/gmm/shared/net/r;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/o/a/c;Lcom/google/android/apps/gmm/reportmapissue/h;Z)V

    goto/16 :goto_0

    :cond_4
    move v1, v6

    .line 248
    goto/16 :goto_1

    :cond_5
    move v1, v6

    goto/16 :goto_2

    :cond_6
    move v1, v6

    goto :goto_3

    :cond_7
    move v0, v6

    goto :goto_4

    :cond_8
    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->c:Lcom/google/android/apps/gmm/reportmapissue/d/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/g;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->d:Lcom/google/android/apps/gmm/reportmapissue/d/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/g;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->n:Lcom/google/android/apps/gmm/reportmapissue/d/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/b;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->e:Lcom/google/android/apps/gmm/reportmapissue/d/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/g;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->e:Lcom/google/android/apps/gmm/reportmapissue/d/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/g;->c:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_c

    :cond_9
    move v1, v0

    :goto_5
    if-nez v1, :cond_d

    move v1, v0

    :goto_6
    if-nez v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->f:Lcom/google/android/apps/gmm/reportmapissue/d/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/g;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->f:Lcom/google/android/apps/gmm/reportmapissue/d/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/g;->c:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_e

    :cond_a
    move v1, v0

    :goto_7
    if-nez v1, :cond_f

    move v1, v0

    :goto_8
    if-nez v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->g:Lcom/google/android/apps/gmm/reportmapissue/d/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/b;->b:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v3, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v3, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    move v1, v0

    :goto_9
    if-nez v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->k:Lcom/google/android/apps/gmm/reportmapissue/d/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/b;->b:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v3, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v3, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    move v1, v0

    :goto_a
    if-nez v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->j:Lcom/google/android/apps/gmm/reportmapissue/d/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/b;->b:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v3, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v3, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    move v1, v0

    :goto_b
    if-nez v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->i:Lcom/google/android/apps/gmm/reportmapissue/d/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/b;->b:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v3, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v3, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    move v1, v0

    :goto_c
    if-nez v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->h:Lcom/google/android/apps/gmm/reportmapissue/d/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/b;->b:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v3, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v3, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    move v1, v0

    :goto_d
    if-nez v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->l:Lcom/google/android/apps/gmm/reportmapissue/d/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/b;->b:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v3, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v3, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    move v1, v0

    :goto_e
    if-nez v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->q:Lcom/google/android/apps/gmm/reportmapissue/a/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->n:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_16

    :cond_b
    move v1, v0

    :goto_f
    if-eqz v1, :cond_2

    move v0, v6

    goto/16 :goto_4

    :cond_c
    move v1, v6

    goto/16 :goto_5

    :cond_d
    move v1, v6

    goto/16 :goto_6

    :cond_e
    move v1, v6

    goto/16 :goto_7

    :cond_f
    move v1, v6

    goto/16 :goto_8

    :cond_10
    move v1, v6

    goto :goto_9

    :cond_11
    move v1, v6

    goto :goto_a

    :cond_12
    move v1, v6

    goto :goto_b

    :cond_13
    move v1, v6

    goto :goto_c

    :cond_14
    move v1, v6

    goto :goto_d

    :cond_15
    move v1, v6

    goto :goto_e

    :cond_16
    move v1, v6

    goto :goto_f

    .line 260
    :cond_17
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/j;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->la:I

    sget v2, Lcom/google/android/apps/gmm/l;->lb:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;II)V

    goto/16 :goto_0
.end method
