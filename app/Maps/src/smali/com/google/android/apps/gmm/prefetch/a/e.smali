.class public final enum Lcom/google/android/apps/gmm/prefetch/a/e;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/prefetch/a/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/prefetch/a/e;

.field public static final enum b:Lcom/google/android/apps/gmm/prefetch/a/e;

.field public static final enum c:Lcom/google/android/apps/gmm/prefetch/a/e;

.field public static final enum d:Lcom/google/android/apps/gmm/prefetch/a/e;

.field public static final enum e:Lcom/google/android/apps/gmm/prefetch/a/e;

.field public static final enum f:Lcom/google/android/apps/gmm/prefetch/a/e;

.field public static final enum g:Lcom/google/android/apps/gmm/prefetch/a/e;

.field public static final enum h:Lcom/google/android/apps/gmm/prefetch/a/e;

.field public static final enum i:Lcom/google/android/apps/gmm/prefetch/a/e;

.field private static final synthetic j:[Lcom/google/android/apps/gmm/prefetch/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 17
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/a/e;

    const-string v1, "PREFETCH_CANCELED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/prefetch/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->a:Lcom/google/android/apps/gmm/prefetch/a/e;

    .line 18
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/a/e;

    const-string v1, "PREFETCH_SUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/prefetch/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->b:Lcom/google/android/apps/gmm/prefetch/a/e;

    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/a/e;

    const-string v1, "PREFETCH_ERROR_REMOVE_REFCOUNTS_IO_ERROR"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/prefetch/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->c:Lcom/google/android/apps/gmm/prefetch/a/e;

    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/a/e;

    const-string v1, "PREFETCH_ERROR_REMOVE_REFCOUNTS_OTHER_ERROR"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/prefetch/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->d:Lcom/google/android/apps/gmm/prefetch/a/e;

    .line 21
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/a/e;

    const-string v1, "PREFETCH_ERROR_OFFLINE_DISABLED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/prefetch/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->e:Lcom/google/android/apps/gmm/prefetch/a/e;

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/a/e;

    const-string v1, "PREFETCH_ERROR_IO_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/prefetch/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->f:Lcom/google/android/apps/gmm/prefetch/a/e;

    .line 23
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/a/e;

    const-string v1, "PREFETCH_ERROR_CANNOT_REFCOUNT_ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/prefetch/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->g:Lcom/google/android/apps/gmm/prefetch/a/e;

    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/a/e;

    const-string v1, "PREFETCH_ERROR_NETWORK_ERROR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/prefetch/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->h:Lcom/google/android/apps/gmm/prefetch/a/e;

    .line 25
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/a/e;

    const-string v1, "PREFETCH_ERROR_OTHER_ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/prefetch/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->i:Lcom/google/android/apps/gmm/prefetch/a/e;

    .line 16
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/gmm/prefetch/a/e;

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/e;->a:Lcom/google/android/apps/gmm/prefetch/a/e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/e;->b:Lcom/google/android/apps/gmm/prefetch/a/e;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/e;->c:Lcom/google/android/apps/gmm/prefetch/a/e;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/e;->d:Lcom/google/android/apps/gmm/prefetch/a/e;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/e;->e:Lcom/google/android/apps/gmm/prefetch/a/e;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/prefetch/a/e;->f:Lcom/google/android/apps/gmm/prefetch/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/prefetch/a/e;->g:Lcom/google/android/apps/gmm/prefetch/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/prefetch/a/e;->h:Lcom/google/android/apps/gmm/prefetch/a/e;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/prefetch/a/e;->i:Lcom/google/android/apps/gmm/prefetch/a/e;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->j:[Lcom/google/android/apps/gmm/prefetch/a/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/prefetch/a/e;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/android/apps/gmm/prefetch/a/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetch/a/e;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/prefetch/a/e;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->j:[Lcom/google/android/apps/gmm/prefetch/a/e;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/prefetch/a/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/prefetch/a/e;

    return-object v0
.end method
