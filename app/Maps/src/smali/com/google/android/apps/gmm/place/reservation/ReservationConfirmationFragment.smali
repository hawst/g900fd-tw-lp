.class public Lcom/google/android/apps/gmm/place/reservation/ReservationConfirmationFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/gmm/place/reservation/p;

.field private b:Lcom/google/android/apps/gmm/z/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/r/b/a/j;Ljava/lang/String;)Lcom/google/android/apps/gmm/place/reservation/ReservationConfirmationFragment;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/ad;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/reservation/ad;-><init>()V

    .line 32
    iput-object p1, v0, Lcom/google/android/apps/gmm/place/reservation/ad;->a:Lcom/google/android/apps/gmm/base/g/c;

    .line 33
    iput-object p2, v0, Lcom/google/android/apps/gmm/place/reservation/ad;->b:Lcom/google/r/b/a/j;

    .line 34
    iput-object p3, v0, Lcom/google/android/apps/gmm/place/reservation/ad;->c:Ljava/lang/String;

    .line 35
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/reservation/ad;->a()Lcom/google/android/apps/gmm/place/reservation/ac;

    move-result-object v0

    .line 37
    new-instance v1, Lcom/google/android/apps/gmm/place/reservation/ReservationConfirmationFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/place/reservation/ReservationConfirmationFragment;-><init>()V

    .line 38
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/place/reservation/ac;->a(Lcom/google/android/apps/gmm/x/a;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/place/reservation/ReservationConfirmationFragment;->setArguments(Landroid/os/Bundle;)V

    .line 39
    return-object v1
.end method


# virtual methods
.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/google/b/f/t;->dS:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/reservation/ReservationConfirmationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/reservation/ad;->a(Lcom/google/android/apps/gmm/x/a;Landroid/os/Bundle;)Lcom/google/android/apps/gmm/place/reservation/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/reservation/ad;->a()Lcom/google/android/apps/gmm/place/reservation/ac;

    move-result-object v0

    .line 49
    new-instance v1, Lcom/google/android/apps/gmm/place/reservation/p;

    .line 50
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/reservation/ac;->a:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v4, v0, Lcom/google/android/apps/gmm/place/reservation/ac;->b:Lcom/google/r/b/a/j;

    .line 51
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/reservation/ac;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/apps/gmm/place/reservation/p;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/r/b/a/j;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationConfirmationFragment;->a:Lcom/google/android/apps/gmm/place/reservation/p;

    .line 53
    new-instance v1, Lcom/google/android/apps/gmm/z/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/z/a;-><init>(Lcom/google/android/apps/gmm/z/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationConfirmationFragment;->b:Lcom/google/android/apps/gmm/z/a;

    .line 54
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 59
    invoke-super {p0, p3}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 61
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 63
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    .line 64
    const-class v1, Lcom/google/android/apps/gmm/place/reservation/a/e;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 66
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationConfirmationFragment;->a:Lcom/google/android/apps/gmm/place/reservation/p;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 68
    return-object v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationConfirmationFragment;->b:Lcom/google/android/apps/gmm/z/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/a;->b()V

    .line 89
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 73
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 76
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/reservation/ReservationConfirmationFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    const/4 v1, 0x0

    .line 78
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    const/4 v1, 0x0

    .line 79
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 80
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ReservationConfirmationFragment;->b:Lcom/google/android/apps/gmm/z/a;

    .line 81
    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/p;->a(Lcom/google/android/apps/gmm/z/a;)Lcom/google/android/apps/gmm/base/activities/y;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 82
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 83
    return-void
.end method
