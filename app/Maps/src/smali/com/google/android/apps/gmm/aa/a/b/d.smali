.class public Lcom/google/android/apps/gmm/aa/a/b/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/aa/a/a/a;

.field final b:F

.field final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(FLcom/google/android/apps/gmm/aa/a/a/a;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/aa/a/b/d;-><init>(FLcom/google/android/apps/gmm/aa/a/a/a;Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public constructor <init>(FLcom/google/android/apps/gmm/aa/a/a/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p1, p0, Lcom/google/android/apps/gmm/aa/a/b/d;->b:F

    .line 38
    iput-object p2, p0, Lcom/google/android/apps/gmm/aa/a/b/d;->a:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 39
    iput-object p3, p0, Lcom/google/android/apps/gmm/aa/a/b/d;->c:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 66
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "score"

    iget v2, p0, Lcom/google/android/apps/gmm/aa/a/b/d;->b:F

    .line 67
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "voiceAction"

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b/d;->a:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 68
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "param"

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b/d;->c:Ljava/lang/String;

    .line 69
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 70
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/b/a/ah;->a:Z

    .line 71
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
