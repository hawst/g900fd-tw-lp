.class Lcom/google/android/apps/gmm/place/bg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/bg;->a:Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/base/e/c;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 69
    iget v0, p1, Lcom/google/android/apps/gmm/base/e/c;->b:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    .line 70
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bg;->a:Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->m:Ljava/lang/String;

    if-eq v0, v2, :cond_0

    if-eqz v0, :cond_3

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    .line 76
    :cond_1
    :goto_2
    return-void

    .line 70
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 74
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bg;->a:Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-nez v2, :cond_5

    :goto_3
    iput-object v1, v0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->m:Ljava/lang/String;

    .line 75
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bg;->a:Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->D()Lcom/google/android/apps/gmm/place/b/b;

    move-result-object v2

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v3, v1, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->e:Lcom/google/maps/g/hy;

    invoke-interface {v2, v0, v3, v1}, Lcom/google/android/apps/gmm/place/b/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/place/b/c;)Lcom/google/android/apps/gmm/shared/net/i;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->d:Lcom/google/android/apps/gmm/shared/net/i;

    goto :goto_2

    .line 74
    :cond_5
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_3
.end method
