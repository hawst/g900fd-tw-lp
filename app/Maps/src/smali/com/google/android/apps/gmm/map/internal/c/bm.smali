.class public Lcom/google/android/apps/gmm/map/internal/c/bm;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static d:Lcom/google/android/apps/gmm/map/internal/c/bm;


# instance fields
.field public final a:I

.field final b:Z

.field private final c:Lcom/google/android/apps/gmm/map/internal/c/bd;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bm;

    .line 27
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bd;->a()Lcom/google/android/apps/gmm/map/internal/c/bd;

    move-result-object v1

    invoke-direct {v0, v2, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/bm;-><init>(ILcom/google/android/apps/gmm/map/internal/c/bd;Z)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bm;->d:Lcom/google/android/apps/gmm/map/internal/c/bm;

    .line 26
    return-void
.end method

.method private constructor <init>(ILcom/google/android/apps/gmm/map/internal/c/bd;Z)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/c/bm;->a:I

    .line 31
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/c/bm;->c:Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 32
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/map/internal/c/bm;->b:Z

    .line 33
    return-void
.end method

.method public static a()Lcom/google/android/apps/gmm/map/internal/c/bm;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bm;->d:Lcom/google/android/apps/gmm/map/internal/c/bm;

    return-object v0
.end method

.method public static a(Lcom/google/maps/b/a/ck;)Lcom/google/android/apps/gmm/map/internal/c/bm;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 42
    iget-object v0, p0, Lcom/google/maps/b/a/ck;->c:Lcom/google/maps/b/a/cz;

    iget v7, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 43
    iget-object v0, p0, Lcom/google/maps/b/a/ck;->a:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 44
    iget-object v0, p0, Lcom/google/maps/b/a/ck;->b:Lcom/google/maps/b/a/cz;

    iget v2, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 46
    iget-object v0, p0, Lcom/google/maps/b/a/ck;->d:Lcom/google/maps/b/a/cs;

    iget v0, v0, Lcom/google/maps/b/a/cs;->b:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    move v6, v0

    .line 48
    :goto_0
    new-instance v8, Lcom/google/android/apps/gmm/map/internal/c/bm;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bd;

    int-to-float v2, v2

    new-array v3, v4, [I

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/c/bd;-><init>(IF[IIF)V

    invoke-direct {v8, v7, v0, v6}, Lcom/google/android/apps/gmm/map/internal/c/bm;-><init>(ILcom/google/android/apps/gmm/map/internal/c/bd;Z)V

    return-object v8

    :cond_0
    move v6, v4

    .line 46
    goto :goto_0
.end method

.method public static a(Ljava/io/DataInput;I)Lcom/google/android/apps/gmm/map/internal/c/bm;
    .locals 4

    .prologue
    .line 36
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    .line 37
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/internal/c/bd;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/bd;

    move-result-object v1

    .line 38
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/bm;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/apps/gmm/map/internal/c/bm;-><init>(ILcom/google/android/apps/gmm/map/internal/c/bd;Z)V

    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 86
    if-ne p0, p1, :cond_1

    .line 106
    :cond_0
    :goto_0
    return v0

    .line 89
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 90
    goto :goto_0

    .line 92
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 93
    goto :goto_0

    .line 95
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/bm;

    .line 96
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bm;->a:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bm;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 97
    goto :goto_0

    .line 99
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bm;->c:Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v2, :cond_5

    .line 100
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bm;->c:Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-eqz v2, :cond_6

    move v0, v1

    .line 101
    goto :goto_0

    .line 103
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bm;->c:Lcom/google/android/apps/gmm/map/internal/c/bd;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bm;->c:Lcom/google/android/apps/gmm/map/internal/c/bd;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/bd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 104
    goto :goto_0

    .line 106
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bm;->b:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bm;->b:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 74
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bm;->a:I

    add-int/lit8 v0, v0, 0x1f

    .line 77
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bm;->c:Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 78
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bm;->b:Z

    if-eqz v1, :cond_0

    .line 79
    add-int/lit8 v0, v0, 0x1f

    .line 81
    :cond_0
    return v0

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bm;->c:Lcom/google/android/apps/gmm/map/internal/c/bd;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/bd;->hashCode()I

    move-result v0

    goto :goto_0
.end method
