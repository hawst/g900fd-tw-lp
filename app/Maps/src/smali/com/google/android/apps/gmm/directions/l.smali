.class Lcom/google/android/apps/gmm/directions/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/libraries/curvular/cg",
        "<",
        "Lcom/google/android/libraries/curvular/ce;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/k;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/k;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/l;->a:Lcom/google/android/apps/gmm/directions/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/libraries/curvular/ce;Landroid/view/View;)V
    .locals 5

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/l;->a:Lcom/google/android/apps/gmm/directions/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/k;->b:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    new-instance v0, Lcom/google/android/apps/gmm/base/support/a;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/google/android/apps/gmm/base/support/a;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 334
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/l;->a:Lcom/google/android/apps/gmm/directions/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/k;->b:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->m:Lcom/google/maps/g/a/hm;

    iget-object v4, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v2, v3, v4, v1}, Lcom/google/android/apps/gmm/directions/i/u;->a(Lcom/google/b/c/cx;Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/o/a/c;Landroid/content/res/Resources;)V

    invoke-virtual {v2}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/support/a;->a(Ljava/util/List;)V

    .line 335
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/support/a;->show()V

    .line 337
    :cond_0
    return-void
.end method
