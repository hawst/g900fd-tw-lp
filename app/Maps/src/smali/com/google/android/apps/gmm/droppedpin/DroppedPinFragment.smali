.class public Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/fragments/a/b;
.implements Lcom/google/android/apps/gmm/reportmapissue/a/h;


# instance fields
.field public a:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field b:Lcom/google/android/apps/gmm/base/placelists/p;

.field public c:Lcom/google/android/apps/gmm/mylocation/b/a;

.field d:Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

.field private e:Lcom/google/b/f/t;

.field private f:Lcom/google/android/apps/gmm/reportmapissue/n;

.field private g:Landroid/view/View;

.field private m:Landroid/view/View;

.field private n:Lcom/google/android/apps/gmm/place/an;

.field private o:Lcom/google/android/apps/gmm/place/m;

.field private p:Lcom/google/android/apps/gmm/base/fragments/m;

.field private q:Lcom/google/android/apps/gmm/base/placelists/o;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;
    .locals 3
    .param p2    # Lcom/google/b/f/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 115
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 116
    invoke-static {p1}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v1

    .line 117
    const-string v2, "placemark"

    invoke-virtual {p0, v0, v2, v1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 118
    if-eqz p2, :cond_0

    .line 119
    const-string v1, "source_ve_type"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 121
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;-><init>()V

    .line 122
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->setArguments(Landroid/os/Bundle;)V

    .line 123
    return-object v1
.end method

.method static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/indoor/d/f;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;
    .locals 2
    .param p2    # Lcom/google/android/apps/gmm/map/indoor/d/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/b/f/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 99
    new-instance v0, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 100
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/base/g/i;->a(Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/base/g/i;

    const/4 v1, 0x0

    .line 101
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    const/4 v1, 0x1

    .line 102
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/g;->g:Z

    sget-object v1, Lcom/google/b/f/t;->fZ:Lcom/google/b/f/t;

    .line 103
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/g;->p:Lcom/google/b/f/cq;

    .line 104
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    if-nez p2, :cond_0

    .line 105
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    .line 107
    invoke-static {p0, v0, p3}, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;

    move-result-object v0

    return-object v0

    .line 104
    :cond_0
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/g/i;->d:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/x/o;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 317
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 318
    new-instance v1, Lcom/google/b/c/cx;

    invoke-direct {v1}, Lcom/google/b/c/cx;-><init>()V

    .line 319
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 320
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v2, v2, Lcom/google/r/b/a/ads;->f:Lcom/google/n/aq;

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    :cond_0
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/google/b/c/cx;->b(Ljava/lang/Iterable;)Lcom/google/b/c/cx;

    .line 321
    iget-object v2, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->d:Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->setAddress(Ljava/util/List;)V

    .line 322
    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->d:Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->n:Lcom/google/android/apps/gmm/place/an;

    invoke-virtual {v1, p1, v2}, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V

    .line 323
    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->o:Lcom/google/android/apps/gmm/place/m;

    iget-object v2, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->d:Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    iget-object v2, v2, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/place/af;->A()Lcom/google/android/apps/gmm/base/l/a/ab;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/apps/gmm/place/m;->b:Landroid/view/View;

    const/4 v4, 0x0

    invoke-static {v3, v2, v4}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/ce;)V

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/m;->a()V

    .line 324
    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->m:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->d:Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    iget-object v2, v2, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    iget-object v2, v2, Lcom/google/android/apps/gmm/place/af;->b:Lcom/google/android/apps/gmm/base/l/j;

    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 327
    iget-object v2, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->b:Lcom/google/android/apps/gmm/base/placelists/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v2, v1, v5, v5}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/base/g/c;IZ)V

    .line 330
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/e/a/a;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/e/a/a;-><init>(Lcom/google/android/apps/gmm/base/g/c;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 333
    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->c:Lcom/google/android/apps/gmm/mylocation/b/a;

    if-nez v1, :cond_1

    .line 335
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/mylocation/b/i;->i()Lcom/google/android/apps/gmm/mylocation/b/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->c:Lcom/google/android/apps/gmm/mylocation/b/a;

    .line 336
    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->c:Lcom/google/android/apps/gmm/mylocation/b/a;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->a(Lcom/google/android/apps/gmm/base/g/b;)V

    .line 337
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->c:Lcom/google/android/apps/gmm/mylocation/b/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->a()V

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->c:Lcom/google/android/apps/gmm/mylocation/b/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->d()V

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->c:Lcom/google/android/apps/gmm/mylocation/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->d:Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/af;->a:Lcom/google/android/apps/gmm/place/j/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/mylocation/b/a;->a(Lcom/google/android/apps/gmm/place/b/a;)V

    .line 347
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 348
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/droppedpin/a;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/droppedpin/a;-><init>(Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 356
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/e/a/a/a/b;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 284
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->f:Lcom/google/android/apps/gmm/reportmapissue/n;

    if-nez v0, :cond_0

    .line 291
    :goto_0
    return-void

    .line 289
    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->f:Lcom/google/android/apps/gmm/reportmapissue/n;

    .line 290
    const/16 v0, 0x1a

    invoke-virtual {p1, v3, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_3

    new-instance v2, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/g/g;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v2

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/base/g/g;->g:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iget-object v3, v0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    array-length v3, v3

    if-gtz v3, :cond_2

    :cond_1
    move-object v0, v1

    :goto_1
    iput-object v0, v2, Lcom/google/android/apps/gmm/base/g/g;->p:Lcom/google/b/f/cq;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/x/o;->b(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a(Lcom/google/android/apps/gmm/x/o;)V

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    aget-object v0, v0, v4

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->hQ:I

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->f()Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v0

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 366
    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->b:Lcom/google/android/apps/gmm/base/placelists/p;

    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/base/g/c;IZ)V

    .line 367
    return-void
.end method

.method public final af_()V
    .locals 3

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->hQ:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 361
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 362
    return-void
.end method

.method public final e_()Lcom/google/android/apps/gmm/base/fragments/a/a;
    .locals 1

    .prologue
    .line 274
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->b:Lcom/google/android/apps/gmm/base/fragments/a/a;

    return-object v0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 279
    sget-object v0, Lcom/google/b/f/t;->et:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final m()Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v1, :cond_0

    .line 373
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->c:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 375
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->b:Lcom/google/android/apps/gmm/feedback/a/d;

    goto :goto_0
.end method

.method public final n()Lcom/google/android/apps/gmm/base/g/c;
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 130
    if-eqz p1, :cond_1

    .line 131
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "placemark"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a:Lcom/google/android/apps/gmm/x/o;

    .line 133
    const-string v0, "source_ve_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 134
    instance-of v1, v0, Lcom/google/b/f/t;

    if-eqz v1, :cond_0

    .line 135
    check-cast v0, Lcom/google/b/f/t;

    iput-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->e:Lcom/google/b/f/t;

    .line 138
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/o;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/placelists/o;-><init>(Lcom/google/android/apps/gmm/z/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->q:Lcom/google/android/apps/gmm/base/placelists/o;

    .line 139
    return-void

    .line 130
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 144
    new-instance v0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->d:Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->d:Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    .line 147
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/f/b;->a(Lcom/google/android/libraries/curvular/bd;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->g:Landroid/view/View;

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->g:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/gmm/base/f/b;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->m:Landroid/view/View;

    .line 151
    new-instance v0, Lcom/google/android/apps/gmm/place/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->q:Lcom/google/android/apps/gmm/base/placelists/o;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/gmm/place/ac;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Landroid/app/Fragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->n:Lcom/google/android/apps/gmm/place/an;

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->d:Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a:Lcom/google/android/apps/gmm/x/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->n:Lcom/google/android/apps/gmm/place/an;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V

    .line 156
    new-instance v0, Lcom/google/android/apps/gmm/place/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/place/m;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->o:Lcom/google/android/apps/gmm/place/m;

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->o:Lcom/google/android/apps/gmm/place/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->d:Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/af;->A()Lcom/google/android/apps/gmm/base/l/a/ab;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/m;->b:Landroid/view/View;

    invoke-static {v2, v1, v3}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/ce;)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/m;->a()V

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->d:Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/af;->b:Lcom/google/android/apps/gmm/base/l/j;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 161
    return-object v3
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 253
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onDestroy()V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->f:Lcom/google/android/apps/gmm/reportmapissue/n;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->f:Lcom/google/android/apps/gmm/reportmapissue/n;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/reportmapissue/n;->f()V

    .line 258
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->f:Lcom/google/android/apps/gmm/reportmapissue/n;

    .line 261
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 242
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->b:Lcom/google/android/apps/gmm/base/placelists/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/p;->a()V

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->c:Lcom/google/android/apps/gmm/mylocation/b/a;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->c:Lcom/google/android/apps/gmm/mylocation/b/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->b()V

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->p:Lcom/google/android/apps/gmm/base/fragments/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/m;->b()V

    .line 248
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->p:Lcom/google/android/apps/gmm/base/fragments/m;

    .line 249
    return-void
.end method

.method public onResume()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v6, 0x0

    .line 166
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 168
    new-instance v0, Lcom/google/android/apps/gmm/base/placelists/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->d:Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    iget-object v3, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->q:Lcom/google/android/apps/gmm/base/placelists/o;

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/google/android/apps/gmm/base/placelists/p;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/place/l;Lcom/google/android/apps/gmm/base/placelists/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->b:Lcom/google/android/apps/gmm/base/placelists/p;

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->c:Lcom/google/android/apps/gmm/mylocation/b/a;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->c:Lcom/google/android/apps/gmm/mylocation/b/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->a()V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->c:Lcom/google/android/apps/gmm/mylocation/b/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->d()V

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->c:Lcom/google/android/apps/gmm/mylocation/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->d:Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/af;->a:Lcom/google/android/apps/gmm/place/j/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/mylocation/b/a;->a(Lcom/google/android/apps/gmm/place/b/a;)V

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/base/g/c;

    iget-boolean v0, v1, Lcom/google/android/apps/gmm/base/g/c;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a(Lcom/google/android/apps/gmm/x/o;)V

    .line 179
    :goto_0
    iget-object v8, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v9, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->g:Landroid/view/View;

    sget v10, Lcom/google/android/apps/gmm/g;->aM:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->o:Lcom/google/android/apps/gmm/place/m;

    .line 180
    iget-object v11, v0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    iget-object v12, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->b:Lcom/google/android/apps/gmm/base/placelists/p;

    move-object v7, p0

    .line 178
    invoke-static/range {v7 .. v13}, Lcom/google/android/apps/gmm/base/fragments/m;->a(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/base/activities/c;Landroid/view/View;ILandroid/view/View;Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;Lcom/google/android/apps/gmm/base/l/aj;)Lcom/google/android/apps/gmm/base/fragments/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->p:Lcom/google/android/apps/gmm/base/fragments/m;

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->p:Lcom/google/android/apps/gmm/base/fragments/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/m;->a()Lcom/google/android/apps/gmm/base/fragments/m;

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->d:Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->b:Lcom/google/android/apps/gmm/base/placelists/p;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/base/placelists/p;->d:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a(Z)V

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->b:Lcom/google/android/apps/gmm/base/placelists/p;

    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v2, v0, v6, v6}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/base/g/c;IZ)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v3, -0x1

    invoke-static {v1, v0, v2, v3, v13}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/map/MapFragment;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/base/activities/c;ILcom/google/android/apps/gmm/map/p;)V

    .line 191
    :cond_1
    return-void

    .line 176
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->e:Lcom/google/b/f/t;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->e:Lcom/google/b/f/t;

    new-instance v2, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    move-object v2, v0

    :goto_1
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->T()Lcom/google/b/c/cv;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/b/c/cv;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->T()Lcom/google/b/c/cv;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/indoor/d/f;

    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move-object v7, p0

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/reportmapissue/n;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/indoor/d/f;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/p/b/a;Landroid/content/res/Resources;ZLcom/google/android/apps/gmm/reportmapissue/a/h;)Lcom/google/android/apps/gmm/reportmapissue/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->f:Lcom/google/android/apps/gmm/reportmapissue/n;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->f:Lcom/google/android/apps/gmm/reportmapissue/n;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    goto/16 :goto_0

    :cond_3
    move-object v1, v13

    goto :goto_2

    :cond_4
    move-object v2, v13

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 265
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "placemark"

    iget-object v2, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->e:Lcom/google/b/f/t;

    if-eqz v0, :cond_0

    .line 268
    const-string v0, "source_ve_type"

    iget-object v1, p0, Lcom/google/android/apps/gmm/droppedpin/DroppedPinFragment;->e:Lcom/google/b/f/t;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 270
    :cond_0
    return-void
.end method
