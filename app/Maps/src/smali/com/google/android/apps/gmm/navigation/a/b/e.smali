.class public Lcom/google/android/apps/gmm/navigation/a/b/e;
.super Lcom/google/android/apps/gmm/navigation/a/b/a;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/navigation/a/b/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/a/b/a;)V
    .locals 0

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/a/b/a;-><init>()V

    .line 222
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/b/e;->a:Lcom/google/android/apps/gmm/navigation/a/b/a;

    .line 223
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 241
    instance-of v0, p1, Lcom/google/android/apps/gmm/navigation/a/b/e;

    if-eqz v0, :cond_0

    .line 242
    check-cast p1, Lcom/google/android/apps/gmm/navigation/a/b/e;

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/a/b/e;->a:Lcom/google/android/apps/gmm/navigation/a/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/b/e;->a:Lcom/google/android/apps/gmm/navigation/a/b/a;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 244
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/b/e;->a:Lcom/google/android/apps/gmm/navigation/a/b/a;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/b/e;->a:Lcom/google/android/apps/gmm/navigation/a/b/a;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x15

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "<optional>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</optional>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
