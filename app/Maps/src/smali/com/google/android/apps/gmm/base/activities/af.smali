.class Lcom/google/android/apps/gmm/base/activities/af;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/activities/ae;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/activities/ae;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/activities/af;->a:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    .line 23
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 28
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 25
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/af;->a:Lcom/google/android/apps/gmm/base/activities/ae;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    if-nez v0, :cond_2

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/ae;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/ae;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/ae;->c:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v0, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/ae;->c:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/ae;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/p;->m:Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    if-eqz v3, :cond_0

    sget v3, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/p;->m:Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_0
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/activities/ae;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/base/activities/ae;->e:Z

    iget-boolean v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->M:Z

    if-nez v0, :cond_2

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->Q:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    const/4 v4, 0x1

    new-instance v5, Lcom/google/android/apps/gmm/base/activities/ad;

    iget-object v6, v2, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v7, v2, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    invoke-direct {v5, v2, v4, v6, v7}, Lcom/google/android/apps/gmm/base/activities/ad;-><init>(Lcom/google/android/apps/gmm/base/activities/p;ILcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/map/b/a/f;)V

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    iget v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_3

    new-instance v0, Lcom/google/android/apps/gmm/base/activities/q;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/apps/gmm/base/activities/q;-><init>(Lcom/google/android/apps/gmm/base/activities/p;Lcom/google/android/apps/gmm/base/activities/c;Z)V

    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Ljava/lang/Runnable;)V

    .line 30
    :cond_2
    :goto_0
    return-void

    .line 25
    :cond_3
    invoke-virtual {v2, v3, v1}, Lcom/google/android/apps/gmm/base/activities/p;->a(Lcom/google/android/apps/gmm/base/activities/c;Z)V

    goto :goto_0

    .line 23
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
