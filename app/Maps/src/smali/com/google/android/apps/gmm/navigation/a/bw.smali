.class public Lcom/google/android/apps/gmm/navigation/a/bw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/a/g;
.implements Lcom/google/android/gms/common/api/q;
.implements Lcom/google/android/gms/common/api/r;


# static fields
.field public static final a:[J

.field public static final b:[J

.field public static final c:[J


# instance fields
.field public final d:Lcom/google/android/gms/common/api/o;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 28
    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bw;->a:[J

    .line 29
    const/4 v0, 0x4

    new-array v0, v0, [J

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bw;->b:[J

    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [J

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    aput-wide v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bw;->c:[J

    return-void

    .line 28
    nop

    :array_0
    .array-data 8
        0x0
        0xfa
    .end array-data

    .line 29
    :array_1
    .array-data 8
        0x0
        0xfa
        0xfa
        0xfa
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/google/android/gms/common/api/p;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/p;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/wearable/p;->d:Lcom/google/android/gms/common/api/a;

    .line 36
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    .line 37
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/q;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    .line 38
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/p;->a()Lcom/google/android/gms/common/api/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bw;->d:Lcom/google/android/gms/common/api/o;

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bw;->d:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->b()V

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/navigation/a/b/h;)Lcom/google/android/apps/gmm/navigation/a/a;
    .locals 3

    .prologue
    .line 48
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/a/b/h;->c:Lcom/google/android/apps/gmm/navigation/a/b/j;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/b/j;->a:Lcom/google/android/apps/gmm/navigation/a/b/j;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bw;->a:[J

    .line 49
    :goto_0
    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/bv;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/bw;->d:Lcom/google/android/gms/common/api/o;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/navigation/a/bv;-><init>(Lcom/google/android/gms/common/api/o;[J)V

    return-object v1

    .line 48
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/b/j;->b:Lcom/google/android/apps/gmm/navigation/a/b/j;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bw;->b:[J

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bw;->c:[J

    goto :goto_0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 80
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bw;->d:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->c()V

    .line 74
    return-void
.end method
