.class abstract Lcom/google/android/apps/gmm/o/af;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/an;


# instance fields
.field private final a:Lcom/google/android/libraries/curvular/aw;

.field private final b:Ljava/lang/CharSequence;

.field private final c:Ljava/lang/CharSequence;

.field private final d:Lcom/google/android/apps/gmm/z/b/l;

.field private final e:Lcom/google/android/libraries/curvular/aw;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 47
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/o/af;-><init>(Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;Lcom/google/android/libraries/curvular/aw;)V

    .line 48
    return-void
.end method

.method constructor <init>(Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;Lcom/google/android/libraries/curvular/aw;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/android/apps/gmm/o/af;->a:Lcom/google/android/libraries/curvular/aw;

    .line 54
    iput-object p2, p0, Lcom/google/android/apps/gmm/o/af;->b:Ljava/lang/CharSequence;

    .line 55
    iput-object p3, p0, Lcom/google/android/apps/gmm/o/af;->c:Ljava/lang/CharSequence;

    .line 56
    iput-object p4, p0, Lcom/google/android/apps/gmm/o/af;->d:Lcom/google/android/apps/gmm/z/b/l;

    .line 57
    iput-object p5, p0, Lcom/google/android/apps/gmm/o/af;->e:Lcom/google/android/libraries/curvular/aw;

    .line 58
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/af;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public b()Lcom/google/android/libraries/curvular/aw;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/af;->e:Lcom/google/android/libraries/curvular/aw;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/af;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/af;->d:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public g()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 89
    const/4 v0, 0x0

    return-object v0
.end method

.method public h()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/af;->a:Lcom/google/android/libraries/curvular/aw;

    return-object v0
.end method

.method public j()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return-object v0
.end method
