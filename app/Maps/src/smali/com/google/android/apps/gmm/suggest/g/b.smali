.class public Lcom/google/android/apps/gmm/suggest/g/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/suggest/f/a;


# instance fields
.field private final a:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/g/d;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/search/d/c;

.field private final c:Lcom/google/android/apps/gmm/suggest/g/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/suggest/d;Ljava/util/List;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/search/aq;Lcom/google/android/apps/gmm/place/b/b;Lcom/google/android/apps/gmm/suggest/g/a;)V
    .locals 6
    .param p4    # Lcom/google/android/apps/gmm/x/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/suggest/d;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/d;",
            ">;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;",
            "Lcom/google/android/apps/gmm/search/aq;",
            "Lcom/google/android/apps/gmm/place/b/b;",
            "Lcom/google/android/apps/gmm/suggest/g/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v3

    .line 38
    const/4 v0, 0x0

    .line 39
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/suggest/e/d;

    .line 40
    new-instance v5, Lcom/google/android/apps/gmm/suggest/g/d;

    add-int/lit8 v2, v1, 0x1

    invoke-direct {v5, p1, p2, v0, v1}, Lcom/google/android/apps/gmm/suggest/g/d;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/suggest/d;Lcom/google/android/apps/gmm/suggest/e/d;I)V

    invoke-virtual {v3, v5}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    move v1, v2

    .line 42
    goto :goto_0

    .line 43
    :cond_0
    invoke-virtual {v3}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/b;->a:Lcom/google/b/c/cv;

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/search/d/c;

    invoke-direct {v0, p5, p6}, Lcom/google/android/apps/gmm/search/d/c;-><init>(Lcom/google/android/apps/gmm/search/aq;Lcom/google/android/apps/gmm/place/b/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/b;->b:Lcom/google/android/apps/gmm/search/d/c;

    .line 46
    if-eqz p4, :cond_1

    .line 47
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/g/b;->b:Lcom/google/android/apps/gmm/search/d/c;

    invoke-virtual {p4}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/search/d/c;->a(Lcom/google/android/apps/gmm/search/al;)V

    .line 51
    :goto_1
    iput-object p7, p0, Lcom/google/android/apps/gmm/suggest/g/b;->c:Lcom/google/android/apps/gmm/suggest/g/a;

    .line 52
    return-void

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/b;->b:Lcom/google/android/apps/gmm/search/d/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/d/c;->a(Lcom/google/android/apps/gmm/search/al;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/suggest/g/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/b;->a:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/search/e/b;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/b;->b:Lcom/google/android/apps/gmm/search/d/c;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/base/l/a/e;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/b;->c:Lcom/google/android/apps/gmm/suggest/g/a;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/b;->a:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/b;->a:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/b;->c:Lcom/google/android/apps/gmm/suggest/g/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/l/a/e;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
