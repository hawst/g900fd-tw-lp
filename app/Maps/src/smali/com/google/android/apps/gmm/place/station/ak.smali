.class Lcom/google/android/apps/gmm/place/station/ak;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/b/c;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/activities/c;

.field final synthetic b:Lcom/google/android/libraries/curvular/ag;

.field final synthetic c:Lcom/google/android/apps/gmm/place/station/ai;

.field final synthetic d:Lcom/google/android/apps/gmm/place/station/ai;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/station/ai;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/libraries/curvular/ag;Lcom/google/android/apps/gmm/place/station/ai;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/station/ak;->d:Lcom/google/android/apps/gmm/place/station/ai;

    iput-object p2, p0, Lcom/google/android/apps/gmm/place/station/ak;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iput-object p3, p0, Lcom/google/android/apps/gmm/place/station/ak;->b:Lcom/google/android/libraries/curvular/ag;

    iput-object p4, p0, Lcom/google/android/apps/gmm/place/station/ak;->c:Lcom/google/android/apps/gmm/place/station/ai;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/shared/net/i;)V
    .locals 3

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ak;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/ak;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 340
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->iU:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 338
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 342
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 343
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/i;Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 16
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/station/ak;->a:Lcom/google/android/apps/gmm/base/activities/c;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/station/ak;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v2, :cond_1

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 328
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/place/station/ak;->d:Lcom/google/android/apps/gmm/place/station/ai;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/station/ak;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 329
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 328
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v3, v3, Lcom/google/r/b/a/ads;->s:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/wb;->j()Lcom/google/maps/g/wb;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/wb;

    iget-object v4, v13, Lcom/google/android/apps/gmm/place/station/ai;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v5, v13, Lcom/google/android/apps/gmm/place/station/ai;->a:Ljava/lang/String;

    if-nez v3, :cond_2

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_2
    if-nez v4, :cond_6

    const-string v4, ""

    :goto_1
    invoke-virtual {v3}, Lcom/google/maps/g/wb;->h()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/vp;

    invoke-virtual {v3}, Lcom/google/maps/g/vp;->d()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/vx;

    invoke-virtual {v3}, Lcom/google/maps/g/vx;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {v3}, Lcom/google/maps/g/vx;->h()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/vk;

    invoke-virtual {v3}, Lcom/google/maps/g/vk;->d()Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    move-object v12, v3

    :goto_2
    if-eqz v12, :cond_a

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v14

    iget-object v3, v13, Lcom/google/android/apps/gmm/place/station/ai;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v4, v13, Lcom/google/android/apps/gmm/place/station/ai;->d:Ljava/lang/String;

    iget-object v5, v13, Lcom/google/android/apps/gmm/place/station/ai;->f:Ljava/util/List;

    iget-object v6, v13, Lcom/google/android/apps/gmm/place/station/ai;->g:Ljava/lang/String;

    iget-object v7, v13, Lcom/google/android/apps/gmm/place/station/ai;->h:Lcom/google/android/libraries/curvular/a;

    iget-object v8, v13, Lcom/google/android/apps/gmm/place/station/ai;->i:Lcom/google/android/libraries/curvular/a;

    iget-object v9, v13, Lcom/google/android/apps/gmm/place/station/ai;->j:Lcom/google/android/libraries/curvular/a;

    iget-object v10, v13, Lcom/google/android/apps/gmm/place/station/ai;->k:Lcom/google/android/apps/gmm/base/views/c/b;

    iget-object v11, v13, Lcom/google/android/apps/gmm/place/station/ai;->l:Lcom/google/android/apps/gmm/base/views/c/b;

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/gmm/place/station/ai;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/maps/g/vk;)Lcom/google/b/c/cv;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/place/station/al;

    invoke-direct {v3, v13, v14, v15}, Lcom/google/android/apps/gmm/place/station/al;-><init>(Lcom/google/android/apps/gmm/place/station/ai;J)V

    if-nez v2, :cond_8

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_6
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/j;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    :cond_7
    const/4 v12, 0x0

    goto :goto_2

    :cond_8
    if-nez v3, :cond_9

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_9
    new-instance v4, Lcom/google/b/c/ee;

    invoke-direct {v4, v2, v3}, Lcom/google/b/c/ee;-><init>(Ljava/lang/Iterable;Lcom/google/b/a/ar;)V

    invoke-static {v4}, Lcom/google/b/c/cv;->a(Ljava/lang/Iterable;)Lcom/google/b/c/cv;

    move-result-object v2

    iput-object v2, v13, Lcom/google/android/apps/gmm/place/station/ai;->b:Lcom/google/b/c/cv;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v13, Lcom/google/android/apps/gmm/place/station/ai;->m:Ljava/lang/Boolean;

    .line 331
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/station/ak;->b:Lcom/google/android/libraries/curvular/ag;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/station/ak;->c:Lcom/google/android/apps/gmm/place/station/ai;

    invoke-interface {v2, v3}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
