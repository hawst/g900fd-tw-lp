.class public Lcom/google/android/apps/gmm/base/k/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# static fields
.field private static final g:I


# instance fields
.field final a:Landroid/view/View;

.field b:Z

.field final c:Landroid/graphics/Point;

.field d:Z

.field e:Z

.field f:Landroid/app/FragmentManager$OnBackStackChangedListener;

.field private final h:Lcom/google/android/apps/gmm/z/a/b;

.field private final i:[I

.field private j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    sget v0, Lcom/google/android/apps/gmm/g;->aV:I

    sput v0, Lcom/google/android/apps/gmm/base/k/g;->g:I

    return-void
.end method

.method private constructor <init>(Landroid/view/View;Lcom/google/android/apps/gmm/z/a/b;)V
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/g;->c:Landroid/graphics/Point;

    .line 62
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/g;->i:[I

    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/base/k/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/k/h;-><init>(Lcom/google/android/apps/gmm/base/k/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/g;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 87
    new-instance v0, Lcom/google/android/apps/gmm/base/k/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/k/i;-><init>(Lcom/google/android/apps/gmm/base/k/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/g;->f:Landroid/app/FragmentManager$OnBackStackChangedListener;

    .line 142
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/k/g;->a:Landroid/view/View;

    .line 143
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/k/g;->h:Lcom/google/android/apps/gmm/z/a/b;

    .line 144
    return-void
.end method

.method public static a(Landroid/view/View;Lcom/google/android/apps/gmm/z/a/b;)Lcom/google/android/apps/gmm/base/k/g;
    .locals 2

    .prologue
    .line 108
    sget v0, Lcom/google/android/apps/gmm/base/k/g;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/k/g;

    .line 109
    if-nez v0, :cond_0

    .line 110
    new-instance v0, Lcom/google/android/apps/gmm/base/k/g;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/base/k/g;-><init>(Landroid/view/View;Lcom/google/android/apps/gmm/z/a/b;)V

    .line 111
    sget v1, Lcom/google/android/apps/gmm/base/k/g;->g:I

    invoke-virtual {p0, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 112
    invoke-virtual {p0, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 114
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/k/g;->b:Z

    .line 115
    return-object v0
.end method

.method public static a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 122
    sget v0, Lcom/google/android/apps/gmm/base/k/g;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/k/g;

    .line 123
    if-eqz v0, :cond_0

    .line 124
    sget v1, Lcom/google/android/apps/gmm/base/k/g;->g:I

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 125
    invoke-virtual {p0, v0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 127
    :cond_0
    return-void
.end method

.method private a(II)Z
    .locals 1

    .prologue
    .line 231
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/g;->c:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    if-ge p1, v0, :cond_0

    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/g;->c:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    if-ge p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method public onPreDraw()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 153
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/k/g;->b:Z

    if-nez v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/g;->a:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/i;->a(Landroid/view/View;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_0

    .line 157
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/g;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 158
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/g;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 161
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 180
    :cond_0
    :goto_0
    return v6

    .line 165
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/k/g;->a:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/k/g;->i:[I

    invoke-virtual {v3, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 166
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/k/g;->i:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    .line 167
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/k/g;->i:[I

    aget v4, v4, v6

    .line 171
    add-int/2addr v1, v3

    add-int/lit8 v1, v1, -0x1

    .line 172
    add-int/2addr v2, v4

    add-int/lit8 v2, v2, -0x1

    .line 173
    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/gmm/base/k/g;->a(II)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-direct {p0, v1, v4}, Lcom/google/android/apps/gmm/base/k/g;->a(II)Z

    move-result v4

    if-nez v4, :cond_2

    .line 174
    invoke-direct {p0, v3, v2}, Lcom/google/android/apps/gmm/base/k/g;->a(II)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/gmm/base/k/g;->a(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 175
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/g;->h:Lcom/google/android/apps/gmm/z/a/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/l;)V

    .line 176
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/base/k/g;->b:Z

    goto :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 199
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/k/g;->b:Z

    .line 200
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 201
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/g;->c:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 202
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 203
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 204
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/k/g;->d:Z

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/g;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/g;->f:Landroid/app/FragmentManager$OnBackStackChangedListener;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 208
    :cond_0
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 218
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/k/g;->d:Z

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/g;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/g;->f:Landroid/app/FragmentManager$OnBackStackChangedListener;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->removeOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 221
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 222
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 224
    return-void
.end method
