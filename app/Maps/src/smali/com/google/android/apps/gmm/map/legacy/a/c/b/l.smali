.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;
.super Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;
.source "PG"


# instance fields
.field private A:F

.field private B:Lcom/google/android/apps/gmm/map/legacy/a/c/b/n;

.field private C:Lcom/google/android/apps/gmm/v/by;

.field private D:Lcom/google/android/apps/gmm/map/o/c/d;

.field private E:Lcom/google/android/apps/gmm/map/o/h;

.field private F:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

.field private G:Lcom/google/android/apps/gmm/map/f/o;

.field private H:Landroid/content/res/Resources;

.field private final I:Lcom/google/android/apps/gmm/map/b/a/ay;

.field public final a:Lcom/google/android/apps/gmm/map/o/b/a;

.field public b:Lcom/google/android/apps/gmm/map/o/b/c;

.field public c:Lcom/google/android/apps/gmm/map/o/b/g;

.field public d:Z

.field public e:I

.field public f:Lcom/google/android/apps/gmm/map/b/a/y;

.field public g:Lcom/google/android/apps/gmm/map/o/b/f;

.field public h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

.field private z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;-><init>()V

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    .line 91
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->I:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 94
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;Lcom/google/android/apps/gmm/map/o/b/c;Lcom/google/android/apps/gmm/map/o/b/g;Lcom/google/android/apps/gmm/map/o/ak;ILcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/t/l;Landroid/content/res/Resources;)V
    .locals 10

    .prologue
    .line 39
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/gmm/map/o/af;->a:Lcom/google/android/apps/gmm/map/o/af;

    move-object v1, p0

    move v3, p4

    move-object v4, p3

    move-object/from16 v9, p8

    invoke-super/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a(Lcom/google/android/apps/gmm/map/internal/c/m;ILcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/internal/c/be;FILcom/google/android/apps/gmm/map/o/af;Lcom/google/android/apps/gmm/map/t/l;)V

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->b:Lcom/google/android/apps/gmm/map/o/b/c;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->F:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->E:Lcom/google/android/apps/gmm/map/o/h;

    iput-object p5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->G:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, p2, Lcom/google/android/apps/gmm/map/o/b/g;->n:Lcom/google/b/c/dn;

    sget-object v2, Lcom/google/android/apps/gmm/map/o/b/f;->e:Lcom/google/android/apps/gmm/map/o/b/f;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/google/b/c/eg;->b(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/o/b/f;

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->g:Lcom/google/android/apps/gmm/map/o/b/f;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->c:Lcom/google/android/apps/gmm/map/o/b/g;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->A:F

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->H:Landroid/content/res/Resources;

    return-void
.end method

.method private b(Lcom/google/android/apps/gmm/map/t/b;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 214
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/t/b;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 231
    :cond_0
    :goto_0
    return v0

    .line 218
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    if-eqz v1, :cond_0

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->C:Lcom/google/android/apps/gmm/v/by;

    if-eqz v0, :cond_2

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->C:Lcom/google/android/apps/gmm/v/by;

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/bw;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/v/bz;->a(Lcom/google/android/apps/gmm/v/by;)V

    .line 227
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/map/o/c/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->E:Lcom/google/android/apps/gmm/map/o/h;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->H:Landroid/content/res/Resources;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/o/c/d;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;Lcom/google/android/apps/gmm/map/o/h;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->D:Lcom/google/android/apps/gmm/map/o/c/d;

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->D:Lcom/google/android/apps/gmm/map/o/c/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/o/c/d;->a(Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/v/by;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->C:Lcom/google/android/apps/gmm/v/by;

    .line 231
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private l()Z
    .locals 6

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    .line 196
    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->b:F

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->h:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    .line 197
    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->c:F

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->i:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 198
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    .line 199
    iget v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->h:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget v2, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->i:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->g:Lcom/google/android/apps/gmm/map/o/b/f;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->c:Lcom/google/android/apps/gmm/map/o/b/g;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->G:Lcom/google/android/apps/gmm/map/f/o;

    .line 200
    iget v5, v5, Lcom/google/android/apps/gmm/map/f/o;->i:F

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;-><init>(FFLcom/google/android/apps/gmm/map/o/b/f;Lcom/google/android/apps/gmm/map/o/b/g;F)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    .line 201
    const/4 v0, 0x1

    .line 204
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/o/b/f;FLcom/google/android/apps/gmm/map/legacy/a/c/b/n;)V
    .locals 3
    .param p4    # Lcom/google/android/apps/gmm/map/legacy/a/c/b/n;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 361
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eq p1, v2, :cond_0

    if-eqz p1, :cond_3

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->g:Lcom/google/android/apps/gmm/map/o/b/f;

    if-ne p2, v2, :cond_4

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->A:F

    cmpl-float v2, p3, v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->B:Lcom/google/android/apps/gmm/map/legacy/a/c/b/n;

    .line 364
    if-eq p4, v2, :cond_1

    if-eqz p4, :cond_2

    invoke-virtual {p4, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    if-eqz v0, :cond_4

    .line 383
    :goto_1
    return-void

    :cond_3
    move v2, v0

    .line 361
    goto :goto_0

    .line 368
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 370
    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 371
    iput p3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->A:F

    .line 372
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->B:Lcom/google/android/apps/gmm/map/legacy/a/c/b/n;

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->g:Lcom/google/android/apps/gmm/map/o/b/f;

    if-eq v0, p2, :cond_6

    .line 375
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->g:Lcom/google/android/apps/gmm/map/o/b/f;

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    if-eqz v0, :cond_5

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a(Lcom/google/android/apps/gmm/map/o/b/f;)V

    .line 379
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/z;I)V
    .locals 6

    .prologue
    .line 399
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 402
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->G:Lcom/google/android/apps/gmm/map/f/o;

    .line 404
    iget v1, v0, Lcom/google/android/apps/gmm/map/f/o;->i:F

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->F:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    .line 402
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/z;FLcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;Lcom/google/android/apps/gmm/map/internal/c/d;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    move-result-object v0

    .line 409
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    if-eqz v1, :cond_0

    .line 410
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a()V

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 413
    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    .line 414
    iput p2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->e:I

    .line 416
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->d:Z

    .line 418
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->u:Lcom/google/android/apps/gmm/map/o/ap;

    if-eqz v0, :cond_1

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->u:Lcom/google/android/apps/gmm/map/o/ap;

    sget-object v2, Lcom/google/android/apps/gmm/map/o/ar;->b:Lcom/google/android/apps/gmm/map/o/ar;

    iget v3, v1, Lcom/google/android/apps/gmm/map/o/ap;->n:F

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/o/ap;->b:Z

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/gmm/map/o/ap;->a(Lcom/google/android/apps/gmm/map/o/ar;FZ)Lcom/google/android/apps/gmm/map/o/ap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/o/ap;)Z

    .line 420
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->l()Z

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->t:Lcom/google/android/apps/gmm/map/t/b;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->b(Lcom/google/android/apps/gmm/map/t/b;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 422
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 426
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 427
    return-void

    .line 426
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/ac;Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 2

    .prologue
    .line 356
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->b:Lcom/google/android/apps/gmm/map/o/b/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/o/b/d;-><init>(Lcom/google/android/apps/gmm/map/o/b/c;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 357
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/b;)V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 245
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->t:Lcom/google/android/apps/gmm/map/t/b;

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 258
    :goto_0
    return-void

    .line 249
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->t:Lcom/google/android/apps/gmm/map/t/b;

    .line 251
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->b(Lcom/google/android/apps/gmm/map/t/b;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 255
    :cond_1
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->d:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/o/ap;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 136
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->u:Lcom/google/android/apps/gmm/map/o/ap;

    if-ne v1, p1, :cond_0

    .line 153
    :goto_0
    return v0

    .line 140
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 143
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    sget-object v2, Lcom/google/android/apps/gmm/map/o/ar;->b:Lcom/google/android/apps/gmm/map/o/ar;

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/ap;->n:F

    iget-boolean v4, p1, Lcom/google/android/apps/gmm/map/o/ap;->b:Z

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/map/o/ap;->a(Lcom/google/android/apps/gmm/map/o/ar;FZ)Lcom/google/android/apps/gmm/map/o/ap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/o/ap;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    const/4 v0, 0x0

    goto :goto_0

    .line 147
    :cond_1
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->d:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 152
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->u:Lcom/google/android/apps/gmm/map/o/ap;

    goto :goto_0

    .line 149
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 162
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/o/au;->f:[F

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_2

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p2, v0, v2}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    .line 171
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 176
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->d:Z

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    const/4 v1, 0x0

    aget v1, v2, v1

    const/4 v3, 0x1

    aget v2, v2, v3

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->d:Lcom/google/android/apps/gmm/map/o/b/f;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a(FFFLcom/google/android/apps/gmm/map/o/b/f;Lcom/google/android/apps/gmm/map/o/b/a;)Lcom/google/android/apps/gmm/map/o/b/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 187
    return v6

    .line 167
    :cond_2
    aput v1, v2, v3

    .line 168
    aput v1, v2, v6

    goto :goto_0

    .line 184
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/bc;Z)Z
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 264
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v1

    if-nez v1, :cond_0

    .line 270
    :goto_0
    return v0

    .line 268
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->f:Lcom/google/android/apps/gmm/map/b/a/y;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_2

    move v0, v8

    .line 270
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 268
    :cond_2
    :try_start_1
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->n:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    move v0, v8

    goto :goto_1

    :cond_3
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/o/au;->f:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->B:Lcom/google/android/apps/gmm/map/legacy/a/c/b/n;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->B:Lcom/google/android/apps/gmm/map/legacy/a/c/b/n;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->I:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-interface {v1, p2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/n;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->I:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    aput v3, v2, v1

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->I:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    aput v3, v2, v1

    :goto_2
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->d:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    aget v1, v2, v0

    const/4 v0, 0x1

    aget v2, v2, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->A:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->g:Lcom/google/android/apps/gmm/map/o/b/f;

    iget-object v5, p1, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a(FFFLcom/google/android/apps/gmm/map/o/b/f;Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->A:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->g:Lcom/google/android/apps/gmm/map/o/b/f;

    iget-object v5, p1, Lcom/google/android/apps/gmm/map/o/au;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->b(FFFLcom/google/android/apps/gmm/map/o/b/f;Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->C:Lcom/google/android/apps/gmm/v/by;

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->C:Lcom/google/android/apps/gmm/v/by;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/au;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/au;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->A:F

    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->v:F

    sget-object v6, Lcom/google/android/apps/gmm/map/t/l;->B:Lcom/google/android/apps/gmm/map/t/l;

    move-object v0, p3

    move-object v7, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/o/bc;->a(Lcom/google/android/apps/gmm/v/by;FFFFLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/f/o;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->A:F

    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->v:F

    sget-object v6, Lcom/google/android/apps/gmm/map/t/l;->C:Lcom/google/android/apps/gmm/map/t/l;

    move-object v1, p3

    move-object v7, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/o/bc;FFFFLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/f/o;)V

    :cond_4
    if-eqz p4, :cond_5

    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/a;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget v2, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->h:F

    div-float/2addr v2, v9

    sub-float/2addr v1, v2

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget v3, v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->i:F

    div-float/2addr v3, v9

    sub-float/2addr v2, v3

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget v4, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->h:F

    div-float/2addr v4, v9

    add-float/2addr v3, v4

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget v5, v5, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->i:F

    div-float/2addr v5, v9

    add-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->D:Lcom/google/android/apps/gmm/map/t/l;

    invoke-virtual {p3, v0, v1}, Lcom/google/android/apps/gmm/map/o/bc;->a(Lcom/google/android/apps/gmm/map/o/b/b;Lcom/google/android/apps/gmm/map/t/l;)V

    :cond_5
    move v0, v8

    goto/16 :goto_1

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p2, v1, v2}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 270
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/o/ap;Lcom/google/android/apps/gmm/map/f/o;Z)Z
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0, p2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->a(Lcom/google/android/apps/gmm/map/o/ap;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/ac;Z)Z
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 348
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/t/ac;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 350
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/o/b/b;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 339
    const/4 v0, 0x1

    return v0
.end method

.method protected final k()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 123
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->z:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->C:Lcom/google/android/apps/gmm/v/by;

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->C:Lcom/google/android/apps/gmm/v/by;

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/bw;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/v/bz;->a(Lcom/google/android/apps/gmm/v/by;)V

    .line 127
    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->C:Lcom/google/android/apps/gmm/v/by;

    .line 130
    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    .line 132
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->k()V

    .line 133
    return-void
.end method
