.class public Lcom/google/android/apps/gmm/aa/a/b/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/aa/a/b/b;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/aa/a/a/a;

.field private final b:Lcom/google/b/c/dn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/util/regex/Pattern;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/aa/a/a/a;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/aa/a/a/a;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/a/b/h;->a:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 32
    invoke-static {p2}, Lcom/google/android/apps/gmm/aa/a/b/h;->a(Ljava/util/List;)Lcom/google/b/c/dn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/a/b/h;->b:Lcom/google/b/c/dn;

    .line 33
    return-void
.end method

.method private static a(Ljava/util/List;)Lcom/google/b/c/dn;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/util/regex/Pattern;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    invoke-static {}, Lcom/google/b/c/dn;->h()Lcom/google/b/c/dp;

    move-result-object v1

    .line 23
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 24
    const-string v3, "*"

    const-string v4, "(.+)"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "^"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "$"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 25
    const/4 v3, 0x2

    invoke-static {v0, v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/b/c/dp;->b(Ljava/lang/Object;)Lcom/google/b/c/dp;

    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {v1}, Lcom/google/b/c/dp;->a()Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/apps/gmm/aa/a/b/d;
    .locals 7

    .prologue
    .line 37
    const/4 v1, 0x0

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/b/h;->b:Lcom/google/b/c/dn;

    invoke-virtual {v0}, Lcom/google/b/c/dn;->b()Lcom/google/b/c/lg;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/regex/Pattern;

    .line 39
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/gmm/aa/a/b/d;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/aa/a/b/h;->a:Lcom/google/android/apps/gmm/aa/a/a/a;

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/gmm/aa/a/b/d;-><init>(FLcom/google/android/apps/gmm/aa/a/a/a;)V

    .line 40
    :goto_1
    if-eqz v1, :cond_0

    iget v3, v0, Lcom/google/android/apps/gmm/aa/a/b/d;->b:F

    iget v4, v1, Lcom/google/android/apps/gmm/aa/a/b/d;->b:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_3

    :cond_0
    :goto_2
    move-object v1, v0

    .line 43
    goto :goto_0

    .line 39
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/b/d;

    const/high16 v4, 0x3f800000    # 1.0f

    iget-object v5, p0, Lcom/google/android/apps/gmm/aa/a/b/h;->a:Lcom/google/android/apps/gmm/aa/a/a/a;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v4, v5, v3}, Lcom/google/android/apps/gmm/aa/a/b/d;-><init>(FLcom/google/android/apps/gmm/aa/a/a/a;Ljava/lang/String;)V

    goto :goto_1

    .line 44
    :cond_2
    return-object v1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 62
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "voiceAction"

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b/h;->a:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 63
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
