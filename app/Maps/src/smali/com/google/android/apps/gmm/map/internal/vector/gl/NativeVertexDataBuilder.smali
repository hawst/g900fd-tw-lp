.class public Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;
.super Lcom/google/android/apps/gmm/map/internal/vector/gl/k;
.source "PG"


# static fields
.field private static b:Lcom/google/android/apps/gmm/map/util/a/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/i",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:J

.field private c:I

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 21
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->nativeInitClass()Z

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/p;

    const/4 v1, 0x3

    const/4 v2, 0x0

    const-string v3, "NativeVertexBuilders"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/p;-><init>(ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->b:Lcom/google/android/apps/gmm/map/util/a/i;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;-><init>()V

    .line 67
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a:J

    .line 68
    return-void
.end method

.method public static a(IZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;
    .locals 4

    .prologue
    .line 58
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->b:Lcom/google/android/apps/gmm/map/util/a/i;

    monitor-enter v1

    .line 59
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->b:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;

    .line 60
    iput p0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->c:I

    iput-boolean p1, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->d:Z

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a:J

    invoke-static {v2, v3, p0, p2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->nativeSetup(JII)V

    .line 61
    monitor-exit v1

    .line 62
    return-object v0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static native nativeBuild(JLjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V
.end method

.method private static native nativeClear(J)V
.end method

.method private static native nativeFinalize(J)V
.end method

.method private static native nativeGetIndexCount(J)I
.end method

.method private static native nativeGetVertexCount(J)I
.end method

.method private static native nativeGetVertexSize(J)I
.end method

.method private static native nativeInit()J
.end method

.method private static native nativeInitClass()Z
.end method

.method private static native nativeSetup(JII)V
.end method


# virtual methods
.method public final a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 97
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a:J

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->nativeGetVertexSize(J)I

    move-result v1

    .line 98
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a:J

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->nativeGetVertexCount(J)I

    move-result v2

    .line 99
    mul-int/2addr v1, v2

    .line 100
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 103
    const/4 v3, 0x0

    .line 104
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->d:Z

    if-eqz v1, :cond_0

    .line 105
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->nativeGetIndexCount(J)I

    move-result v3

    .line 106
    shl-int/lit8 v0, v3, 0x4

    div-int/lit8 v0, v0, 0x8

    .line 107
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 108
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    move-object v4, v0

    move-object v0, v1

    .line 110
    :goto_0
    iget-wide v6, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a:J

    invoke-static {v6, v7, v5, v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->nativeBuild(JLjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 111
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/m;

    invoke-direct {v1, v5, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/m;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ShortBuffer;)V

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->c:I

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;-><init>(Lcom/google/android/apps/gmm/v/aw;IIII)V

    return-object v0

    :cond_0
    move-object v4, v0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->nativeClear(J)V

    .line 83
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->b:Lcom/google/android/apps/gmm/map/util/a/i;

    monitor-enter v1

    .line 84
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->b:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z

    .line 85
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(FF)V
    .locals 1

    .prologue
    .line 157
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(FFF)V
    .locals 1

    .prologue
    .line 188
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 203
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 228
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(III)V
    .locals 1

    .prologue
    .line 298
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(IIII)V
    .locals 1

    .prologue
    .line 293
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 1

    .prologue
    .line 162
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;F)V
    .locals 1

    .prologue
    .line 167
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;FF)V
    .locals 1

    .prologue
    .line 177
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/c/w;)V
    .locals 1

    .prologue
    .line 253
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a([FII)V
    .locals 1

    .prologue
    .line 268
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 117
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->nativeGetVertexCount(J)I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 218
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 273
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->nativeGetIndexCount(J)I

    move-result v0

    return v0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 288
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 153
    return-void
.end method

.method public final f()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 238
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 73
    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->nativeFinalize(J)V

    .line 74
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 77
    return-void

    .line 76
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public final g()[S
    .locals 1

    .prologue
    .line 243
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 137
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->nativeClear(J)V

    .line 138
    return-void
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 278
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 148
    return-void
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->nativeGetVertexCount(J)I

    move-result v0

    return v0
.end method
