.class public final enum Lcom/google/android/apps/gmm/base/h/c;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/aw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/base/h/c;",
        ">;",
        "Lcom/google/android/libraries/curvular/aw;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/base/h/c;

.field public static final enum b:Lcom/google/android/apps/gmm/base/h/c;

.field public static final enum c:Lcom/google/android/apps/gmm/base/h/c;

.field public static final enum d:Lcom/google/android/apps/gmm/base/h/c;

.field public static final enum e:Lcom/google/android/apps/gmm/base/h/c;

.field public static final enum f:Lcom/google/android/apps/gmm/base/h/c;

.field public static final enum g:Lcom/google/android/apps/gmm/base/h/c;

.field public static final enum h:Lcom/google/android/apps/gmm/base/h/c;

.field public static final enum i:Lcom/google/android/apps/gmm/base/h/c;

.field public static final enum j:Lcom/google/android/apps/gmm/base/h/c;

.field public static final enum k:Lcom/google/android/apps/gmm/base/h/c;

.field public static final enum l:Lcom/google/android/apps/gmm/base/h/c;

.field public static final enum m:Lcom/google/android/apps/gmm/base/h/c;

.field public static final enum n:Lcom/google/android/apps/gmm/base/h/c;

.field private static final synthetic r:[Lcom/google/android/apps/gmm/base/h/c;


# instance fields
.field private final o:I

.field private final p:I

.field private final q:Lcom/google/android/libraries/curvular/aw;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/base/h/c;

    const-string v1, "DEFAULT_UNCLIPPED"

    sget v3, Lcom/google/android/apps/gmm/d;->al:I

    sget-object v5, Lcom/google/android/apps/gmm/base/h/e;->a:Lcom/google/android/libraries/curvular/aw;

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/h/c;-><init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V

    sput-object v0, Lcom/google/android/apps/gmm/base/h/c;->a:Lcom/google/android/apps/gmm/base/h/c;

    .line 26
    new-instance v3, Lcom/google/android/apps/gmm/base/h/c;

    const-string v4, "DEFAULT_CLIPPED"

    sget v6, Lcom/google/android/apps/gmm/d;->al:I

    sget-object v8, Lcom/google/android/apps/gmm/base/h/e;->b:Lcom/google/android/libraries/curvular/aw;

    move v5, v9

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/base/h/c;-><init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V

    sput-object v3, Lcom/google/android/apps/gmm/base/h/c;->b:Lcom/google/android/apps/gmm/base/h/c;

    .line 28
    new-instance v3, Lcom/google/android/apps/gmm/base/h/c;

    const-string v4, "WHITE_UNCLIPPED"

    sget v6, Lcom/google/android/apps/gmm/d;->ax:I

    sget-object v8, Lcom/google/android/apps/gmm/base/h/e;->a:Lcom/google/android/libraries/curvular/aw;

    move v5, v10

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/base/h/c;-><init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V

    sput-object v3, Lcom/google/android/apps/gmm/base/h/c;->c:Lcom/google/android/apps/gmm/base/h/c;

    .line 30
    new-instance v3, Lcom/google/android/apps/gmm/base/h/c;

    const-string v4, "WHITE_CLIPPED"

    sget v6, Lcom/google/android/apps/gmm/d;->ax:I

    sget-object v8, Lcom/google/android/apps/gmm/base/h/e;->b:Lcom/google/android/libraries/curvular/aw;

    move v5, v11

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/base/h/c;-><init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V

    sput-object v3, Lcom/google/android/apps/gmm/base/h/c;->d:Lcom/google/android/apps/gmm/base/h/c;

    .line 32
    new-instance v3, Lcom/google/android/apps/gmm/base/h/c;

    const-string v4, "LIGHT_BLUE_UNCLIPPED"

    sget v6, Lcom/google/android/apps/gmm/d;->V:I

    sget-object v8, Lcom/google/android/apps/gmm/base/h/e;->a:Lcom/google/android/libraries/curvular/aw;

    move v5, v12

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/base/h/c;-><init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V

    sput-object v3, Lcom/google/android/apps/gmm/base/h/c;->e:Lcom/google/android/apps/gmm/base/h/c;

    .line 34
    new-instance v3, Lcom/google/android/apps/gmm/base/h/c;

    const-string v4, "LIGHT_BLUE_CLIPPED"

    const/4 v5, 0x5

    sget v6, Lcom/google/android/apps/gmm/d;->V:I

    sget-object v8, Lcom/google/android/apps/gmm/base/h/e;->b:Lcom/google/android/libraries/curvular/aw;

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/base/h/c;-><init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V

    sput-object v3, Lcom/google/android/apps/gmm/base/h/c;->f:Lcom/google/android/apps/gmm/base/h/c;

    .line 36
    new-instance v3, Lcom/google/android/apps/gmm/base/h/c;

    const-string v4, "WHITE_BACKGROUND"

    const/4 v5, 0x6

    sget v6, Lcom/google/android/apps/gmm/d;->al:I

    sget v7, Lcom/google/android/apps/gmm/d;->ax:I

    sget-object v8, Lcom/google/android/apps/gmm/base/h/e;->a:Lcom/google/android/libraries/curvular/aw;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/base/h/c;-><init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V

    sput-object v3, Lcom/google/android/apps/gmm/base/h/c;->g:Lcom/google/android/apps/gmm/base/h/c;

    .line 38
    new-instance v3, Lcom/google/android/apps/gmm/base/h/c;

    const-string v4, "NIGHT_UNCLIPPED"

    const/4 v5, 0x7

    sget v6, Lcom/google/android/apps/gmm/d;->at:I

    sget-object v8, Lcom/google/android/apps/gmm/base/h/e;->a:Lcom/google/android/libraries/curvular/aw;

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/base/h/c;-><init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V

    sput-object v3, Lcom/google/android/apps/gmm/base/h/c;->h:Lcom/google/android/apps/gmm/base/h/c;

    .line 40
    new-instance v3, Lcom/google/android/apps/gmm/base/h/c;

    const-string v4, "NIGHT_BACKGROUND"

    const/16 v5, 0x8

    sget v6, Lcom/google/android/apps/gmm/d;->at:I

    sget v7, Lcom/google/android/apps/gmm/d;->au:I

    sget-object v8, Lcom/google/android/apps/gmm/base/h/e;->a:Lcom/google/android/libraries/curvular/aw;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/base/h/c;-><init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V

    sput-object v3, Lcom/google/android/apps/gmm/base/h/c;->i:Lcom/google/android/apps/gmm/base/h/c;

    .line 42
    new-instance v3, Lcom/google/android/apps/gmm/base/h/c;

    const-string v4, "GREY_BACKGROUND"

    const/16 v5, 0x9

    sget v6, Lcom/google/android/apps/gmm/d;->an:I

    sget v7, Lcom/google/android/apps/gmm/d;->al:I

    sget-object v8, Lcom/google/android/apps/gmm/base/h/e;->a:Lcom/google/android/libraries/curvular/aw;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/base/h/c;-><init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V

    sput-object v3, Lcom/google/android/apps/gmm/base/h/c;->j:Lcom/google/android/apps/gmm/base/h/c;

    .line 44
    new-instance v3, Lcom/google/android/apps/gmm/base/h/c;

    const-string v4, "FAB_WHITE_BACKGROUND"

    const/16 v5, 0xa

    sget v6, Lcom/google/android/apps/gmm/d;->al:I

    sget v7, Lcom/google/android/apps/gmm/d;->ax:I

    sget-object v8, Lcom/google/android/apps/gmm/base/h/e;->c:Lcom/google/android/libraries/curvular/aw;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/base/h/c;-><init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V

    sput-object v3, Lcom/google/android/apps/gmm/base/h/c;->k:Lcom/google/android/apps/gmm/base/h/c;

    .line 46
    new-instance v3, Lcom/google/android/apps/gmm/base/h/c;

    const-string v4, "FAB_BLUE_BACKGROUND"

    const/16 v5, 0xb

    sget v6, Lcom/google/android/apps/gmm/d;->V:I

    sget v7, Lcom/google/android/apps/gmm/d;->X:I

    sget-object v8, Lcom/google/android/apps/gmm/base/h/e;->c:Lcom/google/android/libraries/curvular/aw;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/base/h/c;-><init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V

    sput-object v3, Lcom/google/android/apps/gmm/base/h/c;->l:Lcom/google/android/apps/gmm/base/h/c;

    .line 48
    new-instance v3, Lcom/google/android/apps/gmm/base/h/c;

    const-string v4, "FAB_NIGHT_BACKGROUND"

    const/16 v5, 0xc

    sget v6, Lcom/google/android/apps/gmm/d;->at:I

    sget v7, Lcom/google/android/apps/gmm/d;->au:I

    sget-object v8, Lcom/google/android/apps/gmm/base/h/e;->c:Lcom/google/android/libraries/curvular/aw;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/base/h/c;-><init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V

    sput-object v3, Lcom/google/android/apps/gmm/base/h/c;->m:Lcom/google/android/apps/gmm/base/h/c;

    .line 50
    new-instance v3, Lcom/google/android/apps/gmm/base/h/c;

    const-string v4, "FAB_RED_BACKGROUND"

    const/16 v5, 0xd

    sget v6, Lcom/google/android/apps/gmm/d;->ai:I

    sget v7, Lcom/google/android/apps/gmm/d;->ah:I

    sget-object v8, Lcom/google/android/apps/gmm/base/h/e;->c:Lcom/google/android/libraries/curvular/aw;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/base/h/c;-><init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V

    sput-object v3, Lcom/google/android/apps/gmm/base/h/c;->n:Lcom/google/android/apps/gmm/base/h/c;

    .line 22
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/google/android/apps/gmm/base/h/c;

    sget-object v1, Lcom/google/android/apps/gmm/base/h/c;->a:Lcom/google/android/apps/gmm/base/h/c;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/base/h/c;->b:Lcom/google/android/apps/gmm/base/h/c;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/apps/gmm/base/h/c;->c:Lcom/google/android/apps/gmm/base/h/c;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/apps/gmm/base/h/c;->d:Lcom/google/android/apps/gmm/base/h/c;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/apps/gmm/base/h/c;->e:Lcom/google/android/apps/gmm/base/h/c;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->f:Lcom/google/android/apps/gmm/base/h/c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->g:Lcom/google/android/apps/gmm/base/h/c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->h:Lcom/google/android/apps/gmm/base/h/c;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->i:Lcom/google/android/apps/gmm/base/h/c;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->j:Lcom/google/android/apps/gmm/base/h/c;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->k:Lcom/google/android/apps/gmm/base/h/c;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->l:Lcom/google/android/apps/gmm/base/h/c;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->m:Lcom/google/android/apps/gmm/base/h/c;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->n:Lcom/google/android/apps/gmm/base/h/c;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/base/h/c;->r:[Lcom/google/android/apps/gmm/base/h/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILcom/google/android/libraries/curvular/aw;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/google/android/libraries/curvular/aw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 75
    iput p3, p0, Lcom/google/android/apps/gmm/base/h/c;->o:I

    .line 76
    iput p4, p0, Lcom/google/android/apps/gmm/base/h/c;->p:I

    .line 77
    iput-object p5, p0, Lcom/google/android/apps/gmm/base/h/c;->q:Lcom/google/android/libraries/curvular/aw;

    .line 78
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/base/h/c;
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/gmm/base/h/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/h/c;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/base/h/c;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/apps/gmm/base/h/c;->r:[Lcom/google/android/apps/gmm/base/h/c;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/base/h/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/base/h/c;

    return-object v0
.end method


# virtual methods
.method public final d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 82
    sget-boolean v2, Lcom/google/android/apps/gmm/map/util/c;->c:Z

    if-eqz v2, :cond_5

    .line 83
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/h/c;->q:Lcom/google/android/libraries/curvular/aw;

    invoke-interface {v2, p1}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget v2, p0, Lcom/google/android/apps/gmm/base/h/c;->p:I

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    new-instance v2, Landroid/graphics/drawable/RippleDrawable;

    iget v4, p0, Lcom/google/android/apps/gmm/base/h/c;->o:I

    if-nez v4, :cond_4

    :goto_1
    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-direct {v2, v1, v0, v3}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object v0, v2

    .line 89
    :goto_2
    return-object v0

    .line 83
    :cond_1
    if-nez v3, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/base/h/c;->p:I

    if-eqz v2, :cond_0

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v0, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget v0, p0, Lcom/google/android/apps/gmm/base/h/c;->p:I

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v0, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    move-object v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_3

    :cond_4
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_1

    .line 84
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/h/c;->q:Lcom/google/android/libraries/curvular/aw;

    sget-object v3, Lcom/google/android/apps/gmm/base/h/e;->c:Lcom/google/android/libraries/curvular/aw;

    if-ne v2, v3, :cond_9

    .line 86
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/h/c;->q:Lcom/google/android/libraries/curvular/aw;

    invoke-interface {v2, p1}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_6

    :goto_4
    iget v2, p0, Lcom/google/android/apps/gmm/base/h/c;->p:I

    if-nez v2, :cond_7

    move v2, v1

    :goto_5
    iget v3, p0, Lcom/google/android/apps/gmm/base/h/c;->o:I

    if-nez v3, :cond_8

    move v3, v1

    :goto_6
    new-instance v4, Lcom/google/android/apps/gmm/base/h/d;

    new-array v5, v5, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v5, v1

    invoke-direct {v4, p0, v5, v2, v3}, Lcom/google/android/apps/gmm/base/h/d;-><init>(Lcom/google/android/apps/gmm/base/h/c;[Landroid/graphics/drawable/Drawable;II)V

    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v4, v2, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    move-object v0, v4

    goto :goto_2

    :cond_6
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_4

    :cond_7
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_5

    :cond_8
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    goto :goto_6

    .line 89
    :cond_9
    iget v3, p0, Lcom/google/android/apps/gmm/base/h/c;->o:I

    if-nez v3, :cond_c

    move-object v2, v0

    :goto_7
    iget v3, p0, Lcom/google/android/apps/gmm/base/h/c;->p:I

    if-nez v3, :cond_d

    :goto_8
    new-instance v3, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v3}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    if-eqz v2, :cond_a

    new-array v4, v5, [I

    const v5, 0x10100a7

    aput v5, v4, v1

    invoke-virtual {v3, v4, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_a
    if-eqz v0, :cond_b

    new-array v1, v1, [I

    invoke-virtual {v3, v1, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_b
    move-object v0, v3

    goto/16 :goto_2

    :cond_c
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_7

    :cond_d
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_8
.end method
