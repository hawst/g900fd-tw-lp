.class public Lcom/google/android/apps/gmm/directions/option/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/views/a/e;


# instance fields
.field final a:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field b:Z

.field private final c:Lcom/google/android/apps/gmm/directions/f/b/l;

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/widget/CompoundButton$OnCheckedChangeListener;Lcom/google/android/apps/gmm/directions/f/b/l;ZLandroid/content/Context;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/option/a;->a:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 28
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/option/a;->c:Lcom/google/android/apps/gmm/directions/f/b/l;

    .line 29
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/directions/option/a;->b:Z

    .line 30
    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/option/a;->d:Landroid/content/Context;

    .line 31
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 35
    sget v0, Lcom/google/android/apps/gmm/h;->k:I

    return v0
.end method

.method public final a(Landroid/view/View;)Lcom/google/android/apps/gmm/base/views/a/f;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/directions/option/c;

    check-cast p1, Landroid/widget/CheckBox;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/directions/option/c;-><init>(Landroid/widget/CheckBox;)V

    .line 57
    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/a/f;)V
    .locals 3

    .prologue
    .line 40
    check-cast p1, Lcom/google/android/apps/gmm/directions/option/c;

    .line 41
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/option/c;->a:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/option/a;->c:Lcom/google/android/apps/gmm/directions/f/b/l;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/f/b/l;->a:Lcom/google/android/apps/gmm/directions/f/b/m;

    iget v1, v1, Lcom/google/android/apps/gmm/directions/f/b/m;->m:I

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setId(I)V

    .line 42
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/option/c;->a:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/option/a;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/option/a;->c:Lcom/google/android/apps/gmm/directions/f/b/l;

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/f/b/l;->a:Lcom/google/android/apps/gmm/directions/f/b/m;

    iget v2, v2, Lcom/google/android/apps/gmm/directions/f/b/m;->j:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 43
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/option/c;->a:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/directions/option/a;->b:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 44
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/option/c;->a:Landroid/widget/CheckBox;

    new-instance v1, Lcom/google/android/apps/gmm/directions/option/b;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/directions/option/b;-><init>(Lcom/google/android/apps/gmm/directions/option/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 52
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/android/apps/gmm/directions/option/m;->c:Lcom/google/android/apps/gmm/directions/option/m;

    iget v0, v0, Lcom/google/android/apps/gmm/directions/option/m;->e:I

    return v0
.end method
