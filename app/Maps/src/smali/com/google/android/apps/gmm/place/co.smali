.class Lcom/google/android/apps/gmm/place/co;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/y;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/activities/c;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/base/activities/c;Z)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/co;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/co;->b:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/co;->c:Ljava/lang/String;

    .line 37
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/place/co;->d:Z

    .line 38
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/co;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 7

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/co;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 64
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/co;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 66
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v2, Lcom/google/b/f/t;->fG:Lcom/google/b/f/t;

    .line 65
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 67
    const-class v0, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    .line 68
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/co;->b:Ljava/lang/String;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/co;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    new-instance v3, Lcom/google/android/apps/gmm/search/aj;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/search/aj;-><init>()V

    const-string v4, "*"

    if-eqz v4, :cond_1

    const-string v5, "\\s+"

    const-string v6, " "

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/apps/gmm/search/aj;->a:Ljava/lang/String;

    :cond_1
    const/16 v4, 0x14

    iput v4, v3, Lcom/google/android/apps/gmm/search/aj;->b:I

    if-eqz v1, :cond_2

    iput-object v1, v3, Lcom/google/android/apps/gmm/search/aj;->h:Ljava/lang/String;

    :cond_2
    sget-object v1, Lcom/google/b/f/t;->fG:Lcom/google/b/f/t;

    new-instance v4, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v1}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/hy;

    iput-object v1, v3, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/a/e;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/placelists/a/e;-><init>()V

    sget-object v4, Lcom/google/android/apps/gmm/base/placelists/a/g;->b:Lcom/google/android/apps/gmm/base/placelists/a/g;

    iput-object v4, v1, Lcom/google/android/apps/gmm/base/placelists/a/e;->e:Lcom/google/android/apps/gmm/base/placelists/a/g;

    sget-object v4, Lcom/google/android/apps/gmm/base/placelists/a/h;->a:Lcom/google/android/apps/gmm/base/placelists/a/h;

    iput-object v4, v1, Lcom/google/android/apps/gmm/base/placelists/a/e;->d:Lcom/google/android/apps/gmm/base/placelists/a/h;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/placelists/a/e;->i:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/search/aj;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    .line 70
    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/co;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/co;->d:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/gmm/f;->dw:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
