.class public Lcom/google/android/apps/gmm/car/j/q;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/j/m;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/car/j/y;

.field private final c:I

.field private final d:Lcom/google/android/apps/gmm/car/j/s;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/gmm/car/j/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/j/q;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V
    .locals 1
    .param p3    # Lcom/google/android/apps/gmm/car/j/s;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/car/j/y;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/j/q;->b:Lcom/google/android/apps/gmm/car/j/y;

    .line 61
    iput p2, p0, Lcom/google/android/apps/gmm/car/j/q;->c:I

    .line 62
    iput-object p3, p0, Lcom/google/android/apps/gmm/car/j/q;->d:Lcom/google/android/apps/gmm/car/j/s;

    .line 63
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/car/j/y;)Lcom/google/android/apps/gmm/car/j/s;
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/car/j/r;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/j/r;-><init>(Lcom/google/android/apps/gmm/car/j/y;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/ad;Landroid/view/View;)Lcom/google/android/apps/gmm/car/j/n;
    .locals 2

    .prologue
    .line 67
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/j/q;->b:Lcom/google/android/apps/gmm/car/j/y;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/car/j/y;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_1

    .line 71
    sget-object v1, Lcom/google/android/apps/gmm/car/j/q;->a:Ljava/lang/String;

    .line 72
    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 73
    sget-object v0, Lcom/google/android/apps/gmm/car/j/n;->b:Lcom/google/android/apps/gmm/car/j/n;

    .line 84
    :goto_0
    return-object v0

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/j/q;->d:Lcom/google/android/apps/gmm/car/j/s;

    if-eqz v0, :cond_2

    .line 78
    iget v0, p0, Lcom/google/android/apps/gmm/car/j/q;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/car/j/q;->e:I

    .line 79
    iget v0, p0, Lcom/google/android/apps/gmm/car/j/q;->e:I

    iget v1, p0, Lcom/google/android/apps/gmm/car/j/q;->c:I

    if-ne v0, v1, :cond_2

    .line 80
    sget-object v0, Lcom/google/android/apps/gmm/car/j/q;->a:Ljava/lang/String;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/j/q;->d:Lcom/google/android/apps/gmm/car/j/s;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/car/j/s;->a(Lcom/google/android/apps/gmm/car/ad;Landroid/view/View;)Lcom/google/android/apps/gmm/car/j/n;

    move-result-object v0

    goto :goto_0

    .line 84
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/car/j/n;->a:Lcom/google/android/apps/gmm/car/j/n;

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/car/j/q;->e:I

    .line 90
    return-void
.end method
