.class final enum Lcom/google/android/apps/gmm/mylocation/d;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/mylocation/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/mylocation/d;

.field public static final enum b:Lcom/google/android/apps/gmm/mylocation/d;

.field public static final enum c:Lcom/google/android/apps/gmm/mylocation/d;

.field public static final enum d:Lcom/google/android/apps/gmm/mylocation/d;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/mylocation/d;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 104
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/d;

    const-string v1, "WAITING_FOR_PREFETCH"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/mylocation/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/d;->a:Lcom/google/android/apps/gmm/mylocation/d;

    .line 108
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/d;

    const-string v1, "PREFETCH_DONE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/mylocation/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/d;->b:Lcom/google/android/apps/gmm/mylocation/d;

    .line 113
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/d;

    const-string v1, "PREFETCH_NOT_AVAILABLE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/mylocation/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/d;->c:Lcom/google/android/apps/gmm/mylocation/d;

    .line 117
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/d;

    const-string v1, "PREFETCH_SUPPRESSED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/mylocation/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/d;->d:Lcom/google/android/apps/gmm/mylocation/d;

    .line 101
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/mylocation/d;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/d;->a:Lcom/google/android/apps/gmm/mylocation/d;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/d;->b:Lcom/google/android/apps/gmm/mylocation/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/d;->c:Lcom/google/android/apps/gmm/mylocation/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/d;->d:Lcom/google/android/apps/gmm/mylocation/d;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/d;->e:[Lcom/google/android/apps/gmm/mylocation/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/mylocation/d;
    .locals 1

    .prologue
    .line 101
    const-class v0, Lcom/google/android/apps/gmm/mylocation/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/d;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/mylocation/d;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/d;->e:[Lcom/google/android/apps/gmm/mylocation/d;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/mylocation/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/mylocation/d;

    return-object v0
.end method
