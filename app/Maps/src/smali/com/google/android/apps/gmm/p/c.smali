.class public Lcom/google/android/apps/gmm/p/c;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# static fields
.field private static a:Lcom/google/android/apps/gmm/p/c;


# instance fields
.field private b:I

.field private c:Lcom/google/android/apps/gmm/map/b/a/u;

.field private d:Lcom/google/android/apps/gmm/map/b/a/u;

.field private e:I

.field private f:I

.field private h:[J

.field private final i:Lcom/google/android/apps/gmm/map/c/a;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 2

    .prologue
    .line 99
    sget-object v0, Lcom/google/r/b/a/el;->aa:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/u;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 80
    const/4 v0, 0x6

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    .line 82
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/c;->i:Lcom/google/android/apps/gmm/map/c/a;

    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/c;->g()V

    .line 102
    return-void
.end method

.method public static a(Landroid/location/Location;Lcom/google/android/apps/gmm/map/c/a;)Landroid/location/Location;
    .locals 8

    .prologue
    const-wide v6, 0x412e848000000000L    # 1000000.0

    .line 320
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    mul-double/2addr v2, v6

    double-to-int v1, v2

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v6

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    .line 321
    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/p/c;->a(Lcom/google/android/apps/gmm/map/b/a/u;Lcom/google/android/apps/gmm/map/c/a;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 322
    sget-object v1, Lcom/google/android/apps/gmm/p/c;->a:Lcom/google/android/apps/gmm/p/c;

    if-nez v1, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/gmm/p/c;->b(Lcom/google/android/apps/gmm/map/c/a;)V

    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/p/c;->a:Lcom/google/android/apps/gmm/p/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/p/c;->a(Lcom/google/android/apps/gmm/map/b/a/u;)Lcom/google/android/apps/gmm/map/b/a/u;

    move-result-object v1

    .line 324
    new-instance v0, Landroid/location/Location;

    new-instance v2, Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/r/b/c;-><init>()V

    .line 325
    invoke-virtual {v2, p0}, Lcom/google/android/apps/gmm/map/r/b/c;->a(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v2

    iput-object v1, v2, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    int-to-double v4, v3

    div-double/2addr v4, v6

    iput-wide v4, v2, Lcom/google/android/apps/gmm/map/r/b/c;->e:D

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    int-to-double v4, v1

    div-double/2addr v4, v6

    iput-wide v4, v2, Lcom/google/android/apps/gmm/map/r/b/c;->f:D

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    move-object p0, v0

    .line 327
    :cond_2
    return-object p0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/c/a;)Lcom/google/android/apps/gmm/p/c;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/google/android/apps/gmm/p/c;->a:Lcom/google/android/apps/gmm/p/c;

    if-nez v0, :cond_0

    .line 111
    invoke-static {p0}, Lcom/google/android/apps/gmm/p/c;->b(Lcom/google/android/apps/gmm/map/c/a;)V

    .line 113
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/p/c;->a:Lcom/google/android/apps/gmm/p/c;

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/u;Lcom/google/android/apps/gmm/map/c/a;)Z
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 351
    if-eqz p0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    const v3, 0x2dc6c0

    if-lt v0, v3, :cond_0

    .line 356
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    const v3, 0x337f980

    if-gt v0, v3, :cond_0

    .line 357
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    const v3, 0x44aa200

    if-lt v0, v3, :cond_0

    .line 358
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    const v3, 0x81b3200

    if-le v0, v3, :cond_1

    :cond_0
    move v0, v2

    .line 379
    :goto_0
    return v0

    .line 366
    :cond_1
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->a()Landroid/content/Context;

    move-result-object v0

    const-string v3, "phone"

    .line 367
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 369
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    .line 370
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v3, v4, :cond_2

    move v0, v1

    .line 371
    goto :goto_0

    .line 374
    :cond_2
    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 376
    const/16 v3, 0x1cc

    if-ne v0, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    .line 379
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method private static declared-synchronized b(Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 2

    .prologue
    .line 105
    const-class v1, Lcom/google/android/apps/gmm/p/c;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/android/apps/gmm/p/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/p/c;-><init>(Lcom/google/android/apps/gmm/map/c/a;)V

    sput-object v0, Lcom/google/android/apps/gmm/p/c;->a:Lcom/google/android/apps/gmm/p/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    monitor-exit v1

    return-void

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized g()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 153
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/c;->i:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v3

    .line 154
    instance-of v2, v3, Lcom/google/android/apps/gmm/m/i;

    if-eqz v2, :cond_3

    move-object v0, v3

    check-cast v0, Lcom/google/android/apps/gmm/m/i;

    move-object v2, v0

    const-string v7, "savedLocationShiftCoefficients_lock"

    invoke-interface {v2, v7}, Lcom/google/android/apps/gmm/m/i;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/m/h;

    move-result-object v2

    move-object v7, v2

    .line 155
    :goto_0
    const-string v2, "savedLocationShiftCoefficients"

    invoke-interface {v3, v2}, Lcom/google/android/apps/gmm/m/d;->b(Ljava/lang/String;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    if-nez v8, :cond_4

    move-object v2, v4

    .line 158
    :goto_1
    if-eqz v2, :cond_5

    :try_start_1
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/p/c;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/shared/net/k;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    if-nez v2, :cond_5

    move v2, v5

    .line 164
    :goto_2
    if-eqz v7, :cond_0

    :try_start_2
    invoke-interface {v7}, Lcom/google/android/apps/gmm/m/h;->a()Z

    :cond_0
    move v6, v2

    .line 166
    :cond_1
    :goto_3
    if-nez v6, :cond_2

    .line 167
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    aput-wide v4, v2, v3

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    const/4 v3, 0x1

    const-wide/32 v4, 0xf4240

    aput-wide v4, v2, v3

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    const/4 v3, 0x2

    const-wide/16 v4, 0x0

    aput-wide v4, v2, v3

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    const/4 v3, 0x3

    const-wide/16 v4, 0x0

    aput-wide v4, v2, v3

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    const/4 v3, 0x4

    const-wide/16 v4, 0x0

    aput-wide v4, v2, v3

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    const/4 v3, 0x5

    const-wide/32 v4, 0xf4240

    aput-wide v4, v2, v3

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/gmm/p/c;->e:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/gmm/p/c;->f:I

    const/4 v2, 0x1

    iput v2, p0, Lcom/google/android/apps/gmm/p/c;->b:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 169
    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    move-object v7, v4

    .line 154
    goto :goto_0

    .line 155
    :cond_4
    :try_start_3
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 153
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_5
    move v2, v6

    .line 158
    goto :goto_2

    .line 160
    :catch_0
    move-exception v2

    .line 161
    :try_start_4
    const-string v2, "savedLocationShiftCoefficients"

    invoke-interface {v3, v2}, Lcom/google/android/apps/gmm/m/d;->a(Ljava/lang/String;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 164
    if-eqz v7, :cond_1

    :try_start_5
    invoke-interface {v7}, Lcom/google/android/apps/gmm/m/h;->a()Z

    goto :goto_3

    :catchall_1
    move-exception v2

    if-eqz v7, :cond_6

    invoke-interface {v7}, Lcom/google/android/apps/gmm/m/h;->a()Z

    :cond_6
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private h()Lcom/google/e/a/a/a/b;
    .locals 6

    .prologue
    .line 175
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/u;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 176
    const/4 v0, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/p/c;->b:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 177
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x6

    if-ge v0, v2, :cond_0

    .line 178
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    aget-wide v4, v3, v0

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    :cond_0
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/c;->c:Lcom/google/android/apps/gmm/map/b/a/u;

    .line 181
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/u;->a()Lcom/google/e/a/a/a/b;

    move-result-object v2

    .line 180
    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_1
    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 182
    const/4 v0, 0x4

    iget v2, p0, Lcom/google/android/apps/gmm/p/c;->f:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 183
    const/4 v0, 0x5

    iget v2, p0, Lcom/google/android/apps/gmm/p/c;->e:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 184
    return-object v1
.end method

.method private declared-synchronized o()V
    .locals 6

    .prologue
    .line 189
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/c;->i:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v2

    .line 190
    instance-of v1, v2, Lcom/google/android/apps/gmm/m/i;

    if-eqz v1, :cond_1

    move-object v0, v2

    check-cast v0, Lcom/google/android/apps/gmm/m/i;

    move-object v1, v0

    const-string v3, "savedLocationShiftCoefficients_lock"

    invoke-interface {v1, v3}, Lcom/google/android/apps/gmm/m/i;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/m/h;

    move-result-object v1

    move-object v3, v1

    .line 191
    :goto_0
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 192
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/c;->h()Lcom/google/e/a/a/a/b;

    move-result-object v5

    .line 195
    check-cast v1, Ljava/io/DataOutputStream;

    invoke-virtual {v5, v1}, Lcom/google/e/a/a/a/b;->a(Ljava/io/OutputStream;)V

    .line 196
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    const-string v4, "savedLocationShiftCoefficients"

    invoke-interface {v2, v1, v4}, Lcom/google/android/apps/gmm/m/d;->a([BLjava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 201
    if-eqz v3, :cond_0

    :try_start_2
    invoke-interface {v3}, Lcom/google/android/apps/gmm/m/h;->a()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 202
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 190
    :cond_1
    const/4 v1, 0x0

    move-object v3, v1

    goto :goto_0

    .line 198
    :catch_0
    move-exception v1

    :try_start_3
    const-string v1, "savedLocationShiftCoefficients"

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/m/d;->a(Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 201
    if-eqz v3, :cond_0

    :try_start_4
    invoke-interface {v3}, Lcom/google/android/apps/gmm/m/h;->a()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 189
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 201
    :catchall_1
    move-exception v1

    if-eqz v3, :cond_2

    :try_start_5
    invoke-interface {v3}, Lcom/google/android/apps/gmm/m/h;->a()Z

    :cond_2
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b/a/u;)Lcom/google/android/apps/gmm/map/b/a/u;
    .locals 14

    .prologue
    const-wide/32 v12, 0x15752a00

    const-wide/32 v10, 0xf4240

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 303
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    aget-wide v2, v2, v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    aget-wide v4, v4, v0

    iget v6, p1, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    int-to-long v6, v6

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    iget-object v4, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    const/4 v5, 0x2

    aget-wide v4, v4, v5

    .line 304
    iget v6, p1, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    int-to-long v6, v6

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    div-long v4, v2, v10

    .line 305
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    const/4 v3, 0x3

    aget-wide v2, v2, v3

    iget-object v6, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    const/4 v7, 0x4

    aget-wide v6, v6, v7

    iget v8, p1, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    int-to-long v8, v8

    mul-long/2addr v6, v8

    add-long/2addr v2, v6

    iget-object v6, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    const/4 v7, 0x5

    aget-wide v6, v6, v7

    .line 306
    iget v8, p1, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    int-to-long v8, v8

    mul-long/2addr v6, v8

    add-long/2addr v2, v6

    div-long v6, v2, v10

    .line 307
    iget v2, p0, Lcom/google/android/apps/gmm/p/c;->e:I

    div-int/lit8 v3, v2, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/c;->c:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v2, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_5

    :goto_1
    if-eqz v0, :cond_0

    .line 308
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/c;->c:Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/u;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/c;->d:Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/u;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/gmm/p/c;->d:Lcom/google/android/apps/gmm/map/b/a/u;

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/c;->i:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 310
    :cond_0
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    .line 311
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    .line 312
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/u;

    long-to-int v1, v4

    long-to-int v2, v6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    return-object v0

    .line 307
    :cond_1
    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    iget-object v8, p0, Lcom/google/android/apps/gmm/p/c;->c:Lcom/google/android/apps/gmm/map/b/a/u;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    sub-int/2addr v2, v8

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-lt v2, v3, :cond_2

    move v2, v1

    goto :goto_0

    :cond_2
    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    iget-object v8, p0, Lcom/google/android/apps/gmm/p/c;->c:Lcom/google/android/apps/gmm/map/b/a/u;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    sub-int/2addr v2, v8

    :goto_2
    if-gez v2, :cond_3

    int-to-long v8, v2

    add-long/2addr v8, v12

    long-to-int v2, v8

    goto :goto_2

    :cond_3
    int-to-long v8, v2

    sub-long v8, v12, v8

    long-to-int v8, v8

    invoke-static {v2, v8}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-ge v2, v3, :cond_4

    move v2, v0

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 7

    .prologue
    const/16 v6, 0x15

    .line 215
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/p/c;->b:I

    .line 216
    iget v0, p0, Lcom/google/android/apps/gmm/p/c;->b:I

    if-eqz v0, :cond_0

    .line 217
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->k:Lcom/google/android/apps/gmm/shared/net/k;

    .line 228
    :goto_0
    return-object v0

    .line 219
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    const/4 v0, 0x6

    if-ge v1, v0, :cond_1

    .line 220
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/c;->h:[J

    const/4 v0, 0x2

    const/16 v3, 0x13

    invoke-virtual {p1, v0, v1, v3}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 219
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 222
    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/p/c;->f:I

    .line 223
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/p/c;->e:I

    .line 224
    const/4 v0, 0x3

    .line 225
    const/16 v1, 0x1a

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/u;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/c;->c:Lcom/google/android/apps/gmm/map/b/a/u;

    .line 226
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/c;->o()V

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/c;->c:Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Location Shift Response for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 4

    .prologue
    .line 207
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/u;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 208
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/c;->d:Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/u;->a()Lcom/google/e/a/a/a/b;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 209
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/c;->c:Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Location Shift Request for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    return-object v0
.end method
