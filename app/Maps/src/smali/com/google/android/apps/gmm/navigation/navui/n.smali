.class Lcom/google/android/apps/gmm/navigation/navui/n;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/navui/q;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;)V
    .locals 0

    .prologue
    .line 535
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/n;->a:Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 555
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/n;->a:Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->k:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->k:Lcom/google/android/apps/gmm/base/g/c;

    sget-object v5, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v4

    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/j;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/navui/j;-><init>(Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b(Ljava/lang/Runnable;)V

    .line 556
    :goto_0
    return-void

    .line 555
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/k;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/k;-><init>(Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/navui/b/a;)V
    .locals 1

    .prologue
    .line 539
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 540
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/n;->a:Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c()V

    .line 542
    :cond_0
    return-void

    .line 539
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V
    .locals 2

    .prologue
    .line 547
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 548
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/n;->a:Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/y;

    .line 549
    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/navigation/navui/y;->a(Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V

    goto :goto_0

    .line 551
    :cond_0
    return-void
.end method
