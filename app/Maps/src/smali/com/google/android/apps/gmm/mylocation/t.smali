.class public Lcom/google/android/apps/gmm/mylocation/t;
.super Lcom/google/android/apps/gmm/map/f/d;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/f;


# instance fields
.field b:Lcom/google/android/apps/gmm/v/g;

.field m:Lcom/google/android/apps/gmm/mylocation/d/c;

.field n:Lcom/google/android/apps/gmm/mylocation/a/i;

.field o:Lcom/google/android/apps/gmm/map/f/o;

.field private final p:Lcom/google/android/apps/gmm/mylocation/g/a;

.field private final q:Lcom/google/android/apps/gmm/mylocation/b/f;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/mylocation/d/c;Lcom/google/android/apps/gmm/mylocation/a/i;Lcom/google/android/apps/gmm/mylocation/b/f;)V
    .locals 6

    .prologue
    .line 60
    new-instance v5, Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/mylocation/g/a;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/mylocation/t;-><init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/mylocation/d/c;Lcom/google/android/apps/gmm/mylocation/b/f;Lcom/google/android/apps/gmm/mylocation/a/i;Lcom/google/android/apps/gmm/mylocation/g/a;)V

    .line 61
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/mylocation/d/c;Lcom/google/android/apps/gmm/mylocation/b/f;Lcom/google/android/apps/gmm/mylocation/a/i;Lcom/google/android/apps/gmm/mylocation/g/a;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/f/d;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 72
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p2, Lcom/google/android/apps/gmm/mylocation/d/c;

    iput-object p2, p0, Lcom/google/android/apps/gmm/mylocation/t;->m:Lcom/google/android/apps/gmm/mylocation/d/c;

    .line 73
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p3, Lcom/google/android/apps/gmm/mylocation/b/f;

    iput-object p3, p0, Lcom/google/android/apps/gmm/mylocation/t;->q:Lcom/google/android/apps/gmm/mylocation/b/f;

    .line 74
    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p4, Lcom/google/android/apps/gmm/mylocation/a/i;

    iput-object p4, p0, Lcom/google/android/apps/gmm/mylocation/t;->n:Lcom/google/android/apps/gmm/mylocation/a/i;

    .line 75
    if-nez p5, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p5, Lcom/google/android/apps/gmm/mylocation/g/a;

    iput-object p5, p0, Lcom/google/android/apps/gmm/mylocation/t;->p:Lcom/google/android/apps/gmm/mylocation/g/a;

    .line 76
    return-void
.end method


# virtual methods
.method public final W_()I
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->q:Lcom/google/android/apps/gmm/mylocation/b/f;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->q:Lcom/google/android/apps/gmm/mylocation/b/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/f;->e()I

    move-result v0

    .line 229
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(J)I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->q:Lcom/google/android/apps/gmm/mylocation/b/f;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(J)I

    move-result v0

    return v0
.end method

.method public final a()Lcom/google/android/apps/gmm/v/h;
    .locals 1

    .prologue
    .line 127
    sget-object v0, Lcom/google/android/apps/gmm/v/h;->b:Lcom/google/android/apps/gmm/v/h;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/d;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->q:Lcom/google/android/apps/gmm/mylocation/b/f;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Lcom/google/android/apps/gmm/map/f/a/d;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/c;)V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->q:Lcom/google/android/apps/gmm/mylocation/b/f;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Lcom/google/android/apps/gmm/map/f/a/c;)V

    .line 157
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/t;->b:Lcom/google/android/apps/gmm/v/g;

    .line 81
    sget-object v0, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 82
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->q:Lcom/google/android/apps/gmm/mylocation/b/f;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 133
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/b;Lcom/google/android/apps/gmm/map/f/a/d;)Z
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/map/f/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->q:Lcom/google/android/apps/gmm/mylocation/b/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/f;->b()Lcom/google/android/apps/gmm/map/s/a;

    move-result-object v0

    .line 170
    if-nez p1, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->d:Lcom/google/android/apps/gmm/map/s/a;

    if-ne v0, v1, :cond_2

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->a:Lcom/google/android/apps/gmm/map/f/a/d;

    if-eq p2, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->d:Lcom/google/android/apps/gmm/map/f/a/d;

    if-ne p2, v0, :cond_2

    .line 174
    :cond_1
    const/4 v0, 0x0

    .line 176
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/f/b;Lcom/google/android/apps/gmm/map/f/a/d;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/map/f/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->q:Lcom/google/android/apps/gmm/mylocation/b/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/f;->b()Lcom/google/android/apps/gmm/map/s/a;

    move-result-object v0

    .line 184
    if-eqz p1, :cond_0

    if-eq p1, p0, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->b:Lcom/google/android/apps/gmm/map/s/a;

    if-ne v0, v1, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->a:Lcom/google/android/apps/gmm/map/f/a/d;

    if-ne p2, v1, :cond_2

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->q:Lcom/google/android/apps/gmm/mylocation/b/f;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/f/b;->e()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Z)V

    goto :goto_0

    .line 192
    :cond_2
    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->d:Lcom/google/android/apps/gmm/map/f/a/d;

    if-ne p2, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    if-eq v0, v1, :cond_0

    .line 194
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/f/b;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->q:Lcom/google/android/apps/gmm/mylocation/b/f;

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Lcom/google/android/apps/gmm/map/s/a;)V

    goto :goto_0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 110
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/t;->p:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v2, :cond_1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->p:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/t;->o:Lcom/google/android/apps/gmm/map/f/o;

    .line 111
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/f/o;->b()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/mylocation/g/a;->a(Lcom/google/android/apps/gmm/map/b/a/bd;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->n:Lcom/google/android/apps/gmm/mylocation/a/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/t;->p:Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/mylocation/a/i;->a(Lcom/google/android/apps/gmm/mylocation/g/a;)Z

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->p:Lcom/google/android/apps/gmm/mylocation/g/a;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->l:Z

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->m:Lcom/google/android/apps/gmm/mylocation/d/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/t;->p:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/t;->o:Lcom/google/android/apps/gmm/map/f/o;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/mylocation/d/c;->a(Lcom/google/android/apps/gmm/mylocation/g/a;Lcom/google/android/apps/gmm/map/f/o;)V

    .line 118
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->q:Lcom/google/android/apps/gmm/mylocation/b/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/f;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    sget-object v0, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/d;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    monitor-exit p0

    return-void

    :cond_1
    move v0, v1

    .line 110
    goto :goto_0

    .line 116
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->p:Lcom/google/android/apps/gmm/mylocation/g/a;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->l:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/t;->q:Lcom/google/android/apps/gmm/mylocation/b/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/t;->p:Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Lcom/google/android/apps/gmm/mylocation/g/a;)V

    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 138
    return-void
.end method
