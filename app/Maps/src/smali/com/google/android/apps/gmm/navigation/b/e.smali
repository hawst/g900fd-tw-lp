.class public final enum Lcom/google/android/apps/gmm/navigation/b/e;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/navigation/b/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/navigation/b/e;

.field public static final enum b:Lcom/google/android/apps/gmm/navigation/b/e;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/navigation/b/e;


# instance fields
.field public final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/navigation/b/e;

    const-string v1, "GUIDED"

    const-string v2, "guided"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/navigation/b/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/b/e;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/navigation/b/e;

    const-string v1, "FREE"

    const-string v2, "free"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/navigation/b/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/b/e;->b:Lcom/google/android/apps/gmm/navigation/b/e;

    .line 30
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/gmm/navigation/b/e;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/b/e;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/navigation/b/e;->b:Lcom/google/android/apps/gmm/navigation/b/e;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/navigation/b/e;->d:[Lcom/google/android/apps/gmm/navigation/b/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    const-string v0, "id"

    if-nez p3, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p3, Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/b/e;->c:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/apps/gmm/navigation/b/e;
    .locals 5

    .prologue
    .line 55
    invoke-static {}, Lcom/google/android/apps/gmm/navigation/b/e;->values()[Lcom/google/android/apps/gmm/navigation/b/e;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 56
    iget-object v4, v3, Lcom/google/android/apps/gmm/navigation/b/e;->c:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 57
    return-object v3

    .line 55
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unknown mode key="

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/navigation/b/e;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/apps/gmm/navigation/b/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/b/e;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/navigation/b/e;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/apps/gmm/navigation/b/e;->d:[Lcom/google/android/apps/gmm/navigation/b/e;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/navigation/b/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/navigation/b/e;

    return-object v0
.end method
