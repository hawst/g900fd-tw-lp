.class public Lcom/google/android/apps/gmm/map/f/x;
.super Lcom/google/android/apps/gmm/map/f/t;
.source "PG"


# static fields
.field static final a:Landroid/view/animation/Interpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/x;->a:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/f/t;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 24
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z
    .locals 4
    .param p1    # Lcom/google/android/apps/gmm/map/f/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/apps/gmm/map/f/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 34
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/x;->l:[Lcom/google/android/apps/gmm/map/f/w;

    monitor-enter v1

    .line 35
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    move-result v0

    .line 37
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/x;->k:Lcom/google/android/apps/gmm/map/f/w;

    sget-object v3, Lcom/google/android/apps/gmm/map/f/x;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/f/w;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 39
    sget-object v2, Lcom/google/android/apps/gmm/map/f/a/d;->a:Lcom/google/android/apps/gmm/map/f/a/d;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/gmm/map/f/x;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 40
    sget-object v2, Lcom/google/android/apps/gmm/map/f/a/d;->b:Lcom/google/android/apps/gmm/map/f/a/d;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/gmm/map/f/x;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 41
    sget-object v2, Lcom/google/android/apps/gmm/map/f/a/d;->c:Lcom/google/android/apps/gmm/map/f/a/d;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/gmm/map/f/x;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 42
    sget-object v2, Lcom/google/android/apps/gmm/map/f/a/d;->d:Lcom/google/android/apps/gmm/map/f/a/d;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/gmm/map/f/x;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 44
    monitor-exit v1

    return v0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
