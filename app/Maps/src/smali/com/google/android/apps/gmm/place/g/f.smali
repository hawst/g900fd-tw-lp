.class public Lcom/google/android/apps/gmm/place/g/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/u;
.implements Lcom/google/android/apps/gmm/place/review/b;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/activities/c;

.field private final b:Lcom/google/android/apps/gmm/place/g/a/f;

.field private final c:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/place/g/a/f;Lcom/google/android/apps/gmm/x/o;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/place/g/a/f;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/g/f;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 32
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/g/f;->b:Lcom/google/android/apps/gmm/place/g/a/f;

    .line 33
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/g/f;->c:Lcom/google/android/apps/gmm/x/o;

    .line 34
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    .line 49
    sget v0, Lcom/google/android/apps/gmm/l;->gb:I

    if-eq p1, v0, :cond_0

    sget v0, Lcom/google/android/apps/gmm/l;->gc:I

    if-ne p1, v0, :cond_2

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/f;->b:Lcom/google/android/apps/gmm/place/g/a/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/f;->e()Lcom/google/android/libraries/curvular/cf;

    .line 54
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 51
    :cond_2
    sget v0, Lcom/google/android/apps/gmm/l;->eS:I

    if-eq p1, v0, :cond_3

    sget v0, Lcom/google/android/apps/gmm/l;->eT:I

    if-ne p1, v0, :cond_1

    .line 52
    :cond_3
    new-instance v2, Lcom/google/android/apps/gmm/place/review/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/f;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-static {v0, v1}, Lcom/google/b/h/c;->a(J)Lcom/google/b/h/c;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/f;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->O()Lcom/google/maps/g/pc;

    move-result-object v4

    iget-object v0, v4, Lcom/google/maps/g/pc;->j:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    :goto_1
    invoke-direct {v2, v3, v0, p0}, Lcom/google/android/apps/gmm/place/review/a;-><init>(Lcom/google/b/h/c;Ljava/lang/String;Lcom/google/android/apps/gmm/place/review/b;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/f;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    goto :goto_0

    :cond_4
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    iput-object v1, v4, Lcom/google/maps/g/pc;->j:Ljava/lang/Object;

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method public final a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/f;->b:Lcom/google/android/apps/gmm/place/g/a/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/f;->d()Lcom/google/android/apps/gmm/place/g/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/d;->h()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/f;->b:Lcom/google/android/apps/gmm/place/g/a/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/f;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    if-eqz v2, :cond_1

    sget v0, Lcom/google/android/apps/gmm/l;->gc:I

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    :cond_0
    if-eqz v2, :cond_2

    sget v0, Lcom/google/android/apps/gmm/l;->eT:I

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    return-object v1

    .line 41
    :cond_1
    sget v0, Lcom/google/android/apps/gmm/l;->gb:I

    goto :goto_0

    .line 43
    :cond_2
    sget v0, Lcom/google/android/apps/gmm/l;->eS:I

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/f;->a:Lcom/google/android/apps/gmm/base/activities/c;

    if-nez v0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 71
    :cond_0
    if-eqz p1, :cond_1

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/f;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 73
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/g/c;->a(Lcom/google/maps/g/pc;)V

    .line 74
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/g/f;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/x/o;->b(Ljava/io/Serializable;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/f;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/place/review/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/g/f;->c:Lcom/google/android/apps/gmm/x/o;

    sget-object v3, Lcom/google/android/apps/gmm/place/review/a/b;->b:Lcom/google/android/apps/gmm/place/review/a/b;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/place/review/a/a;-><init>(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/review/a/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 78
    :cond_1
    if-eqz p1, :cond_2

    sget v0, Lcom/google/android/apps/gmm/l;->eV:I

    .line 80
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/g/f;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/g/f;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 81
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 78
    :cond_2
    sget v0, Lcom/google/android/apps/gmm/l;->eU:I

    goto :goto_1
.end method
