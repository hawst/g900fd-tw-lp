.class public Lcom/google/android/apps/gmm/shared/c/c/i;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public b:Ljava/lang/Object;

.field public c:Lcom/google/android/apps/gmm/shared/c/c/j;

.field public d:Landroid/text/style/ClickableSpan;

.field e:I

.field final synthetic f:Lcom/google/android/apps/gmm/shared/c/c/e;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 218
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->f:Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216
    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->e:I

    .line 219
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    .line 220
    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->e:I

    .line 221
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/c/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 222
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    instance-of v0, v0, Landroid/text/SpannableStringBuilder;

    if-eqz v0, :cond_1

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    check-cast v0, Landroid/text/SpannableStringBuilder;

    .line 352
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget v2, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->e:I

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-static {v1, v0, v2, v4, v3}, Lcom/google/android/apps/gmm/shared/c/c/j;->a(Lcom/google/android/apps/gmm/shared/c/c/j;Landroid/text/SpannableStringBuilder;III)V

    .line 353
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 356
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->d:Landroid/text/style/ClickableSpan;

    if-eqz v1, :cond_0

    .line 357
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->d:Landroid/text/style/ClickableSpan;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 360
    :cond_0
    return-object v0

    .line 342
    :cond_1
    const-string v0, "%s"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    .line 344
    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    goto :goto_0

    .line 346
    :cond_2
    new-instance v0, Landroid/text/SpannableStringBuilder;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    aput-object v2, v1, v4

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/shared/c/c/i;
    .locals 4

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->f:Lcom/google/android/apps/gmm/shared/c/c/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v3, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 249
    return-object p0
.end method
