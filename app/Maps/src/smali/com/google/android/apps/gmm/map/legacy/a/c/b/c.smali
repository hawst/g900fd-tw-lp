.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;


# static fields
.field private static final c:[I

.field private static d:I

.field private static final i:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:I

.field b:Lcom/google/android/apps/gmm/map/b/a/e;

.field private e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

.field private f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

.field private g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->c:[I

    .line 84
    const/16 v0, 0x4000

    sput v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->d:I

    .line 127
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/d;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->i:Ljava/lang/ThreadLocal;

    return-void

    .line 78
    nop

    :array_0
    .array-data 4
        0x0
        0x2
        0x2
        0x4
        0x2
        0x4
        0x4
        0x6
    .end array-data
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;III)V
    .locals 3

    .prologue
    const/16 v2, 0xa

    const/4 v1, 0x0

    .line 316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 319
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 318
    invoke-static {p2, v2, v1, v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 321
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 320
    invoke-static {p3, v2, v1, v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 324
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 323
    invoke-static {p4, v2, v1, v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 325
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->b:Lcom/google/android/apps/gmm/map/b/a/e;

    .line 326
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->h:Ljava/util/List;

    .line 327
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;Lcom/google/android/apps/gmm/map/indoor/c/r;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/util/b/g;Ljava/util/List;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "[",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/internal/c/cp;",
            "Lcom/google/android/apps/gmm/v/ad;",
            "Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;",
            "Lcom/google/android/apps/gmm/map/indoor/c/r;",
            "Lcom/google/android/apps/gmm/map/t/k;",
            "Lcom/google/android/apps/gmm/v/bp;",
            "Lcom/google/android/apps/gmm/map/util/b/g;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;"
        }
    .end annotation

    .prologue
    .line 167
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v3

    .line 168
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 171
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v14

    .line 175
    const/4 v5, 0x0

    .line 176
    const/4 v4, 0x0

    .line 177
    const/4 v2, 0x0

    .line 178
    const/4 v6, 0x0

    move v8, v2

    move v9, v4

    move v10, v5

    .line 179
    :goto_0
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_30

    .line 183
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v4

    .line 184
    instance-of v2, v4, Lcom/google/android/apps/gmm/map/internal/c/h;

    if-eqz v2, :cond_30

    move-object v2, v4

    .line 185
    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/h;

    .line 187
    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v7, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    if-nez v7, :cond_0

    const/4 v5, 0x0

    :goto_1
    if-lez v5, :cond_1

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v7, 0x0

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    aget v5, v5, v7

    invoke-static {v5}, Landroid/graphics/Color;->alpha(I)I

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    move v11, v5

    .line 188
    :goto_2
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a(Lcom/google/android/apps/gmm/map/internal/c/h;)Z

    move-result v15

    .line 189
    iget v5, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->l:I

    const/4 v7, 0x4

    and-int/2addr v5, v7

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    move v12, v5

    .line 190
    :goto_3
    if-nez v6, :cond_3

    move-object v7, v4

    .line 205
    :goto_4
    if-eqz v11, :cond_9

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    if-nez v6, :cond_6

    const/4 v5, 0x0

    :goto_5
    if-nez v5, :cond_7

    const/4 v5, 0x0

    move v11, v5

    .line 209
    :goto_6
    if-eqz v15, :cond_f

    const/4 v6, 0x0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->e:[B

    move-object/from16 v16, v0

    if-eqz v16, :cond_a

    const/4 v5, 0x0

    :goto_7
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v5, v0, :cond_b

    aget-byte v17, v16, v5

    and-int/lit8 v17, v17, 0x7

    sget-object v18, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->c:[I

    aget v17, v18, v17

    add-int v6, v6, v17

    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 187
    :cond_0
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    array-length v5, v5

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    move v11, v5

    goto :goto_2

    .line 189
    :cond_2
    const/4 v5, 0x0

    move v12, v5

    goto :goto_3

    .line 192
    :cond_3
    invoke-static {v6, v4}, Lcom/google/android/apps/gmm/map/t/as;->a(Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/internal/c/m;)Z

    move-result v5

    if-nez v5, :cond_4

    if-nez v11, :cond_30

    if-nez v15, :cond_30

    if-nez v12, :cond_30

    :cond_4
    move-object v5, v6

    check-cast v5, Lcom/google/android/apps/gmm/map/internal/c/h;

    .line 196
    iget-object v7, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->m:Ljava/lang/String;

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/internal/c/h;->m:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v7, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->n:Ljava/lang/String;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/h;->n:Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x1

    :goto_8
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_30

    move-object v7, v6

    goto :goto_4

    :cond_5
    const/4 v5, 0x0

    goto :goto_8

    .line 205
    :cond_6
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    array-length v5, v5

    goto :goto_5

    :cond_7
    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->d:Lcom/google/android/apps/gmm/map/b/a/ax;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v6, :cond_8

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v6, v6

    if-lez v6, :cond_8

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v5, v5

    div-int/lit8 v5, v5, 0x3

    :goto_9
    mul-int/lit8 v5, v5, 0x3

    move v11, v5

    goto :goto_6

    :cond_8
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v5, v5

    div-int/lit8 v5, v5, 0x9

    goto :goto_9

    :cond_9
    const/4 v5, 0x0

    move v11, v5

    goto :goto_6

    .line 209
    :cond_a
    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->d:Lcom/google/android/apps/gmm/map/b/a/ax;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v5, v5

    div-int/lit8 v6, v5, 0x3

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->g:[I

    if-nez v5, :cond_d

    const/4 v5, 0x1

    :goto_a
    add-int/2addr v6, v5

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->f:[I

    if-nez v5, :cond_e

    const/4 v5, 0x0

    :goto_b
    sub-int v5, v6, v5

    shl-int/lit8 v6, v5, 0x1

    .line 210
    :cond_b
    :goto_c
    if-eqz v12, :cond_13

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v12, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    if-nez v12, :cond_10

    const/4 v5, 0x0

    :goto_d
    if-nez v5, :cond_11

    const/4 v5, 0x0

    .line 211
    :goto_e
    sget v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->d:I

    if-gt v11, v12, :cond_c

    sget v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->d:I

    if-gt v6, v12, :cond_c

    sget v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->d:I

    if-le v5, v12, :cond_14

    .line 216
    :cond_c
    const-string v2, "GLAreaGroup"

    const-string v4, "unable to handle the Area with (fillVertexCount, strokeVertexCount, levelVertexCount) = (%d, %d, %d) for %s since the limit is %d."

    const/4 v12, 0x5

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v15, 0x0

    .line 220
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v12, v15

    const/4 v11, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v12, v11

    const/4 v6, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v12, v6

    const/4 v5, 0x3

    aput-object p0, v12, v5

    const/4 v5, 0x4

    sget v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->d:I

    .line 221
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v12, v5

    .line 217
    invoke-static {v4, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    .line 216
    invoke-static {v2, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 222
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    move-object v6, v7

    .line 223
    goto/16 :goto_0

    .line 209
    :cond_d
    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->g:[I

    array-length v5, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    :cond_e
    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->f:[I

    array-length v5, v5

    goto :goto_b

    :cond_f
    const/4 v6, 0x0

    goto :goto_c

    .line 210
    :cond_10
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    array-length v5, v5

    goto :goto_d

    :cond_11
    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->d:Lcom/google/android/apps/gmm/map/b/a/ax;

    iget-object v12, v5, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v12, :cond_12

    iget-object v12, v5, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v12, v12

    if-lez v12, :cond_12

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v5, v5

    div-int/lit8 v5, v5, 0x3

    :goto_f
    mul-int/lit8 v5, v5, 0x3

    goto :goto_e

    :cond_12
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v5, v5

    div-int/lit8 v5, v5, 0x9

    goto :goto_f

    :cond_13
    const/4 v5, 0x0

    goto :goto_e

    .line 225
    :cond_14
    add-int v12, v10, v11

    sget v16, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->d:I

    move/from16 v0, v16

    if-gt v12, v0, :cond_1e

    add-int v12, v9, v6

    sget v16, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->d:I

    move/from16 v0, v16

    if-gt v12, v0, :cond_1e

    add-int v12, v8, v5

    sget v16, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->d:I

    move/from16 v0, v16

    if-gt v12, v0, :cond_1e

    .line 228
    add-int/2addr v10, v11

    .line 231
    add-int/2addr v9, v6

    .line 232
    add-int v6, v8, v5

    .line 233
    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/c/m;->j()[I

    move-result-object v5

    array-length v8, v5

    const/4 v4, 0x0

    :goto_10
    if-ge v4, v8, :cond_16

    aget v11, v5, v4

    .line 234
    if-ltz v11, :cond_15

    move-object/from16 v0, p1

    array-length v12, v0

    if-ge v11, v12, :cond_15

    .line 235
    aget-object v11, p1, v11

    move-object/from16 v0, p12

    invoke-interface {v0, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 233
    :cond_15
    add-int/lit8 v4, v4, 0x1

    goto :goto_10

    .line 238
    :cond_16
    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/map/internal/c/u;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/h;)Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v8

    .line 245
    if-eqz p5, :cond_1b

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->e:[B

    if-nez v4, :cond_17

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->g:[I

    if-eqz v4, :cond_1c

    :cond_17
    const/4 v4, 0x1

    :goto_11
    if-eqz v4, :cond_1b

    .line 246
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/c/be;->d()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_18

    if-eqz v15, :cond_19

    :cond_18
    if-eqz v8, :cond_1b

    .line 247
    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/internal/c/be;->d()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1b

    .line 248
    :cond_19
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/h;->f()Ljava/util/List;

    move-result-object v11

    .line 250
    if-eqz v11, :cond_1b

    .line 254
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    move-object/from16 v0, p5

    invoke-static {v0, v2, v11, v4, v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;Lcom/google/android/apps/gmm/map/internal/c/h;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/internal/c/be;)V

    .line 256
    if-eqz v8, :cond_1b

    .line 258
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/c/bv;->b:Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v4

    aget-object v4, v5, v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/c/w;

    .line 260
    if-eqz v4, :cond_1d

    .line 263
    invoke-interface {v14, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 264
    if-nez v5, :cond_1a

    .line 265
    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v12, 0x0

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    aget v5, v5, v12

    invoke-static {v8, v5}, Lcom/google/android/apps/gmm/map/internal/c/be;->a(Lcom/google/android/apps/gmm/map/internal/c/be;I)Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v5

    .line 266
    invoke-interface {v14, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    :cond_1a
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    move-object/from16 v0, p5

    invoke-static {v0, v2, v11, v4, v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;Lcom/google/android/apps/gmm/map/internal/c/h;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/internal/c/be;)V

    .line 282
    :cond_1b
    :goto_12
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    move v8, v6

    move-object v6, v7

    .line 283
    goto/16 :goto_0

    .line 245
    :cond_1c
    const/4 v4, 0x0

    goto :goto_11

    .line 271
    :cond_1d
    const-string v2, "GLAreaGroup"

    const-string v4, "Bad area for indoor level highlight outline. All such areas have to be come from TileCoords with IndoorParameters, while it came from: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x0

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-direct {v8, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_12

    :cond_1e
    move-object v11, v7

    .line 285
    :goto_13
    new-instance v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v10, v9, v8}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;III)V

    .line 287
    const/4 v5, 0x0

    .line 288
    const/4 v4, 0x0

    .line 289
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move v9, v4

    move v10, v5

    :goto_14
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_24

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/c/h;

    .line 290
    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/map/internal/c/t;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/h;)Lcom/google/android/apps/gmm/map/internal/c/t;

    move-result-object v5

    .line 292
    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v7, v6, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    if-nez v7, :cond_1f

    const/4 v6, 0x0

    :goto_15
    if-lez v6, :cond_20

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v7, 0x0

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    aget v6, v6, v7

    invoke-static {v6}, Landroid/graphics/Color;->alpha(I)I

    move-result v6

    if-eqz v6, :cond_20

    const/4 v6, 0x1

    .line 293
    :goto_16
    invoke-static {v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a(Lcom/google/android/apps/gmm/map/internal/c/h;)Z

    move-result v7

    .line 294
    iget v8, v4, Lcom/google/android/apps/gmm/map/internal/c/h;->l:I

    const/4 v12, 0x4

    and-int/2addr v8, v12

    if-eqz v8, :cond_21

    const/4 v8, 0x1

    .line 295
    :goto_17
    if-eqz v6, :cond_23

    iget-object v12, v4, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v14, 0x0

    iget-object v12, v12, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    aget v12, v12, v14

    invoke-static {v12}, Landroid/graphics/Color;->alpha(I)I

    move-result v12

    const/16 v14, 0xff

    if-eq v12, v14, :cond_22

    const/4 v12, 0x1

    :goto_18
    if-eqz v12, :cond_23

    const/4 v12, 0x1

    :goto_19
    or-int/2addr v10, v12

    .line 296
    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a(Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/map/internal/c/h;Lcom/google/android/apps/gmm/map/internal/c/t;ZZZ)V

    .line 297
    iget-boolean v4, v4, Lcom/google/android/apps/gmm/map/internal/c/h;->r:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    or-int/2addr v4, v9

    move v9, v4

    .line 298
    goto :goto_14

    .line 292
    :cond_1f
    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    array-length v6, v6

    goto :goto_15

    :cond_20
    const/4 v6, 0x0

    goto :goto_16

    .line 294
    :cond_21
    const/4 v8, 0x0

    goto :goto_17

    .line 295
    :cond_22
    const/4 v12, 0x0

    goto :goto_18

    :cond_23
    const/4 v12, 0x0

    goto :goto_19

    .line 299
    :cond_24
    new-instance v12, Lcom/google/android/apps/gmm/map/t/as;

    invoke-direct {v12, v11}, Lcom/google/android/apps/gmm/map/t/as;-><init>(Lcom/google/android/apps/gmm/map/internal/c/m;)V

    if-nez p6, :cond_2e

    const/4 v3, 0x1

    move v11, v3

    :goto_1a
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k()I

    move-result v3

    if-lez v3, :cond_28

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v4, 0x4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v3

    iget v4, v3, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    iput v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a:I

    new-instance v13, Lcom/google/android/apps/gmm/map/t/ar;

    move-object/from16 v0, p7

    move-object/from16 v1, p0

    invoke-direct {v13, v0, v1, v12, v11}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x9

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "AreaFill "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v13, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    invoke-virtual {v13, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    move-object/from16 v0, p8

    invoke-virtual {v13, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    if-eqz v10, :cond_25

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v4, 0x302

    const/16 v5, 0x303

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v3

    invoke-virtual {v13, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    :cond_25
    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    move-object/from16 v0, p10

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x0

    iget-boolean v4, v13, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v4, :cond_26

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_26
    int-to-byte v3, v3

    iput-byte v3, v13, Lcom/google/android/apps/gmm/v/aa;->w:B

    if-eqz v9, :cond_27

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_27

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->h:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/internal/c/h;

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/internal/c/ap;->a(Lcom/google/android/apps/gmm/map/internal/c/h;)Lcom/google/android/apps/gmm/map/internal/c/ap;

    move-result-object v9

    new-instance v6, Lcom/google/android/apps/gmm/map/t/v;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->h:Ljava/util/List;

    invoke-direct {v6, v3}, Lcom/google/android/apps/gmm/map/t/v;-><init>(Ljava/util/List;)V

    new-instance v3, Lcom/google/android/apps/gmm/map/t/y;

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget-object v7, Lcom/google/android/apps/gmm/map/j/v;->a:Lcom/google/android/apps/gmm/map/j/v;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v9, v8, v10

    move-object/from16 v9, p9

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/gmm/map/t/y;-><init>(ZZLcom/google/android/apps/gmm/map/t/x;Lcom/google/android/apps/gmm/map/j/s;[Ljava/lang/Object;Lcom/google/android/apps/gmm/map/util/b/g;)V

    invoke-virtual {v13, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    :cond_27
    if-eqz p6, :cond_28

    move-object/from16 v0, p6

    invoke-virtual {v0, v13}, Lcom/google/android/apps/gmm/map/indoor/c/r;->a(Lcom/google/android/apps/gmm/map/t/ar;)V

    :cond_28
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k()I

    move-result v3

    if-lez v3, :cond_2a

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v3

    iget v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a:I

    iget v5, v3, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    add-int/2addr v4, v5

    iput v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a:I

    new-instance v4, Lcom/google/android/apps/gmm/map/t/ar;

    const/4 v5, 0x1

    new-instance v6, Lcom/google/android/apps/gmm/map/t/as;

    iget v7, v12, Lcom/google/android/apps/gmm/map/t/as;->a:I

    iget v8, v12, Lcom/google/android/apps/gmm/map/t/as;->b:I

    iget v9, v12, Lcom/google/android/apps/gmm/map/t/as;->d:I

    invoke-direct {v6, v7, v8, v5, v9}, Lcom/google/android/apps/gmm/map/t/as;-><init>(IIII)V

    move-object/from16 v0, p7

    move-object/from16 v1, p0

    invoke-direct {v4, v0, v1, v6, v11}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0xb

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "AreaStroke "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    move-object/from16 v0, p8

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    move-object/from16 v0, p10

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x0

    iget-boolean v5, v4, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v5, :cond_29

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_29
    int-to-byte v3, v3

    iput-byte v3, v4, Lcom/google/android/apps/gmm/v/aa;->w:B

    if-eqz p6, :cond_2a

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/indoor/c/r;->a(Lcom/google/android/apps/gmm/map/t/ar;)V

    :cond_2a
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k()I

    move-result v3

    if-lez v3, :cond_2d

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v4, 0x4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v3

    iget v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a:I

    iget v5, v3, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    add-int/2addr v4, v5

    iput v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a:I

    new-instance v10, Lcom/google/android/apps/gmm/map/t/ar;

    sget-object v4, Lcom/google/android/apps/gmm/map/t/l;->c:Lcom/google/android/apps/gmm/map/t/l;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v10, v4, v0, v5, v11}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xb

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "AreaShadow "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v10, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    invoke-virtual {v10, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    move-object/from16 v0, p8

    invoke-virtual {v10, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    new-instance v4, Lcom/google/android/apps/gmm/v/m;

    const/16 v5, 0x302

    const/16 v6, 0x303

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/gmm/v/m;-><init>(II)V

    invoke-virtual {v10, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    move-object/from16 v0, p10

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x0

    iget-boolean v5, v10, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v5, :cond_2b

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_2b
    int-to-byte v4, v4

    iput-byte v4, v10, Lcom/google/android/apps/gmm/v/aa;->w:B

    new-instance v11, Lcom/google/android/apps/gmm/map/t/ar;

    const/4 v4, 0x2

    new-instance v5, Lcom/google/android/apps/gmm/map/t/as;

    iget v6, v12, Lcom/google/android/apps/gmm/map/t/as;->a:I

    iget v7, v12, Lcom/google/android/apps/gmm/map/t/as;->b:I

    iget v8, v12, Lcom/google/android/apps/gmm/map/t/as;->d:I

    invoke-direct {v5, v6, v7, v4, v8}, Lcom/google/android/apps/gmm/map/t/as;-><init>(IIII)V

    const/4 v4, 0x0

    move-object/from16 v0, p7

    move-object/from16 v1, p0

    invoke-direct {v11, v0, v1, v5, v4}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xa

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "AreaDepth "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v11, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    invoke-virtual {v11, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    new-instance v3, Lcom/google/android/apps/gmm/v/x;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/v/x;-><init>()V

    invoke-virtual {v11, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v4, 0x201

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/v/bk;->a(I)Lcom/google/android/apps/gmm/v/bm;

    move-result-object v3

    invoke-virtual {v11, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v3

    invoke-virtual {v11, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    new-instance v3, Lcom/google/android/apps/gmm/map/t/y;

    const/4 v4, 0x0

    const/4 v5, 0x1

    new-instance v6, Lcom/google/android/apps/gmm/map/t/t;

    iget-object v7, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->b:Lcom/google/android/apps/gmm/map/b/a/e;

    invoke-direct {v6, v7}, Lcom/google/android/apps/gmm/map/t/t;-><init>(Lcom/google/android/apps/gmm/map/b/a/d;)V

    sget-object v7, Lcom/google/android/apps/gmm/map/j/n;->a:Lcom/google/android/apps/gmm/map/j/n;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v12, 0x0

    sget-object v9, Lcom/google/android/apps/gmm/map/internal/c/bv;->b:Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v13, v13, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v9

    aget-object v9, v13, v9

    check-cast v9, Lcom/google/android/apps/gmm/map/internal/c/w;

    if-nez v9, :cond_2f

    const/4 v9, 0x0

    :goto_1b
    aput-object v9, v8, v12

    move-object/from16 v9, p9

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/gmm/map/t/y;-><init>(ZZLcom/google/android/apps/gmm/map/t/x;Lcom/google/android/apps/gmm/map/j/s;[Ljava/lang/Object;Lcom/google/android/apps/gmm/map/util/b/g;)V

    invoke-virtual {v11, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    move-object/from16 v0, p10

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x0

    iget-boolean v4, v11, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v4, :cond_2c

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_2c
    int-to-byte v3, v3

    iput-byte v3, v11, Lcom/google/android/apps/gmm/v/aa;->w:B

    if-eqz p6, :cond_2d

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/gmm/map/indoor/c/r;->a(Lcom/google/android/apps/gmm/map/t/ar;)V

    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/c/r;->d:Ljava/util/Collection;

    invoke-interface {v3, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/c/r;->b:Lcom/google/android/apps/gmm/v/g;

    if-eqz v3, :cond_2d

    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/c/r;->b:Lcom/google/android/apps/gmm/v/g;

    sget-object v4, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    move-object/from16 v0, p6

    invoke-interface {v3, v0, v4}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    :cond_2d
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 312
    return-object v2

    .line 299
    :cond_2e
    const/4 v3, 0x0

    move v11, v3

    goto/16 :goto_1a

    :cond_2f
    iget-object v9, v9, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    goto :goto_1b

    :cond_30
    move-object v11, v6

    goto/16 :goto_13
.end method

.method private a(Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/map/internal/c/h;Lcom/google/android/apps/gmm/map/internal/c/t;ZZZ)V
    .locals 14

    .prologue
    .line 342
    if-nez p4, :cond_1

    if-nez p5, :cond_1

    if-nez p6, :cond_1

    .line 450
    :cond_0
    :goto_0
    return-void

    .line 346
    :cond_1
    move-object/from16 v0, p2

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->r:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 347
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->h:Ljava/util/List;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 349
    :cond_2
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 351
    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->e:[B

    .line 352
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->d:Lcom/google/android/apps/gmm/map/b/a/ax;

    .line 353
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v2, :cond_b

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    if-lez v2, :cond_b

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    move v11, v2

    .line 354
    :goto_1
    if-eqz v11, :cond_0

    .line 361
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 363
    if-eqz p4, :cond_3

    const/4 v2, 0x0

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    aget v2, v5, v2

    .line 364
    :cond_3
    if-eqz p4, :cond_c

    const/4 v2, 0x0

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    aget v2, v5, v2

    move v10, v2

    .line 365
    :goto_2
    if-eqz p5, :cond_4

    const/4 v2, 0x0

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v2, v5, v2

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    .line 366
    :cond_4
    if-eqz p5, :cond_d

    const/4 v2, 0x0

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v2, v4, v2

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    move v9, v2

    .line 367
    :goto_3
    if-eqz p3, :cond_e

    move-object/from16 v0, p3

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/c/t;->a:I

    move v7, v2

    .line 369
    :goto_4
    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->i:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x0

    aget-object v4, v2, v4

    .line 370
    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->i:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v5, 0x1

    aget-object v5, v2, v5

    .line 371
    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->i:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v6, 0x2

    aget-object v6, v2, v6

    .line 372
    const/4 v2, 0x0

    :goto_5
    if-ge v2, v11, :cond_f

    .line 377
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 379
    if-eqz p4, :cond_5

    .line 380
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 381
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 382
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 383
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v13, 0x3

    invoke-virtual {v8, v10, v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(II)V

    .line 384
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    .line 387
    :cond_5
    if-eqz p5, :cond_9

    if-eqz v12, :cond_9

    .line 388
    const/4 v8, 0x0

    .line 389
    aget-byte v13, v12, v2

    and-int/lit8 v13, v13, 0x1

    if-eqz v13, :cond_6

    .line 390
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 391
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 392
    const/4 v8, 0x2

    .line 394
    :cond_6
    aget-byte v13, v12, v2

    and-int/lit8 v13, v13, 0x2

    if-eqz v13, :cond_7

    .line 395
    iget-object v13, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v13, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 396
    iget-object v13, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v13, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 397
    add-int/lit8 v8, v8, 0x2

    .line 399
    :cond_7
    aget-byte v13, v12, v2

    and-int/lit8 v13, v13, 0x4

    if-eqz v13, :cond_8

    .line 400
    iget-object v13, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v13, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 401
    iget-object v13, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v13, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 402
    add-int/lit8 v8, v8, 0x2

    .line 404
    :cond_8
    if-lez v8, :cond_9

    .line 405
    iget-object v13, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v13, v9, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(II)V

    .line 406
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    .line 407
    :cond_9
    if-eqz p6, :cond_a

    .line 413
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 414
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 415
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 418
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b(I)V

    .line 419
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b(I)V

    .line 420
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b(I)V

    .line 422
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    .line 375
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_5

    .line 353
    :cond_b
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x9

    move v11, v2

    goto/16 :goto_1

    .line 364
    :cond_c
    const/4 v2, 0x0

    move v10, v2

    goto/16 :goto_2

    .line 366
    :cond_d
    const/4 v2, 0x0

    move v9, v2

    goto/16 :goto_3

    .line 367
    :cond_e
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_4

    .line 426
    :cond_f
    if-eqz p5, :cond_11

    if-nez v12, :cond_11

    .line 427
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/h;->f()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 428
    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 429
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 430
    const/4 v4, 0x0

    .line 431
    const/4 v2, 0x1

    :goto_7
    iget-object v6, v1, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v6, v6

    div-int/lit8 v6, v6, 0x3

    if-ge v2, v6, :cond_10

    .line 432
    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 433
    invoke-virtual {v1, v2, v3, v6}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 435
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v8, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 436
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 437
    add-int/lit8 v4, v4, 0x2

    .line 431
    add-int/lit8 v2, v2, 0x1

    move-object v5, v6

    goto :goto_7

    .line 440
    :cond_10
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v1, v9, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(II)V

    .line 441
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    goto :goto_6

    .line 446
    :cond_11
    if-eqz p6, :cond_0

    .line 448
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->d:Lcom/google/android/apps/gmm/map/b/a/ax;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->b:Lcom/google/android/apps/gmm/map/b/a/e;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(Lcom/google/android/apps/gmm/map/b/a/e;)V

    goto/16 :goto_0
.end method

.method private static a(Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;Lcom/google/android/apps/gmm/map/internal/c/h;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/internal/c/be;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;",
            "Lcom/google/android/apps/gmm/map/internal/c/h;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            "Lcom/google/android/apps/gmm/map/internal/c/be;",
            ")V"
        }
    .end annotation

    .prologue
    .line 601
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->d:Lcom/google/android/apps/gmm/map/b/a/ax;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v3, :cond_2

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v3, v3

    if-lez v3, :cond_2

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    .line 604
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/map/internal/c/be;->d()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_1

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->e:[B

    if-nez v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->g:[I

    if-eqz v2, :cond_3

    :cond_0
    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_4

    .line 642
    :cond_1
    return-void

    .line 601
    :cond_2
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x9

    goto :goto_0

    .line 604
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 612
    :cond_4
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/map/internal/c/be;->b()Lcom/google/android/apps/gmm/map/internal/c/bh;

    move-result-object v2

    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/internal/c/bh;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    new-instance v11, Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-direct {v11, v2}, Lcom/google/android/apps/gmm/map/internal/c/be;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bh;)V

    .line 615
    move-object/from16 v0, p1

    iget v14, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->k:I

    .line 616
    move-object/from16 v0, p1

    iget v12, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->i:I

    .line 619
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->j:Ljava/lang/String;

    .line 620
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->q:[I

    move-object/from16 v17, v0

    .line 621
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->p:Lcom/google/android/apps/gmm/map/indoor/d/g;

    move-object/from16 v18, v0

    .line 623
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_5
    :goto_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 624
    new-instance v20, Lcom/google/android/apps/gmm/map/internal/c/ad;

    const-wide/16 v4, -0x1

    .line 626
    move-object/from16 v0, p1

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->a:I

    .line 627
    move-object/from16 v0, p1

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->b:I

    const/4 v10, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    if-eqz p3, :cond_6

    const/16 v19, 0x1

    :goto_3
    move-object/from16 v3, v20

    move-object/from16 v8, p3

    invoke-direct/range {v3 .. v19}, Lcom/google/android/apps/gmm/map/internal/c/ad;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/z;Lcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;IFI[ILcom/google/android/apps/gmm/map/indoor/d/g;Z)V

    .line 640
    move-object/from16 v0, v20

    instance-of v2, v0, Lcom/google/android/apps/gmm/map/internal/c/ad;

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->a:Ljava/util/ArrayList;

    new-instance v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;

    move-object/from16 v3, v20

    check-cast v3, Lcom/google/android/apps/gmm/map/internal/c/ad;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->c:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->d:[Ljava/lang/String;

    new-instance v7, Lcom/google/android/apps/gmm/map/t/as;

    move-object/from16 v0, v20

    invoke-direct {v7, v0}, Lcom/google/android/apps/gmm/map/t/as;-><init>(Lcom/google/android/apps/gmm/map/internal/c/m;)V

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;-><init>(Lcom/google/android/apps/gmm/map/internal/c/ad;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/t/as;)V

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 627
    :cond_6
    const/16 v19, 0x0

    goto :goto_3
.end method

.method private static a(Lcom/google/android/apps/gmm/map/internal/c/h;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 724
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 726
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/c/be;->d()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->e:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->g:[I

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_3

    .line 739
    :cond_1
    :goto_1
    return v1

    :cond_2
    move v0, v1

    .line 726
    goto :goto_0

    :cond_3
    move v0, v1

    .line 730
    :goto_2
    iget-object v3, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v3, :cond_4

    move v3, v1

    :goto_3
    if-ge v0, v3, :cond_6

    .line 731
    iget-object v3, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v5

    if-gtz v3, :cond_1

    .line 734
    iget-object v3, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v3, v3, v0

    iget-object v5, v3, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    if-eqz v5, :cond_5

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    array-length v3, v3

    if-lez v3, :cond_5

    move v3, v2

    :goto_4
    if-nez v3, :cond_1

    .line 730
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v3, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v3, v3

    goto :goto_3

    :cond_5
    move v3, v1

    .line 734
    goto :goto_4

    :cond_6
    move v1, v2

    .line 739
    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 646
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 651
    const/16 v0, 0x9c

    return v0
.end method
