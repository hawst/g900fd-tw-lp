.class public Lcom/google/android/apps/gmm/map/internal/c/al;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/ce;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/internal/c/cd;

.field public b:Z

.field private c:Lcom/google/android/apps/gmm/map/internal/c/cd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/cd;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->a:Lcom/google/android/apps/gmm/map/internal/c/cd;

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->b:Z

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/cd;
    .locals 3

    .prologue
    .line 69
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->a:Lcom/google/android/apps/gmm/map/internal/c/cd;

    monitor-enter v1

    .line 70
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->b:Z

    if-eqz v0, :cond_0

    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/s;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->a:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/internal/c/s;-><init>(Lcom/google/android/apps/gmm/map/internal/c/cd;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->c:Lcom/google/android/apps/gmm/map/internal/c/cd;

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->b:Z

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->c:Lcom/google/android/apps/gmm/map/internal/c/cd;

    monitor-exit v1

    return-object v0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 83
    if-nez p1, :cond_0

    .line 96
    :goto_0
    return v0

    .line 87
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->a:Lcom/google/android/apps/gmm/map/internal/c/cd;

    monitor-enter v3

    .line 88
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->a:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/bu;->a()Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-result-object v4

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v4

    aget-object v2, v2, v4

    .line 90
    if-eq v2, p1, :cond_1

    if-eqz v2, :cond_2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v2, v1

    :goto_1
    if-eqz v2, :cond_3

    .line 91
    monitor-exit v3

    goto :goto_0

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move v2, v0

    .line 90
    goto :goto_1

    .line 94
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->a:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->b:Z

    .line 96
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/cd;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 54
    if-nez p1, :cond_0

    .line 55
    new-instance p1, Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {p1}, Lcom/google/android/apps/gmm/map/internal/c/cd;-><init>()V

    .line 57
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/cd;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bv;

    .line 58
    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/cd;Lcom/google/android/apps/gmm/map/internal/c/bv;)Lcom/google/android/apps/gmm/map/internal/c/bu;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->a:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-static {v5, v0}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/cd;Lcom/google/android/apps/gmm/map/internal/c/bv;)Lcom/google/android/apps/gmm/map/internal/c/bu;

    move-result-object v0

    if-nez v0, :cond_4

    if-eqz v4, :cond_2

    invoke-interface {v4, v0}, Lcom/google/android/apps/gmm/map/internal/c/bu;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 62
    :goto_1
    return v0

    :cond_3
    move v0, v1

    .line 58
    goto :goto_0

    :cond_4
    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/map/internal/c/bu;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z

    move-result v0

    goto :goto_0

    :cond_5
    move v0, v2

    .line 62
    goto :goto_1
.end method

.method public final b()J
    .locals 4

    .prologue
    .line 122
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->a:Lcom/google/android/apps/gmm/map/internal/c/cd;

    monitor-enter v1

    .line 123
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/al;->a:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/cd;->hashCode()I

    move-result v0

    int-to-long v2, v0

    monitor-exit v1

    return-wide v2

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
