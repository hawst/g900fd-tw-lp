.class public Lcom/google/android/apps/gmm/iamhere/d/u;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/common/c;
.implements Lcom/google/android/gms/common/d;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Landroid/content/Context;

.field final c:Lcom/google/android/gms/location/reporting/c;

.field private d:Lcom/google/android/apps/gmm/iamhere/d/y;

.field private final e:Lcom/google/android/apps/gmm/base/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/apps/gmm/iamhere/d/u;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/u;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/iamhere/d/y;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/d/u;->b:Landroid/content/Context;

    .line 50
    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/d/u;->d:Lcom/google/android/apps/gmm/iamhere/d/y;

    .line 51
    new-instance v0, Lcom/google/android/gms/location/reporting/c;

    invoke-direct {v0, p1, p0, p0}, Lcom/google/android/gms/location/reporting/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/u;->c:Lcom/google/android/gms/location/reporting/c;

    .line 52
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/u;->e:Lcom/google/android/apps/gmm/base/a;

    .line 53
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/u;->a:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 3

    .prologue
    .line 65
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/u;->a:Ljava/lang/String;

    const-string v0, "ReportingClient connection failed:"

    invoke-virtual {p1}, Lcom/google/android/gms/common/a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 66
    :goto_0
    return-void

    .line 65
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/j/d/a/e;Lcom/google/j/d/a/w;Lcom/google/b/f/t;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/j;)Z
    .locals 9
    .param p2    # Lcom/google/j/d/a/w;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/b/f/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/j/d/a/e;",
            "Lcom/google/j/d/a/w;",
            "Lcom/google/b/f/t;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/l;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 102
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/u;->a:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2d

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "starting log here notification report type : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/u;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->B_()Landroid/accounts/Account;

    move-result-object v3

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/u;->c:Lcom/google/android/gms/location/reporting/c;

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/iamhere/e/a;->a(Lcom/google/android/gms/location/reporting/c;Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/u;->a:Ljava/lang/String;

    move v0, v2

    .line 139
    :goto_0
    return v0

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/u;->d:Lcom/google/android/apps/gmm/iamhere/d/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/d/y;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 110
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/u;->a:Ljava/lang/String;

    move v0, v2

    .line 111
    goto :goto_0

    .line 113
    :cond_1
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/apps/gmm/iamhere/e/a;->a(Lcom/google/j/d/a/e;Lcom/google/j/d/a/w;Lcom/google/b/f/t;Ljava/util/List;)Lcom/google/j/d/a/b;

    move-result-object v0

    .line 116
    invoke-static {}, Lcom/google/j/d/a/aa;->newBuilder()Lcom/google/j/d/a/ac;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/j/d/a/ac;->a(Lcom/google/j/d/a/b;)Lcom/google/j/d/a/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/j/d/a/ac;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/j/d/a/aa;

    .line 117
    iget-object v4, p0, Lcom/google/android/apps/gmm/iamhere/d/u;->c:Lcom/google/android/gms/location/reporting/c;

    .line 118
    invoke-static {}, Lcom/google/o/e/b;->newBuilder()Lcom/google/o/e/d;

    move-result-object v5

    invoke-static {}, Lcom/google/d/a/a/ds;->newBuilder()Lcom/google/d/a/a/du;

    move-result-object v1

    iget-wide v6, p5, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    iget v8, v1, Lcom/google/d/a/a/du;->a:I

    or-int/lit8 v8, v8, 0x1

    iput v8, v1, Lcom/google/d/a/a/du;->a:I

    iput-wide v6, v1, Lcom/google/d/a/a/du;->b:J

    iget-wide v6, p5, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget v8, v1, Lcom/google/d/a/a/du;->a:I

    or-int/lit8 v8, v8, 0x2

    iput v8, v1, Lcom/google/d/a/a/du;->a:I

    iput-wide v6, v1, Lcom/google/d/a/a/du;->c:J

    invoke-virtual {v1}, Lcom/google/d/a/a/du;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/d/a/a/ds;

    invoke-virtual {v5, v1}, Lcom/google/o/e/d;->a(Lcom/google/d/a/a/ds;)Lcom/google/o/e/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/o/e/d;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/o/e/b;

    invoke-static {v1}, Lcom/google/android/apps/gmm/iamhere/e/c;->a(Lcom/google/o/e/b;)Ljava/lang/String;

    move-result-object v1

    const-string v5, "payload:"

    .line 119
    invoke-virtual {v0}, Lcom/google/j/d/a/aa;->l()[B

    move-result-object v0

    const/4 v6, 0x2

    invoke-static {v0, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/gms/location/places/PlaceReport;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceReport;

    move-result-object v0

    .line 117
    invoke-virtual {v4, v3, v0}, Lcom/google/android/gms/location/reporting/c;->a(Landroid/accounts/Account;Lcom/google/android/gms/location/places/PlaceReport;)I

    move-result v0

    .line 120
    if-eqz v0, :cond_3

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x37

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "ULR place report error: PlaceReportResult = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v2

    .line 123
    goto/16 :goto_0

    .line 119
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 135
    :catch_0
    move-exception v0

    .line 136
    const-string v1, "ULR place report error"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v2

    .line 137
    goto/16 :goto_0

    .line 139
    :cond_3
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public final am_()V
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/u;->a:Ljava/lang/String;

    .line 76
    return-void
.end method
