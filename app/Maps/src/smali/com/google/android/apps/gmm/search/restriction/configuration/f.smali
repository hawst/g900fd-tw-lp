.class public Lcom/google/android/apps/gmm/search/restriction/configuration/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/restriction/configuration/e;


# instance fields
.field final a:Lcom/google/android/apps/gmm/search/restriction/c/k;

.field b:Ljava/lang/Runnable;

.field private final c:Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p5, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/f;->c:Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/k;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/search/restriction/c/k;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/f;->a:Lcom/google/android/apps/gmm/search/restriction/c/k;

    .line 41
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/search/restriction/d/b;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/f;->a:Lcom/google/android/apps/gmm/search/restriction/c/k;

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/f;->c:Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;

    new-instance v1, Lcom/google/android/apps/gmm/search/restriction/configuration/d;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/f;->a:Lcom/google/android/apps/gmm/search/restriction/c/k;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/search/restriction/c/k;->a()Lcom/google/r/b/a/alh;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/search/restriction/configuration/d;-><init>(Lcom/google/r/b/a/alh;)V

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b:Lcom/google/android/apps/gmm/base/fragments/a/b;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b:Lcom/google/android/apps/gmm/base/fragments/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/fragments/a/b;->a(Ljava/lang/Object;)V

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/f;->c:Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;->dismiss()V

    .line 63
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/f;->c:Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b:Lcom/google/android/apps/gmm/base/fragments/a/b;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b:Lcom/google/android/apps/gmm/base/fragments/a/b;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/base/fragments/a/b;->a(Ljava/lang/Object;)V

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/f;->c:Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;->dismiss()V

    .line 73
    return-object v2
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/f;->a:Lcom/google/android/apps/gmm/search/restriction/c/k;

    invoke-static {}, Lcom/google/r/b/a/alh;->d()Lcom/google/r/b/a/alh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/c/k;->a(Lcom/google/r/b/a/alh;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/f;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 82
    const/4 v0, 0x0

    return-object v0
.end method
