.class public final enum Lcom/google/android/apps/gmm/map/t/l;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/t/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/t/l;",
        ">;",
        "Lcom/google/android/apps/gmm/map/t/k;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum B:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum C:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum D:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum E:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum F:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum G:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum H:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum I:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum J:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum K:Lcom/google/android/apps/gmm/map/t/l;

.field private static final L:I

.field private static final synthetic N:[Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum a:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum b:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum c:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum d:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum e:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum f:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum g:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum h:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum i:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum j:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum k:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum l:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum m:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum n:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum o:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum p:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum q:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum r:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum s:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum t:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum u:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum v:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum w:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum x:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum y:Lcom/google/android/apps/gmm/map/t/l;

.field public static final enum z:Lcom/google/android/apps/gmm/map/t/l;


# instance fields
.field private final M:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 116
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "TRAFFIC_OUTLINE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->a:Lcom/google/android/apps/gmm/map/t/l;

    .line 119
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "TRAFFIC_FILL"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->b:Lcom/google/android/apps/gmm/map/t/l;

    .line 125
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "INDOOR_GROUND"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->c:Lcom/google/android/apps/gmm/map/t/l;

    .line 128
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "INDOOR"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->d:Lcom/google/android/apps/gmm/map/t/l;

    .line 131
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "INDOOR_LINES"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->e:Lcom/google/android/apps/gmm/map/t/l;

    .line 134
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "INDOOR_DIMMER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->f:Lcom/google/android/apps/gmm/map/t/l;

    .line 137
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "TRANSIT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->g:Lcom/google/android/apps/gmm/map/t/l;

    .line 140
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "BICYCLING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->h:Lcom/google/android/apps/gmm/map/t/l;

    .line 143
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "POLYLINE_DEACTIVATED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->i:Lcom/google/android/apps/gmm/map/t/l;

    .line 146
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "POLYLINE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->j:Lcom/google/android/apps/gmm/map/t/l;

    .line 149
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "API_OVERLAY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->k:Lcom/google/android/apps/gmm/map/t/l;

    .line 155
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "BUILDING_DEPTH"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->l:Lcom/google/android/apps/gmm/map/t/l;

    .line 158
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "BUILDING_COLOR"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->m:Lcom/google/android/apps/gmm/map/t/l;

    .line 164
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "LABELS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->n:Lcom/google/android/apps/gmm/map/t/l;

    .line 167
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "MY_MAPS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->o:Lcom/google/android/apps/gmm/map/t/l;

    .line 173
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "MY_MAPS_LABELS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->p:Lcom/google/android/apps/gmm/map/t/l;

    .line 176
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "POLYLINE_MEASLES"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->q:Lcom/google/android/apps/gmm/map/t/l;

    .line 179
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "TURN_ARROW_OVERLAY"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->r:Lcom/google/android/apps/gmm/map/t/l;

    .line 182
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "STARS"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->s:Lcom/google/android/apps/gmm/map/t/l;

    .line 185
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "TRAFFIC_INCIDENTS"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->t:Lcom/google/android/apps/gmm/map/t/l;

    .line 188
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "ADS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->u:Lcom/google/android/apps/gmm/map/t/l;

    .line 191
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "SEARCH_RESULT_MEASLES"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->v:Lcom/google/android/apps/gmm/map/t/l;

    .line 194
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "SEARCH_RESULT_ICONS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->w:Lcom/google/android/apps/gmm/map/t/l;

    .line 197
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "FOCUSED_LABEL_HIGHLIGHT"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->x:Lcom/google/android/apps/gmm/map/t/l;

    .line 199
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "FOCUSED_LABEL"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->y:Lcom/google/android/apps/gmm/map/t/l;

    .line 202
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "DEBUG_TILE_BOUNDS"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->z:Lcom/google/android/apps/gmm/map/t/l;

    .line 205
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "LAYER_MARKERS"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->A:Lcom/google/android/apps/gmm/map/t/l;

    .line 208
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "CALLOUT_LABEL"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->B:Lcom/google/android/apps/gmm/map/t/l;

    .line 211
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "CALLOUT_CONTENTS"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->C:Lcom/google/android/apps/gmm/map/t/l;

    .line 214
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "DEBUG_LABELS"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->D:Lcom/google/android/apps/gmm/map/t/l;

    .line 217
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "MY_LOCATION_OVERLAY_VECTORMAPS_BACKGROUND"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->E:Lcom/google/android/apps/gmm/map/t/l;

    .line 220
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "MY_LOCATION_OVERLAY_VECTORMAPS_CHEVRON"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->F:Lcom/google/android/apps/gmm/map/t/l;

    .line 223
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "BREADCRUMB_BOTTOM"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->G:Lcom/google/android/apps/gmm/map/t/l;

    .line 229
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "BREADCRUMB_DEFAULT"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->H:Lcom/google/android/apps/gmm/map/t/l;

    .line 232
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "BREADCRUMB_TOP"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->I:Lcom/google/android/apps/gmm/map/t/l;

    .line 235
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "PLACEMARK"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->J:Lcom/google/android/apps/gmm/map/t/l;

    .line 238
    new-instance v0, Lcom/google/android/apps/gmm/map/t/l;

    const-string v1, "INFO_WINDOWS"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->K:Lcom/google/android/apps/gmm/map/t/l;

    .line 114
    const/16 v0, 0x25

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/t/l;

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->a:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->b:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->c:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->d:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->e:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->f:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->g:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->h:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->i:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->j:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->k:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->l:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->m:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->n:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->o:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->p:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->q:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->r:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->s:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->t:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->u:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->v:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->w:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->x:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->y:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->z:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->A:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->B:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->C:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->D:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->E:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->F:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->G:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->H:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->I:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->J:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->K:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/t/l;->N:[Lcom/google/android/apps/gmm/map/t/l;

    .line 241
    sget v0, Lcom/google/android/apps/gmm/map/t/j;->c:I

    .line 242
    invoke-static {}, Lcom/google/android/apps/gmm/map/t/j;->values()[Lcom/google/android/apps/gmm/map/t/j;

    move-result-object v1

    array-length v1, v1

    add-int/2addr v0, v1

    sput v0, Lcom/google/android/apps/gmm/map/t/l;->L:I

    .line 241
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 251
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/t/l;-><init>(Ljava/lang/String;IZ)V

    .line 252
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 254
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 255
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/map/t/l;->M:Z

    .line 256
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/t/l;
    .locals 1

    .prologue
    .line 114
    const-class v0, Lcom/google/android/apps/gmm/map/t/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/l;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/t/l;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/google/android/apps/gmm/map/t/l;->N:[Lcom/google/android/apps/gmm/map/t/l;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/t/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/t/l;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x4

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/t/l;->ordinal()I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 270
    sget v0, Lcom/google/android/apps/gmm/map/t/l;->L:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/t/l;->ordinal()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 275
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/t/l;->M:Z

    return v0
.end method
