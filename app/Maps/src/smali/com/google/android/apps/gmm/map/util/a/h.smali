.class public Lcom/google/android/apps/gmm/map/util/a/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/util/a/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Value:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/map/util/a/m;"
    }
.end annotation


# instance fields
.field public final a:I

.field private b:Lcom/google/android/apps/gmm/map/util/a/b;

.field private c:La/a/a/a/c/al;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/c/al",
            "<TValue;>;"
        }
    .end annotation
.end field

.field private d:La/a/a/a/c/ab;

.field private e:I

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 90
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/apps/gmm/map/util/a/h;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    .line 91
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/map/util/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, La/a/a/a/c/al;

    invoke-direct {v0}, La/a/a/a/c/al;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->c:La/a/a/a/c/al;

    .line 79
    new-instance v0, La/a/a/a/c/ab;

    invoke-direct {v0}, La/a/a/a/c/ab;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->d:La/a/a/a/c/ab;

    .line 80
    iput p1, p0, Lcom/google/android/apps/gmm/map/util/a/h;->a:I

    .line 81
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/util/a/h;->b:Lcom/google/android/apps/gmm/map/util/a/b;

    .line 82
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/util/a/h;->f:Ljava/lang/String;

    .line 83
    if-eqz p3, :cond_0

    .line 84
    iget-object v1, p3, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    invoke-interface {v0, p0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    .line 86
    :cond_0
    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized a(JZ)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ)TValue;"
        }
    .end annotation

    .prologue
    .line 215
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->c:La/a/a/a/c/al;

    invoke-virtual {v0, p1, p2}, La/a/a/a/c/al;->a(J)Ljava/lang/Object;

    move-result-object v0

    .line 216
    if-eqz v0, :cond_0

    .line 217
    iget v1, p0, Lcom/google/android/apps/gmm/map/util/a/h;->e:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/util/a/h;->d:La/a/a/a/c/ab;

    invoke-virtual {v2, p1, p2}, La/a/a/a/c/ab;->a(J)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/map/util/a/h;->e:I

    .line 218
    if-eqz p3, :cond_0

    .line 220
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/util/a/h;->a(JLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    :cond_0
    monitor-exit p0

    return-object v0

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()I
    .locals 1

    .prologue
    .line 113
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(F)I
    .locals 7

    .prologue
    const/high16 v0, 0x3f000000    # 0.5f

    .line 276
    monitor-enter p0

    const/4 v1, 0x0

    cmpg-float v1, p1, v1

    if-gez v1, :cond_1

    .line 277
    :try_start_0
    const-string v1, "LRUCache"

    const-string v2, "fraction %f < 0"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v0

    .line 280
    :goto_0
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    .line 281
    const-string v2, "LRUCache"

    const-string v3, "fraction %f > 1"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 284
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/util/a/h;->b()I

    move-result v1

    .line 285
    int-to-float v2, v1

    mul-float/2addr v2, v0

    float-to-int v2, v2

    .line 286
    const-string v3, "LRUCache"

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/util/a/h;->f:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x47

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Trimming "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'s current size "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", or "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v0, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/util/a/h;->a(I)V

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->c:La/a/a/a/c/al;

    invoke-virtual {v0}, La/a/a/a/c/al;->k()Z

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->d:La/a/a/a/c/ab;

    invoke-virtual {v0}, La/a/a/a/c/ab;->d()Z

    .line 295
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/util/a/h;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    sub-int v0, v1, v0

    monitor-exit p0

    return v0

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    move v1, p1

    goto :goto_0
.end method

.method public declared-synchronized a(J)Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TValue;"
        }
    .end annotation

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/a/h;->c:La/a/a/a/c/al;

    iget-object v2, v1, La/a/a/a/c/al;->a:[J

    iget-object v3, v1, La/a/a/a/c/al;->c:[Z

    iget v4, v1, La/a/a/a/c/al;->g:I

    invoke-static {p1, p2}, La/a/a/a/c;->a(J)J

    move-result-wide v6

    long-to-int v0, v6

    and-int/2addr v0, v4

    :goto_0
    aget-boolean v5, v3, v0

    if-eqz v5, :cond_1

    aget-wide v6, v2, v0

    cmp-long v5, p1, v6

    if-nez v5, :cond_0

    invoke-virtual {v1, v0}, La/a/a/a/c/al;->a(I)V

    iget-object v1, v1, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v0, v1, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p0

    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    and-int/2addr v0, v4

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 94
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/util/a/h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    monitor-exit p0

    return-void

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)V
    .locals 6

    .prologue
    .line 247
    monitor-enter p0

    if-nez p1, :cond_3

    .line 249
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->d:La/a/a/a/c/ab;

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->c:La/a/a/a/c/al;

    invoke-virtual {v0}, La/a/a/a/c/al;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->c:La/a/a/a/c/al;

    .line 252
    iget-object v1, v0, La/a/a/a/c/al;->i:La/a/a/a/c/az;

    if-nez v1, :cond_0

    new-instance v1, La/a/a/a/c/as;

    invoke-direct {v1, v0}, La/a/a/a/c/as;-><init>(La/a/a/a/c/al;)V

    iput-object v1, v0, La/a/a/a/c/al;->i:La/a/a/a/c/az;

    :cond_0
    iget-object v0, v0, La/a/a/a/c/al;->i:La/a/a/a/c/az;

    invoke-interface {v0}, La/a/a/a/c/az;->c()La/a/a/a/d/bg;

    move-result-object v2

    .line 253
    :goto_0
    invoke-interface {v2}, La/a/a/a/d/bg;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254
    invoke-interface {v2}, La/a/a/a/d/bg;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/a/a/c/aw;

    .line 255
    invoke-interface {v0}, La/a/a/a/c/aw;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v0}, La/a/a/a/c/aw;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v4, v5, v1}, Lcom/google/android/apps/gmm/map/util/a/h;->a(JLjava/lang/Object;)V

    .line 256
    invoke-interface {v0}, La/a/a/a/c/aw;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    invoke-interface {v0}, La/a/a/a/c/aw;->getValue()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 259
    :cond_1
    :try_start_1
    new-instance v0, La/a/a/a/c/al;

    invoke-direct {v0}, La/a/a/a/c/al;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->c:La/a/a/a/c/al;

    .line 260
    new-instance v0, La/a/a/a/c/ab;

    invoke-direct {v0}, La/a/a/a/c/ab;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->d:La/a/a/a/c/ab;

    .line 261
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->e:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 271
    :cond_2
    monitor-exit p0

    return-void

    .line 264
    :cond_3
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->c:La/a/a/a/c/al;

    invoke-virtual {v0}, La/a/a/a/c/al;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->e:I

    if-le v0, p1, :cond_2

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->c:La/a/a/a/c/al;

    invoke-virtual {v0}, La/a/a/a/c/n;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 266
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/map/util/a/h;->c(J)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public a(JLjava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JTValue;)V"
        }
    .end annotation

    .prologue
    .line 138
    return-void
.end method

.method public final declared-synchronized b(J)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TValue;"
        }
    .end annotation

    .prologue
    .line 183
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->c:La/a/a/a/c/al;

    invoke-virtual {v0, p1, p2}, La/a/a/a/c/al;->c(J)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(JLjava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JTValue;)V"
        }
    .end annotation

    .prologue
    .line 153
    monitor-enter p0

    if-nez p3, :cond_1

    .line 155
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/util/a/h;->a(JLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 159
    :cond_1
    const/4 v0, 0x1

    .line 163
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/a/h;->c:La/a/a/a/c/al;

    invoke-virtual {v1, p1, p2, p3}, La/a/a/a/c/al;->b(JLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 167
    iget v2, p0, Lcom/google/android/apps/gmm/map/util/a/h;->e:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/util/a/h;->d:La/a/a/a/c/ab;

    invoke-virtual {v3, p1, p2, v0}, La/a/a/a/c/ab;->a(JI)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/gmm/map/util/a/h;->e:I

    .line 168
    if-eqz v1, :cond_2

    .line 169
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/apps/gmm/map/util/a/h;->a(JLjava/lang/Object;)V

    .line 174
    :goto_1
    iget v1, p0, Lcom/google/android/apps/gmm/map/util/a/h;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->e:I

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->b:Lcom/google/android/apps/gmm/map/util/a/b;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/h;->b:Lcom/google/android/apps/gmm/map/util/a/b;

    goto :goto_0

    .line 172
    :cond_2
    iget v1, p0, Lcom/google/android/apps/gmm/map/util/a/h;->a:I

    sub-int/2addr v1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/util/a/h;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(J)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TValue;"
        }
    .end annotation

    .prologue
    .line 210
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/util/a/h;->a(JZ)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
