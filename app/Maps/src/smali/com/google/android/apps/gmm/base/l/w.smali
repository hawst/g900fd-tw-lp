.class public Lcom/google/android/apps/gmm/base/l/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/f;


# instance fields
.field public a:Ljava/lang/Runnable;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field private final d:Lcom/google/android/libraries/curvular/bi;

.field private final e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/bi;Lcom/google/b/f/t;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->b:Ljava/lang/Boolean;

    .line 18
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->c:Ljava/lang/Boolean;

    .line 40
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/l/w;->d:Lcom/google/android/libraries/curvular/bi;

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/w;->e:Landroid/content/Context;

    .line 42
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->c:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/w;->a:Ljava/lang/Runnable;

    .line 47
    return-void
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->b:Ljava/lang/Boolean;

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 84
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 79
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->b:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->d:Lcom/google/android/libraries/curvular/bi;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/w;->e:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/bi;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
