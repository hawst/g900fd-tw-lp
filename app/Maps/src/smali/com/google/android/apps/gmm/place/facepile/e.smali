.class public Lcom/google/android/apps/gmm/place/facepile/e;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/place/facepile/a/b;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    .line 73
    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 13

    .prologue
    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    const/4 v12, 0x0

    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    const/4 v9, 0x1

    const v8, 0xffffff

    .line 42
    const/4 v0, 0x5

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 43
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v12

    .line 45
    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v3, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v8

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bm:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v9

    const/4 v2, 0x2

    .line 46
    new-instance v3, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_1

    double-to-int v4, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v3, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bj:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v2, 0x3

    .line 47
    new-instance v3, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_2

    double-to-int v4, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v3, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bk:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v2, 0x4

    const/4 v0, 0x4

    new-array v3, v0, [Lcom/google/android/libraries/curvular/cu;

    sget v0, Lcom/google/android/apps/gmm/d;->aR:I

    .line 51
    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    sget-object v4, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v12

    const-wide/high16 v4, 0x402c000000000000L    # 14.0

    .line 53
    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v4, v5}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_3

    double-to-int v4, v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v6, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bj:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v9

    const/4 v4, 0x2

    const-wide/high16 v6, 0x402c000000000000L    # 14.0

    .line 54
    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_4

    double-to-int v6, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v8

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/util/TypedValue;->data:I

    :goto_4
    invoke-direct {v5, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bk:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x3

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/android/libraries/curvular/cu;

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/facepile/e;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v12

    const/high16 v5, 0x3f800000    # 1.0f

    .line 58
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v9

    .line 56
    new-instance v5, Lcom/google/android/libraries/curvular/cs;

    new-array v6, v9, [Lcom/google/android/libraries/curvular/cu;

    const-class v7, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v12

    invoke-direct {v5, v6}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    invoke-virtual {v5, v4}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v4

    aput-object v4, v3, v0

    .line 49
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v3}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.LinearLayout"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v2

    .line 42
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    new-array v2, v9, [Lcom/google/android/libraries/curvular/cu;

    sget v3, Lcom/google/android/apps/gmm/h;->aC:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->g:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v12

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    .line 45
    :cond_0
    mul-double v4, v6, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v3

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v8

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x11

    iput v3, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    .line 46
    :cond_1
    mul-double v4, v6, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x11

    iput v4, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    .line 47
    :cond_2
    mul-double v4, v6, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x11

    iput v4, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    .line 53
    :cond_3
    mul-double/2addr v4, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x11

    iput v4, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3

    .line 54
    :cond_4
    mul-double/2addr v6, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v8

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_4
.end method

.method protected final bridge synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 2

    .prologue
    .line 37
    check-cast p2, Lcom/google/android/apps/gmm/place/facepile/a/b;

    const-class v0, Lcom/google/android/apps/gmm/place/facepile/f;

    invoke-virtual {p4, v0, p2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    const-class v0, Lcom/google/android/apps/gmm/place/facepile/a;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/facepile/a/b;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p4, v0, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Ljava/util/List;)V

    return-void
.end method
