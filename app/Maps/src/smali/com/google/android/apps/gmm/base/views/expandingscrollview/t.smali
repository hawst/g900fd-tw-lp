.class public Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:I

.field final b:I

.field final c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

.field final d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;

.field final e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;

.field f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

.field g:F

.field h:F

.field i:F

.field j:F

.field k:F

.field l:Z

.field m:Landroid/view/View;

.field n:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;)V
    .locals 2

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    .line 101
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->k:F

    .line 111
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->n:Landroid/graphics/Rect;

    .line 115
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 116
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;

    invoke-direct {v0, p2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;-><init>(Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;

    .line 117
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;

    invoke-direct {v0, p3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;-><init>(Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;

    .line 119
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->a:I

    .line 121
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->b:I

    .line 122
    return-void
.end method

.method static a(Landroid/view/View;II)Z
    .locals 8

    .prologue
    .line 358
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 359
    const/4 v0, 0x0

    .line 378
    :goto_0
    return v0

    .line 362
    :cond_0
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 363
    check-cast v0, Landroid/view/ViewGroup;

    .line 364
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v2

    .line 365
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v3

    .line 366
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 368
    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_2

    .line 370
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 371
    add-int v5, p1, v2

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v6

    if-lt v5, v6, :cond_1

    add-int v5, p1, v2

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v6

    if-ge v5, v6, :cond_1

    add-int v5, p2, v3

    .line 372
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v6

    if-lt v5, v6, :cond_1

    add-int v5, p2, v3

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v6

    if-ge v5, v6, :cond_1

    add-int v5, p1, v2

    .line 373
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v6

    sub-int/2addr v5, v6

    add-int v6, p2, v3

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->a(Landroid/view/View;II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 374
    const/4 v0, 0x1

    goto :goto_0

    .line 368
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 378
    :cond_2
    const/4 v0, -0x1

    invoke-static {p0, v0}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v0

    goto :goto_0
.end method
