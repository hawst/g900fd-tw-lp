.class public final enum Lcom/google/android/apps/gmm/navigation/a/b/j;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/navigation/a/b/j;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/navigation/a/b/j;

.field public static final enum b:Lcom/google/android/apps/gmm/navigation/a/b/j;

.field public static final enum c:Lcom/google/android/apps/gmm/navigation/a/b/j;

.field public static final enum d:Lcom/google/android/apps/gmm/navigation/a/b/j;

.field public static final enum e:Lcom/google/android/apps/gmm/navigation/a/b/j;

.field public static final enum f:Lcom/google/android/apps/gmm/navigation/a/b/j;

.field public static final enum g:Lcom/google/android/apps/gmm/navigation/a/b/j;

.field public static final enum h:Lcom/google/android/apps/gmm/navigation/a/b/j;

.field private static final synthetic i:[Lcom/google/android/apps/gmm/navigation/a/b/j;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/b/j;

    const-string v1, "PREPARE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/navigation/a/b/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->a:Lcom/google/android/apps/gmm/navigation/a/b/j;

    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/b/j;

    const-string v1, "ACT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/navigation/a/b/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->b:Lcom/google/android/apps/gmm/navigation/a/b/j;

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/b/j;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/navigation/a/b/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->c:Lcom/google/android/apps/gmm/navigation/a/b/j;

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/b/j;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/navigation/a/b/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->d:Lcom/google/android/apps/gmm/navigation/a/b/j;

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/b/j;

    const-string v1, "DISTANCE_PREFIX"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/navigation/a/b/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->e:Lcom/google/android/apps/gmm/navigation/a/b/j;

    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/b/j;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/a/b/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->f:Lcom/google/android/apps/gmm/navigation/a/b/j;

    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/b/j;

    const-string v1, "OTHER_WITH_LOCALIZED_NAME"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/a/b/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->g:Lcom/google/android/apps/gmm/navigation/a/b/j;

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/b/j;

    const-string v1, "OTHER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/a/b/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->h:Lcom/google/android/apps/gmm/navigation/a/b/j;

    .line 20
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/apps/gmm/navigation/a/b/j;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/b/j;->a:Lcom/google/android/apps/gmm/navigation/a/b/j;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/b/j;->b:Lcom/google/android/apps/gmm/navigation/a/b/j;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/b/j;->c:Lcom/google/android/apps/gmm/navigation/a/b/j;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/b/j;->d:Lcom/google/android/apps/gmm/navigation/a/b/j;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/b/j;->e:Lcom/google/android/apps/gmm/navigation/a/b/j;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/navigation/a/b/j;->f:Lcom/google/android/apps/gmm/navigation/a/b/j;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/navigation/a/b/j;->g:Lcom/google/android/apps/gmm/navigation/a/b/j;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/navigation/a/b/j;->h:Lcom/google/android/apps/gmm/navigation/a/b/j;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->i:[Lcom/google/android/apps/gmm/navigation/a/b/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/navigation/a/b/j;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/gmm/navigation/a/b/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/b/j;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/navigation/a/b/j;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->i:[Lcom/google/android/apps/gmm/navigation/a/b/j;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/navigation/a/b/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/navigation/a/b/j;

    return-object v0
.end method
