.class public final Lcom/google/android/apps/gmm/directions/f/c/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/f/c/g;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/android/apps/gmm/directions/f/c/b;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/android/apps/gmm/directions/f/c/b;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:I

.field public e:J

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/c/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/f/c/c;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/c/b;->PARSER:Lcom/google/n/ax;

    .line 335
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/c/b;->i:Lcom/google/n/aw;

    .line 690
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/c/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/f/c/b;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/c/b;->f:Lcom/google/android/apps/gmm/directions/f/c/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 204
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->b:Lcom/google/n/ao;

    .line 220
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->c:Lcom/google/n/ao;

    .line 266
    iput-byte v3, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->g:B

    .line 306
    iput v3, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->h:I

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 65
    iput v2, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->d:I

    .line 66
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->e:J

    .line 67
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Lcom/google/android/apps/gmm/directions/f/c/b;-><init>()V

    .line 74
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 79
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 80
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 81
    sparse-switch v3, :sswitch_data_0

    .line 86
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 88
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 84
    goto :goto_0

    .line 93
    :sswitch_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 94
    iget v3, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 120
    :catch_0
    move-exception v0

    .line 121
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 126
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->au:Lcom/google/n/bn;

    throw v0

    .line 98
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 99
    iget v3, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 122
    :catch_1
    move-exception v0

    .line 123
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 124
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 103
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 104
    invoke-static {v3}, Lcom/google/android/apps/gmm/directions/f/c/e;->a(I)Lcom/google/android/apps/gmm/directions/f/c/e;

    move-result-object v4

    .line 105
    if-nez v4, :cond_1

    .line 106
    const/4 v4, 0x3

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 108
    :cond_1
    iget v4, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    .line 109
    iput v3, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->d:I

    goto :goto_0

    .line 114
    :sswitch_4
    iget v3, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    .line 115
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->e:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 126
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->au:Lcom/google/n/bn;

    .line 127
    return-void

    .line 81
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 60
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 204
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->b:Lcom/google/n/ao;

    .line 220
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->c:Lcom/google/n/ao;

    .line 266
    iput-byte v1, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->g:B

    .line 306
    iput v1, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->h:I

    .line 61
    return-void
.end method

.method public static d()Lcom/google/android/apps/gmm/directions/f/c/b;
    .locals 1

    .prologue
    .line 693
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/c/b;->f:Lcom/google/android/apps/gmm/directions/f/c/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/android/apps/gmm/directions/f/c/d;
    .locals 1

    .prologue
    .line 397
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/c/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/f/c/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/android/apps/gmm/directions/f/c/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/c/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 290
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/f/c/b;->c()I

    .line 291
    iget v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 294
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 297
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 298
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->d:I

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_4

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 300
    :cond_2
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 301
    iget-wide v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->e:J

    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    .line 303
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 304
    return-void

    .line 298
    :cond_4
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 268
    iget-byte v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->g:B

    .line 269
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 285
    :goto_0
    return v0

    .line 270
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 272
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agd;->d()Lcom/google/r/b/a/agd;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agd;

    invoke-virtual {v0}, Lcom/google/r/b/a/agd;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 274
    iput-byte v2, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->g:B

    move v0, v2

    .line 275
    goto :goto_0

    :cond_2
    move v0, v2

    .line 272
    goto :goto_1

    .line 278
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agl;->d()Lcom/google/r/b/a/agl;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agl;

    invoke-virtual {v0}, Lcom/google/r/b/a/agl;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 280
    iput-byte v2, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->g:B

    move v0, v2

    .line 281
    goto :goto_0

    :cond_4
    move v0, v2

    .line 278
    goto :goto_2

    .line 284
    :cond_5
    iput-byte v1, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->g:B

    move v0, v1

    .line 285
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 308
    iget v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->h:I

    .line 309
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 330
    :goto_0
    return v0

    .line 312
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->b:Lcom/google/n/ao;

    .line 314
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 316
    :goto_1
    iget v2, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 317
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->c:Lcom/google/n/ao;

    .line 318
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 320
    :cond_1
    iget v2, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 321
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->d:I

    .line 322
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_4

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 324
    :cond_2
    iget v2, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 325
    iget-wide v2, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->e:J

    .line 326
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 328
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 329
    iput v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->h:I

    goto :goto_0

    .line 322
    :cond_4
    const/16 v2, 0xa

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/google/android/apps/gmm/directions/f/c/b;->newBuilder()Lcom/google/android/apps/gmm/directions/f/c/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/directions/f/c/d;->a(Lcom/google/android/apps/gmm/directions/f/c/b;)Lcom/google/android/apps/gmm/directions/f/c/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/google/android/apps/gmm/directions/f/c/b;->newBuilder()Lcom/google/android/apps/gmm/directions/f/c/d;

    move-result-object v0

    return-object v0
.end method
