.class public Lcom/google/android/apps/gmm/car/l/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/l/h;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field c:Lcom/google/android/apps/gmm/car/l/l;

.field d:Landroid/graphics/drawable/Drawable;

.field private final e:Lcom/google/android/apps/gmm/car/l/k;

.field private final f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/l/k;ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/car/l/k;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/l/i;->e:Lcom/google/android/apps/gmm/car/l/k;

    .line 38
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/car/l/i;->f:Z

    .line 39
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p3, Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/l/i;->a:Ljava/lang/String;

    .line 41
    sget-object v0, Lcom/google/android/apps/gmm/car/l/l;->a:Lcom/google/android/apps/gmm/car/l/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/l/i;->c:Lcom/google/android/apps/gmm/car/l/l;

    .line 42
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/i;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/i;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/i;->c:Lcom/google/android/apps/gmm/car/l/l;

    sget-object v1, Lcom/google/android/apps/gmm/car/l/l;->b:Lcom/google/android/apps/gmm/car/l/l;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/i;->c:Lcom/google/android/apps/gmm/car/l/l;

    sget-object v1, Lcom/google/android/apps/gmm/car/l/l;->c:Lcom/google/android/apps/gmm/car/l/l;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/google/android/apps/gmm/car/l/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/l/j;-><init>(Lcom/google/android/apps/gmm/car/l/i;)V

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/l/i;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/i;->e:Lcom/google/android/apps/gmm/car/l/k;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/l/k;->a()V

    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/l/i;->e:Lcom/google/android/apps/gmm/car/l/k;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/l/k;->b()V

    .line 112
    const/4 v0, 0x0

    return-object v0
.end method
