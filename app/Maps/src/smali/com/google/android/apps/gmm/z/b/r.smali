.class public Lcom/google/android/apps/gmm/z/b/r;
.super Lcom/google/android/apps/gmm/z/b/e;
.source "PG"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/z/b/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/z/b/s;",
            ">;",
            "Lcom/google/android/apps/gmm/shared/c/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/z/b/e;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 20
    invoke-static {p1}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/b/r;->a:Ljava/util/List;

    .line 21
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/r/b/a/apf;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 25
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/z/b/e;->a(Lcom/google/r/b/a/apf;)V

    .line 27
    invoke-static {}, Lcom/google/b/f/ad;->newBuilder()Lcom/google/b/f/af;

    move-result-object v1

    .line 28
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/r;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/z/b/s;

    .line 29
    invoke-static {}, Lcom/google/b/f/x;->newBuilder()Lcom/google/b/f/z;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/apps/gmm/z/b/s;->b:Lcom/google/b/f/aa;

    if-nez v4, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v5, v3, Lcom/google/b/f/z;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v3, Lcom/google/b/f/z;->a:I

    iget v4, v4, Lcom/google/b/f/aa;->f:I

    iput v4, v3, Lcom/google/b/f/z;->c:I

    iget-object v4, v0, Lcom/google/android/apps/gmm/z/b/s;->a:Lcom/google/android/apps/gmm/z/b/l;

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/google/android/apps/gmm/z/b/s;->a:Lcom/google/android/apps/gmm/z/b/l;

    iget-object v4, v4, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/apps/gmm/z/b/s;->a:Lcom/google/android/apps/gmm/z/b/l;

    iget-object v5, v5, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/z/b/t;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/b/f/aj;

    move-result-object v4

    iget-object v5, v3, Lcom/google/b/f/z;->b:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/b/f/aj;->g()Lcom/google/n/t;

    move-result-object v4

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v8, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v7, v5, Lcom/google/n/ao;->d:Z

    iget v4, v3, Lcom/google/b/f/z;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/google/b/f/z;->a:I

    :cond_1
    iget-object v4, v0, Lcom/google/android/apps/gmm/z/b/s;->c:Lcom/google/android/apps/gmm/z/b/l;

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/google/android/apps/gmm/z/b/s;->c:Lcom/google/android/apps/gmm/z/b/l;

    iget-object v4, v4, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/s;->c:Lcom/google/android/apps/gmm/z/b/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/google/android/apps/gmm/z/b/t;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/b/f/aj;

    move-result-object v0

    iget-object v4, v3, Lcom/google/b/f/z;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/b/f/aj;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v8, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v7, v4, Lcom/google/n/ao;->d:Z

    iget v0, v3, Lcom/google/b/f/z;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v3, Lcom/google/b/f/z;->a:I

    :cond_2
    invoke-virtual {v3}, Lcom/google/b/f/z;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/x;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    invoke-virtual {v1}, Lcom/google/b/f/af;->c()V

    iget-object v3, v1, Lcom/google/b/f/af;->a:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    invoke-direct {v4}, Lcom/google/n/ao;-><init>()V

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v8, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v7, v4, Lcom/google/n/ao;->d:Z

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 31
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/apf;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/b/f/af;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v8, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v7, v0, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/r/b/a/apf;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p1, Lcom/google/r/b/a/apf;->a:I

    .line 32
    return-void
.end method
