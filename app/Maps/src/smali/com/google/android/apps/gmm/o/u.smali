.class public Lcom/google/android/apps/gmm/o/u;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/an;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    if-eqz p3, :cond_0

    if-eqz p4, :cond_1

    if-eqz p5, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 33
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/o/u;->a:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lcom/google/android/apps/gmm/o/u;->b:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/google/android/apps/gmm/o/u;->c:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lcom/google/android/apps/gmm/o/u;->d:Ljava/lang/String;

    .line 37
    iput-object p5, p0, Lcom/google/android/apps/gmm/o/u;->e:Ljava/lang/String;

    .line 38
    return-void
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/u;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/u;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 52
    :goto_0
    return-object v4

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/u;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v3, p0, Lcom/google/android/apps/gmm/o/u;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final b()Lcom/google/android/libraries/curvular/aw;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 77
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 71
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 99
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/u;->d:Ljava/lang/String;

    .line 100
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/u;->e:Ljava/lang/String;

    .line 101
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 102
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 83
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/aw;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 64
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    return-object v0
.end method
