.class public final Lcom/google/android/apps/gmm/car/i/x;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:Lcom/google/android/apps/gmm/search/al;

.field c:Lcom/google/android/apps/gmm/car/i/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/car/i/k",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/car/bm;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/apps/gmm/search/ah;

.field private final e:Lcom/google/android/apps/gmm/map/t;

.field private final f:Lcom/google/android/apps/gmm/p/b/a;

.field private final g:Lcom/google/android/apps/gmm/hotels/a/b;

.field private final h:I

.field private final i:Lcom/google/android/apps/gmm/search/am;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/apps/gmm/car/i/x;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/i/x;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/search/ah;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/p/b/a;Lcom/google/android/apps/gmm/hotels/a/b;I)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    new-instance v0, Lcom/google/android/apps/gmm/car/i/y;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/i/y;-><init>(Lcom/google/android/apps/gmm/car/i/x;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/x;->i:Lcom/google/android/apps/gmm/search/am;

    .line 53
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/search/ah;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/i/x;->d:Lcom/google/android/apps/gmm/search/ah;

    .line 54
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/map/t;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/i/x;->e:Lcom/google/android/apps/gmm/map/t;

    .line 55
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/p/b/a;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/i/x;->f:Lcom/google/android/apps/gmm/p/b/a;

    .line 56
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p4, Lcom/google/android/apps/gmm/hotels/a/b;

    iput-object p4, p0, Lcom/google/android/apps/gmm/car/i/x;->g:Lcom/google/android/apps/gmm/hotels/a/b;

    .line 57
    iput p5, p0, Lcom/google/android/apps/gmm/car/i/x;->h:I

    .line 58
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/x;->b:Lcom/google/android/apps/gmm/search/al;

    if-nez v0, :cond_1

    .line 127
    :cond_0
    return-void

    .line 109
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/car/i/x;->a:Ljava/lang/String;

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/x;->b:Lcom/google/android/apps/gmm/search/al;

    .line 116
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/x;->c:Lcom/google/android/apps/gmm/car/i/k;

    .line 117
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/i/x;->b:Lcom/google/android/apps/gmm/search/al;

    .line 118
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/i/x;->c:Lcom/google/android/apps/gmm/car/i/k;

    .line 119
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/x;->d:Lcom/google/android/apps/gmm/search/ah;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/al;->f()V

    .line 120
    invoke-interface {v1}, Lcom/google/android/apps/gmm/car/i/k;->b()V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/x;->b:Lcom/google/android/apps/gmm/search/al;

    if-eqz v0, :cond_0

    .line 125
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Tried to start a search while it was being canceled."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/maps/a/a;[BLcom/google/android/apps/gmm/car/i/k;)V
    .locals 8
    .param p2    # Lcom/google/maps/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # [B
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/maps/a/a;",
            "[B",
            "Lcom/google/android/apps/gmm/car/i/k",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/car/bm;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    .line 85
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 86
    :cond_0
    if-nez p4, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 88
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/i/x;->a()V

    .line 90
    new-instance v3, Lcom/google/android/apps/gmm/search/aj;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/search/aj;-><init>()V

    if-eqz p1, :cond_2

    const-string v0, "\\s+"

    const-string v2, " "

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/search/aj;->a:Ljava/lang/String;

    :cond_2
    if-eqz p3, :cond_3

    iput-object p3, v3, Lcom/google/android/apps/gmm/search/aj;->e:[B

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/x;->e:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v2

    if-nez v2, :cond_5

    if-nez p2, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/car/i/x;->a:Ljava/lang/String;

    move-object v0, v1

    .line 91
    :goto_0
    if-nez v0, :cond_d

    .line 92
    invoke-interface {p4}, Lcom/google/android/apps/gmm/car/i/k;->a()V

    .line 99
    :goto_1
    return-void

    .line 90
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/car/i/x;->a:Ljava/lang/String;

    move-object v0, p2

    :goto_2
    if-eqz v0, :cond_9

    invoke-static {v0}, Lcom/google/maps/a/a;->a(Lcom/google/maps/a/a;)Lcom/google/maps/a/c;

    move-result-object v2

    iget-object v0, v0, Lcom/google/maps/a/a;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/i;->d()Lcom/google/maps/a/i;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/i;

    invoke-static {v0}, Lcom/google/maps/a/i;->a(Lcom/google/maps/a/i;)Lcom/google/maps/a/k;

    move-result-object v0

    const/4 v4, 0x0

    iget v5, v0, Lcom/google/maps/a/k;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v0, Lcom/google/maps/a/k;->a:I

    iput v4, v0, Lcom/google/maps/a/k;->c:F

    invoke-virtual {v0}, Lcom/google/maps/a/k;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/i;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget-object v0, v2, Lcom/google/maps/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    iget-wide v4, v0, Lcom/google/maps/a/e;->c:D

    cmpl-double v0, v4, v6

    if-nez v0, :cond_7

    iget-object v0, v2, Lcom/google/maps/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    iget-wide v4, v0, Lcom/google/maps/a/e;->b:D

    cmpl-double v0, v4, v6

    if-nez v0, :cond_7

    if-eqz p2, :cond_6

    sget-object v0, Lcom/google/android/apps/gmm/car/i/x;->a:Ljava/lang/String;

    move-object v0, p2

    goto :goto_2

    :cond_6
    sget-object v0, Lcom/google/android/apps/gmm/car/i/x;->a:Ljava/lang/String;

    :cond_7
    move-object v0, v2

    goto :goto_2

    :cond_8
    iget-object v4, v2, Lcom/google/maps/a/c;->c:Lcom/google/n/ao;

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v4, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v2, Lcom/google/maps/a/c;->a:I

    invoke-virtual {v2}, Lcom/google/maps/a/c;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/a;

    :cond_9
    iput-object v0, v3, Lcom/google/android/apps/gmm/search/aj;->d:Lcom/google/maps/a/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/x;->f:Lcom/google/android/apps/gmm/p/b/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v0

    sget-object v1, Lcom/google/o/b/a/a/a;->f:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/search/aj;->g:Lcom/google/e/a/a/a/b;

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/x;->g:Lcom/google/android/apps/gmm/hotels/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->a()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/x;->g:Lcom/google/android/apps/gmm/hotels/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/search/aj;->m:Lcom/google/e/a/a/a/b;

    iget v0, p0, Lcom/google/android/apps/gmm/car/i/x;->h:I

    iput v0, v3, Lcom/google/android/apps/gmm/search/aj;->b:I

    new-instance v0, Lcom/google/android/apps/gmm/search/al;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/search/aj;->a()Lcom/google/android/apps/gmm/search/ai;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/base/placelists/a/e;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/placelists/a/e;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/search/al;-><init>(Lcom/google/android/apps/gmm/search/ai;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/x;->i:Lcom/google/android/apps/gmm/search/am;

    iput-object v1, v0, Lcom/google/android/apps/gmm/search/al;->d:Lcom/google/android/apps/gmm/search/am;

    goto/16 :goto_0

    :cond_a
    if-eqz p2, :cond_c

    sget-object v0, Lcom/google/android/apps/gmm/car/i/x;->a:Ljava/lang/String;

    iget-object v0, p2, Lcom/google/maps/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    iget-wide v4, v0, Lcom/google/maps/a/e;->c:D

    iget-object v0, p2, Lcom/google/maps/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    iget-wide v0, v0, Lcom/google/maps/a/e;->b:D

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v6, 0x64

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "No GmmLocation, using latLng from passed in camera "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/r/b/c;-><init>()V

    iget-object v0, p2, Lcom/google/maps/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    iget-wide v4, v0, Lcom/google/maps/a/e;->c:D

    iget-object v0, p2, Lcom/google/maps/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    iget-wide v6, v0, Lcom/google/maps/a/e;->b:D

    invoke-virtual {v1, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/r/b/c;->a(DD)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v1, :cond_b

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v0

    sget-object v1, Lcom/google/o/b/a/a/a;->f:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/search/aj;->g:Lcom/google/e/a/a/a/b;

    goto/16 :goto_3

    :cond_c
    sget-object v0, Lcom/google/android/apps/gmm/car/i/x;->a:Ljava/lang/String;

    goto/16 :goto_3

    .line 95
    :cond_d
    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/x;->b:Lcom/google/android/apps/gmm/search/al;

    .line 96
    iput-object p4, p0, Lcom/google/android/apps/gmm/car/i/x;->c:Lcom/google/android/apps/gmm/car/i/k;

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/x;->d:Lcom/google/android/apps/gmm/search/ah;

    iget-object v1, v1, Lcom/google/android/apps/gmm/search/ah;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    goto/16 :goto_1
.end method
