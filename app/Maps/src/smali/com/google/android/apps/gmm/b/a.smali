.class public Lcom/google/android/apps/gmm/b/a;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/b/a/a;
.implements Lcom/google/android/apps/gmm/b/f;


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field public a:Lcom/google/android/apps/gmm/base/g/c;

.field b:Lcom/google/android/apps/gmm/map/b/a/t;

.field c:Lcom/google/android/apps/gmm/b/a/b;

.field private g:Lcom/google/android/a/t;

.field private h:Ljava/lang/Long;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 55
    const-string v1, "GMM-"

    sget-object v0, Lcom/google/android/apps/gmm/d/a;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    sput-object v0, Lcom/google/android/apps/gmm/b/a;->f:Ljava/lang/String;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/b/a;->i:Z

    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 335
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 338
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    .line 339
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/b/f/cq;

    sget-object v4, Lcom/google/b/f/t;->fY:Lcom/google/b/f/t;

    aput-object v4, v3, v5

    .line 340
    iput-object v3, v2, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 341
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    .line 339
    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 345
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    .line 344
    invoke-static {v0, p1, v6, v6, v5}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hy;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    move-result-object v2

    .line 351
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->h()Lcom/google/r/b/a/up;

    move-result-object v0

    .line 353
    iget-boolean v0, v0, Lcom/google/r/b/a/up;->f:Z

    if-eqz v0, :cond_0

    .line 354
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 358
    :cond_0
    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v0

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 359
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/b/d;Lcom/google/maps/a/a;)V
    .locals 10
    .param p1    # Lcom/google/android/apps/gmm/map/r/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/maps/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 210
    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/j/c;->e:Z

    if-eqz v2, :cond_0

    .line 219
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 220
    if-eqz v3, :cond_0

    .line 223
    iget-object v4, v3, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    .line 224
    if-eqz v4, :cond_0

    .line 231
    iget-object v2, v4, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    if-eqz v5, :cond_5

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->q()Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v1

    :goto_1
    if-eqz v2, :cond_2

    iget-object v2, v4, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->k:Lcom/google/android/apps/gmm/map/b/a/t;

    iget-object v4, p0, Lcom/google/android/apps/gmm/b/a;->b:Lcom/google/android/apps/gmm/map/b/a/t;

    if-eq v2, v4, :cond_2

    move v0, v1

    :cond_2
    if-nez v0, :cond_0

    .line 237
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->h()Lcom/google/r/b/a/up;

    move-result-object v0

    .line 238
    invoke-static {p3}, Lcom/google/android/apps/gmm/map/b/a/p;->a(Lcom/google/maps/a/a;)D

    move-result-wide v4

    iget v2, v0, Lcom/google/r/b/a/up;->d:I

    int-to-double v6, v2

    cmpg-double v2, v4, v6

    if-ltz v2, :cond_0

    .line 243
    iget v2, v0, Lcom/google/r/b/a/up;->e:I

    .line 244
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/b/a;->h:Ljava/lang/Long;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/b/a;->h:Ljava/lang/Long;

    .line 246
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v6, v4, v6

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v8, v2

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-ltz v0, :cond_0

    .line 251
    :cond_3
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/b/a;->h:Ljava/lang/Long;

    .line 253
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v2

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->x:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v0, :cond_4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v0

    :cond_4
    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/be;

    .line 254
    new-instance v2, Lcom/google/android/apps/gmm/b/e;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/b/e;-><init>()V

    .line 255
    iget-object v4, v2, Lcom/google/android/apps/gmm/b/e;->a:Lcom/google/android/apps/gmm/b/c;

    iput-object p2, v4, Lcom/google/android/apps/gmm/b/c;->a:Lcom/google/android/apps/gmm/b/d;

    sget-object v4, Lcom/google/maps/a/a/a;->a:Lcom/google/e/a/a/a/d;

    .line 256
    invoke-static {p3, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v4

    iget-object v5, v2, Lcom/google/android/apps/gmm/b/e;->a:Lcom/google/android/apps/gmm/b/c;

    iput-object v4, v5, Lcom/google/android/apps/gmm/b/c;->d:Lcom/google/e/a/a/a/b;

    .line 258
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/be;->a()Lcom/google/android/apps/gmm/map/internal/d/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 257
    iget-object v4, v2, Lcom/google/android/apps/gmm/b/e;->a:Lcom/google/android/apps/gmm/b/c;

    iput-object v0, v4, Lcom/google/android/apps/gmm/b/c;->c:Lcom/google/e/a/a/a/b;

    .line 259
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/b/a;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v4, v2, Lcom/google/android/apps/gmm/b/e;->a:Lcom/google/android/apps/gmm/b/c;

    iput-object v0, v4, Lcom/google/android/apps/gmm/b/c;->b:Ljava/lang/String;

    .line 260
    iget-object v0, v2, Lcom/google/android/apps/gmm/b/e;->a:Lcom/google/android/apps/gmm/b/c;

    iput-object p0, v0, Lcom/google/android/apps/gmm/b/c;->f:Lcom/google/android/apps/gmm/b/f;

    .line 261
    iget-object v0, v2, Lcom/google/android/apps/gmm/b/e;->a:Lcom/google/android/apps/gmm/b/c;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/b/c;->e:Z

    .line 262
    iget-object v1, v2, Lcom/google/android/apps/gmm/b/e;->a:Lcom/google/android/apps/gmm/b/c;

    .line 263
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    goto/16 :goto_0

    :cond_5
    move v2, v0

    .line 231
    goto/16 :goto_1
.end method


# virtual methods
.method public final Y_()V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 86
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/b/a/b;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/gmm/b/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/android/apps/gmm/b/a;->c:Lcom/google/android/apps/gmm/b/a/b;

    .line 97
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/b/g;)V
    .locals 6

    .prologue
    const/16 v4, 0x1a

    const/4 v5, 0x1

    .line 133
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 134
    if-nez v1, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    .line 138
    if-eqz v0, :cond_0

    .line 142
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/t;

    iget-object v0, p1, Lcom/google/android/apps/gmm/b/g;->b:Lcom/google/e/a/a/a/b;

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/gmm/b/g;->d:Lcom/google/e/a/a/a/b;

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    iput-object v0, p1, Lcom/google/android/apps/gmm/b/g;->b:Lcom/google/e/a/a/a/b;

    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/b/g;->b:Lcom/google/e/a/a/a/b;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/t;-><init>(Lcom/google/e/a/a/a/b;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/b/a;->b:Lcom/google/android/apps/gmm/map/b/a/t;

    iget-object v0, p0, Lcom/google/android/apps/gmm/b/a;->c:Lcom/google/android/apps/gmm/b/a/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/b/a;->c:Lcom/google/android/apps/gmm/b/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/b/a/b;->a()V

    .line 145
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/b/g;->a:Lcom/google/e/a/a/a/b;

    if-nez v0, :cond_4

    iget-object v0, p1, Lcom/google/android/apps/gmm/b/g;->d:Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v5, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    iput-object v0, p1, Lcom/google/android/apps/gmm/b/g;->a:Lcom/google/e/a/a/a/b;

    :cond_4
    iget-object v0, p1, Lcom/google/android/apps/gmm/b/g;->a:Lcom/google/e/a/a/a/b;

    invoke-static {v0, v5}, Lcom/google/android/apps/gmm/base/g/a;->b(Lcom/google/e/a/a/a/b;Z)Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 148
    new-instance v2, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 151
    iput-boolean v5, v2, Lcom/google/android/apps/gmm/base/g/g;->d:Z

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/g/g;->c:Lcom/google/android/apps/gmm/base/g/h;

    iput-boolean v5, v3, Lcom/google/android/apps/gmm/base/g/h;->c:Z

    .line 152
    iget-object v3, v2, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iget-boolean v4, v2, Lcom/google/android/apps/gmm/base/g/g;->d:Z

    invoke-virtual {v3, v0, v4}, Lcom/google/android/apps/gmm/base/g/i;->a(Lcom/google/e/a/a/a/b;Z)Lcom/google/android/apps/gmm/base/g/i;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/g/g;->c:Lcom/google/android/apps/gmm/base/g/h;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/g/h;->a:Lcom/google/e/a/a/a/b;

    iput-boolean v5, v2, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    .line 153
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/b/a;->a:Lcom/google/android/apps/gmm/base/g/c;

    .line 157
    iget-object v0, p1, Lcom/google/android/apps/gmm/b/g;->c:Ljava/lang/Long;

    if-nez v0, :cond_5

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v0, p1, Lcom/google/android/apps/gmm/b/g;->d:Lcom/google/e/a/a/a/b;

    const/4 v3, 0x4

    const/16 v4, 0x15

    invoke-virtual {v0, v3, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v0, v4

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/apps/gmm/b/g;->c:Ljava/lang/Long;

    :cond_5
    iget-object v0, p1, Lcom/google/android/apps/gmm/b/g;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 158
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 160
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/b/b;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/b/b;-><init>(Lcom/google/android/apps/gmm/b/a;)V

    sget-object v4, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v4, v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/b/a;->i:Z

    .line 74
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/map/b/a/t;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/gmm/base/g/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/apps/gmm/b/a;->a:Lcom/google/android/apps/gmm/base/g/c;

    .line 103
    iput-object p2, p0, Lcom/google/android/apps/gmm/b/a;->b:Lcom/google/android/apps/gmm/map/b/a/t;

    .line 104
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/g/a;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 273
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/g/a;->a:Lcom/google/android/apps/gmm/map/internal/c/c;

    .line 274
    if-nez v3, :cond_1

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    iget v0, v3, Lcom/google/android/apps/gmm/map/internal/c/c;->a:I

    const/4 v4, -0x1

    if-eq v0, v4, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/gmm/b/a;->a:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/gmm/b/a;->a:Lcom/google/android/apps/gmm/base/g/c;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/b/a;->a(Lcom/google/android/apps/gmm/base/g/c;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 277
    goto :goto_1

    .line 283
    :cond_3
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/internal/c/c;->b:Lcom/google/android/apps/gmm/map/internal/c/y;

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    .line 285
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/internal/c/c;->b:Lcom/google/android/apps/gmm/map/internal/c/y;

    invoke-static {}, Lcom/google/r/b/a/aba;->newBuilder()Lcom/google/r/b/a/abc;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/y;->a:Ljava/lang/String;

    if-nez v4, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v0, v2

    .line 283
    goto :goto_2

    .line 285
    :cond_5
    iget v5, v3, Lcom/google/r/b/a/abc;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v3, Lcom/google/r/b/a/abc;->a:I

    iput-object v4, v3, Lcom/google/r/b/a/abc;->b:Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/y;->b:Ljava/lang/String;

    if-nez v4, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    iget v5, v3, Lcom/google/r/b/a/abc;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, v3, Lcom/google/r/b/a/abc;->a:I

    iput-object v4, v3, Lcom/google/r/b/a/abc;->c:Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/y;->c:Ljava/lang/String;

    if-nez v4, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget v5, v3, Lcom/google/r/b/a/abc;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, v3, Lcom/google/r/b/a/abc;->a:I

    iput-object v4, v3, Lcom/google/r/b/a/abc;->d:Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/y;->d:Ljava/lang/String;

    if-nez v4, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget v5, v3, Lcom/google/r/b/a/abc;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, v3, Lcom/google/r/b/a/abc;->a:I

    iput-object v4, v3, Lcom/google/r/b/a/abc;->e:Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/y;->e:Ljava/lang/String;

    if-nez v4, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    iget v5, v3, Lcom/google/r/b/a/abc;->a:I

    or-int/lit16 v5, v5, 0x200

    iput v5, v3, Lcom/google/r/b/a/abc;->a:I

    iput-object v4, v3, Lcom/google/r/b/a/abc;->g:Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/y;->e:Ljava/lang/String;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    iget v4, v3, Lcom/google/r/b/a/abc;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, v3, Lcom/google/r/b/a/abc;->a:I

    iput-object v0, v3, Lcom/google/r/b/a/abc;->f:Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/google/r/b/a/abc;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aba;

    invoke-static {}, Lcom/google/r/b/a/abe;->newBuilder()Lcom/google/r/b/a/abg;

    move-result-object v3

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    invoke-virtual {v3}, Lcom/google/r/b/a/abg;->c()V

    iget-object v4, v3, Lcom/google/r/b/a/abg;->a:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    invoke-direct {v5}, Lcom/google/n/ao;-><init>()V

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v5, Lcom/google/n/ao;->d:Z

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Lcom/google/r/b/a/abg;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/abe;

    invoke-static {}, Lcom/google/r/b/a/aag;->newBuilder()Lcom/google/r/b/a/aai;

    move-result-object v3

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    iget-object v4, v3, Lcom/google/r/b/a/aai;->b:Lcom/google/n/ao;

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v0, v3, Lcom/google/r/b/a/aai;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v3, Lcom/google/r/b/a/aai;->a:I

    invoke-virtual {v3}, Lcom/google/r/b/a/aai;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aag;

    .line 287
    new-instance v3, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 289
    iput-boolean v1, v3, Lcom/google/android/apps/gmm/base/g/g;->d:Z

    iget-object v4, v3, Lcom/google/android/apps/gmm/base/g/g;->c:Lcom/google/android/apps/gmm/base/g/h;

    iput-boolean v1, v4, Lcom/google/android/apps/gmm/base/g/h;->c:Z

    .line 290
    iget-object v0, v0, Lcom/google/r/b/a/aag;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/abe;->d()Lcom/google/r/b/a/abe;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/abe;

    iget-object v0, v0, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aba;->d()Lcom/google/r/b/a/aba;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aba;

    sget-object v1, Lcom/google/r/b/a/b/at;->f:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    iget-object v1, v3, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iget-boolean v2, v3, Lcom/google/android/apps/gmm/base/g/g;->d:Z

    invoke-virtual {v1, v0, p1, v2}, Lcom/google/android/apps/gmm/base/g/i;->a(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/map/g/a;Z)Lcom/google/android/apps/gmm/base/g/i;

    iget-object v1, v3, Lcom/google/android/apps/gmm/base/g/g;->c:Lcom/google/android/apps/gmm/base/g/h;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/g/h;->a:Lcom/google/e/a/a/a/b;

    if-eqz p1, :cond_d

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/g/a;->a:Lcom/google/android/apps/gmm/map/internal/c/c;

    if-nez v0, :cond_d

    .line 291
    :goto_3
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    .line 293
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/b/a;->a(Lcom/google/android/apps/gmm/base/g/c;)V

    goto/16 :goto_0

    .line 290
    :cond_d
    iput-object p1, v1, Lcom/google/android/apps/gmm/base/g/h;->b:Lcom/google/android/apps/gmm/map/g/a;

    goto :goto_3
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/b/a;->i:Z

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->h()Lcom/google/r/b/a/up;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/r/b/a/up;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/b/d;->a:Lcom/google/android/apps/gmm/b/d;

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/apps/gmm/b/a;->a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/b/d;Lcom/google/maps/a/a;)V

    .line 112
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/b/a;->i:Z

    .line 114
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/maps/a/a;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/map/r/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/maps/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 198
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->h()Lcom/google/r/b/a/up;

    move-result-object v0

    .line 200
    iget-boolean v0, v0, Lcom/google/r/b/a/up;->c:Z

    if-nez v0, :cond_0

    .line 204
    :goto_0
    return-void

    .line 203
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/b/d;->b:Lcom/google/android/apps/gmm/b/d;

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/gmm/b/a;->a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/b/d;Lcom/google/maps/a/a;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->b()V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 80
    return-void
.end method

.method public final c()Lcom/google/android/apps/gmm/map/b/a/t;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/b/a;->b:Lcom/google/android/apps/gmm/map/b/a/t;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 122
    iget-object v1, p0, Lcom/google/android/apps/gmm/b/a;->g:Lcom/google/android/a/t;

    if-nez v1, :cond_0

    .line 123
    new-instance v1, Lcom/google/android/a/t;

    sget-object v2, Lcom/google/android/apps/gmm/b/a;->f:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/a/q;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/a/q;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/a/t;-><init>(Lcom/google/android/a/o;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/b/a;->g:Lcom/google/android/a/t;

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/b/a;->g:Lcom/google/android/a/t;

    iget-object v1, v1, Lcom/google/android/a/t;->a:Lcom/google/android/a/o;

    invoke-interface {v1, v0}, Lcom/google/android/a/o;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/b/a;->h:Ljava/lang/Long;

    .line 181
    return-void
.end method
