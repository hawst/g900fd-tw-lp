.class public Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;
.super Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;
.source "PG"


# instance fields
.field a:Z

.field private final j:Lcom/google/android/apps/gmm/streetview/b;

.field private final k:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/c/a;ZLjava/lang/String;Lcom/google/android/apps/gmm/map/b/a/u;Lcom/google/android/apps/gmm/streetview/b/a;)V
    .locals 1
    .param p5    # Lcom/google/android/apps/gmm/map/b/a/u;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/android/apps/gmm/streetview/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 186
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/c/a;ZLjava/lang/String;Lcom/google/android/apps/gmm/map/b/a/u;Lcom/google/android/apps/gmm/streetview/b/a;)V

    .line 155
    new-instance v0, Lcom/google/android/apps/gmm/streetview/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/streetview/b;-><init>(Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/streetview/b;

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->a:Z

    .line 160
    new-instance v0, Lcom/google/android/apps/gmm/streetview/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/streetview/a;-><init>(Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->k:Ljava/lang/Object;

    .line 188
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->a:Z

    .line 189
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;FF)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/streetview/internal/b;->c()Lcom/google/android/apps/gmm/streetview/b/a;

    move-result-object v0

    iput p1, v0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    iput p2, v0, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/streetview/internal/b;->a(Lcom/google/android/apps/gmm/streetview/b/a;)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 220
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->a:Z

    if-nez v0, :cond_1

    move v1, v2

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->a:Z

    if-eq v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->b:Lcom/google/android/apps/gmm/map/c/a;

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->k_()Lcom/google/android/apps/gmm/p/b/g;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->d:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v4, v4, Lcom/google/android/apps/gmm/streetview/internal/aq;->b:Lcom/google/android/apps/gmm/v/n;

    if-eqz v4, :cond_3

    :goto_2
    if-eqz v2, :cond_6

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/streetview/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/streetview/b;->a()V

    :goto_3
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->a:Z

    .line 221
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->a:Z

    return v0

    :cond_1
    move v1, v3

    .line 220
    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/streetview/b;

    iget-object v0, v2, Lcom/google/android/apps/gmm/streetview/b;->a:Landroid/hardware/SensorManager;

    if-nez v0, :cond_5

    iget-object v0, v2, Lcom/google/android/apps/gmm/streetview/b;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "sensor"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, v2, Lcom/google/android/apps/gmm/streetview/b;->a:Landroid/hardware/SensorManager;

    :cond_5
    iget-object v0, v2, Lcom/google/android/apps/gmm/streetview/b;->a:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    goto :goto_3

    :cond_6
    if-eqz v1, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->k:Ljava/lang/Object;

    sget-object v3, Lcom/google/android/apps/gmm/p/b/h;->b:Lcom/google/android/apps/gmm/p/b/h;

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/gmm/p/b/g;->a(Ljava/lang/Object;Lcom/google/android/apps/gmm/p/b/h;)V

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->k:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/p/b/g;->a(Ljava/lang/Object;)V

    goto :goto_3
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 255
    invoke-super {p0}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->onPause()V

    .line 256
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->a:Z

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->d:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/aq;->b:Lcom/google/android/apps/gmm/v/n;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    .line 258
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/streetview/b;

    iget-object v0, v1, Lcom/google/android/apps/gmm/streetview/b;->a:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    iget-object v0, v1, Lcom/google/android/apps/gmm/streetview/b;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "sensor"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, v1, Lcom/google/android/apps/gmm/streetview/b;->a:Landroid/hardware/SensorManager;

    :cond_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/streetview/b;->a:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 267
    :cond_1
    :goto_1
    return-void

    .line 257
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 261
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->b:Lcom/google/android/apps/gmm/map/c/a;

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->k_()Lcom/google/android/apps/gmm/p/b/g;

    move-result-object v0

    .line 262
    :goto_2
    if-eqz v0, :cond_1

    .line 263
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->k:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/p/b/g;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 261
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 238
    invoke-super {p0}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->onResume()V

    .line 239
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->a:Z

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->d:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/aq;->b:Lcom/google/android/apps/gmm/v/n;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/streetview/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/streetview/b;->a()V

    .line 251
    :cond_0
    :goto_1
    return-void

    .line 240
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 244
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->b:Lcom/google/android/apps/gmm/map/c/a;

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->k_()Lcom/google/android/apps/gmm/p/b/g;

    move-result-object v0

    .line 245
    :goto_2
    if-eqz v0, :cond_0

    .line 246
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->k:Ljava/lang/Object;

    sget-object v2, Lcom/google/android/apps/gmm/p/b/h;->b:Lcom/google/android/apps/gmm/p/b/h;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/p/b/g;->a(Ljava/lang/Object;Lcom/google/android/apps/gmm/p/b/h;)V

    goto :goto_1

    .line 244
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method
