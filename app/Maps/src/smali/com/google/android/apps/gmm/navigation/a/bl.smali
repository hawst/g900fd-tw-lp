.class Lcom/google/android/apps/gmm/navigation/a/bl;
.super Lcom/google/android/apps/gmm/navigation/a/a;
.source "PG"


# instance fields
.field b:Lcom/google/android/apps/gmm/navigation/a/a;

.field private c:Ljava/io/File;

.field private d:I

.field private final e:Lcom/google/android/apps/gmm/shared/c/a/j;

.field private final f:Landroid/content/Context;

.field private final g:Lcom/google/android/apps/gmm/shared/b/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/io/File;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/shared/b/a;)V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/a/a;-><init>()V

    .line 127
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->f:Landroid/content/Context;

    .line 128
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->c:Ljava/io/File;

    .line 129
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->e:Lcom/google/android/apps/gmm/shared/c/a/j;

    .line 130
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->g:Lcom/google/android/apps/gmm/shared/b/a;

    .line 131
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->a:Z

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->c:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->g:Lcom/google/android/apps/gmm/shared/b/a;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/a/c;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/android/apps/gmm/navigation/a/c;

    move-result-object v0

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->f:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->c:Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->e:Lcom/google/android/apps/gmm/shared/c/a/j;

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/navigation/a/al;->a(Landroid/content/Context;Ljava/io/File;Lcom/google/android/apps/gmm/navigation/a/c;Lcom/google/android/apps/gmm/shared/c/a/j;)Lcom/google/android/apps/gmm/navigation/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->b:Lcom/google/android/apps/gmm/navigation/a/a;

    .line 139
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/a/b;)V
    .locals 2

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->b:Lcom/google/android/apps/gmm/navigation/a/a;

    if-nez v0, :cond_1

    .line 148
    invoke-interface {p1, p0}, Lcom/google/android/apps/gmm/navigation/a/b;->a(Lcom/google/android/apps/gmm/navigation/a/a;)V

    .line 159
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->b:Lcom/google/android/apps/gmm/navigation/a/a;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/bm;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/navigation/a/bm;-><init>(Lcom/google/android/apps/gmm/navigation/a/bl;Lcom/google/android/apps/gmm/navigation/a/b;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/a/a;->a(Lcom/google/android/apps/gmm/navigation/a/b;)V

    .line 157
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->d:I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/a/c;)V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->b:Lcom/google/android/apps/gmm/navigation/a/a;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->b:Lcom/google/android/apps/gmm/navigation/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/navigation/a/a;->a(Lcom/google/android/apps/gmm/navigation/a/c;)V

    .line 190
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->b:Lcom/google/android/apps/gmm/navigation/a/a;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->b:Lcom/google/android/apps/gmm/navigation/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/a;->b()V

    .line 168
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->b:Lcom/google/android/apps/gmm/navigation/a/a;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->b:Lcom/google/android/apps/gmm/navigation/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/a;->c()V

    .line 175
    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 208
    if-ne p0, p1, :cond_1

    .line 217
    :cond_0
    :goto_0
    return v0

    .line 212
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/navigation/a/bl;

    if-nez v2, :cond_2

    move v0, v1

    .line 213
    goto :goto_0

    .line 216
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/navigation/a/bl;

    .line 217
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->c:Ljava/io/File;

    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/a/bl;->c:Ljava/io/File;

    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_3
    move v2, v0

    :goto_1
    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->b:Lcom/google/android/apps/gmm/navigation/a/a;

    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/a/bl;->b:Lcom/google/android/apps/gmm/navigation/a/a;

    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_4
    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->d:I

    iget v3, p1, Lcom/google/android/apps/gmm/navigation/a/bl;->d:I

    if-eq v2, v3, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v2, v1

    goto :goto_1

    :cond_7
    move v2, v1

    goto :goto_2
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 223
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->c:Ljava/io/File;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->b:Lcom/google/android/apps/gmm/navigation/a/a;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/a/bl;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
