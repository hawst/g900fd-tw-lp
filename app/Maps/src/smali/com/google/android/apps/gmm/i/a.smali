.class public final Lcom/google/android/apps/gmm/i/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field private static final d:Lcom/google/android/gms/common/api/q;

.field private static final e:Lcom/google/android/gms/common/api/r;


# instance fields
.field public final b:Lcom/google/android/gms/common/api/p;

.field public c:Lcom/google/android/gms/common/api/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/android/apps/gmm/i/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/i/a;->a:Ljava/lang/String;

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/i/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/i/b;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/i/a;->d:Lcom/google/android/gms/common/api/q;

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/i/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/i/c;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/i/a;->e:Lcom/google/android/gms/common/api/r;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Lcom/google/android/gms/common/api/p;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/p;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/i/a;->b:Lcom/google/android/gms/common/api/p;

    .line 72
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/apps/gmm/i/a;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/util/c/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    const/4 v0, 0x0

    .line 67
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/i/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/i/a;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static c()Lcom/google/android/gms/common/api/q;
    .locals 1

    .prologue
    .line 161
    sget-object v0, Lcom/google/android/apps/gmm/i/a;->d:Lcom/google/android/gms/common/api/q;

    return-object v0
.end method

.method public static d()Lcom/google/android/gms/common/api/r;
    .locals 1

    .prologue
    .line 165
    sget-object v0, Lcom/google/android/apps/gmm/i/a;->e:Lcom/google/android/gms/common/api/r;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/apps/gmm/i/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/a",
            "<+",
            "Lcom/google/android/gms/common/api/d;",
            ">;)",
            "Lcom/google/android/apps/gmm/i/a;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    if-eqz v0, :cond_0

    .line 80
    const-string v0, "addApi() was called after getGoogleApiClient() on GoogleApiClientHelper"

    .line 82
    sget-object v1, Lcom/google/android/apps/gmm/i/a;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 86
    :goto_0
    return-object p0

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/i/a;->b:Lcom/google/android/gms/common/api/p;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/gms/common/api/p;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/q;)Lcom/google/android/apps/gmm/i/a;
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    if-eqz v0, :cond_0

    .line 95
    const-string v0, "addConnectionCallbacks() was called after getGoogleApiClient() on GoogleApiClientHelper"

    .line 97
    sget-object v1, Lcom/google/android/apps/gmm/i/a;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 101
    :goto_0
    return-object p0

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/i/a;->b:Lcom/google/android/gms/common/api/p;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/q;)Lcom/google/android/gms/common/api/p;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/apps/gmm/i/a;
    .locals 3

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    if-eqz v0, :cond_0

    .line 111
    const-string v0, "addOnConnectionFailedListener() was called after getGoogleApiClient() on GoogleApiClientHelper"

    .line 113
    sget-object v1, Lcom/google/android/apps/gmm/i/a;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 117
    :goto_0
    return-object p0

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/i/a;->b:Lcom/google/android/gms/common/api/p;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/p;

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/i/a;->b:Lcom/google/android/gms/common/api/p;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/p;->a()Lcom/google/android/gms/common/api/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    .line 139
    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->e()Z

    move-result v1

    if-nez v1, :cond_1

    .line 140
    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->b()V

    .line 144
    :goto_0
    return-void

    .line 142
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/i/a;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    if-nez v0, :cond_0

    .line 151
    const-string v0, "stop() was called before start() on GoogleApiClientHelper"

    .line 152
    sget-object v1, Lcom/google/android/apps/gmm/i/a;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->c()V

    .line 156
    sget-object v0, Lcom/google/android/apps/gmm/i/a;->a:Ljava/lang/String;

    .line 158
    :cond_2
    return-void
.end method
