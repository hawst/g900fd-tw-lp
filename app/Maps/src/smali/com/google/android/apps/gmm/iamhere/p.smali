.class public Lcom/google/android/apps/gmm/iamhere/p;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/iamhere/a/b;
.implements Lcom/google/android/apps/gmm/iamhere/i;
.implements Lcom/google/android/apps/gmm/map/internal/d/c/b/f;
.implements Lcom/google/android/apps/gmm/startpage/a/a;
.implements Lcom/google/android/gms/common/c;
.implements Lcom/google/android/gms/common/d;


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/apps/gmm/iamhere/c/o;

.field final b:Lcom/google/android/apps/gmm/iamhere/a/a;

.field private f:Lcom/google/android/apps/gmm/iamhere/j;

.field private g:Lcom/google/android/apps/gmm/iamhere/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private h:Lcom/google/android/apps/gmm/map/r/b/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private i:Lcom/google/android/apps/gmm/map/s/a;

.field private j:Lcom/google/android/apps/gmm/base/e/c;

.field private k:Lcom/google/android/apps/gmm/iamhere/a;

.field private l:Lcom/google/android/apps/gmm/iamhere/e;

.field private m:Lcom/google/j/d/a/e;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private n:Lcom/google/android/gms/location/reporting/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private o:Z

.field private p:F

.field private q:Lcom/google/o/h/a/gx;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    const-class v0, Lcom/google/android/apps/gmm/iamhere/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/p;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 133
    sget-object v0, Lcom/google/android/apps/gmm/base/e/c;->d:Lcom/google/android/apps/gmm/base/e/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->j:Lcom/google/android/apps/gmm/base/e/c;

    .line 136
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 167
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/q;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/q;-><init>(Lcom/google/android/apps/gmm/iamhere/p;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->b:Lcom/google/android/apps/gmm/iamhere/a/a;

    return-void
.end method

.method private a(Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/b/l;
    .locals 3

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/c/o;->b()Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 262
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 263
    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 264
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0

    .line 263
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/iamhere/p;Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/j/d/a/e;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 97
    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/p;->m:Lcom/google/j/d/a/e;

    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/iamhere/p;->c(Lcom/google/android/apps/gmm/iamhere/c/o;)Lcom/google/android/apps/gmm/iamhere/c/o;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;

    if-nez v1, :cond_0

    instance-of v0, v0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;

    if-eqz v0, :cond_1

    :cond_0
    move v0, v3

    :goto_0
    if-eqz v0, :cond_3

    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/gmm/iamhere/p;->q:Lcom/google/o/h/a/gx;

    if-eqz v5, :cond_2

    if-eqz v1, :cond_2

    :goto_2
    invoke-static {v0, v4, v3}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/iamhere/c/o;Z)Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_2

    :cond_3
    move v1, v3

    goto :goto_1
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/iamhere/k;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/map/r/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 563
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 568
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 566
    :cond_1
    :try_start_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/p;->h:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->f:Lcom/google/android/apps/gmm/iamhere/j;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/iamhere/j;->a(Lcom/google/android/apps/gmm/p/d/f;Lcom/google/android/apps/gmm/iamhere/k;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 563
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(FLcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/android/apps/gmm/map/s/a;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 623
    monitor-enter p0

    const/high16 v2, 0x41000000    # 8.0f

    cmpl-float v2, p1, v2

    if-lez v2, :cond_2

    move v4, v0

    .line 624
    :goto_0
    :try_start_0
    iget-object v2, p2, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v3, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-eq v2, v3, :cond_0

    .line 625
    iget-object v2, p2, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v3, Lcom/google/android/apps/gmm/iamhere/c/q;->c:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-ne v2, v3, :cond_3

    .line 626
    iget-boolean v2, p2, Lcom/google/android/apps/gmm/iamhere/c/o;->l:Z

    if-eqz v2, :cond_3

    :cond_0
    move v3, v0

    .line 627
    :goto_1
    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    if-eq p3, v2, :cond_1

    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->d:Lcom/google/android/apps/gmm/map/s/a;

    if-eq p3, v2, :cond_1

    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->b:Lcom/google/android/apps/gmm/map/s/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p3, v2, :cond_4

    :cond_1
    move v2, v0

    .line 629
    :goto_2
    if-eqz v4, :cond_5

    if-eqz v3, :cond_5

    if-eqz v2, :cond_5

    :goto_3
    monitor-exit p0

    return v0

    :cond_2
    move v4, v1

    .line 623
    goto :goto_0

    :cond_3
    move v3, v1

    .line 626
    goto :goto_1

    :cond_4
    move v2, v1

    .line 627
    goto :goto_2

    :cond_5
    move v0, v1

    .line 629
    goto :goto_3

    .line 623
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/e;Lcom/google/j/d/a/w;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/base/e/c;Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/android/gms/location/reporting/c;)Z
    .locals 8
    .param p1    # Lcom/google/android/apps/gmm/base/g/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/j/d/a/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/b/f/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/android/gms/location/reporting/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 387
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->F()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    const/4 v0, 0x0

    .line 445
    :goto_0
    return v0

    .line 390
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    if-nez v0, :cond_2

    .line 391
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 393
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v2

    .line 394
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/p;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x30

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "starting log place report: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", type : "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", context : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    :try_start_0
    invoke-virtual {p6}, Lcom/google/android/apps/gmm/iamhere/c/o;->b()Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 398
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    if-eq v1, v2, :cond_3

    if-eqz v1, :cond_5

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_3
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_6

    .line 399
    :cond_4
    sget-object v1, Lcom/google/android/apps/gmm/iamhere/p;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x31

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "place report skipped: current place is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " state is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 398
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 404
    :cond_6
    iget-object v0, p5, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    .line 403
    invoke-static {p7, v0}, Lcom/google/android/apps/gmm/iamhere/e/a;->a(Lcom/google/android/gms/location/reporting/c;Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 405
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/p;->c:Ljava/lang/String;

    .line 406
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 408
    :cond_7
    invoke-static {p2, p3, p4, p6}, Lcom/google/android/apps/gmm/iamhere/e/a;->a(Lcom/google/j/d/a/e;Lcom/google/j/d/a/w;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/iamhere/c/o;)Lcom/google/j/d/a/b;

    move-result-object v0

    .line 411
    invoke-static {}, Lcom/google/j/d/a/aa;->newBuilder()Lcom/google/j/d/a/ac;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/j/d/a/ac;->a(Lcom/google/j/d/a/b;)Lcom/google/j/d/a/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/j/d/a/ac;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/j/d/a/aa;

    .line 413
    iget-object v3, p5, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    .line 414
    invoke-static {}, Lcom/google/o/e/b;->newBuilder()Lcom/google/o/e/d;

    move-result-object v4

    invoke-static {}, Lcom/google/d/a/a/ds;->newBuilder()Lcom/google/d/a/a/du;

    move-result-object v1

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    iget v5, v1, Lcom/google/d/a/a/du;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v1, Lcom/google/d/a/a/du;->a:I

    iput-wide v6, v1, Lcom/google/d/a/a/du;->b:J

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget v2, v1, Lcom/google/d/a/a/du;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/d/a/a/du;->a:I

    iput-wide v6, v1, Lcom/google/d/a/a/du;->c:J

    invoke-virtual {v1}, Lcom/google/d/a/a/du;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/d/a/a/ds;

    invoke-virtual {v4, v1}, Lcom/google/o/e/d;->a(Lcom/google/d/a/a/ds;)Lcom/google/o/e/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/o/e/d;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/o/e/b;

    invoke-static {v1}, Lcom/google/android/apps/gmm/iamhere/e/c;->a(Lcom/google/o/e/b;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "payload:"

    .line 415
    invoke-virtual {v0}, Lcom/google/j/d/a/aa;->l()[B

    move-result-object v0

    const/4 v4, 0x2

    invoke-static {v0, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 414
    :goto_2
    invoke-static {v1, v0}, Lcom/google/android/gms/location/places/PlaceReport;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceReport;

    move-result-object v0

    .line 412
    invoke-virtual {p7, v3, v0}, Lcom/google/android/gms/location/reporting/c;->a(Landroid/accounts/Account;Lcom/google/android/gms/location/places/PlaceReport;)I

    move-result v0

    .line 416
    if-eqz v0, :cond_9

    .line 417
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x37

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "ULR place report error: PlaceReportResult = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 419
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 415
    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 441
    :catch_0
    move-exception v0

    .line 442
    const-string v1, "ULR place report error"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 443
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 432
    :cond_9
    :try_start_1
    iget-object v0, p5, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    const-string v1, "GMM place report"

    const-wide/16 v2, 0x0

    .line 431
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/location/reporting/UploadRequest;->a(Landroid/accounts/Account;Ljava/lang/String;J)Lcom/google/android/gms/location/reporting/e;

    move-result-object v0

    const-wide/16 v2, 0x0

    .line 432
    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/reporting/e;->a(J)Lcom/google/android/gms/location/reporting/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/e;->a()Lcom/google/android/gms/location/reporting/UploadRequest;

    move-result-object v0

    .line 430
    invoke-virtual {p7, v0}, Lcom/google/android/gms/location/reporting/c;->a(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcom/google/android/gms/location/reporting/UploadRequestResult;

    move-result-object v0

    .line 433
    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/UploadRequestResult;->a()I

    move-result v1

    if-eqz v1, :cond_a

    .line 434
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "ULR upload request error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/RuntimeException;

    .line 435
    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/UploadRequestResult;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 434
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 436
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 445
    :cond_a
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/iamhere/p;)Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    return v0
.end method

.method private declared-synchronized c(Lcom/google/android/apps/gmm/iamhere/c/o;)Lcom/google/android/apps/gmm/iamhere/c/o;
    .locals 3

    .prologue
    .line 530
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->h:Lcom/google/android/apps/gmm/map/r/b/a;

    if-nez v0, :cond_1

    .line 532
    sget-object p1, Lcom/google/android/apps/gmm/iamhere/c/o;->b:Lcom/google/android/apps/gmm/iamhere/c/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 534
    :cond_0
    :goto_0
    monitor-exit p0

    return-object p1

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->f:Lcom/google/android/apps/gmm/iamhere/j;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/p;->h:Lcom/google/android/apps/gmm/map/r/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/k;->b:Lcom/google/android/apps/gmm/iamhere/k;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/j;->a(Lcom/google/android/apps/gmm/p/d/f;Lcom/google/android/apps/gmm/iamhere/k;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/google/android/apps/gmm/iamhere/c/o;->b:Lcom/google/android/apps/gmm/iamhere/c/o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 530
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private e()Lcom/google/b/f/t;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/i;->d()Lcom/google/android/apps/gmm/mylocation/b/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/n;->f:Lcom/google/android/apps/gmm/mylocation/d/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/d/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    sget-object v0, Lcom/google/b/f/t;->bx:Lcom/google/b/f/t;

    .line 257
    :goto_0
    return-object v0

    .line 232
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/s;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v3, v3, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/iamhere/c/q;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 253
    sget-object v0, Lcom/google/b/f/t;->br:Lcom/google/b/f/t;

    goto :goto_0

    .line 234
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->h:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 235
    if-eqz v0, :cond_3

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/c;->d:Ljava/lang/String;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    if-nez v1, :cond_4

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->d:Ljava/lang/String;

    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 236
    :cond_3
    sget-object v0, Lcom/google/b/f/t;->br:Lcom/google/b/f/t;

    goto :goto_0

    .line 235
    :cond_4
    const-string v0, ""

    goto :goto_1

    .line 238
    :cond_5
    sget-object v0, Lcom/google/b/f/t;->bt:Lcom/google/b/f/t;

    goto :goto_0

    .line 242
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v3, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v1, :cond_9

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    :goto_2
    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 243
    if-eqz v0, :cond_8

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/c;->d:Ljava/lang/String;

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_7

    :cond_6
    move v1, v2

    :cond_7
    if-nez v1, :cond_a

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->d:Ljava/lang/String;

    :goto_3
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 244
    :cond_8
    sget-object v0, Lcom/google/b/f/t;->br:Lcom/google/b/f/t;

    goto :goto_0

    .line 242
    :cond_9
    const/4 v0, 0x0

    goto :goto_2

    .line 243
    :cond_a
    const-string v0, ""

    goto :goto_3

    .line 246
    :cond_b
    sget-object v0, Lcom/google/b/f/t;->bs:Lcom/google/b/f/t;

    goto :goto_0

    .line 232
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private i()V
    .locals 5

    .prologue
    .line 584
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/p;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588
    new-instance v0, Lcom/google/android/apps/gmm/base/l/t;

    .line 589
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 590
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/iamhere/c/o;->b()Lcom/google/android/apps/gmm/x/o;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/iamhere/p;->q:Lcom/google/o/h/a/gx;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/base/l/t;-><init>(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/base/j/b;Lcom/google/android/apps/gmm/x/o;Lcom/google/o/h/a/gx;)V

    .line 591
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->j()Lcom/google/android/apps/gmm/c/a/a;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/c/a/b;->a:Lcom/google/android/apps/gmm/c/a/b;

    .line 592
    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/c/a/a;->a(Lcom/google/android/apps/gmm/c/a/b;Lcom/google/android/apps/gmm/c/a/d;)V

    .line 597
    :goto_0
    return-void

    .line 594
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->j()Lcom/google/android/apps/gmm/c/a/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/c/a/b;->a:Lcom/google/android/apps/gmm/c/a/b;

    .line 595
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/c/a/a;->a(Lcom/google/android/apps/gmm/c/a/b;)V

    goto :goto_0
.end method

.method private declared-synchronized j()Z
    .locals 3

    .prologue
    .line 616
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->p:F

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/p;->i:Lcom/google/android/apps/gmm/map/s/a;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/p;->a(FLcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/android/apps/gmm/map/s/a;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized k()Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 782
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 784
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 783
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v3

    .line 785
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/p;->h:Lcom/google/android/apps/gmm/map/r/b/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    if-nez v3, :cond_2

    :cond_0
    move v0, v1

    .line 788
    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/p;->h:Lcom/google/android/apps/gmm/map/r/b/a;

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v8

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iget-wide v6, v4, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v8, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    cmpg-double v2, v8, v6

    if-gtz v2, :cond_4

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v8, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    cmpg-double v2, v6, v8

    if-gtz v2, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/r;->a(D)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1

    .line 782
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final Y_()V
    .locals 4

    .prologue
    .line 319
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->b:Lcom/google/android/apps/gmm/iamhere/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/p;->f:Lcom/google/android/apps/gmm/iamhere/j;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/iamhere/j;->a(Lcom/google/android/apps/gmm/iamhere/a/a;)V

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/p;->l:Lcom/google/android/apps/gmm/iamhere/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 324
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/p;->l:Lcom/google/android/apps/gmm/iamhere/e;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/i;->d()Lcom/google/android/apps/gmm/mylocation/b/f;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/n;->f:Lcom/google/android/apps/gmm/mylocation/d/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/iamhere/e;->a(Lcom/google/android/apps/gmm/mylocation/d/c;)V

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/startpage/a/e;->a(Lcom/google/android/apps/gmm/startpage/a/a;)V

    .line 329
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/iamhere/c/o;
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/x/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)",
            "Lcom/google/android/apps/gmm/iamhere/c/o;"
        }
    .end annotation

    .prologue
    .line 679
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-nez v0, :cond_0

    .line 680
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 686
    :goto_0
    return-object v0

    .line 684
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/p;->f:Lcom/google/android/apps/gmm/iamhere/j;

    if-eqz p1, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    :goto_1
    invoke-interface {v1, v0, p1}, Lcom/google/android/apps/gmm/iamhere/j;->a(Lcom/google/android/apps/gmm/iamhere/c/q;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/iamhere/c/o;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->b:Lcom/google/android/apps/gmm/iamhere/c/q;

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 450
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/p;->c:Ljava/lang/String;

    .line 451
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 5

    .prologue
    .line 285
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 286
    new-instance v0, Lcom/google/android/gms/location/reporting/c;

    invoke-direct {v0, p1, p0, p0}, Lcom/google/android/gms/location/reporting/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->n:Lcom/google/android/gms/location/reporting/c;

    .line 287
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/util/c/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->n:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/c;->a()V

    .line 294
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    const-class v1, Lcom/google/r/b/a/alz;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/b;

    move-result-object v0

    .line 295
    new-instance v1, Lcom/google/android/apps/gmm/iamhere/l;

    invoke-direct {v1, p1, v0}, Lcom/google/android/apps/gmm/iamhere/l;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/shared/net/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/iamhere/p;->f:Lcom/google/android/apps/gmm/iamhere/j;

    .line 296
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/a;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/iamhere/a;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->k:Lcom/google/android/apps/gmm/iamhere/a;

    .line 297
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->g:Lcom/google/android/apps/gmm/iamhere/c;

    .line 301
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v1

    .line 302
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/gmm/e;->az:I

    .line 303
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 304
    new-instance v2, Lcom/google/android/apps/gmm/iamhere/e;

    new-instance v3, Lcom/google/android/apps/gmm/map/t/a/a;

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v0, v4

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/gmm/map/t/a/a;-><init>(FF)V

    invoke-direct {v2, v3, v1, p0}, Lcom/google/android/apps/gmm/iamhere/e;-><init>(Lcom/google/android/apps/gmm/v/c;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/iamhere/i;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/iamhere/p;->l:Lcom/google/android/apps/gmm/iamhere/e;

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/iamhere/b/a;->a(Landroid/content/Context;)V

    .line 314
    const/high16 v0, 0x41700000    # 15.0f

    iput v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->p:F

    .line 315
    return-void

    .line 290
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/p;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/base/e/c;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 269
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->j:Lcom/google/android/apps/gmm/base/e/c;

    sget-object v2, Lcom/google/android/apps/gmm/base/e/c;->d:Lcom/google/android/apps/gmm/base/e/c;

    if-ne v0, v2, :cond_2

    const-string v0, ""

    .line 271
    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/p;->j:Lcom/google/android/apps/gmm/base/e/c;

    .line 272
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-nez v2, :cond_4

    :goto_1
    if-eq v1, v0, :cond_0

    if-eqz v1, :cond_5

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_1

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->f:Lcom/google/android/apps/gmm/iamhere/j;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/iamhere/j;->a()V

    .line 275
    :cond_1
    return-void

    .line 269
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->j:Lcom/google/android/apps/gmm/base/e/c;

    .line 270
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-nez v2, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0

    .line 272
    :cond_4
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V
    .locals 8
    .param p3    # Lcom/google/b/f/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-nez v0, :cond_0

    .line 375
    :goto_0
    return-void

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/p;->m:Lcom/google/j/d/a/e;

    iget-object v5, p0, Lcom/google/android/apps/gmm/iamhere/p;->j:Lcom/google/android/apps/gmm/base/e/c;

    iget-object v6, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v7, p0, Lcom/google/android/apps/gmm/iamhere/p;->n:Lcom/google/android/gms/location/reporting/c;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/iamhere/p;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/e;Lcom/google/j/d/a/w;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/base/e/c;Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/android/gms/location/reporting/c;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/iamhere/a/a;)V
    .locals 1

    .prologue
    .line 720
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->f:Lcom/google/android/apps/gmm/iamhere/j;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/iamhere/j;->a(Lcom/google/android/apps/gmm/iamhere/a/a;)V

    .line 721
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/iamhere/c/o;)V
    .locals 2

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-nez v0, :cond_1

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 468
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    if-ne v0, p1, :cond_0

    .line 471
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    .line 472
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/p;->e()Lcom/google/b/f/t;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/iamhere/p;->a(Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/l;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/j/d/a/e;)V
    .locals 3

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-nez v0, :cond_0

    .line 503
    :goto_0
    return-void

    .line 482
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/r;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/gmm/iamhere/r;-><init>(Lcom/google/android/apps/gmm/iamhere/p;Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/j/d/a/e;)V

    .line 501
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 502
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->s()Lcom/google/android/apps/gmm/mylocation/b/b;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/mylocation/b/b;->a(ZLcom/google/android/apps/gmm/mylocation/b/c;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 835
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-nez v0, :cond_1

    .line 854
    :cond_0
    :goto_0
    return-void

    .line 838
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/j/c;->e:Z

    if-nez v0, :cond_2

    .line 839
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/p;->c:Ljava/lang/String;

    goto :goto_0

    .line 845
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/p;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x24

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "handleResource is called for state: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 848
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 851
    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/gmm/iamhere/p;->k:Lcom/google/android/apps/gmm/iamhere/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/apps/gmm/iamhere/a;->b:Ljava/util/List;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/b;->a:[I

    iget-object v3, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/iamhere/c/q;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/a;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 852
    :cond_4
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->l:Lcom/google/android/apps/gmm/iamhere/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/p;->k:Lcom/google/android/apps/gmm/iamhere/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/iamhere/a;->a()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/e;->a(Ljava/util/List;Lcom/google/android/apps/gmm/iamhere/c/o;)V

    goto :goto_0

    .line 851
    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/c/o;->b()Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_8

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->d:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_9

    :cond_5
    move v1, v5

    :goto_2
    if-nez v1, :cond_a

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->d:Ljava/lang/String;

    :goto_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    iget-object v3, v6, Lcom/google/android/apps/gmm/iamhere/a;->d:Lcom/google/android/apps/gmm/v/ar;

    iget-object v7, v6, Lcom/google/android/apps/gmm/iamhere/a;->e:Lcom/google/android/apps/gmm/v/ar;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_b

    :cond_6
    move v0, v5

    :goto_4
    if-nez v0, :cond_d

    iget-object v0, v6, Lcom/google/android/apps/gmm/iamhere/a;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v0

    const-string v8, "BlueDotAssetManager#createTextureStatePair"

    invoke-interface {v0, v1, v8, v2}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v1

    iget v0, v1, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    const/4 v8, 0x3

    if-eq v0, v8, :cond_c

    move-object v0, v2

    :cond_7
    :goto_5
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v1

    if-eqz v1, :cond_d

    if-eqz v0, :cond_d

    new-instance v1, Lcom/google/android/apps/gmm/v/ar;

    iget-object v2, v6, Lcom/google/android/apps/gmm/iamhere/a;->c:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    invoke-direct {v1, v0, v2, v4}, Lcom/google/android/apps/gmm/v/ar;-><init>(Landroid/graphics/Bitmap;Lcom/google/android/apps/gmm/v/ao;Z)V

    move-object v0, v1

    :goto_6
    new-instance v1, Lcom/google/android/apps/gmm/v/ci;

    invoke-direct {v1, v0, v9, v5}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;IZ)V

    new-instance v0, Lcom/google/android/apps/gmm/v/ci;

    invoke-direct {v0, v7, v9, v5}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;IZ)V

    invoke-static {v1, v0}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v2

    :cond_8
    if-eqz v2, :cond_4

    invoke-static {v2}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/apps/gmm/iamhere/a;->b:Ljava/util/List;

    goto/16 :goto_1

    :cond_9
    move v1, v4

    goto :goto_2

    :cond_a
    const-string v1, ""

    goto :goto_3

    :cond_b
    move v0, v4

    goto :goto_4

    :cond_c
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_7

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d()V

    goto :goto_5

    :cond_d
    move-object v0, v3

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/ad;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 192
    iget v0, p1, Lcom/google/android/apps/gmm/map/j/ad;->a:F

    iput v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->p:F

    .line 193
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/p;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->j()Lcom/google/android/apps/gmm/c/a/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/c/a/b;->a:Lcom/google/android/apps/gmm/c/a/b;

    .line 195
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/c/a/a;->a(Lcom/google/android/apps/gmm/c/a/b;)V

    .line 197
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/z;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-nez v0, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/z;->a:Lcom/google/android/apps/gmm/v/aa;

    instance-of v0, v0, Lcom/google/android/apps/gmm/mylocation/d/d;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->o:Z

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    .line 221
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/p;->e()Lcom/google/b/f/t;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/iamhere/p;->a(Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 222
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v3, Lcom/google/j/d/a/e;->b:Lcom/google/j/d/a/e;

    .line 223
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/p;->e()Lcom/google/b/f/t;

    move-result-object v4

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v5, Lcom/google/android/apps/gmm/iamhere/t;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/iamhere/t;-><init>()V

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/s;->a:[I

    iget-object v5, v1, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/iamhere/c/q;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/p;->c:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->m:Lcom/google/j/d/a/e;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/iamhere/c/o;->b()Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v5, Lcom/google/j/d/a/w;->d:Lcom/google/j/d/a/w;

    invoke-virtual {p0, v0, v5, v4}, Lcom/google/android/apps/gmm/iamhere/p;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/iamhere/c/o;->h:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/gmm/iamhere/p;->a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/j/d/a/e;)V

    goto/16 :goto_0

    :pswitch_1
    invoke-virtual {p0, v1, v3}, Lcom/google/android/apps/gmm/iamhere/p;->a(Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/j/d/a/e;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-nez v0, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/k;->c:Lcom/google/android/apps/gmm/iamhere/k;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/iamhere/p;->a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/iamhere/k;)V

    goto :goto_0
.end method

.method public declared-synchronized a(Lcom/google/android/apps/gmm/map/s/b;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 202
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 208
    :goto_0
    monitor-exit p0

    return-void

    .line 205
    :cond_0
    :try_start_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/s/b;->a:Lcom/google/android/apps/gmm/map/s/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->i:Lcom/google/android/apps/gmm/map/s/a;

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->h:Lcom/google/android/apps/gmm/map/r/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/k;->b:Lcom/google/android/apps/gmm/iamhere/k;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/iamhere/p;->a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/iamhere/k;)V

    .line 207
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/p;->i()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 202
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/j/d/a/e;)V
    .locals 4
    .param p3    # Lcom/google/j/d/a/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;",
            "Lcom/google/j/d/a/e;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-nez v0, :cond_0

    .line 551
    :goto_0
    return-void

    .line 545
    :cond_0
    if-eqz p3, :cond_1

    .line 546
    iput-object p3, p0, Lcom/google/android/apps/gmm/iamhere/p;->m:Lcom/google/j/d/a/e;

    .line 548
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v3, v2}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->D()Lcom/google/android/apps/gmm/place/b/b;

    move-result-object v1

    .line 550
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-interface {v1, v0, v2, v3, p2}, Lcom/google/android/apps/gmm/place/b/b;->a(Lcom/google/android/apps/gmm/base/g/c;ZLcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 1

    .prologue
    .line 460
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/p;->c:Ljava/lang/String;

    .line 461
    return-void
.end method

.method public final a(Lcom/google/j/d/a/e;)V
    .locals 1
    .param p1    # Lcom/google/j/d/a/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-nez v0, :cond_0

    .line 363
    :goto_0
    return-void

    .line 362
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/p;->m:Lcom/google/j/d/a/e;

    goto :goto_0
.end method

.method public final a(Lcom/google/o/h/a/gx;Z)V
    .locals 3
    .param p1    # Lcom/google/o/h/a/gx;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 868
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/p;->q:Lcom/google/o/h/a/gx;

    .line 869
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->q:Lcom/google/o/h/a/gx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->q:Lcom/google/o/h/a/gx;

    iget v0, v0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/gmm/base/l/s;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/p;->q:Lcom/google/o/h/a/gx;

    invoke-direct {v0, v1, v2, p2}, Lcom/google/android/apps/gmm/base/l/s;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/o/h/a/gx;Z)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->j()Lcom/google/android/apps/gmm/c/a/a;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/c/a/b;->b:Lcom/google/android/apps/gmm/c/a/b;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/c/a/a;->a(Lcom/google/android/apps/gmm/c/a/b;Lcom/google/android/apps/gmm/c/a/d;)V

    .line 870
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-nez v0, :cond_2

    .line 871
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->j()Lcom/google/android/apps/gmm/c/a/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/c/a/b;->a:Lcom/google/android/apps/gmm/c/a/b;

    .line 873
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/c/a/a;->a(Lcom/google/android/apps/gmm/c/a/b;)V

    .line 878
    :goto_2
    return-void

    .line 869
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->j()Lcom/google/android/apps/gmm/c/a/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/c/a/b;->b:Lcom/google/android/apps/gmm/c/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/c/a/a;->a(Lcom/google/android/apps/gmm/c/a/b;)V

    goto :goto_1

    .line 877
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/p;->i()V

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 704
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-nez v0, :cond_1

    .line 716
    :cond_0
    :goto_0
    return-void

    .line 707
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->o:Z

    if-eq v0, p1, :cond_0

    .line 710
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/iamhere/p;->o:Z

    .line 711
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->o:Z

    if-nez v0, :cond_0

    .line 714
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->f:Lcom/google/android/apps/gmm/iamhere/j;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/iamhere/j;->b()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/j;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 669
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 674
    :goto_0
    return v0

    .line 672
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v3, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 673
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/c/o;->b()Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 674
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/c/o;->b()Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    .line 673
    goto :goto_1

    :cond_2
    move v0, v1

    .line 674
    goto :goto_0
.end method

.method public final am_()V
    .locals 1

    .prologue
    .line 455
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/p;->c:Ljava/lang/String;

    .line 456
    return-void
.end method

.method public final b(Z)Lcom/google/android/apps/gmm/iamhere/c/o;
    .locals 2

    .prologue
    .line 771
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 772
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-nez v0, :cond_0

    .line 773
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 778
    :goto_0
    return-object v0

    .line 775
    :cond_0
    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/p;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 776
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    goto :goto_0

    .line 778
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->f:Lcom/google/android/apps/gmm/iamhere/j;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/iamhere/j;->d()Lcom/google/android/apps/gmm/iamhere/c/o;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/startpage/a/e;->b(Lcom/google/android/apps/gmm/startpage/a/a;)V

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/p;->l:Lcom/google/android/apps/gmm/iamhere/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->l:Lcom/google/android/apps/gmm/iamhere/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/e;->c()V

    .line 340
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->b:Lcom/google/android/apps/gmm/iamhere/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/p;->f:Lcom/google/android/apps/gmm/iamhere/j;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/iamhere/j;->b(Lcom/google/android/apps/gmm/iamhere/a/a;)V

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 342
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->b()V

    .line 343
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/iamhere/a/a;)V
    .locals 1

    .prologue
    .line 728
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->f:Lcom/google/android/apps/gmm/iamhere/j;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/iamhere/j;->b(Lcom/google/android/apps/gmm/iamhere/a/a;)V

    .line 729
    return-void
.end method

.method declared-synchronized b(Lcom/google/android/apps/gmm/iamhere/c/o;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 571
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/c/o;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/iamhere/c/o;->a()Ljava/util/List;

    move-result-object v4

    if-eq v0, v4, :cond_0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_3

    move v0, v2

    .line 572
    :goto_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 575
    if-eqz v0, :cond_1

    .line 576
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->k:Lcom/google/android/apps/gmm/iamhere/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/iamhere/p;->a:Lcom/google/android/apps/gmm/iamhere/c/o;

    invoke-virtual {v0, v4, p0}, Lcom/google/android/apps/gmm/iamhere/a;->a(Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)V

    .line 578
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v4

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/s;->a:[I

    iget-object v5, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/iamhere/c/q;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_0

    .line 579
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/p;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 580
    monitor-exit p0

    return-void

    :cond_2
    move v0, v3

    .line 571
    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1

    .line 578
    :pswitch_0
    :try_start_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v5, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-ne v0, v5, :cond_4

    :goto_3
    if-eqz v2, :cond_5

    iget-object v0, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->h:Lcom/google/android/apps/gmm/x/o;

    :goto_4
    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eq v1, v3, :cond_8

    new-instance v1, Lcom/google/android/apps/gmm/p/d/f;

    const-string v3, "snap_to_place"

    invoke-direct {v1, v3}, Lcom/google/android/apps/gmm/p/d/f;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-static {}, Lcom/google/o/b/a/h;->newBuilder()Lcom/google/o/b/a/j;

    move-result-object v3

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    iget v5, v3, Lcom/google/o/b/a/j;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v3, Lcom/google/o/b/a/j;->a:I

    iput-wide v6, v3, Lcom/google/o/b/a/j;->b:J

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget v0, v3, Lcom/google/o/b/a/j;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v3, Lcom/google/o/b/a/j;->a:I

    iput-wide v6, v3, Lcom/google/o/b/a/j;->c:J

    invoke-virtual {v3}, Lcom/google/o/b/a/j;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/h;

    iput-object v0, v1, Lcom/google/android/apps/gmm/p/d/f;->e:Lcom/google/o/b/a/h;

    iget-object v0, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/c/q;->a()I

    move-result v0

    iput v0, v1, Lcom/google/android/apps/gmm/p/d/f;->f:I

    if-eqz v2, :cond_7

    sget-object v0, Lcom/google/o/b/a/aa;->V:Lcom/google/o/b/a/aa;

    :goto_5
    iput-object v0, v1, Lcom/google/android/apps/gmm/p/d/f;->b:Lcom/google/o/b/a/aa;

    invoke-interface {v4, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/p/d/f;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 571
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v2, v3

    .line 578
    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    :try_start_2
    iget-object v5, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v3, :cond_6

    iget-object v1, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    goto :goto_4

    :cond_6
    move-object v0, v1

    goto :goto_4

    :cond_7
    sget-object v0, Lcom/google/o/b/a/aa;->U:Lcom/google/o/b/a/aa;

    goto :goto_5

    :cond_8
    const/4 v0, 0x0

    invoke-interface {v4, v0}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/p/d/f;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-nez v0, :cond_0

    .line 559
    :goto_0
    return-void

    .line 558
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->f:Lcom/google/android/apps/gmm/iamhere/j;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/iamhere/j;->a()V

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 347
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->h()V

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->f:Lcom/google/android/apps/gmm/iamhere/j;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/iamhere/j;->c()V

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->g:Lcom/google/android/apps/gmm/iamhere/c;

    if-eqz v0, :cond_0

    .line 350
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/p;->g:Lcom/google/android/apps/gmm/iamhere/c;

    iget-object v0, v1, Lcom/google/android/apps/gmm/iamhere/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->n:Lcom/google/android/gms/location/reporting/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->n:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/c;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/p;->n:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/c;->c()V

    .line 355
    :cond_1
    return-void
.end method
