.class public Lcom/google/android/apps/gmm/map/legacy/internal/b/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/map/b/a/ab;

.field b:[Lcom/google/android/apps/gmm/map/internal/c/aq;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ab;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ab;[I)V
    .locals 4

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 31
    array-length v0, p2

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    .line 32
    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 33
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/aq;

    aget v3, p2, v0

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/aq;-><init>(I)V

    aput-object v2, v1, v0

    .line 32
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_0
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/aq;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 26
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    .line 27
    return-void
.end method


# virtual methods
.method final a(II)Lcom/google/android/apps/gmm/map/legacy/internal/b/a;
    .locals 3

    .prologue
    .line 78
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(Lcom/google/android/apps/gmm/map/b/a/ab;II)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 79
    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/aq;)V

    return-object v1

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    .line 79
    invoke-static {v0, p1, p2}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/c/aq;

    goto :goto_0
.end method

.method final a(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    if-eqz v1, :cond_0

    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    array-length v1, v1

    if-gt v1, p1, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    aget-object v1, v1, p1

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ar;->b:Lcom/google/android/apps/gmm/map/internal/c/ar;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/aq;->a:I

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/ar;->c:I

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
