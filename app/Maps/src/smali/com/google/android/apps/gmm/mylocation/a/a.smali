.class public Lcom/google/android/apps/gmm/mylocation/a/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/mylocation/a/i;


# static fields
.field static final a:J

.field static final b:J

.field static final c:J

.field static final d:J

.field private static final g:J

.field private static final v:Landroid/animation/TypeEvaluator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/animation/TypeEvaluator",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation
.end field

.field private static final w:Landroid/animation/TimeInterpolator;

.field private static final x:Landroid/animation/TimeInterpolator;

.field private static final y:Landroid/animation/TimeInterpolator;


# instance fields
.field private A:Landroid/animation/ValueAnimator;

.field private B:Landroid/animation/ValueAnimator;

.field private C:Landroid/animation/ValueAnimator;

.field private D:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field

.field e:Landroid/animation/ValueAnimator;

.field f:Lcom/google/android/apps/gmm/mylocation/g/a;

.field private h:J

.field private i:J

.field private j:Lcom/google/android/apps/gmm/mylocation/g/a;

.field private k:Lcom/google/android/apps/gmm/mylocation/g/a;

.field private l:F

.field private m:D

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:Z

.field private s:Z

.field private t:Lcom/google/android/apps/gmm/mylocation/a/h;

.field private u:Lcom/google/android/apps/gmm/mylocation/a/h;

.field private z:Landroid/animation/ValueAnimator;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 36
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/mylocation/a/a;->a:J

    .line 38
    const-wide/high16 v0, 0x3ff8000000000000L    # 1.5

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    sput-wide v0, Lcom/google/android/apps/gmm/mylocation/a/a;->b:J

    .line 40
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/mylocation/a/a;->c:J

    .line 44
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/mylocation/a/a;->d:J

    .line 47
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/mylocation/a/a;->g:J

    .line 152
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/a/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/mylocation/a/b;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/a/a;->v:Landroid/animation/TypeEvaluator;

    .line 159
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/a/a;->w:Landroid/animation/TimeInterpolator;

    .line 160
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/a/a;->x:Landroid/animation/TimeInterpolator;

    .line 161
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/a/a;->y:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x2

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    new-instance v2, Landroid/animation/ValueAnimator;

    invoke-direct {v2}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->e:Landroid/animation/ValueAnimator;

    .line 118
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->h:J

    .line 121
    sget-wide v2, Lcom/google/android/apps/gmm/mylocation/a/a;->a:J

    iput-wide v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->i:J

    .line 123
    new-instance v2, Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/mylocation/g/a;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->j:Lcom/google/android/apps/gmm/mylocation/g/a;

    .line 124
    new-instance v2, Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/mylocation/g/a;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    .line 125
    new-instance v2, Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/mylocation/g/a;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->f:Lcom/google/android/apps/gmm/mylocation/g/a;

    .line 131
    iput v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->l:F

    .line 132
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->m:D

    .line 133
    iput v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->n:F

    .line 134
    iput v4, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->o:F

    .line 135
    iput v4, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->p:F

    .line 136
    iput v4, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->q:F

    .line 138
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->r:Z

    .line 139
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->s:Z

    .line 148
    sget-object v2, Lcom/google/android/apps/gmm/mylocation/a/h;->a:Lcom/google/android/apps/gmm/mylocation/a/h;

    iput-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->t:Lcom/google/android/apps/gmm/mylocation/a/h;

    .line 149
    sget-object v2, Lcom/google/android/apps/gmm/mylocation/a/h;->a:Lcom/google/android/apps/gmm/mylocation/a/h;

    iput-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->u:Lcom/google/android/apps/gmm/mylocation/a/h;

    .line 166
    new-instance v2, Landroid/animation/ValueAnimator;

    invoke-direct {v2}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->z:Landroid/animation/ValueAnimator;

    .line 167
    new-instance v2, Landroid/animation/ValueAnimator;

    invoke-direct {v2}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->A:Landroid/animation/ValueAnimator;

    .line 168
    new-instance v2, Landroid/animation/ValueAnimator;

    invoke-direct {v2}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->B:Landroid/animation/ValueAnimator;

    .line 169
    new-instance v2, Landroid/animation/ValueAnimator;

    invoke-direct {v2}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->C:Landroid/animation/ValueAnimator;

    .line 175
    const/4 v2, 0x4

    new-array v2, v2, [Landroid/animation/ValueAnimator;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->B:Landroid/animation/ValueAnimator;

    aput-object v3, v2, v1

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->z:Landroid/animation/ValueAnimator;

    aput-object v3, v2, v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->A:Landroid/animation/ValueAnimator;

    aput-object v3, v2, v6

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->C:Landroid/animation/ValueAnimator;

    aput-object v4, v2, v3

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    array-length v3, v2

    if-ltz v3, :cond_1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x5

    int-to-long v4, v3

    add-long/2addr v0, v4

    div-int/lit8 v3, v3, 0xa

    int-to-long v4, v3

    add-long/2addr v0, v4

    const-wide/32 v4, 0x7fffffff

    cmp-long v3, v0, v4

    if-lez v3, :cond_3

    const v0, 0x7fffffff

    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v1, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    iput-object v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->D:Ljava/util/List;

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->B:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/google/android/apps/gmm/mylocation/a/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/mylocation/a/c;-><init>(Lcom/google/android/apps/gmm/mylocation/a/a;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->C:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/google/android/apps/gmm/mylocation/a/d;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/mylocation/a/d;-><init>(Lcom/google/android/apps/gmm/mylocation/a/a;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->A:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/google/android/apps/gmm/mylocation/a/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/mylocation/a/e;-><init>(Lcom/google/android/apps/gmm/mylocation/a/a;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->z:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/google/android/apps/gmm/mylocation/a/f;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/mylocation/a/f;-><init>(Lcom/google/android/apps/gmm/mylocation/a/a;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->e:Landroid/animation/ValueAnimator;

    new-array v1, v6, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->e:Landroid/animation/ValueAnimator;

    sget-wide v2, Lcom/google/android/apps/gmm/mylocation/a/a;->b:J

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->e:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->w:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->e:Landroid/animation/ValueAnimator;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v6}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 219
    return-void

    .line 175
    :cond_3
    const-wide/32 v4, -0x80000000

    cmp-long v3, v0, v4

    if-gez v3, :cond_4

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_4
    long-to-int v0, v0

    goto :goto_1

    .line 214
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private b(J)V
    .locals 3

    .prologue
    .line 408
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->D:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 409
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->D:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    .line 410
    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 408
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 412
    :cond_0
    iput-wide p1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->i:J

    .line 413
    return-void
.end method


# virtual methods
.method public final a(J)I
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 255
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_2

    move v0, v3

    :goto_0
    if-nez v0, :cond_3

    .line 257
    :cond_0
    :goto_1
    iget-wide v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->h:J

    iget-wide v6, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->i:J

    add-long/2addr v0, v6

    iget-wide v6, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->h:J

    cmp-long v5, p1, v6

    if-ltz v5, :cond_e

    cmp-long v0, p1, v0

    if-gtz v0, :cond_e

    move v0, v3

    :goto_2
    if-nez v0, :cond_1

    .line 258
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/a/h;->a:Lcom/google/android/apps/gmm/mylocation/a/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->t:Lcom/google/android/apps/gmm/mylocation/a/h;

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->u:Lcom/google/android/apps/gmm/mylocation/a/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->t:Lcom/google/android/apps/gmm/mylocation/a/h;

    sget-object v0, Lcom/google/android/apps/gmm/mylocation/a/h;->a:Lcom/google/android/apps/gmm/mylocation/a/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->u:Lcom/google/android/apps/gmm/mylocation/a/h;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->t:Lcom/google/android/apps/gmm/mylocation/a/h;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/h;->a:Lcom/google/android/apps/gmm/mylocation/a/h;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->B:Landroid/animation/ValueAnimator;

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->f:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v5, v1, v2

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->j:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v5, v1, v3

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setObjectValues([Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->B:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->v:Landroid/animation/TypeEvaluator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->C:Landroid/animation/ValueAnimator;

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->f:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v5, v1, v2

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->j:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v5, v1, v3

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setObjectValues([Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->C:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->v:Landroid/animation/TypeEvaluator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->z:Landroid/animation/ValueAnimator;

    new-array v1, v4, [I

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->f:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    aput v5, v1, v2

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->j:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    aput v5, v1, v3

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    sget-object v0, Lcom/google/android/apps/gmm/mylocation/a/g;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->t:Lcom/google/android/apps/gmm/mylocation/a/h;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/mylocation/a/h;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_3
    iput-wide p1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->h:J

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->t:Lcom/google/android/apps/gmm/mylocation/a/h;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/h;->a:Lcom/google/android/apps/gmm/mylocation/a/h;

    if-eq v0, v1, :cond_f

    iget-wide v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->h:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_f

    .line 263
    const-wide/16 v0, 0x0

    iget-wide v6, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->h:J

    sub-long v6, p1, v6

    iget-wide v8, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->i:J

    .line 264
    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    .line 263
    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->D:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v1, v2

    :goto_4
    if-ge v1, v5, :cond_f

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->D:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    .line 267
    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v6, v7}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 265
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_2
    move v0, v2

    .line 255
    goto/16 :goto_0

    :cond_3
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->s:Z

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->r:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->f:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/mylocation/g/a;->a(Lcom/google/android/apps/gmm/mylocation/g/a;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->j:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/mylocation/g/a;->a(Lcom/google/android/apps/gmm/mylocation/g/a;)V

    iput-boolean v3, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->r:Z

    goto/16 :goto_1

    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/a/h;->a:Lcom/google/android/apps/gmm/mylocation/a/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->u:Lcom/google/android/apps/gmm/mylocation/a/h;

    iget-wide v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->h:J

    sub-long v6, p1, v0

    sget-wide v0, Lcom/google/android/apps/gmm/mylocation/a/a;->d:J

    cmp-long v0, v6, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->j:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_a

    move v0, v3

    :goto_5
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_b

    move v0, v3

    :goto_6
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->j:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->j:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->l:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->n:F

    div-float/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v1, v1, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->j:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    float-to-double v8, v1

    iget-wide v10, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->m:D

    mul-double/2addr v8, v10

    double-to-float v1, v8

    iget v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->l:F

    div-float/2addr v1, v5

    iget v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->n:F

    div-float/2addr v1, v5

    sget-wide v8, Lcom/google/android/apps/gmm/mylocation/a/a;->g:J

    cmp-long v5, v6, v8

    if-gtz v5, :cond_6

    const/high16 v5, 0x40200000    # 2.5f

    cmpl-float v5, v0, v5

    if-lez v5, :cond_13

    const v5, 0x3e4ccccd    # 0.2f

    mul-float/2addr v1, v5

    cmpl-float v1, v0, v1

    if-lez v1, :cond_13

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->j:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v8, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v8, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v8, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v5, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move v1, v3

    :goto_7
    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v8, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    int-to-float v8, v8

    float-to-double v8, v8

    iget-wide v10, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->m:D

    mul-double/2addr v8, v10

    double-to-float v8, v8

    iget v9, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->l:F

    div-float/2addr v8, v9

    iget v9, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->n:F

    div-float/2addr v8, v9

    const/high16 v9, 0x41880000    # 17.0f

    cmpg-float v8, v8, v9

    if-gez v8, :cond_7

    iget v8, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->q:F

    const/high16 v9, 0x41400000    # 12.0f

    cmpg-float v8, v8, v9

    if-gez v8, :cond_7

    iput v2, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    :cond_7
    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->j:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    iget-object v8, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v8, v8, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    if-eq v5, v8, :cond_9

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    iget-object v8, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->j:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v8, v8, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    sub-int/2addr v5, v8

    int-to-float v5, v5

    float-to-double v8, v5

    iget-wide v10, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->m:D

    mul-double/2addr v8, v10

    double-to-float v5, v8

    iget v8, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->l:F

    div-float/2addr v5, v8

    iget v8, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->n:F

    div-float/2addr v5, v8

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    sget-wide v8, Lcom/google/android/apps/gmm/mylocation/a/a;->g:J

    cmp-long v6, v6, v8

    if-gtz v6, :cond_8

    const/high16 v6, 0x42480000    # 50.0f

    cmpl-float v5, v5, v6

    if-gtz v5, :cond_8

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->j:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    if-nez v5, :cond_9

    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->j:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    iput v5, v1, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    move v1, v3

    :cond_9
    if-eqz v1, :cond_0

    const/high16 v1, 0x42c80000    # 100.0f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_c

    sget-object v0, Lcom/google/android/apps/gmm/mylocation/a/h;->b:Lcom/google/android/apps/gmm/mylocation/a/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->u:Lcom/google/android/apps/gmm/mylocation/a/h;

    goto/16 :goto_1

    :cond_a
    move v0, v2

    goto/16 :goto_5

    :cond_b
    move v0, v2

    goto/16 :goto_6

    :cond_c
    iget v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->o:F

    iget v5, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->p:F

    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_d

    sget-object v0, Lcom/google/android/apps/gmm/mylocation/a/h;->b:Lcom/google/android/apps/gmm/mylocation/a/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->u:Lcom/google/android/apps/gmm/mylocation/a/h;

    goto/16 :goto_1

    :cond_d
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/a/h;->d:Lcom/google/android/apps/gmm/mylocation/a/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->u:Lcom/google/android/apps/gmm/mylocation/a/h;

    goto/16 :goto_1

    :cond_e
    move v0, v2

    .line 257
    goto/16 :goto_2

    .line 259
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->B:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->w:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->C:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->w:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->z:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->w:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->A:Landroid/animation/ValueAnimator;

    new-array v1, v4, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->A:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->w:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-wide v0, Lcom/google/android/apps/gmm/mylocation/a/a;->a:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/mylocation/a/a;->b(J)V

    goto/16 :goto_3

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->B:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->y:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->C:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->w:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->z:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->w:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->A:Landroid/animation/ValueAnimator;

    new-array v1, v12, [F

    fill-array-data v1, :array_1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->A:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->x:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-wide v0, Lcom/google/android/apps/gmm/mylocation/a/a;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/mylocation/a/a;->b(J)V

    goto/16 :goto_3

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->B:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->y:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->C:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->y:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->z:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->w:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->A:Landroid/animation/ValueAnimator;

    new-array v1, v12, [F

    fill-array-data v1, :array_2

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->A:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/a/a;->x:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    sget-wide v0, Lcom/google/android/apps/gmm/mylocation/a/a;->c:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/mylocation/a/a;->b(J)V

    goto/16 :goto_3

    .line 271
    :cond_f
    iget-wide v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->h:J

    iget-wide v6, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->i:J

    add-long/2addr v0, v6

    iget-wide v6, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->h:J

    cmp-long v5, p1, v6

    if-ltz v5, :cond_12

    cmp-long v0, p1, v0

    if-gtz v0, :cond_12

    :goto_8
    if-nez v3, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_10
    move v2, v4

    .line 274
    :cond_11
    return v2

    :cond_12
    move v3, v2

    .line 271
    goto :goto_8

    :cond_13
    move v1, v2

    goto/16 :goto_7

    .line 259
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3e4ccccd    # 0.2f
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x3ecccccd    # 0.4f
        0x0
    .end array-data
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 313
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 3

    .prologue
    .line 283
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p1, Lcom/google/android/apps/gmm/map/f/o;->g:F

    mul-float/2addr v0, v1

    iget v1, p1, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v2, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->l:F

    .line 284
    iget v0, p1, Lcom/google/android/apps/gmm/map/f/o;->i:F

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->n:F

    .line 285
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->n:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->o:F

    .line 286
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->n:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->p:F

    .line 287
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->m:D

    .line 288
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->q:F

    .line 289
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/w;)V
    .locals 0

    .prologue
    .line 487
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;)Z
    .locals 1

    .prologue
    .line 481
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/mylocation/g/a;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v5, 0x0

    const/high16 v6, 0x41400000    # 12.0f

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->f:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->f:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v4, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v4, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    :goto_0
    iput-object v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->f:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    iput v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->f:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->h:F

    iput v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->h:F

    .line 241
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->q:F

    cmpl-float v0, v0, v5

    if-ltz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->k:F

    .line 243
    iget v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->q:F

    const/high16 v4, 0x41800000    # 16.0f

    cmpg-float v4, v0, v4

    if-gez v4, :cond_4

    cmpg-float v3, v0, v6

    if-gez v3, :cond_3

    move v0, v2

    :goto_2
    iput v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    .line 245
    iget v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    int-to-float v0, v0

    float-to-double v2, v0

    iget-wide v4, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->m:D

    mul-double/2addr v2, v4

    double-to-float v0, v2

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->l:F

    div-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->n:F

    div-float/2addr v0, v2

    const/high16 v2, 0x41880000    # 17.0f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->q:F

    cmpg-float v0, v0, v6

    if-gez v0, :cond_0

    iput v1, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    .line 247
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 238
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_0

    :cond_2
    move v0, v1

    .line 241
    goto :goto_1

    .line 243
    :cond_3
    sub-float/2addr v0, v6

    const/high16 v3, 0x3d800000    # 0.0625f

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    goto :goto_2

    :cond_4
    move v0, v3

    goto :goto_2
.end method

.method public final b()Lcom/google/android/apps/gmm/mylocation/b/g;
    .locals 1

    .prologue
    .line 491
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/g;->b:Lcom/google/android/apps/gmm/mylocation/b/g;

    return-object v0
.end method

.method public final b(Lcom/google/android/apps/gmm/mylocation/g/a;)V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->k:Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/mylocation/g/a;->a(Lcom/google/android/apps/gmm/mylocation/g/a;)V

    .line 301
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/a/a;->s:Z

    .line 306
    return-void
.end method
