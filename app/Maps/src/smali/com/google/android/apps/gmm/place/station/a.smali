.class public Lcom/google/android/apps/gmm/place/station/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/station/b/a;
.implements Ljava/io/Serializable;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/android/apps/gmm/place/station/z;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/cg;Lcom/google/r/b/a/ach;J)V
    .locals 8
    .param p2    # Lcom/google/android/libraries/curvular/cg;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/c;",
            ">;",
            "Lcom/google/r/b/a/ach;",
            "J)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v0

    .line 44
    iget-object v1, p3, Lcom/google/r/b/a/ach;->f:Lcom/google/r/b/a/aby;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/r/b/a/aby;->d()Lcom/google/r/b/a/aby;

    move-result-object v1

    :goto_0
    iget v1, v1, Lcom/google/r/b/a/aby;->b:I

    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v5, v2

    move-object v6, v2

    .line 43
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZZLcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/a;->a:Ljava/lang/String;

    .line 46
    new-instance v0, Lcom/google/android/apps/gmm/place/station/z;

    .line 49
    iget-object v1, p3, Lcom/google/r/b/a/ach;->g:Lcom/google/maps/g/wb;

    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/maps/g/wb;->j()Lcom/google/maps/g/wb;

    move-result-object v3

    :goto_1
    move-object v1, p1

    move-object v2, p2

    move-wide v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/station/z;-><init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/cg;Lcom/google/maps/g/wb;J)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/a;->b:Lcom/google/android/apps/gmm/place/station/z;

    .line 51
    return-void

    .line 44
    :cond_0
    iget-object v1, p3, Lcom/google/r/b/a/ach;->f:Lcom/google/r/b/a/aby;

    goto :goto_0

    .line 49
    :cond_1
    iget-object v3, p3, Lcom/google/r/b/a/ach;->g:Lcom/google/maps/g/wb;

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/place/station/b/b;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/a;->b:Lcom/google/android/apps/gmm/place/station/z;

    return-object v0
.end method
