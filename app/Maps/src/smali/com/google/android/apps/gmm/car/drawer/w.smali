.class Lcom/google/android/apps/gmm/car/drawer/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/drawer/s;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/drawer/t;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/drawer/t;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 265
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/car/drawer/t;->f:Z

    if-ne v0, v1, :cond_0

    .line 283
    :goto_0
    return-object v4

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/car/drawer/t;->b(Lcom/google/android/apps/gmm/car/drawer/t;Z)V

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/drawer/t;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/car/drawer/t;->a(Lcom/google/android/apps/gmm/car/drawer/t;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/t;->e:Landroid/os/Handler;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->g:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 281
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/car/drawer/t;->b(Lcom/google/android/apps/gmm/car/drawer/t;Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->bh:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 236
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/ad;->o:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 246
    sget v0, Lcom/google/android/apps/gmm/car/drawer/t;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/drawer/t;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 257
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->f:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/car/drawer/t;->a(Lcom/google/android/apps/gmm/car/drawer/t;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/w;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/t;->e:Landroid/os/Handler;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->g:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 260
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 257
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
