.class public Lcom/google/android/apps/gmm/addaplace/c/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/addaplace/c/a;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/addaplace/a/a;

.field public final b:Lcom/google/android/apps/gmm/addaplace/c/d;

.field final c:Landroid/content/Context;

.field public d:Lcom/google/r/b/a/abn;

.field e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

.field public f:Landroid/app/ProgressDialog;

.field public g:Z

.field h:Z

.field private final i:Lcom/google/maps/g/gy;

.field private final j:Ljava/lang/String;

.field private k:Ljava/lang/CharSequence;

.field private final l:Lcom/google/android/apps/gmm/base/l/a/n;

.field private m:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/addaplace/a/a;Lcom/google/maps/g/gy;Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->m:Z

    .line 52
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->g:Z

    .line 53
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->h:Z

    .line 57
    iput-object p1, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    .line 58
    iput-object p2, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->i:Lcom/google/maps/g/gy;

    .line 59
    iput-object p3, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    .line 60
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->c:Landroid/content/Context;

    .line 61
    new-instance v0, Lcom/google/android/apps/gmm/addaplace/c/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/addaplace/c/d;-><init>(Lcom/google/android/apps/gmm/addaplace/c/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->b:Lcom/google/android/apps/gmm/addaplace/c/d;

    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/addaplace/c/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/addaplace/c/e;-><init>(Lcom/google/android/apps/gmm/addaplace/c/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->l:Lcom/google/android/apps/gmm/base/l/a/n;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->c:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->h:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->j:Ljava/lang/String;

    .line 64
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/addaplace/c/b;)Z
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->d:Lcom/google/maps/g/cc;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->c:Lcom/google/maps/g/gy;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->a:Ljava/lang/String;

    .line 148
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;Z)Lcom/google/android/libraries/curvular/cf;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 174
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 176
    const/4 v3, 0x0

    .line 179
    if-nez p2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->m:Z

    if-nez v1, :cond_1

    .line 180
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iput-object v0, v1, Lcom/google/android/apps/gmm/addaplace/a/a;->b:Ljava/lang/String;

    move v3, v2

    .line 186
    :cond_1
    if-eqz p2, :cond_4

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->g:Z

    if-nez v1, :cond_4

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {}, Lcom/google/r/b/a/ahw;->newBuilder()Lcom/google/r/b/a/ahy;

    move-result-object v4

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v5, v4, Lcom/google/r/b/a/ahy;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v4, Lcom/google/r/b/a/ahy;->a:I

    iput-object v0, v4, Lcom/google/r/b/a/ahy;->b:Ljava/lang/Object;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget-object v5, v4, Lcom/google/r/b/a/ahy;->c:Lcom/google/n/ao;

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v5, Lcom/google/n/ao;->d:Z

    iget v0, v4, Lcom/google/r/b/a/ahy;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v4, Lcom/google/r/b/a/ahy;->a:I

    invoke-virtual {v4}, Lcom/google/r/b/a/ahy;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ahw;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    new-instance v4, Lcom/google/android/apps/gmm/addaplace/c/c;

    invoke-direct {v4, p0}, Lcom/google/android/apps/gmm/addaplace/c/c;-><init>(Lcom/google/android/apps/gmm/addaplace/c/b;)V

    sget-object v5, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v0, v4, v5}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 190
    :cond_4
    if-eqz p2, :cond_6

    .line 191
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->m:Z

    move v0, v2

    .line 195
    :goto_0
    if-eqz v0, :cond_5

    .line 196
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 198
    :cond_5
    return-object v7

    :cond_6
    move v0, v3

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/CharSequence;)Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->e:Ljava/lang/String;

    .line 248
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/CharSequence;)Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->f:Ljava/lang/String;

    .line 255
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->c:Lcom/google/maps/g/gy;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 6

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->c:Lcom/google/maps/g/gy;

    .line 89
    if-eqz v1, :cond_0

    .line 90
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    .line 91
    iget-wide v2, v1, Lcom/google/maps/g/gy;->b:D

    iget-wide v4, v1, Lcom/google/maps/g/gy;->c:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    .line 93
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->d:Lcom/google/maps/g/cc;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->d:Lcom/google/maps/g/cc;

    invoke-virtual {v0}, Lcom/google/maps/g/cc;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->k:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->E()Lcom/google/android/apps/gmm/reportmapissue/a/f;

    move-result-object v0

    .line 125
    invoke-interface {v0}, Lcom/google/android/apps/gmm/reportmapissue/a/f;->d()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->k:Ljava/lang/CharSequence;

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->k:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/base/l/a/aa;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->b:Lcom/google/android/apps/gmm/addaplace/c/d;

    return-object v0
.end method

.method public final j()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    :goto_0
    return-object v3

    .line 160
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/suggest/k;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/suggest/k;-><init>()V

    .line 161
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->h:Lcom/google/android/apps/gmm/suggest/e/c;

    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(Lcom/google/android/apps/gmm/suggest/e/c;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->b:Ljava/lang/String;

    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(Ljava/lang/String;)V

    .line 163
    const/4 v0, 0x0

    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(Z)V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    .line 166
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    .line 165
    invoke-static {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/suggest/k;Landroid/app/Fragment;Landroid/app/Fragment;)Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V

    goto :goto_0
.end method

.method public final k()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    :goto_0
    return-object v2

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    .line 265
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;

    move-result-object v0

    .line 267
    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V

    goto :goto_0
.end method

.method public final l()Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 299
    :goto_0
    return-object v4

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->d:Lcom/google/maps/g/cc;

    if-nez v0, :cond_1

    const-string v0, ""

    .line 284
    :goto_1
    new-instance v1, Lcom/google/android/apps/gmm/suggest/k;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/suggest/k;-><init>()V

    .line 285
    sget-object v2, Lcom/google/android/apps/gmm/suggest/e/c;->i:Lcom/google/android/apps/gmm/suggest/e/c;

    iget-object v3, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/suggest/l;->a(Lcom/google/android/apps/gmm/suggest/e/c;)V

    .line 286
    iget-object v2, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->j:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/suggest/l;->b(Ljava/lang/String;)V

    .line 287
    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(Ljava/lang/String;)V

    .line 289
    const/4 v0, 0x1

    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/suggest/l;->c(Z)V

    .line 291
    iget-object v0, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/suggest/l;->b(Z)V

    .line 293
    iget-object v0, v1, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Z)V

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    .line 296
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    .line 295
    invoke-static {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/suggest/k;Landroid/app/Fragment;Landroid/app/Fragment;)Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    move-result-object v0

    .line 298
    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V

    goto :goto_0

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    .line 283
    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->d:Lcom/google/maps/g/cc;

    invoke-virtual {v0}, Lcom/google/maps/g/cc;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
