.class public final enum Lcom/google/android/apps/gmm/map/o/ar;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/o/ar;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/o/ar;

.field public static final enum b:Lcom/google/android/apps/gmm/map/o/ar;

.field public static final enum c:Lcom/google/android/apps/gmm/map/o/ar;

.field public static final enum d:Lcom/google/android/apps/gmm/map/o/ar;

.field public static final enum e:Lcom/google/android/apps/gmm/map/o/ar;

.field public static final enum f:Lcom/google/android/apps/gmm/map/o/ar;

.field public static final enum g:Lcom/google/android/apps/gmm/map/o/ar;

.field private static final synthetic h:[Lcom/google/android/apps/gmm/map/o/ar;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ar;

    const-string v1, "NAVIGATION"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/o/ar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/ar;->a:Lcom/google/android/apps/gmm/map/o/ar;

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ar;

    const-string v1, "NAVIGATION_CALLOUTS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/o/ar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/ar;->b:Lcom/google/android/apps/gmm/map/o/ar;

    .line 29
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ar;

    const-string v1, "VECTOR_MAP"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/map/o/ar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/ar;->c:Lcom/google/android/apps/gmm/map/o/ar;

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ar;

    const-string v1, "VECTOR_MAP_TABLET"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/map/o/ar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/ar;->d:Lcom/google/android/apps/gmm/map/o/ar;

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ar;

    const-string v1, "VECTOR_MAP_LDPI"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/map/o/ar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/ar;->e:Lcom/google/android/apps/gmm/map/o/ar;

    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ar;

    const-string v1, "NAVIGATION_CAR_HEAD_UNIT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/o/ar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/ar;->f:Lcom/google/android/apps/gmm/map/o/ar;

    .line 33
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ar;

    const-string v1, "VECTOR_MAP_CAR_HEAD_UNIT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/o/ar;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/ar;->g:Lcom/google/android/apps/gmm/map/o/ar;

    .line 26
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/o/ar;

    sget-object v1, Lcom/google/android/apps/gmm/map/o/ar;->a:Lcom/google/android/apps/gmm/map/o/ar;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/o/ar;->b:Lcom/google/android/apps/gmm/map/o/ar;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/o/ar;->c:Lcom/google/android/apps/gmm/map/o/ar;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/map/o/ar;->d:Lcom/google/android/apps/gmm/map/o/ar;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/map/o/ar;->e:Lcom/google/android/apps/gmm/map/o/ar;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/map/o/ar;->f:Lcom/google/android/apps/gmm/map/o/ar;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/map/o/ar;->g:Lcom/google/android/apps/gmm/map/o/ar;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/o/ar;->h:[Lcom/google/android/apps/gmm/map/o/ar;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/o/ar;
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/gmm/map/o/ar;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/ar;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/o/ar;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/apps/gmm/map/o/ar;->h:[Lcom/google/android/apps/gmm/map/o/ar;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/o/ar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/o/ar;

    return-object v0
.end method
