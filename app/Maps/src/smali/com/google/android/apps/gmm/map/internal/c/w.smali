.class public Lcom/google/android/apps/gmm/map/internal/c/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/bu;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/indoor/d/f;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/map/indoor/d/f;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 23
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/indoor/d/f;)Lcom/google/android/apps/gmm/map/internal/c/w;
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/x;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/x;-><init>()V

    iput-object p0, v0, Lcom/google/android/apps/gmm/map/internal/c/x;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/x;->a()Lcom/google/android/apps/gmm/map/internal/c/w;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bv;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->b:Lcom/google/android/apps/gmm/map/internal/c/bv;

    return-object v0
.end method

.method public final a(Lcom/google/e/a/a/a/b;)V
    .locals 3

    .prologue
    .line 80
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/l;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 81
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ai;)Z
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->q:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/bu;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/c/w;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 61
    if-ne p0, p1, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v0

    .line 64
    :cond_1
    if-nez p1, :cond_2

    .line 65
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 67
    :cond_2
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/internal/c/w;

    if-nez v2, :cond_3

    move v0, v1

    .line 68
    goto :goto_0

    .line 70
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/w;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    if-eq v2, v3, :cond_0

    if-eqz v2, :cond_4

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 51
    return v0

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/l;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/l;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
