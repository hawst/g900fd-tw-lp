.class public abstract Lcom/google/android/apps/gmm/place/riddler/d/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/riddler/d/a;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/base/activities/c;

.field public b:Z

.field public c:Z

.field public d:I

.field public e:Lcom/google/android/libraries/curvular/h;

.field public f:Lcom/google/android/libraries/curvular/h;

.field private g:Lcom/google/android/libraries/curvular/c/h;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->b:Z

    .line 43
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->c:Z

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/place/riddler/d/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/riddler/d/e;-><init>(Lcom/google/android/apps/gmm/place/riddler/d/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->g:Lcom/google/android/libraries/curvular/c/h;

    .line 63
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 64
    iput v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->d:I

    .line 65
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/place/riddler/d/b;)Lcom/google/android/libraries/curvular/h;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->e:Lcom/google/android/libraries/curvular/h;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Lcom/google/android/apps/gmm/place/riddler/d/f;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/place/riddler/d/f;-><init>(Lcom/google/android/apps/gmm/place/riddler/d/d;Lcom/google/android/apps/gmm/place/riddler/d/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->e:Lcom/google/android/libraries/curvular/h;

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->e:Lcom/google/android/libraries/curvular/h;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/place/riddler/d/c;)Lcom/google/android/libraries/curvular/h;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->f:Lcom/google/android/libraries/curvular/h;

    if-nez v0, :cond_0

    .line 146
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/place/riddler/d/d;->b(Lcom/google/android/apps/gmm/place/riddler/d/c;)Lcom/google/android/libraries/curvular/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->f:Lcom/google/android/libraries/curvular/h;

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->f:Lcom/google/android/libraries/curvular/h;

    return-object v0
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/riddler/d/d;->c()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 183
    iget v2, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->d:I

    if-eq p1, v2, :cond_2

    .line 184
    iget v2, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->d:I

    if-lt p1, v2, :cond_0

    if-nez p1, :cond_3

    .line 186
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->b:Z

    .line 191
    :goto_0
    iput p1, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->d:I

    .line 192
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->e:Lcom/google/android/libraries/curvular/h;

    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/riddler/d/d;->c()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->c:Z

    if-nez v2, :cond_4

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/riddler/d/d;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 194
    :cond_1
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 197
    :cond_2
    return-void

    .line 189
    :cond_3
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->b:Z

    goto :goto_0

    :cond_4
    move v0, v1

    .line 193
    goto :goto_1
.end method

.method public a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 158
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->b:Z

    .line 159
    iput v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->d:I

    .line 160
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 0

    .prologue
    .line 166
    return-void
.end method

.method final a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 239
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->b:Z

    if-eq v0, v1, :cond_0

    .line 240
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->b:Z

    .line 241
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->e:Lcom/google/android/libraries/curvular/h;

    .line 242
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/riddler/d/d;->c()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->c:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 246
    :cond_0
    return-void

    .line 242
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Lcom/google/android/apps/gmm/place/riddler/d/c;)Lcom/google/android/libraries/curvular/h;
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 0

    .prologue
    .line 173
    return-void
.end method

.method public abstract c()Ljava/lang/Boolean;
.end method

.method public g()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 113
    const/4 v0, 0x0

    return-object v0
.end method

.method public h()Lcom/google/android/libraries/curvular/cf;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->c:Z

    .line 120
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 121
    const/4 v0, 0x0

    return-object v0
.end method

.method public i()Lcom/google/android/libraries/curvular/cf;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 127
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()Lcom/google/android/libraries/curvular/c/h;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->g:Lcom/google/android/libraries/curvular/c/h;

    return-object v0
.end method
