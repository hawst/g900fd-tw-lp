.class Lcom/google/android/apps/gmm/p/f/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/p/f/h;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/p/f/h;)V
    .locals 0

    .prologue
    .line 1175
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 3

    .prologue
    .line 1220
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v1, v0, Lcom/google/android/apps/gmm/p/f/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1221
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/f/h;->i:Landroid/hardware/Sensor;

    if-ne p1, v0, :cond_1

    .line 1222
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    iput p2, v0, Lcom/google/android/apps/gmm/p/f/l;->d:I

    .line 1223
    const-string v0, "OrientationProviderImpl"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1224
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x1d

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Magnetometer acc: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1228
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/p/f/l;->a()V

    .line 1230
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    .line 1186
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v1, v0, Lcom/google/android/apps/gmm/p/f/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1187
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/f/l;->a:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    .line 1188
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v4, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v4, v4, Lcom/google/android/apps/gmm/p/f/h;->f:Landroid/hardware/Sensor;

    if-ne v0, v4, :cond_2

    .line 1189
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v5, v5, Lcom/google/android/apps/gmm/p/f/h;->g:[F

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    .line 1190
    iget-object v7, v7, Lcom/google/android/apps/gmm/p/f/h;->g:[F

    array-length v7, v7

    .line 1189
    invoke-static {v0, v4, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1212
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v4, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/p/f/h;->b(Landroid/hardware/Sensor;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1213
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v4, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/p/f/h;->a(JLandroid/hardware/Sensor;)V

    .line 1215
    :cond_1
    monitor-exit v1

    :goto_1
    return-void

    .line 1191
    :cond_2
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v4, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v4, v4, Lcom/google/android/apps/gmm/p/f/h;->h:Landroid/hardware/Sensor;

    if-ne v0, v4, :cond_3

    .line 1192
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v5, v5, Lcom/google/android/apps/gmm/p/f/h;->k:[F

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v7, v7, Lcom/google/android/apps/gmm/p/f/h;->k:[F

    array-length v7, v7

    invoke-static {v0, v4, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1193
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/f/h;->k:[F

    iget-object v4, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v4, v4, Lcom/google/android/apps/gmm/p/f/h;->m:[F

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/p/f/h;->a([F[F)V

    .line 1194
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/f/h;->o:J

    goto :goto_0

    .line 1215
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1195
    :cond_3
    :try_start_1
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v4, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v4, v4, Lcom/google/android/apps/gmm/p/f/h;->j:Landroid/hardware/Sensor;

    if-ne v0, v4, :cond_4

    .line 1196
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v5, v5, Lcom/google/android/apps/gmm/p/f/h;->l:[F

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v7, v7, Lcom/google/android/apps/gmm/p/f/h;->l:[F

    array-length v7, v7

    invoke-static {v0, v4, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1197
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/f/h;->l:[F

    iget-object v4, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v4, v4, Lcom/google/android/apps/gmm/p/f/h;->n:[F

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/p/f/h;->a([F[F)V

    .line 1198
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/f/h;->p:J

    goto :goto_0

    .line 1199
    :cond_4
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v4, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v4, v4, Lcom/google/android/apps/gmm/p/f/h;->q:Landroid/hardware/Sensor;

    if-ne v0, v4, :cond_5

    .line 1200
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v5, v5, Lcom/google/android/apps/gmm/p/f/h;->r:[F

    const/4 v6, 0x0

    iget-object v7, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v7, v7

    iget-object v8, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    .line 1201
    iget-object v8, v8, Lcom/google/android/apps/gmm/p/f/h;->r:[F

    array-length v8, v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 1200
    invoke-static {v0, v4, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1202
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v0, v0

    if-ne v0, v9, :cond_0

    .line 1203
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/f/h;->r:[F

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v5, v5, Lcom/google/android/apps/gmm/p/f/h;->r:[F

    invoke-static {v5}, Lcom/google/android/apps/gmm/p/f/h;->a([F)F

    move-result v5

    aput v5, v0, v4

    goto/16 :goto_0

    .line 1205
    :cond_5
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/i;->a:Lcom/google/android/apps/gmm/p/f/h;

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/f/h;->i:Landroid/hardware/Sensor;

    if-ne v0, v2, :cond_6

    .line 1207
    monitor-exit v1

    goto/16 :goto_1

    .line 1209
    :cond_6
    const-string v0, "Sensor event for unknown "

    iget-object v2, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-static {v2}, Lcom/google/android/apps/gmm/p/f/h;->a(Landroid/hardware/Sensor;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 1210
    :goto_2
    monitor-exit v1

    goto/16 :goto_1

    .line 1209
    :cond_7
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method
