.class public Lcom/google/android/apps/gmm/v/ac;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/bc;


# static fields
.field static final a:Lcom/google/android/apps/gmm/v/cn;


# instance fields
.field private b:Lcom/google/android/apps/gmm/v/n;

.field private final c:F

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/concurrent/Semaphore;

.field private final f:Lcom/google/android/apps/gmm/v/cj;

.field private final g:Lcom/google/android/apps/gmm/v/cj;

.field private final h:Lcom/google/android/apps/gmm/v/cn;

.field private final i:Lcom/google/android/apps/gmm/v/cn;

.field private final j:Lcom/google/android/apps/gmm/v/cn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cn;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/v/ac;->a:Lcom/google/android/apps/gmm/v/cn;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/n;F)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ac;->d:Ljava/util/List;

    .line 32
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ac;->e:Ljava/util/concurrent/Semaphore;

    .line 35
    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ac;->f:Lcom/google/android/apps/gmm/v/cj;

    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ac;->g:Lcom/google/android/apps/gmm/v/cj;

    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cn;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ac;->h:Lcom/google/android/apps/gmm/v/cn;

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cn;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ac;->i:Lcom/google/android/apps/gmm/v/cn;

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cn;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ac;->j:Lcom/google/android/apps/gmm/v/cn;

    .line 42
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/ac;->b:Lcom/google/android/apps/gmm/v/n;

    .line 43
    iput p2, p0, Lcom/google/android/apps/gmm/v/ac;->c:F

    .line 44
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 114
    const/4 v0, 0x0

    move v1, v0

    .line 118
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ac;->e:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    if-eqz v1, :cond_0

    .line 128
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 119
    :cond_0
    return-void

    .line 122
    :catch_0
    move-exception v0

    const/4 v0, 0x1

    move v1, v0

    .line 123
    goto :goto_0

    .line 127
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 128
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_1
    throw v0
.end method

.method public final a(FF)V
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v8, 0x3

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    .line 59
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ac;->e:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 61
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ac;->b:Lcom/google/android/apps/gmm/v/n;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v2

    int-to-float v2, v2

    div-float v2, p1, v2

    mul-float/2addr v2, v4

    sub-float/2addr v2, v3

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v1

    int-to-float v1, v1

    div-float v1, p2, v1

    mul-float/2addr v1, v4

    sub-float/2addr v1, v3

    neg-float v1, v1

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ac;->f:Lcom/google/android/apps/gmm/v/cj;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/v/ac;->b:Lcom/google/android/apps/gmm/v/n;

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/n;->G:[F

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Landroid/opengl/Matrix;->invertM([FI[FI)Z

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ac;->h:Lcom/google/android/apps/gmm/v/cn;

    const/high16 v4, 0x3f800000    # 1.0f

    iget-object v5, v3, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v6, 0x0

    aput v2, v5, v6

    iget-object v2, v3, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v5, 0x1

    aput v1, v2, v5

    iget-object v1, v3, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v2, 0x2

    aput v4, v1, v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ac;->f:Lcom/google/android/apps/gmm/v/cj;

    iget-object v7, p0, Lcom/google/android/apps/gmm/v/ac;->i:Lcom/google/android/apps/gmm/v/cn;

    iget-object v5, p0, Lcom/google/android/apps/gmm/v/ac;->h:Lcom/google/android/apps/gmm/v/cn;

    iget-object v1, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v2, 0x3

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v1, v2

    iget-object v1, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v2, 0x0

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v4, 0x0

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    move v1, v10

    :goto_0
    if-ge v1, v8, :cond_0

    iget-object v2, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v3, v2, v1

    iget-object v4, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v5, 0x3

    aget v4, v4, v5

    div-float/2addr v3, v4

    aput v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ac;->i:Lcom/google/android/apps/gmm/v/cn;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/cn;->a()Lcom/google/android/apps/gmm/v/cn;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ac;->g:Lcom/google/android/apps/gmm/v/cj;

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ac;->b:Lcom/google/android/apps/gmm/v/n;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/n;->F:[F

    const/4 v3, 0x0

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v4, 0x0

    const/16 v5, 0x10

    invoke-static {v2, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ac;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    move-object v7, v0

    iget-object v1, v7, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->a:Lcom/google/android/apps/gmm/v/aj;

    iget v2, v2, Lcom/google/android/apps/gmm/v/aj;->o:I

    aget-object v1, v1, v2

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/gmm/v/bb;

    move-object v8, v0

    if-eqz v8, :cond_1

    iget-object v3, v7, Lcom/google/android/apps/gmm/v/aa;->m:Lcom/google/android/apps/gmm/v/cj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ac;->h:Lcom/google/android/apps/gmm/v/cn;

    sget-object v5, Lcom/google/android/apps/gmm/v/ac;->a:Lcom/google/android/apps/gmm/v/cn;

    iget-object v2, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v4, 0x3

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v2, v4

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v2, 0x0

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v4, 0x0

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ac;->g:Lcom/google/android/apps/gmm/v/cj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ac;->j:Lcom/google/android/apps/gmm/v/cn;

    iget-object v5, p0, Lcom/google/android/apps/gmm/v/ac;->h:Lcom/google/android/apps/gmm/v/cn;

    iget-object v2, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v4, 0x3

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v2, v4

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v2, 0x0

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v4, 0x0

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ac;->i:Lcom/google/android/apps/gmm/v/cn;

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ac;->j:Lcom/google/android/apps/gmm/v/cn;

    iget v3, p0, Lcom/google/android/apps/gmm/v/ac;->c:F

    invoke-virtual {v2, v2}, Lcom/google/android/apps/gmm/v/cn;->a(Lcom/google/android/apps/gmm/v/cn;)F

    move-result v4

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/cn;->a(Lcom/google/android/apps/gmm/v/cn;)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-lez v2, :cond_3

    mul-float/2addr v1, v1

    sub-float v1, v4, v1

    mul-float v2, v3, v3

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    move v1, v9

    :goto_1
    if-eqz v1, :cond_1

    invoke-virtual {v8, v7}, Lcom/google/android/apps/gmm/v/bb;->a(Lcom/google/android/apps/gmm/v/aa;)V

    .line 63
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ac;->e:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_2
    return-void

    :cond_3
    move v1, v10

    .line 61
    goto :goto_1

    .line 67
    :catch_0
    move-exception v1

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ac;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ac;->e:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 136
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ac;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 54
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ac;->b:Lcom/google/android/apps/gmm/v/n;

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ac;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 142
    return-void
.end method
