.class public Lcom/google/android/apps/gmm/base/views/CheckableButton;
.super Landroid/widget/Button;
.source "PG"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final b:[I


# instance fields
.field a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/base/views/CheckableButton;->b:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/CheckableButton;->a:Z

    .line 24
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/CheckableButton;->a:Z

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 48
    sget-object v0, Lcom/google/android/apps/gmm/base/views/CheckableButton;->b:[I

    add-int/lit8 v0, p1, 0x1

    .line 49
    invoke-super {p0, v0}, Landroid/widget/Button;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CheckableButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    sget-object v1, Lcom/google/android/apps/gmm/base/views/CheckableButton;->b:[I

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/CheckableButton;->mergeDrawableStates([I[I)[I

    .line 53
    :cond_0
    return-object v0
.end method

.method public setChecked(Z)V
    .locals 0

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/views/CheckableButton;->a:Z

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CheckableButton;->refreshDrawableState()V

    .line 35
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/CheckableButton;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/CheckableButton;->setChecked(Z)V

    .line 40
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
