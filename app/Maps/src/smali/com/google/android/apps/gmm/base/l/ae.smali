.class public Lcom/google/android/apps/gmm/base/l/ae;
.super Lcom/google/android/apps/gmm/base/l/l;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Lcom/google/android/apps/gmm/base/activities/c;

.field private final d:Lcom/google/android/libraries/curvular/ag;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/base/l/j;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/google/android/apps/gmm/map/s/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/gmm/base/l/ae;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/l/ae;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/libraries/curvular/ag;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/activities/c;",
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/base/l/j;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    sget-object v2, Lcom/google/android/apps/gmm/base/l/n;->a:Lcom/google/android/apps/gmm/base/l/n;

    sget-object v3, Lcom/google/android/apps/gmm/base/l/k;->b:Lcom/google/android/apps/gmm/base/l/k;

    sget v4, Lcom/google/android/apps/gmm/f;->cR:I

    const-string v5, ""

    const/4 v6, 0x0

    const/4 v7, 0x1

    sget v8, Lcom/google/android/apps/gmm/g;->bN:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/base/l/l;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/l/n;Lcom/google/android/apps/gmm/base/l/k;ILjava/lang/String;Lcom/google/android/apps/gmm/z/b/l;ZI)V

    .line 33
    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/ae;->e:Lcom/google/android/apps/gmm/map/s/a;

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/ae;->c:Lcom/google/android/apps/gmm/base/activities/c;

    .line 41
    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/ae;->e:Lcom/google/android/apps/gmm/map/s/a;

    .line 42
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/l/ae;->d:Lcom/google/android/libraries/curvular/ag;

    .line 43
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ae;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/mylocation/e/c;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/mylocation/e/c;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 58
    const/4 v0, 0x0

    return-object v0
.end method

.method public declared-synchronized a(Lcom/google/android/apps/gmm/map/s/b;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/s/b;->a:Lcom/google/android/apps/gmm/map/s/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/ae;->e:Lcom/google/android/apps/gmm/map/s/a;

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ae;->d:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    monitor-exit p0

    return-void

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/aw;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 67
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/ae;->e:Lcom/google/android/apps/gmm/map/s/a;

    if-nez v1, :cond_0

    .line 68
    sget-object v1, Lcom/google/android/apps/gmm/base/l/ae;->b:Ljava/lang/String;

    const-string v2, "AutoPan mode should not be null."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    :goto_0
    return-object v0

    .line 72
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/base/l/af;->a:[I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/ae;->e:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/s/a;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 75
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/f;->cR:I

    sget v1, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    goto :goto_0

    .line 78
    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/f;->cR:I

    sget v1, Lcom/google/android/apps/gmm/d;->as:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    goto :goto_0

    .line 81
    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/f;->cN:I

    sget v1, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final g()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ae;->e:Lcom/google/android/apps/gmm/map/s/a;

    if-nez v0, :cond_0

    .line 94
    sget-object v0, Lcom/google/android/apps/gmm/base/l/ae;->b:Ljava/lang/String;

    const-string v1, "AutoPan mode should not be null."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    const-string v0, ""

    .line 111
    :goto_0
    return-object v0

    .line 98
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/l/af;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/ae;->e:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/s/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 110
    sget-object v0, Lcom/google/android/apps/gmm/base/l/ae;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/ae;->e:Lcom/google/android/apps/gmm/map/s/a;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled autoPan mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    const-string v0, ""

    goto :goto_0

    .line 101
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ae;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->v:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 104
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ae;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->aa:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 107
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ae;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->x:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final h()Lcom/google/android/apps/gmm/z/b/l;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 120
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/ae;->e:Lcom/google/android/apps/gmm/map/s/a;

    if-nez v1, :cond_0

    .line 121
    sget-object v1, Lcom/google/android/apps/gmm/base/l/ae;->b:Ljava/lang/String;

    const-string v2, "AutoPan mode should not be null."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    :goto_0
    return-object v0

    .line 125
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/base/l/af;->a:[I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/ae;->e:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/s/a;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 140
    sget-object v1, Lcom/google/android/apps/gmm/base/l/ae;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/ae;->e:Lcom/google/android/apps/gmm/map/s/a;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unhandled autoPan mode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 128
    :pswitch_0
    sget-object v0, Lcom/google/b/f/t;->bu:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 132
    :pswitch_1
    sget-object v0, Lcom/google/b/f/t;->by:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 136
    :pswitch_2
    sget-object v0, Lcom/google/b/f/t;->bv:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
