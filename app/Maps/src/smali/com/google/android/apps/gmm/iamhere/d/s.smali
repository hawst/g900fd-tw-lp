.class Lcom/google/android/apps/gmm/iamhere/d/s;
.super Lcom/google/android/apps/gmm/util/webimageview/k;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/iamhere/c/a;

.field final synthetic b:Landroid/support/v4/app/ax;

.field final synthetic c:Landroid/support/v4/app/av;

.field final synthetic d:Lcom/google/android/apps/gmm/iamhere/d/p;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/iamhere/d/p;Lcom/google/android/apps/gmm/iamhere/c/a;Landroid/support/v4/app/ax;Landroid/support/v4/app/av;)V
    .locals 0

    .prologue
    .line 498
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/d/s;->d:Lcom/google/android/apps/gmm/iamhere/d/p;

    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/d/s;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    iput-object p3, p0, Lcom/google/android/apps/gmm/iamhere/d/s;->b:Landroid/support/v4/app/ax;

    iput-object p4, p0, Lcom/google/android/apps/gmm/iamhere/d/s;->c:Landroid/support/v4/app/av;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/util/webimageview/k;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 501
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/p;->a:Ljava/lang/String;

    const-string v0, "Image loaded for "

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/s;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/iamhere/c/a;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 503
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/s;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/s;->d:Lcom/google/android/apps/gmm/iamhere/d/p;

    iget-object v1, v1, Lcom/google/android/apps/gmm/iamhere/d/p;->d:Lcom/google/android/apps/gmm/iamhere/c/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/iamhere/c/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/s;->b:Landroid/support/v4/app/ax;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/s;->c:Landroid/support/v4/app/av;

    invoke-virtual {v1, p1}, Landroid/support/v4/app/av;->a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/av;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ax;->a(Landroid/support/v4/app/bi;)Landroid/support/v4/app/ax;

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/s;->d:Lcom/google/android/apps/gmm/iamhere/d/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/s;->b:Landroid/support/v4/app/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/d/p;->c:Landroid/app/NotificationManager;

    sget v2, Lcom/google/android/apps/gmm/iamhere/d/p;->b:I

    invoke-virtual {v1}, Landroid/support/v4/app/ax;->a()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 508
    :cond_0
    return-void

    .line 501
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
