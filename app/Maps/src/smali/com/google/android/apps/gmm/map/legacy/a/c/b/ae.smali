.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;


# static fields
.field private static a:I

.field private static b:I

.field private static c:F

.field private static d:F


# instance fields
.field private e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

.field private f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

.field private g:Z

.field private final h:I

.field private final i:Lcom/google/android/apps/gmm/map/b/a/y;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 56
    const/16 v0, 0x4000

    sput v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a:I

    .line 69
    const/4 v0, 0x1

    sput v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->b:I

    .line 75
    sput v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->c:F

    .line 78
    sput v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->d:F

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;II)V
    .locals 3

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    const/16 v0, 0xa

    const/4 v1, 0x0

    .line 255
    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 254
    invoke-static {p2, v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 256
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 257
    iput p3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->h:I

    .line 258
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/bp;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "[",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/internal/c/cp;",
            "Lcom/google/android/apps/gmm/v/ad;",
            "Lcom/google/android/apps/gmm/map/t/k;",
            "Lcom/google/android/apps/gmm/v/bp;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;"
        }
    .end annotation

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v5

    .line 120
    const/4 v2, 0x0

    .line 121
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 123
    const/4 v1, -0x1

    move v3, v1

    move v4, v2

    .line 126
    :goto_0
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 127
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v2

    .line 128
    instance-of v1, v2, Lcom/google/android/apps/gmm/map/internal/c/af;

    if-eqz v1, :cond_2

    .line 129
    const/4 v1, 0x1

    if-gt v3, v1, :cond_6

    .line 130
    const/4 v3, 0x1

    move-object v1, v2

    .line 133
    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/af;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/af;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v1, v1

    div-int/lit8 v1, v1, 0x3

    .line 134
    sget v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a:I

    if-le v1, v7, :cond_0

    .line 136
    const-string v2, "GLLineMesh"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    sget v8, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a:I

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x5a

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "unable to handle the LineMesh with "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, " vertices in "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " since the limit is "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v2, v1, v7}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    goto :goto_0

    .line 141
    :cond_0
    add-int v7, v1, v4

    sget v8, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a:I

    if-gt v7, v8, :cond_6

    .line 142
    add-int/2addr v1, v4

    .line 145
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v11, v3

    move v3, v1

    move v1, v11

    .line 175
    :goto_1
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/c/m;->j()[I

    move-result-object v4

    array-length v7, v4

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v7, :cond_5

    aget v8, v4, v2

    .line 176
    if-ltz v8, :cond_1

    array-length v9, p1

    if-ge v8, v9, :cond_1

    .line 177
    aget-object v8, p1, v8

    move-object/from16 v0, p7

    invoke-interface {v0, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 175
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 146
    :cond_2
    instance-of v1, v2, Lcom/google/android/apps/gmm/map/internal/c/ad;

    if-eqz v1, :cond_6

    move-object v1, v2

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/ad;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a(Lcom/google/android/apps/gmm/map/internal/c/ad;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 147
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/c/m;->g()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v1

    const/4 v7, 0x0

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v1, v1, v7

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->b(F)I

    move-result v1

    .line 148
    if-eq v1, v3, :cond_3

    .line 149
    if-gtz v3, :cond_6

    move v3, v1

    :cond_3
    move-object v1, v2

    .line 155
    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/ad;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v1, v1

    div-int/lit8 v1, v1, 0x3

    add-int/lit8 v1, v1, -0x1

    shl-int/lit8 v1, v1, 0x1

    .line 156
    sget v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a:I

    if-le v1, v7, :cond_4

    .line 158
    const-string v2, "GLLineMesh"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    sget v8, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a:I

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x56

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "unable to handle the Line with "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, " vertices in "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " since the limit is "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v2, v1, v7}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    goto/16 :goto_0

    .line 163
    :cond_4
    add-int v7, v1, v4

    sget v8, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a:I

    if-gt v7, v8, :cond_6

    .line 164
    add-int/2addr v1, v4

    .line 167
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v11, v3

    move v3, v1

    move v1, v11

    goto/16 :goto_1

    .line 180
    :cond_5
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    move v4, v3

    move v3, v1

    .line 181
    goto/16 :goto_0

    :cond_6
    move v1, v3

    .line 183
    if-gez v1, :cond_7

    .line 184
    const/4 v1, 0x1

    .line 188
    :cond_7
    new-instance v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;

    invoke-direct {v2, p0, v4, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;II)V

    .line 189
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/m;

    .line 190
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/c/m;->h()I

    move-result v4

    const/4 v7, 0x5

    if-ne v4, v7, :cond_9

    .line 191
    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/af;

    invoke-direct {v2, v5, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a(Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/map/internal/c/af;)V

    goto :goto_3

    .line 192
    :cond_9
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/c/m;->h()I

    move-result v4

    const/16 v7, 0x8

    if-ne v4, v7, :cond_8

    .line 193
    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/ad;

    invoke-direct {v2, v5, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a(Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/map/internal/c/ad;)V

    goto :goto_3

    .line 196
    :cond_a
    new-instance v3, Lcom/google/android/apps/gmm/map/t/as;

    const/4 v1, 0x0

    .line 200
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/m;

    invoke-direct {v3, v1}, Lcom/google/android/apps/gmm/map/t/as;-><init>(Lcom/google/android/apps/gmm/map/internal/c/m;)V

    .line 196
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k()I

    move-result v1

    if-lez v1, :cond_d

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v1

    iput-object v1, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    new-instance v1, Lcom/google/android/apps/gmm/map/t/ar;

    const/4 v4, 0x1

    move-object/from16 v0, p4

    invoke-direct {v1, v0, p0, v3, v4}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v4, v4, Lcom/google/android/apps/gmm/v/av;->b:I

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x17

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "LineMesh "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "   "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    move-object/from16 v0, p5

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    iget-boolean v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->g:Z

    if-eqz v3, :cond_b

    iget-object v3, p3, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v4, 0x302

    const/16 v5, 0x303

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    :cond_b
    new-instance v3, Lcom/google/android/apps/gmm/v/bd;

    iget v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->h:I

    int-to-float v4, v4

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/v/bd;-><init>(F)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    invoke-virtual {v1, p0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    const/4 v3, 0x0

    iget-boolean v4, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v4, :cond_c

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_c
    int-to-byte v3, v3

    iput-byte v3, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    move-object/from16 v0, p6

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_d
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    const/4 v1, 0x0

    iput-object v1, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 203
    return-object v2
.end method

.method public static declared-synchronized a(F)V
    .locals 4

    .prologue
    .line 241
    const-class v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;

    monitor-enter v1

    :try_start_0
    sput p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->d:F

    .line 242
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v2, 0x5

    sget v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->b:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    sget v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->d:F

    div-float/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->c:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    monitor-exit v1

    return-void

    .line 241
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(I)V
    .locals 4

    .prologue
    .line 231
    const-class v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;

    monitor-enter v1

    :try_start_0
    sput p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->b:I

    .line 232
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v2, 0x5

    sget v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->b:I

    .line 233
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    sget v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->d:F

    div-float/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->c:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    monitor-exit v1

    return-void

    .line 231
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/map/internal/c/ad;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 298
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/internal/c/ad;->f:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 299
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v1, :cond_1

    move v1, v0

    :goto_0
    if-eqz v1, :cond_0

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_0

    .line 300
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    if-nez v1, :cond_2

    .line 329
    :cond_0
    :goto_1
    return-void

    .line 299
    :cond_1
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v1, v1

    goto :goto_0

    .line 305
    :cond_2
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    .line 306
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->g:Z

    if-nez v2, :cond_3

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    const/16 v3, 0xff

    if-ge v2, v3, :cond_3

    .line 307
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->g:Z

    .line 310
    :cond_3
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/internal/c/ad;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 311
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v3, v3

    div-int/lit8 v3, v3, 0x3

    add-int/lit8 v3, v3, -0x1

    .line 316
    iget-object v4, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 317
    :goto_2
    if-gt v0, v3, :cond_5

    .line 318
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v0, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 319
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v5, v4, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 320
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 321
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v5, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b(I)V

    .line 322
    if-lez v0, :cond_4

    if-ge v0, v3, :cond_4

    .line 323
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 324
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v5, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b(I)V

    .line 317
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 328
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/map/internal/c/af;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 265
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/internal/c/af;->b:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 266
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v1, :cond_1

    move v1, v0

    :goto_0
    if-eqz v1, :cond_0

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_0

    .line 267
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    if-nez v1, :cond_2

    .line 291
    :cond_0
    :goto_1
    return-void

    .line 266
    :cond_1
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v1, v1

    goto :goto_0

    .line 272
    :cond_2
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    .line 273
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->g:Z

    if-nez v2, :cond_3

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    const/16 v3, 0xff

    if-ge v2, v3, :cond_3

    .line 274
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->g:Z

    .line 277
    :cond_3
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/internal/c/af;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 278
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v3, v3

    div-int/lit8 v3, v3, 0x3

    .line 283
    iget-object v4, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 284
    :goto_2
    if-ge v0, v3, :cond_4

    .line 285
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v0, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 286
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v5, v4, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 287
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 288
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v5, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b(I)V

    .line 284
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 290
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    goto :goto_1
.end method

.method public static declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/c/ad;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 218
    const-class v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;

    monitor-enter v4

    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->f:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 219
    iget-object v2, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v2, :cond_0

    move v2, v1

    :goto_0
    if-lez v2, :cond_1

    const/4 v2, 0x0

    .line 220
    iget-object v3, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v2, v3, v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    array-length v2, v2

    if-eqz v2, :cond_1

    move v3, v0

    .line 221
    :goto_1
    iget-object v2, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v2, :cond_2

    move v2, v1

    :goto_2
    if-ne v2, v0, :cond_3

    const/4 v2, 0x0

    .line 222
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v2, v5, v2

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    sget v5, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->c:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmpg-float v2, v2, v5

    if-gtz v2, :cond_3

    if-nez v3, :cond_3

    :goto_3
    monitor-exit v4

    return v0

    .line 219
    :cond_0
    :try_start_1
    iget-object v2, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v2, v2

    goto :goto_0

    :cond_1
    move v3, v1

    .line 220
    goto :goto_1

    .line 221
    :cond_2
    iget-object v2, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v2, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :cond_3
    move v0, v1

    .line 222
    goto :goto_3

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method

.method private static declared-synchronized b(F)I
    .locals 4

    .prologue
    .line 250
    const-class v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    sget v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->b:I

    sget v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->d:F

    mul-float/2addr v3, p0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 362
    const/16 v0, 0x78

    return v0
.end method
