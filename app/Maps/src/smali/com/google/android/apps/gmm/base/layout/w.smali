.class public final enum Lcom/google/android/apps/gmm/base/layout/w;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/base/layout/w;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/base/layout/w;

.field public static final enum b:Lcom/google/android/apps/gmm/base/layout/w;

.field public static final enum c:Lcom/google/android/apps/gmm/base/layout/w;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/base/layout/w;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 210
    new-instance v0, Lcom/google/android/apps/gmm/base/layout/w;

    const-string v1, "DISABLE_MAP_RENDERING"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/base/layout/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/layout/w;->a:Lcom/google/android/apps/gmm/base/layout/w;

    .line 215
    new-instance v0, Lcom/google/android/apps/gmm/base/layout/w;

    const-string v1, "ENABLE_MAP_RENDERING"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/base/layout/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/layout/w;->b:Lcom/google/android/apps/gmm/base/layout/w;

    .line 221
    new-instance v0, Lcom/google/android/apps/gmm/base/layout/w;

    const-string v1, "DISABLE_IF_NO_MAP_VISIBLE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/base/layout/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/layout/w;->c:Lcom/google/android/apps/gmm/base/layout/w;

    .line 206
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/base/layout/w;

    sget-object v1, Lcom/google/android/apps/gmm/base/layout/w;->a:Lcom/google/android/apps/gmm/base/layout/w;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/base/layout/w;->b:Lcom/google/android/apps/gmm/base/layout/w;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/base/layout/w;->c:Lcom/google/android/apps/gmm/base/layout/w;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/base/layout/w;->d:[Lcom/google/android/apps/gmm/base/layout/w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 206
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/base/layout/w;
    .locals 1

    .prologue
    .line 206
    const-class v0, Lcom/google/android/apps/gmm/base/layout/w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/layout/w;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/base/layout/w;
    .locals 1

    .prologue
    .line 206
    sget-object v0, Lcom/google/android/apps/gmm/base/layout/w;->d:[Lcom/google/android/apps/gmm/base/layout/w;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/base/layout/w;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/base/layout/w;

    return-object v0
.end method
