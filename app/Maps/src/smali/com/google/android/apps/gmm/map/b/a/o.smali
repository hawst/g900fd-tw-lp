.class public Lcom/google/android/apps/gmm/map/b/a/o;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method private static a(Lcom/google/b/e/ab;[II)I
    .locals 4

    .prologue
    .line 48
    const/4 v0, 0x0

    .line 49
    :goto_0
    invoke-virtual {p0}, Lcom/google/b/e/ab;->available()I

    move-result v1

    if-lez v1, :cond_0

    .line 50
    add-int v1, p2, v0

    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v2

    ushr-int/lit8 v3, v2, 0x1

    and-int/lit8 v2, v2, 0x1

    neg-int v2, v2

    xor-int/2addr v2, v3

    aput v2, p1, v1

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_0
    return v0
.end method

.method public static a([BIIIII)[I
    .locals 4

    .prologue
    .line 21
    new-array v0, p5, [I

    .line 22
    const/4 v1, 0x0

    new-instance v2, Lcom/google/b/e/ab;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, p0, p1, p2}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    invoke-direct {v2, v3}, Lcom/google/b/e/ab;-><init>(Ljava/io/InputStream;)V

    packed-switch p3, :pswitch_data_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x24

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown vertex encoding :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/o;->a(Lcom/google/b/e/ab;[II)I

    .line 23
    :goto_0
    return-object v0

    .line 22
    :pswitch_1
    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/o;->b(Lcom/google/b/e/ab;[II)I

    goto :goto_0

    :pswitch_2
    const/4 v3, 0x3

    if-ne p4, v3, :cond_0

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/o;->d(Lcom/google/b/e/ab;[II)I

    goto :goto_0

    :cond_0
    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/o;->c(Lcom/google/b/e/ab;[II)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static b(Lcom/google/b/e/ab;[II)I
    .locals 3

    .prologue
    .line 58
    const/4 v0, 0x0

    .line 59
    :goto_0
    invoke-virtual {p0}, Lcom/google/b/e/ab;->available()I

    move-result v1

    if-lez v1, :cond_0

    .line 60
    add-int v1, p2, v0

    invoke-virtual {p0}, Lcom/google/b/e/ab;->readShort()S

    move-result v2

    aput v2, p1, v1

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :cond_0
    return v0
.end method

.method private static c(Lcom/google/b/e/ab;[II)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 68
    move v1, v0

    move v2, v0

    .line 71
    :goto_0
    invoke-virtual {p0}, Lcom/google/b/e/ab;->available()I

    move-result v3

    if-lez v3, :cond_0

    .line 72
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v3

    ushr-int/lit8 v4, v3, 0x1

    and-int/lit8 v3, v3, 0x1

    neg-int v3, v3

    xor-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 73
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v3

    ushr-int/lit8 v4, v3, 0x1

    and-int/lit8 v3, v3, 0x1

    neg-int v3, v3

    xor-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 74
    add-int v3, p2, v0

    aput v2, p1, v3

    .line 75
    add-int v3, p2, v0

    add-int/lit8 v3, v3, 0x1

    aput v1, p1, v3

    .line 76
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 78
    :cond_0
    return v0
.end method

.method private static d(Lcom/google/b/e/ab;[II)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 84
    move v1, v0

    move v2, v0

    move v3, v0

    .line 88
    :goto_0
    invoke-virtual {p0}, Lcom/google/b/e/ab;->available()I

    move-result v4

    if-lez v4, :cond_0

    .line 89
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    ushr-int/lit8 v5, v4, 0x1

    and-int/lit8 v4, v4, 0x1

    neg-int v4, v4

    xor-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 90
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    ushr-int/lit8 v5, v4, 0x1

    and-int/lit8 v4, v4, 0x1

    neg-int v4, v4

    xor-int/2addr v4, v5

    add-int/2addr v2, v4

    .line 91
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    ushr-int/lit8 v5, v4, 0x1

    and-int/lit8 v4, v4, 0x1

    neg-int v4, v4

    xor-int/2addr v4, v5

    add-int/2addr v1, v4

    .line 92
    add-int v4, p2, v0

    aput v3, p1, v4

    .line 93
    add-int v4, p2, v0

    add-int/lit8 v4, v4, 0x1

    aput v2, p1, v4

    .line 94
    add-int v4, p2, v0

    add-int/lit8 v4, v4, 0x2

    aput v1, p1, v4

    .line 95
    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    .line 97
    :cond_0
    return v0
.end method
