.class public Lcom/google/android/apps/gmm/base/views/WebImageView;
.super Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 34
    .line 35
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v0

    .line 34
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/google/android/apps/gmm/util/webimageview/j;)V

    .line 36
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/base/views/WebImageView;Lcom/google/android/apps/gmm/util/webimageview/f;Landroid/graphics/drawable/TransitionDrawable;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setTransitionDrawable(Lcom/google/android/apps/gmm/util/webimageview/f;Landroid/graphics/drawable/TransitionDrawable;)V

    return-void
.end method


# virtual methods
.method protected onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 41
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->onSizeChanged(IIII)V

    .line 42
    return-void
.end method

.method protected final setFadingImage(Lcom/google/android/apps/gmm/util/webimageview/f;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/WebImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/views/bg;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/gmm/base/views/bg;-><init>(Lcom/google/android/apps/gmm/base/views/WebImageView;Lcom/google/android/apps/gmm/util/webimageview/f;Landroid/graphics/Bitmap;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 64
    return-void
.end method
