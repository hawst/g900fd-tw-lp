.class public Lcom/google/android/apps/gmm/j/a;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/j/a/a;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 56
    const-class v0, Lcom/google/android/apps/gmm/j/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/j/a;->a:Ljava/lang/String;

    .line 67
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.category.LAUNCHER"

    .line 68
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.apps.plus"

    const-string v2, "com.google.android.apps.plus.phone.HomeActivity"

    .line 69
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 47
    return-void
.end method

.method private a(ILcom/google/android/apps/gmm/j/g;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 111
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    .line 114
    sget-object v1, Lcom/google/android/apps/gmm/j/e;->a:[I

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/j/g;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 144
    :goto_0
    return-void

    .line 116
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/l;->lV:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/gmm/j/a;->a(ILjava/lang/Runnable;)V

    goto :goto_0

    .line 119
    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/l;->kg:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/gmm/j/a;->a(ILjava/lang/Runnable;)V

    goto :goto_0

    .line 122
    :pswitch_2
    sget v1, Lcom/google/android/apps/gmm/l;->jK:I

    new-instance v2, Lcom/google/android/apps/gmm/j/b;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/gmm/j/b;-><init>(Lcom/google/android/apps/gmm/j/a;Lcom/google/android/apps/gmm/base/activities/c;)V

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/gmm/j/a;->a(ILjava/lang/Runnable;)V

    goto :goto_0

    .line 135
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/j/e;->a:[I

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/j/g;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 143
    :pswitch_3
    invoke-static {p3}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->a(Landroid/os/Bundle;)Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    goto :goto_0

    .line 137
    :pswitch_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->C()Lcom/google/android/apps/gmm/photo/a/a;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/google/android/apps/gmm/photo/a/a;->b(Landroid/os/Bundle;)V

    goto :goto_0

    .line 140
    :pswitch_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->C()Lcom/google/android/apps/gmm/photo/a/a;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/google/android/apps/gmm/photo/a/a;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 135
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(ILjava/lang/Runnable;)V
    .locals 3
    .param p2    # Ljava/lang/Runnable;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 241
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 242
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 243
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->jE:I

    new-instance v2, Lcom/google/android/apps/gmm/j/d;

    invoke-direct {v2, p0, p2}, Lcom/google/android/apps/gmm/j/d;-><init>(Lcom/google/android/apps/gmm/j/a;Ljava/lang/Runnable;)V

    .line 244
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 252
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 253
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/j/a;ILcom/google/android/apps/gmm/j/g;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/j/a;->a(ILcom/google/android/apps/gmm/j/g;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/j/a;Lcom/google/android/apps/gmm/j/g;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/c/a;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->i:Lcom/google/android/apps/gmm/util/b;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/apps/gmm/j/f;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/j/f;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/util/b;->a(ZLcom/google/android/apps/gmm/util/g;Lcom/google/android/apps/gmm/util/f;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/j/a;->b:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/apps/gmm/j/a;->b:Landroid/os/Bundle;

    const-string v2, "pendingAction"

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/j/g;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/j/a;->b:Landroid/os/Bundle;

    const-string v2, "fragmentArgs"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    sget-object v0, Lcom/google/android/apps/gmm/j/a;->a:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/a/a;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/l/al;->e:Lcom/google/android/apps/gmm/l/al;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/l/al;->ordinal()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/j/g;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 178
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 180
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    const/4 v0, -0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/gmm/j/a;->a(ILcom/google/android/apps/gmm/j/g;Landroid/os/Bundle;)V

    .line 205
    :goto_0
    return-void

    .line 185
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/common/a/a;->a:[Ljava/lang/String;

    new-instance v3, Lcom/google/android/apps/gmm/j/c;

    invoke-direct {v3, p0, p1, p2}, Lcom/google/android/apps/gmm/j/c;-><init>(Lcom/google/android/apps/gmm/j/a;Lcom/google/android/apps/gmm/j/g;Landroid/os/Bundle;)V

    const/4 v4, 0x0

    .line 184
    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/gms/common/a/a;->a(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/j/a;->b:Landroid/os/Bundle;

    const-string v1, "pendingAction"

    .line 97
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/j/g;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/j/g;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lcom/google/android/apps/gmm/j/a;->b:Landroid/os/Bundle;

    const-string v2, "fragmentArgs"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 100
    sget-object v2, Lcom/google/android/apps/gmm/j/a;->a:Ljava/lang/String;

    const-string v2, "Returned from G+ signin flow; starting action: "

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/j/g;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 101
    :goto_0
    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/gmm/j/a;->a(ILcom/google/android/apps/gmm/j/g;Landroid/os/Bundle;)V

    .line 102
    return-void

    .line 100
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 80
    if-eqz p1, :cond_0

    .line 81
    const-string v0, "gplusSavedData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/j/a;->b:Landroid/os/Bundle;

    .line 83
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 87
    const-string v0, "gplusSavedData"

    iget-object v1, p0, Lcom/google/android/apps/gmm/j/a;->b:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 88
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 152
    sget-object v0, Lcom/google/android/apps/gmm/j/g;->b:Lcom/google/android/apps/gmm/j/g;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/j/a;->a(Lcom/google/android/apps/gmm/j/g;Landroid/os/Bundle;)V

    .line 153
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 157
    sget-object v0, Lcom/google/android/apps/gmm/j/g;->a:Lcom/google/android/apps/gmm/j/g;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/j/a;->a(Lcom/google/android/apps/gmm/j/g;Landroid/os/Bundle;)V

    .line 158
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 162
    sget-object v0, Lcom/google/android/apps/gmm/j/g;->c:Lcom/google/android/apps/gmm/j/g;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/j/a;->a(Lcom/google/android/apps/gmm/j/g;Landroid/os/Bundle;)V

    .line 163
    return-void
.end method
