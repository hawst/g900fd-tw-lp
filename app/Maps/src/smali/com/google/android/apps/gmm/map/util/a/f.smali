.class public Lcom/google/android/apps/gmm/map/util/a/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Key:",
        "Ljava/lang/Object;",
        "Value:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/google/android/apps/gmm/map/util/a/g",
        "<TKey;TValue;>;>;"
    }
.end annotation


# instance fields
.field a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "La/a/a/a/d/ax",
            "<TKey;TValue;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(La/a/a/a/d/am;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/a/a/a/d/am",
            "<TKey;TValue;>;)V"
        }
    .end annotation

    .prologue
    .line 340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341
    iget-object v0, p1, La/a/a/a/d/am;->i:La/a/a/a/d/ba;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/d/at;

    invoke-direct {v0, p1}, La/a/a/a/d/at;-><init>(La/a/a/a/d/am;)V

    iput-object v0, p1, La/a/a/a/d/am;->i:La/a/a/a/d/ba;

    :cond_0
    iget-object v0, p1, La/a/a/a/d/am;->i:La/a/a/a/d/ba;

    invoke-interface {v0}, La/a/a/a/d/ba;->c()La/a/a/a/d/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/f;->a:Ljava/util/Iterator;

    .line 342
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/util/a/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/gmm/map/util/a/g",
            "<TKey;TValue;>;"
        }
    .end annotation

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/f;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/a/a/d/ax;

    .line 352
    new-instance v1, Lcom/google/android/apps/gmm/map/util/a/g;

    .line 353
    invoke-interface {v0}, La/a/a/a/d/ax;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, La/a/a/a/d/ax;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/map/util/a/g;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 354
    return-object v1
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/f;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/util/a/f;->a()Lcom/google/android/apps/gmm/map/util/a/g;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 359
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
