.class public Lcom/google/android/apps/gmm/p/e/n;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Lcom/google/android/apps/gmm/p/d/d;

.field static final b:D

.field static final c:D


# instance fields
.field final d:Lcom/google/android/apps/gmm/shared/c/f;

.field e:J

.field f:Lcom/google/android/apps/gmm/p/e/g;

.field g:Lcom/google/android/apps/gmm/map/r/b/a;

.field h:Lcom/google/android/apps/gmm/p/d/d;

.field final i:Lcom/google/android/apps/gmm/shared/net/a/l;

.field private j:Ljava/util/Random;

.field private final k:Lcom/google/android/apps/gmm/p/e/a/c;

.field private final l:Lcom/google/android/apps/gmm/p/e/s;

.field private final m:Lcom/google/android/apps/gmm/map/util/a/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/i",
            "<",
            "Lcom/google/android/apps/gmm/p/e/q;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/e/q;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcom/google/android/apps/gmm/map/r/a/ae;

.field private p:J

.field private q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const-wide v6, 0x402d555555555556L    # 14.666666666666668

    .line 107
    new-instance v0, Lcom/google/android/apps/gmm/p/d/d;

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x4004000000000000L    # 2.5

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/p/d/d;-><init>(DD)V

    sput-object v0, Lcom/google/android/apps/gmm/p/e/n;->a:Lcom/google/android/apps/gmm/p/d/d;

    .line 132
    const/16 v0, 0x14

    int-to-double v0, v0

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/p/e/n;->b:D

    .line 135
    const/4 v0, 0x2

    int-to-double v0, v0

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/p/e/n;->c:D

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/p/e/a/c;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/n;->j:Ljava/util/Random;

    .line 144
    new-instance v0, Lcom/google/android/apps/gmm/p/e/s;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/p/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/n;->l:Lcom/google/android/apps/gmm/p/e/s;

    .line 149
    new-instance v0, Lcom/google/android/apps/gmm/p/e/o;

    const/16 v1, 0x258

    const-string v2, "ParticlePool"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/p/e/o;-><init>(ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/n;->m:Lcom/google/android/apps/gmm/map/util/a/i;

    .line 152
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/n;->n:Ljava/util/List;

    .line 155
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/ae;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/n;->o:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 158
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/n;->p:J

    .line 168
    iput-object v3, p0, Lcom/google/android/apps/gmm/p/e/n;->f:Lcom/google/android/apps/gmm/p/e/g;

    .line 173
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/e/n;->q:Z

    .line 176
    iput-object v3, p0, Lcom/google/android/apps/gmm/p/e/n;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 188
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/n;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 190
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/n;->i:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 191
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/e/n;->k:Lcom/google/android/apps/gmm/p/e/a/c;

    .line 192
    return-void
.end method

.method private static a(Ljava/util/List;)Lcom/google/android/apps/gmm/p/d/d;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/e/q;",
            ">;)",
            "Lcom/google/android/apps/gmm/p/d/d;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 516
    .line 517
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v2

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/e/q;

    .line 518
    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v3, v3, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/16 v5, 0x10

    and-int/2addr v3, v5

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_0

    .line 519
    if-nez v1, :cond_2

    .line 520
    new-instance v1, Lcom/google/android/apps/gmm/p/e/p;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/p/e/p;-><init>(Lcom/google/android/apps/gmm/p/e/q;)V

    goto :goto_0

    .line 518
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 522
    :cond_2
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/p/e/p;->a(Lcom/google/android/apps/gmm/p/e/q;)V

    goto :goto_0

    .line 526
    :cond_3
    if-nez v1, :cond_4

    :goto_2
    return-object v2

    :cond_4
    new-instance v2, Lcom/google/android/apps/gmm/p/d/d;

    iget-wide v0, v1, Lcom/google/android/apps/gmm/p/e/p;->d:D

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-direct {v2, v0, v1, v4, v5}, Lcom/google/android/apps/gmm/p/d/d;-><init>(DD)V

    goto :goto_2
.end method

.method private static a(Ljava/util/List;D[DLjava/util/List;)Lcom/google/android/apps/gmm/p/e/p;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/e/q;",
            ">;D[D",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/b/b;",
            ">;)",
            "Lcom/google/android/apps/gmm/p/e/p;"
        }
    .end annotation

    .prologue
    .line 461
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 464
    :cond_1
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v7

    .line 465
    const/4 v4, 0x0

    .line 466
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/q;

    .line 467
    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    invoke-interface {v7, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/p/e/p;

    .line 468
    if-nez v3, :cond_3

    .line 469
    new-instance v5, Lcom/google/android/apps/gmm/p/e/p;

    invoke-direct {v5, v2}, Lcom/google/android/apps/gmm/p/e/p;-><init>(Lcom/google/android/apps/gmm/p/e/q;)V

    .line 473
    :goto_2
    if-eqz v4, :cond_2

    iget-wide v8, v5, Lcom/google/android/apps/gmm/p/e/p;->a:D

    iget-wide v10, v4, Lcom/google/android/apps/gmm/p/e/p;->a:D

    cmpl-double v3, v8, v10

    if-lez v3, :cond_b

    :cond_2
    move-object v3, v5

    .line 476
    :goto_3
    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    invoke-interface {v7, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v3

    .line 477
    goto :goto_1

    .line 471
    :cond_3
    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/p/e/p;->a(Lcom/google/android/apps/gmm/p/e/q;)V

    move-object v5, v3

    goto :goto_2

    .line 484
    :cond_4
    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-object v6, v4

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/google/android/apps/gmm/p/e/a/g;

    .line 485
    invoke-interface {v7, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/google/android/apps/gmm/p/e/p;

    .line 486
    iget-object v2, v3, Lcom/google/android/apps/gmm/p/e/a/g;->h:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/a/e;

    .line 487
    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/a/e;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    .line 488
    invoke-interface {v7, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 489
    new-instance v5, Lcom/google/android/apps/gmm/p/e/p;

    .line 490
    invoke-interface {v7, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/p;

    invoke-direct {v5, v4, v2}, Lcom/google/android/apps/gmm/p/e/p;-><init>(Lcom/google/android/apps/gmm/p/e/p;Lcom/google/android/apps/gmm/p/e/p;)V

    .line 491
    iget-wide v12, v5, Lcom/google/android/apps/gmm/p/e/p;->a:D

    iget-wide v14, v6, Lcom/google/android/apps/gmm/p/e/p;->a:D

    cmpl-double v2, v12, v14

    if-lez v2, :cond_9

    move-object v2, v5

    .line 495
    :goto_6
    new-instance v6, Lcom/google/android/apps/gmm/map/r/b/b;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/p/e/p;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v9

    iget-wide v12, v5, Lcom/google/android/apps/gmm/p/e/p;->a:D

    div-double v12, v12, p1

    double-to-float v11, v12

    iget-object v5, v5, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget v5, v5, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    invoke-direct {v6, v9, v11, v5}, Lcom/google/android/apps/gmm/map/r/b/b;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;FF)V

    move-object/from16 v0, p4

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_7
    move-object v6, v2

    .line 497
    goto :goto_5

    .line 499
    :cond_5
    const/4 v2, 0x0

    :goto_8
    move-object/from16 v0, p3

    array-length v5, v0

    if-ge v2, v5, :cond_7

    .line 500
    aget-wide v12, p3, v2

    iget-object v5, v3, Lcom/google/android/apps/gmm/p/e/a/g;->i:[Z

    aget-boolean v5, v5, v2

    if-eqz v5, :cond_6

    iget-wide v8, v4, Lcom/google/android/apps/gmm/p/e/p;->a:D

    :goto_9
    add-double/2addr v8, v12

    aput-wide v8, p3, v2

    .line 499
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 500
    :cond_6
    const-wide/16 v8, 0x0

    goto :goto_9

    .line 503
    :cond_7
    new-instance v2, Lcom/google/android/apps/gmm/map/r/b/b;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/p/e/p;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    iget-wide v8, v4, Lcom/google/android/apps/gmm/p/e/p;->a:D

    div-double v8, v8, p1

    double-to-float v5, v8

    iget-object v4, v4, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget v4, v4, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    invoke-direct {v2, v3, v5, v4}, Lcom/google/android/apps/gmm/map/r/b/b;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;FF)V

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 505
    :cond_8
    return-object v6

    :cond_9
    move-object v2, v6

    goto :goto_6

    :cond_a
    move-object v2, v6

    goto :goto_7

    :cond_b
    move-object v3, v4

    goto/16 :goto_3
.end method

.method private a(Landroid/location/Location;Ljava/util/List;)Ljava/util/List;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/location/Location;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/e/a/g;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/e/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392
    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    .line 393
    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 394
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    if-ltz v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 395
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/google/android/apps/gmm/p/e/a/g;

    .line 396
    iget-object v2, v3, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v8, v3, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v9, 0x1

    invoke-static {v2, v8, v4, v9, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;ZLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 397
    iget-object v2, v3, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v8, v3, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v2, v8, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    float-to-double v8, v2

    .line 398
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const-wide v12, 0x3f747ae147ae147bL    # 0.005

    .line 399
    iget-object v2, v3, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v14, v3, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v14}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    float-to-double v14, v2

    iget-object v2, v3, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v0, v2

    move-wide/from16 v16, v0

    const-wide v18, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double v16, v16, v18

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->exp(D)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->atan(D)D

    move-result-wide v16

    const-wide v20, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double v16, v16, v20

    mul-double v16, v16, v18

    const-wide v18, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v16

    div-double v14, v14, v16

    mul-double/2addr v12, v14

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(DD)D

    move-result-wide v10

    .line 401
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/n;->m:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/q;

    .line 402
    iget-object v12, v3, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v13, v3, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v12, v13}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v12

    float-to-double v12, v12

    iget-object v14, v3, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v14, v14, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v14, v14

    const-wide v16, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double v14, v14, v16

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    invoke-static {v14, v15}, Ljava/lang/Math;->exp(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->atan(D)D

    move-result-wide v14

    const-wide v18, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double v14, v14, v18

    mul-double v14, v14, v16

    const-wide v16, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double v14, v14, v16

    invoke-static {v14, v15}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v14

    div-double/2addr v12, v14

    mul-double/2addr v8, v12

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getSpeed()F

    move-result v12

    float-to-double v12, v12

    .line 401
    iput-wide v10, v2, Lcom/google/android/apps/gmm/p/e/q;->a:D

    iput-object v3, v2, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iput-wide v8, v2, Lcom/google/android/apps/gmm/p/e/q;->c:D

    iput-wide v12, v2, Lcom/google/android/apps/gmm/p/e/q;->d:D

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/p/e/q;->a()V

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 404
    :cond_2
    return-object v6
.end method

.method private static a(Lcom/google/android/apps/gmm/map/util/a/i;Ljava/util/Random;Ljava/util/List;ID)Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/util/a/i",
            "<",
            "Lcom/google/android/apps/gmm/p/e/q;",
            ">;",
            "Ljava/util/Random;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/e/q;",
            ">;ID)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/e/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 424
    move/from16 v0, p3

    int-to-double v2, v0

    div-double v6, p4, v2

    .line 426
    invoke-virtual/range {p1 .. p1}, Ljava/util/Random;->nextDouble()D

    move-result-wide v2

    mul-double/2addr v2, v6

    .line 427
    if-ltz p3, :cond_0

    const/4 v4, 0x1

    :goto_0
    if-nez v4, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 428
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-wide v4, v2

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/google/android/apps/gmm/p/e/q;

    .line 430
    iget-wide v10, v3, Lcom/google/android/apps/gmm/p/e/q;->a:D

    sub-double/2addr v4, v10

    .line 433
    :goto_2
    const-wide/16 v10, 0x0

    cmpg-double v2, v4, v10

    if-gez v2, :cond_2

    .line 434
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/q;

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    iget-object v12, v3, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-wide v14, v3, Lcom/google/android/apps/gmm/p/e/q;->c:D

    iget-wide v0, v3, Lcom/google/android/apps/gmm/p/e/q;->d:D

    move-wide/from16 v16, v0

    iput-wide v10, v2, Lcom/google/android/apps/gmm/p/e/q;->a:D

    iput-object v12, v2, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iput-wide v14, v2, Lcom/google/android/apps/gmm/p/e/q;->c:D

    move-wide/from16 v0, v16

    iput-wide v0, v2, Lcom/google/android/apps/gmm/p/e/q;->d:D

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/p/e/q;->a()V

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 435
    add-double/2addr v4, v6

    goto :goto_2

    .line 438
    :cond_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z

    goto :goto_1

    .line 440
    :cond_3
    return-object v8
.end method


# virtual methods
.method declared-synchronized a(JLcom/google/android/apps/gmm/p/e/g;Lcom/google/android/apps/gmm/map/r/b/a;)Lcom/google/android/apps/gmm/map/r/b/a;
    .locals 27

    .prologue
    .line 261
    monitor-enter p0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 262
    :try_start_0
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/gmm/p/e/n;->p:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-eqz v6, :cond_2a

    .line 263
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/p/e/n;->p:J

    sub-long v4, p1, v4

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    move-wide v14, v4

    .line 265
    :goto_0
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/gmm/p/e/n;->p:J

    .line 269
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v4

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    const-wide/high16 v6, 0x4049000000000000L    # 50.0

    .line 271
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-int v5, v6

    .line 268
    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/y;I)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v4

    .line 272
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/n;->k:Lcom/google/android/apps/gmm/p/e/a/c;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/p/e/a/c;->a(Lcom/google/android/apps/gmm/map/b/a/ae;)Ljava/util/List;

    move-result-object v16

    .line 273
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    .line 380
    :goto_1
    monitor-exit p0

    return-object p4

    .line 278
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/n;->n:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/p/e/q;

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/e/n;->j:Ljava/util/Random;

    move-object/from16 v18, v0

    sget-object v10, Lcom/google/android/apps/gmm/p/e/n;->a:Lcom/google/android/apps/gmm/p/d/d;

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    move-wide v8, v6

    :goto_3
    const/16 v6, 0x14

    if-ge v5, v6, :cond_2

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    invoke-virtual/range {v18 .. v18}, Ljava/util/Random;->nextDouble()D

    move-result-wide v12

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    cmpg-double v11, v12, v20

    if-gez v11, :cond_1

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    :cond_1
    add-double/2addr v6, v8

    add-int/lit8 v5, v5, 0x1

    move-wide v8, v6

    goto :goto_3

    :cond_2
    sget-wide v6, Lcom/google/android/apps/gmm/p/d/d;->a:D

    div-double v6, v8, v6

    iget-wide v8, v10, Lcom/google/android/apps/gmm/p/d/d;->c:D

    mul-double/2addr v6, v8

    iget-wide v8, v10, Lcom/google/android/apps/gmm/p/d/d;->b:D

    add-double/2addr v6, v8

    mul-double/2addr v6, v14

    iget-wide v8, v4, Lcom/google/android/apps/gmm/p/e/q;->c:D

    const-wide/16 v10, 0x0

    iget-wide v12, v4, Lcom/google/android/apps/gmm/p/e/q;->d:D

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    mul-double v20, v20, v6

    add-double v12, v12, v20

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->max(DD)D

    move-result-wide v10

    mul-double/2addr v10, v14

    add-double/2addr v8, v10

    iput-wide v8, v4, Lcom/google/android/apps/gmm/p/e/q;->c:D

    const-wide/16 v8, 0x0

    iget-wide v10, v4, Lcom/google/android/apps/gmm/p/e/q;->d:D

    add-double/2addr v6, v10

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    iput-wide v6, v4, Lcom/google/android/apps/gmm/p/e/q;->d:D

    :goto_4
    iget-wide v6, v4, Lcom/google/android/apps/gmm/p/e/q;->c:D

    iget-object v5, v4, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v8, v5, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v9, v5, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v8

    float-to-double v8, v8

    iget-object v5, v5, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v10, v5

    const-wide v12, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v10, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v10, v11}, Ljava/lang/Math;->exp(D)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->atan(D)D

    move-result-wide v10

    const-wide v20, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double v10, v10, v20

    mul-double/2addr v10, v12

    const-wide v12, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v10

    div-double/2addr v8, v10

    cmpl-double v5, v6, v8

    if-lez v5, :cond_c

    iget-object v5, v4, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v5, v5, Lcom/google/android/apps/gmm/p/e/a/g;->h:Ljava/util/ArrayList;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v4, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v6, v5, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v7, v5, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v6

    float-to-double v6, v6

    iget-object v5, v5, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v8, v5

    const-wide v10, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9}, Ljava/lang/Math;->exp(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->atan(D)D

    move-result-wide v8

    const-wide v12, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v8, v12

    mul-double/2addr v8, v10

    const-wide v10, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v8

    div-double/2addr v6, v8

    iput-wide v6, v4, Lcom/google/android/apps/gmm/p/e/q;->c:D

    const-wide/16 v6, 0x0

    iput-wide v6, v4, Lcom/google/android/apps/gmm/p/e/q;->d:D
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 261
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 279
    :cond_3
    :try_start_2
    iget-wide v6, v4, Lcom/google/android/apps/gmm/p/e/q;->c:D

    iget-object v5, v4, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v8, v5, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v9, v5, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v8

    float-to-double v8, v8

    iget-object v5, v5, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v10, v5

    const-wide v12, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v10, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v10, v11}, Ljava/lang/Math;->exp(D)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->atan(D)D

    move-result-wide v10

    const-wide v20, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double v10, v10, v20

    mul-double/2addr v10, v12

    const-wide v12, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v10

    div-double/2addr v8, v10

    sub-double/2addr v6, v8

    iput-wide v6, v4, Lcom/google/android/apps/gmm/p/e/q;->c:D

    iget-object v0, v4, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    move-object/from16 v19, v0

    sget-wide v8, Lcom/google/android/apps/gmm/p/e/n;->b:D

    sget-wide v10, Lcom/google/android/apps/gmm/p/e/n;->c:D

    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/a/g;->h:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x0

    :goto_5
    iput-object v5, v4, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    goto/16 :goto_4

    :cond_4
    const-wide/16 v6, 0x0

    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/a/g;->h:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    move-wide v12, v6

    :goto_6
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/gmm/p/e/a/e;

    iget-object v5, v5, Lcom/google/android/apps/gmm/p/e/a/e;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-boolean v6, v5, Lcom/google/android/apps/gmm/p/e/a/g;->l:Z

    if-eqz v6, :cond_5

    move-wide v6, v8

    :goto_7
    add-double/2addr v6, v12

    move-wide v12, v6

    goto :goto_6

    :cond_5
    iget-boolean v5, v5, Lcom/google/android/apps/gmm/p/e/a/g;->m:Z

    if-eqz v5, :cond_6

    move-wide v6, v10

    goto :goto_7

    :cond_6
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    goto :goto_7

    :cond_7
    invoke-virtual/range {v18 .. v18}, Ljava/util/Random;->nextDouble()D

    move-result-wide v6

    mul-double/2addr v6, v12

    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/a/g;->h:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    move-wide v12, v6

    :goto_8
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/gmm/p/e/a/e;

    iget-object v6, v5, Lcom/google/android/apps/gmm/p/e/a/e;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-boolean v7, v6, Lcom/google/android/apps/gmm/p/e/a/g;->l:Z

    if-eqz v7, :cond_8

    move-wide v6, v8

    :goto_9
    sub-double v6, v12, v6

    const-wide/16 v12, 0x0

    cmpg-double v12, v6, v12

    if-gtz v12, :cond_a

    iget-object v5, v5, Lcom/google/android/apps/gmm/p/e/a/e;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    goto :goto_5

    :cond_8
    iget-boolean v6, v6, Lcom/google/android/apps/gmm/p/e/a/g;->m:Z

    if-eqz v6, :cond_9

    move-wide v6, v10

    goto :goto_9

    :cond_9
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    goto :goto_9

    :cond_a
    move-wide v12, v6

    goto :goto_8

    :cond_b
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    :cond_c
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/p/e/q;->a()V

    goto/16 :goto_2

    .line 285
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/p/e/n;->q:Z

    if-eqz v4, :cond_e

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/g;->b:Lcom/google/android/apps/gmm/p/d/d;

    if-eqz v4, :cond_f

    const/4 v4, 0x1

    :goto_a
    if-eqz v4, :cond_e

    .line 286
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/n;->n:Ljava/util/List;

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/p/e/n;->a(Landroid/location/Location;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 287
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/p/e/n;->q:Z

    .line 294
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/n;->n:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 295
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/p/e/n;->q:Z

    goto/16 :goto_1

    .line 285
    :cond_f
    const/4 v4, 0x0

    goto :goto_a

    .line 301
    :cond_10
    const-wide/16 v8, 0x0

    .line 302
    const-wide/16 v4, 0x0

    .line 303
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/p/e/n;->n:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move-wide v14, v4

    :goto_b
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1e

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/p/e/q;

    .line 304
    iget-wide v0, v4, Lcom/google/android/apps/gmm/p/e/q;->a:D

    move-wide/from16 v18, v0

    iget-object v5, v4, Lcom/google/android/apps/gmm/p/e/q;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-wide v0, v4, Lcom/google/android/apps/gmm/p/e/q;->d:D

    move-wide/from16 v20, v0

    iget-object v6, v4, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget v6, v6, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    float-to-double v6, v6

    iget-object v0, v4, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    move-object/from16 v17, v0

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-virtual {v0, v5, v1}, Lcom/google/android/apps/gmm/p/e/g;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/p/e/a/g;)D

    move-result-wide v22

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/g;->d:Lcom/google/android/apps/gmm/p/d/d;

    if-eqz v5, :cond_13

    const/4 v5, 0x1

    :goto_c
    if-eqz v5, :cond_14

    double-to-float v5, v6

    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/google/android/apps/gmm/p/e/g;->d:Lcom/google/android/apps/gmm/p/d/d;

    iget-wide v6, v6, Lcom/google/android/apps/gmm/p/d/d;->b:D

    double-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x43340000    # 180.0f

    cmpl-float v6, v5, v6

    if-lez v6, :cond_11

    const/high16 v6, 0x43b40000    # 360.0f

    sub-float v5, v6, v5

    :cond_11
    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/google/android/apps/gmm/p/e/g;->d:Lcom/google/android/apps/gmm/p/d/d;

    float-to-double v10, v5

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/g;->d:Lcom/google/android/apps/gmm/p/d/d;

    iget-wide v12, v5, Lcom/google/android/apps/gmm/p/d/d;->b:D

    sub-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(D)D

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Lcom/google/android/apps/gmm/p/d/d;->a(D)D

    move-result-wide v6

    move-wide v12, v6

    :goto_d
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/g;->e:Ljava/lang/Boolean;

    if-eqz v5, :cond_12

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/g;->e:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_17

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/16 v10, 0x10

    and-int/2addr v5, v10

    if-eqz v5, :cond_15

    const/4 v5, 0x1

    :goto_e
    if-eqz v5, :cond_16

    const-wide v10, 0x3fa999999999999aL    # 0.05

    :goto_f
    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/g;->g:Lcom/google/android/apps/gmm/p/d/a;

    if-eqz v5, :cond_12

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/g;->g:Lcom/google/android/apps/gmm/p/d/a;

    move-wide/from16 v0, v20

    invoke-interface {v5, v0, v1}, Lcom/google/android/apps/gmm/p/d/a;->a(D)D

    move-result-wide v6

    :cond_12
    :goto_10
    mul-double v12, v12, v22

    mul-double/2addr v10, v12

    mul-double/2addr v6, v10

    mul-double v6, v6, v18

    iput-wide v6, v4, Lcom/google/android/apps/gmm/p/e/q;->a:D

    .line 305
    iget-wide v6, v4, Lcom/google/android/apps/gmm/p/e/q;->a:D

    add-double/2addr v8, v6

    .line 306
    iget-object v5, v4, Lcom/google/android/apps/gmm/p/e/q;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v6, v4, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    .line 307
    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/g;->b:Lcom/google/android/apps/gmm/p/d/d;

    if-eqz v4, :cond_1c

    const/4 v4, 0x1

    :goto_11
    if-eqz v4, :cond_1d

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/gmm/p/e/g;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/p/e/a/g;)D

    move-result-wide v4

    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/google/android/apps/gmm/p/e/g;->b:Lcom/google/android/apps/gmm/p/d/d;

    const-wide/16 v10, 0x0

    invoke-virtual {v6, v10, v11}, Lcom/google/android/apps/gmm/p/d/d;->a(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    .line 306
    :goto_12
    invoke-static {v14, v15, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    move-wide v14, v4

    .line 308
    goto/16 :goto_b

    .line 304
    :cond_13
    const/4 v5, 0x0

    goto/16 :goto_c

    :cond_14
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    move-wide v12, v6

    goto :goto_d

    :cond_15
    const/4 v5, 0x0

    goto :goto_e

    :cond_16
    const-wide v10, 0x3feccccccccccccdL    # 0.9

    goto :goto_f

    :cond_17
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/16 v10, 0x10

    and-int/2addr v5, v10

    if-eqz v5, :cond_18

    const/4 v5, 0x1

    :goto_13
    if-eqz v5, :cond_19

    const-wide v10, 0x3fa999999999999aL    # 0.05

    :goto_14
    sub-double v10, v24, v10

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/16 v17, 0x10

    and-int v5, v5, v17

    if-eqz v5, :cond_1a

    const/4 v5, 0x1

    :goto_15
    if-nez v5, :cond_1b

    sget-object v5, Lcom/google/android/apps/gmm/p/e/g;->a:Lcom/google/android/apps/gmm/p/d/d;

    move-wide/from16 v0, v20

    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/gmm/p/d/d;->a(D)D

    move-result-wide v6

    goto :goto_10

    :cond_18
    const/4 v5, 0x0

    goto :goto_13

    :cond_19
    const-wide v10, 0x3feccccccccccccdL    # 0.9

    goto :goto_14

    :cond_1a
    const/4 v5, 0x0

    goto :goto_15

    :cond_1b
    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/g;->f:Lcom/google/android/apps/gmm/p/d/d;

    if-eqz v5, :cond_12

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/g;->f:Lcom/google/android/apps/gmm/p/d/d;

    move-wide/from16 v0, v20

    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/gmm/p/d/d;->a(D)D

    move-result-wide v6

    goto/16 :goto_10

    .line 307
    :cond_1c
    const/4 v4, 0x0

    goto :goto_11

    :cond_1d
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    goto :goto_12

    .line 311
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/n;->o:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v4

    new-array v11, v4, [D

    .line 312
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 313
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/n;->n:Ljava/util/List;

    .line 314
    invoke-static {v4, v8, v9, v11, v12}, Lcom/google/android/apps/gmm/p/e/n;->a(Ljava/util/List;D[DLjava/util/List;)Lcom/google/android/apps/gmm/p/e/p;

    move-result-object v13

    .line 315
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/n;->n:Ljava/util/List;

    invoke-static {v4}, Lcom/google/android/apps/gmm/p/e/n;->a(Ljava/util/List;)Lcom/google/android/apps/gmm/p/d/d;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/gmm/p/e/n;->h:Lcom/google/android/apps/gmm/p/d/d;

    .line 317
    invoke-virtual {v13}, Lcom/google/android/apps/gmm/p/e/p;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v10

    .line 320
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/n;->m:Lcom/google/android/apps/gmm/map/util/a/i;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/n;->j:Ljava/util/Random;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/p/e/n;->n:Ljava/util/List;

    const/16 v7, 0x1f4

    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/gmm/p/e/n;->a(Lcom/google/android/apps/gmm/map/util/a/i;Ljava/util/Random;Ljava/util/List;ID)Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/gmm/p/e/n;->n:Ljava/util/List;

    .line 323
    new-instance v4, Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/r/b/c;-><init>()V

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/r/b/c;->a(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v6

    .line 324
    iget-object v4, v13, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v4, v4, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget-object v5, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v5, :cond_1f

    new-instance v5, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v5, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_1f
    iget-object v5, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-object v4, v5, Lcom/google/android/apps/gmm/map/r/b/e;->b:Lcom/google/android/apps/gmm/map/internal/c/az;

    if-nez v4, :cond_24

    const/4 v4, 0x0

    :goto_16
    iput-object v4, v5, Lcom/google/android/apps/gmm/map/r/b/e;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 326
    const/4 v4, 0x1

    iget-object v5, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v5, :cond_20

    new-instance v5, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v5, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_20
    iget-object v5, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-boolean v4, v5, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    .line 347
    iget v4, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v4

    const-wide v14, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v14

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v16, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double v4, v4, v16

    mul-double/2addr v4, v14

    const-wide v14, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v14

    invoke-virtual {v10}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v14

    invoke-virtual {v6, v4, v5, v14, v15}, Lcom/google/android/apps/gmm/map/r/b/c;->a(DD)Lcom/google/android/apps/gmm/map/r/b/c;

    .line 348
    iget-object v4, v13, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget v4, v4, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    iput v4, v6, Lcom/google/android/apps/gmm/map/r/b/c;->c:F

    const/4 v4, 0x1

    iput-boolean v4, v6, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    .line 349
    iget-wide v4, v13, Lcom/google/android/apps/gmm/p/e/p;->d:D

    double-to-float v4, v4

    iput v4, v6, Lcom/google/android/apps/gmm/map/r/b/c;->h:F

    const/4 v4, 0x1

    iput-boolean v4, v6, Lcom/google/android/apps/gmm/map/r/b/c;->t:Z

    .line 352
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/n;->l:Lcom/google/android/apps/gmm/p/e/s;

    iget-object v5, v13, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/gmm/p/e/s;->a(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/map/r/b/c;)V

    .line 359
    const/4 v4, 0x1

    iput-boolean v4, v6, Lcom/google/android/apps/gmm/map/r/b/c;->v:Z

    move-wide/from16 v0, p1

    iput-wide v0, v6, Lcom/google/android/apps/gmm/map/r/b/c;->j:J

    .line 360
    iget-object v4, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v4, :cond_21

    new-instance v4, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v4, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_21
    iget-object v4, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-object v12, v4, Lcom/google/android/apps/gmm/map/r/b/e;->d:Ljava/util/List;

    .line 362
    const/4 v4, 0x0

    move v5, v4

    :goto_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/n;->o:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v4

    if-ge v5, v4, :cond_25

    .line 363
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/n;->o:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/r/a/w;

    .line 364
    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    .line 365
    iget v7, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v14, v7

    const-wide v16, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double v14, v14, v16

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    invoke-static {v14, v15}, Ljava/lang/Math;->exp(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->atan(D)D

    move-result-wide v14

    const-wide v18, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double v14, v14, v18

    mul-double v14, v14, v16

    const-wide v16, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double v14, v14, v16

    invoke-static {v14, v15}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    invoke-virtual {v4, v10, v12, v13}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/b/a/y;D)Lcom/google/android/apps/gmm/map/r/a/ad;

    move-result-object v7

    .line 366
    iget-object v12, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v12, :cond_22

    new-instance v12, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v12}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v12, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_22
    iget-object v12, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v12, v12, Lcom/google/android/apps/gmm/map/r/b/e;->e:Ljava/util/Map;

    invoke-interface {v12, v4, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    aget-wide v12, v11, v5

    div-double/2addr v12, v8

    iget-object v7, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v7, :cond_23

    new-instance v7, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v7, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_23
    iget-object v7, v6, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/r/b/e;->f:Ljava/util/Map;

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    invoke-interface {v7, v4, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_17

    :cond_24
    move-object v4, v10

    .line 324
    goto/16 :goto_16

    .line 372
    :cond_25
    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/g;->b:Lcom/google/android/apps/gmm/p/d/d;

    if-eqz v4, :cond_27

    const/4 v4, 0x1

    :goto_18
    if-eqz v4, :cond_28

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v10, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v4

    float-to-double v4, v4

    iget v7, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v8, v7

    const-wide v10, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9}, Ljava/lang/Math;->exp(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->atan(D)D

    move-result-wide v8

    const-wide v12, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v8, v12

    mul-double/2addr v8, v10

    const-wide v10, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v8

    div-double/2addr v4, v8

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/android/apps/gmm/p/e/g;->b:Lcom/google/android/apps/gmm/p/d/d;

    iget-wide v8, v7, Lcom/google/android/apps/gmm/p/d/d;->c:D

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    mul-double/2addr v8, v10

    cmpg-double v4, v4, v8

    if-gtz v4, :cond_28

    const/4 v4, 0x1

    :goto_19
    if-nez v4, :cond_26

    .line 373
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/p/e/n;->q:Z

    .line 377
    :cond_26
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/gmm/p/e/n;->f:Lcom/google/android/apps/gmm/p/e/g;

    .line 378
    iget-object v4, v6, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v4, :cond_29

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "latitude and longitude must be set"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 372
    :cond_27
    const/4 v4, 0x0

    goto :goto_18

    :cond_28
    const/4 v4, 0x0

    goto :goto_19

    .line 378
    :cond_29
    new-instance v4, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v4, v6}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/gmm/p/e/n;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 380
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/e/n;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    move-object/from16 p4, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    :cond_2a
    move-wide v14, v4

    goto/16 :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/h;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->LOCATION_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 201
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/n;->o:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/n;->k:Lcom/google/android/apps/gmm/p/e/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/n;->o:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/p/e/a/c;->a(Lcom/google/android/apps/gmm/map/r/a/ae;)V

    .line 203
    return-void
.end method
