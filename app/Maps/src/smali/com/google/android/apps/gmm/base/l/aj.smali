.class public Lcom/google/android/apps/gmm/base/l/aj;
.super Lcom/google/android/apps/gmm/base/l/b;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/activities/c;

.field private final h:Lcom/google/android/apps/gmm/z/b/l;

.field private final i:Lcom/google/android/apps/gmm/z/b/l;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/l/b;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/aj;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 28
    sget-object v0, Lcom/google/android/apps/gmm/base/l/c;->a:Lcom/google/android/apps/gmm/base/l/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->g:Lcom/google/android/apps/gmm/base/l/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 29
    :cond_0
    sget-object v0, Lcom/google/b/f/t;->bq:Lcom/google/b/f/t;

    .line 30
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/aj;->h:Lcom/google/android/apps/gmm/z/b/l;

    .line 31
    sget-object v0, Lcom/google/b/f/t;->k:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/aj;->i:Lcom/google/android/apps/gmm/z/b/l;

    .line 33
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 65
    sget v0, Lcom/google/android/apps/gmm/f;->em:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/aj;->h:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public c()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/aj;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v0

    .line 43
    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/a/f;->j()V

    .line 44
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 60
    sget v0, Lcom/google/android/apps/gmm/f;->eP:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 81
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/b;->e:Lcom/google/android/apps/gmm/x/o;

    .line 82
    if-eqz v1, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/aj;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v2, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    .line 84
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/x/o;)V

    .line 89
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/aj;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v1, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/b;->d:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, ""

    .line 86
    :cond_1
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/aq;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final w()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/aj;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->W:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final x()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 49
    .line 50
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/b;->f:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/gmm/f;->en:I

    .line 49
    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    return-object v0

    .line 50
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/f;->eo:I

    goto :goto_0
.end method

.method public final y()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/aj;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->ix:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final z()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/aj;->i:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method
