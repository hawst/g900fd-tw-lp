.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;
.implements Lcom/google/android/apps/gmm/map/o/ak;


# static fields
.field private static final ap:Z

.field static r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field public static t:Lcom/google/android/apps/gmm/map/util/a;

.field public static u:Landroid/graphics/Paint;


# instance fields
.field private A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

.field private B:Lcom/google/android/apps/gmm/v/bp;

.field private final C:[F

.field private D:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;",
            ">;"
        }
    .end annotation
.end field

.field private E:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;",
            ">;"
        }
    .end annotation
.end field

.field private G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;",
            ">;"
        }
    .end annotation
.end field

.field private H:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;",
            ">;"
        }
    .end annotation
.end field

.field private I:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;",
            ">;"
        }
    .end annotation
.end field

.field private J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;",
            ">;"
        }
    .end annotation
.end field

.field private K:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;",
            ">;"
        }
    .end annotation
.end field

.field private L:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;",
            ">;"
        }
    .end annotation
.end field

.field private M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;",
            ">;"
        }
    .end annotation
.end field

.field private N:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;",
            ">;"
        }
    .end annotation
.end field

.field private P:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/ay;",
            ">;"
        }
    .end annotation
.end field

.field private volatile Q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/as;",
            ">;"
        }
    .end annotation
.end field

.field private R:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/as;",
            ">;"
        }
    .end annotation
.end field

.field private volatile S:Lcom/google/android/apps/gmm/map/o/bk;

.field private final T:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field private final U:Lcom/google/android/apps/gmm/map/b/a/ae;

.field private final V:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final W:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final X:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/i;",
            ">;"
        }
    .end annotation
.end field

.field private Y:I

.field private Z:Z

.field public a:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field private aa:Z

.field private ab:Lcom/google/android/apps/gmm/map/t/ar;

.field private final ac:[F

.field private volatile ad:I

.field private ae:J

.field private af:J

.field private ag:J

.field private ah:Lcom/google/android/apps/gmm/map/t/b;

.field private ai:Z

.field private aj:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

.field private ak:I

.field private final al:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final am:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;"
        }
    .end annotation
.end field

.field private an:F

.field private ao:Lcom/google/android/apps/gmm/v/ax;

.field private final aq:Z

.field private ar:Lcom/google/android/apps/gmm/v/ck;

.field private as:Lcom/google/android/apps/gmm/map/indoor/c/r;

.field private final at:F

.field public b:Lcom/google/android/apps/gmm/map/t/am;

.field public c:Lcom/google/android/apps/gmm/map/t/e;

.field public d:Lcom/google/android/apps/gmm/map/t/e;

.field public final e:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field public f:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public g:I

.field public h:J

.field public i:I

.field public j:Z

.field public k:Lcom/google/android/apps/gmm/map/t/q;

.field public l:Lcom/google/android/apps/gmm/map/t/q;

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:F

.field public final q:Lcom/google/android/apps/gmm/v/ad;

.field public s:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/t/p;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcom/google/android/apps/gmm/map/t/g;

.field private w:Lcom/google/android/apps/gmm/map/t/g;

.field private x:Lcom/google/android/apps/gmm/map/t/am;

.field private y:Lcom/google/android/apps/gmm/map/t/am;

.field private z:Lcom/google/android/apps/gmm/v/bp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 414
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->r:Ljava/util/ArrayList;

    .line 435
    sget-object v0, Lcom/google/android/apps/gmm/map/util/a;->a:Lcom/google/android/apps/gmm/map/util/a;

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->t:Lcom/google/android/apps/gmm/map/util/a;

    .line 448
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ap:Z

    .line 767
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->u:Landroid/graphics/Paint;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/internal/c/bo;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v0, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/ac;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 158
    new-instance v1, Lcom/google/android/apps/gmm/map/t/g;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/t/g;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->v:Lcom/google/android/apps/gmm/map/t/g;

    .line 163
    new-instance v1, Lcom/google/android/apps/gmm/map/t/g;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/t/g;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->w:Lcom/google/android/apps/gmm/map/t/g;

    .line 168
    new-instance v1, Lcom/google/android/apps/gmm/map/t/am;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/t/am;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->b:Lcom/google/android/apps/gmm/map/t/am;

    .line 173
    new-instance v1, Lcom/google/android/apps/gmm/map/t/am;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/t/am;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->x:Lcom/google/android/apps/gmm/map/t/am;

    .line 178
    new-instance v1, Lcom/google/android/apps/gmm/map/t/am;

    const/4 v2, 0x2

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/map/t/am;-><init>(II)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->y:Lcom/google/android/apps/gmm/map/t/am;

    .line 187
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    .line 194
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    .line 200
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    .line 203
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->c:Lcom/google/android/apps/gmm/map/t/e;

    .line 206
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->d:Lcom/google/android/apps/gmm/map/t/e;

    .line 213
    const/4 v1, 0x4

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->C:[F

    .line 233
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->N:Ljava/util/List;

    .line 234
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->O:Ljava/util/List;

    .line 291
    sget-boolean v1, Lcom/google/android/apps/gmm/map/util/c;->d:Z

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->Z:Z

    .line 303
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aa:Z

    .line 308
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->k:Lcom/google/android/apps/gmm/map/t/q;

    .line 313
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ab:Lcom/google/android/apps/gmm/map/t/ar;

    .line 318
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->l:Lcom/google/android/apps/gmm/map/t/q;

    .line 323
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->m:Z

    .line 329
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->n:Z

    .line 332
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->o:Z

    .line 341
    const/4 v1, 0x3

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ac:[F

    .line 354
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ad:I

    .line 361
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ae:J

    .line 368
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->af:J

    .line 375
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ag:J

    .line 380
    sget-object v1, Lcom/google/android/apps/gmm/map/t/b;->f:Lcom/google/android/apps/gmm/map/t/b;

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ah:Lcom/google/android/apps/gmm/map/t/b;

    .line 401
    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ak:I

    .line 406
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->al:Ljava/util/List;

    .line 411
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    .line 419
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->s:Ljava/util/Set;

    .line 424
    iput v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->an:F

    .line 429
    new-instance v1, Lcom/google/android/apps/gmm/v/ax;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/v/ax;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ao:Lcom/google/android/apps/gmm/v/ax;

    .line 779
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 780
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->T:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 781
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->U:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 782
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    .line 783
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->W:Ljava/util/HashSet;

    .line 784
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->X:Ljava/util/HashSet;

    .line 786
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    .line 787
    sget-boolean v1, Lcom/google/android/apps/gmm/map/util/c;->d:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    .line 790
    :cond_0
    iput-boolean v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    .line 792
    new-instance v1, Lcom/google/android/apps/gmm/map/t/e;

    const v2, 0x3a83126f    # 0.001f

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/t/e;-><init>(F)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->c:Lcom/google/android/apps/gmm/map/t/e;

    .line 793
    new-instance v1, Lcom/google/android/apps/gmm/map/t/e;

    invoke-direct {v1, v7}, Lcom/google/android/apps/gmm/map/t/e;-><init>(F)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->d:Lcom/google/android/apps/gmm/map/t/e;

    .line 795
    new-instance v1, Lcom/google/android/apps/gmm/v/ck;

    new-instance v2, Lcom/google/android/apps/gmm/v/a/c;

    new-instance v3, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v3, v6, v6, v7}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    new-instance v4, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v4, v6, v6, v6}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/v/a/c;-><init>(Lcom/google/android/apps/gmm/v/cn;Lcom/google/android/apps/gmm/v/cn;)V

    invoke-direct {v1, v2, p2}, Lcom/google/android/apps/gmm/v/ck;-><init>(Lcom/google/android/apps/gmm/v/c;Lcom/google/android/apps/gmm/v/ad;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ar:Lcom/google/android/apps/gmm/v/ck;

    .line 798
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ar:Lcom/google/android/apps/gmm/v/ck;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->d:Lcom/google/android/apps/gmm/map/t/e;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ck;->a(Lcom/google/android/apps/gmm/v/cl;)V

    .line 799
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ar:Lcom/google/android/apps/gmm/v/ck;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->c:Lcom/google/android/apps/gmm/map/t/e;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ck;->a(Lcom/google/android/apps/gmm/v/cl;)V

    .line 801
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/bv;->b:Lcom/google/android/apps/gmm/map/internal/c/bv;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v1

    aget-object v1, v2, v1

    if-eqz v1, :cond_1

    .line 802
    new-instance v1, Lcom/google/android/apps/gmm/map/indoor/c/r;

    .line 803
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/internal/c/v;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)F

    move-result v2

    invoke-direct {v1, v2, p2}, Lcom/google/android/apps/gmm/map/indoor/c/r;-><init>(FLcom/google/android/apps/gmm/v/ad;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->as:Lcom/google/android/apps/gmm/map/indoor/c/r;

    .line 804
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->y:Lcom/google/android/apps/gmm/map/t/am;

    invoke-virtual {v1, v7}, Lcom/google/android/apps/gmm/map/t/am;->a(F)V

    .line 807
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->u:Landroid/graphics/Paint;

    if-nez v1, :cond_2

    .line 808
    const/4 v1, -0x1

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v1, 0x41b00000    # 22.0f

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sput-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->u:Landroid/graphics/Paint;

    .line 811
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->l:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ah:Lcom/google/android/apps/gmm/map/t/b;

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->b:Lcom/google/android/apps/gmm/map/t/b;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ah:Lcom/google/android/apps/gmm/map/t/b;

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->c:Lcom/google/android/apps/gmm/map/t/b;

    if-ne v1, v2, :cond_5

    .line 814
    :cond_3
    :goto_0
    if-eqz p4, :cond_6

    .line 815
    invoke-virtual {p4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 816
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->at:F

    .line 821
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    if-eqz v0, :cond_7

    instance-of v0, p5, Lcom/google/android/apps/gmm/map/internal/c/cm;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    .line 822
    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    iget-object v2, p3, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/e;

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->at:F

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;-><init>(Lcom/google/android/apps/gmm/v/ao;Lcom/google/android/apps/gmm/map/internal/vector/gl/e;FLcom/google/android/apps/gmm/map/internal/c/bp;Z)V

    .line 824
    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aj:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    .line 827
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    if-eqz v0, :cond_8

    .line 828
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aj:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    .line 830
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aj:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    .line 831
    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    .line 838
    :goto_3
    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ap:Z

    if-nez v0, :cond_4

    .line 839
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aj:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    .line 840
    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Lcom/google/android/apps/gmm/v/ao;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    .line 842
    :cond_4
    return-void

    :cond_5
    move v5, v0

    .line 811
    goto :goto_0

    .line 818
    :cond_6
    iput v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->at:F

    goto :goto_1

    .line 822
    :cond_7
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    .line 824
    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    iget-object v2, p3, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/e;

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->at:F

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/f;-><init>(Lcom/google/android/apps/gmm/v/ao;Lcom/google/android/apps/gmm/map/internal/vector/gl/e;FLcom/google/android/apps/gmm/map/internal/c/bp;Z)V

    goto :goto_2

    .line 833
    :cond_8
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aj:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    .line 834
    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Lcom/google/android/apps/gmm/v/ao;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    .line 835
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aj:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    .line 836
    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Lcom/google/android/apps/gmm/v/ao;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    goto :goto_3
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;Lcom/google/android/apps/gmm/map/t/b;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;
    .locals 55
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;",
            "Lcom/google/android/apps/gmm/map/t/b;",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/a;",
            "Lcom/google/android/apps/gmm/map/c/a;",
            "Landroid/content/res/Resources;",
            "Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;",
            ")",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;"
        }
    .end annotation

    .prologue
    .line 491
    invoke-interface/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/c/bo;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v3

    .line 492
    new-instance v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    .line 493
    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v5, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/internal/c/bo;)V

    .line 494
    move-object/from16 v0, p0

    instance-of v3, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;

    if-eqz v3, :cond_66

    .line 495
    check-cast p0, Lcom/google/android/apps/gmm/map/internal/c/cm;

    .line 496
    if-eqz p6, :cond_0

    .line 497
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;->a()I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ak:I

    .line 499
    :cond_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->g:I

    iput v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->Y:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/c/cm;->e()[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v6, v4, v3

    iget-object v7, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    invoke-virtual {v7, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->f:[Lcom/google/android/apps/gmm/map/internal/c/i;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_2

    aget-object v6, v4, v3

    iget-object v7, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->X:Ljava/util/HashSet;

    invoke-virtual {v7, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->j:Lcom/google/android/apps/gmm/map/internal/c/ct;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/c/cm;->f()[Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->k:Z

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->n:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->e:Lcom/google/android/apps/gmm/map/internal/c/bs;

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->e:Lcom/google/android/apps/gmm/map/internal/c/bs;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bs;->e:Lcom/google/android/apps/gmm/map/internal/c/ac;

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->e:Lcom/google/android/apps/gmm/map/internal/c/bs;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bs;->f:I

    iput v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->g:I

    :cond_3
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->p:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq v3, v4, :cond_4

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v3, v4, :cond_11

    :cond_4
    const/4 v3, 0x1

    move/from16 v45, v3

    :goto_2
    sget-object v3, Lcom/google/android/apps/gmm/map/t/j;->b:Lcom/google/android/apps/gmm/map/t/j;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->p:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v3, v4, :cond_12

    sget-object v3, Lcom/google/android/apps/gmm/map/t/l;->g:Lcom/google/android/apps/gmm/map/t/l;

    move-object/from16 v46, v3

    :goto_3
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->q:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v3, v4, :cond_14

    const/4 v3, 0x1

    move/from16 v47, v3

    :goto_4
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/ai;->c()Z

    move-result v49

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->x:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v3, v4, :cond_15

    const/4 v3, 0x1

    move/from16 v48, v3

    :goto_5
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->t:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v3, v4, :cond_16

    const/4 v3, 0x1

    :goto_6
    if-eqz v3, :cond_17

    sget-object v23, Lcom/google/android/apps/gmm/map/t/l;->o:Lcom/google/android/apps/gmm/map/t/l;

    :goto_7
    check-cast v23, Lcom/google/android/apps/gmm/map/t/k;

    if-eqz v3, :cond_18

    sget-object v3, Lcom/google/android/apps/gmm/map/t/l;->o:Lcom/google/android/apps/gmm/map/t/l;

    :goto_8
    move-object/from16 v44, v3

    check-cast v44, Lcom/google/android/apps/gmm/map/t/k;

    new-instance v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    iget-object v7, v5, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    iget-object v9, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->al:Ljava/util/List;

    iget-object v10, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v5, p6

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;[Ljava/lang/String;Lcom/google/android/apps/gmm/v/ao;Ljava/util/List;Ljava/util/List;Ljava/util/Set;)V

    new-instance v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    iget-object v11, v4, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    iget-object v12, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    iget-object v13, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->al:Ljava/util/List;

    iget-object v14, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v9, p6

    move-object v10, v6

    invoke-direct/range {v7 .. v14}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;[Ljava/lang/String;Lcom/google/android/apps/gmm/v/ao;Ljava/util/List;Ljava/util/List;Ljava/util/Set;)V

    new-instance v8, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;

    iget-object v9, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    iget-object v12, v4, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    iget-object v13, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    iget-object v14, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->al:Ljava/util/List;

    iget-object v15, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v10, p6

    move-object v11, v6

    invoke-direct/range {v8 .. v15}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;[Ljava/lang/String;Lcom/google/android/apps/gmm/v/ao;Ljava/util/List;Ljava/util/List;Ljava/util/Set;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/c/cm;->h()Lcom/google/android/apps/gmm/map/internal/c/cp;

    move-result-object v52

    const/4 v4, 0x1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v50

    const/high16 v12, 0x3f800000    # 1.0f

    if-eqz p5, :cond_5

    invoke-virtual/range {p5 .. p5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v12, v5, Landroid/util/DisplayMetrics;->density:F

    :cond_5
    new-instance v19, Lcom/google/android/apps/gmm/v/ay;

    const/4 v5, 0x0

    const/4 v9, 0x1

    move-object/from16 v0, v19

    invoke-direct {v0, v5, v9}, Lcom/google/android/apps/gmm/v/ay;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    sget-boolean v5, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ap:Z

    if-eqz v5, :cond_19

    const/16 v5, 0x2601

    const/16 v9, 0x2601

    move-object/from16 v0, v19

    iget-boolean v10, v0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v10, :cond_6

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_6
    move-object/from16 v0, v19

    iput v5, v0, Lcom/google/android/apps/gmm/v/ci;->h:I

    move-object/from16 v0, v19

    iput v9, v0, Lcom/google/android/apps/gmm/v/ci;->i:I

    const/4 v5, 0x1

    move-object/from16 v0, v19

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/v/ci;->j:Z

    :goto_9
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/t/b;->p:Lcom/google/android/apps/gmm/map/util/a;

    sget-object v9, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->t:Lcom/google/android/apps/gmm/map/util/a;

    invoke-static {v5, v9}, Lcom/google/android/apps/gmm/map/util/a;->a(Lcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/util/a;)Lcom/google/android/apps/gmm/map/util/a;

    move-result-object v53

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v9, Lcom/google/android/apps/gmm/map/b/a/ai;->l:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v5, v9, :cond_1b

    sget-object v5, Lcom/google/android/apps/gmm/map/t/b;->b:Lcom/google/android/apps/gmm/map/t/b;

    move-object/from16 v0, p2

    if-eq v0, v5, :cond_7

    sget-object v5, Lcom/google/android/apps/gmm/map/t/b;->c:Lcom/google/android/apps/gmm/map/t/b;

    move-object/from16 v0, p2

    if-ne v0, v5, :cond_1b

    :cond_7
    const/4 v14, 0x1

    :goto_a
    iget-boolean v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    if-eqz v5, :cond_1c

    new-instance v9, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    iget-object v10, v5, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    move-object/from16 v0, p3

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/e;

    iget-object v13, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct/range {v9 .. v14}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;-><init>(Lcom/google/android/apps/gmm/v/ao;Lcom/google/android/apps/gmm/map/internal/vector/gl/e;FLcom/google/android/apps/gmm/map/internal/c/bp;Z)V

    :goto_b
    iput-object v9, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aj:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    const/16 v16, 0x0

    const-wide/16 v10, 0x2

    move-object/from16 v0, v53

    iget-wide v14, v0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    const-wide/16 v20, 0x1

    long-to-int v5, v10

    shl-long v10, v20, v5

    and-long/2addr v10, v14

    const-wide/16 v14, 0x0

    cmp-long v5, v10, v14

    if-eqz v5, :cond_1d

    const/4 v5, 0x1

    :goto_c
    if-eqz v5, :cond_8

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->G:Ljava/util/List;

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    :cond_8
    const-wide/16 v10, 0x6

    move-object/from16 v0, v53

    iget-wide v14, v0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    const-wide/16 v20, 0x1

    long-to-int v5, v10

    shl-long v10, v20, v5

    and-long/2addr v10, v14

    const-wide/16 v14, 0x0

    cmp-long v5, v10, v14

    if-eqz v5, :cond_1e

    const/4 v5, 0x1

    :goto_d
    if-eqz v5, :cond_9

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->D:Ljava/util/List;

    :cond_9
    const-wide/16 v10, 0x5

    move-object/from16 v0, v53

    iget-wide v14, v0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    const-wide/16 v20, 0x1

    long-to-int v5, v10

    shl-long v10, v20, v5

    and-long/2addr v10, v14

    const-wide/16 v14, 0x0

    cmp-long v5, v10, v14

    if-eqz v5, :cond_1f

    const/4 v5, 0x1

    :goto_e
    if-nez v5, :cond_a

    const-wide/16 v10, 0x3

    move-object/from16 v0, v53

    iget-wide v14, v0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    const-wide/16 v20, 0x1

    long-to-int v5, v10

    shl-long v10, v20, v5

    and-long/2addr v10, v14

    const-wide/16 v14, 0x0

    cmp-long v5, v10, v14

    if-eqz v5, :cond_20

    const/4 v5, 0x1

    :goto_f
    if-eqz v5, :cond_b

    :cond_a
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->E:Ljava/util/List;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->H:Ljava/util/List;

    :cond_b
    const-wide/16 v10, 0x4

    move-object/from16 v0, v53

    iget-wide v14, v0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    const-wide/16 v20, 0x1

    long-to-int v5, v10

    shl-long v10, v20, v5

    and-long/2addr v10, v14

    const-wide/16 v14, 0x0

    cmp-long v5, v10, v14

    if-eqz v5, :cond_21

    const/4 v5, 0x1

    :goto_10
    if-eqz v5, :cond_c

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->K:Ljava/util/List;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->L:Ljava/util/List;

    :cond_c
    const-wide/16 v10, 0x9

    move-object/from16 v0, v53

    iget-wide v14, v0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    const-wide/16 v20, 0x1

    long-to-int v5, v10

    shl-long v10, v20, v5

    and-long/2addr v10, v14

    const-wide/16 v14, 0x0

    cmp-long v5, v10, v14

    if-eqz v5, :cond_22

    const/4 v5, 0x1

    :goto_11
    if-eqz v5, :cond_d

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->M:Ljava/util/List;

    :cond_d
    invoke-interface/range {p4 .. p4}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v33

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->P:Ljava/util/List;

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/b/a/ai;->b()Z

    move-result v5

    if-nez v5, :cond_e

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/b/a/ai;->d()Z

    move-result v5

    if-eqz v5, :cond_f

    :cond_e
    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->P:Ljava/util/List;

    new-instance v9, Lcom/google/android/apps/gmm/map/o/bl;

    iget-object v10, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->U:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v11, v11, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    invoke-direct {v9, v10, v11}, Lcom/google/android/apps/gmm/map/o/bl;-><init>(Lcom/google/android/apps/gmm/map/b/a/ae;I)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_f
    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v9, Lcom/google/android/apps/gmm/map/b/a/ai;->x:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v5, v9, :cond_10

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->P:Ljava/util/List;

    new-instance v9, Lcom/google/android/apps/gmm/map/legacy/a/c/b/av;

    invoke-direct {v9, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/av;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_10
    move v10, v4

    :goto_12
    invoke-interface/range {v52 .. v52}, Lcom/google/android/apps/gmm/map/internal/c/cp;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3e

    invoke-interface/range {v52 .. v52}, Lcom/google/android/apps/gmm/map/internal/c/cp;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/c/m;->h()I

    move-result v5

    int-to-long v14, v5

    move-object/from16 v0, v53

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    move-wide/from16 v20, v0

    const-wide/16 v24, 0x1

    long-to-int v5, v14

    shl-long v14, v24, v5

    and-long v14, v14, v20

    const-wide/16 v20, 0x0

    cmp-long v5, v14, v20

    if-eqz v5, :cond_23

    const/4 v5, 0x1

    :goto_13
    if-nez v5, :cond_24

    invoke-interface/range {v52 .. v52}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    goto :goto_12

    :cond_11
    const/4 v3, 0x0

    move/from16 v45, v3

    goto/16 :goto_2

    :cond_12
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->t:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v3, v4, :cond_13

    sget-object v3, Lcom/google/android/apps/gmm/map/t/l;->o:Lcom/google/android/apps/gmm/map/t/l;

    move-object/from16 v46, v3

    goto/16 :goto_3

    :cond_13
    sget-object v3, Lcom/google/android/apps/gmm/map/t/l;->h:Lcom/google/android/apps/gmm/map/t/l;

    move-object/from16 v46, v3

    goto/16 :goto_3

    :cond_14
    const/4 v3, 0x0

    move/from16 v47, v3

    goto/16 :goto_4

    :cond_15
    const/4 v3, 0x0

    move/from16 v48, v3

    goto/16 :goto_5

    :cond_16
    const/4 v3, 0x0

    goto/16 :goto_6

    :cond_17
    sget-object v23, Lcom/google/android/apps/gmm/map/t/j;->b:Lcom/google/android/apps/gmm/map/t/j;

    goto/16 :goto_7

    :cond_18
    sget-object v3, Lcom/google/android/apps/gmm/map/t/j;->a:Lcom/google/android/apps/gmm/map/t/j;

    goto/16 :goto_8

    :cond_19
    const/16 v5, 0x2600

    const/16 v9, 0x2600

    move-object/from16 v0, v19

    iget-boolean v10, v0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v10, :cond_1a

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1a
    move-object/from16 v0, v19

    iput v5, v0, Lcom/google/android/apps/gmm/v/ci;->h:I

    move-object/from16 v0, v19

    iput v9, v0, Lcom/google/android/apps/gmm/v/ci;->i:I

    const/4 v5, 0x1

    move-object/from16 v0, v19

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/v/ci;->j:Z

    goto/16 :goto_9

    :cond_1b
    const/4 v14, 0x0

    goto/16 :goto_a

    :cond_1c
    new-instance v9, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/f;

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    iget-object v10, v5, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    move-object/from16 v0, p3

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/e;

    iget-object v13, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct/range {v9 .. v14}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/f;-><init>(Lcom/google/android/apps/gmm/v/ao;Lcom/google/android/apps/gmm/map/internal/vector/gl/e;FLcom/google/android/apps/gmm/map/internal/c/bp;Z)V

    goto/16 :goto_b

    :cond_1d
    const/4 v5, 0x0

    goto/16 :goto_c

    :cond_1e
    const/4 v5, 0x0

    goto/16 :goto_d

    :cond_1f
    const/4 v5, 0x0

    goto/16 :goto_e

    :cond_20
    const/4 v5, 0x0

    goto/16 :goto_f

    :cond_21
    const/4 v5, 0x0

    goto/16 :goto_10

    :cond_22
    const/4 v5, 0x0

    goto/16 :goto_11

    :cond_23
    const/4 v5, 0x0

    goto :goto_13

    :cond_24
    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/c/m;->h()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :pswitch_0
    invoke-interface/range {v52 .. v52}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    :cond_25
    :goto_14
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long v14, v4, v50

    const-wide/16 v20, 0xa

    cmp-long v9, v14, v20

    if-lez v9, :cond_6a

    invoke-static {}, Ljava/lang/Thread;->yield()V

    :goto_15
    move-wide/from16 v50, v4

    goto/16 :goto_12

    :pswitch_1
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->clear()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v16

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x0

    :goto_16
    invoke-interface/range {v52 .. v52}, Lcom/google/android/apps/gmm/map/internal/c/cp;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_30

    invoke-interface/range {v52 .. v52}, Lcom/google/android/apps/gmm/map/internal/c/cp;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/c/m;->h()I

    move-result v4

    const/4 v11, 0x2

    if-ne v4, v11, :cond_30

    invoke-interface/range {v52 .. v52}, Lcom/google/android/apps/gmm/map/internal/c/cp;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/c/az;

    if-eqz v10, :cond_26

    iget-object v11, v4, Lcom/google/android/apps/gmm/map/internal/c/az;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v11, v11, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v11, v11

    div-int/lit8 v11, v11, 0x3

    const/4 v13, 0x2

    if-ge v11, v13, :cond_28

    const/4 v11, 0x0

    :goto_17
    if-eqz v11, :cond_26

    const/4 v10, 0x0

    :cond_26
    if-nez v5, :cond_2f

    move-object v5, v4

    :cond_27
    :goto_18
    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface/range {v52 .. v52}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    goto :goto_16

    :cond_28
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/c/az;->m()Z

    move-result v11

    if-eqz v11, :cond_29

    const/4 v11, 0x1

    goto :goto_17

    :cond_29
    const/4 v11, 0x0

    :goto_19
    iget-object v13, v4, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v14, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v14, v14

    if-nez v14, :cond_2a

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v13

    :goto_1a
    iget-object v14, v13, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v14, :cond_2b

    const/4 v13, 0x0

    :goto_1b
    if-ge v11, v13, :cond_2e

    iget-object v13, v4, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v14, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v14, v14

    if-nez v14, :cond_2c

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v13

    :goto_1c
    iget-object v13, v13, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v13, v13, v11

    iget v14, v13, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    if-eqz v14, :cond_2d

    iget v13, v13, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-eqz v13, :cond_2d

    const/4 v11, 0x1

    goto :goto_17

    :cond_2a
    iget-object v13, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v14, 0x0

    aget-object v13, v13, v14

    goto :goto_1a

    :cond_2b
    iget-object v13, v13, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v13, v13

    goto :goto_1b

    :cond_2c
    iget-object v13, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v14, 0x0

    aget-object v13, v13, v14

    goto :goto_1c

    :cond_2d
    add-int/lit8 v11, v11, 0x1

    goto :goto_19

    :cond_2e
    const/4 v11, 0x0

    goto :goto_17

    :cond_2f
    invoke-static {v5, v4}, Lcom/google/android/apps/gmm/map/t/as;->a(Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/internal/c/m;)Z

    move-result v11

    if-nez v11, :cond_27

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v9, v5

    move-object v5, v4

    goto :goto_18

    :cond_30
    iget-object v13, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v14, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    move-object/from16 v17, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    move-object/from16 v18, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aj:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    move-object/from16 v20, v0

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a()Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;

    move-result-object v24

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v25, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v26, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->G:Ljava/util/List;

    move-object/from16 v27, v0

    move-object v15, v6

    move-object/from16 v21, p6

    move-object/from16 v22, p3

    invoke-static/range {v13 .. v27}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/v/ad;[Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;Lcom/google/android/apps/gmm/v/ci;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;Ljava/util/List;Ljava/util/Set;Ljava/util/List;)V

    goto/16 :goto_14

    :pswitch_2
    if-eqz v47, :cond_31

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v27, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->as:Lcom/google/android/apps/gmm/map/indoor/c/r;

    move-object/from16 v30, v0

    sget-object v31, Lcom/google/android/apps/gmm/map/t/l;->d:Lcom/google/android/apps/gmm/map/t/l;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->v:Lcom/google/android/apps/gmm/map/t/g;

    move-object/from16 v32, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v34, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->al:Ljava/util/List;

    move-object/from16 v35, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v36, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v52

    move-object/from16 v28, p6

    move-object/from16 v29, v8

    invoke-static/range {v24 .. v36}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;Lcom/google/android/apps/gmm/map/indoor/c/r;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/util/b/g;Ljava/util/List;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;

    move-result-object v4

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->N:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    const/16 v9, 0x12

    if-lt v5, v9, :cond_25

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->P:Ljava/util/List;

    new-instance v9, Lcom/google/android/apps/gmm/map/o/n;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->b:Lcom/google/android/apps/gmm/map/b/a/e;

    invoke-direct {v9, v4}, Lcom/google/android/apps/gmm/map/o/n;-><init>(Lcom/google/android/apps/gmm/map/b/a/d;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :cond_31
    if-eqz v10, :cond_32

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->E:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v27, v0

    const/16 v30, 0x0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->v:Lcom/google/android/apps/gmm/map/t/g;

    move-object/from16 v32, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v34, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->al:Ljava/util/List;

    move-object/from16 v35, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v36, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v52

    move-object/from16 v28, p6

    move-object/from16 v29, v3

    move-object/from16 v31, v23

    invoke-static/range {v24 .. v36}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;Lcom/google/android/apps/gmm/map/indoor/c/r;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/util/b/g;Ljava/util/List;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :cond_32
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->H:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v27, v0

    const/16 v30, 0x0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->v:Lcom/google/android/apps/gmm/map/t/g;

    move-object/from16 v32, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v34, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->al:Ljava/util/List;

    move-object/from16 v35, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v36, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v52

    move-object/from16 v28, p6

    move-object/from16 v29, v7

    move-object/from16 v31, v23

    invoke-static/range {v24 .. v36}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;Lcom/google/android/apps/gmm/map/indoor/c/r;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/util/b/g;Ljava/util/List;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :pswitch_3
    if-eqz v10, :cond_33

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->E:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v27, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->w:Lcom/google/android/apps/gmm/map/t/g;

    move-object/from16 v29, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v30, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v31, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v52

    move-object/from16 v28, v23

    invoke-static/range {v24 .. v31}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/bp;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :cond_33
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->H:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v27, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->w:Lcom/google/android/apps/gmm/map/t/g;

    move-object/from16 v29, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v30, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v31, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v52

    move-object/from16 v28, v23

    invoke-static/range {v24 .. v31}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/bp;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :pswitch_4
    check-cast v4, Lcom/google/android/apps/gmm/map/internal/c/ad;

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a(Lcom/google/android/apps/gmm/map/internal/c/ad;)Z

    move-result v4

    if-eqz v4, :cond_36

    if-eqz v45, :cond_34

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->H:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v27, v0

    sget-object v28, Lcom/google/android/apps/gmm/map/t/l;->g:Lcom/google/android/apps/gmm/map/t/l;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->w:Lcom/google/android/apps/gmm/map/t/g;

    move-object/from16 v29, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v30, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v31, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v52

    invoke-static/range {v24 .. v31}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/bp;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :cond_34
    if-eqz v10, :cond_35

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->E:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v27, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->w:Lcom/google/android/apps/gmm/map/t/g;

    move-object/from16 v29, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v30, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v31, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v52

    move-object/from16 v28, v23

    invoke-static/range {v24 .. v31}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/bp;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :cond_35
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->H:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v27, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->w:Lcom/google/android/apps/gmm/map/t/g;

    move-object/from16 v29, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v30, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v31, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v52

    move-object/from16 v28, v23

    invoke-static/range {v24 .. v31}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/bp;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :cond_36
    if-nez v45, :cond_37

    if-eqz v10, :cond_37

    move-object/from16 v0, v52

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->a(Lcom/google/android/apps/gmm/map/internal/c/cp;)V

    goto/16 :goto_14

    :cond_37
    move-object/from16 v0, v52

    invoke-virtual {v7, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->a(Lcom/google/android/apps/gmm/map/internal/c/cp;)V

    goto/16 :goto_14

    :pswitch_5
    check-cast v4, Lcom/google/android/apps/gmm/map/internal/c/j;

    move-object/from16 v0, p4

    invoke-static {v4, v0}, Lcom/google/android/apps/gmm/map/indoor/c/u;->a(Lcom/google/android/apps/gmm/map/internal/c/j;Lcom/google/android/apps/gmm/map/c/a;)Z

    move-result v4

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->K:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v27, v0

    if-eqz v4, :cond_38

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->c:Lcom/google/android/apps/gmm/map/t/e;

    move-object/from16 v28, v0

    :goto_1d
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v30, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v31, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v52

    move-object/from16 v29, p4

    invoke-static/range {v24 .. v31}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/e;Lcom/google/android/apps/gmm/map/c/a;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :cond_38
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->d:Lcom/google/android/apps/gmm/map/t/e;

    move-object/from16 v28, v0

    goto :goto_1d

    :pswitch_6
    check-cast v4, Lcom/google/android/apps/gmm/map/internal/c/bc;

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bc;->c:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_39

    const/4 v4, 0x1

    :goto_1e
    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->L:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    if-eqz v4, :cond_3a

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->c:Lcom/google/android/apps/gmm/map/t/e;

    move-object/from16 v27, v0

    :goto_1f
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ar:Lcom/google/android/apps/gmm/v/ck;

    move-object/from16 v28, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v30, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v31, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v52

    move-object/from16 v29, p4

    invoke-static/range {v24 .. v31}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/map/t/e;Lcom/google/android/apps/gmm/v/ck;Lcom/google/android/apps/gmm/map/c/a;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :cond_39
    const/4 v4, 0x0

    goto :goto_1e

    :cond_3a
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->d:Lcom/google/android/apps/gmm/map/t/e;

    move-object/from16 v27, v0

    goto :goto_1f

    :pswitch_7
    if-eqz v45, :cond_3b

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->D:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v27, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->b:Lcom/google/android/apps/gmm/map/t/am;

    move-object/from16 v29, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v31, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v32, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v52

    move-object/from16 v28, v46

    move-object/from16 v30, p3

    invoke-static/range {v24 .. v32}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :cond_3b
    if-eqz v49, :cond_3c

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->D:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v27, v0

    sget-object v28, Lcom/google/android/apps/gmm/map/t/l;->b:Lcom/google/android/apps/gmm/map/t/l;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->b:Lcom/google/android/apps/gmm/map/t/am;

    move-object/from16 v29, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v31, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v32, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v52

    move-object/from16 v30, p3

    invoke-static/range {v24 .. v32}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :cond_3c
    if-eqz v48, :cond_3d

    invoke-interface/range {v52 .. v52}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    goto/16 :goto_14

    :cond_3d
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->D:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v27, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->b:Lcom/google/android/apps/gmm/map/t/am;

    move-object/from16 v29, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v31, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v32, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v52

    move-object/from16 v28, v23

    move-object/from16 v30, p3

    invoke-static/range {v24 .. v32}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :pswitch_8
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->M:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v34, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v37, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    move-object/from16 v38, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aj:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    move-object/from16 v40, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v42, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    move-object/from16 v43, v0

    move-object/from16 v35, v6

    move-object/from16 v36, v52

    move-object/from16 v39, v19

    move-object/from16 v41, p3

    invoke-static/range {v34 .. v43}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/v/ci;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :pswitch_9
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->O:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v26, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v28, v0

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    invoke-interface/range {p4 .. p4}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v29

    move-object/from16 v25, v52

    move-object/from16 v27, p3

    invoke-static/range {v24 .. v29}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/List;Lcom/google/android/apps/gmm/map/util/a/b;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14

    :cond_3e
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aj:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    iget v5, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->i:I

    iget v4, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->f:I

    mul-int/2addr v4, v5

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/shared/c/s;->e(II)I

    move-result v5

    iget-boolean v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    iput-boolean v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->Z:Z

    iget-boolean v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    if-eqz v4, :cond_40

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    check-cast v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    iget-object v9, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->a:[F

    const/4 v10, 0x0

    int-to-float v11, v5

    aput v11, v9, v10

    const-wide v10, 0x3fd3333333333333L    # 0.3

    const-wide v14, 0x3fb999999999999aL    # 0.1

    const-wide v16, 0x3fb999999999999aL    # 0.1

    const/high16 v9, 0x40400000    # 3.0f

    sub-float/2addr v9, v12

    float-to-double v0, v9

    move-wide/from16 v20, v0

    mul-double v16, v16, v20

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->max(DD)D

    move-result-wide v14

    invoke-static {v10, v11, v14, v15}, Ljava/lang/Math;->min(DD)D

    move-result-wide v10

    double-to-float v9, v10

    iput v9, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->i:F

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    check-cast v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    iget-object v9, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->a:[F

    const/4 v10, 0x0

    int-to-float v11, v5

    aput v11, v9, v10

    const-wide v10, 0x3fd3333333333333L    # 0.3

    const-wide v14, 0x3fb999999999999aL    # 0.1

    const-wide v16, 0x3fb999999999999aL    # 0.1

    const/high16 v9, 0x40400000    # 3.0f

    sub-float/2addr v9, v12

    float-to-double v0, v9

    move-wide/from16 v20, v0

    mul-double v16, v16, v20

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->max(DD)D

    move-result-wide v14

    invoke-static {v10, v11, v14, v15}, Ljava/lang/Math;->min(DD)D

    move-result-wide v10

    double-to-float v9, v10

    iput v9, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->i:F

    :goto_20
    sget-boolean v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ap:Z

    if-nez v4, :cond_3f

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->b:[F

    const/4 v9, 0x0

    int-to-float v5, v5

    aput v5, v4, v9

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    const-wide v10, 0x3fd3333333333333L    # 0.3

    const-wide v14, 0x3fb999999999999aL    # 0.1

    const-wide v16, 0x3fb999999999999aL    # 0.1

    const/high16 v5, 0x40400000    # 3.0f

    sub-float/2addr v5, v12

    float-to-double v12, v5

    mul-double v12, v12, v16

    invoke-static {v14, v15, v12, v13}, Ljava/lang/Math;->max(DD)D

    move-result-wide v12

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(DD)D

    move-result-wide v10

    double-to-float v5, v10

    iput v5, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->a:F

    :cond_3f
    const/16 v22, 0x0

    iget-object v10, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aj:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->h:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_41

    const/4 v10, 0x0

    :goto_21
    if-eqz v10, :cond_67

    sget-boolean v15, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ap:Z

    new-instance v9, Lcom/google/android/apps/gmm/v/ar;

    const/4 v11, 0x4

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aj:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->a()I

    move-result v12

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aj:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;

    iget v5, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->i:I

    iget v4, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->f:I

    mul-int/2addr v4, v5

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/shared/c/s;->e(II)I

    move-result v13

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    iget-object v14, v4, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    invoke-direct/range {v9 .. v15}, Lcom/google/android/apps/gmm/v/ar;-><init>(Ljava/nio/ByteBuffer;IIILcom/google/android/apps/gmm/v/ao;Z)V

    :goto_22
    const/4 v5, 0x0

    sget-boolean v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ap:Z

    if-eqz v4, :cond_5b

    const/16 v4, 0x2901

    :goto_23
    const v10, 0x812f

    move-object/from16 v0, v19

    invoke-virtual {v0, v9, v5, v4, v10}, Lcom/google/android/apps/gmm/v/ci;->a(Lcom/google/android/apps/gmm/v/ar;ZII)V

    const/16 v24, 0x0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->x:Lcom/google/android/apps/gmm/map/t/am;

    move-object/from16 v25, v0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v26, v0

    move-object/from16 v22, v3

    move-object/from16 v27, p6

    invoke-virtual/range {v22 .. v27}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->a(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/indoor/c/r;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    const/4 v11, 0x0

    iget-object v12, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->x:Lcom/google/android/apps/gmm/map/t/am;

    iget-object v13, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object v9, v7

    move-object/from16 v10, v46

    move-object/from16 v14, p6

    invoke-virtual/range {v9 .. v14}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->a(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/indoor/c/r;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    sget-object v9, Lcom/google/android/apps/gmm/map/t/l;->e:Lcom/google/android/apps/gmm/map/t/l;

    iget-object v10, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->as:Lcom/google/android/apps/gmm/map/indoor/c/r;

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->y:Lcom/google/android/apps/gmm/map/t/am;

    iget-object v12, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v13, p6

    invoke-virtual/range {v8 .. v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->a(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/indoor/c/r;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->J:Ljava/util/List;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/c/cm;->g()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/c/cm;->h()Lcom/google/android/apps/gmm/map/internal/c/cp;

    move-result-object v5

    if-ltz v4, :cond_5c

    const/4 v3, 0x1

    :goto_24
    if-nez v3, :cond_5d

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_40
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    check-cast v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->a:[F

    const/4 v9, 0x0

    int-to-float v10, v5

    aput v10, v4, v9

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    check-cast v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->a:[F

    const/4 v9, 0x0

    int-to-float v10, v5

    aput v10, v4, v9

    goto/16 :goto_20

    :cond_41
    invoke-virtual {v10}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->a()I

    move-result v13

    invoke-virtual {v10}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->b()I

    move-result v4

    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->c()I

    move-result v5

    mul-int/2addr v4, v5

    iget v5, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->c:I

    add-int/2addr v4, v5

    sub-int v12, v13, v4

    iget v4, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->i:I

    iget v5, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->f:I

    mul-int/2addr v4, v5

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/shared/c/s;->e(II)I

    move-result v4

    iget-object v5, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->h:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    iget v9, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->f:I

    mul-int/2addr v5, v9

    sub-int v14, v4, v5

    mul-int/lit8 v5, v13, 0x4

    mul-int/2addr v4, v5

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v11

    new-instance v9, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;

    invoke-direct/range {v9 .. v14}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Ljava/nio/ByteBuffer;III)V

    iget-boolean v4, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->m:Z

    if-eqz v4, :cond_42

    new-instance v12, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/o;

    invoke-direct {v12, v10}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/o;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;)V

    :goto_25
    const/4 v4, 0x0

    move/from16 v21, v4

    :goto_26
    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->h:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v21

    if-ge v0, v4, :cond_58

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->h:Ljava/util/List;

    move/from16 v0, v21

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/apps/gmm/map/internal/c/bi;

    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->d()Z

    move-result v4

    if-eqz v4, :cond_49

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v4, v4

    if-nez v4, :cond_43

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v4

    :goto_27
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v5, :cond_44

    const/4 v5, 0x0

    :goto_28
    const/4 v13, 0x0

    move v15, v13

    :goto_29
    const/4 v13, 0x4

    if-ge v15, v13, :cond_49

    if-ge v15, v5, :cond_47

    iget-object v13, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v13, v13, v15

    iget-object v0, v13, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    move-object/from16 v17, v0

    const/4 v14, 0x0

    if-eqz v17, :cond_45

    const/4 v13, 0x0

    :goto_2a
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_45

    aget v18, v17, v13

    add-int v14, v14, v18

    add-int/lit8 v13, v13, 0x1

    goto :goto_2a

    :cond_42
    new-instance v12, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/p;

    invoke-direct {v12, v10}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/p;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;)V

    goto :goto_25

    :cond_43
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    goto :goto_27

    :cond_44
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v5, v5

    goto :goto_28

    :cond_45
    if-nez v14, :cond_46

    const/high16 v13, 0x3f800000    # 1.0f

    :goto_2b
    const/16 v17, 0x0

    const/4 v14, 0x0

    move/from16 v54, v14

    move/from16 v14, v17

    move/from16 v17, v13

    move/from16 v13, v54

    :goto_2c
    const/16 v18, 0x4

    move/from16 v0, v18

    if-ge v13, v0, :cond_48

    const/high16 v18, 0x437f0000    # 255.0f

    mul-float v17, v17, v18

    const/16 v18, 0xff

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v18

    move/from16 v1, v20

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v17, v17, v20

    shl-int/lit8 v14, v14, 0x8

    or-int v14, v14, v18

    add-int/lit8 v13, v13, 0x1

    goto :goto_2c

    :cond_46
    const/high16 v13, 0x3f800000    # 1.0f

    int-to-float v14, v14

    const/high16 v17, 0x40000000    # 2.0f

    mul-float v14, v14, v17

    div-float/2addr v13, v14

    goto :goto_2b

    :cond_47
    const/high16 v13, 0x3f800000    # 1.0f

    goto :goto_2b

    :cond_48
    iget-object v13, v9, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v13, v14}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v13, 0x0

    iput v13, v9, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->f:I

    iget-object v13, v9, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->e:Lcom/google/android/apps/gmm/shared/c/j;

    const/4 v14, 0x0

    iput v14, v13, Lcom/google/android/apps/gmm/shared/c/j;->b:I

    add-int/lit8 v13, v15, 0x1

    move v15, v13

    goto/16 :goto_29

    :cond_49
    const/4 v4, 0x0

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    aget-byte v4, v5, v4

    int-to-float v4, v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v25

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/gmm/map/internal/c/bi;->b()F

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/gmm/map/internal/c/bi;->c()I

    move-result v26

    const/4 v4, 0x0

    move/from16 v18, v4

    :goto_2d
    invoke-virtual {v10}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->b()I

    move-result v4

    move/from16 v0, v18

    if-ge v0, v4, :cond_52

    move/from16 v0, v18

    move/from16 v1, v26

    if-lt v0, v1, :cond_68

    add-int/lit8 v4, v26, -0x1

    move/from16 v20, v4

    :goto_2e
    iget-boolean v4, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->n:Z

    if-eqz v4, :cond_4c

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    aget-byte v4, v4, v20

    int-to-float v4, v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v4

    div-float v15, v4, v25

    :goto_2f
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v5, 0x0

    move-object/from16 v0, v16

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v13, v13

    add-int/lit8 v13, v13, -0x1

    move/from16 v0, v20

    invoke-static {v0, v5, v13}, Lcom/google/android/apps/gmm/shared/c/s;->a(III)I

    move-result v5

    aget-object v17, v4, v5

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v4, :cond_4d

    const/4 v14, 0x0

    :goto_30
    const/4 v4, 0x0

    move/from16 v24, v4

    :goto_31
    const/4 v4, 0x4

    move/from16 v0, v24

    if-ge v0, v4, :cond_51

    move/from16 v0, v24

    if-ge v0, v14, :cond_4e

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v4, v4, v24

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    :cond_4a
    :goto_32
    iget-object v5, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->k:Ljava/util/Set;

    move-object/from16 v0, v16

    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4b

    iget-object v5, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->l:[F

    invoke-static {v4, v5}, Landroid/graphics/Color;->colorToHSV(I[F)V

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->l:[F

    const/4 v5, 0x2

    const/high16 v13, 0x3f800000    # 1.0f

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->l:[F

    move-object/from16 v27, v0

    const/16 v28, 0x2

    aget v27, v27, v28

    const/high16 v28, 0x3fc00000    # 1.5f

    mul-float v27, v27, v28

    move/from16 v0, v27

    invoke-static {v13, v0}, Ljava/lang/Math;->min(FF)F

    move-result v13

    aput v13, v4, v5

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->l:[F

    invoke-static {v4}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v4

    :cond_4b
    shl-int/lit8 v5, v4, 0x8

    ushr-int/lit8 v4, v4, 0x18

    or-int/2addr v4, v5

    iget-object v5, v9, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->e:Lcom/google/android/apps/gmm/shared/c/j;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/shared/c/j;->a(I)V

    iget-object v5, v9, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v24, 0x1

    move/from16 v24, v4

    goto :goto_31

    :cond_4c
    const/4 v4, 0x0

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    aget-byte v4, v5, v4

    add-int v4, v4, v18

    int-to-float v4, v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v4

    div-float v15, v4, v25

    goto/16 :goto_2f

    :cond_4d
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v14, v4

    goto :goto_30

    :cond_4e
    const/4 v4, 0x0

    add-int/lit8 v5, v20, 0x1

    :goto_33
    invoke-virtual {v10}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->b()I

    move-result v13

    if-ge v5, v13, :cond_4a

    move/from16 v0, v26

    if-lt v5, v0, :cond_69

    add-int/lit8 v13, v26, -0x1

    :goto_34
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    array-length v0, v0

    move/from16 v29, v0

    add-int/lit8 v29, v29, -0x1

    move/from16 v0, v28

    move/from16 v1, v29

    invoke-static {v13, v0, v1}, Lcom/google/android/apps/gmm/shared/c/s;->a(III)I

    move-result v13

    aget-object v27, v27, v13

    move-object/from16 v0, v27

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v13, :cond_4f

    const/4 v13, 0x0

    :goto_35
    move/from16 v0, v24

    if-ge v0, v13, :cond_50

    move-object/from16 v0, v27

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v4, v4, v24

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    goto/16 :goto_32

    :cond_4f
    move-object/from16 v0, v27

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v13, v13

    goto :goto_35

    :cond_50
    add-int/lit8 v5, v5, 0x1

    goto :goto_33

    :cond_51
    move-object v13, v9

    invoke-virtual/range {v12 .. v17}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/q;->a(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;IFLcom/google/android/apps/gmm/map/internal/c/bi;Lcom/google/android/apps/gmm/map/internal/c/be;)V

    add-int/lit8 v4, v18, 0x1

    move/from16 v18, v4

    goto/16 :goto_2d

    :cond_52
    const/4 v4, 0x0

    :goto_36
    iget v5, v9, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->b:I

    if-ge v4, v5, :cond_53

    iget-object v5, v9, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->a:Ljava/nio/ByteBuffer;

    const/4 v13, 0x0

    invoke-virtual {v5, v13}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v4, 0x1

    goto :goto_36

    :cond_53
    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->d()Z

    move-result v4

    if-eqz v4, :cond_57

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v4, v4

    if-nez v4, :cond_54

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v4

    :goto_37
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v5, :cond_55

    const/4 v5, 0x0

    :goto_38
    const/4 v13, 0x0

    move v14, v13

    :goto_39
    const/4 v13, 0x4

    if-ge v14, v13, :cond_57

    if-ge v14, v5, :cond_56

    iget-object v13, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v13, v13, v14

    iget-object v13, v13, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    :goto_3a
    invoke-virtual {v9, v13}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->a([I)V

    add-int/lit8 v13, v14, 0x1

    move v14, v13

    goto :goto_39

    :cond_54
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    goto :goto_37

    :cond_55
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v5, v5

    goto :goto_38

    :cond_56
    sget-object v13, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->b:[I

    goto :goto_3a

    :cond_57
    add-int/lit8 v4, v21, 0x1

    move/from16 v21, v4

    goto/16 :goto_26

    :cond_58
    const/4 v4, 0x0

    :goto_3b
    iget v5, v9, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->d:I

    if-ge v4, v5, :cond_5a

    const/4 v5, 0x0

    :goto_3c
    iget v10, v9, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->c:I

    if-ge v5, v10, :cond_59

    iget-object v10, v9, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->a:Ljava/nio/ByteBuffer;

    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    add-int/lit8 v5, v5, 0x1

    goto :goto_3c

    :cond_59
    add-int/lit8 v4, v4, 0x1

    goto :goto_3b

    :cond_5a
    iget-object v4, v9, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->a:Ljava/nio/ByteBuffer;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-object v10, v11

    goto/16 :goto_21

    :cond_5b
    const v4, 0x812f

    goto/16 :goto_23

    :cond_5c
    const/4 v3, 0x0

    goto/16 :goto_24

    :cond_5d
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->Q:Ljava/util/List;

    if-ltz v4, :cond_5e

    const/4 v3, 0x1

    :goto_3d
    if-nez v3, :cond_5f

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_5e
    const/4 v3, 0x0

    goto :goto_3d

    :cond_5f
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->R:Ljava/util/List;

    :cond_60
    :goto_3e
    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/internal/c/cp;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_61

    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/internal/c/m;

    const/4 v7, 0x0

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    invoke-direct {v2, v3, v7, v8, v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/internal/c/m;ZLjava/util/Set;[Ljava/lang/String;)Lcom/google/android/apps/gmm/map/o/as;

    move-result-object v3

    if-eqz v3, :cond_60

    iget-object v7, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->R:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3e

    :cond_61
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/c/a;Ljava/util/List;)Z

    const-string v3, "GLVectorTile"

    const-string v5, "%1$s:%2$s labels %3$d/%4$d"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->Q:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v7

    invoke-static {v3, v5, v6}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_62

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    new-instance v5, Lcom/google/android/apps/gmm/map/t/at;

    iget-object v6, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    const/4 v7, 0x1

    move-object/from16 v0, p3

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    invoke-virtual {v4, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->a(I)Lcom/google/android/apps/gmm/v/co;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-direct {v5, v0, v6, v4}, Lcom/google/android/apps/gmm/map/t/at;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/v/co;)V

    iput-object v5, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ab:Lcom/google/android/apps/gmm/map/t/ar;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ab:Lcom/google/android/apps/gmm/map/t/ar;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_62
    iget-boolean v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->Z:Z

    if-eqz v3, :cond_63

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    int-to-float v3, v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->b(F)V

    :cond_63
    const/4 v3, 0x0

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v4, v3

    :goto_3f
    if-ge v4, v5, :cond_64

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/t/ar;

    iget-object v6, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    iget-object v6, v6, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v7, Lcom/google/android/apps/gmm/v/af;

    const/4 v8, 0x1

    invoke-direct {v7, v3, v8}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v6, v7}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_3f

    .line 501
    :cond_64
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->j:Lcom/google/android/apps/gmm/map/internal/c/ct;

    iget-wide v4, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-ltz v3, :cond_66

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ae:J

    const-wide/16 v8, -0x1

    cmp-long v3, v6, v8

    if-eqz v3, :cond_65

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ae:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_66

    :cond_65
    iput-wide v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ae:J

    const-wide/32 v6, 0xea60

    add-long/2addr v4, v6

    iput-wide v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->af:J

    .line 503
    :cond_66
    return-object v2

    :cond_67
    move-object/from16 v9, v22

    goto/16 :goto_22

    :cond_68
    move/from16 v20, v18

    goto/16 :goto_2e

    :cond_69
    move v13, v5

    goto/16 :goto_34

    :cond_6a
    move-wide/from16 v4, v50

    goto/16 :goto_15

    .line 499
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_7
        :pswitch_0
        :pswitch_4
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_9
    .end packed-switch
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/r;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 568
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/internal/c/bo;)V

    .line 570
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->e:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->Y:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->d:[Ljava/lang/String;

    array-length v3, v2

    move v1, v7

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->f:Lcom/google/android/apps/gmm/map/internal/c/bt;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    new-array v9, v8, [Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->c:[B

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->b:Lcom/google/android/apps/gmm/map/t/am;

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object v4, p1

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->a([BLcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/List;Lcom/google/android/apps/gmm/v/ad;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;

    move-result-object v1

    aput-object v1, v9, v7

    if-nez v9, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    array-length v2, v9

    if-ltz v2, :cond_2

    move v1, v8

    :goto_1
    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v1, v7

    goto :goto_1

    :cond_3
    const-wide/16 v4, 0x5

    int-to-long v10, v2

    add-long/2addr v4, v10

    div-int/lit8 v1, v2, 0xa

    int-to-long v2, v1

    add-long/2addr v2, v4

    const-wide/32 v4, 0x7fffffff

    cmp-long v1, v2, v4

    if-lez v1, :cond_4

    const v1, 0x7fffffff

    :goto_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v2, v9}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->D:Ljava/util/List;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v7

    :goto_3
    if-ge v2, v3, :cond_6

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/t/p;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v5, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v5, v1, v8}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_4
    const-wide/32 v4, -0x80000000

    cmp-long v1, v2, v4

    if-gez v1, :cond_5

    const/high16 v1, -0x80000000

    goto :goto_2

    :cond_5
    long-to-int v1, v2

    goto :goto_2

    :cond_6
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->a:[F

    int-to-float v2, v7

    aput v2, v1, v7

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->a:[F

    int-to-float v2, v7

    aput v2, v1, v7

    :goto_4
    sget-boolean v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ap:Z

    if-nez v1, :cond_7

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->b:[F

    int-to-float v2, v7

    aput v2, v1, v7

    :cond_7
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->Q:Ljava/util/List;

    .line 571
    return-object v0

    .line 570
    :cond_8
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->a:[F

    int-to-float v2, v7

    aput v2, v1, v7

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->a:[F

    int-to-float v2, v7

    aput v2, v1, v7

    goto :goto_4
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/c/m;ZLjava/util/Set;[Ljava/lang/String;)Lcom/google/android/apps/gmm/map/o/as;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/m;",
            "Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/gmm/map/o/as;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1610
    const/4 v1, 0x0

    .line 1611
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/m;->h()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    move-object v0, v1

    .line 1644
    :cond_1
    :goto_0
    return-object v0

    .line 1613
    :sswitch_0
    new-instance v0, Lcom/google/android/apps/gmm/map/o/as;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/apps/gmm/map/o/as;-><init>(Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/o/ak;Z)V

    .line 1617
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/m;->j()[I

    move-result-object v1

    array-length v3, v1

    :goto_1
    if-ge v2, v3, :cond_1

    aget v4, v1, v2

    .line 1618
    if-ltz v4, :cond_2

    array-length v5, p4

    if-ge v4, v5, :cond_2

    .line 1619
    aget-object v4, p4, v4

    invoke-interface {p3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1617
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :sswitch_1
    move-object v0, p1

    .line 1627
    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/ad;

    .line 1629
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/ad;->e:[Lcom/google/android/apps/gmm/map/internal/c/z;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 1630
    new-instance v0, Lcom/google/android/apps/gmm/map/o/as;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/apps/gmm/map/o/as;-><init>(Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/o/ak;Z)V

    goto :goto_0

    :sswitch_2
    move-object v0, p1

    .line 1635
    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/az;

    .line 1637
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    if-nez v3, :cond_3

    move v0, v2

    :goto_2
    if-lez v0, :cond_0

    .line 1638
    new-instance v0, Lcom/google/android/apps/gmm/map/o/as;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/apps/gmm/map/o/as;-><init>(Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/o/ak;Z)V

    goto :goto_0

    .line 1637
    :cond_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    array-length v0, v0

    goto :goto_2

    .line 1611
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x7 -> :sswitch_0
        0x8 -> :sswitch_1
        0xb -> :sswitch_1
    .end sparse-switch
.end method

.method private static a(Lcom/google/android/apps/gmm/map/internal/c/m;)Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 1861
    instance-of v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;

    if-eqz v0, :cond_2

    .line 1862
    check-cast p0, Lcom/google/android/apps/gmm/map/internal/c/an;

    .line 1863
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    if-eqz v0, :cond_2

    .line 1864
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bb;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 1865
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bb;->a:Ljava/lang/String;

    .line 1868
    :goto_1
    return-object v0

    .line 1864
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1868
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v2, 0x5f

    .line 907
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 909
    const/4 v0, 0x0

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 910
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 911
    if-eq v1, v2, :cond_0

    invoke-static {v1}, Ljava/lang/Character;->isLowerCase(C)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v3}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 912
    :cond_0
    if-nez v0, :cond_2

    invoke-static {v3}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    :goto_1
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 909
    :cond_1
    add-int/lit8 v0, v0, 0x1

    move v1, v3

    goto :goto_0

    .line 912
    :cond_2
    invoke-static {v3}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v1

    goto :goto_1

    .line 916
    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/internal/c/cm;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 5
    .param p5    # Ljava/util/Set;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Ljava/util/Set;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/c/a;",
            "Lcom/google/android/apps/gmm/map/internal/c/m;",
            "Lcom/google/android/apps/gmm/map/internal/c/cm;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/as;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1816
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/internal/c/m;->h()I

    move-result v0

    const/4 v3, 0x7

    if-eq v0, v3, :cond_0

    const/16 v3, 0xb

    if-ne v0, v3, :cond_3

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 1817
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/internal/c/m;->e()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v3

    .line 1819
    invoke-static {v3}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1820
    invoke-static {p2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/internal/c/m;)Ljava/lang/String;

    move-result-object v0

    .line 1821
    if-eqz p6, :cond_5

    if-eqz v0, :cond_5

    .line 1822
    invoke-interface {p6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 1830
    :goto_1
    if-eqz v0, :cond_2

    .line 1831
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/internal/c/cm;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v0

    .line 1832
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->T:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/internal/c/bp;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v2, v1

    .line 1833
    :cond_1
    :goto_2
    if-eqz v2, :cond_2

    .line 1834
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->W:Ljava/util/HashSet;

    .line 1836
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/internal/c/cm;->e()[Ljava/lang/String;

    move-result-object v2

    .line 1834
    invoke-direct {p0, p2, v1, v0, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/internal/c/m;ZLjava/util/Set;[Ljava/lang/String;)Lcom/google/android/apps/gmm/map/o/as;

    move-result-object v0

    .line 1837
    if-eqz v0, :cond_2

    .line 1838
    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1839
    invoke-static {v3}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1840
    if-eqz p7, :cond_2

    .line 1841
    invoke-interface {p7, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1853
    :cond_2
    :goto_3
    return-void

    :cond_3
    move v0, v2

    .line 1816
    goto :goto_0

    :cond_4
    move v0, v2

    .line 1822
    goto :goto_1

    :cond_5
    move v0, v1

    .line 1827
    goto :goto_1

    .line 1828
    :cond_6
    if-eqz p5, :cond_7

    invoke-interface {p5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_7
    move v0, v1

    goto :goto_1

    :cond_8
    move v0, v2

    goto :goto_1

    .line 1832
    :cond_9
    instance-of v0, p2, Lcom/google/android/apps/gmm/map/internal/c/an;

    if-eqz v0, :cond_a

    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->U:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/an;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_a
    move v2, v1

    goto :goto_2

    .line 1844
    :cond_b
    invoke-static {p2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/internal/c/m;)Ljava/lang/String;

    move-result-object v0

    .line 1845
    if-eqz p8, :cond_2

    if-eqz v0, :cond_2

    .line 1846
    invoke-interface {p8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method private a(Lcom/google/android/apps/gmm/map/t/b;)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1028
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ai;->c()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->b:Lcom/google/android/apps/gmm/map/t/b;

    if-eq p1, v0, :cond_6

    .line 1029
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    if-eqz v0, :cond_1

    .line 1030
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    if-eqz v0, :cond_4

    .line 1031
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->l:F

    .line 1036
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    if-eqz v0, :cond_2

    .line 1037
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    if-eqz v0, :cond_5

    .line 1038
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->l:F

    .line 1043
    :cond_2
    :goto_1
    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ap:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    if-eqz v0, :cond_3

    .line 1044
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->d:F

    .line 1066
    :cond_3
    :goto_2
    return-void

    .line 1033
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->d:F

    goto :goto_0

    .line 1040
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->d:F

    goto :goto_1

    .line 1047
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ai:Z

    if-eqz v0, :cond_9

    const v0, 0x3f19999a    # 0.6f

    move v1, v0

    .line 1048
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    if-eqz v0, :cond_7

    .line 1049
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    if-eqz v0, :cond_a

    .line 1050
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->l:F

    .line 1055
    :cond_7
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    if-eqz v0, :cond_8

    .line 1056
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    if-eqz v0, :cond_b

    .line 1057
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->l:F

    .line 1062
    :cond_8
    :goto_5
    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ap:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    if-eqz v0, :cond_3

    .line 1063
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->d:F

    goto :goto_2

    .line 1047
    :cond_9
    const/4 v0, 0x0

    move v1, v0

    goto :goto_3

    .line 1052
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->d:F

    goto :goto_4

    .line 1059
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->d:F

    goto :goto_5
.end method

.method public static a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1138
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1139
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1140
    return-void
.end method

.method private a(Ljava/util/List;IF)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;",
            ">;IF)V"
        }
    .end annotation

    .prologue
    .line 678
    if-eqz p1, :cond_4

    .line 679
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_4

    .line 680
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    .line 681
    if-eqz v0, :cond_2

    .line 682
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ao:Lcom/google/android/apps/gmm/v/ax;

    const/4 v1, 0x0

    int-to-float v5, p2

    sub-float v5, p3, v5

    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    const/high16 v5, 0x3f000000    # 0.5f

    div-float v5, v1, v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/v/ax;->a()Lcom/google/android/apps/gmm/v/ax;

    const/high16 v6, 0x3f800000    # 1.0f

    iget-boolean v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->c:Z

    if-eqz v7, :cond_0

    int-to-float v1, v5

    :cond_0
    const v5, 0x3faaaaab

    div-float/2addr v1, v5

    add-float/2addr v1, v6

    const/high16 v5, 0x3f000000    # 0.5f

    const/high16 v6, 0x3f800000    # 1.0f

    sub-float/2addr v6, v1

    mul-float/2addr v5, v6

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->e:Lcom/google/android/apps/gmm/v/ci;

    if-eqz v6, :cond_2

    iget-object v6, v4, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v7, 0x0

    aput v1, v6, v7

    iget-object v6, v4, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v7, 0x6

    aput v5, v6, v7

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->c:Z

    if-eqz v6, :cond_3

    iget-object v6, v4, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v7, 0x4

    aput v1, v6, v7

    iget-object v1, v4, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v6, 0x7

    aput v5, v1, v6

    :goto_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->e:Lcom/google/android/apps/gmm/v/ci;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/v/ci;->u:Lcom/google/android/apps/gmm/v/ax;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v5, 0x0

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v6, 0x0

    const/16 v7, 0x9

    invoke-static {v4, v5, v1, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-wide v4, v0, Lcom/google/android/apps/gmm/v/ci;->v:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v0, Lcom/google/android/apps/gmm/v/ci;->v:J

    .line 679
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 682
    :cond_3
    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v5, v4, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v6, 0x4

    aput v1, v5, v6

    const/4 v1, 0x0

    iget-object v5, v4, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v6, 0x7

    aput v1, v5, v6

    goto :goto_1

    .line 686
    :cond_4
    return-void
.end method

.method private b(F)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const-wide v10, 0x3fd3333333333333L    # 0.3

    const/high16 v8, 0x40400000    # 3.0f

    const-wide v6, 0x3fb999999999999aL    # 0.1

    .line 710
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    .line 712
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    int-to-float v1, v1

    sub-float v4, p1, v1

    .line 715
    cmpl-float v1, v4, v8

    if-ltz v1, :cond_0

    .line 721
    const/4 v1, 0x3

    .line 722
    const/4 v2, 0x2

    .line 723
    const/high16 v3, 0x3f800000    # 1.0f

    .line 734
    :goto_0
    invoke-virtual {v0, v3, v2, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->a(FII)V

    .line 735
    iget v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->at:F

    sub-float v4, v8, v4

    float-to-double v4, v4

    mul-double/2addr v4, v6

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    invoke-static {v10, v11, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->i:F

    .line 737
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    .line 738
    invoke-virtual {v0, v3, v2, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->a(FII)V

    .line 739
    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->at:F

    sub-float v1, v8, v1

    float-to-double v2, v1

    mul-double/2addr v2, v6

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    invoke-static {v10, v11, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->i:F

    .line 740
    return-void

    .line 724
    :cond_0
    cmpg-float v1, v4, v3

    if-gez v1, :cond_1

    .line 726
    const/4 v2, 0x0

    .line 727
    const/4 v1, 0x1

    goto :goto_0

    .line 729
    :cond_1
    float-to-int v2, v4

    .line 730
    add-int/lit8 v1, v2, 0x1

    .line 731
    int-to-float v3, v2

    sub-float v3, v4, v3

    goto :goto_0
.end method

.method private c(F)V
    .locals 3

    .prologue
    .line 923
    iput p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->p:F

    .line 924
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->v:Lcom/google/android/apps/gmm/map/t/g;

    iput p1, v0, Lcom/google/android/apps/gmm/map/t/g;->a:F

    .line 925
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->w:Lcom/google/android/apps/gmm/map/t/g;

    iput p1, v0, Lcom/google/android/apps/gmm/map/t/g;->a:F

    .line 926
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->b:Lcom/google/android/apps/gmm/map/t/am;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->o:Z

    if-eqz v0, :cond_c

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->p:F

    const v2, 0x3e4ccccd    # 0.2f

    mul-float/2addr v0, v2

    :goto_0
    iget-boolean v2, v1, Lcom/google/android/apps/gmm/map/t/am;->p:Z

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_0
    iput v0, v1, Lcom/google/android/apps/gmm/map/t/am;->b:F

    .line 927
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    if-eqz v0, :cond_1

    .line 928
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    if-eqz v0, :cond_d

    .line 929
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    iput p1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->c:F

    .line 934
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    if-eqz v0, :cond_2

    .line 935
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    if-eqz v0, :cond_e

    .line 936
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    iput p1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->c:F

    .line 941
    :cond_2
    :goto_2
    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ap:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    if-eqz v0, :cond_3

    .line 942
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    iput p1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->c:F

    .line 945
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->x:Lcom/google/android/apps/gmm/map/t/am;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/t/am;->p:Z

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_4
    iput p1, v0, Lcom/google/android/apps/gmm/map/t/am;->b:F

    .line 946
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->y:Lcom/google/android/apps/gmm/map/t/am;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/t/am;->p:Z

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_5
    iput p1, v0, Lcom/google/android/apps/gmm/map/t/am;->b:F

    .line 948
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->c:Lcom/google/android/apps/gmm/map/t/e;

    if-eqz v0, :cond_7

    .line 949
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->c:Lcom/google/android/apps/gmm/map/t/e;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/t/e;->p:Z

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_6
    iput p1, v0, Lcom/google/android/apps/gmm/map/t/e;->d:F

    .line 951
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->d:Lcom/google/android/apps/gmm/map/t/e;

    if-eqz v0, :cond_9

    .line 952
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->d:Lcom/google/android/apps/gmm/map/t/e;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/t/e;->p:Z

    if-eqz v1, :cond_8

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_8
    iput p1, v0, Lcom/google/android/apps/gmm/map/t/e;->d:F

    .line 955
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->O:Ljava/util/List;

    if-eqz v0, :cond_f

    .line 956
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->O:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;

    .line 957
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->c:Lcom/google/android/apps/gmm/map/t/av;

    if-eqz v2, :cond_a

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->c:Lcom/google/android/apps/gmm/map/t/av;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/t/av;->p:Z

    if-eqz v2, :cond_b

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_b
    iput p1, v0, Lcom/google/android/apps/gmm/map/t/av;->b:F

    goto :goto_3

    .line 926
    :cond_c
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->p:F

    goto/16 :goto_0

    .line 931
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    iput p1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->c:F

    goto :goto_1

    .line 938
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    iput p1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->c:F

    goto :goto_2

    .line 960
    :cond_f
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 1104
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->al:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1108
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->al:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 1107
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1110
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;)V
    .locals 11

    .prologue
    .line 1994
    if-eqz p2, :cond_4

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/u;

    .line 1998
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->U:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->U:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v2, v3, v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->C:[F

    invoke-static {v0, p1, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/t;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/u;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/y;F[F)V

    .line 2001
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ac:[F

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/f/o;->A:Lcom/google/android/apps/gmm/map/f/j;

    const/4 v2, 0x0

    iget v3, v1, Lcom/google/android/apps/gmm/map/f/j;->d:F

    aput v3, v0, v2

    const/4 v2, 0x1

    iget v3, v1, Lcom/google/android/apps/gmm/map/f/j;->e:F

    aput v3, v0, v2

    const/4 v2, 0x2

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/j;->f:F

    aput v1, v0, v2

    .line 2002
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ac:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    const/16 v2, 0x11

    if-ne v0, v2, :cond_5

    const/4 v0, 0x0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aa:Z

    .line 2004
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v3, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->a(IF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->an:F

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    if-eqz v0, :cond_6

    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ap:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v1, v3

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    int-to-double v6, v2

    sub-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-float v1, v4

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->b:F

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v1, v3

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    int-to-double v6, v2

    sub-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-float v1, v4

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->b:F

    :cond_0
    :goto_2
    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ap:Z

    if-nez v0, :cond_1

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->a(IF)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    iget v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->an:F

    iget-object v5, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->b:[F

    const/4 v6, 0x1

    aput v4, v5, v6

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->b:[F

    const/4 v4, 0x2

    aput v0, v1, v4

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->G:Ljava/util/List;

    if-eqz v4, :cond_7

    const/4 v0, 0x0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v1, v0

    :goto_3
    if-ge v1, v5, :cond_7

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;

    if-eqz v0, :cond_3

    iget v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->an:F

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

    if-eqz v7, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v6, v8

    const/high16 v8, 0x3fc00000    # 1.5f

    mul-float/2addr v6, v8

    add-float/2addr v6, v7

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->g:Lcom/google/android/apps/gmm/v/ci;

    if-eqz v7, :cond_3

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->g:Lcom/google/android/apps/gmm/v/ci;

    iget-boolean v8, v7, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v8, :cond_2

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_2
    const/high16 v8, 0x3f000000    # 0.5f

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v9, v6

    mul-float/2addr v8, v9

    const/high16 v9, 0x3f000000    # 0.5f

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v6

    mul-float/2addr v9, v10

    invoke-virtual {v7, v8, v9, v6, v6}, Lcom/google/android/apps/gmm/v/ci;->a(FFFF)V

    const/4 v6, 0x0

    const/high16 v7, 0x40400000    # 3.0f

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v8, v8, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    int-to-float v8, v8

    sub-float v8, v3, v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->d:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/g;

    iput v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/g;->a:F

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1994
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2002
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2004
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->an:F

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->a:[F

    const/4 v4, 0x1

    aput v1, v0, v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->an:F

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->a:[F

    const/4 v4, 0x1

    aput v1, v0, v4

    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ap:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v1, v3

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    int-to-double v6, v2

    sub-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-float v1, v4

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->b:F

    goto/16 :goto_2

    :cond_7
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->Z:Z

    if-eqz v0, :cond_8

    invoke-direct {p0, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->b(F)V

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->c:Lcom/google/android/apps/gmm/map/t/e;

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/t/e;->a(F)F

    move-result v1

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/t/e;->p:Z

    if-eqz v4, :cond_9

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_9
    iput v1, v0, Lcom/google/android/apps/gmm/map/t/e;->b:F

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Ljava/util/List;IF)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Ljava/util/List;IF)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->J:Ljava/util/List;

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Ljava/util/List;IF)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->O:Ljava/util/List;

    if-eqz v0, :cond_b

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    int-to-float v2, v2

    sub-float v2, v3, v2

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    const/high16 v1, 0x40400000    # 3.0f

    div-float v2, v0, v1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->O:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->O:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->c:Lcom/google/android/apps/gmm/map/t/av;

    if-eqz v4, :cond_a

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->c:Lcom/google/android/apps/gmm/map/t/av;

    iput v2, v0, Lcom/google/android/apps/gmm/map/t/av;->a:F

    :cond_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 2006
    :cond_b
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    const/high16 v1, 0x418c0000    # 17.5f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_c

    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ai:Z

    .line 2007
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ah:Lcom/google/android/apps/gmm/map/t/b;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    .line 2008
    return-void

    .line 2006
    :cond_c
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/o;Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/f/o;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2174
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->V:Ljava/util/HashSet;

    invoke-interface {p2, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 2175
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->W:Ljava/util/HashSet;

    invoke-interface {p2, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 2177
    if-eqz p1, :cond_1

    .line 2178
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/f/o;->b()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/bb;->c:Lcom/google/android/apps/gmm/map/b/a/m;

    .line 2179
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->X:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/i;

    .line 2180
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/i;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2181
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/i;->a:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2185
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V
    .locals 2

    .prologue
    .line 1090
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ak:I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;->a()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 1100
    :goto_0
    return-void

    .line 1096
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->al:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1097
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->al:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 1096
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1099
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ak:I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/am;)V
    .locals 2

    .prologue
    .line 1956
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->Q:Ljava/util/List;

    .line 1957
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1958
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/o/am;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1960
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->P:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->P:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1961
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/am;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->P:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1963
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->S:Lcom/google/android/apps/gmm/map/o/bk;

    .line 1964
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/p;)V
    .locals 0

    .prologue
    .line 1972
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/t/p;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 602
    if-nez p1, :cond_0

    .line 613
    :goto_0
    return-void

    .line 608
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v3, p1, v1}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 611
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ah:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/t/p;->c:Lcom/google/android/apps/gmm/map/t/k;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/t/k;->c()I

    move-result v3

    shl-int v3, v1, v3

    int-to-long v4, v3

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/t/b;->o:J

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    :goto_1
    if-eqz v1, :cond_1

    const/16 v0, 0xff

    :cond_1
    iget-boolean v1, p1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_2
    int-to-byte v0, v0

    iput-byte v0, p1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 612
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->s:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    move v1, v0

    .line 611
    goto :goto_1
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->as:Lcom/google/android/apps/gmm/map/indoor/c/r;

    if-eqz v0, :cond_0

    .line 1084
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->as:Lcom/google/android/apps/gmm/map/indoor/c/r;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/indoor/c/r;->b:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iput-boolean p1, v0, Lcom/google/android/apps/gmm/map/indoor/c/r;->f:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/indoor/c/r;->b:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 1086
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ah:Lcom/google/android/apps/gmm/map/t/b;

    sget-object v1, Lcom/google/android/apps/gmm/map/t/b;->f:Lcom/google/android/apps/gmm/map/t/b;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/c/a;Ljava/util/List;)Z
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/c/a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1660
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->h:J

    .line 1661
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->W:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    .line 1662
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->i:I

    .line 1663
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->j:Z

    .line 1664
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ag:J

    .line 1667
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1668
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->R:Ljava/util/List;

    .line 1724
    :cond_0
    new-instance v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aw;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aw;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;)V

    invoke-static {v6, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1730
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->Q:Ljava/util/List;

    .line 1732
    const/4 v2, 0x1

    return v2

    .line 1670
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->R:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-instance v6, Ljava/util/ArrayList;

    if-ltz v3, :cond_2

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    const-wide/16 v4, 0x5

    int-to-long v8, v3

    add-long/2addr v4, v8

    div-int/lit8 v2, v3, 0xa

    int-to-long v2, v2

    add-long/2addr v2, v4

    const-wide/32 v4, 0x7fffffff

    cmp-long v4, v2, v4

    if-lez v4, :cond_5

    const v2, 0x7fffffff

    :goto_1
    invoke-direct {v6, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1671
    new-instance v14, Ljava/util/TreeSet;

    invoke-direct {v14}, Ljava/util/TreeSet;-><init>()V

    .line 1672
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_7

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 1678
    :goto_2
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_8

    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 1680
    :goto_3
    const/4 v2, 0x0

    move v13, v2

    :goto_4
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v13, v2, :cond_15

    .line 1681
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Lcom/google/android/apps/gmm/map/internal/c/bo;

    .line 1682
    instance-of v2, v11, Lcom/google/android/apps/gmm/map/internal/c/cm;

    if-eqz v2, :cond_12

    move-object v5, v11

    .line 1683
    check-cast v5, Lcom/google/android/apps/gmm/map/internal/c/cm;

    if-eqz v7, :cond_9

    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    :goto_5
    if-eqz v8, :cond_a

    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    :goto_6
    iget-object v2, v5, Lcom/google/android/apps/gmm/map/internal/c/cm;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v15

    const/4 v2, 0x0

    move v12, v2

    :goto_7
    iget-object v2, v5, Lcom/google/android/apps/gmm/map/internal/c/cm;->i:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    if-eqz v2, :cond_b

    iget-object v2, v5, Lcom/google/android/apps/gmm/map/internal/c/cm;->i:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    array-length v2, v2

    :goto_8
    if-ge v12, v2, :cond_e

    iget-object v2, v5, Lcom/google/android/apps/gmm/map/internal/c/cm;->i:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    if-eqz v2, :cond_c

    iget-object v2, v5, Lcom/google/android/apps/gmm/map/internal/c/cm;->i:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    aget-object v2, v2, v12

    :goto_9
    instance-of v3, v2, Lcom/google/android/apps/gmm/map/internal/c/ck;

    if-eqz v3, :cond_d

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/ck;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/c/ck;->a:Lcom/google/android/apps/gmm/map/internal/c/m;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/internal/c/cm;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    :cond_4
    :goto_a
    add-int/lit8 v2, v12, 0x1

    move v12, v2

    goto :goto_7

    .line 1670
    :cond_5
    const-wide/32 v4, -0x80000000

    cmp-long v4, v2, v4

    if-gez v4, :cond_6

    const/high16 v2, -0x80000000

    goto :goto_1

    :cond_6
    long-to-int v2, v2

    goto :goto_1

    .line 1672
    :cond_7
    const/4 v7, 0x0

    goto :goto_2

    .line 1678
    :cond_8
    const/4 v8, 0x0

    goto :goto_3

    .line 1683
    :cond_9
    const/4 v9, 0x0

    goto :goto_5

    :cond_a
    const/4 v10, 0x0

    goto :goto_6

    :cond_b
    const/4 v2, 0x0

    goto :goto_8

    :cond_c
    const/4 v2, 0x0

    goto :goto_9

    :cond_d
    instance-of v3, v2, Lcom/google/android/apps/gmm/map/internal/c/cl;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->T:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v15, v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/cl;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/internal/c/cl;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v14, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_e
    const/4 v2, 0x0

    move v12, v2

    :goto_b
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/c/cm;->g()I

    move-result v2

    if-ge v12, v2, :cond_f

    invoke-virtual {v5, v12}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(I)Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v4

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/internal/c/cm;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    add-int/lit8 v2, v12, 0x1

    move v12, v2

    goto :goto_b

    :cond_f
    if-eqz v7, :cond_10

    invoke-interface {v7, v9}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    :cond_10
    if-eqz v8, :cond_11

    invoke-interface {v8, v10}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1689
    :cond_11
    invoke-interface {v11}, Lcom/google/android/apps/gmm/map/internal/c/bo;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1692
    invoke-interface {v11}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->i:I

    .line 1696
    :goto_c
    invoke-interface {v11}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v2

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_12

    .line 1697
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ag:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_14

    .line 1698
    invoke-interface {v11}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v2

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ag:J

    .line 1680
    :cond_12
    :goto_d
    add-int/lit8 v2, v13, 0x1

    move v13, v2

    goto/16 :goto_4

    .line 1694
    :cond_13
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->j:Z

    goto :goto_c

    .line 1700
    :cond_14
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ag:J

    .line 1701
    invoke-interface {v11}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v4

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ag:J

    goto :goto_d

    .line 1706
    :cond_15
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_16

    const/4 v2, 0x0

    :goto_e
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->S:Lcom/google/android/apps/gmm/map/o/bk;

    .line 1708
    invoke-interface {v14}, Ljava/util/Set;->size()I

    move-result v2

    if-nez v2, :cond_17

    .line 1709
    const/4 v2, 0x0

    :goto_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->R:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 1710
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->R:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1709
    add-int/lit8 v2, v2, 0x1

    goto :goto_f

    .line 1706
    :cond_16
    new-instance v2, Lcom/google/android/apps/gmm/map/o/bk;

    invoke-direct {v2, v6}, Lcom/google/android/apps/gmm/map/o/bk;-><init>(Ljava/util/List;)V

    goto :goto_e

    .line 1713
    :cond_17
    const/4 v2, 0x0

    move v3, v2

    :goto_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->R:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 1714
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->R:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/o/as;

    .line 1715
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    .line 1716
    if-eqz v4, :cond_18

    .line 1717
    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/c/m;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v14, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_19

    .line 1718
    :cond_18
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1713
    :cond_19
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_10
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/b;F)Z
    .locals 10

    .prologue
    .line 976
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aq:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;

    iput p2, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->k:F

    .line 981
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->n:Z

    if-nez v0, :cond_1

    .line 985
    sget-object v0, Lcom/google/android/apps/gmm/map/t/c;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/t/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 987
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ah:Lcom/google/android/apps/gmm/map/t/b;

    if-ne v0, p1, :cond_2

    .line 988
    const/4 v0, 0x0

    .line 1024
    :goto_1
    return v0

    .line 985
    :pswitch_0
    sget-object p1, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_0

    :pswitch_1
    sget-object p1, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_0

    :pswitch_2
    sget-object p1, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_0

    :pswitch_3
    sget-object p1, Lcom/google/android/apps/gmm/map/t/b;->c:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_0

    :pswitch_4
    sget-object p1, Lcom/google/android/apps/gmm/map/t/b;->b:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_0

    :pswitch_5
    sget-object p1, Lcom/google/android/apps/gmm/map/t/b;->d:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_0

    :pswitch_6
    sget-object p1, Lcom/google/android/apps/gmm/map/t/b;->e:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_0

    .line 991
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ah:Lcom/google/android/apps/gmm/map/t/b;

    .line 993
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->c:Lcom/google/android/apps/gmm/map/t/b;

    if-ne p1, v0, :cond_4

    .line 994
    const v0, 0x3e8f5c29    # 0.28f

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->c(F)V

    .line 999
    :goto_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    .line 1000
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_7

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ah:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/t/p;->c:Lcom/google/android/apps/gmm/map/t/k;

    const/4 v6, 0x1

    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/t/k;->c()I

    move-result v5

    shl-int v5, v6, v5

    int-to-long v6, v5

    iget-wide v8, v1, Lcom/google/android/apps/gmm/map/t/b;->o:J

    and-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-lez v1, :cond_5

    const/4 v1, 0x1

    :goto_4
    if-eqz v1, :cond_6

    const/16 v1, 0xff

    :goto_5
    iget-boolean v5, v0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v5, :cond_3

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_3
    int-to-byte v1, v1

    iput-byte v1, v0, Lcom/google/android/apps/gmm/v/aa;->w:B

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 996
    :cond_4
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->c(F)V

    goto :goto_2

    .line 1000
    :cond_5
    const/4 v1, 0x0

    goto :goto_4

    :cond_6
    const/4 v1, 0x0

    goto :goto_5

    .line 1001
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 1002
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ah:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/t/p;->c:Lcom/google/android/apps/gmm/map/t/k;

    const/4 v4, 0x1

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/t/k;->c()I

    move-result v3

    shl-int v3, v4, v3

    int-to-long v4, v3

    iget-wide v6, v1, Lcom/google/android/apps/gmm/map/t/b;->o:J

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_9

    const/4 v1, 0x1

    :goto_7
    if-eqz v1, :cond_a

    const/16 v1, 0xff

    :goto_8
    iget-boolean v3, v0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_8

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_8
    int-to-byte v1, v1

    iput-byte v1, v0, Lcom/google/android/apps/gmm/v/aa;->w:B

    goto :goto_6

    :cond_9
    const/4 v1, 0x0

    goto :goto_7

    :cond_a
    const/4 v1, 0x0

    goto :goto_8

    .line 1004
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ar:Lcom/google/android/apps/gmm/v/ck;

    if-eqz v0, :cond_c

    .line 1011
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ar:Lcom/google/android/apps/gmm/v/ck;

    sget-object v0, Lcom/google/android/apps/gmm/map/t/l;->m:Lcom/google/android/apps/gmm/map/t/l;

    const/4 v2, 0x1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/t/k;->c()I

    move-result v0

    shl-int v0, v2, v0

    int-to-long v2, v0

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/t/b;->o:J

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_f

    const/4 v0, 0x1

    :goto_9
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->aa:Z

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/v/ck;->a(ZZ)V

    .line 1014
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->as:Lcom/google/android/apps/gmm/map/indoor/c/r;

    if-eqz v0, :cond_e

    .line 1015
    sget-object v0, Lcom/google/android/apps/gmm/map/t/l;->d:Lcom/google/android/apps/gmm/map/t/l;

    const/4 v1, 0x1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/t/k;->c()I

    move-result v0

    shl-int v0, v1, v0

    int-to-long v0, v0

    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/t/b;->o:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_10

    const/4 v0, 0x1

    .line 1016
    :goto_a
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->as:Lcom/google/android/apps/gmm/map/indoor/c/r;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/indoor/c/r;->c:Lcom/google/android/apps/gmm/v/ck;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/gmm/v/ck;->a(ZZ)V

    if-eqz v0, :cond_d

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/indoor/c/r;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    const/4 v4, 0x1

    invoke-direct {v3, v1, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 1019
    :cond_d
    if-nez v0, :cond_e

    .line 1020
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Z)V

    .line 1024
    :cond_e
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 1011
    :cond_f
    const/4 v0, 0x0

    goto :goto_9

    .line 1015
    :cond_10
    const/4 v0, 0x0

    goto :goto_a

    .line 985
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/c/f;)Z
    .locals 4

    .prologue
    .line 2110
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ae:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ae:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/o/p;)V
    .locals 0

    .prologue
    .line 1976
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1951
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/shared/c/f;)Z
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v0, 0x1

    .line 2115
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    .line 2116
    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->af:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->af:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 2123
    :cond_0
    :goto_0
    return v0

    .line 2119
    :cond_1
    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ag:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_2

    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ag:J

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 2123
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/apps/gmm/map/b/a/ai;
    .locals 1

    .prologue
    .line 2022
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1980
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/ar;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/t/ar;->t:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 1

    .prologue
    .line 2012
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    return-object v0
.end method

.method public final f()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 2027
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    .line 2028
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/p;

    .line 2029
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v5, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v5, v0, v1}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 2027
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2031
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/p;

    .line 2032
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v4, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v4, v0, v1}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto :goto_1

    .line 2037
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2038
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_2
    if-ge v2, v3, :cond_2

    .line 2039
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    .line 2040
    iput-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    .line 2038
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2043
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 2044
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_3
    if-ge v2, v3, :cond_3

    .line 2045
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    .line 2046
    iput-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    .line 2044
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 2049
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->J:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 2050
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    :goto_4
    if-ge v1, v2, :cond_4

    .line 2051
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->J:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    .line 2052
    iput-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    .line 2050
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 2055
    :cond_4
    return-void
.end method

.method public final g()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 2059
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f()V

    .line 2063
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2064
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    .line 2065
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    .line 2066
    iput-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    .line 2064
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2069
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 2070
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_1

    .line 2071
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    .line 2072
    iput-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    .line 2070
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2075
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->J:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2076
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    :goto_2
    if-ge v1, v2, :cond_2

    .line 2077
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->J:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    .line 2078
    iput-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    .line 2076
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2082
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->am:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2083
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2084
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ar:Lcom/google/android/apps/gmm/v/ck;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/ck;->c()V

    .line 2085
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->as:Lcom/google/android/apps/gmm/map/indoor/c/r;

    if-eqz v0, :cond_3

    .line 2086
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->as:Lcom/google/android/apps/gmm/map/indoor/c/r;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/indoor/c/r;->c:Lcom/google/android/apps/gmm/v/ck;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/ck;->c()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/c/r;->e:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 2089
    :cond_3
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->D:Ljava/util/List;

    .line 2090
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->E:Ljava/util/List;

    .line 2091
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    .line 2092
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->G:Ljava/util/List;

    .line 2093
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->H:Ljava/util/List;

    .line 2094
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    .line 2095
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->K:Ljava/util/List;

    .line 2096
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->L:Ljava/util/List;

    .line 2097
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->M:Ljava/util/List;

    .line 2098
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->N:Ljava/util/List;

    .line 2099
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->O:Ljava/util/List;

    .line 2101
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->z:Lcom/google/android/apps/gmm/v/bp;

    .line 2102
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->B:Lcom/google/android/apps/gmm/v/bp;

    .line 2103
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->A:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;

    .line 2104
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->c:Lcom/google/android/apps/gmm/map/t/e;

    .line 2105
    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->d:Lcom/google/android/apps/gmm/map/t/e;

    .line 2106
    return-void
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 2193
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->Y:I

    return v0
.end method

.method public final i()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2198
    .line 2199
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->D:Ljava/util/List;

    if-eqz v0, :cond_12

    .line 2200
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->D:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    move v1, v2

    :goto_0
    if-ge v3, v4, :cond_0

    .line 2201
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->D:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;

    .line 2202
    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->c:I

    add-int/2addr v1, v0

    .line 2200
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 2205
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 2206
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    move v1, v0

    :goto_2
    if-ge v3, v4, :cond_1

    .line 2207
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    .line 2208
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    add-int/2addr v1, v0

    .line 2206
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_1
    move v0, v1

    .line 2211
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->G:Ljava/util/List;

    if-eqz v1, :cond_4

    .line 2212
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->G:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    move v1, v0

    :goto_3
    if-ge v3, v4, :cond_3

    .line 2213
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->G:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;

    .line 2214
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a()I

    move-result v0

    add-int/2addr v1, v0

    .line 2212
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_3
    move v0, v1

    .line 2217
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    if-eqz v1, :cond_6

    .line 2218
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    move v1, v0

    :goto_4
    if-ge v3, v4, :cond_5

    .line 2219
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    .line 2220
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    add-int/2addr v1, v0

    .line 2218
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_5
    move v0, v1

    .line 2223
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->K:Ljava/util/List;

    if-eqz v1, :cond_9

    .line 2224
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->K:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    move v1, v0

    :goto_5
    if-ge v3, v4, :cond_8

    .line 2225
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->K:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;

    .line 2226
    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-nez v5, :cond_7

    move v0, v2

    :goto_6
    add-int/2addr v1, v0

    .line 2224
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 2226
    :cond_7
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    goto :goto_6

    :cond_8
    move v0, v1

    .line 2229
    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->L:Ljava/util/List;

    if-eqz v1, :cond_b

    .line 2230
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->L:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;

    .line 2231
    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-nez v4, :cond_a

    move v0, v2

    :goto_8
    add-int/2addr v0, v1

    move v1, v0

    .line 2232
    goto :goto_7

    .line 2231
    :cond_a
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    goto :goto_8

    :cond_b
    move v1, v0

    .line 2234
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->M:Ljava/util/List;

    if-eqz v0, :cond_d

    .line 2235
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_9
    if-ge v3, v4, :cond_d

    .line 2236
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->M:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;

    .line 2237
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->a()I

    move-result v0

    add-int/2addr v1, v0

    .line 2235
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_9

    .line 2240
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->E:Ljava/util/List;

    if-eqz v0, :cond_e

    .line 2241
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->E:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_a
    if-ge v3, v4, :cond_e

    .line 2242
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->E:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;

    .line 2243
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;->a()I

    move-result v0

    add-int/2addr v1, v0

    .line 2241
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_a

    .line 2246
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->H:Ljava/util/List;

    if-eqz v0, :cond_f

    .line 2247
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->H:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_b
    if-ge v3, v4, :cond_f

    .line 2248
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->H:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;

    .line 2249
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;->a()I

    move-result v0

    add-int/2addr v1, v0

    .line 2247
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_b

    .line 2252
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->N:Ljava/util/List;

    if-eqz v0, :cond_10

    .line 2253
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->N:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_c
    if-ge v3, v4, :cond_10

    .line 2254
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->N:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;

    .line 2255
    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;->a:I

    add-int/2addr v1, v0

    .line 2253
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_c

    .line 2258
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->O:Ljava/util/List;

    if-eqz v0, :cond_11

    .line 2259
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->O:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    :goto_d
    if-ge v2, v3, :cond_11

    .line 2260
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->O:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;

    .line 2261
    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->b:I

    add-int/2addr v1, v0

    .line 2259
    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    .line 2265
    :cond_11
    return v1

    :cond_12
    move v0, v2

    goto/16 :goto_1
.end method

.method public final j()I
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 2270
    const/16 v0, 0x100

    .line 2271
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->D:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 2272
    const/16 v1, 0x110

    .line 2273
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->D:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_0

    .line 2274
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->D:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;

    .line 2275
    add-int/lit8 v1, v1, 0x60

    .line 2273
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 2278
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    if-eqz v1, :cond_3

    .line 2279
    add-int/lit8 v1, v0, 0x18

    .line 2280
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_2

    .line 2281
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->F:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    .line 2282
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->b()I

    move-result v0

    add-int/2addr v1, v0

    .line 2280
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2285
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->G:Ljava/util/List;

    if-eqz v1, :cond_5

    .line 2286
    add-int/lit8 v2, v0, 0x18

    .line 2287
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v4, v3

    :goto_2
    if-ge v4, v5, :cond_4

    .line 2288
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->G:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;

    .line 2289
    const/16 v1, 0x1d0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

    if-eqz v6, :cond_16

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

    const/16 v0, 0x220

    :goto_3
    add-int/2addr v2, v0

    .line 2287
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_4
    move v0, v2

    .line 2292
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    if-eqz v1, :cond_7

    .line 2293
    add-int/lit8 v1, v0, 0x18

    .line 2294
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    :goto_4
    if-ge v2, v4, :cond_6

    .line 2295
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->I:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    .line 2296
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->b()I

    move-result v0

    add-int/2addr v1, v0

    .line 2294
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_6
    move v0, v1

    .line 2299
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->K:Ljava/util/List;

    if-eqz v1, :cond_a

    .line 2300
    add-int/lit8 v1, v0, 0x18

    .line 2301
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->K:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    :goto_5
    if-ge v2, v4, :cond_9

    .line 2302
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->K:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;

    .line 2303
    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-nez v5, :cond_8

    move v0, v3

    :goto_6
    add-int/2addr v1, v0

    .line 2301
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 2303
    :cond_8
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    add-int/lit8 v0, v0, 0x0

    goto :goto_6

    :cond_9
    move v0, v1

    .line 2306
    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->L:Ljava/util/List;

    if-eqz v1, :cond_c

    .line 2307
    add-int/lit8 v0, v0, 0x18

    .line 2308
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->L:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;

    .line 2309
    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-nez v4, :cond_b

    move v0, v3

    :goto_8
    add-int/2addr v0, v1

    move v1, v0

    .line 2310
    goto :goto_7

    .line 2309
    :cond_b
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    add-int/lit8 v0, v0, 0x0

    goto :goto_8

    :cond_c
    move v1, v0

    .line 2312
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->M:Ljava/util/List;

    if-eqz v0, :cond_e

    .line 2313
    add-int/lit8 v1, v1, 0x18

    .line 2314
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    :goto_9
    if-ge v2, v4, :cond_e

    .line 2315
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->M:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;

    .line 2316
    add-int/lit16 v1, v1, 0xb8

    .line 2314
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_9

    .line 2319
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->E:Ljava/util/List;

    if-eqz v0, :cond_f

    .line 2320
    add-int/lit8 v1, v1, 0x18

    .line 2321
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->E:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    :goto_a
    if-ge v2, v4, :cond_f

    .line 2322
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->E:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;

    .line 2323
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;->b()I

    move-result v0

    add-int/2addr v1, v0

    .line 2321
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_a

    .line 2326
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->H:Ljava/util/List;

    if-eqz v0, :cond_10

    .line 2327
    add-int/lit8 v1, v1, 0x18

    .line 2328
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->H:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    :goto_b
    if-ge v2, v4, :cond_10

    .line 2329
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->H:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;

    .line 2330
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;->b()I

    move-result v0

    add-int/2addr v1, v0

    .line 2328
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_b

    .line 2333
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->N:Ljava/util/List;

    if-eqz v0, :cond_11

    .line 2334
    add-int/lit8 v1, v1, 0x18

    .line 2335
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->N:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    :goto_c
    if-ge v2, v4, :cond_11

    .line 2336
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->N:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/c;

    .line 2337
    add-int/lit16 v1, v1, 0x9c

    .line 2335
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_c

    .line 2340
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->O:Ljava/util/List;

    if-eqz v0, :cond_12

    .line 2341
    add-int/lit8 v1, v1, 0x18

    .line 2342
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->O:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    :goto_d
    if-ge v2, v4, :cond_12

    .line 2343
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->O:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;

    .line 2344
    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->b:I

    add-int/lit8 v0, v0, 0x0

    add-int/2addr v1, v0

    .line 2342
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_d

    .line 2348
    :cond_12
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ad:I

    .line 2349
    const/4 v0, -0x1

    if-ne v2, v0, :cond_14

    .line 2350
    const/16 v2, 0x18

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->Q:Ljava/util/List;

    if-eqz v4, :cond_13

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    :goto_e
    if-ge v3, v5, :cond_13

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/as;

    if-eqz v0, :cond_15

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/m;->k()I

    move-result v0

    add-int/lit8 v0, v0, 0x20

    add-int/2addr v0, v2

    :goto_f
    add-int/lit8 v3, v3, 0x1

    move v2, v0

    goto :goto_e

    .line 2351
    :cond_13
    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->ad:I

    .line 2353
    :cond_14
    add-int v0, v1, v2

    .line 2356
    return v0

    :cond_15
    move v0, v2

    goto :goto_f

    :cond_16
    move v0, v1

    goto/16 :goto_3
.end method
