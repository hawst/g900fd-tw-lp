.class public Lcom/google/android/apps/gmm/place/reservation/ag;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/util/Date;

.field final c:Ljava/lang/Integer;

.field final d:Ljava/lang/Boolean;

.field public e:Ljava/util/Date;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Date;IZ)V
    .locals 6

    .prologue
    .line 23
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/reservation/ag;-><init>(Ljava/lang/String;Ljava/util/Date;IZLjava/util/Date;)V

    .line 24
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/util/Date;IZLjava/util/Date;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/reservation/ag;->a:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/reservation/ag;->b:Ljava/util/Date;

    .line 31
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/ag;->c:Ljava/lang/Integer;

    .line 32
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/ag;->d:Ljava/lang/Boolean;

    .line 33
    iput-object p5, p0, Lcom/google/android/apps/gmm/place/reservation/ag;->e:Ljava/util/Date;

    .line 34
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/ag;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/ag;->b:Ljava/util/Date;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/ag;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/ag;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/ag;->e:Ljava/util/Date;

    if-eqz v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method
