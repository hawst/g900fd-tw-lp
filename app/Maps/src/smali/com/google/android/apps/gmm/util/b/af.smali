.class public Lcom/google/android/apps/gmm/util/b/af;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/iv;)Lcom/google/b/a/an;
    .locals 8
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/util/b/ag;",
            "Lcom/google/o/h/a/iv;",
            ")",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<*>;>;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 31
    sget-object v6, Lcom/google/android/apps/gmm/util/b/l;->a:Lcom/google/android/apps/gmm/util/b/ac;

    .line 33
    invoke-virtual {v6, p2}, Lcom/google/android/apps/gmm/util/b/ac;->a(Lcom/google/o/h/a/iv;)Lcom/google/b/a/an;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v4, v5

    :goto_0
    if-nez v4, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/util/b/ac;->a:Ljava/lang/String;

    move-object v2, v5

    :goto_1
    if-nez v4, :cond_6

    move-object v1, v5

    :goto_2
    if-eqz v2, :cond_8

    if-eqz v1, :cond_8

    new-instance v0, Lcom/google/b/a/an;

    invoke-direct {v0, v2, v1}, Lcom/google/b/a/an;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 34
    :goto_3
    if-eqz v0, :cond_0

    move-object v5, v0

    .line 36
    :cond_0
    return-object v5

    .line 33
    :cond_1
    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/o/h/a/jf;

    move-object v4, v0

    goto :goto_0

    :cond_2
    iget v0, v4, Lcom/google/o/h/a/jf;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/ji;->a(I)Lcom/google/o/h/a/ji;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/o/h/a/ji;->a:Lcom/google/o/h/a/ji;

    move-object v1, v0

    :goto_4
    iget-object v0, p2, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    invoke-static {v0}, Lcom/google/android/apps/gmm/util/b/n;->a(Lcom/google/o/h/a/ir;)Lcom/google/o/h/a/cf;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/util/b/ac;->a:Ljava/lang/String;

    move-object v2, v5

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_4

    :cond_4
    iget-object v0, v6, Lcom/google/android/apps/gmm/util/b/ac;->c:Lcom/google/b/c/ky;

    invoke-interface {v0, v2, v1}, Lcom/google/b/c/ky;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/util/b/p;

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/android/apps/gmm/util/b/ac;->a:Ljava/lang/String;

    const-string v0, "No factory is registered for (data:%s, style:%s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v3, v7

    const/4 v2, 0x1

    aput-object v1, v3, v2

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-object v2, v5

    goto :goto_1

    :cond_5
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/gmm/util/b/p;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/iv;Lcom/google/o/h/a/jf;Lcom/google/android/libraries/curvular/ag;)Lcom/google/android/libraries/curvular/ce;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    :cond_6
    iget-object v1, v6, Lcom/google/android/apps/gmm/util/b/ac;->b:Ljava/util/Map;

    iget v0, v4, Lcom/google/o/h/a/jf;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/ji;->a(I)Lcom/google/o/h/a/ji;

    move-result-object v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/google/o/h/a/ji;->a:Lcom/google/o/h/a/ji;

    :cond_7
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    move-object v1, v0

    goto :goto_2

    :cond_8
    move-object v0, v5

    goto :goto_3
.end method

.method public static a(Lcom/google/o/h/a/iv;)Z
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/apps/gmm/util/b/l;->a:Lcom/google/android/apps/gmm/util/b/ac;

    .line 46
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/util/b/ac;->a(Lcom/google/o/h/a/iv;)Lcom/google/b/a/an;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 47
    :goto_0
    if-nez v0, :cond_1

    .line 48
    const/4 v0, 0x1

    .line 50
    :goto_1
    return v0

    .line 46
    :cond_0
    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/o/h/a/jf;

    goto :goto_0

    .line 50
    :cond_1
    iget-boolean v0, v0, Lcom/google/o/h/a/jf;->d:Z

    goto :goto_1
.end method

.method public static b(Lcom/google/o/h/a/iv;)Z
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/apps/gmm/util/b/l;->a:Lcom/google/android/apps/gmm/util/b/ac;

    .line 55
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/util/b/ac;->a(Lcom/google/o/h/a/iv;)Lcom/google/b/a/an;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 56
    :goto_0
    if-nez v0, :cond_1

    .line 57
    const/4 v0, 0x1

    .line 59
    :goto_1
    return v0

    .line 55
    :cond_0
    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/o/h/a/jf;

    goto :goto_0

    .line 59
    :cond_1
    iget-boolean v0, v0, Lcom/google/o/h/a/jf;->e:Z

    goto :goto_1
.end method

.method public static c(Lcom/google/o/h/a/iv;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 63
    sget-object v0, Lcom/google/android/apps/gmm/util/b/l;->a:Lcom/google/android/apps/gmm/util/b/ac;

    .line 64
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/util/b/ac;->a(Lcom/google/o/h/a/iv;)Lcom/google/b/a/an;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v3, v0, Lcom/google/android/apps/gmm/util/b/ac;->d:Lcom/google/b/c/ky;

    iget-object v4, v2, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    iget-object v0, v2, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/o/h/a/jf;

    iget v0, v0, Lcom/google/o/h/a/jf;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/ji;->a(I)Lcom/google/o/h/a/ji;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/o/h/a/ji;->a:Lcom/google/o/h/a/ji;

    :cond_1
    invoke-interface {v3, v4, v0}, Lcom/google/b/c/ky;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
