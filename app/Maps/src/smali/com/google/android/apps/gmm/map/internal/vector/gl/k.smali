.class public Lcom/google/android/apps/gmm/map/internal/vector/gl/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;


# static fields
.field private static I:Lcom/google/android/apps/gmm/map/util/a/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/i",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/k;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:Ljava/nio/ByteBuffer;

.field private H:Z

.field private a:Lcom/google/android/apps/gmm/shared/c/i;

.field private b:Lcom/google/android/apps/gmm/shared/c/w;

.field private c:Lcom/google/android/apps/gmm/shared/c/i;

.field private d:Lcom/google/android/apps/gmm/shared/c/i;

.field private e:Lcom/google/android/apps/gmm/shared/c/j;

.field private f:Lcom/google/android/apps/gmm/shared/c/w;

.field private g:Lcom/google/android/apps/gmm/shared/c/j;

.field private h:Lcom/google/android/apps/gmm/shared/c/c;

.field private i:Lcom/google/android/apps/gmm/shared/c/i;

.field private j:Lcom/google/android/apps/gmm/shared/c/c;

.field private k:Lcom/google/android/apps/gmm/shared/c/i;

.field private l:Lcom/google/android/apps/gmm/shared/c/c;

.field private m:Lcom/google/android/apps/gmm/shared/c/i;

.field private n:Lcom/google/android/apps/gmm/shared/c/c;

.field private o:Lcom/google/android/apps/gmm/shared/c/i;

.field private p:I

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

.field private u:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

.field private v:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

.field private w:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

.field private x:Z

.field private y:Z

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 147
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/l;

    const/4 v1, 0x3

    const/4 v2, 0x0

    const-string v3, "VertexBuilders"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/l;-><init>(ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->I:Lcom/google/android/apps/gmm/map/util/a/i;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    .line 71
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    .line 72
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->u:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    .line 73
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->v:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    .line 74
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->w:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    .line 85
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    .line 86
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->B:I

    .line 87
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->C:I

    .line 88
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->D:I

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->H:Z

    .line 235
    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    .line 71
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    .line 72
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->u:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    .line 73
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->v:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    .line 74
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->w:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    .line 85
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    .line 86
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->B:I

    .line 87
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->C:I

    .line 88
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->D:I

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->H:Z

    .line 238
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b(IIZI)V

    .line 239
    return-void
.end method

.method public static a(IIZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-result-object v0

    return-object v0
.end method

.method public static a(IIZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;
    .locals 2

    .prologue
    .line 176
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->I:Lcom/google/android/apps/gmm/map/util/a/i;

    monitor-enter v1

    .line 177
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->I:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 178
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b(IIZI)V

    .line 179
    monitor-exit v1

    .line 180
    return-object v0

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(IIZI)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 247
    const/16 v0, 0x7fff

    if-ge p1, v0, :cond_d

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->H:Z

    .line 248
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->E:I

    .line 249
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->y:Z

    .line 250
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_e

    move v0, v1

    :goto_1
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    .line 255
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    .line 256
    and-int/lit8 v0, p2, 0x10

    if-eqz v0, :cond_10

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->q:Z

    .line 257
    and-int/lit8 v0, p2, 0x8

    if-eqz v0, :cond_11

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->s:Z

    .line 258
    and-int/lit8 v0, p2, 0x20

    if-eqz v0, :cond_12

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->r:Z

    .line 260
    and-int/lit16 v0, p2, 0x880

    if-eqz v0, :cond_14

    .line 261
    and-int/lit16 v0, p2, 0x80

    if-eqz v0, :cond_13

    .line 262
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    .line 270
    :goto_5
    and-int/lit16 v0, p2, 0x1100

    if-eqz v0, :cond_16

    .line 271
    and-int/lit16 v0, p2, 0x100

    if-eqz v0, :cond_15

    .line 272
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->u:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    .line 280
    :goto_6
    and-int/lit16 v0, p2, 0x2200

    if-eqz v0, :cond_18

    .line 281
    and-int/lit16 v0, p2, 0x200

    if-eqz v0, :cond_17

    .line 282
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->v:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    .line 290
    :goto_7
    and-int/lit16 v0, p2, 0x4400

    if-eqz v0, :cond_1a

    .line 291
    and-int/lit16 v0, p2, 0x400

    if-eqz v0, :cond_19

    .line 292
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->w:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    .line 300
    :goto_8
    const v0, 0x8000

    and-int/2addr v0, p2

    if-eqz v0, :cond_0

    move v2, v1

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->x:Z

    .line 302
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    if-ne v0, v1, :cond_1b

    .line 303
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/w;

    mul-int/lit8 v1, p1, 0x3

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/w;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    .line 306
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    .line 311
    :cond_1
    :goto_9
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->s:Z

    if-eqz v0, :cond_1c

    .line 312
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    add-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    if-nez v0, :cond_2

    .line 314
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/i;

    shl-int/lit8 v1, p1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    .line 322
    :cond_2
    :goto_a
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->q:Z

    if-eqz v0, :cond_3

    .line 323
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c:Lcom/google/android/apps/gmm/shared/c/i;

    if-nez v0, :cond_3

    .line 325
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/i;

    shl-int/lit8 v1, p1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c:Lcom/google/android/apps/gmm/shared/c/i;

    .line 329
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_1d

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h:Lcom/google/android/apps/gmm/shared/c/c;

    if-nez v0, :cond_4

    .line 331
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v1, p1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h:Lcom/google/android/apps/gmm/shared/c/c;

    .line 333
    :cond_4
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    .line 341
    :cond_5
    :goto_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->u:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_1f

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->j:Lcom/google/android/apps/gmm/shared/c/c;

    if-nez v0, :cond_6

    .line 343
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v1, p1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->j:Lcom/google/android/apps/gmm/shared/c/c;

    .line 345
    :cond_6
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    .line 353
    :cond_7
    :goto_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->v:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_21

    .line 354
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->l:Lcom/google/android/apps/gmm/shared/c/c;

    if-nez v0, :cond_8

    .line 355
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v1, p1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->l:Lcom/google/android/apps/gmm/shared/c/c;

    .line 357
    :cond_8
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    .line 365
    :cond_9
    :goto_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->w:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_23

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->n:Lcom/google/android/apps/gmm/shared/c/c;

    if-nez v0, :cond_a

    .line 367
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v1, p1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->n:Lcom/google/android/apps/gmm/shared/c/c;

    .line 369
    :cond_a
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    .line 377
    :cond_b
    :goto_e
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->H:Z

    if-eqz v0, :cond_25

    .line 378
    if-eqz p3, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    if-nez v0, :cond_c

    .line 380
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/w;

    div-int/lit8 v1, p1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/w;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    .line 388
    :cond_c
    :goto_f
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(I)V

    .line 389
    return-void

    :cond_d
    move v0, v2

    .line 247
    goto/16 :goto_0

    .line 250
    :cond_e
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_f

    move v0, v3

    goto/16 :goto_1

    :cond_f
    move v0, v2

    goto/16 :goto_1

    :cond_10
    move v0, v2

    .line 256
    goto/16 :goto_2

    :cond_11
    move v0, v2

    .line 257
    goto/16 :goto_3

    :cond_12
    move v0, v2

    .line 258
    goto/16 :goto_4

    .line 264
    :cond_13
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    goto/16 :goto_5

    .line 267
    :cond_14
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    goto/16 :goto_5

    .line 274
    :cond_15
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->u:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    goto/16 :goto_6

    .line 277
    :cond_16
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->u:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    goto/16 :goto_6

    .line 284
    :cond_17
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->v:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    goto/16 :goto_7

    .line 287
    :cond_18
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->v:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    goto/16 :goto_7

    .line 294
    :cond_19
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->w:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    goto/16 :goto_8

    .line 297
    :cond_1a
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->w:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    goto/16 :goto_8

    .line 307
    :cond_1b
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    if-ne v0, v3, :cond_1

    .line 308
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/i;

    mul-int/lit8 v1, p1, 0x3

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    .line 309
    const/16 v0, 0xc

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    goto/16 :goto_9

    .line 316
    :cond_1c
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->r:Z

    if-eqz v0, :cond_2

    .line 317
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e:Lcom/google/android/apps/gmm/shared/c/j;

    if-nez v0, :cond_2

    .line 319
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/j;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/shared/c/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e:Lcom/google/android/apps/gmm/shared/c/j;

    goto/16 :goto_a

    .line 334
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_5

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->i:Lcom/google/android/apps/gmm/shared/c/i;

    if-nez v0, :cond_1e

    .line 336
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/i;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/shared/c/i;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->i:Lcom/google/android/apps/gmm/shared/c/i;

    .line 338
    :cond_1e
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    goto/16 :goto_b

    .line 346
    :cond_1f
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->u:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_7

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k:Lcom/google/android/apps/gmm/shared/c/i;

    if-nez v0, :cond_20

    .line 348
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/i;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/shared/c/i;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k:Lcom/google/android/apps/gmm/shared/c/i;

    .line 350
    :cond_20
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    goto/16 :goto_c

    .line 358
    :cond_21
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->v:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_9

    .line 359
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->m:Lcom/google/android/apps/gmm/shared/c/i;

    if-nez v0, :cond_22

    .line 360
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/i;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/shared/c/i;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->m:Lcom/google/android/apps/gmm/shared/c/i;

    .line 362
    :cond_22
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    goto/16 :goto_d

    .line 370
    :cond_23
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->w:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_b

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->o:Lcom/google/android/apps/gmm/shared/c/i;

    if-nez v0, :cond_24

    .line 372
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/i;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/shared/c/i;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->o:Lcom/google/android/apps/gmm/shared/c/i;

    .line 374
    :cond_24
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    goto/16 :goto_e

    .line 383
    :cond_25
    if-eqz p3, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g:Lcom/google/android/apps/gmm/shared/c/j;

    if-nez v0, :cond_c

    .line 384
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/j;

    div-int/lit8 v1, p1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g:Lcom/google/android/apps/gmm/shared/c/j;

    goto/16 :goto_f
.end method


# virtual methods
.method public a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;
    .locals 6

    .prologue
    .line 1214
    if-eqz p2, :cond_0

    .line 1215
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/m;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g()[S

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/m;-><init>(Ljava/nio/ByteBuffer;[S)V

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->D:I

    .line 1216
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->E:I

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;-><init>(Lcom/google/android/apps/gmm/v/aw;IIII)V

    .line 1221
    :goto_0
    return-object v0

    .line 1218
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    .line 1219
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f()Ljava/nio/ByteBuffer;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->D:I

    .line 1221
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g()[S

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->E:I

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;-><init>(Ljava/nio/ByteBuffer;I[SII)V

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h()V

    .line 188
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->I:Lcom/google/android/apps/gmm/map/util/a/i;

    monitor-enter v1

    .line 189
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->I:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z

    .line 190
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(FF)V
    .locals 2

    .prologue
    .line 393
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->C:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->C:I

    .line 394
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->q:Z

    if-nez v0, :cond_0

    .line 395
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Texture coordinate 0 not enabled in this VBO"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 397
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 399
    return-void
.end method

.method public a(FFF)V
    .locals 2

    .prologue
    .line 681
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 682
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 683
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 684
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 685
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    .line 686
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 714
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_1

    .line 715
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    mul-int/2addr v0, p1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 716
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    .line 715
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    .line 731
    :cond_0
    :goto_0
    return-void

    .line 717
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    mul-int/2addr v0, p1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 718
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->z:I

    mul-int/2addr v0, p1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 719
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    .line 718
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 721
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    if-eqz v1, :cond_2

    .line 722
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 727
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 729
    :cond_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method public a(II)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/high16 v5, 0x437f0000    # 255.0f

    .line 776
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->s:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->r:Z

    if-nez v1, :cond_0

    .line 777
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Vertex Colors not enabled in this VBO"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 779
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->s:Z

    if-eqz v1, :cond_1

    .line 780
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v5

    .line 781
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    .line 782
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    .line 783
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v5

    .line 784
    :goto_0
    if-ge v0, p2, :cond_2

    .line 785
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v5, v1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 786
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 787
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v5, v3}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 788
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 784
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 791
    :cond_1
    :goto_1
    if-ge v0, p2, :cond_2

    .line 792
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e:Lcom/google/android/apps/gmm/shared/c/j;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/shared/c/j;->a(I)V

    .line 791
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 795
    :cond_2
    return-void
.end method

.method public a(III)V
    .locals 2

    .prologue
    .line 1178
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    if-eqz v0, :cond_0

    .line 1179
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    int-to-short v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 1180
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    int-to-short v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 1181
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    int-to-short v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 1187
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->B:I

    add-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->B:I

    .line 1188
    return-void

    .line 1183
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g:Lcom/google/android/apps/gmm/shared/c/j;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/shared/c/j;->a(I)V

    .line 1184
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g:Lcom/google/android/apps/gmm/shared/c/j;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/shared/c/j;->a(I)V

    .line 1185
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g:Lcom/google/android/apps/gmm/shared/c/j;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/gmm/shared/c/j;->a(I)V

    goto :goto_0
.end method

.method public a(IIII)V
    .locals 2

    .prologue
    .line 1158
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    if-eqz v0, :cond_0

    .line 1159
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    int-to-short v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 1160
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    int-to-short v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 1161
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    int-to-short v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 1162
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    int-to-short v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 1163
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    int-to-short v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 1164
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    int-to-short v1, p4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 1173
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->B:I

    add-int/lit8 v0, v0, 0x6

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->B:I

    .line 1174
    return-void

    .line 1166
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g:Lcom/google/android/apps/gmm/shared/c/j;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/shared/c/j;->a(I)V

    .line 1167
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g:Lcom/google/android/apps/gmm/shared/c/j;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/shared/c/j;->a(I)V

    .line 1168
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g:Lcom/google/android/apps/gmm/shared/c/j;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/gmm/shared/c/j;->a(I)V

    .line 1169
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g:Lcom/google/android/apps/gmm/shared/c/j;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/gmm/shared/c/j;->a(I)V

    .line 1170
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g:Lcom/google/android/apps/gmm/shared/c/j;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/shared/c/j;->a(I)V

    .line 1171
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g:Lcom/google/android/apps/gmm/shared/c/j;

    invoke-virtual {v0, p4}, Lcom/google/android/apps/gmm/shared/c/j;->a(I)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 3

    .prologue
    .line 409
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    if-nez v0, :cond_0

    .line 413
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Setting vert position without specifying position type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 415
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 416
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v2, :cond_1

    int-to-short v0, v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 417
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v2, :cond_2

    int-to-short v0, v0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 418
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->x:Z

    if-eqz v0, :cond_4

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v2, :cond_3

    int-to-short v0, v0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 424
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-eq v0, v1, :cond_6

    .line 425
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expecting UserData0"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 416
    :cond_1
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v2

    int-to-short v0, v0

    goto :goto_0

    .line 417
    :cond_2
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v2

    int-to-short v0, v0

    goto :goto_1

    .line 418
    :cond_3
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v2

    int-to-short v0, v0

    goto :goto_2

    :cond_4
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-short v0, v0

    goto :goto_2

    .line 420
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 422
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    goto :goto_3

    .line 427
    :cond_6
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    .line 428
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/b/a/y;F)V
    .locals 3

    .prologue
    .line 454
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    if-nez v0, :cond_0

    .line 455
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Setting vert position without specifying position type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 457
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 458
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v2, :cond_1

    int-to-short v0, v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 459
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v2, :cond_2

    int-to-short v0, v0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 460
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->x:Z

    if-eqz v0, :cond_4

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v2, :cond_3

    int-to-short v0, v0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 466
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-eq v0, v1, :cond_6

    .line 467
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not expecting single float for user data"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 458
    :cond_1
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v2

    int-to-short v0, v0

    goto :goto_0

    .line 459
    :cond_2
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v2

    int-to-short v0, v0

    goto :goto_1

    .line 460
    :cond_3
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v2

    int-to-short v0, v0

    goto :goto_2

    :cond_4
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-short v0, v0

    goto :goto_2

    .line 462
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 464
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    goto :goto_3

    .line 469
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->i:Lcom/google/android/apps/gmm/shared/c/i;

    if-eqz v0, :cond_7

    .line 470
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->i:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 472
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->u:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-eq v0, v1, :cond_8

    .line 473
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expecting UserData1"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 475
    :cond_8
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    .line 476
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/b/a/y;FF)V
    .locals 3

    .prologue
    .line 497
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    if-nez v0, :cond_0

    .line 498
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Setting vert position without specifying position type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    .line 501
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v2, :cond_2

    int-to-short v0, v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 502
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v2, :cond_3

    int-to-short v0, v0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 503
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->x:Z

    if-eqz v0, :cond_5

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v2, :cond_4

    int-to-short v0, v0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 509
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->u:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-eq v0, v1, :cond_7

    .line 510
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not expecting single floats for user data"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 501
    :cond_2
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v2

    int-to-short v0, v0

    goto :goto_0

    .line 502
    :cond_3
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v2

    int-to-short v0, v0

    goto :goto_1

    .line 503
    :cond_4
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v2

    int-to-short v0, v0

    goto :goto_2

    :cond_5
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-short v0, v0

    goto :goto_2

    .line 505
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    goto :goto_3

    .line 512
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->i:Lcom/google/android/apps/gmm/shared/c/i;

    if-eqz v0, :cond_8

    .line 513
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->i:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 514
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 516
    :cond_8
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    .line 517
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;[B)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    .line 548
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    .line 549
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v3, :cond_0

    int-to-short v0, v0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 550
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v3, :cond_1

    int-to-short v0, v0

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 551
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->x:Z

    if-eqz v0, :cond_3

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v3, :cond_2

    int-to-short v0, v0

    :goto_2
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 557
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h:Lcom/google/android/apps/gmm/shared/c/c;

    if-eqz v0, :cond_5

    move v0, v1

    .line 558
    :goto_4
    if-ge v0, v4, :cond_5

    .line 559
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h:Lcom/google/android/apps/gmm/shared/c/c;

    aget-byte v3, p2, v0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c;->a(B)V

    .line 558
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 549
    :cond_0
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v3

    int-to-short v0, v0

    goto :goto_0

    .line 550
    :cond_1
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v3

    int-to-short v0, v0

    goto :goto_1

    .line 551
    :cond_2
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v3

    int-to-short v0, v0

    goto :goto_2

    :cond_3
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-short v0, v0

    goto :goto_2

    .line 553
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 554
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 555
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    goto :goto_3

    .line 562
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->j:Lcom/google/android/apps/gmm/shared/c/c;

    if-eqz v0, :cond_6

    move v0, v1

    .line 563
    :goto_5
    if-ge v0, v4, :cond_6

    .line 564
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->j:Lcom/google/android/apps/gmm/shared/c/c;

    add-int/lit8 v3, v0, 0x4

    aget-byte v3, p2, v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c;->a(B)V

    .line 563
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 567
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->l:Lcom/google/android/apps/gmm/shared/c/c;

    if-eqz v0, :cond_7

    move v0, v1

    .line 568
    :goto_6
    if-ge v0, v4, :cond_7

    .line 569
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->l:Lcom/google/android/apps/gmm/shared/c/c;

    add-int/lit8 v3, v0, 0x8

    aget-byte v3, p2, v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c;->a(B)V

    .line 568
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 572
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->n:Lcom/google/android/apps/gmm/shared/c/c;

    if-eqz v0, :cond_8

    move v0, v1

    .line 573
    :goto_7
    if-ge v0, v4, :cond_8

    .line 574
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->n:Lcom/google/android/apps/gmm/shared/c/c;

    add-int/lit8 v2, v0, 0xc

    aget-byte v2, p2, v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/c;->a(B)V

    .line 573
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 577
    :cond_8
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    .line 578
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;[B[F)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x4

    const/4 v1, 0x0

    .line 617
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    if-ne v0, v5, :cond_4

    .line 618
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v3, :cond_0

    int-to-short v0, v0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 619
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v3, :cond_1

    int-to-short v0, v0

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 620
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->x:Z

    if-eqz v0, :cond_3

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    if-gtz v3, :cond_2

    int-to-short v0, v0

    :goto_2
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 627
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h:Lcom/google/android/apps/gmm/shared/c/c;

    if-eqz v0, :cond_5

    move v0, v1

    .line 628
    :goto_4
    if-ge v0, v4, :cond_6

    .line 629
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h:Lcom/google/android/apps/gmm/shared/c/c;

    aget-byte v3, p2, v0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c;->a(B)V

    .line 628
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 618
    :cond_0
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v3

    int-to-short v0, v0

    goto :goto_0

    .line 619
    :cond_1
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v3

    int-to-short v0, v0

    goto :goto_1

    .line 620
    :cond_2
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->F:I

    shr-int/2addr v0, v3

    int-to-short v0, v0

    goto :goto_2

    :cond_3
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-short v0, v0

    goto :goto_2

    .line 622
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 623
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 624
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    goto :goto_3

    .line 631
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->i:Lcom/google/android/apps/gmm/shared/c/i;

    if-eqz v0, :cond_6

    .line 632
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->i:Lcom/google/android/apps/gmm/shared/c/i;

    aget v2, p3, v1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 635
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->j:Lcom/google/android/apps/gmm/shared/c/c;

    if-eqz v0, :cond_7

    move v0, v1

    .line 636
    :goto_5
    if-ge v0, v4, :cond_8

    .line 637
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->j:Lcom/google/android/apps/gmm/shared/c/c;

    add-int/lit8 v3, v0, 0x4

    aget-byte v3, p2, v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c;->a(B)V

    .line 636
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 639
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k:Lcom/google/android/apps/gmm/shared/c/i;

    if-eqz v0, :cond_8

    .line 640
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k:Lcom/google/android/apps/gmm/shared/c/i;

    aget v2, p3, v5

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 643
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->l:Lcom/google/android/apps/gmm/shared/c/c;

    if-eqz v0, :cond_9

    move v0, v1

    .line 644
    :goto_6
    if-ge v0, v4, :cond_a

    .line 645
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->l:Lcom/google/android/apps/gmm/shared/c/c;

    add-int/lit8 v3, v0, 0x8

    aget-byte v3, p2, v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c;->a(B)V

    .line 644
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 647
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->m:Lcom/google/android/apps/gmm/shared/c/i;

    if-eqz v0, :cond_a

    .line 648
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->m:Lcom/google/android/apps/gmm/shared/c/i;

    const/4 v2, 0x2

    aget v2, p3, v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 651
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->n:Lcom/google/android/apps/gmm/shared/c/c;

    if-eqz v0, :cond_b

    move v0, v1

    .line 652
    :goto_7
    if-ge v0, v4, :cond_c

    .line 653
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->n:Lcom/google/android/apps/gmm/shared/c/c;

    add-int/lit8 v2, v0, 0xc

    aget-byte v2, p2, v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/c;->a(B)V

    .line 652
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 655
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->m:Lcom/google/android/apps/gmm/shared/c/i;

    if-eqz v0, :cond_c

    .line 656
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->o:Lcom/google/android/apps/gmm/shared/c/i;

    const/4 v1, 0x3

    aget v1, p3, v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 659
    :cond_c
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    .line 660
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/shared/c/w;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1065
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    iget-object v1, v0, Lcom/google/android/apps/gmm/shared/c/w;->a:[S

    array-length v1, v1

    iget v2, v0, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    add-int/2addr v2, v3

    if-ge v1, v2, :cond_0

    iget v1, v0, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    iget v2, p1, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    add-int/2addr v1, v2

    new-array v1, v1, [S

    iget-object v2, v0, Lcom/google/android/apps/gmm/shared/c/w;->a:[S

    iget v3, v0, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    invoke-static {v2, v5, v1, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p1, Lcom/google/android/apps/gmm/shared/c/w;->a:[S

    iget v3, v0, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    iget v4, p1, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    invoke-static {v2, v5, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/shared/c/w;->a:[S

    iget-object v1, v0, Lcom/google/android/apps/gmm/shared/c/w;->a:[S

    array-length v1, v1

    iput v1, v0, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    .line 1066
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->B:I

    iget v1, p1, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->B:I

    .line 1067
    return-void

    .line 1065
    :cond_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/shared/c/w;->a:[S

    iget-object v2, v0, Lcom/google/android/apps/gmm/shared/c/w;->a:[S

    iget v3, v0, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    iget v4, p1, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    invoke-static {v1, v5, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, v0, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    iget v2, p1, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    goto :goto_0
.end method

.method public a([FII)V
    .locals 3

    .prologue
    .line 1114
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->C:I

    div-int/lit8 v1, p3, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->C:I

    move v0, p2

    .line 1115
    :goto_0
    add-int v1, p2, p3

    if-ge v0, v1, :cond_0

    .line 1116
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c:Lcom/google/android/apps/gmm/shared/c/i;

    aget v2, p1, v0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 1115
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1118
    :cond_0
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 799
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    return v0
.end method

.method public b(I)V
    .locals 5

    .prologue
    const/high16 v4, 0x437f0000    # 255.0f

    .line 745
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->s:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->r:Z

    if-nez v0, :cond_0

    .line 746
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Vertex Colors not enabled in this VBO"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 748
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->s:Z

    if-eqz v0, :cond_1

    .line 749
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    .line 750
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    .line 751
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    .line 752
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    .line 754
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 755
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 756
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 757
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 761
    :goto_0
    return-void

    .line 759
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e:Lcom/google/android/apps/gmm/shared/c/j;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/shared/c/j;->a(I)V

    goto :goto_0
.end method

.method public b(II)V
    .locals 3

    .prologue
    const/high16 v2, 0x47800000    # 65536.0f

    .line 1122
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->C:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->C:I

    .line 1123
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c:Lcom/google/android/apps/gmm/shared/c/i;

    int-to-float v1, p1

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 1124
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c:Lcom/google/android/apps/gmm/shared/c/i;

    int-to-float v1, p2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/i;->a(F)V

    .line 1125
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 804
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->B:I

    return v0
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 1148
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    if-eqz v0, :cond_0

    .line 1149
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    int-to-short v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/w;->a(S)V

    .line 1153
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->B:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->B:I

    .line 1154
    return-void

    .line 1151
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g:Lcom/google/android/apps/gmm/shared/c/j;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/shared/c/j;->a(I)V

    goto :goto_0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 810
    return-void
.end method

.method public e()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 824
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    if-ne v0, v6, :cond_d

    .line 825
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    if-nez v0, :cond_1

    .line 993
    :cond_0
    :goto_0
    return-void

    .line 828
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    div-int/lit8 v0, v0, 0x3

    :goto_1
    move v1, v2

    .line 858
    :goto_2
    if-ge v1, v0, :cond_1a

    .line 889
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    if-ne v3, v6, :cond_18

    .line 890
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    mul-int/lit8 v5, v1, 0x3

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/w;->a:[S

    aget-short v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 891
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    mul-int/lit8 v5, v1, 0x3

    add-int/lit8 v5, v5, 0x1

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/w;->a:[S

    aget-short v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 892
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    mul-int/lit8 v5, v1, 0x3

    add-int/lit8 v5, v5, 0x2

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/w;->a:[S

    aget-short v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 893
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 899
    :cond_2
    :goto_3
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->s:Z

    if-eqz v3, :cond_19

    .line 900
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    shl-int/lit8 v5, v1, 0x2

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/i;->a:[F

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 901
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x1

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/i;->a:[F

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 902
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x2

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/i;->a:[F

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 903
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x3

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/i;->a:[F

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 912
    :cond_3
    :goto_4
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->q:Z

    if-eqz v3, :cond_4

    .line 913
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c:Lcom/google/android/apps/gmm/shared/c/i;

    shl-int/lit8 v5, v1, 0x1

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/i;->a:[F

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 914
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c:Lcom/google/android/apps/gmm/shared/c/i;

    shl-int/lit8 v5, v1, 0x1

    add-int/lit8 v5, v5, 0x1

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/i;->a:[F

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 916
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v3, v4, :cond_5

    .line 917
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 918
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x1

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 919
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x2

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 920
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x3

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 922
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v3, v4, :cond_6

    .line 923
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->i:Lcom/google/android/apps/gmm/shared/c/i;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/i;->a:[F

    aget v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 925
    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->u:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v3, v4, :cond_7

    .line 926
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->j:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 927
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->j:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x1

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 928
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->j:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x2

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 929
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->j:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x3

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 931
    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->u:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v3, v4, :cond_8

    .line 932
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k:Lcom/google/android/apps/gmm/shared/c/i;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/i;->a:[F

    aget v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 934
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->v:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v3, v4, :cond_9

    .line 935
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->l:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 936
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->l:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x1

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 937
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->l:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x2

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 938
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->l:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x3

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 940
    :cond_9
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->v:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v3, v4, :cond_a

    .line 941
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->m:Lcom/google/android/apps/gmm/shared/c/i;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/i;->a:[F

    aget v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 943
    :cond_a
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->w:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v3, v4, :cond_b

    .line 944
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->n:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 945
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->n:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x1

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 946
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->n:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x2

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 947
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->n:Lcom/google/android/apps/gmm/shared/c/c;

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v5, v5, 0x3

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/c;->a:[B

    aget-byte v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 949
    :cond_b
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->w:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v3, v4, :cond_c

    .line 950
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->o:Lcom/google/android/apps/gmm/shared/c/i;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/i;->a:[F

    aget v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 888
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 829
    :cond_d
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    if-ne v0, v7, :cond_e

    .line 830
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    if-eqz v0, :cond_0

    .line 833
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    div-int/lit8 v0, v0, 0x3

    goto/16 :goto_1

    .line 834
    :cond_e
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->s:Z

    if-eqz v0, :cond_f

    .line 835
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    div-int/lit8 v0, v0, 0x4

    goto/16 :goto_1

    .line 836
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->r:Z

    if-eqz v0, :cond_10

    .line 837
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e:Lcom/google/android/apps/gmm/shared/c/j;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/j;->b:I

    goto/16 :goto_1

    .line 838
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_11

    .line 839
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h:Lcom/google/android/apps/gmm/shared/c/c;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/c;->b:I

    div-int/lit8 v0, v0, 0x4

    goto/16 :goto_1

    .line 840
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->t:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_12

    .line 841
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->i:Lcom/google/android/apps/gmm/shared/c/i;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    goto/16 :goto_1

    .line 842
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->u:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_13

    .line 843
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->j:Lcom/google/android/apps/gmm/shared/c/c;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/c;->b:I

    div-int/lit8 v0, v0, 0x4

    goto/16 :goto_1

    .line 844
    :cond_13
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->u:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_14

    .line 845
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k:Lcom/google/android/apps/gmm/shared/c/i;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    goto/16 :goto_1

    .line 846
    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->v:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_15

    .line 847
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->l:Lcom/google/android/apps/gmm/shared/c/c;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/c;->b:I

    div-int/lit8 v0, v0, 0x4

    goto/16 :goto_1

    .line 848
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->v:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_16

    .line 849
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->m:Lcom/google/android/apps/gmm/shared/c/i;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    goto/16 :goto_1

    .line 850
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->w:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_17

    .line 851
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->n:Lcom/google/android/apps/gmm/shared/c/c;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/c;->b:I

    div-int/lit8 v0, v0, 0x4

    goto/16 :goto_1

    .line 852
    :cond_17
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->w:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/n;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/n;

    if-ne v0, v1, :cond_0

    .line 853
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->o:Lcom/google/android/apps/gmm/shared/c/i;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    goto/16 :goto_1

    .line 894
    :cond_18
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    if-ne v3, v7, :cond_2

    .line 895
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    mul-int/lit8 v5, v1, 0x3

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/i;->a:[F

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 896
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    mul-int/lit8 v5, v1, 0x3

    add-int/lit8 v5, v5, 0x1

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/i;->a:[F

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 897
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    mul-int/lit8 v5, v1, 0x3

    add-int/lit8 v5, v5, 0x2

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/c/i;->a:[F

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    goto/16 :goto_3

    .line 904
    :cond_19
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->r:Z

    if-eqz v3, :cond_3

    .line 906
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e:Lcom/google/android/apps/gmm/shared/c/j;

    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/c/j;->a:[I

    aget v3, v3, v1

    .line 907
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    shr-int/lit8 v5, v3, 0x10

    and-int/lit16 v5, v5, 0xff

    int-to-byte v5, v5

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 908
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    shr-int/lit8 v5, v3, 0x8

    and-int/lit16 v5, v5, 0xff

    int-to-byte v5, v5

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 909
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    int-to-byte v5, v3

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 910
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    shr-int/lit8 v3, v3, 0x18

    int-to-byte v3, v3

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto/16 :goto_4

    .line 954
    :cond_1a
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->D:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->D:I

    .line 956
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    if-ne v0, v6, :cond_24

    .line 957
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    .line 961
    :cond_1b
    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c:Lcom/google/android/apps/gmm/shared/c/i;

    if-eqz v0, :cond_1c

    .line 962
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c:Lcom/google/android/apps/gmm/shared/c/i;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    .line 964
    :cond_1c
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    if-eqz v0, :cond_1d

    .line 965
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    .line 967
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e:Lcom/google/android/apps/gmm/shared/c/j;

    if-eqz v0, :cond_1e

    .line 968
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e:Lcom/google/android/apps/gmm/shared/c/j;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/j;->b:I

    .line 970
    :cond_1e
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h:Lcom/google/android/apps/gmm/shared/c/c;

    if-eqz v0, :cond_1f

    .line 971
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h:Lcom/google/android/apps/gmm/shared/c/c;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/c;->b:I

    .line 974
    :cond_1f
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->i:Lcom/google/android/apps/gmm/shared/c/i;

    if-eqz v0, :cond_20

    .line 975
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->i:Lcom/google/android/apps/gmm/shared/c/i;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    .line 978
    :cond_20
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->j:Lcom/google/android/apps/gmm/shared/c/c;

    if-eqz v0, :cond_21

    .line 979
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->j:Lcom/google/android/apps/gmm/shared/c/c;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/c;->b:I

    .line 982
    :cond_21
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k:Lcom/google/android/apps/gmm/shared/c/i;

    if-eqz v0, :cond_22

    .line 983
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k:Lcom/google/android/apps/gmm/shared/c/i;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    .line 986
    :cond_22
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->l:Lcom/google/android/apps/gmm/shared/c/c;

    if-eqz v0, :cond_23

    .line 987
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->l:Lcom/google/android/apps/gmm/shared/c/c;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/c;->b:I

    .line 990
    :cond_23
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->n:Lcom/google/android/apps/gmm/shared/c/c;

    if-eqz v0, :cond_0

    .line 991
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->n:Lcom/google/android/apps/gmm/shared/c/c;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/c;->b:I

    goto/16 :goto_0

    .line 958
    :cond_24
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    if-ne v0, v7, :cond_1b

    .line 959
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    goto :goto_5
.end method

.method public f()Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    .line 1007
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    .line 1008
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    .line 1010
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1011
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    .line 1012
    return-object v0
.end method

.method public g()[S
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1019
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->y:Z

    if-nez v0, :cond_1

    .line 1020
    :cond_0
    const/4 v0, 0x0

    .line 1036
    :goto_0
    return-object v0

    .line 1023
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    iget v0, v1, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    new-array v0, v0, [S

    iget-object v2, v1, Lcom/google/android/apps/gmm/shared/c/w;->a:[S

    iget v1, v1, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    invoke-static {v2, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public h()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1078
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    if-eqz v0, :cond_0

    .line 1079
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->f:Lcom/google/android/apps/gmm/shared/c/w;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    .line 1081
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g:Lcom/google/android/apps/gmm/shared/c/j;

    if-eqz v0, :cond_1

    .line 1082
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->g:Lcom/google/android/apps/gmm/shared/c/j;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/j;->b:I

    .line 1084
    :cond_1
    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->B:I

    .line 1085
    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    .line 1086
    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->C:I

    .line 1087
    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->D:I

    .line 1088
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    .line 1089
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b:Lcom/google/android/apps/gmm/shared/c/w;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    .line 1093
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    if-eqz v0, :cond_3

    .line 1094
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d:Lcom/google/android/apps/gmm/shared/c/i;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    .line 1096
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e:Lcom/google/android/apps/gmm/shared/c/j;

    if-eqz v0, :cond_4

    .line 1097
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e:Lcom/google/android/apps/gmm/shared/c/j;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/j;->b:I

    .line 1099
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c:Lcom/google/android/apps/gmm/shared/c/i;

    if-eqz v0, :cond_5

    .line 1100
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c:Lcom/google/android/apps/gmm/shared/c/i;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    .line 1102
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_6

    .line 1103
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1105
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->G:Ljava/nio/ByteBuffer;

    .line 1106
    return-void

    .line 1090
    :cond_7
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->p:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1091
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a:Lcom/google/android/apps/gmm/shared/c/i;

    iput v2, v0, Lcom/google/android/apps/gmm/shared/c/i;->b:I

    goto :goto_0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 1129
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->C:I

    return v0
.end method

.method public j()V
    .locals 0

    .prologue
    .line 1135
    return-void
.end method

.method public k()I
    .locals 1

    .prologue
    .line 1143
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->A:I

    return v0
.end method
