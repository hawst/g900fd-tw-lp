.class public Lcom/google/android/apps/gmm/cardui/n;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/b/c;


# instance fields
.field private final a:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/google/android/apps/gmm/cardui/o;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 41
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/n;->a:Ljava/util/WeakHashMap;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/o/h/a/mr;)Landroid/app/Fragment;
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 84
    new-instance v2, Lcom/google/android/apps/gmm/cardui/f/d;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/cardui/f/d;-><init>()V

    .line 85
    iget-object v3, v2, Lcom/google/android/apps/gmm/cardui/f/d;->a:Lcom/google/android/apps/gmm/cardui/f/c;

    new-array v4, v0, [Lcom/google/android/apps/gmm/base/g/c;

    aput-object p2, v4, v1

    if-nez v4, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    array-length v5, v4

    if-ltz v5, :cond_1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const-wide/16 v6, 0x5

    int-to-long v8, v5

    add-long/2addr v6, v8

    div-int/lit8 v0, v5, 0xa

    int-to-long v8, v0

    add-long/2addr v6, v8

    const-wide/32 v8, 0x7fffffff

    cmp-long v0, v6, v8

    if-lez v0, :cond_3

    const v0, 0x7fffffff

    :goto_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v5, v4}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-virtual {v3, v5}, Lcom/google/android/apps/gmm/cardui/f/c;->a(Ljava/util/List;)V

    .line 87
    invoke-static {}, Lcom/google/o/h/a/nt;->newBuilder()Lcom/google/o/h/a/nv;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/o/h/a/nv;->a(Lcom/google/o/h/a/mr;)Lcom/google/o/h/a/nv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/h/a/nv;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nt;

    .line 86
    iput-object v0, v2, Lcom/google/android/apps/gmm/cardui/f/d;->b:Lcom/google/o/h/a/nt;

    .line 88
    iget-object v0, v2, Lcom/google/android/apps/gmm/cardui/f/d;->a:Lcom/google/android/apps/gmm/cardui/f/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/cardui/f/c;->e(I)V

    .line 89
    invoke-static {v2}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v1

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;

    move-result-object v0

    return-object v0

    .line 85
    :cond_3
    const-wide/32 v8, -0x80000000

    cmp-long v0, v6, v8

    if-gez v0, :cond_4

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_4
    long-to-int v0, v6

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/cardui/b/b;Ljava/lang/String;Ljava/util/List;Lcom/google/o/h/a/nt;)Landroid/app/Fragment;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/cardui/b/b;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/br;",
            ">;",
            "Lcom/google/o/h/a/nt;",
            ")",
            "Landroid/app/Fragment;"
        }
    .end annotation

    .prologue
    .line 74
    .line 76
    iget-object v0, p4, Lcom/google/o/h/a/nt;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/d/a/a/ds;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 74
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/cardui/n;->a(Lcom/google/android/apps/gmm/cardui/b/b;Ljava/lang/String;Ljava/util/List;Lcom/google/o/h/a/nt;Lcom/google/android/apps/gmm/map/b/a/j;)Landroid/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/cardui/b/b;Ljava/lang/String;Ljava/util/List;Lcom/google/o/h/a/nt;Lcom/google/android/apps/gmm/map/b/a/j;)Landroid/app/Fragment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/cardui/b/b;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/br;",
            ">;",
            "Lcom/google/o/h/a/nt;",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            ")",
            "Landroid/app/Fragment;"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Lcom/google/android/apps/gmm/cardui/f/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/cardui/f/d;-><init>()V

    .line 102
    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/f/d;->a:Lcom/google/android/apps/gmm/cardui/f/c;

    invoke-static {p3}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/cardui/f/c;->a(Ljava/util/List;)V

    .line 103
    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/f/d;->a:Lcom/google/android/apps/gmm/cardui/f/c;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/gmm/cardui/f/c;->b(Ljava/lang/String;)V

    .line 104
    iput-object p4, v0, Lcom/google/android/apps/gmm/cardui/f/d;->b:Lcom/google/o/h/a/nt;

    .line 105
    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/f/d;->a:Lcom/google/android/apps/gmm/cardui/f/c;

    invoke-virtual {v1, p5}, Lcom/google/android/apps/gmm/cardui/f/c;->b(Lcom/google/android/apps/gmm/map/b/a/j;)I

    move-result v1

    .line 106
    if-ltz v1, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/f/d;->a:Lcom/google/android/apps/gmm/cardui/f/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/cardui/f/c;->b()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 107
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/f/d;->a:Lcom/google/android/apps/gmm/cardui/f/c;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/cardui/f/c;->e(I)V

    .line 109
    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v1

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;

    move-result-object v0

    return-object v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/cardui/o;)V
    .locals 2

    .prologue
    .line 58
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/n;->a:Ljava/util/WeakHashMap;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    monitor-exit p0

    return-void

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(Lcom/google/android/apps/gmm/cardui/o;)V
    .locals 1

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/n;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    monitor-exit p0

    return-void

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
