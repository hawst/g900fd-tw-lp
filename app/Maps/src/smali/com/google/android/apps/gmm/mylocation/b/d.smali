.class public final enum Lcom/google/android/apps/gmm/mylocation/b/d;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/mylocation/b/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/mylocation/b/d;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum b:Lcom/google/android/apps/gmm/mylocation/b/d;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum c:Lcom/google/android/apps/gmm/mylocation/b/d;

.field public static final enum d:Lcom/google/android/apps/gmm/mylocation/b/d;

.field public static final enum e:Lcom/google/android/apps/gmm/mylocation/b/d;

.field public static final enum f:Lcom/google/android/apps/gmm/mylocation/b/d;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum g:Lcom/google/android/apps/gmm/mylocation/b/d;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum h:Lcom/google/android/apps/gmm/mylocation/b/d;

.field private static final synthetic k:[Lcom/google/android/apps/gmm/mylocation/b/d;


# instance fields
.field public i:Z

.field public j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/b/d;

    const-string v1, "ALREADY_OPTIMIZED"

    invoke-direct {v0, v1, v4, v4, v3}, Lcom/google/android/apps/gmm/mylocation/b/d;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->a:Lcom/google/android/apps/gmm/mylocation/b/d;

    .line 78
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/b/d;

    const-string v1, "DIALOGS_ARE_SUPPRESSED"

    invoke-direct {v0, v1, v3, v4, v3}, Lcom/google/android/apps/gmm/mylocation/b/d;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->b:Lcom/google/android/apps/gmm/mylocation/b/d;

    .line 83
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/b/d;

    const-string v1, "OPTIMIZED_OR_DIALOG_SUPPRESSED"

    invoke-direct {v0, v1, v5, v4, v3}, Lcom/google/android/apps/gmm/mylocation/b/d;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->c:Lcom/google/android/apps/gmm/mylocation/b/d;

    .line 88
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/b/d;

    const-string v1, "NO_LOCATION_DEVICE"

    invoke-direct {v0, v1, v6, v4, v4}, Lcom/google/android/apps/gmm/mylocation/b/d;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->d:Lcom/google/android/apps/gmm/mylocation/b/d;

    .line 93
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/b/d;

    const-string v1, "ANOTHER_DIALOG_SHOWN"

    invoke-direct {v0, v1, v7, v4, v4}, Lcom/google/android/apps/gmm/mylocation/b/d;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->e:Lcom/google/android/apps/gmm/mylocation/b/d;

    .line 99
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/b/d;

    const-string v1, "LOCATION_IS_DISABLED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/google/android/apps/gmm/mylocation/b/d;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->f:Lcom/google/android/apps/gmm/mylocation/b/d;

    .line 104
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/b/d;

    const-string v1, "LOCATION_IS_NOT_OPTIMIZED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/google/android/apps/gmm/mylocation/b/d;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->g:Lcom/google/android/apps/gmm/mylocation/b/d;

    .line 109
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/b/d;

    const-string v1, "NOT_OPTIMIZED_OR_DISABLED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/google/android/apps/gmm/mylocation/b/d;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->h:Lcom/google/android/apps/gmm/mylocation/b/d;

    .line 68
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/apps/gmm/mylocation/b/d;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/b/d;->a:Lcom/google/android/apps/gmm/mylocation/b/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/b/d;->b:Lcom/google/android/apps/gmm/mylocation/b/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/b/d;->c:Lcom/google/android/apps/gmm/mylocation/b/d;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/b/d;->d:Lcom/google/android/apps/gmm/mylocation/b/d;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/b/d;->e:Lcom/google/android/apps/gmm/mylocation/b/d;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/mylocation/b/d;->f:Lcom/google/android/apps/gmm/mylocation/b/d;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/mylocation/b/d;->g:Lcom/google/android/apps/gmm/mylocation/b/d;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/mylocation/b/d;->h:Lcom/google/android/apps/gmm/mylocation/b/d;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->k:[Lcom/google/android/apps/gmm/mylocation/b/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 115
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/mylocation/b/d;->i:Z

    .line 116
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/mylocation/b/d;->j:Z

    .line 117
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/mylocation/b/d;
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/google/android/apps/gmm/mylocation/b/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/b/d;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/mylocation/b/d;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->k:[Lcom/google/android/apps/gmm/mylocation/b/d;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/mylocation/b/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/mylocation/b/d;

    return-object v0
.end method
