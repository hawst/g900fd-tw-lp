.class public final enum Lcom/google/android/apps/gmm/map/internal/c/ar;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/internal/c/ar;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/internal/c/ar;

.field public static final enum b:Lcom/google/android/apps/gmm/map/internal/c/ar;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/map/internal/c/ar;


# instance fields
.field public c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 17
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ar;

    const-string v1, "STICKY"

    const/16 v2, 0x100

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/map/internal/c/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ar;->a:Lcom/google/android/apps/gmm/map/internal/c/ar;

    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ar;

    const-string v1, "GHOSTED"

    const/16 v2, 0x200

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/internal/c/ar;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ar;->b:Lcom/google/android/apps/gmm/map/internal/c/ar;

    .line 15
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/ar;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/ar;->a:Lcom/google/android/apps/gmm/map/internal/c/ar;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/ar;->b:Lcom/google/android/apps/gmm/map/internal/c/ar;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ar;->d:[Lcom/google/android/apps/gmm/map/internal/c/ar;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/ar;->c:I

    .line 25
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/ar;
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/google/android/apps/gmm/map/internal/c/ar;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/ar;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/internal/c/ar;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/ar;->d:[Lcom/google/android/apps/gmm/map/internal/c/ar;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/internal/c/ar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/c/ar;

    return-object v0
.end method
