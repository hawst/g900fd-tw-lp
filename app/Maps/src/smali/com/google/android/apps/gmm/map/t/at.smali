.class public Lcom/google/android/apps/gmm/map/t/at;
.super Lcom/google/android/apps/gmm/map/t/ar;
.source "PG"


# instance fields
.field private f:Lcom/google/android/apps/gmm/v/bt;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/v/co;)V
    .locals 8

    .prologue
    const/16 v3, 0x38

    const/16 v5, 0x1e01

    const/4 v4, 0x0

    const/high16 v1, -0x80000000

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/map/t/as;

    invoke-direct {v0, v1, v1, v1}, Lcom/google/android/apps/gmm/map/t/as;-><init>(III)V

    invoke-direct {p0, p1, p2, v0, v4}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    .line 30
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xf

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Tile stencil ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    .line 31
    invoke-virtual {p0, p3}, Lcom/google/android/apps/gmm/map/t/at;->a(Lcom/google/android/apps/gmm/v/co;)V

    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/v/x;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/x;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/t/at;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 33
    new-instance v0, Lcom/google/android/apps/gmm/v/m;

    const/4 v1, 0x1

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/gmm/v/m;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/t/at;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 34
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_0
    int-to-byte v0, v4

    iput-byte v0, p0, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 35
    new-instance v0, Lcom/google/android/apps/gmm/v/bt;

    const/16 v1, 0x207

    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/t/at;->b()I

    move-result v2

    move v4, v3

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/v/bt;-><init>(IIIIIII)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/at;->f:Lcom/google/android/apps/gmm/v/bt;

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/at;->f:Lcom/google/android/apps/gmm/v/bt;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/t/at;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 44
    return-void
.end method
