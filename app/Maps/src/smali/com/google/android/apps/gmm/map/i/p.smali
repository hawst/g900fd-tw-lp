.class public Lcom/google/android/apps/gmm/map/i/p;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/r/a/w;

.field final b:Z

.field final c:Z

.field public d:[B

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/anw;",
            ">;"
        }
    .end annotation
.end field

.field g:Lcom/google/android/apps/gmm/map/r/a/t;

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/m;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/google/android/apps/gmm/map/c/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final j:Lcom/google/android/apps/gmm/map/i/q;

.field public k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/ar;",
            ">;>;"
        }
    .end annotation
.end field

.field final l:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final m:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/r/a/s;Lcom/google/maps/g/a/fw;ZLcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/x;)V
    .locals 8
    .param p5    # Lcom/google/android/apps/gmm/map/c/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 304
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->f:Ljava/util/List;

    .line 321
    const-string v0, "route"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 322
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/map/r/a/s;->f:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/p;->b:Z

    .line 323
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/i/p;->i:Lcom/google/android/apps/gmm/map/c/a;

    .line 324
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/p;->b:Z

    if-nez v0, :cond_1

    if-eqz p4, :cond_2

    :cond_1
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/p;->c:Z

    .line 325
    new-instance v0, Lcom/google/android/apps/gmm/map/i/q;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/i/q;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    .line 326
    iget-object v4, p1, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    .line 327
    iget-object v5, p1, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    .line 329
    if-eqz v4, :cond_7

    .line 330
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v0, v0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/ao;->b()Ljava/lang/String;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->l:Ljava/lang/String;

    .line 331
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v0, v0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_5

    move v0, v2

    :goto_3
    if-eqz v0, :cond_6

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/ao;->c()Ljava/lang/String;

    move-result-object v0

    :goto_4
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->m:Ljava/lang/String;

    .line 336
    :goto_5
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->i:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    if-nez v0, :cond_8

    .line 338
    const-string v0, "PolylineMapData"

    const-string v2, "Route had no polyline points."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 376
    :goto_6
    return-void

    :cond_2
    move v0, v1

    .line 324
    goto :goto_0

    :cond_3
    move v0, v1

    .line 330
    goto :goto_1

    :cond_4
    move-object v0, v3

    goto :goto_2

    :cond_5
    move v0, v1

    .line 331
    goto :goto_3

    :cond_6
    move-object v0, v3

    goto :goto_4

    .line 333
    :cond_7
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/i/p;->m:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/gmm/map/i/p;->l:Ljava/lang/String;

    goto :goto_5

    .line 342
    :cond_8
    sget-object v0, Lcom/google/android/apps/gmm/map/i/o;->a:[I

    invoke-virtual {v5}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v6

    aget v0, v0, v6

    packed-switch v0, :pswitch_data_0

    .line 364
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unsupported travel mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 344
    :pswitch_0
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    .line 345
    iget-boolean v4, p2, Lcom/google/android/apps/gmm/map/r/a/s;->h:Z

    if-eqz v4, :cond_13

    if-eqz p3, :cond_13

    iget-object v4, p3, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    iget-object v6, p3, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    :goto_7
    if-eqz v4, :cond_9

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-lt v6, v7, :cond_9

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Lcom/google/b/c/hj;->a(I)Ljava/util/HashMap;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/apps/gmm/map/i/q;->b:Ljava/util/Map;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/i/q;->b:Ljava/util/Map;

    invoke-static {v0, v4, v6}, Lcom/google/android/apps/gmm/map/i/p;->a(Ljava/util/List;Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    :cond_9
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lt v4, v7, :cond_a

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/i/q;->b:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/i/q;->b:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/maps/g/a/er;->a:Lcom/google/maps/g/a/er;

    invoke-static {v7}, Lcom/google/android/apps/gmm/map/i/j;->a(Lcom/google/maps/g/a/er;)B

    move-result v7

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    invoke-interface {v4, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_a
    iget-object v4, v5, Lcom/google/android/apps/gmm/map/i/q;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {p6, v2}, Lcom/google/android/apps/gmm/map/x;->a(Z)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v4, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v5, Lcom/google/android/apps/gmm/map/i/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p6, v1}, Lcom/google/android/apps/gmm/map/x;->a(Z)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    .line 346
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/i/p;->a(Lcom/google/android/apps/gmm/map/r/a/s;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->d:[B

    .line 347
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/p;->c:Z

    if-eqz v0, :cond_b

    .line 348
    invoke-direct {p0, p3}, Lcom/google/android/apps/gmm/map/i/p;->a(Lcom/google/maps/g/a/fw;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->f:Ljava/util/List;

    .line 367
    :cond_b
    :goto_8
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    if-eqz v2, :cond_10

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v4, :cond_c

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_9
    iget-object v4, v0, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    if-nez v4, :cond_d

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    :goto_a
    if-eqz v0, :cond_10

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v4, :cond_e

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_b
    iget-object v4, v0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v4, :cond_f

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v0

    :goto_c
    if-eqz v0, :cond_10

    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/t;

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/apps/gmm/map/r/a/t;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/r/a/ao;)V

    :goto_d
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->g:Lcom/google/android/apps/gmm/map/r/a/t;

    .line 370
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/p;->b:Z

    if-eqz v0, :cond_11

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/i/p;->a()Ljava/util/List;

    move-result-object v0

    :goto_e
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->h:Ljava/util/List;

    .line 374
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/map/r/a/s;->g:Z

    if-nez p3, :cond_12

    .line 373
    :goto_f
    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/gmm/map/i/p;->a(ZLjava/util/List;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->k:Ljava/util/Map;

    goto/16 :goto_6

    .line 353
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    invoke-static {v0, v1, v5, p6}, Lcom/google/android/apps/gmm/map/i/p;->a(Lcom/google/android/apps/gmm/map/i/q;Lcom/google/android/apps/gmm/map/r/a/ao;Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/x;)V

    .line 354
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/i/p;->c()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->d:[B

    goto :goto_8

    .line 359
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    invoke-static {v0, v4, v5, p6}, Lcom/google/android/apps/gmm/map/i/p;->a(Lcom/google/android/apps/gmm/map/i/q;Lcom/google/android/apps/gmm/map/r/a/ao;Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/x;)V

    .line 360
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/i/p;->b()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->d:[B

    goto :goto_8

    .line 367
    :cond_c
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_9

    :cond_d
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    goto :goto_a

    :cond_e
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_b

    :cond_f
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto :goto_c

    :cond_10
    move-object v0, v3

    goto :goto_d

    .line 370
    :cond_11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_e

    .line 375
    :cond_12
    iget-object v3, p3, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    goto :goto_f

    :cond_13
    move-object v4, v3

    goto/16 :goto_7

    .line 342
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private a()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 752
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 753
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/i/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 754
    if-ltz v3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 755
    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/m;

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    .line 758
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v6, v0

    const-wide v8, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    move-result-wide v6

    const-wide v10, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v6, v10

    mul-double/2addr v6, v8

    const-wide v8, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v6, v8

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v8

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/i/q;->a:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v4, v5, v0}, Lcom/google/android/apps/gmm/map/r/a/m;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;I)V

    .line 757
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 760
    :cond_1
    return-object v1
.end method

.method private a(Lcom/google/maps/g/a/fw;)Ljava/util/List;
    .locals 5
    .param p1    # Lcom/google/maps/g/a/fw;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/g/a/fw;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/anw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 772
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 773
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->i:Lcom/google/android/apps/gmm/map/c/a;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 786
    :goto_0
    return-object v0

    .line 776
    :cond_1
    iget-object v0, p1, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    .line 777
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    .line 778
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/i/p;->i:Lcom/google/android/apps/gmm/map/c/a;

    .line 780
    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/c/a;->D_()Lcom/google/android/apps/gmm/map/i/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/i/p;->i:Lcom/google/android/apps/gmm/map/c/a;

    .line 781
    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/c/a;->a()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 779
    invoke-static {v0, v3, v4}, Lcom/google/android/apps/gmm/map/r/c/a;->a(Lcom/google/maps/g/a/da;Lcom/google/android/apps/gmm/map/i/a/a;Landroid/content/res/Resources;)Lcom/google/r/b/a/anw;

    move-result-object v0

    .line 782
    if-eqz v0, :cond_2

    .line 783
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 786
    goto :goto_0
.end method

.method private static a(Ljava/util/List;Ljava/util/List;Ljava/util/Map;)Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/eo;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Byte;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 466
    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 467
    :cond_0
    if-nez p1, :cond_1

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 468
    :cond_1
    if-nez p2, :cond_2

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 472
    :cond_2
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v3, v2

    if-ltz v3, :cond_3

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_4

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 474
    const/4 v8, 0x0

    .line 475
    const/4 v5, 0x0

    .line 476
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 478
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 479
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/eo;

    .line 480
    iget v4, v3, Lcom/google/maps/g/a/eo;->b:I

    .line 482
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move-object v6, v2

    move v7, v5

    move-object v5, v3

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 484
    invoke-virtual {v6, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    float-to-double v12, v2

    .line 485
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v14

    div-double/2addr v12, v14

    .line 487
    float-to-double v14, v8

    add-double/2addr v14, v12

    double-to-float v8, v14

    .line 488
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v14

    .line 492
    :goto_2
    if-eqz v5, :cond_9

    if-gt v4, v14, :cond_9

    .line 499
    if-eq v4, v14, :cond_7

    .line 502
    int-to-float v2, v4

    sub-float/2addr v2, v7

    float-to-double v0, v2

    move-wide/from16 v16, v0

    div-double v16, v16, v12

    .line 505
    move-wide/from16 v0, v16

    double-to-float v2, v0

    new-instance v15, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v15}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-static {v6, v3, v2, v15}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 508
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v15, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 509
    invoke-interface {v9, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 512
    :cond_5
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    .line 519
    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    .line 520
    iget v2, v5, Lcom/google/maps/g/a/eo;->e:I

    invoke-static {v2}, Lcom/google/maps/g/a/er;->a(I)Lcom/google/maps/g/a/er;

    move-result-object v2

    if-nez v2, :cond_6

    sget-object v2, Lcom/google/maps/g/a/er;->a:Lcom/google/maps/g/a/er;

    :cond_6
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/i/j;->a(Lcom/google/maps/g/a/er;)B

    move-result v2

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    .line 519
    move-object/from16 v0, p2

    invoke-interface {v0, v15, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/eo;

    .line 526
    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_d

    .line 527
    const/4 v5, 0x0

    .line 530
    :goto_5
    if-eqz v5, :cond_c

    .line 531
    iget v2, v5, Lcom/google/maps/g/a/eo;->b:I

    :goto_6
    move v4, v2

    .line 533
    goto :goto_2

    .line 515
    :cond_7
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 522
    :cond_8
    const/4 v2, 0x0

    goto :goto_4

    .line 536
    :cond_9
    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v6, v3

    move v7, v8

    .line 540
    goto/16 :goto_1

    .line 546
    :cond_a
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_b

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 547
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/maps/g/a/er;->a:Lcom/google/maps/g/a/er;

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/i/j;->a(Lcom/google/maps/g/a/er;)B

    move-result v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    :cond_b
    return-object v9

    :cond_c
    move v2, v4

    goto :goto_6

    :cond_d
    move-object v5, v2

    goto :goto_5
.end method

.method private a(ZLjava/util/List;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/eo;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/ar;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 802
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/i/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/i/q;->c:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->size()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x2

    .line 804
    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    .line 805
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 807
    :cond_0
    invoke-static {v0}, Lcom/google/b/c/hj;->a(I)Ljava/util/HashMap;

    move-result-object v4

    .line 810
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 811
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/ar;->a:Lcom/google/android/apps/gmm/map/internal/c/ar;

    invoke-static {v4, v3, v0}, Lcom/google/android/apps/gmm/map/i/p;->a(Ljava/util/Map;ILcom/google/android/apps/gmm/map/internal/c/ar;)V

    .line 812
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/ar;->a:Lcom/google/android/apps/gmm/map/internal/c/ar;

    invoke-static {v4, v0, v1}, Lcom/google/android/apps/gmm/map/i/p;->a(Ljava/util/Map;ILcom/google/android/apps/gmm/map/internal/c/ar;)V

    .line 816
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/i/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 817
    if-ltz v0, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v0, :cond_2

    .line 818
    sget-object v5, Lcom/google/android/apps/gmm/map/internal/c/ar;->a:Lcom/google/android/apps/gmm/map/internal/c/ar;

    invoke-static {v4, v0, v5}, Lcom/google/android/apps/gmm/map/i/p;->a(Ljava/util/Map;ILcom/google/android/apps/gmm/map/internal/c/ar;)V

    goto :goto_0

    .line 825
    :cond_3
    if-nez p1, :cond_5

    move v0, v2

    .line 826
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/p;->d:[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_5

    .line 827
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/p;->d:[B

    aget-byte v1, v1, v0

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/i/p;->d:[B

    add-int/lit8 v6, v0, 0x1

    aget-byte v5, v5, v6

    if-eq v1, v5, :cond_4

    .line 828
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/ar;->a:Lcom/google/android/apps/gmm/map/internal/c/ar;

    invoke-static {v4, v0, v1}, Lcom/google/android/apps/gmm/map/i/p;->a(Ljava/util/Map;ILcom/google/android/apps/gmm/map/internal/c/ar;)V

    .line 826
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 834
    :cond_5
    const/4 v0, 0x0

    .line 835
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/i/q;->c:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v0

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 838
    if-eqz v1, :cond_7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    if-eq v1, v6, :cond_6

    if-eqz v1, :cond_8

    invoke-virtual {v1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_6
    move v1, v2

    :goto_3
    if-nez v1, :cond_7

    .line 839
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v6, Lcom/google/android/apps/gmm/map/internal/c/ar;->a:Lcom/google/android/apps/gmm/map/internal/c/ar;

    invoke-static {v4, v1, v6}, Lcom/google/android/apps/gmm/map/i/p;->a(Ljava/util/Map;ILcom/google/android/apps/gmm/map/internal/c/ar;)V

    :cond_7
    move-object v1, v0

    .line 842
    goto :goto_2

    :cond_8
    move v1, v3

    .line 838
    goto :goto_3

    .line 844
    :cond_9
    return-object v4
.end method

.method private static a(Lcom/google/android/apps/gmm/map/i/q;Lcom/google/android/apps/gmm/map/r/a/ao;Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/x;)V
    .locals 14

    .prologue
    .line 899
    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    move-object/from16 v0, p2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 901
    :cond_1
    const/4 v1, 0x0

    move v5, v1

    :goto_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v1, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v5, v1, :cond_a

    .line 902
    new-instance v6, Lcom/google/android/apps/gmm/map/r/a/p;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v1, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/ea;

    invoke-direct {v6, v1}, Lcom/google/android/apps/gmm/map/r/a/p;-><init>(Lcom/google/maps/g/a/ea;)V

    .line 903
    const/4 v2, 0x0

    .line 904
    const/4 v1, 0x0

    move v4, v1

    :goto_2
    iget-object v1, v6, Lcom/google/android/apps/gmm/map/r/a/p;->a:Lcom/google/maps/g/a/ea;

    iget-object v1, v1, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_9

    .line 905
    invoke-virtual {v6, v4}, Lcom/google/android/apps/gmm/map/r/a/p;->a(I)Lcom/google/android/apps/gmm/map/r/a/al;

    move-result-object v7

    .line 906
    const/4 v1, 0x0

    move v13, v1

    move v1, v2

    move v2, v13

    :goto_3
    iget-object v3, v7, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v3, v3, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_8

    .line 907
    invoke-virtual {v7, v2, v1}, Lcom/google/android/apps/gmm/map/r/a/al;->a(II)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v8

    .line 908
    add-int/lit8 v3, v1, 0x1

    .line 909
    iget v9, v8, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    .line 910
    const/4 v1, -0x1

    if-ne v9, v1, :cond_2

    .line 911
    const-string v1, "step (%d->%d->%d) had no compact_polyline_vertex_offset"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    .line 913
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    .line 911
    invoke-static {v1, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 916
    :cond_2
    if-nez v9, :cond_6

    .line 917
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/q;->a:Ljava/util/Map;

    .line 918
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const/4 v11, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Lcom/google/android/apps/gmm/map/x;->a(Z)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 917
    invoke-interface {v1, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 925
    :cond_3
    :goto_4
    sget-object v1, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    move-object/from16 v0, p2

    if-ne v0, v1, :cond_5

    .line 926
    iget-object v1, v8, Lcom/google/android/apps/gmm/map/r/a/ag;->a:Lcom/google/maps/g/a/fk;

    iget v1, v1, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    .line 928
    :cond_4
    sget-object v10, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-ne v1, v10, :cond_7

    .line 929
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/q;->b:Ljava/util/Map;

    add-int/lit8 v10, v9, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const/4 v11, 0x2

    invoke-static {v11}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v11

    invoke-interface {v1, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 936
    :cond_5
    :goto_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/q;->c:Ljava/util/TreeMap;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/r/a/ag;->x:Lcom/google/maps/g/by;

    invoke-virtual {v1, v9, v8}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 906
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_3

    .line 919
    :cond_6
    if-nez v2, :cond_3

    .line 920
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/q;->a:Ljava/util/Map;

    .line 921
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Lcom/google/android/apps/gmm/map/x;->a(Z)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 920
    invoke-interface {v1, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 931
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/q;->b:Ljava/util/Map;

    add-int/lit8 v10, v9, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const/4 v11, 0x6

    invoke-static {v11}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v11

    invoke-interface {v1, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 904
    :cond_8
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    goto/16 :goto_2

    .line 901
    :cond_9
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto/16 :goto_1

    .line 940
    :cond_a
    return-void
.end method

.method private static a(Ljava/util/Map;ILcom/google/android/apps/gmm/map/internal/c/ar;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/ar;",
            ">;>;I",
            "Lcom/google/android/apps/gmm/map/internal/c/ar;",
            ")V"
        }
    .end annotation

    .prologue
    .line 856
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 857
    if-nez v0, :cond_0

    .line 858
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 862
    :goto_0
    return-void

    .line 860
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/r/a/s;)[B
    .locals 6

    .prologue
    const/4 v2, -0x1

    .line 733
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [B

    .line 737
    const/4 v0, 0x1

    move v1, v0

    move v0, v2

    :goto_0
    array-length v3, v4

    if-ge v1, v3, :cond_3

    .line 738
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/i/q;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 739
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/i/q;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    .line 742
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/p;->b:Z

    if-nez v0, :cond_0

    if-ne v3, v2, :cond_1

    :cond_0
    move v0, v3

    .line 744
    :goto_2
    aput-byte v0, v4, v1

    .line 737
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto :goto_0

    .line 744
    :cond_1
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/a/s;->h:Z

    if-eqz v0, :cond_2

    add-int/lit8 v0, v3, 0x4

    int-to-byte v0, v0

    goto :goto_2

    :cond_2
    const/4 v0, 0x6

    goto :goto_2

    .line 748
    :cond_3
    return-object v4

    :cond_4
    move v3, v0

    goto :goto_1
.end method

.method private b()[B
    .locals 3

    .prologue
    .line 870
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [B

    .line 871
    const/4 v0, 0x0

    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_1

    .line 872
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/i/p;->b:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    :goto_1
    aput-byte v1, v2, v0

    .line 871
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 872
    :cond_0
    const/4 v1, 0x6

    goto :goto_1

    .line 874
    :cond_1
    return-object v2
.end method

.method private c()[B
    .locals 6

    .prologue
    .line 879
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [B

    .line 880
    const/4 v1, -0x1

    .line 882
    const/4 v0, 0x0

    move v5, v0

    move v0, v1

    move v1, v5

    :goto_0
    array-length v2, v3

    if-ge v1, v2, :cond_1

    .line 883
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/i/q;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 884
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/i/q;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    .line 887
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/p;->b:Z

    if-eqz v0, :cond_0

    move v0, v2

    :goto_2
    aput-byte v0, v3, v1

    .line 882
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 887
    :cond_0
    const/4 v0, 0x6

    goto :goto_2

    .line 889
    :cond_1
    return-object v3

    :cond_2
    move v2, v0

    goto :goto_1
.end method
