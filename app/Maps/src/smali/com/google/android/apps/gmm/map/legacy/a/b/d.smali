.class public Lcom/google/android/apps/gmm/map/legacy/a/b/d;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;


# instance fields
.field public final b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

.field public final c:Lcom/google/android/apps/gmm/map/internal/d/as;

.field public final d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

.field public e:Lcom/google/android/apps/gmm/map/internal/c/ce;

.field public f:Ljava/util/concurrent/atomic/AtomicInteger;

.field final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/a/e;",
            ">;"
        }
    .end annotation
.end field

.field h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/b/c;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field public final j:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

.field k:Lcom/google/android/apps/gmm/map/legacy/a/b/c;

.field final l:Lcom/google/android/apps/gmm/map/internal/d/a/c;

.field final m:Lcom/google/android/apps/gmm/map/internal/d/a/c;

.field final n:Lcom/google/android/apps/gmm/shared/c/f;

.field public volatile o:Lcom/google/android/apps/gmm/map/internal/vector/gl/a;

.field public p:Lcom/google/android/apps/gmm/map/legacy/a/b/i;

.field public volatile q:Z

.field private final r:Lcom/google/android/apps/gmm/map/t/b;

.field private final s:Lcom/google/android/apps/gmm/map/internal/d/at;

.field private final t:Lcom/google/android/apps/gmm/map/internal/d/a/c;

.field private final u:Lcom/google/android/apps/gmm/map/c/a;

.field private final v:Landroid/content/res/Resources;

.field private final w:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

.field private final x:Lcom/google/android/apps/gmm/map/e/a;

.field private final y:Lcom/google/android/apps/gmm/v/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 118
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ax;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ax;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/legacy/a/b/a;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V
    .locals 7

    .prologue
    .line 264
    sget-object v4, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;-><init>(Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/t/b;Lcom/google/android/apps/gmm/map/legacy/a/b/a;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    .line 270
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/t/b;Lcom/google/android/apps/gmm/map/legacy/a/b/a;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V
    .locals 2

    .prologue
    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 162
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->g:Ljava/util/Set;

    .line 171
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->h:Ljava/util/Map;

    .line 180
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->i:Ljava/util/Map;

    .line 196
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/b/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/legacy/a/b/f;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/b/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->t:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    .line 199
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/b/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/legacy/a/b/h;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/b/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->l:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    .line 202
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/legacy/a/b/g;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/b/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->m:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    .line 246
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->q:Z

    .line 290
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->u:Lcom/google/android/apps/gmm/map/c/a;

    .line 291
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->v:Landroid/content/res/Resources;

    .line 292
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->n:Lcom/google/android/apps/gmm/shared/c/f;

    .line 293
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    iget-object v1, p4, Lcom/google/android/apps/gmm/map/t/b;->p:Lcom/google/android/apps/gmm/map/util/a;

    invoke-direct {v0, p3, v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/e;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/util/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    .line 294
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->r:Lcom/google/android/apps/gmm/map/t/b;

    .line 295
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->w:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

    .line 296
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->v_()Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    .line 297
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->q()Lcom/google/android/apps/gmm/map/e/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->x:Lcom/google/android/apps/gmm/map/e/a;

    .line 298
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v1

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v0, :cond_0

    invoke-virtual {v1, p3}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->c:Lcom/google/android/apps/gmm/map/internal/d/as;

    .line 299
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/b/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/legacy/a/b/e;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/b/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->s:Lcom/google/android/apps/gmm/map/internal/d/at;

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->c:Lcom/google/android/apps/gmm/map/internal/d/as;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->s:Lcom/google/android/apps/gmm/map/internal/d/at;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/as;->a(Lcom/google/android/apps/gmm/map/internal/d/at;)V

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->c:Lcom/google/android/apps/gmm/map/internal/d/as;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/as;->d()Lcom/google/android/apps/gmm/v/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->y:Lcom/google/android/apps/gmm/v/b/a;

    .line 337
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    .line 338
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/legacy/a/b/d;Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 114
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/internal/c/bp;Z)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v0, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->u:Lcom/google/android/apps/gmm/map/c/a;

    invoke-virtual {v0, v2, p2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/c/a;Ljava/util/List;)Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->p:Lcom/google/android/apps/gmm/map/legacy/a/b/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->p:Lcom/google/android/apps/gmm/map/legacy/a/b/i;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/b/i;->b(Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;
    .locals 6

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/internal/c/bp;Z)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    move-result-object v1

    .line 395
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    if-eq v1, v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->n:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->b(Lcom/google/android/apps/gmm/shared/c/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    const/4 v1, 0x0

    .line 399
    :cond_0
    if-eqz v1, :cond_1

    instance-of v0, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 400
    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    .line 401
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->c:Lcom/google/android/apps/gmm/map/internal/d/as;

    .line 403
    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->h:J

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->l:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    .line 401
    invoke-interface {v2, p1, v4, v5, v0}, Lcom/google/android/apps/gmm/map/internal/d/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;JLcom/google/android/apps/gmm/map/internal/d/a/c;)V

    .line 407
    :cond_1
    return-object v1
.end method

.method declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "I",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;)",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 672
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->e:Lcom/google/android/apps/gmm/map/internal/c/ce;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/ce;->a(Lcom/google/android/apps/gmm/map/internal/c/cd;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v7

    .line 675
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->o:Lcom/google/android/apps/gmm/map/internal/vector/gl/a;

    if-eqz v3, :cond_9

    if-nez p2, :cond_9

    instance-of v0, p3, Lcom/google/android/apps/gmm/map/internal/c/cm;

    if-eqz v0, :cond_7

    invoke-interface {p3}, Lcom/google/android/apps/gmm/map/internal/c/bo;->b()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ai;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_6

    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v8

    check-cast p3, Lcom/google/android/apps/gmm/map/internal/c/cm;

    invoke-static {p3, v8}, Lcom/google/android/apps/gmm/map/internal/c/aj;->a(Lcom/google/android/apps/gmm/map/internal/c/cm;Ljava/util/Map;)Lcom/google/android/apps/gmm/map/internal/c/cm;

    move-result-object p3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/cm;

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->r:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->u:Lcom/google/android/apps/gmm/map/c/a;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->v:Landroid/content/res/Resources;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->w:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;Lcom/google/android/apps/gmm/map/t/b;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->e()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v0, v2, v4, v5}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/internal/c/bp;Z)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->e()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v4

    invoke-virtual {v0, v2, v4, v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->u:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/c/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/c/c;->e:Lcom/google/android/apps/gmm/map/legacy/internal/b/f;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 672
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move-object v0, p3

    .line 675
    :goto_2
    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->r:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->u:Lcom/google/android/apps/gmm/map/c/a;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->v:Landroid/content/res/Resources;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->w:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

    move-object v1, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;Lcom/google/android/apps/gmm/map/t/b;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    move-result-object v0

    :goto_3
    if-nez v0, :cond_4

    const/4 v0, 0x2

    if-ne p2, v0, :cond_8

    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    invoke-virtual {v1, v2, p1, v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;)V

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->p:Lcom/google/android/apps/gmm/map/legacy/a/b/i;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->p:Lcom/google/android/apps/gmm/map/legacy/a/b/i;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/legacy/a/b/i;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;)V

    goto/16 :goto_0

    :cond_6
    check-cast p3, Lcom/google/android/apps/gmm/map/internal/c/cm;

    move-object v0, p3

    goto :goto_2

    :cond_7
    instance-of v0, p3, Lcom/google/android/apps/gmm/map/internal/c/r;

    if-eqz v0, :cond_9

    check-cast p3, Lcom/google/android/apps/gmm/map/internal/c/r;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->v:Landroid/content/res/Resources;

    invoke-static {p3, v3, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/internal/c/r;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_3

    :cond_8
    move-object v0, v7

    goto/16 :goto_0

    :cond_9
    move-object v0, v7

    goto :goto_3
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/a/e;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 369
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/internal/c/bp;Z)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    move-result-object v1

    .line 372
    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    if-ne v1, v2, :cond_0

    .line 378
    :goto_0
    return-object v0

    .line 374
    :cond_0
    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->n:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->b(Lcom/google/android/apps/gmm/shared/c/f;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 375
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 378
    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/b/e;Lcom/google/android/apps/gmm/map/b/a/y;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Lcom/google/android/apps/gmm/map/b/a/bc;IZ)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/b/e;",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/bc;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->d(Lcom/google/android/apps/gmm/map/legacy/a/a/e;)V

    .line 510
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->c:Lcom/google/android/apps/gmm/map/internal/d/as;

    invoke-interface {v0, p6, p3}, Lcom/google/android/apps/gmm/map/internal/d/as;->a(Lcom/google/android/apps/gmm/map/b/a/bc;Ljava/util/List;)V

    .line 512
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    monitor-enter v8

    .line 516
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p7

    move/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->a(Lcom/google/android/apps/gmm/map/internal/b/e;Lcom/google/android/apps/gmm/map/b/a/y;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;IZ)V

    .line 523
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->c()Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    move-result-object v0

    .line 525
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->k:Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    if-nez v1, :cond_0

    .line 527
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->b:Z

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->m:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ZLcom/google/android/apps/gmm/map/internal/d/a/c;)V

    .line 529
    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->k:Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    .line 530
    monitor-exit v8

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Lcom/google/android/apps/gmm/map/internal/c/bp;ZLcom/google/android/apps/gmm/map/internal/d/a/c;)V
    .locals 2

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->c:Lcom/google/android/apps/gmm/map/internal/d/as;

    if-eqz v0, :cond_0

    .line 452
    if-eqz p2, :cond_1

    .line 453
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->c:Lcom/google/android/apps/gmm/map/internal/d/as;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->q:Z

    invoke-interface {v0, p1, p3, v1}, Lcom/google/android/apps/gmm/map/internal/d/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Z)V

    .line 459
    :cond_0
    :goto_0
    return-void

    .line 456
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->c:Lcom/google/android/apps/gmm/map/internal/d/as;

    invoke-interface {v0, p1, p3}, Lcom/google/android/apps/gmm/map/internal/d/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 627
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 630
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->g:Ljava/util/Set;

    monitor-enter v1

    .line 632
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    .line 634
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;)V

    goto :goto_0

    .line 637
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 636
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 637
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 638
    const/4 v0, 0x1

    .line 640
    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 435
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->n:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {p2, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->a(Lcom/google/android/apps/gmm/shared/c/f;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 436
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->h:Ljava/util/Map;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 437
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->t:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ZLcom/google/android/apps/gmm/map/internal/d/a/c;)V

    .line 440
    :cond_1
    const/4 v0, 0x1

    .line 442
    :cond_2
    return v0
.end method
