.class Lcom/google/android/apps/gmm/map/internal/d/ax;
.super Lcom/google/android/apps/gmm/map/internal/d/aw;
.source "PG"


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 0

    .prologue
    .line 274
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/aw;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 275
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 311
    const/16 v0, 0x800

    return v0
.end method

.method final a(Lcom/google/android/apps/gmm/map/internal/d/ac;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZLcom/google/android/apps/gmm/map/internal/d/r;Lcom/google/android/apps/gmm/map/internal/d/ag;)Lcom/google/android/apps/gmm/map/internal/d/as;
    .locals 8

    .prologue
    const/16 v2, 0x100

    .line 287
    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v1, 0xa0

    if-le v0, v1, :cond_0

    const/4 v3, 0x3

    .line 289
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ax;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->e:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v0, v1, :cond_1

    .line 290
    invoke-static {p2, v2}, Lcom/google/android/apps/gmm/map/internal/a/a;->b(Landroid/content/res/Resources;I)I

    move-result v4

    .line 296
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ax;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/b/a/ai;->C:Z

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    .line 297
    :goto_2
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/aa;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/ax;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object v1, p1

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/internal/d/aa;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/b/a/ai;IIFLjava/util/Locale;Ljava/io/File;)V

    return-object v0

    .line 287
    :cond_0
    const/4 v3, 0x1

    goto :goto_0

    .line 293
    :cond_1
    invoke-static {p2, v2}, Lcom/google/android/apps/gmm/map/internal/a/a;->a(Landroid/content/res/Resources;I)I

    move-result v4

    goto :goto_1

    .line 296
    :cond_2
    const/high16 v5, 0x3f800000    # 1.0f

    goto :goto_2
.end method

.method final b()Lcom/google/android/apps/gmm/map/internal/d/b/ap;
    .locals 2

    .prologue
    .line 306
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/ay;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/ax;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/ay;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    return-object v0
.end method

.method public final c()B
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x0

    return v0
.end method
