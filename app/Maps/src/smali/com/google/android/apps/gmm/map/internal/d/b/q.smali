.class public Lcom/google/android/apps/gmm/map/internal/d/b/q;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/util/Locale;


# instance fields
.field private final b:I

.field private final c:Lcom/google/android/apps/gmm/map/internal/d/b/t;

.field private final d:Lcom/google/android/apps/gmm/shared/c/f;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->a:Ljava/util/Locale;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;ILjava/io/File;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->b:I

    .line 63
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->c()J

    move-result-wide v8

    .line 67
    const/4 v7, 0x0

    .line 69
    :try_start_0
    const-string v0, "r"

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/b/x;->b:Lcom/google/android/apps/gmm/map/internal/d/b/x;

    .line 70
    invoke-static {p1, v0, p3, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/b/x;)Lcom/google/android/apps/gmm/map/internal/d/b/t;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 79
    :goto_0
    if-eqz v0, :cond_0

    .line 80
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->c()J

    move-result-wide v2

    sub-long/2addr v2, v8

    .line 81
    const-string v1, "DiskResourceCache"

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x32

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Loaded "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " entries, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v10, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->e:Z

    .line 84
    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->c:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    .line 85
    return-void

    :catch_0
    move-exception v0

    .line 73
    :try_start_1
    const-string v1, "r"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->b:I

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/b/q;->a:Ljava/util/Locale;

    sget-object v6, Lcom/google/android/apps/gmm/map/internal/d/b/x;->b:Lcom/google/android/apps/gmm/map/internal/d/b/x;

    move-object v0, p1

    move-object v5, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;IILjava/util/Locale;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/b/x;)Lcom/google/android/apps/gmm/map/internal/d/b/t;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 75
    :catch_1
    move-exception v0

    .line 76
    const-string v1, "DiskResourceCache"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v7

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/io/File;)Lcom/google/android/apps/gmm/map/internal/d/b/q;
    .locals 3

    .prologue
    .line 90
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/q;

    const/16 v1, 0x200

    .line 91
    invoke-interface {p0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/q;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;ILjava/io/File;Lcom/google/android/apps/gmm/shared/c/f;)V

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;
    .locals 10

    .prologue
    const/16 v6, 0x9

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 135
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_0

    move-object v0, v1

    .line 166
    :goto_0
    monitor-exit p0

    return-object v0

    .line 139
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->c:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-static {p1}, Lcom/google/android/apps/gmm/map/internal/d/b/b;->a(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(JLjava/lang/String;)Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v2

    .line 140
    if-nez v2, :cond_1

    move-object v0, v1

    .line 141
    goto :goto_0

    .line 143
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v0

    .line 144
    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v3

    if-le v3, v6, :cond_2

    const/4 v3, 0x0

    aget-byte v3, v0, v3

    if-eq v3, v5, :cond_3

    :cond_2
    move-object v0, v1

    .line 145
    goto :goto_0

    .line 148
    :cond_3
    const/4 v3, 0x1

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v3

    int-to-long v4, v3

    const/4 v3, 0x5

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v3

    int-to-long v6, v3

    const-wide v8, 0xffffffffL

    and-long/2addr v6, v8

    const/16 v3, 0x20

    shl-long/2addr v4, v3

    or-long/2addr v4, v6

    .line 149
    new-instance v3, Lcom/google/e/a/a/a/b;

    sget-object v6, Lcom/google/r/b/a/b/ah;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {v3, v6}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 151
    :try_start_2
    new-instance v6, Ljava/io/ByteArrayInputStream;

    const/16 v7, 0x9

    .line 152
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v8

    add-int/lit8 v8, v8, -0x9

    invoke-direct {v6, v0, v7, v8}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 151
    const v0, 0x7fffffff

    const/4 v7, 0x1

    new-instance v8, Lcom/google/e/a/a/a/c;

    invoke-direct {v8}, Lcom/google/e/a/a/a/c;-><init>()V

    invoke-virtual {v3, v6, v0, v7, v8}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;IZLcom/google/e/a/a/a/c;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 156
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->c:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v6, :cond_4

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 158
    :cond_4
    const/4 v0, 0x2

    const/16 v2, 0x1c

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    move-object v0, v1

    .line 159
    goto :goto_0

    .line 154
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->c:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v3, :cond_5

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_5
    move-object v0, v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->c:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v3, :cond_6

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_6
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 135
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 162
    :cond_7
    :try_start_4
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;-><init>()V

    .line 163
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->c:Z

    .line 164
    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(Lcom/google/e/a/a/a/b;)Z

    .line 165
    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(J)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0
.end method

.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 221
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 225
    :goto_0
    monitor-exit p0

    return-void

    .line 224
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->c:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g:Lcom/google/android/apps/gmm/map/internal/d/b/aa;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/aa;->a(F)I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/b;->a(F)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 221
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/e/a/a/a/b;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 175
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 196
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 179
    :cond_1
    if-eqz p1, :cond_0

    .line 185
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v6

    const/4 v0, 0x2

    const/16 v1, 0x1c

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->c:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g:Lcom/google/android/apps/gmm/map/internal/d/b/aa;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/b/aa;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/d/b/z;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/b;->a(Ljava/lang/String;)J

    move-result-wide v8

    const/4 v0, 0x0

    invoke-static {v8, v9, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(JLjava/lang/String;)Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/b/z;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/b/z;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->b:[B

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/b/z;->b:[B

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    const/16 v5, 0x9

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/util/f;->a(I)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v0

    const/4 v5, 0x0

    const/4 v8, 0x1

    aput-byte v8, v0, v5

    const/4 v5, 0x1

    const/16 v8, 0x20

    shr-long v8, v6, v8

    long-to-int v8, v8

    invoke-static {v0, v5, v8}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    const/4 v5, 0x5

    long-to-int v6, v6

    invoke-static {v0, v5, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    new-instance v5, Lcom/google/android/apps/gmm/map/util/f;

    invoke-direct {v5, v0}, Lcom/google/android/apps/gmm/map/util/f;-><init>([B)V

    iput-object v5, v1, Lcom/google/android/apps/gmm/map/internal/d/b/z;->e:Lcom/google/android/apps/gmm/map/util/f;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 186
    const/4 v0, 0x1

    :try_start_2
    new-array v2, v0, [Lcom/google/android/apps/gmm/map/internal/d/b/z;

    const/4 v0, 0x0

    aput-object v1, v2, v0

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 188
    :catch_0
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    .line 189
    :goto_1
    :try_start_3
    const-string v3, "DiskResourceCache"

    const/4 v0, 0x2

    .line 190
    const/16 v4, 0x1c

    invoke-virtual {p1, v0, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x14

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Error inserting: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " : "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 189
    invoke-static {v3, v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 192
    if-eqz v2, :cond_0

    .line 193
    :try_start_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->c:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/z;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 175
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 186
    :cond_2
    :try_start_5
    array-length v5, v2

    if-ltz v5, :cond_4

    move v0, v3

    :goto_2
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 192
    :catchall_1
    move-exception v0

    move-object v2, v1

    :goto_3
    if-eqz v2, :cond_3

    .line 193
    :try_start_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->c:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/z;)Z

    :cond_3
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_4
    move v0, v4

    .line 186
    goto :goto_2

    :cond_5
    const-wide/16 v6, 0x5

    int-to-long v8, v5

    add-long/2addr v6, v8

    :try_start_7
    div-int/lit8 v0, v5, 0xa

    int-to-long v4, v0

    add-long/2addr v4, v6

    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-lez v0, :cond_6

    const v0, 0x7fffffff

    :goto_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v3, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->c:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(Ljava/util/List;)I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 192
    if-eqz v1, :cond_0

    .line 193
    :try_start_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->c:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/z;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 186
    :cond_6
    const-wide/32 v6, -0x80000000

    cmp-long v0, v4, v6

    if-gez v0, :cond_7

    const/high16 v0, -0x80000000

    goto :goto_4

    :cond_7
    long-to-int v0, v4

    goto :goto_4

    .line 192
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 188
    :catch_1
    move-exception v0

    move-object v1, v0

    goto/16 :goto_1
.end method

.method public final declared-synchronized b()V
    .locals 4

    .prologue
    .line 229
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 237
    :goto_0
    monitor-exit p0

    return-void

    .line 233
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/q;->c:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/b/q;->a:Ljava/util/Locale;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(ILjava/util/Locale;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 234
    :catch_0
    move-exception v0

    .line 235
    :try_start_2
    const-string v1, "DiskResourceCache"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x10

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Clearing cache: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
