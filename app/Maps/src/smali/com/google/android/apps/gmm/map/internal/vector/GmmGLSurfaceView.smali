.class public Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;
.super Landroid/view/SurfaceView;
.source "PG"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field static final a:Lcom/google/android/apps/gmm/map/internal/vector/i;


# instance fields
.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/google/android/apps/gmm/map/internal/vector/h;

.field public d:Lcom/google/android/apps/gmm/map/internal/vector/k;

.field public e:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

.field public f:Lcom/google/android/apps/gmm/map/internal/vector/e;

.field public g:Lcom/google/android/apps/gmm/map/internal/vector/f;

.field public h:I

.field public i:Z

.field private j:Z

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2086
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/vector/i;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 240
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 2088
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->b:Ljava/lang/ref/WeakReference;

    .line 241
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->d()V

    .line 242
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 249
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2088
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->b:Ljava/lang/ref/WeakReference;

    .line 250
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->d()V

    .line 251
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    .line 270
    invoke-interface {v1, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 273
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-ge v0, v2, :cond_0

    .line 274
    const/4 v0, 0x4

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 290
    :goto_0
    return-void

    .line 282
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 283
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getPixelFormat()I

    move-result v0

    .line 284
    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->setFormat(I)V

    goto :goto_0
.end method


# virtual methods
.method public final V_()V
    .locals 3

    .prologue
    .line 590
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v1

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->l:Z

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 628
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v1

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->b:Z

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->a:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 639
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->b:Z

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->l:Z

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->m:Z

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->a:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->c:Z

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 256
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 263
    return-void

    .line 262
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method protected onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 658
    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    .line 662
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->j:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    .line 663
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 665
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    if-eqz v0, :cond_3

    .line 666
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->b()I

    move-result v0

    .line 670
    :goto_0
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/vector/h;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->b:Ljava/lang/ref/WeakReference;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/vector/k;->e()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/vector/h;-><init>(Ljava/lang/ref/WeakReference;Lcom/google/android/apps/gmm/map/c/a/a;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    .line 672
    if-eq v0, v1, :cond_1

    .line 673
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->a(I)V

    .line 675
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->start()V

    .line 677
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->j:Z

    .line 678
    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 690
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    if-eqz v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->c()V

    .line 693
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->j:Z

    .line 694
    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    .line 695
    return-void
.end method

.method public final setKeepEglContextOnDetach(Z)V
    .locals 1

    .prologue
    .line 375
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->k:Z

    .line 376
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->c()V

    .line 379
    :cond_0
    return-void
.end method

.method public final setPreserveEGLContextOnPause(Z)V
    .locals 0

    .prologue
    .line 349
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->i:Z

    .line 350
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 618
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v4

    :try_start_0
    iput p3, v3, Lcom/google/android/apps/gmm/map/internal/vector/h;->j:I

    iput p4, v3, Lcom/google/android/apps/gmm/map/internal/vector/h;->k:I

    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/google/android/apps/gmm/map/internal/vector/h;->n:Z

    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/google/android/apps/gmm/map/internal/vector/h;->l:Z

    const/4 v0, 0x0

    iput-boolean v0, v3, Lcom/google/android/apps/gmm/map/internal/vector/h;->m:Z

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, v3, Lcom/google/android/apps/gmm/map/internal/vector/h;->a:Z

    if-nez v0, :cond_1

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/map/internal/vector/h;->c:Z

    if-nez v0, :cond_1

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/map/internal/vector/h;->m:Z

    if-nez v0, :cond_1

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/map/internal/vector/h;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/vector/h;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    :try_start_1
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v1

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->d:Z

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->h:Z

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->e:Z

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->h:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3

    .prologue
    .line 609
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->c:Lcom/google/android/apps/gmm/map/internal/vector/h;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->d:Z

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->e:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method
