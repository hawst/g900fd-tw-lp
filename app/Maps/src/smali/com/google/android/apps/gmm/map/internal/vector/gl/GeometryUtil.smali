.class public Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:F

.field public static final l:[[F

.field private static final q:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;",
            ">;"
        }
    .end annotation
.end field

.field private static final r:[I

.field private static final s:[I

.field private static final t:[I

.field private static final u:[I

.field private static final v:[[F


# instance fields
.field public final b:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final c:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final d:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final e:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final f:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final g:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final h:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final i:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final j:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final k:Lcom/google/android/apps/gmm/map/b/a/y;

.field public m:Lcom/google/android/apps/gmm/map/internal/vector/gl/d;

.field private final n:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final o:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final p:Lcom/google/android/apps/gmm/map/b/a/ay;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/high16 v6, 0x3e800000    # 0.25f

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x2

    .line 47
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->nativeInitClass()Z

    .line 95
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    .line 97
    sput v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a:F

    .line 124
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/b;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->q:Ljava/lang/ThreadLocal;

    .line 210
    const/4 v0, 0x7

    new-array v0, v0, [[F

    const/4 v1, 0x0

    new-array v2, v3, [F

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [F

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    new-array v1, v3, [F

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    const/4 v1, 0x3

    new-array v2, v3, [F

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    new-array v1, v3, [F

    fill-array-data v1, :array_4

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [F

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    .line 221
    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput v5, v0, v1

    const/4 v1, 0x1

    aput v4, v0, v1

    aput v4, v0, v3

    const/4 v1, 0x3

    aput v4, v0, v1

    aput v5, v0, v7

    const/4 v1, 0x5

    const/high16 v2, 0x3f000000    # 0.5f

    aput v2, v0, v1

    const/4 v1, 0x6

    aput v4, v0, v1

    const/4 v1, 0x7

    const/high16 v2, 0x3f000000    # 0.5f

    aput v2, v0, v1

    .line 228
    new-array v0, v7, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->r:[I

    .line 236
    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput v5, v0, v1

    const/4 v1, 0x1

    const/high16 v2, 0x3f000000    # 0.5f

    aput v2, v0, v1

    aput v4, v0, v3

    const/4 v1, 0x3

    const/high16 v2, 0x3f000000    # 0.5f

    aput v2, v0, v1

    aput v5, v0, v7

    const/4 v1, 0x5

    aput v4, v0, v1

    const/4 v1, 0x6

    aput v4, v0, v1

    const/4 v1, 0x7

    aput v4, v0, v1

    .line 243
    new-array v0, v7, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->s:[I

    .line 251
    new-array v0, v7, [F

    const/4 v1, 0x0

    aput v5, v0, v1

    const/4 v1, 0x1

    aput v6, v0, v1

    aput v4, v0, v3

    const/4 v1, 0x3

    aput v6, v0, v1

    .line 256
    new-array v0, v3, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->t:[I

    .line 262
    const/16 v0, 0xa

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput v5, v0, v1

    const/4 v1, 0x1

    aput v6, v0, v1

    aput v4, v0, v3

    const/4 v1, 0x3

    aput v6, v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    aput v1, v0, v7

    const/4 v1, 0x5

    aput v6, v0, v1

    const/4 v1, 0x6

    aput v5, v0, v1

    const/4 v1, 0x7

    aput v6, v0, v1

    const/16 v1, 0x8

    aput v4, v0, v1

    const/16 v1, 0x9

    aput v6, v0, v1

    .line 270
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->u:[I

    .line 1559
    const/16 v0, 0x10

    new-array v0, v0, [[F

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->v:[[F

    .line 1560
    return-void

    .line 210
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x3f000000    # 0.5f
    .end array-data

    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
    .end array-data

    :array_4
    .array-data 4
        0x0
        0x3e800000    # 0.25f
    .end array-data

    :array_5
    .array-data 4
        0x3f800000    # 1.0f
        0x3e800000    # 0.25f
    .end array-data

    :array_6
    .array-data 4
        0x3f000000    # 0.5f
        0x3e800000    # 0.25f
    .end array-data

    .line 228
    :array_7
    .array-data 4
        0x0
        0x1
        0x2
        0x3
    .end array-data

    .line 243
    :array_8
    .array-data 4
        0x2
        0x3
        0x0
        0x1
    .end array-data

    .line 256
    :array_9
    .array-data 4
        0x4
        0x5
    .end array-data

    .line 270
    :array_a
    .array-data 4
        0x4
        0x5
        0x6
        0x4
        0x5
    .end array-data
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->m:Lcom/google/android/apps/gmm/map/internal/vector/gl/d;

    .line 140
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 141
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 142
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 143
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 144
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 145
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 146
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 147
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 148
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 149
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 150
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 151
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 152
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    .line 153
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    .line 154
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    .line 155
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    .line 156
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    .line 157
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    .line 158
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    .line 159
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    .line 160
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 161
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ab;)I
    .locals 2

    .prologue
    .line 1523
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, -0x1

    .line 1524
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1526
    const/16 v0, 0x8

    .line 1531
    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, 0x8

    goto :goto_0
.end method

.method public static a()Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->q:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;

    return-object v0
.end method

.method public static a(IZI[ILcom/google/android/apps/gmm/map/legacy/a/c/a/b;)V
    .locals 8

    .prologue
    .line 1813
    if-eqz p1, :cond_0

    .line 1816
    const/4 v0, 0x5

    .line 1821
    :goto_0
    invoke-interface {p4}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->i()I

    .line 1820
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->v:[[F

    aget-object v1, v1, p2

    if-nez v1, :cond_2

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->v:[[F

    const/4 v1, 0x1

    shl-int v4, v1, p2

    mul-int/lit8 v1, v4, 0x5

    shl-int/lit8 v1, v1, 0x1

    new-array v5, v1, [F

    const/high16 v1, 0x3f000000    # 0.5f

    int-to-float v2, v4

    div-float v2, v1, v2

    const/4 v1, 0x0

    :goto_1
    array-length v6, v5

    if-ge v1, v6, :cond_1

    const/4 v6, 0x0

    aput v6, v5, v1

    add-int/lit8 v6, v1, 0x1

    aput v2, v5, v6

    add-int/lit8 v6, v1, 0x2

    const/high16 v7, 0x3f800000    # 1.0f

    aput v7, v5, v6

    add-int/lit8 v6, v1, 0x3

    aput v2, v5, v6

    add-int/lit8 v6, v1, 0x4

    const/high16 v7, 0x3f800000    # 1.0f

    aput v7, v5, v6

    add-int/lit8 v6, v1, 0x5

    aput v2, v5, v6

    add-int/lit8 v6, v1, 0x6

    const/4 v7, 0x0

    aput v7, v5, v6

    add-int/lit8 v6, v1, 0x7

    aput v2, v5, v6

    add-int/lit8 v6, v1, 0x8

    const/high16 v7, 0x3f000000    # 0.5f

    aput v7, v5, v6

    add-int/lit8 v6, v1, 0x9

    aput v2, v5, v6

    const/high16 v6, 0x3f800000    # 1.0f

    int-to-float v7, v4

    div-float/2addr v6, v7

    add-float/2addr v2, v6

    add-int/lit8 v1, v1, 0xa

    goto :goto_1

    .line 1818
    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    .line 1820
    :cond_1
    aput-object v5, v3, p2

    :cond_2
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->v:[[F

    aget-object v3, v1, p2

    .line 1825
    if-eqz p3, :cond_3

    array-length v1, p3

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 1826
    :cond_3
    if-nez p3, :cond_4

    const/4 v1, 0x0

    .line 1827
    :goto_2
    const/4 v2, 0x1

    :goto_3
    if-ge v2, p0, :cond_6

    .line 1828
    mul-int/lit8 v4, v1, 0x5

    shl-int/lit8 v4, v4, 0x1

    shl-int/lit8 v5, v0, 0x1

    invoke-interface {p4, v3, v4, v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a([FII)V

    .line 1827
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1826
    :cond_4
    const/4 v1, 0x0

    aget v1, p3, v1

    goto :goto_2

    .line 1833
    :cond_5
    const/4 v1, 0x1

    :goto_4
    if-ge v1, p0, :cond_6

    .line 1834
    aget v2, p3, v1

    mul-int/lit8 v2, v2, 0x5

    shl-int/lit8 v2, v2, 0x1

    shl-int/lit8 v4, v0, 0x1

    invoke-interface {p4, v3, v2, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a([FII)V

    .line 1833
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1839
    :cond_6
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V
    .locals 6

    .prologue
    const v3, 0x477fff00    # 65535.0f

    const/high16 v2, 0x3f000000    # 0.5f

    .line 1379
    .line 1380
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v0, v0

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v0, v0

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    sget v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a:F

    div-float v0, v2, v0

    .line 1381
    iget v1, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v1, v0

    iput v1, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v1, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v0, v1

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 1384
    iget v0, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    add-float/2addr v0, v2

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 1385
    iget v1, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    add-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    .line 1387
    div-int/lit16 v2, v0, 0x100

    int-to-byte v2, v2

    rem-int/lit16 v0, v0, 0x100

    int-to-byte v0, v0

    div-int/lit16 v3, v1, 0x100

    int-to-byte v3, v3

    rem-int/lit16 v1, v1, 0x100

    int-to-byte v1, v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    const/16 v5, 0x8

    aput-byte v2, v4, v5

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    const/16 v4, 0x9

    aput-byte v0, v2, v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    const/16 v2, 0xa

    aput-byte v3, v0, v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    const/16 v2, 0xb

    aput-byte v1, v0, v2

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    .line 1392
    return-void
.end method

.method public static b(Lcom/google/android/apps/gmm/map/b/a/ab;)I
    .locals 2

    .prologue
    .line 1537
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, -0x1

    .line 1540
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1543
    const/16 v0, 0x12

    .line 1546
    :goto_0
    return v0

    :cond_0
    add-int/lit8 v1, v0, 0x2

    mul-int/lit8 v1, v1, 0x6

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private native nativeAddExtrudedRoad([IIIFZZIIJ)I
.end method

.method private native nativeAddExtrudedRoadWithWidths([IIIFZZII[FJ)I
.end method

.method private static native nativeInitClass()Z
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b/a/ab;FLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/internal/vector/gl/k;ZZIILcom/google/android/apps/gmm/map/internal/vector/gl/c;Z)I
    .locals 31

    .prologue
    .line 444
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->m:Lcom/google/android/apps/gmm/map/internal/vector/gl/d;

    const/4 v5, 0x0

    iput v5, v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    .line 445
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->m:Lcom/google/android/apps/gmm/map/internal/vector/gl/d;

    move/from16 v0, p7

    div-int/lit16 v5, v0, 0x100

    int-to-byte v5, v5

    move/from16 v0, p7

    rem-int/lit16 v6, v0, 0x100

    int-to-byte v6, v6

    move/from16 v0, p8

    int-to-byte v7, v0

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(BBBB)V

    .line 450
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->m:Lcom/google/android/apps/gmm/map/internal/vector/gl/d;

    const/high16 v4, 0x3f800000    # 1.0f

    cmpg-float v4, p2, v4

    if-gez v4, :cond_0

    const/4 v4, 0x0

    :goto_0
    return v4

    :cond_0
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v4, v4

    div-int/lit8 v14, v4, 0x3

    const/4 v4, 0x2

    if-ge v14, v4, :cond_1

    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->b()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b()I

    move-result v5

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v4, v1, v15}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v4, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, v16

    invoke-virtual {v0, v4, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v16

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v4, v6

    move-object/from16 v0, v18

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v6

    move-object/from16 v0, v18

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v18

    move/from16 v1, p2

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v20

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    neg-int v4, v4

    iput v4, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v4, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v4, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v6, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v4, v6

    iput v4, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v4, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v6, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v4, v6

    iput v4, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, p9

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->d:Z

    if-eqz v4, :cond_7

    iget v4, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v4, v6

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v4, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v4, v6

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    :goto_1
    move/from16 v0, p2

    neg-float v4, v0

    if-eqz p10, :cond_2

    iget-object v6, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    const/4 v9, 0x1

    aput v4, v6, v9

    iget v6, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    or-int/lit8 v6, v6, 0x20

    iput v6, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    :cond_2
    sget-object v6, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->r:[I

    const/4 v9, 0x0

    aget v6, v6, v9

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p9

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->e:Z

    if-eqz v6, :cond_8

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v6, v9

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v6, v9

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    :goto_2
    sget-object v6, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->r:[I

    const/4 v9, 0x1

    aget v6, v6, v9

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v9, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v6, v9

    iput v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v9, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v6, v9

    iput v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, p9

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->d:Z

    if-eqz v6, :cond_9

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v6, v9

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v6, v9

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    :goto_3
    if-eqz p10, :cond_3

    const/4 v4, 0x0

    const/4 v6, 0x0

    iget-object v9, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    const/4 v10, 0x1

    aput v6, v9, v10

    iget v6, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    or-int/lit8 v6, v6, 0x20

    iput v6, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    :cond_3
    sget-object v6, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->r:[I

    const/4 v9, 0x2

    aget v6, v6, v9

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p9

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->e:Z

    if-eqz v6, :cond_a

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v6, v9

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v6, v9

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    :goto_4
    sget-object v6, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->r:[I

    const/4 v9, 0x3

    aget v6, v6, v9

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v6, 0x4

    if-eqz p5, :cond_b

    add-int/lit8 v9, v5, 0x1

    add-int/lit8 v10, v5, 0x2

    add-int/lit8 v11, v5, 0x3

    move-object/from16 v0, p4

    invoke-virtual {v0, v5, v9, v10, v11}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIII)V

    :goto_5
    add-int/lit8 v9, v5, 0x2

    add-int/lit8 v10, v5, 0x3

    add-int/lit8 v11, v5, 0x4

    add-int/lit8 v12, v5, 0x5

    move-object/from16 v0, p4

    invoke-virtual {v0, v9, v10, v11, v12}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIII)V

    add-int/lit8 v5, v5, 0x4

    const/4 v9, 0x2

    if-le v14, v9, :cond_16

    mul-float v23, p2, p2

    const/4 v9, 0x1

    move v12, v9

    move v10, v6

    move v9, v5

    :goto_6
    add-int/lit8 v5, v14, -0x1

    if-ge v12, v5, :cond_13

    if-eqz p10, :cond_4

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/b/a/y;->i()F

    move-result v5

    add-float/2addr v4, v5

    iget-object v5, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    const/4 v6, 0x1

    aput v4, v5, v6

    iget v5, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    :cond_4
    add-int/lit8 v5, v12, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, v17

    invoke-virtual {v0, v5, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v17

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v5, v6

    move-object/from16 v0, v19

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v5, v6

    move-object/from16 v0, v19

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v19

    move/from16 v1, p2

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v18

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-long v0, v5

    move-wide/from16 v24, v0

    move-object/from16 v0, v19

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-long v0, v5

    move-wide/from16 v26, v0

    mul-long v24, v24, v26

    move-object/from16 v0, v18

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-long v0, v5

    move-wide/from16 v26, v0

    move-object/from16 v0, v19

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-long v0, v5

    move-wide/from16 v28, v0

    mul-long v26, v26, v28

    sub-long v24, v24, v26

    const-wide/16 v26, 0x0

    cmp-long v5, v24, v26

    if-lez v5, :cond_c

    const/4 v5, 0x1

    :goto_7
    const/4 v11, 0x1

    move-object/from16 v0, v20

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v21

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v24, v0

    add-int v6, v6, v24

    move-object/from16 v0, v22

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v21

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v24, v0

    add-int v6, v6, v24

    move-object/from16 v0, v22

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static/range {v21 .. v22}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v6

    const/high16 v24, 0x3f800000    # 1.0f

    cmpl-float v24, v6, v24

    if-lez v24, :cond_1d

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v24

    const/16 v25, 0x0

    cmpl-float v24, v24, v25

    if-ltz v24, :cond_1d

    div-float v6, v23, v6

    move-object/from16 v0, v22

    move-object/from16 v1, v22

    invoke-static {v0, v6, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v24, v0

    add-int v6, v6, v24

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v24, v0

    add-int v6, v6, v24

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v24, v0

    sub-int v6, v6, v24

    iput v6, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v24, v0

    sub-int v6, v6, v24

    iput v6, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-eqz v5, :cond_d

    move-object v6, v7

    :goto_8
    move-object/from16 v0, v16

    invoke-static {v0, v15, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v24

    const/high16 v25, 0x3f000000    # 0.5f

    cmpg-float v24, v24, v25

    if-gez v24, :cond_1d

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v6

    const/high16 v24, 0x3f000000    # 0.5f

    cmpg-float v6, v6, v24

    if-gez v6, :cond_1d

    move-object/from16 v0, p9

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->e:Z

    if-nez v6, :cond_5

    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v6, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v6, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v6, v8, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    :cond_5
    move-object/from16 v0, p9

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->d:Z

    if-nez v6, :cond_6

    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    :cond_6
    sget-object v6, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->t:[I

    const/4 v11, 0x0

    aget v6, v6, v11

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    sget-object v6, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->t:[I

    const/4 v11, 0x1

    aget v6, v6, v11

    iput v6, v8, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    add-int/lit8 v10, v10, 0x2

    add-int/lit8 v6, v9, 0x1

    add-int/lit8 v11, v9, 0x2

    add-int/lit8 v24, v9, 0x3

    move-object/from16 v0, p4

    move/from16 v1, v24

    invoke-virtual {v0, v9, v6, v11, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIII)V

    add-int/lit8 v9, v9, 0x2

    const/4 v6, 0x0

    move/from16 v30, v6

    move v6, v9

    move v9, v10

    move/from16 v10, v30

    :goto_9
    if-eqz v10, :cond_1c

    move-object/from16 v0, p9

    iget-boolean v10, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->d:Z

    if-eqz v10, :cond_e

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v10, v11

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v10, v11

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    :goto_a
    sget-object v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->u:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p9

    iget-boolean v10, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->e:Z

    if-eqz v10, :cond_f

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v10, v11

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v10, v11

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    :goto_b
    sget-object v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->u:[I

    const/4 v11, 0x1

    aget v10, v10, v11

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    sget-object v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->u:[I

    const/4 v11, 0x2

    aget v10, v10, v11

    move-object/from16 v0, v16

    iput v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p4

    move-object/from16 v1, v16

    invoke-virtual {v13, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p9

    iget-boolean v10, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->d:Z

    if-eqz v10, :cond_10

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v21

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v10, v11

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v21

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v10, v11

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    :goto_c
    sget-object v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->u:[I

    const/4 v11, 0x3

    aget v10, v10, v11

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p9

    iget-boolean v10, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->e:Z

    if-eqz v10, :cond_11

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v21

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v10, v11

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v21

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v10, v11

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    :goto_d
    sget-object v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->u:[I

    const/4 v11, 0x4

    aget v10, v10, v11

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    add-int/lit8 v9, v9, 0x5

    if-eqz v5, :cond_12

    add-int/lit8 v5, v6, 0x2

    add-int/lit8 v10, v6, 0x1

    add-int/lit8 v11, v6, 0x4

    move-object/from16 v0, p4

    invoke-virtual {v0, v5, v10, v11}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(III)V

    :goto_e
    add-int/lit8 v5, v6, 0x3

    add-int/lit8 v10, v6, 0x4

    add-int/lit8 v11, v6, 0x5

    add-int/lit8 v24, v6, 0x6

    move-object/from16 v0, p4

    move/from16 v1, v24

    invoke-virtual {v0, v5, v10, v11, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIII)V

    add-int/lit8 v5, v6, 0x5

    move v6, v9

    :goto_f
    move-object/from16 v0, v21

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v21

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v21

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v20

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v19

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v18

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v19

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v18

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v19

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v18

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v16

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v9, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v9, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v16

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v9, v15, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v16

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v16

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    add-int/lit8 v9, v12, 0x1

    move v12, v9

    move v10, v6

    move v9, v5

    goto/16 :goto_6

    :cond_7
    iget v4, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v4, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v4, v15, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_1

    :cond_8
    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_2

    :cond_9
    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_3

    :cond_a
    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v6, v15, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v6, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_4

    :cond_b
    add-int/lit8 v9, v5, 0x2

    add-int/lit8 v10, v5, 0x2

    add-int/lit8 v11, v5, 0x2

    add-int/lit8 v12, v5, 0x2

    move-object/from16 v0, p4

    invoke-virtual {v0, v9, v10, v11, v12}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIII)V

    goto/16 :goto_5

    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_7

    :cond_d
    move-object v6, v8

    goto/16 :goto_8

    :cond_e
    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_a

    :cond_f
    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_b

    :cond_10
    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_c

    :cond_11
    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_d

    :cond_12
    add-int/lit8 v5, v6, 0x2

    add-int/lit8 v10, v6, 0x3

    move-object/from16 v0, p4

    invoke-virtual {v0, v6, v5, v10}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(III)V

    goto/16 :goto_e

    :cond_13
    move v5, v9

    move v6, v10

    :goto_10
    move-object/from16 v0, p9

    iget-boolean v9, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->d:Z

    if-eqz v9, :cond_17

    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v21

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v9, v10

    iput v9, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v21

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v9, v10

    iput v9, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    :goto_11
    if-eqz p10, :cond_14

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/b/a/y;->i()F

    move-result v9

    add-float/2addr v4, v9

    iget-object v9, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    const/4 v10, 0x1

    aput v4, v9, v10

    iget v9, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    or-int/lit8 v9, v9, 0x20

    iput v9, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    :cond_14
    sget-object v9, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->s:[I

    const/4 v10, 0x0

    aget v9, v9, v10

    iput v9, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p9

    iget-boolean v9, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->e:Z

    if-eqz v9, :cond_18

    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v21

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v9, v10

    iput v9, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v21

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v9, v10

    iput v9, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    :goto_12
    sget-object v9, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->s:[I

    const/4 v10, 0x1

    aget v9, v9, v10

    iput v9, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v21

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    neg-int v9, v9

    iput v9, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v21

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v9, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v10, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v9, v10

    move-object/from16 v0, v17

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int v8, v9, v8

    move-object/from16 v0, v17

    iput v8, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, p9

    iget-boolean v8, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->d:Z

    if-eqz v8, :cond_19

    move-object/from16 v0, v17

    iget v8, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v21

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v8, v9

    iput v8, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v8, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v21

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v8, v9

    iput v8, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    :goto_13
    if-eqz p10, :cond_15

    add-float v4, v4, p2

    iget-object v8, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    const/4 v9, 0x1

    aput v4, v8, v9

    iget v4, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    :cond_15
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->s:[I

    const/4 v8, 0x2

    aget v4, v4, v8

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p9

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->e:Z

    if-eqz v4, :cond_1a

    move-object/from16 v0, v17

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v21

    iget v8, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v4, v8

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v21

    iget v8, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v8

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    :goto_14
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->s:[I

    const/4 v8, 0x3

    aget v4, v4, v8

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    add-int/lit8 v4, v6, 0x4

    if-eqz p6, :cond_1b

    add-int/lit8 v6, v5, 0x1

    add-int/lit8 v7, v5, 0x2

    add-int/lit8 v8, v5, 0x3

    move-object/from16 v0, p4

    invoke-virtual {v0, v5, v6, v7, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIII)V

    :goto_15
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v6, 0x0

    iput v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_0

    :cond_16
    move-object/from16 v0, v16

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v17

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v16

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v17

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v20

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v21

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v21

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v21

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_10

    :cond_17
    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v9, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v9, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v9, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_11

    :cond_18
    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v9, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v9, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v9, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_12

    :cond_19
    move-object/from16 v0, v17

    iget v8, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v8, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v8, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v8, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v17

    iget v8, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v8, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_13

    :cond_1a
    move-object/from16 v0, v17

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v17

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_14

    :cond_1b
    move-object/from16 v0, p4

    invoke-virtual {v0, v5, v5, v5, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIII)V

    goto/16 :goto_15

    :cond_1c
    move v5, v6

    move v6, v9

    goto/16 :goto_f

    :cond_1d
    move v6, v9

    move v9, v10

    move v10, v11

    goto/16 :goto_9
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/internal/vector/gl/k;ZZIILcom/google/android/apps/gmm/map/internal/vector/gl/c;Z[F)I
    .locals 27

    .prologue
    .line 478
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;

    if-eqz v2, :cond_1

    .line 479
    check-cast p4, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;

    move-object/from16 v0, p4

    iget-wide v12, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a:J

    .line 480
    sget-boolean v2, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    if-nez v2, :cond_0

    .line 481
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, p2

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v2, p0

    move/from16 v6, p3

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move-object/from16 v11, p11

    invoke-direct/range {v2 .. v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->nativeAddExtrudedRoadWithWidths([IIIFZZII[FJ)I

    move-result v2

    .line 497
    :goto_0
    return v2

    .line 485
    :cond_0
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    move-object/from16 v0, p2

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, p2

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v3, p0

    move/from16 v7, p3

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    move/from16 v11, p8

    invoke-direct/range {v3 .. v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->nativeAddExtrudedRoad([IIIFZZIIJ)I

    move-result v2

    goto :goto_0

    .line 490
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->m:Lcom/google/android/apps/gmm/map/internal/vector/gl/d;

    const/4 v3, 0x0

    iput v3, v2, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    .line 491
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->m:Lcom/google/android/apps/gmm/map/internal/vector/gl/d;

    move/from16 v0, p7

    div-int/lit16 v3, v0, 0x100

    int-to-byte v3, v3

    move/from16 v0, p7

    rem-int/lit16 v4, v0, 0x100

    int-to-byte v4, v4

    move/from16 v0, p8

    int-to-byte v5, v0

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(BBBB)V

    .line 497
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->m:Lcom/google/android/apps/gmm/map/internal/vector/gl/d;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v2, v2

    div-int/lit8 v11, v2, 0x3

    const/4 v2, 0x2

    if-ge v11, v2, :cond_2

    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->b()V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v21, v0

    sget-boolean v2, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    if-nez v2, :cond_3

    const/4 v2, 0x0

    aget v2, p11, v2

    float-to-int v2, v2

    const/4 v3, 0x1

    aget v3, p11, v3

    float-to-int v3, v3

    const/4 v5, 0x2

    aget v5, p11, v5

    float-to-int v5, v5

    const/4 v6, 0x3

    aget v6, p11, v6

    float-to-int v6, v6

    int-to-byte v2, v2

    int-to-byte v3, v3

    int-to-byte v5, v5

    int-to-byte v6, v6

    iget-object v7, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    const/16 v8, 0xc

    aput-byte v2, v7, v8

    iget-object v2, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    const/16 v7, 0xd

    aput-byte v3, v2, v7

    iget-object v2, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    const/16 v3, 0xe

    aput-byte v5, v2, v3

    iget-object v2, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    const/16 v3, 0xf

    aput-byte v6, v2, v3

    iget v2, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    :cond_3
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b()I

    move-result v3

    const/4 v2, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v12}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v2, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v13}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget v2, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v5, v12, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v5

    iput v2, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v5, v12, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, v5

    iput v2, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/high16 v2, 0x437f0000    # 255.0f

    move-object/from16 v0, v17

    invoke-static {v15, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v17

    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    neg-int v2, v2

    iput v2, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v2, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/high16 v2, -0x40800000    # -1.0f

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v5, v6

    move-object/from16 v0, v20

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v17

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v5, v6

    move-object/from16 v0, v20

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v5, 0x0

    move-object/from16 v0, v21

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    const/4 v5, 0x0

    move-object/from16 v0, v21

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v5, 0x0

    move-object/from16 v0, v21

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->f(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    move/from16 v0, p3

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(F)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    invoke-virtual {v5, v12}, Lcom/google/android/apps/gmm/map/b/a/y;->f(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    if-eqz p10, :cond_4

    const/high16 v5, -0x40800000    # -1.0f

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    const/4 v7, 0x1

    aput v5, v6, v7

    iget v5, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    :cond_4
    sget-object v5, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->r:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    move-object/from16 v0, v21

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v5, v6

    move-object/from16 v0, v20

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v17

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v5, v6

    move-object/from16 v0, v20

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->r:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    move-object/from16 v0, v21

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    if-eqz p10, :cond_5

    const/4 v2, 0x0

    const/4 v5, 0x0

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    const/4 v7, 0x1

    aput v5, v6, v7

    iget v5, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    :cond_5
    move-object/from16 v0, v17

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v17

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v20

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->r:[I

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, v21

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v17

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    neg-int v5, v5

    move-object/from16 v0, v17

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    neg-int v6, v6

    move-object/from16 v0, v20

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v5, 0x0

    move-object/from16 v0, v20

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->r:[I

    const/4 v6, 0x3

    aget v5, v5, v6

    move-object/from16 v0, v21

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v5, 0x4

    if-eqz p5, :cond_7

    add-int/lit8 v6, v3, 0x1

    add-int/lit8 v7, v3, 0x2

    add-int/lit8 v8, v3, 0x3

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v6, v7, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIII)V

    :goto_1
    add-int/lit8 v6, v3, 0x2

    add-int/lit8 v7, v3, 0x3

    add-int/lit8 v8, v3, 0x4

    add-int/lit8 v9, v3, 0x5

    move-object/from16 v0, p4

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIII)V

    add-int/lit8 v3, v3, 0x4

    const/4 v6, 0x2

    if-le v11, v6, :cond_e

    const/4 v6, 0x1

    move v8, v6

    move v7, v5

    move v5, v3

    :goto_2
    add-int/lit8 v3, v11, -0x1

    if-ge v8, v3, :cond_b

    if-eqz p10, :cond_6

    invoke-virtual {v15}, Lcom/google/android/apps/gmm/map/b/a/y;->i()F

    move-result v3

    add-float/2addr v2, v3

    iget-object v3, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    const/4 v6, 0x1

    aput v2, v3, v6

    iget v3, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    :cond_6
    move v6, v2

    add-int/lit8 v2, v8, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v14}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget v2, v14, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v3

    move-object/from16 v0, v16

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, v14, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v3, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, v3

    move-object/from16 v0, v16

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/high16 v2, 0x437f0000    # 255.0f

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    iget v2, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-long v2, v2

    move-object/from16 v0, v16

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-long v0, v9

    move-wide/from16 v22, v0

    mul-long v2, v2, v22

    iget v9, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-long v0, v9

    move-wide/from16 v22, v0

    move-object/from16 v0, v16

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-long v0, v9

    move-wide/from16 v24, v0

    mul-long v22, v22, v24

    sub-long v2, v2, v22

    const-wide/16 v22, 0x0

    cmp-long v2, v2, v22

    if-lez v2, :cond_8

    const/4 v2, 0x1

    :goto_3
    const/4 v9, 0x1

    move-object/from16 v0, v17

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v18

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v22, v0

    add-int v3, v3, v22

    move-object/from16 v0, v19

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v18

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v22, v0

    add-int v3, v3, v22

    move-object/from16 v0, v19

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    const/high16 v22, 0x3f800000    # 1.0f

    cmpl-float v22, v3, v22

    if-lez v22, :cond_11

    invoke-static/range {v15 .. v16}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v22

    const/16 v23, 0x0

    cmpl-float v22, v22, v23

    if-ltz v22, :cond_11

    const v22, 0x477e0100    # 65025.0f

    div-float v3, v22, v3

    move-object/from16 v0, v19

    move-object/from16 v1, v19

    invoke-static {v0, v3, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v22, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v23, v0

    add-int v22, v22, v23

    move/from16 v0, v22

    iput v0, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v0, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v22, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v23, v0

    add-int v22, v22, v23

    move/from16 v0, v22

    iput v0, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v0, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v22, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v23, v0

    sub-int v22, v22, v23

    move/from16 v0, v22

    iput v0, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v0, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v22, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v23, v0

    sub-int v22, v22, v23

    move/from16 v0, v22

    iput v0, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-eqz v2, :cond_9

    :goto_4
    invoke-static {v13, v12, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v22

    const/high16 v23, 0x3f000000    # 0.5f

    cmpg-float v22, v22, v23

    if-gez v22, :cond_11

    invoke-static {v13, v14, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    const/high16 v22, 0x3f000000    # 0.5f

    cmpg-float v3, v3, v22

    if-gez v3, :cond_11

    move-object/from16 v0, v19

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v19

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v19

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v20

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    const/4 v3, 0x0

    move-object/from16 v0, v21

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    const/4 v3, 0x0

    move-object/from16 v0, v21

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v3, 0x0

    move-object/from16 v0, v21

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->f(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(F)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    invoke-virtual {v3, v13}, Lcom/google/android/apps/gmm/map/b/a/y;->f(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->t:[I

    const/4 v9, 0x0

    aget v3, v3, v9

    move-object/from16 v0, v21

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v19

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    neg-int v3, v3

    move-object/from16 v0, v19

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v19

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    neg-int v3, v3

    move-object/from16 v0, v19

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v19

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    neg-int v3, v3

    move-object/from16 v0, v19

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v19

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v19

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v19

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v20

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->t:[I

    const/4 v9, 0x1

    aget v3, v3, v9

    move-object/from16 v0, v21

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    add-int/lit8 v7, v7, 0x2

    add-int/lit8 v3, v5, 0x1

    add-int/lit8 v9, v5, 0x2

    add-int/lit8 v22, v5, 0x3

    move-object/from16 v0, p4

    move/from16 v1, v22

    invoke-virtual {v0, v5, v3, v9, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIII)V

    add-int/lit8 v5, v5, 0x2

    const/4 v3, 0x0

    move/from16 v26, v3

    move v3, v5

    move v5, v7

    move/from16 v7, v26

    :goto_5
    if-eqz v7, :cond_10

    move-object/from16 v0, v17

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v17

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v20

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    const/4 v7, 0x0

    move-object/from16 v0, v21

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    const/4 v7, 0x0

    move-object/from16 v0, v21

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v7, 0x0

    move-object/from16 v0, v21

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->f(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v7

    move/from16 v0, p3

    invoke-virtual {v7, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(F)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v7

    invoke-virtual {v7, v13}, Lcom/google/android/apps/gmm/map/b/a/y;->f(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->u:[I

    const/4 v9, 0x0

    aget v7, v7, v9

    move-object/from16 v0, v21

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v17

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    neg-int v7, v7

    move-object/from16 v0, v17

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    neg-int v9, v9

    move-object/from16 v0, v20

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v7, 0x0

    move-object/from16 v0, v20

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->u:[I

    const/4 v9, 0x1

    aget v7, v7, v9

    move-object/from16 v0, v21

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v20

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v7, 0x0

    move-object/from16 v0, v20

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    const/4 v7, 0x0

    move-object/from16 v0, v21

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    const/4 v7, 0x0

    move-object/from16 v0, v21

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v7, 0x0

    move-object/from16 v0, v21

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->f(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v7

    move/from16 v0, p3

    invoke-virtual {v7, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(F)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v7

    invoke-virtual {v7, v13}, Lcom/google/android/apps/gmm/map/b/a/y;->f(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->u:[I

    const/4 v9, 0x2

    aget v7, v7, v9

    move-object/from16 v0, v21

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v18

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v18

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v18

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v20

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    const/4 v7, 0x0

    move-object/from16 v0, v21

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    const/4 v7, 0x0

    move-object/from16 v0, v21

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v7, 0x0

    move-object/from16 v0, v21

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->f(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v7

    move/from16 v0, p3

    invoke-virtual {v7, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(F)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v7

    invoke-virtual {v7, v13}, Lcom/google/android/apps/gmm/map/b/a/y;->f(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->u:[I

    const/4 v9, 0x3

    aget v7, v7, v9

    move-object/from16 v0, v21

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v18

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    neg-int v7, v7

    move-object/from16 v0, v18

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    neg-int v9, v9

    move-object/from16 v0, v20

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v7, 0x0

    move-object/from16 v0, v20

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->u:[I

    const/4 v9, 0x4

    aget v7, v7, v9

    move-object/from16 v0, v21

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    add-int/lit8 v5, v5, 0x5

    if-eqz v2, :cond_a

    add-int/lit8 v2, v3, 0x2

    add-int/lit8 v7, v3, 0x1

    add-int/lit8 v9, v3, 0x4

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v7, v9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(III)V

    :goto_6
    add-int/lit8 v2, v3, 0x3

    add-int/lit8 v7, v3, 0x4

    add-int/lit8 v9, v3, 0x5

    add-int/lit8 v22, v3, 0x6

    move-object/from16 v0, p4

    move/from16 v1, v22

    invoke-virtual {v0, v2, v7, v9, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIII)V

    add-int/lit8 v2, v3, 0x5

    move v3, v5

    :goto_7
    move-object/from16 v0, v18

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v18

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v17

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v18

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v17

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v16

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v5, v15, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v16

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v5, v15, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v16

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v5, v15, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v5, v12, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v5, v12, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v5, v12, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v5, v14, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v5, v14, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v5, v14, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    add-int/lit8 v5, v8, 0x1

    move v8, v5

    move v7, v3

    move v5, v2

    move v2, v6

    goto/16 :goto_2

    :cond_7
    add-int/lit8 v6, v3, 0x2

    add-int/lit8 v7, v3, 0x2

    add-int/lit8 v8, v3, 0x2

    add-int/lit8 v9, v3, 0x2

    move-object/from16 v0, p4

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIII)V

    goto/16 :goto_1

    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_9
    move-object v3, v4

    goto/16 :goto_4

    :cond_a
    add-int/lit8 v2, v3, 0x2

    add-int/lit8 v7, v3, 0x3

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v2, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(III)V

    goto :goto_6

    :cond_b
    move v3, v5

    move v5, v7

    :goto_8
    move-object/from16 v0, v18

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v18

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v18

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v20

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    const/4 v6, 0x0

    move-object/from16 v0, v21

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    const/4 v6, 0x0

    move-object/from16 v0, v21

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v6, 0x0

    move-object/from16 v0, v21

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->f(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    move/from16 v0, p3

    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(F)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    invoke-virtual {v6, v14}, Lcom/google/android/apps/gmm/map/b/a/y;->f(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    if-eqz p10, :cond_c

    invoke-virtual {v15}, Lcom/google/android/apps/gmm/map/b/a/y;->i()F

    move-result v6

    add-float/2addr v2, v6

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    const/4 v7, 0x1

    aput v2, v6, v7

    iget v6, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    or-int/lit8 v6, v6, 0x20

    iput v6, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    :cond_c
    sget-object v6, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->s:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    move-object/from16 v0, v21

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v18

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    neg-int v6, v6

    move-object/from16 v0, v18

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    neg-int v7, v7

    move-object/from16 v0, v20

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v20

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v6, 0x0

    move-object/from16 v0, v20

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    sget-object v6, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->s:[I

    const/4 v7, 0x1

    aget v6, v6, v7

    move-object/from16 v0, v21

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v18

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    neg-int v6, v6

    iput v6, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v18

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v6, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v6, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    neg-int v6, v6

    iput v6, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v6, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    neg-int v6, v6

    iput v6, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v6, v4, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    neg-int v6, v6

    iput v6, v4, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    if-eqz p10, :cond_d

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v2, v6

    neg-float v2, v2

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    const/4 v7, 0x1

    aput v2, v6, v7

    iget v2, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    :cond_d
    iget v2, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v18

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v2, v6

    move-object/from16 v0, v20

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v18

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v2, v6

    move-object/from16 v0, v20

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->s:[I

    const/4 v6, 0x2

    aget v2, v2, v6

    move-object/from16 v0, v21

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget v2, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v18

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v6

    move-object/from16 v0, v20

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v18

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, v4

    move-object/from16 v0, v20

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v20

    invoke-static {v10, v0, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/d;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->s:[I

    const/4 v4, 0x3

    aget v2, v2, v4

    move-object/from16 v0, v21

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p4

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    add-int/lit8 v2, v5, 0x4

    if-eqz p6, :cond_f

    add-int/lit8 v4, v3, 0x1

    add-int/lit8 v5, v3, 0x2

    add-int/lit8 v6, v3, 0x3

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIII)V

    goto/16 :goto_0

    :cond_e
    iget v6, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v6, v14, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v6, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v6, v14, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v6, v13, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v6, v14, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v17

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v18

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v17

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v18

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v17

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v18

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_8

    :cond_f
    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v3, v3, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIII)V

    goto/16 :goto_0

    :cond_10
    move v2, v3

    move v3, v5

    goto/16 :goto_7

    :cond_11
    move v3, v5

    move v5, v7

    move v7, v9

    goto/16 :goto_5
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ab;FZLcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;)V
    .locals 19

    .prologue
    .line 1631
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v2, v2

    div-int/lit8 v7, v2, 0x3

    .line 1632
    add-int/lit8 v8, v7, -0x1

    .line 1633
    invoke-interface/range {p6 .. p6}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->b()I

    move-result v9

    .line 1636
    if-gtz v8, :cond_1

    .line 1784
    :cond_0
    :goto_0
    return-void

    .line 1641
    :cond_1
    if-eqz p3, :cond_5

    .line 1642
    const/4 v2, 0x5

    .line 1646
    :goto_1
    mul-int v10, v2, v8

    .line 1648
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x0

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    const/4 v4, 0x0

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v4, 0x0

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 1649
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x0

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    const/4 v4, 0x0

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v4, 0x0

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 1650
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x0

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    const/4 v4, 0x0

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v4, 0x0

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 1652
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1653
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1656
    invoke-interface/range {p6 .. p6}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->b()I

    move-result v3

    add-int/2addr v3, v10

    move-object/from16 v0, p6

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(I)V

    .line 1659
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1661
    if-eqz p8, :cond_2

    .line 1662
    invoke-interface/range {p8 .. p8}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->i()I

    .line 1666
    :cond_2
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1667
    iget v3, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, p4

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v3, v4

    iput v3, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, p4

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v3, v4

    iput v3, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 1668
    const/4 v4, 0x0

    .line 1669
    const/4 v3, 0x1

    move/from16 v18, v3

    move v3, v4

    move/from16 v4, v18

    :goto_2
    if-ge v4, v7, :cond_6

    .line 1670
    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1671
    iget v12, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, p4

    iget v13, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v12, v13

    iput v12, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v12, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, p4

    iget v13, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v12, v13

    iput v12, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 1672
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v13, 0x0

    iput v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    const/4 v13, 0x0

    iput v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v13, 0x0

    iput v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v13, 0x0

    iput v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    const/4 v13, 0x0

    iput v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v13, 0x0

    iput v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v14, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v15, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v14, v15

    iput v14, v12, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v14, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v15, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v14, v15

    iput v14, v12, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v0, p2

    invoke-static {v12, v0, v13}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    iget v14, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v15, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v14, v15

    iput v14, v12, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v14, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v15, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v14, v15

    iput v14, v12, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, p6

    invoke-interface {v0, v12}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget v14, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v15, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v14, v15

    iput v14, v12, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v14, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v15, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v14, v15

    iput v14, v12, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, p6

    invoke-interface {v0, v12}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget v14, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v15, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v14, v15

    iput v14, v12, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v14, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v15, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v14, v15

    iput v14, v12, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, p6

    invoke-interface {v0, v12}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget v14, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v15, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v14, v15

    iput v14, v12, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v14, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v13, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v13, v14

    iput v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, p6

    invoke-interface {v0, v12}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    if-eqz p3, :cond_3

    move-object/from16 v0, p6

    invoke-interface {v0, v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1674
    :cond_3
    if-eqz p8, :cond_4

    .line 1676
    iget v12, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v13, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v12, v13

    iput v12, v11, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v12, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v13, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v12, v13

    iput v12, v11, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 1678
    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/b/a/y;->i()F

    move-result v12

    mul-float v12, v12, p5

    .line 1679
    const/4 v13, 0x0

    move-object/from16 v0, p8

    invoke-interface {v0, v13, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    .line 1680
    const/high16 v13, 0x3f800000    # 1.0f

    move-object/from16 v0, p8

    invoke-interface {v0, v13, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    .line 1681
    add-float/2addr v3, v12

    .line 1682
    const/high16 v12, 0x3f800000    # 1.0f

    move-object/from16 v0, p8

    invoke-interface {v0, v12, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    .line 1683
    const/4 v12, 0x0

    move-object/from16 v0, p8

    invoke-interface {v0, v12, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    .line 1685
    if-eqz p3, :cond_4

    .line 1686
    const/high16 v12, 0x3f000000    # 0.5f

    move-object/from16 v0, p8

    invoke-interface {v0, v12, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    .line 1669
    :cond_4
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v18, v6

    move-object v6, v5

    move-object/from16 v5, v18

    goto/16 :goto_2

    .line 1644
    :cond_5
    const/4 v2, 0x4

    goto/16 :goto_1

    .line 1711
    :cond_6
    if-eqz p7, :cond_0

    .line 1712
    add-int v3, v9, v10

    .line 1713
    const/16 v4, 0x7fff

    if-le v3, v4, :cond_7

    .line 1714
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x32

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " required, but we can only store 32767"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1717
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1718
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1719
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1720
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/b/a/ab;->f()Z

    .line 1722
    if-eqz p3, :cond_8

    .line 1723
    invoke-interface/range {p7 .. p7}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->c()I

    .line 1730
    :goto_3
    const/4 v3, 0x0

    :goto_4
    if-ge v3, v8, :cond_9

    .line 1731
    mul-int v11, v3, v2

    add-int/2addr v11, v9

    .line 1732
    add-int/lit8 v12, v11, 0x1

    add-int/lit8 v13, v11, 0x2

    move-object/from16 v0, p7

    invoke-interface {v0, v11, v12, v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    .line 1733
    add-int/lit8 v12, v11, 0x2

    add-int/lit8 v13, v11, 0x3

    move-object/from16 v0, p7

    invoke-interface {v0, v11, v12, v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    .line 1730
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1726
    :cond_8
    invoke-interface/range {p7 .. p7}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->c()I

    goto :goto_3

    .line 1735
    :cond_9
    if-eqz p3, :cond_0

    .line 1740
    const/4 v2, 0x0

    move v3, v2

    :goto_5
    add-int/lit8 v2, v8, -0x1

    if-ge v3, v2, :cond_c

    .line 1744
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1745
    add-int/lit8 v2, v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1746
    add-int/lit8 v2, v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1747
    iget v2, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v11, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v11

    iput v2, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v11, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, v11

    iput v2, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 1748
    iget v2, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v11, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v11

    iput v2, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v11, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, v11

    iput v2, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 1749
    iget v2, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-long v12, v2

    iget v2, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-long v14, v2

    mul-long/2addr v12, v14

    iget v2, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-long v14, v2

    iget v2, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-long v0, v2

    move-wide/from16 v16, v0

    mul-long v14, v14, v16

    sub-long/2addr v12, v14

    const-wide/16 v14, 0x0

    cmp-long v2, v12, v14

    if-lez v2, :cond_a

    const/4 v2, 0x1

    .line 1751
    :goto_6
    mul-int/lit8 v11, v3, 0x5

    add-int/2addr v11, v9

    .line 1752
    add-int/lit8 v12, v11, 0x5

    .line 1754
    if-eqz v2, :cond_b

    .line 1756
    add-int/lit8 v2, v11, 0x2

    add-int/lit8 v12, v12, 0x1

    add-int/lit8 v11, v11, 0x4

    move-object/from16 v0, p7

    invoke-interface {v0, v2, v12, v11}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    .line 1740
    :goto_7
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5

    .line 1749
    :cond_a
    const/4 v2, 0x0

    goto :goto_6

    .line 1759
    :cond_b
    add-int/lit8 v2, v11, 0x3

    add-int/lit8 v11, v11, 0x4

    move-object/from16 v0, p7

    invoke-interface {v0, v2, v11, v12}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    goto :goto_7

    .line 1762
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/b/a/ab;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1763
    add-int/lit8 v2, v8, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1764
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1765
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1766
    iget v2, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v3

    iput v2, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v3, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, v3

    iput v2, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 1767
    iget v2, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v3

    iput v2, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v3, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, v3

    iput v2, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 1768
    iget v2, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-long v2, v2

    iget v4, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    iget v4, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-long v4, v4

    iget v6, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-long v6, v6

    mul-long/2addr v4, v6

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_d

    const/4 v2, 0x1

    .line 1770
    :goto_8
    add-int/lit8 v3, v8, -0x1

    mul-int/lit8 v3, v3, 0x5

    add-int/2addr v3, v9

    .line 1772
    if-eqz v2, :cond_e

    .line 1776
    add-int/lit8 v2, v3, 0x2

    add-int/lit8 v4, v9, 0x1

    add-int/lit8 v3, v3, 0x4

    move-object/from16 v0, p7

    invoke-interface {v0, v2, v4, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    goto/16 :goto_0

    .line 1768
    :cond_d
    const/4 v2, 0x0

    goto :goto_8

    .line 1779
    :cond_e
    add-int/lit8 v2, v3, 0x3

    add-int/lit8 v3, v3, 0x4

    move-object/from16 v0, p7

    invoke-interface {v0, v2, v3, v9}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    goto/16 :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 296
    return-void
.end method
