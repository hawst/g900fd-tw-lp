.class public Lcom/google/android/apps/gmm/base/f/ad;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/base/l/a/n;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/f/ad;->a:Lcom/google/android/libraries/curvular/bk;

    .line 42
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/f/ad;->b:Lcom/google/android/libraries/curvular/bk;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method

.method public static varargs a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/cf;Ljava/lang/Boolean;Ljava/lang/Boolean;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 11

    .prologue
    .line 70
    const/4 v0, 0x1

    .line 73
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v0, 0x1

    .line 74
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    move-object v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    .line 70
    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/gmm/base/f/ad;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/apps/gmm/z/b/l;Lcom/google/android/apps/gmm/z/b/l;Ljava/lang/Boolean;Ljava/lang/Boolean;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/apps/gmm/z/b/l;Lcom/google/android/apps/gmm/z/b/l;Ljava/lang/Boolean;Ljava/lang/Boolean;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 12
    .param p6    # Lcom/google/android/apps/gmm/z/b/l;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/android/apps/gmm/z/b/l;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 100
    const/4 v1, 0x5

    new-array v2, v1, [Lcom/google/android/libraries/curvular/cu;

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 101
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const/4 v3, -0x2

    .line 102
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x2

    const/4 v3, 0x1

    .line 103
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x3

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/libraries/curvular/cu;

    .line 105
    invoke-static {v3}, Lcom/google/android/apps/gmm/base/k/ao;->b([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v3, 0x4

    const/4 v1, 0x5

    new-array v4, v1, [Lcom/google/android/libraries/curvular/cu;

    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 107
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x1

    const/4 v5, -0x2

    .line 108
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v5, 0x2

    const/4 v1, 0x5

    new-array v6, v1, [Lcom/google/android/libraries/curvular/cu;

    const/4 v1, 0x0

    .line 112
    sget-object v7, Lcom/google/android/apps/gmm/base/h/c;->b:Lcom/google/android/apps/gmm/base/h/c;

    sget-object v8, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v1

    const/4 v1, 0x1

    sget-object v7, Lcom/google/android/apps/gmm/base/f/ad;->a:Lcom/google/android/libraries/curvular/bk;

    .line 113
    sget-object v8, Lcom/google/android/libraries/curvular/g;->Z:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v1

    const/4 v1, 0x2

    .line 114
    sget-object v7, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v1

    const/4 v7, 0x3

    .line 115
    const/4 v1, 0x0

    if-eqz p4, :cond_0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v8

    new-instance v1, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v1, v8}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_0
    sget-object v8, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v6, v7

    const/4 v1, 0x4

    .line 116
    sget-object v7, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    move-object/from16 v0, p6

    invoke-static {v7, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v1

    .line 110
    invoke-static {p2, v6}, Lcom/google/android/apps/gmm/base/k/ao;->a(Ljava/lang/Boolean;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x3

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/google/android/libraries/curvular/cu;

    .line 118
    invoke-static {v5}, Lcom/google/android/apps/gmm/base/k/ao;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v5, 0x4

    const/4 v1, 0x4

    new-array v6, v1, [Lcom/google/android/libraries/curvular/cu;

    const/4 v1, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 120
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v1

    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 121
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v1

    const/4 v7, 0x2

    const/4 v1, 0x6

    new-array v8, v1, [Lcom/google/android/libraries/curvular/cu;

    const/4 v1, 0x0

    .line 125
    sget-object v9, Lcom/google/android/apps/gmm/base/h/c;->b:Lcom/google/android/apps/gmm/base/h/c;

    sget-object v10, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    aput-object v9, v8, v1

    const/4 v1, 0x1

    sget-object v9, Lcom/google/android/apps/gmm/base/f/ad;->b:Lcom/google/android/libraries/curvular/bk;

    .line 126
    sget-object v10, Lcom/google/android/libraries/curvular/g;->Z:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    aput-object v9, v8, v1

    const/4 v1, 0x2

    .line 127
    sget-object v9, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, p1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    aput-object v9, v8, v1

    const/4 v9, 0x3

    .line 128
    const/4 v1, 0x0

    if-eqz p5, :cond_1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v10

    new-instance v1, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v1, v10}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_1
    sget-object v10, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v8, v9

    const/4 v1, 0x4

    .line 129
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    sget-object v10, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    const/16 v10, 0x8

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    sget-object v11, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    move-object/from16 v0, p8

    invoke-static {v0, v9, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    aput-object v9, v8, v1

    const/4 v1, 0x5

    .line 130
    sget-object v9, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    move-object/from16 v0, p7

    invoke-static {v9, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    aput-object v9, v8, v1

    .line 123
    invoke-static {p3, v8}, Lcom/google/android/apps/gmm/base/k/ao;->a(Ljava/lang/Boolean;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v1

    aput-object v1, v6, v7

    const/4 v1, 0x3

    const/4 v7, 0x3

    new-array v7, v7, [Lcom/google/android/libraries/curvular/cu;

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    .line 133
    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    sget-object v10, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 134
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    sget-object v10, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    .line 135
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    sget-object v10, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    const/16 v10, 0x8

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    sget-object v11, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    move-object/from16 v0, p9

    invoke-static {v0, v9, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    aput-object v9, v7, v8

    .line 132
    new-instance v8, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v8, v7}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v7, "android.widget.ProgressBar"

    sget-object v9, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    invoke-virtual {v8, v7}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v7

    aput-object v7, v6, v1

    .line 119
    new-instance v1, Lcom/google/android/libraries/curvular/cs;

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/google/android/libraries/curvular/cu;

    const/4 v8, 0x0

    sget v9, Lcom/google/android/apps/gmm/h;->aK:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    sget-object v10, Lcom/google/android/libraries/curvular/g;->g:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-direct {v1, v7}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const/4 v7, 0x1

    invoke-virtual {v1, v6, v7}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;Z)V

    aput-object v1, v4, v5

    .line 106
    new-instance v1, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v1, v4}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v4, "android.widget.LinearLayout"

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v1

    aput-object v1, v2, v3

    .line 100
    new-instance v1, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v1, v2}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v2, "android.widget.LinearLayout"

    sget-object v3, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v1

    .line 135
    const/4 v2, 0x1

    move-object/from16 v0, p10

    invoke-virtual {v1, v0, v2}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;Z)V

    return-object v1
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 11

    .prologue
    .line 46
    .line 47
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/base/l/a/n;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/l/a/n;->a()Ljava/lang/CharSequence;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/curvular/ce;

    check-cast v1, Lcom/google/android/apps/gmm/base/l/a/n;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/l/a/n;->b()Ljava/lang/CharSequence;

    move-result-object v1

    .line 49
    iget-object v2, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/base/l/a/n;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/a/n;->c()Ljava/lang/Boolean;

    move-result-object v2

    .line 50
    iget-object v3, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v3}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/curvular/ce;

    check-cast v3, Lcom/google/android/apps/gmm/base/l/a/n;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/l/a/n;->d()Ljava/lang/Boolean;

    move-result-object v3

    .line 51
    iget-object v4, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/base/l/a/n;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/l/a/n;->e()Lcom/google/android/libraries/curvular/cf;

    move-result-object v4

    .line 52
    iget-object v5, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v5}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/libraries/curvular/ce;

    check-cast v5, Lcom/google/android/apps/gmm/base/l/a/n;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/l/a/n;->f()Lcom/google/android/libraries/curvular/cf;

    move-result-object v5

    .line 53
    iget-object v6, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v6}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/libraries/curvular/ce;

    check-cast v6, Lcom/google/android/apps/gmm/base/l/a/n;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/base/l/a/n;->i()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v6

    .line 54
    iget-object v7, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v7}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/libraries/curvular/ce;

    check-cast v7, Lcom/google/android/apps/gmm/base/l/a/n;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/base/l/a/n;->j()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v7

    .line 55
    iget-object v8, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v8}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/libraries/curvular/ce;

    check-cast v8, Lcom/google/android/apps/gmm/base/l/a/n;

    invoke-interface {v8}, Lcom/google/android/apps/gmm/base/l/a/n;->g()Ljava/lang/Boolean;

    move-result-object v8

    .line 56
    iget-object v9, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v9}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/libraries/curvular/ce;

    check-cast v9, Lcom/google/android/apps/gmm/base/l/a/n;

    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/l/a/n;->h()Ljava/lang/Boolean;

    move-result-object v9

    const/4 v10, 0x0

    new-array v10, v10, [Lcom/google/android/libraries/curvular/cu;

    .line 46
    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/gmm/base/f/ad;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/apps/gmm/z/b/l;Lcom/google/android/apps/gmm/z/b/l;Ljava/lang/Boolean;Ljava/lang/Boolean;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method
