.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;


# instance fields
.field a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

.field private final b:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field private final c:F

.field private final d:[F

.field private e:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

.field private f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

.field private g:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

.field private h:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

.field private final i:Z


# direct methods
.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->d:[F

    .line 189
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 374
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->i:Z

    .line 377
    const/high16 v0, 0x3f800000    # 1.0f

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 378
    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    int-to-float v2, v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v2

    mul-float/2addr v0, v2

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->c:F

    .line 385
    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x1000

    :goto_0
    or-int/lit16 v2, v0, 0x80

    .line 388
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->i:Z

    if-eqz v0, :cond_0

    .line 392
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    .line 394
    :cond_0
    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->f:Z

    if-eqz v0, :cond_2

    .line 399
    :cond_1
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    or-int/lit16 v0, v0, 0x1c2

    .line 405
    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 400
    invoke-static {v0, v1, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a(IZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 407
    :cond_2
    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->d()Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->g:Z

    if-eqz v0, :cond_3

    .line 415
    iget v0, p2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->a:I

    or-int/lit8 v1, v2, 0x12

    .line 418
    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 415
    invoke-static {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 421
    :cond_3
    iget v0, p2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->c:I

    if-lez v0, :cond_4

    .line 422
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v2, p2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->c:I

    invoke-direct {v0, v1, p3, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

    .line 424
    :cond_4
    return-void

    :cond_5
    move v0, v1

    .line 385
    goto :goto_0

    .line 399
    :cond_6
    const/16 v0, 0x600

    goto :goto_1
.end method

.method private static a(Lcom/google/android/apps/gmm/map/internal/vector/gl/i;Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/i;",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v10, 0x1

    .line 1018
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1019
    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->c:I

    .line 1020
    int-to-float v0, v4

    const/high16 v1, 0x41c00000    # 24.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x42800000    # 64.0f

    div-float v5, v0, v1

    .line 1022
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 1023
    invoke-virtual {v2, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    move v9, v10

    .line 1024
    :goto_0
    if-lez v4, :cond_2

    .line 1026
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v4, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 1027
    invoke-virtual {v12, v8}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 1028
    const/4 v0, 0x0

    iput v0, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;->a:F

    iput-boolean v10, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;->b:Z

    .line 1029
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move v7, v8

    .line 1031
    :goto_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->d:I

    if-ge v7, v0, :cond_1

    .line 1032
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->a:[I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->e:F

    int-to-float v6, v9

    div-float v6, v3, v6

    move-object v3, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a([ILandroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;IFF)V

    .line 1037
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->a:[I

    array-length v0, v0

    rem-int/lit8 v0, v0, 0x2

    if-ne v0, v10, :cond_0

    .line 1038
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->a:[I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->e:F

    int-to-float v6, v9

    div-float v6, v3, v6

    move-object v3, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a([ILandroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;IFF)V

    .line 1031
    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    .line 1043
    :cond_1
    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1044
    shr-int/lit8 v4, v4, 0x1

    .line 1045
    const/high16 v0, 0x40000000    # 2.0f

    div-float/2addr v5, v0

    .line 1046
    shl-int/lit8 v0, v9, 0x1

    move v9, v0

    .line 1047
    goto :goto_0

    .line 1048
    :cond_2
    return-object v11
.end method

.method private static a(Lcom/google/android/apps/gmm/map/internal/c/az;Lcom/google/android/apps/gmm/map/legacy/a/c/b/ao;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1135
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    .line 1136
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/c/bi;->b()F

    move-result v0

    iput v0, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ao;->d:F

    .line 1137
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->e:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->e:Ljava/lang/Boolean;

    move v2, v1

    :goto_0
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v0, v0

    if-ge v2, v0, :cond_4

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v6, v0, v2

    move v0, v1

    :goto_1
    iget-object v4, v6, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v4, :cond_1

    move v4, v1

    :goto_2
    if-ge v0, v4, :cond_3

    iget-object v4, v6, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v4, v4, v0

    iget v7, v4, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    if-eqz v7, :cond_2

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    const/4 v7, 0x0

    cmpl-float v4, v4, v7

    if-lez v4, :cond_2

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->e:Ljava/lang/Boolean;

    move v0, v3

    :goto_3
    iput-boolean v0, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ao;->b:Z

    .line 1143
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    const/4 v2, 0x2

    if-lt v0, v2, :cond_5

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ao;->b:Z

    if-nez v0, :cond_0

    .line 1144
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/c/az;->m()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_0
    move v0, v3

    :goto_4
    iput-boolean v0, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ao;->a:Z

    .line 1146
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/16 v2, 0x40

    and-int/2addr v0, v2

    if-eqz v0, :cond_6

    :goto_5
    iput-boolean v3, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ao;->c:Z

    .line 1147
    return-void

    .line 1137
    :cond_1
    iget-object v4, v6, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v4, v4

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_4
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_3

    :cond_5
    move v0, v1

    .line 1144
    goto :goto_4

    :cond_6
    move v3, v1

    .line 1146
    goto :goto_5
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/v/ad;[Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;Lcom/google/android/apps/gmm/v/ci;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;Ljava/util/List;Ljava/util/Set;Ljava/util/List;)V
    .locals 59
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/v/ad;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/az;",
            ">;>;",
            "Lcom/google/android/apps/gmm/v/bp;",
            "Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;",
            "Lcom/google/android/apps/gmm/v/ci;",
            "Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;",
            "Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/a;",
            "Lcom/google/android/apps/gmm/map/t/k;",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 225
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v40

    .line 228
    const/16 v4, 0x200

    new-instance v41, Ljava/util/ArrayList;

    move-object/from16 v0, v41

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 234
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    move/from16 v42, v0

    .line 235
    new-instance v36, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;

    invoke-direct/range {v36 .. v36}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;-><init>()V

    .line 236
    new-instance v43, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ao;

    invoke-direct/range {v43 .. v43}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ao;-><init>()V

    .line 238
    const v6, -0x48481b

    .line 239
    const/4 v7, 0x0

    .line 241
    const/4 v10, 0x0

    .line 242
    const/16 v26, 0x0

    .line 244
    const/16 v34, 0x0

    .line 249
    const/4 v4, 0x0

    move/from16 v37, v4

    :goto_0
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v37

    if-ge v0, v4, :cond_68

    .line 250
    move-object/from16 v0, p3

    move/from16 v1, v37

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v25, v4

    check-cast v25, Ljava/util/List;

    .line 251
    const/4 v4, 0x0

    move/from16 v38, v4

    :goto_1
    const/4 v4, 0x4

    move/from16 v0, v38

    if-ge v0, v4, :cond_67

    .line 252
    const/4 v4, 0x0

    .line 253
    :goto_2
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_66

    move/from16 v27, v4

    .line 254
    :goto_3
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v27

    if-ge v0, v4, :cond_72

    .line 255
    move-object/from16 v0, v25

    move/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/c/az;

    .line 256
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v8, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v8, v8

    if-nez v8, :cond_1

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v5

    :goto_4
    iget-object v8, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v8, :cond_2

    const/4 v5, 0x0

    :goto_5
    move/from16 v0, v38

    if-ge v0, v5, :cond_75

    .line 257
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/c/az;->l()Z

    move-result v5

    if-eqz v5, :cond_74

    .line 260
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v8, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v8, v8

    if-nez v8, :cond_3

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v5

    :goto_6
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v5, :cond_4

    .line 262
    const v5, -0x48481b

    .line 270
    :goto_7
    const/4 v6, 0x1

    move/from16 v58, v6

    move v6, v5

    move/from16 v5, v58

    .line 272
    :goto_8
    move-object/from16 v0, v43

    invoke-static {v4, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a(Lcom/google/android/apps/gmm/map/internal/c/az;Lcom/google/android/apps/gmm/map/legacy/a/c/b/ao;)V

    .line 274
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    iget-object v7, v7, Lcom/google/android/apps/gmm/v/ao;->a:Lcom/google/android/apps/gmm/v/ap;

    iget-boolean v7, v7, Lcom/google/android/apps/gmm/v/ap;->e:Z

    .line 273
    iget-object v13, v4, Lcom/google/android/apps/gmm/map/internal/c/az;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v7, v13, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v7, v7

    div-int/lit8 v7, v7, 0x3

    move-object/from16 v0, v43

    iget-boolean v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ao;->a:Z

    if-nez v7, :cond_8

    const/4 v7, 0x1

    :goto_9
    if-nez v7, :cond_25

    .line 276
    const/4 v4, 0x1

    move/from16 v28, v5

    move/from16 v29, v6

    .line 293
    :goto_a
    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->d()Z

    move-result v5

    if-nez v5, :cond_71

    move-object/from16 v0, v36

    iget-boolean v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->g:Z

    if-eqz v5, :cond_71

    .line 294
    const/4 v4, 0x1

    move/from16 v35, v4

    .line 297
    :goto_b
    const/4 v4, 0x3

    move/from16 v0, v38

    if-ne v0, v4, :cond_28

    .line 298
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v27

    if-lt v0, v4, :cond_28

    .line 299
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, v37

    if-ne v0, v4, :cond_28

    const/4 v4, 0x1

    move/from16 v39, v4

    .line 300
    :goto_c
    if-eqz v39, :cond_0

    .line 301
    const/16 v35, 0x1

    .line 303
    :cond_0
    if-eqz v35, :cond_70

    if-eqz v26, :cond_70

    .line 305
    new-instance v44, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;

    .line 306
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    move-object/from16 v0, v44

    move-object/from16 v1, p0

    move-object/from16 v2, v36

    move/from16 v3, v29

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;I)V

    .line 307
    const/4 v5, 0x0

    .line 309
    const/4 v4, 0x0

    move/from16 v33, v4

    move/from16 v12, v34

    :goto_d
    invoke-interface/range {v41 .. v41}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v33

    if-ge v0, v4, :cond_5e

    .line 310
    move-object/from16 v0, v41

    move/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v24, v4

    check-cast v24, Lcom/google/android/apps/gmm/map/internal/c/az;

    .line 311
    if-nez v24, :cond_29

    .line 313
    add-int/lit8 v12, v12, 0x1

    .line 314
    const/4 v4, 0x4

    if-lt v12, v4, :cond_6b

    .line 315
    const/4 v12, 0x0

    move-object v4, v5

    .line 309
    :goto_e
    add-int/lit8 v5, v33, 0x1

    move/from16 v33, v5

    move-object v5, v4

    goto :goto_d

    .line 256
    :cond_1
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v8, 0x0

    aget-object v5, v5, v8

    goto/16 :goto_4

    :cond_2
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v5, v5

    goto/16 :goto_5

    .line 260
    :cond_3
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v8, 0x0

    aget-object v5, v5, v8

    goto/16 :goto_6

    .line 263
    :cond_4
    if-nez v7, :cond_6

    .line 264
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v6, v6

    if-nez v6, :cond_5

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v5

    :goto_f
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    goto/16 :goto_7

    :cond_5
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    goto :goto_f

    .line 265
    :cond_6
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v8, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v8, v8

    if-nez v8, :cond_7

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v5

    :goto_10
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    if-eq v6, v5, :cond_73

    .line 267
    const/4 v4, 0x1

    move/from16 v28, v7

    move/from16 v29, v6

    .line 268
    goto/16 :goto_a

    .line 265
    :cond_7
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v8, 0x0

    aget-object v5, v5, v8

    goto :goto_10

    .line 273
    :cond_8
    invoke-static {v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/b/a/ab;)I

    move-result v14

    move-object/from16 v0, v36

    iget v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->a:I

    if-lez v7, :cond_9

    move-object/from16 v0, v36

    iget v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->a:I

    add-int/2addr v7, v14

    const/16 v8, 0x4000

    if-le v7, v8, :cond_9

    const/4 v7, 0x0

    goto/16 :goto_9

    :cond_9
    iget-object v7, v4, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/internal/c/bi;->c()I

    move-result v7

    if-nez v7, :cond_e

    const/4 v7, 0x0

    :goto_11
    move-object/from16 v0, v36

    iget v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->d:I

    if-nez v8, :cond_a

    move-object/from16 v0, v36

    iput v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->d:I

    :cond_a
    iget-object v7, v4, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v8, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v8, v8

    if-nez v8, :cond_11

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v7

    :goto_12
    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->d()Z

    move-result v8

    if-nez v8, :cond_22

    move-object/from16 v0, v36

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->e:[Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    if-nez v8, :cond_1e

    const/4 v8, 0x4

    new-array v8, v8, [Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    move-object/from16 v0, v36

    iput-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->e:[Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    const/4 v8, 0x0

    move v9, v8

    :goto_13
    const/4 v8, 0x4

    if-ge v9, v8, :cond_16

    iget-object v8, v7, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v8, :cond_12

    const/4 v8, 0x0

    :goto_14
    if-ge v9, v8, :cond_13

    iget-object v8, v7, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v8, v8, v9

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    :goto_15
    if-eqz v8, :cond_b

    array-length v11, v8

    if-eqz v11, :cond_b

    move-object/from16 v0, v36

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->e:[Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    new-instance v12, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    invoke-direct {v12, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;-><init>([I)V

    aput-object v12, v11, v9

    :cond_b
    if-eqz v8, :cond_14

    array-length v8, v8

    if-eqz v8, :cond_14

    const/4 v8, 0x1

    move v11, v8

    :goto_16
    const/4 v8, 0x1

    move/from16 v0, v38

    if-eq v9, v0, :cond_c

    const/4 v8, 0x0

    :cond_c
    if-eqz v8, :cond_15

    if-eqz v11, :cond_15

    const/4 v8, 0x1

    move-object/from16 v0, v36

    iput-boolean v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->g:Z

    :cond_d
    :goto_17
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_13

    :cond_e
    iget-object v7, v4, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v8, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v8, v8

    if-nez v8, :cond_f

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v7

    :goto_18
    iget-object v8, v7, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v8, :cond_10

    const/4 v7, 0x0

    goto :goto_11

    :cond_f
    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    goto :goto_18

    :cond_10
    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v7, v7

    goto :goto_11

    :cond_11
    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    goto :goto_12

    :cond_12
    iget-object v8, v7, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v8, v8

    goto :goto_14

    :cond_13
    const/4 v8, 0x0

    goto :goto_15

    :cond_14
    const/4 v8, 0x0

    move v11, v8

    goto :goto_16

    :cond_15
    if-eqz v8, :cond_d

    const/4 v8, 0x1

    move-object/from16 v0, v36

    iput-boolean v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->f:Z

    goto :goto_17

    :cond_16
    move-object/from16 v0, v36

    iget-boolean v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->g:Z

    if-eqz v7, :cond_22

    move-object/from16 v0, v36

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->e:[Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    const/4 v9, 0x1

    const/4 v8, 0x0

    array-length v0, v15

    move/from16 v16, v0

    const/4 v7, 0x0

    move v12, v7

    move v7, v8

    move v8, v9

    :goto_19
    move/from16 v0, v16

    if-ge v12, v0, :cond_1a

    aget-object v11, v15, v12

    if-eqz v11, :cond_19

    iget v9, v11, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->b:I

    invoke-static {v8, v9}, Lcom/google/android/apps/gmm/shared/c/s;->c(II)I

    move-result v9

    iget-object v0, v11, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->a:[I

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    const/4 v8, 0x0

    move v11, v8

    move v8, v7

    :goto_1a
    move/from16 v0, v18

    if-ge v11, v0, :cond_18

    aget v7, v17, v11

    if-nez v8, :cond_17

    :goto_1b
    add-int/lit8 v8, v11, 0x1

    move v11, v8

    move v8, v7

    goto :goto_1a

    :cond_17
    invoke-static {v8, v7}, Lcom/google/android/apps/gmm/shared/c/s;->d(II)I

    move-result v7

    goto :goto_1b

    :cond_18
    move v7, v8

    move v8, v9

    :cond_19
    add-int/lit8 v9, v12, 0x1

    move v12, v9

    goto :goto_19

    :cond_1a
    div-int v7, v8, v7

    const/16 v9, 0x80

    if-le v7, v9, :cond_1b

    const/16 v9, 0x80

    invoke-static {v7, v9}, Lcom/google/android/apps/gmm/shared/c/s;->a(II)I

    move-result v9

    div-int/2addr v8, v9

    div-int/2addr v7, v9

    :cond_1b
    const/16 v9, 0x40

    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-static {v8}, Lcom/google/android/apps/gmm/shared/c/s;->a(I)Z

    move-result v9

    if-nez v9, :cond_1c

    const/4 v9, 0x1

    invoke-static {v7, v9}, Lcom/google/android/apps/gmm/shared/c/s;->e(II)I

    move-result v7

    :cond_1c
    const/16 v9, 0x80

    invoke-static {v7, v9}, Ljava/lang/Math;->min(II)I

    move-result v9

    array-length v11, v15

    const/4 v7, 0x0

    :goto_1c
    if-ge v7, v11, :cond_22

    aget-object v12, v15, v7

    if-eqz v12, :cond_1d

    const/16 v16, 0x1

    iget v0, v12, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->b:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-static {v8, v0}, Lcom/google/android/apps/gmm/shared/c/s;->b(II)I

    move-result v17

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->max(II)I

    move-result v16

    move/from16 v0, v16

    iput v0, v12, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->d:I

    iput v8, v12, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->b:I

    const/16 v16, 0x0

    move/from16 v0, v16

    iput v0, v12, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->f:I

    iput v9, v12, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->c:I

    int-to-float v0, v9

    move/from16 v16, v0

    iget v0, v12, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->b:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    div-float v16, v16, v17

    move/from16 v0, v16

    iput v0, v12, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->e:F

    const/16 v16, 0x0

    move/from16 v0, v16

    iput v0, v12, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->f:I

    :cond_1d
    add-int/lit8 v7, v7, 0x1

    goto :goto_1c

    :cond_1e
    move-object/from16 v0, v36

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->e:[Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    array-length v8, v8

    add-int/lit8 v9, v38, 0x1

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v12

    move/from16 v11, v38

    :goto_1d
    if-ge v11, v12, :cond_22

    iget-object v8, v7, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v8, v8, v11

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    if-eqz v8, :cond_1f

    array-length v9, v8

    if-nez v9, :cond_1f

    const/4 v8, 0x0

    :cond_1f
    move-object/from16 v0, v36

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->e:[Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    aget-object v9, v9, v11

    if-nez v9, :cond_20

    const/4 v9, 0x0

    :goto_1e
    invoke-static {v8, v9}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v8

    if-nez v8, :cond_21

    const/4 v7, 0x0

    goto/16 :goto_9

    :cond_20
    iget-object v9, v9, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->a:[I

    goto :goto_1e

    :cond_21
    add-int/lit8 v8, v11, 0x1

    move v11, v8

    goto :goto_1d

    :cond_22
    move-object/from16 v0, v36

    iget v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->a:I

    add-int/2addr v7, v14

    move-object/from16 v0, v36

    iput v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->a:I

    move-object/from16 v0, v36

    iget v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->b:I

    invoke-static {v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->b(Lcom/google/android/apps/gmm/map/b/a/ab;)I

    move-result v8

    add-int/2addr v7, v8

    move-object/from16 v0, v36

    iput v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->b:I

    if-nez v38, :cond_23

    const/16 v7, 0xe

    move/from16 v0, v42

    if-le v0, v7, :cond_24

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/c/az;->l()Z

    move-result v7

    if-eqz v7, :cond_24

    const/4 v7, 0x1

    :goto_1f
    if-eqz v7, :cond_23

    move/from16 v0, v42

    int-to-float v7, v0

    invoke-static {v4, v7}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->a(Lcom/google/android/apps/gmm/map/internal/c/az;F)I

    move-result v7

    move-object/from16 v0, v36

    iget v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->c:I

    add-int/2addr v7, v8

    move-object/from16 v0, v36

    iput v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->c:I

    :cond_23
    const/4 v7, 0x1

    goto/16 :goto_9

    :cond_24
    const/4 v7, 0x0

    goto :goto_1f

    .line 280
    :cond_25
    if-nez v38, :cond_27

    .line 281
    iget-object v8, v4, Lcom/google/android/apps/gmm/map/internal/c/az;->j:[I

    array-length v9, v8

    const/4 v7, 0x0

    :goto_20
    if-ge v7, v9, :cond_27

    aget v11, v8, v7

    .line 282
    if-ltz v11, :cond_26

    move-object/from16 v0, p2

    array-length v12, v0

    if-ge v11, v12, :cond_26

    .line 283
    aget-object v11, p2, v11

    move-object/from16 v0, p13

    invoke-interface {v0, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 281
    :cond_26
    add-int/lit8 v7, v7, 0x1

    goto :goto_20

    .line 287
    :cond_27
    move-object/from16 v0, v41

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    const/4 v4, 0x1

    .line 254
    :goto_21
    add-int/lit8 v7, v27, 0x1

    move/from16 v27, v7

    move/from16 v26, v4

    move v7, v5

    goto/16 :goto_3

    .line 299
    :cond_28
    const/4 v4, 0x0

    move/from16 v39, v4

    goto/16 :goto_c

    .line 318
    :cond_29
    if-nez v5, :cond_6a

    move-object/from16 v32, v24

    .line 323
    :goto_22
    move-object/from16 v0, v24

    move-object/from16 v1, v43

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a(Lcom/google/android/apps/gmm/map/internal/c/az;Lcom/google/android/apps/gmm/map/legacy/a/c/b/ao;)V

    .line 324
    move-object/from16 v0, v43

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ao;->a:Z

    if-eqz v4, :cond_5d

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v24

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    const/4 v6, 0x4

    invoke-virtual {v13}, Lcom/google/android/apps/gmm/map/internal/c/bi;->c()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v14

    const/4 v6, 0x0

    :goto_23
    if-ge v6, v14, :cond_2a

    iget-object v7, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v8, 0x0

    iget-object v9, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v9, v9

    add-int/lit8 v9, v9, -0x1

    invoke-static {v6, v8, v9}, Lcom/google/android/apps/gmm/shared/c/s;->a(III)I

    move-result v8

    aget-object v8, v7, v8

    iget-object v7, v8, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v7, :cond_30

    const/4 v7, 0x0

    :goto_24
    if-lez v7, :cond_33

    const/4 v7, 0x0

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v7, v8, v7

    if-eqz v7, :cond_33

    iget v4, v7, Lcom/google/android/apps/gmm/map/internal/c/bd;->d:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_31

    const/4 v4, 0x1

    :goto_25
    iget v5, v7, Lcom/google/android/apps/gmm/map/internal/c/bd;->d:I

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_32

    const/4 v5, 0x1

    :cond_2a
    :goto_26
    if-nez v4, :cond_2c

    move-object/from16 v0, v24

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->g:I

    const/4 v6, 0x1

    if-eq v4, v6, :cond_2b

    move-object/from16 v0, v24

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->g:I

    const/4 v6, 0x2

    if-ne v4, v6, :cond_34

    :cond_2b
    const/4 v4, 0x1

    :goto_27
    if-eqz v4, :cond_35

    :cond_2c
    const/4 v9, 0x1

    :goto_28
    if-nez v5, :cond_2e

    move-object/from16 v0, v24

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->h:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_2d

    move-object/from16 v0, v24

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->h:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_36

    :cond_2d
    const/4 v4, 0x1

    :goto_29
    if-eqz v4, :cond_37

    :cond_2e
    const/4 v10, 0x1

    :goto_2a
    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v6, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    aget-byte v4, v6, v4

    int-to-float v4, v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v7

    const/4 v4, 0x0

    move/from16 v58, v4

    move v4, v5

    move/from16 v5, v58

    :goto_2b
    if-ge v5, v14, :cond_39

    iget-object v6, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    aget-byte v6, v6, v5

    int-to-float v6, v6

    invoke-static {v6}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v6

    div-float v8, v6, v7

    iget-object v6, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v11, 0x0

    iget-object v15, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v15, v15

    add-int/lit8 v15, v15, -0x1

    invoke-static {v5, v11, v15}, Lcom/google/android/apps/gmm/shared/c/s;->a(III)I

    move-result v11

    aget-object v11, v6, v11

    iget-object v6, v11, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v6, :cond_38

    const/4 v6, 0x0

    :goto_2c
    if-ge v12, v6, :cond_2f

    iget-object v6, v11, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v6, v6, v12

    iget v6, v6, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    mul-float/2addr v6, v8

    invoke-static {v4, v6}, Ljava/lang/Math;->max(FF)F

    move-result v4

    :cond_2f
    add-int/lit8 v5, v5, 0x1

    goto :goto_2b

    :cond_30
    iget-object v7, v8, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v7, v7

    goto/16 :goto_24

    :cond_31
    const/4 v4, 0x0

    goto :goto_25

    :cond_32
    const/4 v5, 0x0

    goto :goto_26

    :cond_33
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_23

    :cond_34
    const/4 v4, 0x0

    goto :goto_27

    :cond_35
    const/4 v9, 0x0

    goto :goto_28

    :cond_36
    const/4 v4, 0x0

    goto :goto_29

    :cond_37
    const/4 v10, 0x0

    goto :goto_2a

    :cond_38
    iget-object v6, v11, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v6, v6

    goto :goto_2c

    :cond_39
    move-object/from16 v0, v40

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v24

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, v44

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v7, v7, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    int-to-float v7, v7

    invoke-static {v7}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v7

    mul-float/2addr v7, v4

    const v8, 0x3f2aaaab

    mul-float/2addr v7, v8

    move-object/from16 v0, v44

    iget v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->c:F

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v31

    move-object/from16 v0, v24

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v8, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v8, v8

    if-nez v8, :cond_3b

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v7

    :goto_2d
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/internal/c/be;->c()F

    move-result v7

    move-object/from16 v0, v44

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v8, v8, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    int-to-float v8, v8

    invoke-static {v8}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v8

    mul-float/2addr v7, v8

    const v8, 0x3f2aaaab

    mul-float/2addr v7, v8

    move-object/from16 v0, v44

    iget v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->c:F

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-object/from16 v0, v44

    iget-boolean v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->i:Z

    if-eqz v7, :cond_3c

    const v7, 0x402aaaab

    mul-float/2addr v4, v7

    move/from16 v30, v4

    :goto_2e
    move-object/from16 v0, v43

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ao;->b:Z

    if-eqz v4, :cond_4d

    const/4 v7, 0x0

    const/4 v4, 0x0

    move/from16 v58, v4

    move v4, v7

    move/from16 v7, v58

    :goto_2f
    if-ge v7, v14, :cond_3f

    iget-object v8, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v11, 0x0

    iget-object v15, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v15, v15

    add-int/lit8 v15, v15, -0x1

    invoke-static {v7, v11, v15}, Lcom/google/android/apps/gmm/shared/c/s;->a(III)I

    move-result v11

    aget-object v8, v8, v11

    if-nez v7, :cond_3e

    iget-object v4, v8, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v4, :cond_3d

    const/4 v4, 0x0

    :cond_3a
    :goto_30
    add-int/lit8 v7, v7, 0x1

    goto :goto_2f

    :cond_3b
    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    goto :goto_2d

    :cond_3c
    move/from16 v30, v31

    goto :goto_2e

    :cond_3d
    iget-object v4, v8, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v4, v4

    goto :goto_30

    :cond_3e
    iget-object v11, v8, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v11, :cond_42

    const/4 v8, 0x0

    :goto_31
    if-eq v4, v8, :cond_3a

    const-string v7, "GLRoadGroup"

    const-string v8, "Inconsistent stroke count %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v13, v11, v15

    invoke-static {v7, v8, v11}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3f
    if-lt v12, v4, :cond_40

    const-string v4, "GLRoadGroup"

    const-string v7, "Invalid stroke number %d in %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v13, v8, v11

    invoke-static {v4, v7, v8}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_40
    move-object/from16 v0, v24

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/16 v7, 0x100

    and-int/2addr v4, v7

    if-eqz v4, :cond_43

    const/4 v4, 0x1

    :goto_32
    if-eqz v4, :cond_44

    move-object/from16 v0, p7

    iget v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->i:I

    move-object/from16 v0, p7

    invoke-virtual {v0, v13}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->a(Lcom/google/android/apps/gmm/map/internal/c/bi;)I

    move-result v11

    move-object/from16 v0, p7

    iget-boolean v7, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->j:Z

    if-eqz v7, :cond_41

    if-ne v11, v4, :cond_41

    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->k:Ljava/util/Set;

    invoke-interface {v4, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_41
    :goto_33
    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    if-eqz v4, :cond_4b

    move-object/from16 v0, v44

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->i:Z

    if-eqz v4, :cond_4e

    const/4 v4, 0x0

    move v7, v4

    :goto_34
    if-ge v7, v14, :cond_4a

    iget-object v4, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v8, 0x0

    iget-object v15, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v15, v15

    add-int/lit8 v15, v15, -0x1

    invoke-static {v7, v8, v15}, Lcom/google/android/apps/gmm/shared/c/s;->a(III)I

    move-result v8

    aget-object v8, v4, v8

    iget-object v4, v8, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v4, :cond_45

    const/4 v4, 0x0

    :goto_35
    if-ge v12, v4, :cond_46

    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->d:[F

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v8, v8, v12

    iget v8, v8, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    const/4 v15, 0x0

    iget-object v0, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    move-object/from16 v18, v0

    aget-byte v15, v18, v15

    iget-object v0, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    move-object/from16 v18, v0

    aget-byte v18, v18, v7

    sub-int v15, v15, v18

    int-to-double v0, v15

    move-wide/from16 v18, v0

    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v15, v0

    mul-float/2addr v8, v15

    aput v8, v4, v7

    :goto_36
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_34

    :cond_42
    iget-object v8, v8, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v8, v8

    goto/16 :goto_31

    :cond_43
    const/4 v4, 0x0

    goto :goto_32

    :cond_44
    move-object/from16 v0, p7

    invoke-virtual {v0, v13}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->a(Lcom/google/android/apps/gmm/map/internal/c/bi;)I

    move-result v11

    goto :goto_33

    :cond_45
    iget-object v4, v8, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v4, v4

    goto :goto_35

    :cond_46
    iget-object v4, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v4, v4

    if-nez v4, :cond_47

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v4

    :goto_37
    iget-object v8, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v8, :cond_48

    const/4 v8, 0x0

    :goto_38
    if-ge v12, v8, :cond_49

    const-string v8, "GLRoadGroup"

    const-string v15, "Style doesn\'t have enough strokes: stroke index %d, style index %d, style entry %s"

    const/16 v16, 0x3

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x2

    aput-object v13, v16, v17

    move-object/from16 v0, v16

    invoke-static {v8, v15, v0}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object/from16 v0, v44

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->d:[F

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v4, v4, v12

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    aput v4, v8, v7

    goto :goto_36

    :cond_47
    iget-object v4, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v8, 0x0

    aget-object v4, v4, v8

    goto :goto_37

    :cond_48
    iget-object v8, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v8, v8

    goto :goto_38

    :cond_49
    const-string v4, "GLRoadGroup"

    const-string v8, "Even first style doesn\'t have enough strokes: stroke index %d, style index %d, style entry %s"

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x2

    aput-object v13, v15, v16

    invoke-static {v4, v8, v15}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->d:[F

    const/4 v8, 0x0

    aput v8, v4, v7

    goto/16 :goto_36

    :cond_4a
    const/4 v7, 0x0

    move-object/from16 v0, v44

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    sget-object v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->d()Z

    move-result v14

    move-object/from16 v0, v44

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->d:[F

    move-object/from16 v4, p11

    invoke-virtual/range {v4 .. v15}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/internal/vector/gl/k;ZZIILcom/google/android/apps/gmm/map/internal/vector/gl/c;Z[F)I

    :goto_39
    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    :cond_4b
    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->d()Z

    move-result v4

    if-nez v4, :cond_4d

    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    if-eqz v4, :cond_4d

    const/4 v7, 0x1

    const/4 v4, 0x0

    :goto_3a
    move-object/from16 v0, v36

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->e:[Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    array-length v8, v8

    if-ge v4, v8, :cond_6f

    move-object/from16 v0, v36

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->e:[Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    aget-object v8, v8, v4

    if-eqz v8, :cond_4f

    move-object/from16 v0, v36

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->e:[Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    aget-object v4, v7, v4

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->b:I

    :goto_3b
    const/high16 v7, 0x45000000    # 2048.0f

    move-object/from16 v0, v40

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v40

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v9, v9, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v8, v9

    mul-int/2addr v4, v8

    int-to-float v4, v4

    div-float v16, v7, v4

    move-object/from16 v0, v44

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-object/from16 v17, v0

    move-object/from16 v0, v44

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-object/from16 v18, v0

    move-object/from16 v0, v44

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-object/from16 v19, v0

    move-object/from16 v0, p11

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->m:Lcom/google/android/apps/gmm/map/internal/vector/gl/d;

    const/4 v7, 0x0

    iput v7, v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    move-object/from16 v0, p11

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->m:Lcom/google/android/apps/gmm/map/internal/vector/gl/d;

    div-int/lit16 v7, v11, 0x100

    int-to-byte v7, v7

    rem-int/lit16 v8, v11, 0x100

    int-to-byte v8, v8

    int-to-byte v9, v12

    const/4 v10, 0x0

    invoke-virtual {v4, v7, v8, v9, v10}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(BBBB)V

    move-object/from16 v0, p11

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->m:Lcom/google/android/apps/gmm/map/internal/vector/gl/d;

    move-object/from16 v20, v0

    const/high16 v4, 0x3f800000    # 1.0f

    cmpg-float v4, v30, v4

    if-gez v4, :cond_50

    :cond_4c
    :goto_3c
    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    :cond_4d
    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

    if-eqz v4, :cond_5c

    if-nez v12, :cond_5c

    move-object/from16 v0, v44

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    move-object/from16 v0, v24

    invoke-static {v4, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->a(ILcom/google/android/apps/gmm/map/internal/c/az;)Z

    move-result v4

    if-eqz v4, :cond_5c

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    int-to-float v4, v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v4

    const/high16 v5, 0x40c00000    # 6.0f

    mul-float/2addr v5, v4

    const/high16 v7, 0x42c00000    # 96.0f

    mul-float v11, v7, v4

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    int-to-float v4, v4

    move-object/from16 v0, v24

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->a(Lcom/google/android/apps/gmm/map/internal/c/az;F)I

    move-result v7

    if-eqz v7, :cond_5b

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/gmm/map/internal/c/az;->f()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v8

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->i:Lcom/google/android/apps/gmm/map/l/b;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/l/b;->a()V

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->i:Lcom/google/android/apps/gmm/map/l/b;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/b/a/ab;->b()I

    move-result v9

    invoke-virtual {v4, v9}, Lcom/google/android/apps/gmm/map/l/b;->a(I)V

    const/4 v4, 0x0

    :goto_3d
    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/b/a/ab;->b()I

    move-result v9

    if-ge v4, v9, :cond_56

    invoke-virtual {v8, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v9

    iget-object v13, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->i:Lcom/google/android/apps/gmm/map/l/b;

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/map/b/a/y;->g()I

    move-result v14

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/b/a/y;->g()I

    move-result v15

    sub-int/2addr v14, v15

    int-to-float v14, v14

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/map/b/a/y;->h()I

    move-result v9

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/b/a/y;->h()I

    move-result v15

    sub-int/2addr v9, v15

    int-to-float v9, v9

    invoke-virtual {v13, v14, v9}, Lcom/google/android/apps/gmm/map/l/b;->a(FF)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_3d

    :cond_4e
    move-object/from16 v0, v44

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-object/from16 v17, v0

    sget-object v22, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->d()Z

    move-result v23

    move-object/from16 v13, p11

    move-object v14, v5

    move/from16 v15, v30

    move-object/from16 v16, v6

    move/from16 v18, v9

    move/from16 v19, v10

    move/from16 v20, v11

    move/from16 v21, v12

    invoke-virtual/range {v13 .. v23}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/b/a/ab;FLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/internal/vector/gl/k;ZZIILcom/google/android/apps/gmm/map/internal/vector/gl/c;Z)I

    goto/16 :goto_39

    :cond_4f
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_3a

    :cond_50
    iget-object v4, v5, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v4, v4

    div-int/lit8 v21, v4, 0x3

    const/4 v4, 0x2

    move/from16 v0, v21

    if-ne v0, v4, :cond_51

    invoke-interface/range {v17 .. v17}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->b()I

    move-result v4

    invoke-virtual/range {p11 .. p11}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->b()V

    move-object/from16 v0, p11

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p11

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p11

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p11

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p11

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p11

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v14, 0x0

    invoke-virtual {v5, v14, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v14, 0x1

    invoke-virtual {v5, v14, v6, v8}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget v5, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v14, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v5, v14

    iput v5, v9, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v5, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v14, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v5, v14

    iput v5, v9, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v0, v30

    invoke-static {v9, v0, v10}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    iget v5, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v14, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v5, v14

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v5, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v14, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v5, v14

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget v5, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v14, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v5, v14

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v5, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v14, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v5, v14

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget v5, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    div-int/lit8 v5, v5, 0x2

    iget v14, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    div-int/lit8 v14, v14, 0x2

    add-int/2addr v5, v14

    iput v5, v11, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v5, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    div-int/lit8 v5, v5, 0x2

    iget v7, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v5, v7

    iput v5, v11, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v5, v11, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v7, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v5, v7

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v5, v11, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v7, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v5, v7

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget v5, v11, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v7, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v5, v7

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v5, v11, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v7, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v5, v7

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget v5, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v7, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v5, v7

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v5, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v7, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v5, v7

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget v5, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v7, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v5, v7

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v5, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v7, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v5, v7

    iput v5, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/map/b/a/y;->i()F

    move-result v5

    mul-float v5, v5, v16

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v7, v8}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v7, v8}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    const/4 v7, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    div-float v8, v5, v8

    move-object/from16 v0, v18

    invoke-interface {v0, v7, v8}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v8, 0x40000000    # 2.0f

    div-float v8, v5, v8

    move-object/from16 v0, v18

    invoke-interface {v0, v7, v8}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v7, v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    const/high16 v7, 0x3f800000    # 1.0f

    move-object/from16 v0, v18

    invoke-interface {v0, v7, v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    add-int/lit8 v5, v4, 0x1

    add-int/lit8 v7, v4, 0x2

    add-int/lit8 v8, v4, 0x3

    move-object/from16 v0, v19

    invoke-interface {v0, v4, v5, v7, v8}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(IIII)V

    add-int/lit8 v5, v4, 0x2

    add-int/lit8 v7, v4, 0x3

    add-int/lit8 v8, v4, 0x4

    add-int/lit8 v4, v4, 0x5

    move-object/from16 v0, v19

    invoke-interface {v0, v5, v7, v8, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(IIII)V

    goto/16 :goto_3c

    :cond_51
    const/4 v4, 0x2

    move/from16 v0, v21

    if-lt v0, v4, :cond_4c

    invoke-virtual/range {p11 .. p11}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->b()V

    move-object/from16 v0, p11

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v22, v0

    move-object/from16 v0, p11

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v23, v0

    move-object/from16 v0, p11

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v45, v0

    move-object/from16 v0, p11

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v46, v0

    move-object/from16 v0, p11

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v47, v0

    move-object/from16 v0, p11

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v48, v0

    move-object/from16 v0, p11

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v49, v0

    move-object/from16 v0, p11

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v50, v0

    move-object/from16 v0, p11

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-interface/range {v17 .. v17}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->b()I

    move-result v11

    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v5, v4, v6, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v4, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v5, v4, v6, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v23

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v22

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v4, v7

    move-object/from16 v0, v46

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v23

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v22

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v7

    move-object/from16 v0, v46

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v46

    move/from16 v1, v30

    move-object/from16 v2, v48

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v7, 0x0

    move-object/from16 v0, v22

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v48

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v4, v9

    iput v4, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v22

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v48

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v4, v9

    iput v4, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v4, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4, v9}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    move-object/from16 v0, v22

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v48

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v4, v9

    iput v4, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v22

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v48

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v9

    iput v4, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4, v9}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    const/4 v13, 0x2

    mul-float v51, v30, v30

    const/4 v4, 0x1

    move v14, v4

    move v4, v7

    :goto_3e
    add-int/lit8 v7, v21, -0x1

    if-ge v14, v7, :cond_55

    invoke-virtual/range {v46 .. v46}, Lcom/google/android/apps/gmm/map/b/a/y;->i()F

    move-result v7

    mul-float v7, v7, v16

    add-float v15, v4, v7

    add-int/lit8 v4, v14, 0x1

    move-object/from16 v0, v45

    invoke-virtual {v5, v4, v6, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v23

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v4, v7

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v23

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v7

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v47

    move/from16 v1, v30

    move-object/from16 v2, v49

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v46

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-long v0, v4

    move-wide/from16 v52, v0

    move-object/from16 v0, v47

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-long v0, v4

    move-wide/from16 v54, v0

    mul-long v52, v52, v54

    move-object/from16 v0, v46

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-long v0, v4

    move-wide/from16 v54, v0

    move-object/from16 v0, v47

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-long v0, v4

    move-wide/from16 v56, v0

    mul-long v54, v54, v56

    sub-long v52, v52, v54

    const-wide/16 v54, 0x0

    cmp-long v4, v52, v54

    if-lez v4, :cond_52

    const/4 v4, 0x1

    :goto_3f
    const/4 v10, 0x1

    move-object/from16 v0, v48

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v49

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v7, v9

    move-object/from16 v0, v50

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v48

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v49

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v7, v9

    move-object/from16 v0, v50

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static/range {v49 .. v50}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v7

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v9, v7, v9

    if-lez v9, :cond_6e

    invoke-static/range {v46 .. v47}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v9

    const/16 v52, 0x0

    cmpl-float v9, v9, v52

    if-ltz v9, :cond_6e

    div-float v7, v51, v7

    move-object/from16 v0, v50

    move-object/from16 v1, v50

    invoke-static {v0, v7, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p11

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v23

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v50

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v52, v0

    add-int v7, v7, v52

    iput v7, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v23

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v50

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v52, v0

    add-int v7, v7, v52

    iput v7, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v23

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v50

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v52, v0

    sub-int v7, v7, v52

    iput v7, v9, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v23

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v50

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v52, v0

    sub-int v7, v7, v52

    iput v7, v9, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-eqz v4, :cond_53

    move-object v7, v8

    :goto_40
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-static {v0, v1, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v52

    const/high16 v53, 0x3f000000    # 0.5f

    cmpg-float v52, v52, v53

    if-gez v52, :cond_6e

    move-object/from16 v0, v23

    move-object/from16 v1, v45

    invoke-static {v0, v1, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v7

    const/high16 v52, 0x3f000000    # 0.5f

    cmpg-float v7, v7, v52

    if-gez v7, :cond_6e

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v7, v15}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/high16 v7, 0x3f800000    # 1.0f

    move-object/from16 v0, v18

    invoke-interface {v0, v7, v15}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    add-int/lit8 v10, v13, 0x2

    add-int/lit8 v7, v11, 0x1

    add-int/lit8 v9, v11, 0x2

    add-int/lit8 v13, v11, 0x3

    move-object/from16 v0, v19

    invoke-interface {v0, v11, v7, v9, v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(IIII)V

    add-int/lit8 v9, v11, 0x2

    const/4 v7, 0x0

    move/from16 v58, v7

    move v7, v9

    move v9, v10

    move/from16 v10, v58

    :goto_41
    if-eqz v10, :cond_6d

    move-object/from16 v0, v23

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v48

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v10, v11

    iput v10, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v23

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v48

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v10, v11

    iput v10, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v10, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v10, v15}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    move-object/from16 v0, v23

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v48

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v10, v11

    iput v10, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v23

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v48

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v10, v11

    iput v10, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/high16 v10, 0x3f800000    # 1.0f

    move-object/from16 v0, v18

    invoke-interface {v0, v10, v15}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/high16 v10, 0x3f000000    # 0.5f

    move-object/from16 v0, v18

    invoke-interface {v0, v10, v15}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    move-object/from16 v0, v23

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v49

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v10, v11

    iput v10, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v23

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v49

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v10, v11

    iput v10, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v10, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v10, v15}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    move-object/from16 v0, v23

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v49

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v10, v11

    iput v10, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v23

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v49

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v10, v11

    iput v10, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/high16 v10, 0x3f800000    # 1.0f

    move-object/from16 v0, v18

    invoke-interface {v0, v10, v15}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    add-int/lit8 v9, v9, 0x5

    if-eqz v4, :cond_54

    add-int/lit8 v4, v7, 0x2

    add-int/lit8 v10, v7, 0x1

    add-int/lit8 v11, v7, 0x4

    move-object/from16 v0, v19

    invoke-interface {v0, v4, v10, v11}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    :goto_42
    add-int/lit8 v4, v7, 0x3

    add-int/lit8 v10, v7, 0x4

    add-int/lit8 v11, v7, 0x5

    add-int/lit8 v13, v7, 0x6

    move-object/from16 v0, v19

    invoke-interface {v0, v4, v10, v11, v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(IIII)V

    add-int/lit8 v4, v7, 0x5

    move v7, v9

    :goto_43
    move-object/from16 v0, v49

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v48

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v49

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v48

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v49

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v48

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v47

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v46

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v47

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v46

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v47

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, v46

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    invoke-virtual/range {v22 .. v23}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v23

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    add-int/lit8 v9, v14, 0x1

    move v14, v9

    move v11, v4

    move v13, v7

    move v4, v15

    goto/16 :goto_3e

    :cond_52
    const/4 v4, 0x0

    goto/16 :goto_3f

    :cond_53
    move-object v7, v9

    goto/16 :goto_40

    :cond_54
    add-int/lit8 v4, v7, 0x2

    add-int/lit8 v10, v7, 0x3

    move-object/from16 v0, v19

    invoke-interface {v0, v7, v4, v10}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    goto :goto_42

    :cond_55
    invoke-virtual/range {v46 .. v46}, Lcom/google/android/apps/gmm/map/b/a/y;->i()F

    move-result v5

    mul-float v5, v5, v16

    add-float/2addr v4, v5

    move-object/from16 v0, v45

    move-object/from16 v1, v49

    invoke-static {v0, v1, v8}, Lcom/google/android/apps/gmm/map/b/a/z;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v5, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v5, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    move-object/from16 v0, v45

    move-object/from16 v1, v49

    invoke-static {v0, v1, v8}, Lcom/google/android/apps/gmm/map/b/a/z;->d(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V

    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v0, v18

    invoke-interface {v0, v5, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;->a(FF)V

    add-int/lit8 v4, v13, 0x2

    add-int/lit8 v4, v11, 0x1

    add-int/lit8 v5, v11, 0x2

    add-int/lit8 v7, v11, 0x3

    move-object/from16 v0, v19

    invoke-interface {v0, v11, v4, v5, v7}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(IIII)V

    goto/16 :goto_3c

    :cond_56
    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/b/a/ab;->b()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b()I

    move-result v13

    const/high16 v6, 0x3f800000    # 1.0f

    div-float v5, v6, v5

    if-lez v4, :cond_5b

    div-int/lit8 v14, v7, 0x4

    add-int v4, v13, v7

    const/16 v6, 0x7fff

    if-le v4, v6, :cond_57

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x32

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " required, but we can only store 32767"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_57
    div-int/lit8 v4, v7, 0x2

    div-int/lit8 v15, v4, 0x2

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c()I

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->d()V

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b()I

    move-result v6

    add-int/2addr v6, v7

    invoke-virtual {v4, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(I)V

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->i()I

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->j()V

    mul-float v4, v31, v5

    const/high16 v5, 0x3f000000    # 0.5f

    const/high16 v6, 0x3e800000    # 0.25f

    mul-float/2addr v6, v4

    sub-float/2addr v5, v6

    const/high16 v6, 0x3f000000    # 0.5f

    const/high16 v7, 0x3e800000    # 0.25f

    mul-float/2addr v4, v7

    add-float/2addr v4, v6

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->c:[[F

    const/4 v7, 0x0

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aput v5, v6, v7

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->c:[[F

    const/4 v7, 0x1

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aput v4, v6, v7

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->c:[[F

    const/4 v7, 0x2

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aput v4, v6, v7

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->c:[[F

    const/4 v6, 0x3

    aget-object v4, v4, v6

    const/4 v6, 0x0

    aput v5, v4, v6

    const/high16 v4, 0x3f800000    # 1.0f

    add-int/lit8 v5, v14, 0x1

    int-to-float v5, v5

    div-float v16, v4, v5

    invoke-static {v14}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->a(I)I

    move-result v17

    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    move v9, v4

    :goto_44
    move/from16 v0, v17

    if-ge v9, v0, :cond_5a

    add-int/lit8 v4, v17, -0x1

    if-ge v9, v4, :cond_6c

    shl-int/lit8 v4, v5, 0x1

    :goto_45
    move v7, v8

    :goto_46
    if-ge v7, v14, :cond_59

    add-int/lit8 v6, v7, 0x1

    const/high16 v18, -0x41000000    # -0.5f

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    move/from16 v0, v16

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v10, v0, v6, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->a(FIFLcom/google/android/apps/gmm/map/b/a/ay;)V

    add-int/lit8 v6, v7, 0x1

    const v18, -0x41333333    # -0.4f

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->k:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    move/from16 v0, v16

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v10, v0, v6, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->a(FIFLcom/google/android/apps/gmm/map/b/a/ay;)V

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->l:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->k:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/b/a/ay;->a()F

    move-result v18

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/b/a/ay;->a()F

    move-result v19

    sub-float v18, v18, v19

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->k:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/b/a/ay;->b()F

    move-result v19

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/gmm/map/b/a/ay;->b()F

    move-result v20

    sub-float v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(FF)Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->l:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->m:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v6, v0}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->m:Lcom/google/android/apps/gmm/map/b/a/ay;

    const/high16 v18, 0x3e800000    # 0.25f

    mul-float v18, v18, v31

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(F)Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->l:Lcom/google/android/apps/gmm/map/b/a/ay;

    const/high16 v18, 0x3e800000    # 0.25f

    mul-float v18, v18, v11

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(F)Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/b/a/ay;->a()F

    move-result v18

    const/high16 v19, 0x3f000000    # 0.5f

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->l:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/gmm/map/b/a/ay;->a()F

    move-result v20

    mul-float v19, v19, v20

    sub-float v18, v18, v19

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/b/a/ay;->b()F

    move-result v19

    const/high16 v20, 0x3f000000    # 0.5f

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->l:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/gmm/map/b/a/ay;->b()F

    move-result v21

    mul-float v20, v20, v21

    sub-float v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(FF)Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->h:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/16 v18, 0x0

    aget-object v6, v6, v18

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/b/a/ay;->a()F

    move-result v18

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->m:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/b/a/ay;->a()F

    move-result v19

    add-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/b/a/ay;->b()F

    move-result v19

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->m:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/gmm/map/b/a/ay;->b()F

    move-result v20

    add-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->d(II)V

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->h:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/16 v18, 0x1

    aget-object v6, v6, v18

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/b/a/ay;->a()F

    move-result v18

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->m:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/b/a/ay;->a()F

    move-result v19

    sub-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/b/a/ay;->b()F

    move-result v19

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->m:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/gmm/map/b/a/ay;->b()F

    move-result v20

    sub-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->d(II)V

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/b/a/ay;->a()F

    move-result v18

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->l:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/b/a/ay;->a()F

    move-result v19

    add-float v18, v18, v19

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/b/a/ay;->b()F

    move-result v19

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->l:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/gmm/map/b/a/ay;->b()F

    move-result v20

    add-float v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(FF)Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->h:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/16 v18, 0x2

    aget-object v6, v6, v18

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/b/a/ay;->a()F

    move-result v18

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->m:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/b/a/ay;->a()F

    move-result v19

    sub-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/b/a/ay;->b()F

    move-result v19

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->m:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/gmm/map/b/a/ay;->b()F

    move-result v20

    sub-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->d(II)V

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->h:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/16 v18, 0x3

    aget-object v6, v6, v18

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/b/a/ay;->a()F

    move-result v18

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->m:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/b/a/ay;->a()F

    move-result v19

    add-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/b/a/ay;->b()F

    move-result v19

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->m:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/gmm/map/b/a/ay;->b()F

    move-result v20

    add-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->d(II)V

    add-int/lit8 v6, v17, -0x1

    sub-int/2addr v6, v9

    int-to-float v0, v6

    move/from16 v18, v0

    const/4 v6, 0x0

    :goto_47
    const/16 v19, 0x4

    move/from16 v0, v19

    if-ge v6, v0, :cond_58

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-object/from16 v19, v0

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->h:[Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v20, v0

    aget-object v20, v20, v6

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;F)V

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-object/from16 v19, v0

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->c:[[F

    move-object/from16 v20, v0

    aget-object v20, v20, v6

    const/16 v21, 0x0

    aget v20, v20, v21

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->c:[[F

    move-object/from16 v21, v0

    aget-object v21, v21, v6

    const/16 v22, 0x1

    aget v21, v21, v22

    invoke-virtual/range {v19 .. v21}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(FF)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_47

    :cond_58
    add-int v6, v7, v4

    move v7, v6

    goto/16 :goto_46

    :cond_59
    add-int/2addr v8, v5

    add-int/lit8 v5, v9, 0x1

    move v9, v5

    move v5, v4

    goto/16 :goto_44

    :cond_5a
    const/4 v4, 0x0

    :goto_48
    if-ge v4, v15, :cond_5b

    shl-int/lit8 v5, v4, 0x2

    add-int/2addr v5, v13

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    add-int/lit8 v7, v5, 0x1

    add-int/lit8 v8, v5, 0x2

    invoke-virtual {v6, v5, v7, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(III)V

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    add-int/lit8 v7, v5, 0x2

    add-int/lit8 v8, v5, 0x3

    invoke-virtual {v6, v5, v7, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(III)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_48

    :cond_5b
    iget-object v4, v10, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    :cond_5c
    move-object/from16 v0, v24

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/16 v5, 0x80

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/internal/c/ai;->a(II)Z

    :cond_5d
    move-object/from16 v4, v32

    goto/16 :goto_e

    .line 335
    :cond_5e
    new-instance v6, Lcom/google/android/apps/gmm/map/t/as;

    invoke-direct {v6, v5}, Lcom/google/android/apps/gmm/map/t/as;-><init>(Lcom/google/android/apps/gmm/map/internal/c/m;)V

    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    if-eqz v4, :cond_60

    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b()I

    move-result v4

    if-lez v4, :cond_5f

    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v5, 0x5

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v4

    move-object/from16 v0, v44

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    new-instance v5, Lcom/google/android/apps/gmm/map/t/ar;

    move/from16 v0, v34

    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/map/t/as;->a(I)Lcom/google/android/apps/gmm/map/t/as;

    move-result-object v7

    const/4 v8, 0x1

    move-object/from16 v0, p10

    move-object/from16 v1, p0

    invoke-direct {v5, v0, v1, v7, v8}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0xe

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Road Stroke 0 "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/apps/gmm/map/t/ar;->a(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    invoke-virtual/range {p9 .. p9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->a()Lcom/google/android/apps/gmm/map/internal/vector/gl/r;

    move-result-object v4

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a(I)Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p4

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/v/ad;->a()Lcom/google/android/apps/gmm/v/bk;

    move-result-object v4

    const/16 v7, 0x207

    invoke-virtual {v4, v7}, Lcom/google/android/apps/gmm/v/bk;->a(I)Lcom/google/android/apps/gmm/v/bm;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/v/ad;->a()Lcom/google/android/apps/gmm/v/bk;

    move-result-object v4

    const/16 v7, 0x302

    const/16 v8, 0x303

    invoke-virtual {v4, v7, v8}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p6

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    move-object/from16 v0, p12

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x0

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/map/t/ar;->b(I)V

    :cond_5f
    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    const/4 v4, 0x0

    move-object/from16 v0, v44

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    :cond_60
    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->d()Z

    move-result v4

    if-nez v4, :cond_63

    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    if-eqz v4, :cond_63

    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b()I

    move-result v4

    if-lez v4, :cond_62

    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v5, 0x4

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v4

    move-object/from16 v0, v44

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    new-instance v5, Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;-><init>()V

    move-object/from16 v0, p9

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/g;

    new-instance v8, Lcom/google/android/apps/gmm/map/t/ar;

    move/from16 v0, v34

    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/map/t/as;->a(I)Lcom/google/android/apps/gmm/map/t/as;

    move-result-object v9

    const/4 v10, 0x1

    move-object/from16 v0, p10

    move-object/from16 v1, p0

    invoke-direct {v8, v0, v1, v9, v10}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x15

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "Dashed Road Stroke 0 "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/gmm/map/t/ar;->a(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    move-object/from16 v0, v36

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->e:[Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    aget-object v4, v4, v38

    iget-object v9, v7, Lcom/google/android/apps/gmm/map/internal/vector/gl/g;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v9, v4}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/v/v;

    if-nez v4, :cond_61

    move-object/from16 v0, v36

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->e:[Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    aget-object v4, v4, v38

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/i;Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;)Ljava/util/List;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/gmm/v/ar;

    const/4 v9, 0x1

    invoke-direct {v5, v4, v9}, Lcom/google/android/apps/gmm/v/ar;-><init>(Ljava/util/List;Z)V

    new-instance v4, Lcom/google/android/apps/gmm/v/v;

    const/4 v9, 0x0

    invoke-direct {v4, v5, v9}, Lcom/google/android/apps/gmm/v/v;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    const v5, 0x812f

    const/16 v9, 0x2901

    invoke-virtual {v4, v5, v9}, Lcom/google/android/apps/gmm/v/v;->a(II)V

    const/16 v5, 0x2703

    const/16 v9, 0x2601

    invoke-virtual {v4, v5, v9}, Lcom/google/android/apps/gmm/v/v;->b(II)V

    move-object/from16 v0, v36

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;->e:[Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    aget-object v5, v5, v38

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/vector/gl/g;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v7, v5, v4}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/v/v;->a(Lcom/google/android/apps/gmm/v/ad;)V

    :cond_61
    invoke-virtual {v8, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p5

    invoke-virtual {v8, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/v/ad;->a()Lcom/google/android/apps/gmm/v/bk;

    move-result-object v4

    const/16 v5, 0x207

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/v/bk;->a(I)Lcom/google/android/apps/gmm/v/bm;

    move-result-object v4

    invoke-virtual {v8, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/v/ad;->a()Lcom/google/android/apps/gmm/v/bk;

    move-result-object v4

    const/16 v5, 0x302

    const/16 v7, 0x303

    invoke-virtual {v4, v5, v7}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v4

    invoke-virtual {v8, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p6

    invoke-virtual {v8, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p0

    invoke-virtual {v8, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    move-object/from16 v0, p12

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x0

    invoke-virtual {v8, v4}, Lcom/google/android/apps/gmm/map/t/ar;->b(I)V

    :cond_62
    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    const/4 v4, 0x0

    move-object/from16 v0, v44

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    :cond_63
    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

    if-eqz v4, :cond_65

    move-object/from16 v0, v44

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b()I

    move-result v5

    if-eqz v5, :cond_64

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v7, 0x4

    const/4 v8, 0x1

    invoke-virtual {v5, v7, v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    new-instance v5, Lcom/google/android/apps/gmm/map/t/ar;

    sget-object v7, Lcom/google/android/apps/gmm/map/t/j;->b:Lcom/google/android/apps/gmm/map/t/j;

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    const/4 v9, 0x4

    invoke-virtual {v6, v9}, Lcom/google/android/apps/gmm/map/t/as;->a(I)Lcom/google/android/apps/gmm/map/t/as;

    move-result-object v6

    const/4 v9, 0x0

    invoke-direct {v5, v7, v8, v6, v9}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0xa

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Road Arrow"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/t/ar;->a(Ljava/lang/String;)V

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    invoke-virtual/range {p9 .. p9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->a()Lcom/google/android/apps/gmm/map/internal/vector/gl/r;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a(I)Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    move-result-object v6

    iput-object v6, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->g:Lcom/google/android/apps/gmm/v/ci;

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->g:Lcom/google/android/apps/gmm/v/ci;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    new-instance v6, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/g;

    iget-object v7, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->b:[F

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/g;-><init>([FI)V

    iput-object v6, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->d:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/g;

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->d:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/g;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/v/ad;->a()Lcom/google/android/apps/gmm/v/bk;

    move-result-object v6

    const/16 v7, 0x302

    const/16 v8, 0x303

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p6

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    move-object/from16 v0, p12

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/t/ar;->b(I)V

    :cond_64
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 348
    :cond_65
    move-object/from16 v0, p14

    move-object/from16 v1, v44

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 351
    if-nez v39, :cond_69

    .line 352
    new-instance v5, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/am;-><init>()V

    .line 353
    invoke-interface/range {v41 .. v41}, Ljava/util/List;->clear()V

    .line 354
    const/16 v28, 0x0

    .line 355
    const v29, -0x48481b

    .line 356
    const/4 v4, 0x0

    .line 357
    const/16 v26, 0x0

    :goto_49
    move/from16 v34, v12

    move v10, v4

    move/from16 v7, v28

    move/from16 v6, v29

    move-object/from16 v36, v5

    move/from16 v4, v27

    .line 360
    goto/16 :goto_2

    .line 363
    :cond_66
    const/4 v4, 0x0

    move-object/from16 v0, v41

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    add-int/lit8 v4, v38, 0x1

    move/from16 v38, v4

    goto/16 :goto_1

    .line 249
    :cond_67
    add-int/lit8 v4, v37, 0x1

    move/from16 v37, v4

    goto/16 :goto_0

    .line 366
    :cond_68
    return-void

    :cond_69
    move/from16 v4, v35

    move-object/from16 v5, v36

    goto :goto_49

    :cond_6a
    move-object/from16 v32, v5

    goto/16 :goto_22

    :cond_6b
    move-object v4, v5

    goto/16 :goto_e

    :cond_6c
    move v4, v5

    goto/16 :goto_45

    :cond_6d
    move v4, v7

    move v7, v9

    goto/16 :goto_43

    :cond_6e
    move v7, v11

    move v9, v13

    goto/16 :goto_41

    :cond_6f
    move v4, v7

    goto/16 :goto_3b

    :cond_70
    move/from16 v12, v34

    move/from16 v4, v35

    move-object/from16 v5, v36

    goto :goto_49

    :cond_71
    move/from16 v35, v4

    goto/16 :goto_b

    :cond_72
    move v4, v10

    move/from16 v28, v7

    move/from16 v29, v6

    goto/16 :goto_a

    :cond_73
    move v5, v6

    goto/16 :goto_7

    :cond_74
    move v5, v7

    goto/16 :goto_8

    :cond_75
    move/from16 v4, v26

    move v5, v7

    goto/16 :goto_21
.end method

.method private static a([ILandroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;IFF)V
    .locals 8

    .prologue
    .line 1068
    if-eqz p0, :cond_0

    array-length v0, p0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1070
    :cond_1
    int-to-float v0, p4

    sub-float/2addr v0, p5

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 1071
    int-to-float v1, p4

    sub-float v2, v1, v0

    .line 1076
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 1077
    add-int/lit8 v0, p4, -0x1

    int-to-float v0, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 1078
    cmpl-float v0, v1, v3

    if-lez v0, :cond_3

    .line 1093
    :cond_2
    return-void

    .line 1083
    :cond_3
    array-length v7, p0

    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v7, :cond_2

    aget v0, p0, v6

    .line 1084
    int-to-float v0, v0

    mul-float/2addr v0, p6

    .line 1085
    iget v2, p3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;->a:F

    add-float v4, v2, v0

    .line 1086
    iget-boolean v0, p3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;->b:Z

    if-eqz v0, :cond_4

    .line 1088
    iget v2, p3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;->a:F

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1090
    :cond_4
    iput v4, p3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;->a:F

    .line 1091
    iget-boolean v0, p3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;->b:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/an;->b:Z

    .line 1083
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 1091
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1103
    .line 1104
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

    if-eqz v1, :cond_1

    .line 1105
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-eqz v2, :cond_0

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    add-int/lit8 v0, v0, 0x0

    :cond_0
    add-int/lit8 v0, v0, 0x0

    .line 1107
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-eqz v1, :cond_2

    .line 1108
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    add-int/2addr v0, v1

    .line 1110
    :cond_2
    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->d()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-eqz v1, :cond_3

    .line 1111
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    add-int/2addr v0, v1

    .line 1114
    :cond_3
    return v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1119
    const/16 v0, 0x1d0

    .line 1121
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

    if-eqz v1, :cond_0

    .line 1122
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/al;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;

    const/16 v0, 0x220

    .line 1125
    :cond_0
    return v0
.end method
