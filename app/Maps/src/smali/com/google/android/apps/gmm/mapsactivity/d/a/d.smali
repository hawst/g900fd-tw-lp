.class public Lcom/google/android/apps/gmm/mapsactivity/d/a/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/mapsactivity/a/c;
.implements Lcom/google/android/apps/gmm/mapsactivity/a/i;
.implements Lcom/google/android/apps/gmm/shared/net/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/mapsactivity/a/c;",
        "Lcom/google/android/apps/gmm/mapsactivity/a/i;",
        "Lcom/google/android/apps/gmm/shared/net/c",
        "<",
        "Lcom/google/r/b/a/lh;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/mapsactivity/a/a;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/mapsactivity/a/d;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/android/apps/gmm/mapsactivity/a/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/gmm/mapsactivity/d/a/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/mapsactivity/d/a/d;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/mapsactivity/a/j;Lcom/google/android/apps/gmm/mapsactivity/a/b;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/d;->b:Ljava/util/Map;

    .line 45
    iput-object p1, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/d;->c:Lcom/google/android/apps/gmm/mapsactivity/a/j;

    .line 46
    invoke-interface {p2, p0}, Lcom/google/android/apps/gmm/mapsactivity/a/b;->a(Lcom/google/android/apps/gmm/mapsactivity/a/c;)V

    .line 47
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/mapsactivity/a/a;Lcom/google/r/b/a/lo;)V
    .locals 2

    .prologue
    .line 71
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/mapsactivity/d/a/d;->a(Lcom/google/android/apps/gmm/mapsactivity/a/a;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v1

    .line 72
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mapsactivity/a/d;

    invoke-interface {v0, p2}, Lcom/google/android/apps/gmm/mapsactivity/a/d;->a(Lcom/google/r/b/a/lo;)Lcom/google/android/apps/gmm/mapsactivity/a/d;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/x/o;->b(Ljava/io/Serializable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    monitor-exit p0

    return-void

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/mapsactivity/a/a;Lcom/google/r/b/a/lw;)V
    .locals 2

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/d;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    .line 66
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/mapsactivity/a/d;

    invoke-interface {v1, p2}, Lcom/google/android/apps/gmm/mapsactivity/a/d;->a(Lcom/google/r/b/a/lw;)Lcom/google/android/apps/gmm/mapsactivity/a/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/x/o;->b(Ljava/io/Serializable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    monitor-exit p0

    return-void

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/mapsactivity/a/a;)Lcom/google/android/apps/gmm/x/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/mapsactivity/a/a;",
            ")",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/mapsactivity/a/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/d;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    sget-object v0, Lcom/google/android/apps/gmm/mapsactivity/d/a/d;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x18

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Adding placeholder day "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    invoke-static {p1}, Lcom/google/android/apps/gmm/mapsactivity/d/a/c;->a(Lcom/google/android/apps/gmm/mapsactivity/a/a;)Lcom/google/android/apps/gmm/mapsactivity/d/a/c;

    move-result-object v0

    .line 58
    iget-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/d;->b:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/d;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final synthetic a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 4

    .prologue
    .line 27
    check-cast p1, Lcom/google/r/b/a/lh;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/d;->b()Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p1, Lcom/google/r/b/a/lh;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/lt;->d()Lcom/google/r/b/a/lt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/lt;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/google/r/b/a/lt;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Lcom/google/r/b/a/lt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/lw;->d()Lcom/google/r/b/a/lw;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/lw;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/lw;

    iget-object v1, v0, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v1

    :goto_2
    invoke-static {v1}, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->a(Lcom/google/r/b/a/ay;)Lcom/google/android/apps/gmm/mapsactivity/a/a;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/mapsactivity/d/a/d;->a(Lcom/google/android/apps/gmm/mapsactivity/a/a;Lcom/google/r/b/a/lw;)V

    goto :goto_1

    :cond_1
    iget-object v1, v0, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    goto :goto_2

    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/lh;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ll;->g()Lcom/google/r/b/a/ll;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ll;

    invoke-virtual {v0}, Lcom/google/r/b/a/ll;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/lo;

    iget-object v1, v0, Lcom/google/r/b/a/lo;->b:Lcom/google/maps/g/co;

    if-nez v1, :cond_3

    invoke-static {}, Lcom/google/maps/g/co;->d()Lcom/google/maps/g/co;

    move-result-object v1

    :goto_4
    invoke-static {v1}, Lcom/google/android/apps/gmm/mapsactivity/d/a/a;->a(Lcom/google/maps/g/co;)Lcom/google/android/apps/gmm/mapsactivity/a/a;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/mapsactivity/d/a/d;->a(Lcom/google/android/apps/gmm/mapsactivity/a/a;Lcom/google/r/b/a/lo;)V

    goto :goto_3

    :cond_3
    iget-object v1, v0, Lcom/google/r/b/a/lo;->b:Lcom/google/maps/g/co;

    goto :goto_4

    :cond_4
    return-void
.end method
