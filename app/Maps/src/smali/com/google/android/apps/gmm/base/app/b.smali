.class public Lcom/google/android/apps/gmm/base/app/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/a;
.implements Lcom/google/android/apps/gmm/map/c/a/a;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private final A:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/shared/net/a;",
            ">;"
        }
    .end annotation
.end field

.field private final B:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/prefetchcache/api/f;",
            ">;"
        }
    .end annotation
.end field

.field private final C:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/place/riddler/a;",
            ">;"
        }
    .end annotation
.end field

.field b:Landroid/content/Context;

.field c:Lcom/google/android/apps/gmm/map/c/a;

.field final d:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/navigation/a/d;",
            ">;"
        }
    .end annotation
.end field

.field final e:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/x/a;",
            ">;"
        }
    .end annotation
.end field

.field final f:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/z/a/b;",
            ">;"
        }
    .end annotation
.end field

.field final g:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/prefetch/a/f;",
            ">;"
        }
    .end annotation
.end field

.field final h:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/q/a;",
            ">;"
        }
    .end annotation
.end field

.field final i:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/e/a;",
            ">;"
        }
    .end annotation
.end field

.field private j:Landroid/app/Application;

.field private k:I

.field private final l:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/myplaces/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/p/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/p/b/g;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/login/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/shared/c/c/c;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/map/h/c;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/replay/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/suggest/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/hotels/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/aa/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private final v:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/car/a/g;",
            ">;"
        }
    .end annotation
.end field

.field private final w:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/ab/h;",
            ">;"
        }
    .end annotation
.end field

.field private final x:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/navigation/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private final y:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/search/av;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123
    const-class v0, Lcom/google/android/apps/gmm/base/app/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/app/b;->a:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/app/Application;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    new-instance v0, Lcom/google/android/apps/gmm/base/app/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/c;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 145
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->d:Lcom/google/b/a/bw;

    .line 153
    new-instance v0, Lcom/google/android/apps/gmm/base/app/n;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/n;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_2

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->e:Lcom/google/b/a/bw;

    .line 163
    new-instance v0, Lcom/google/android/apps/gmm/base/app/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/v;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 164
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_4

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->l:Lcom/google/b/a/bw;

    .line 178
    new-instance v0, Lcom/google/android/apps/gmm/base/app/w;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/w;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 179
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_6

    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->m:Lcom/google/b/a/bw;

    .line 188
    new-instance v0, Lcom/google/android/apps/gmm/base/app/x;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/x;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 189
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_8

    :goto_4
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->n:Lcom/google/b/a/bw;

    .line 198
    new-instance v0, Lcom/google/android/apps/gmm/base/app/y;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/y;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 199
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_a

    :goto_5
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->o:Lcom/google/b/a/bw;

    .line 211
    new-instance v0, Lcom/google/android/apps/gmm/base/app/z;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/z;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 212
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_c

    :goto_6
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->p:Lcom/google/b/a/bw;

    .line 223
    new-instance v0, Lcom/google/android/apps/gmm/base/app/aa;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/aa;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 224
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_e

    :goto_7
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->q:Lcom/google/b/a/bw;

    .line 232
    new-instance v0, Lcom/google/android/apps/gmm/base/app/ab;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/ab;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 233
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_10

    :goto_8
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->r:Lcom/google/b/a/bw;

    .line 241
    new-instance v0, Lcom/google/android/apps/gmm/base/app/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/d;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 242
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_12

    :goto_9
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->f:Lcom/google/b/a/bw;

    .line 250
    new-instance v0, Lcom/google/android/apps/gmm/base/app/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/e;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 251
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_14

    :goto_a
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->s:Lcom/google/b/a/bw;

    .line 259
    new-instance v0, Lcom/google/android/apps/gmm/base/app/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/f;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 260
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_16

    :goto_b
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->t:Lcom/google/b/a/bw;

    .line 268
    new-instance v0, Lcom/google/android/apps/gmm/base/app/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/g;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 269
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_18

    :goto_c
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->g:Lcom/google/b/a/bw;

    .line 278
    new-instance v0, Lcom/google/android/apps/gmm/base/app/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/h;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 279
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_1a

    :goto_d
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->u:Lcom/google/b/a/bw;

    .line 290
    new-instance v0, Lcom/google/android/apps/gmm/base/app/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/i;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 291
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_1c

    :goto_e
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->h:Lcom/google/b/a/bw;

    .line 299
    new-instance v0, Lcom/google/android/apps/gmm/base/app/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/j;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 300
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_1e

    :goto_f
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->i:Lcom/google/b/a/bw;

    .line 308
    new-instance v0, Lcom/google/android/apps/gmm/base/app/k;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/k;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 309
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_20

    :goto_10
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->v:Lcom/google/b/a/bw;

    .line 322
    new-instance v0, Lcom/google/android/apps/gmm/base/app/l;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/l;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 323
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_22

    :goto_11
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->w:Lcom/google/b/a/bw;

    .line 331
    new-instance v0, Lcom/google/android/apps/gmm/base/app/m;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/m;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 332
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_24

    :goto_12
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->x:Lcom/google/b/a/bw;

    .line 340
    new-instance v0, Lcom/google/android/apps/gmm/base/app/o;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/o;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 341
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_26

    :goto_13
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->y:Lcom/google/b/a/bw;

    .line 363
    new-instance v0, Lcom/google/android/apps/gmm/base/app/p;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/p;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 364
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_28

    :goto_14
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->z:Lcom/google/b/a/bw;

    .line 376
    new-instance v0, Lcom/google/android/apps/gmm/base/app/q;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/q;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_2a

    :goto_15
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->A:Lcom/google/b/a/bw;

    .line 390
    new-instance v0, Lcom/google/android/apps/gmm/base/app/r;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/r;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 391
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_2c

    :goto_16
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->B:Lcom/google/b/a/bw;

    .line 406
    new-instance v0, Lcom/google/android/apps/gmm/base/app/s;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/s;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    .line 407
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_2e

    :goto_17
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->C:Lcom/google/b/a/bw;

    .line 442
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/app/b;->j:Landroid/app/Application;

    .line 443
    invoke-virtual {p1}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->b:Landroid/content/Context;

    .line 444
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->b:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/c/a;)V

    .line 453
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/stitch/incompat/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 519
    :goto_18
    return-void

    .line 145
    :cond_0
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 153
    :cond_2
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_1

    .line 164
    :cond_4
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_2

    .line 179
    :cond_6
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_3

    .line 189
    :cond_8
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_4

    .line 199
    :cond_a
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_5

    .line 212
    :cond_c
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_6

    .line 224
    :cond_e
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_7

    .line 233
    :cond_10
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_11
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_8

    .line 242
    :cond_12
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_13
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_9

    .line 251
    :cond_14
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_15

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_15
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_a

    .line 260
    :cond_16
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_17

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_17
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_b

    .line 269
    :cond_18
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_19

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_19
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_c

    .line 279
    :cond_1a
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_1b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1b
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_d

    .line 291
    :cond_1c
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_1d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1d
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_e

    .line 300
    :cond_1e
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_1f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1f
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_f

    .line 309
    :cond_20
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_21

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_21
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_10

    .line 323
    :cond_22
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_23

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_23
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_11

    .line 332
    :cond_24
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_25

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_25
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_12

    .line 341
    :cond_26
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_27

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_27
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_13

    .line 364
    :cond_28
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_29

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_29
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_14

    .line 376
    :cond_2a
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_2b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2b
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_15

    .line 391
    :cond_2c
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_2d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2d
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_16

    .line 407
    :cond_2e
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_2f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2f
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_17

    .line 456
    :cond_30
    new-instance v0, Lcom/google/android/libraries/stitch/incompat/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/app/b;->b:Landroid/content/Context;

    .line 457
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/base/app/t;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/base/app/t;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/stitch/incompat/b;-><init>(Landroid/content/Context;Ljava/lang/Thread$UncaughtExceptionHandler;Lcom/google/android/libraries/stitch/incompat/c;)V

    .line 456
    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 467
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/jni/crashreporter/NativeCrashHandler;->a(Landroid/content/Context;)V

    .line 478
    new-instance v0, Lcom/google/android/apps/gmm/util/replay/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/util/replay/i;-><init>()V

    .line 483
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/app/b;->b:Landroid/content/Context;

    const-string v2, "GMM"

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/app/b;->b:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v3

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/n/g;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/util/replay/a;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/app/u;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/base/app/u;-><init>(Lcom/google/android/apps/gmm/base/app/b;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0xa

    .line 498
    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 489
    invoke-interface {v0, v1, v2, v4, v5}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 513
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/navigation/j/a/b;

    invoke-direct {v1, v6, v6}, Lcom/google/android/apps/gmm/navigation/j/a/b;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/f;Lcom/google/android/apps/gmm/navigation/g/b/d;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto/16 :goto_18
.end method


# virtual methods
.method public final A()Lcom/google/android/apps/gmm/map/c/c;
    .locals 1

    .prologue
    .line 734
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->A()Lcom/google/android/apps/gmm/map/c/c;

    move-result-object v0

    return-object v0
.end method

.method public final A_()Lcom/google/android/apps/gmm/map/internal/b/f;
    .locals 1

    .prologue
    .line 714
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->A_()Lcom/google/android/apps/gmm/map/internal/b/f;

    move-result-object v0

    return-object v0
.end method

.method public final B()Lcom/google/android/apps/gmm/map/internal/c/o;
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v0

    return-object v0
.end method

.method public final B_()Landroid/accounts/Account;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 832
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->o:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/login/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public final C_()Lcom/google/android/apps/gmm/map/internal/c/cw;
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->C_()Lcom/google/android/apps/gmm/map/internal/c/cw;

    move-result-object v0

    return-object v0
.end method

.method public final D_()Lcom/google/android/apps/gmm/map/i/a/a;
    .locals 1

    .prologue
    .line 729
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->D_()Lcom/google/android/apps/gmm/map/i/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final E_()Lcom/google/android/apps/gmm/shared/c/a/j;
    .locals 1

    .prologue
    .line 609
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    return-object v0
.end method

.method public final a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 751
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->b:Landroid/content/Context;

    return-object v0
.end method

.method public final a(Landroid/app/Activity;)Lcom/google/android/apps/gmm/base/j/b;
    .locals 1

    .prologue
    .line 837
    new-instance v0, Lcom/google/android/apps/gmm/base/app/ae;

    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/base/app/ae;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;)Lcom/google/android/libraries/curvular/bd;
    .locals 1

    .prologue
    .line 985
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/app/ac;->a(Landroid/content/Context;)Lcom/google/android/libraries/curvular/bd;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/libraries/curvular/bd;)V
    .locals 1

    .prologue
    .line 980
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/base/app/ac;->a(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/shared/c/a/j;)V

    .line 981
    return-void
.end method

.method public final b()Lcom/google/android/apps/gmm/shared/net/r;
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/shared/b/a;
    .locals 1

    .prologue
    .line 724
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/util/b/g;
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/gmm/p/b/a;
    .locals 1

    .prologue
    .line 805
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->m:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/b/a;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/shared/c/f;
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/internal/d/bd;
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/gmm/navigation/a/d;
    .locals 1

    .prologue
    .line 816
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->d:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/d;

    return-object v0
.end method

.method public final h_()Lcom/google/android/apps/gmm/login/a/a;
    .locals 1

    .prologue
    .line 778
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->o:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/login/a/a;

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/shared/c/c/c;
    .locals 1

    .prologue
    .line 783
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->p:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/c/c/c;

    return-object v0
.end method

.method public final i_()Lcom/google/android/apps/gmm/x/a;
    .locals 1

    .prologue
    .line 788
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->e:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/a;

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/gmm/map/h/c;
    .locals 1

    .prologue
    .line 821
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->q:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/h/c;

    return-object v0
.end method

.method public final j_()Lcom/google/android/apps/gmm/myplaces/a/a;
    .locals 1

    .prologue
    .line 799
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->l:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/myplaces/a/a;

    return-object v0
.end method

.method public final k()Lcom/google/android/apps/gmm/replay/a/a;
    .locals 1

    .prologue
    .line 826
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->r:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/replay/a/a;

    return-object v0
.end method

.method public final k_()Lcom/google/android/apps/gmm/p/b/g;
    .locals 1

    .prologue
    .line 811
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->n:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/b/g;

    return-object v0
.end method

.method public final l()Lcom/google/android/apps/gmm/map/indoor/a/a;
    .locals 1

    .prologue
    .line 679
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final l_()Z
    .locals 1

    .prologue
    .line 916
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/h/d;->a()V

    .line 917
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->b:Landroid/net/NetworkInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    goto :goto_0
.end method

.method public final m()Landroid/app/Application;
    .locals 1

    .prologue
    .line 756
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->j:Landroid/app/Application;

    return-object v0
.end method

.method public final m_()Lcom/google/android/apps/gmm/z/a/b;
    .locals 1

    .prologue
    .line 773
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->f:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/z/a/b;

    return-object v0
.end method

.method public final n()Lcom/google/android/apps/gmm/map/q/b;
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->n()Lcom/google/android/apps/gmm/map/q/b;

    move-result-object v0

    return-object v0
.end method

.method public final n_()Lcom/google/android/apps/gmm/suggest/a/b;
    .locals 1

    .prologue
    .line 842
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->s:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/suggest/a/b;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final o_()Lcom/google/android/apps/gmm/hotels/a/b;
    .locals 1

    .prologue
    .line 847
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->t:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/hotels/a/b;

    return-object v0
.end method

.method public final p()Lcom/google/android/apps/gmm/prefetchcache/api/f;
    .locals 1

    .prologue
    .line 857
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->B:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetchcache/api/f;

    return-object v0
.end method

.method public final p_()Lcom/google/android/apps/gmm/q/a;
    .locals 1

    .prologue
    .line 868
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->h:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/q/a;

    return-object v0
.end method

.method public final q()Lcom/google/android/apps/gmm/map/e/a;
    .locals 1

    .prologue
    .line 674
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->q()Lcom/google/android/apps/gmm/map/e/a;

    move-result-object v0

    return-object v0
.end method

.method public final q_()Lcom/google/android/apps/gmm/aa/a/d;
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->u:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/aa/a/d;

    return-object v0
.end method

.method public final r()Lcom/google/android/apps/gmm/map/c/a/a;
    .locals 0

    .prologue
    .line 922
    return-object p0
.end method

.method public final r_()Lcom/google/android/apps/gmm/ab/h;
    .locals 1

    .prologue
    .line 889
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->w:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/ab/h;

    return-object v0
.end method

.method public final s()Lcom/google/android/apps/gmm/car/a/g;
    .locals 1

    .prologue
    .line 884
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->v:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/a/g;

    return-object v0
.end method

.method public final s_()Lcom/google/android/apps/gmm/navigation/b/c;
    .locals 1

    .prologue
    .line 894
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->x:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/b/c;

    return-object v0
.end method

.method public final t()Lcom/google/android/apps/gmm/shared/net/a/b;
    .locals 1

    .prologue
    .line 699
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    return-object v0
.end method

.method public final t_()Lcom/google/android/apps/gmm/map/util/a/b;
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v0

    return-object v0
.end method

.method public final u()Lcom/google/android/apps/gmm/map/h/d;
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    return-object v0
.end method

.method public final u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;
    .locals 1

    .prologue
    .line 634
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v0

    return-object v0
.end method

.method public final v()Lcom/google/android/apps/gmm/search/av;
    .locals 1

    .prologue
    .line 903
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->y:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/av;

    return-object v0
.end method

.method public final v_()Lcom/google/android/apps/gmm/map/legacy/a/a/a;
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->v_()Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final w()Lcom/google/android/apps/gmm/place/riddler/a;
    .locals 1

    .prologue
    .line 908
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->C:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/riddler/a;

    return-object v0
.end method

.method public final w_()Lcom/google/android/apps/gmm/m/d;
    .locals 1

    .prologue
    .line 649
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v0

    return-object v0
.end method

.method public final x()V
    .locals 3

    .prologue
    .line 940
    iget v0, p0, Lcom/google/android/apps/gmm/base/app/b;->k:I

    if-nez v0, :cond_1

    .line 941
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/util/a/b;->c:Lcom/google/android/libraries/memorymonitor/d;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/util/a/b;->c:Lcom/google/android/libraries/memorymonitor/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/a/b;->d:Lcom/google/android/libraries/memorymonitor/f;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/memorymonitor/d;->a(Lcom/google/android/libraries/memorymonitor/f;)V

    .line 942
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->m:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/b/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->d()V

    .line 943
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->f:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/z/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/z/a/b;->d()V

    .line 944
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->t:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/hotels/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->d()V

    .line 945
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->i:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/e/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/e/a;->c:Lcom/google/android/apps/gmm/e/b;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/e/b;->a:Z

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/e/a;->c:Lcom/google/android/apps/gmm/e/b;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/e/b;->a:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/e/a;->a()V

    .line 947
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/base/app/b;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/app/b;->k:I

    .line 948
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/e/a;

    iget v2, p0, Lcom/google/android/apps/gmm/base/app/b;->k:I

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/base/e/a;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 949
    return-void
.end method

.method public final x_()Lcom/google/android/apps/gmm/util/replay/a;
    .locals 1

    .prologue
    .line 654
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->x_()Lcom/google/android/apps/gmm/util/replay/a;

    move-result-object v0

    return-object v0
.end method

.method public final y()V
    .locals 3

    .prologue
    .line 953
    iget v0, p0, Lcom/google/android/apps/gmm/base/app/b;->k:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/app/b;->k:I

    .line 954
    iget v0, p0, Lcom/google/android/apps/gmm/base/app/b;->k:I

    if-nez v0, :cond_1

    .line 955
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->i:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/e/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/e/a;->c:Lcom/google/android/apps/gmm/e/b;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/e/b;->a:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/e/a;->c:Lcom/google/android/apps/gmm/e/b;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/e/b;->a:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/e/a;->a()V

    .line 956
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->t:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/hotels/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->e()V

    .line 957
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->f:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/z/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/z/a/b;->c()V

    .line 958
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->m:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/b/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->c()V

    .line 959
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/util/a/b;->c:Lcom/google/android/libraries/memorymonitor/d;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/util/a/b;->c:Lcom/google/android/libraries/memorymonitor/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/a/b;->d:Lcom/google/android/libraries/memorymonitor/f;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/memorymonitor/d;->b(Lcom/google/android/libraries/memorymonitor/f;)V

    .line 961
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/e/a;

    iget v2, p0, Lcom/google/android/apps/gmm/base/app/b;->k:I

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/base/e/a;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 962
    return-void
.end method

.method public final y_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 669
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->y_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final z()Lcom/google/android/apps/gmm/map/t/d;
    .locals 1

    .prologue
    .line 739
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->z()Lcom/google/android/apps/gmm/map/t/d;

    move-result-object v0

    return-object v0
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/b;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->z_()Z

    move-result v0

    return v0
.end method
