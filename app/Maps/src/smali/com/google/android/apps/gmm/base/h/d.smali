.class Lcom/google/android/apps/gmm/base/h/d;
.super Landroid/graphics/drawable/LayerDrawable;
.source "PG"


# instance fields
.field final synthetic a:I

.field final synthetic b:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/h/c;[Landroid/graphics/drawable/Drawable;II)V
    .locals 0

    .prologue
    .line 134
    iput p3, p0, Lcom/google/android/apps/gmm/base/h/d;->a:I

    iput p4, p0, Lcom/google/android/apps/gmm/base/h/d;->b:I

    invoke-direct {p0, p2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public isStateful()Z
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x1

    return v0
.end method

.method protected onStateChange([I)Z
    .locals 4

    .prologue
    .line 141
    iget v1, p0, Lcom/google/android/apps/gmm/base/h/d;->a:I

    .line 142
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 143
    aget v2, p1, v0

    const v3, 0x10100a7

    if-ne v2, v3, :cond_0

    .line 144
    iget v0, p0, Lcom/google/android/apps/gmm/base/h/d;->b:I

    .line 148
    :goto_1
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/base/h/d;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 149
    invoke-super {p0, p1}, Landroid/graphics/drawable/LayerDrawable;->onStateChange([I)Z

    move-result v0

    return v0

    .line 142
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method
