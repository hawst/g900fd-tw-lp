.class public Lcom/google/android/apps/gmm/p/f/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/p/b/i;


# static fields
.field private static final J:F

.field private static final K:F

.field private static final L:F

.field private static final M:F

.field static final c:J

.field private static final t:F

.field private static final u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final A:[F

.field private final B:[F

.field private final C:[F

.field private D:J

.field private final E:[F

.field private final F:[F

.field private final G:[F

.field private H:I

.field private I:Lcom/google/android/apps/gmm/p/f/k;

.field private N:Z

.field private final O:Landroid/hardware/SensorEventListener;

.field final a:Ljava/lang/Object;

.field final b:Lcom/google/android/apps/gmm/p/f/l;

.field public d:Landroid/content/Context;

.field public e:Landroid/hardware/SensorManager;

.field f:Landroid/hardware/Sensor;

.field final g:[F

.field h:Landroid/hardware/Sensor;

.field i:Landroid/hardware/Sensor;

.field j:Landroid/hardware/Sensor;

.field final k:[F

.field final l:[F

.field final m:[F

.field final n:[F

.field o:J

.field p:J

.field q:Landroid/hardware/Sensor;

.field final r:[F

.field public s:Landroid/view/WindowManager;

.field private v:Z

.field private w:Lcom/google/android/apps/gmm/p/b/l;

.field private x:Z

.field private final y:Lcom/google/android/apps/gmm/p/f/a;

.field private final z:Lcom/google/android/apps/gmm/p/f/a;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const-wide/16 v4, 0x5

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 61
    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sput v2, Lcom/google/android/apps/gmm/p/f/h;->t:F

    .line 80
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sput-wide v2, Lcom/google/android/apps/gmm/p/f/h;->c:J

    .line 130
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Google Inc."

    aput-object v3, v2, v1

    const-string v3, "LG Electronics Inc."

    aput-object v3, v2, v0

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    array-length v3, v2

    if-ltz v3, :cond_1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    int-to-long v0, v3

    add-long/2addr v0, v4

    div-int/lit8 v3, v3, 0xa

    int-to-long v4, v3

    add-long/2addr v0, v4

    const-wide/32 v4, 0x7fffffff

    cmp-long v3, v0, v4

    if-lez v3, :cond_3

    const v0, 0x7fffffff

    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v1, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    sput-object v1, Lcom/google/android/apps/gmm/p/f/h;->u:Ljava/util/List;

    .line 359
    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/gmm/p/f/h;->J:F

    .line 362
    const-wide v0, 0x3fc99999a0000000L    # 0.20000000298023224

    .line 363
    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/gmm/p/f/h;->K:F

    .line 366
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 367
    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/gmm/p/f/h;->L:F

    .line 370
    const-wide v0, 0x3fb99999a0000000L    # 0.10000000149011612

    .line 371
    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/gmm/p/f/h;->M:F

    .line 370
    return-void

    .line 130
    :cond_3
    const-wide/32 v4, -0x80000000

    cmp-long v3, v0, v4

    if-gez v3, :cond_4

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_4
    long-to-int v0, v0

    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/p/b/k;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 6

    .prologue
    const/16 v5, 0x9

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->a:Ljava/lang/Object;

    .line 138
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/p/f/h;->v:Z

    .line 145
    sget-object v0, Lcom/google/android/apps/gmm/p/b/l;->a:Lcom/google/android/apps/gmm/p/b/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->w:Lcom/google/android/apps/gmm/p/b/l;

    .line 169
    new-instance v0, Lcom/google/android/apps/gmm/p/f/a;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/p/f/a;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->y:Lcom/google/android/apps/gmm/p/f/a;

    .line 174
    new-instance v0, Lcom/google/android/apps/gmm/p/f/a;

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/p/f/a;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->z:Lcom/google/android/apps/gmm/p/f/a;

    .line 190
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->g:[F

    .line 222
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->k:[F

    .line 231
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->l:[F

    .line 237
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->m:[F

    .line 243
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->n:[F

    .line 250
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->A:[F

    .line 257
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->B:[F

    .line 296
    new-array v0, v4, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->r:[F

    .line 303
    new-array v0, v4, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->C:[F

    .line 310
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/f/h;->D:J

    .line 314
    new-array v0, v5, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->E:[F

    .line 317
    new-array v0, v5, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->F:[F

    .line 322
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->G:[F

    .line 329
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/p/f/h;->H:I

    .line 341
    new-instance v0, Lcom/google/android/apps/gmm/p/f/k;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/p/f/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->I:Lcom/google/android/apps/gmm/p/f/k;

    .line 1175
    new-instance v0, Lcom/google/android/apps/gmm/p/f/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/p/f/i;-><init>(Lcom/google/android/apps/gmm/p/f/h;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->O:Landroid/hardware/SensorEventListener;

    .line 385
    new-instance v0, Lcom/google/android/apps/gmm/p/f/l;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/gmm/p/f/l;-><init>(Lcom/google/android/apps/gmm/p/b/k;Lcom/google/android/apps/gmm/shared/c/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    .line 386
    return-void
.end method

.method static a([F)F
    .locals 4

    .prologue
    .line 624
    const/4 v1, 0x0

    .line 625
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x3

    if-ge v0, v2, :cond_0

    .line 626
    aget v2, p0, v0

    .line 627
    mul-float/2addr v2, v2

    add-float/2addr v1, v2

    .line 625
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 632
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 635
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v0

    sub-double v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private a(I)Landroid/hardware/Sensor;
    .locals 2

    .prologue
    .line 570
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->d:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    return-object v0
.end method

.method private a(II)Landroid/hardware/Sensor;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 536
    invoke-static {p2}, Lcom/google/android/apps/gmm/p/f/h;->b(I)Ljava/lang/String;

    move-result-object v3

    .line 539
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->d:Landroid/content/Context;

    const-string v2, "sensor"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    .line 540
    if-nez v2, :cond_2

    .line 541
    const-string v0, "No sensor of "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_0
    move-object v0, v1

    .line 555
    :goto_1
    return-object v0

    .line 541
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 546
    :cond_2
    invoke-static {v2}, Lcom/google/android/apps/gmm/p/f/h;->a(Landroid/hardware/Sensor;)Ljava/lang/String;

    move-result-object v3

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->d:Landroid/content/Context;

    const-string v4, "sensor"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    iget-object v4, p0, Lcom/google/android/apps/gmm/p/f/h;->O:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v4, v2, p1}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 548
    const-string v0, "Failed registering for "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_2
    move-object v0, v1

    .line 549
    goto :goto_1

    .line 548
    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 552
    :cond_5
    const-string v0, "OrientationProviderImpl"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 553
    const-string v0, "Registered for "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :cond_6
    :goto_3
    move-object v0, v2

    .line 555
    goto :goto_1

    .line 553
    :cond_7
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method

.method static a(Landroid/hardware/Sensor;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 591
    const-string v0, "sensor of %s \"%s\" v%d by %s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/hardware/Sensor;->getType()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/p/f/h;->b(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 592
    invoke-virtual {p0}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Landroid/hardware/Sensor;->getVersion()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 591
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a([F[F)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 763
    array-length v0, p0

    array-length v2, p1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    move v2, v3

    .line 765
    :goto_1
    array-length v4, p0

    if-ge v0, v4, :cond_2

    .line 766
    aget v4, p0, v0

    aget v5, p0, v0

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    .line 765
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 768
    :cond_2
    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v0, v4

    .line 769
    cmpl-float v2, v0, v3

    if-nez v2, :cond_3

    .line 770
    :goto_2
    array-length v0, p0

    if-ge v1, v0, :cond_4

    .line 771
    aput v3, p1, v1

    .line 770
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 774
    :cond_3
    :goto_3
    array-length v2, p0

    if-ge v1, v2, :cond_4

    .line 775
    aget v2, p0, v1

    div-float/2addr v2, v0

    aput v2, p1, v1

    .line 774
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 778
    :cond_4
    return-void
.end method

.method private static b([F[F)F
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 745
    array-length v1, p0

    array-length v2, p1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v1, v0

    goto :goto_0

    .line 746
    :cond_1
    const/4 v1, 0x0

    .line 747
    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 748
    aget v2, p0, v0

    aget v3, p1, v0

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 747
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 750
    :cond_2
    return v1
.end method

.method private static b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 574
    packed-switch p0, :pswitch_data_0

    .line 586
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 576
    :pswitch_1
    const-string v0, "TYPE_ROTATION_VECTOR"

    goto :goto_0

    .line 578
    :pswitch_2
    const-string v0, "TYPE_MAGNETIC_FIELD"

    goto :goto_0

    .line 580
    :pswitch_3
    const-string v0, "TYPE_ACCELEROMETER"

    goto :goto_0

    .line 582
    :pswitch_4
    const-string v0, "TYPE_ORIENTATION"

    goto :goto_0

    .line 584
    :pswitch_5
    const-string v0, "TYPE_GYROSCOPE"

    goto :goto_0

    .line 574
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 432
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 433
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/h;->v:Z

    .line 436
    sget-object v0, Lcom/google/android/apps/gmm/p/b/l;->b:Lcom/google/android/apps/gmm/p/b/l;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/p/f/h;->a(Lcom/google/android/apps/gmm/p/b/l;)V

    .line 437
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final a(JLandroid/hardware/Sensor;)V
    .locals 11

    .prologue
    .line 789
    iget-object v4, p0, Lcom/google/android/apps/gmm/p/f/h;->a:Ljava/lang/Object;

    monitor-enter v4

    .line 791
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->f:Landroid/hardware/Sensor;

    if-ne p3, v0, :cond_8

    .line 794
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->g:[F

    const/4 v1, 0x0

    aget v1, v0, v1

    .line 795
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->g:[F

    const/4 v2, 0x1

    aget v0, v0, v2

    .line 796
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->g:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    .line 808
    iget v3, p0, Lcom/google/android/apps/gmm/p/f/h;->H:I

    const/4 v5, -0x1

    if-ne v3, v5, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/f/h;->s:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getRotation()I

    move-result v3

    :goto_0
    packed-switch v3, :pswitch_data_0

    :goto_1
    move v2, v1

    .line 934
    :goto_2
    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/f/h;->r:[F

    .line 935
    invoke-virtual {p3}, Landroid/hardware/Sensor;->getType()I

    move-result v1

    const/16 v5, 0xb

    if-eq v1, v5, :cond_e

    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_13

    .line 938
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    const/high16 v2, -0x40800000    # -1.0f

    iput v2, v1, Lcom/google/android/apps/gmm/p/f/l;->e:F

    .line 948
    :goto_4
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 950
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    const/high16 v1, -0x3b860000    # -1000.0f

    iput v1, v0, Lcom/google/android/apps/gmm/p/f/l;->f:F

    .line 957
    :goto_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->I:Lcom/google/android/apps/gmm/p/f/k;

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    iget v3, v0, Lcom/google/android/apps/gmm/p/f/l;->e:F

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    .line 958
    iget v5, v0, Lcom/google/android/apps/gmm/p/f/l;->f:F

    iget-object v6, p0, Lcom/google/android/apps/gmm/p/f/h;->w:Lcom/google/android/apps/gmm/p/b/l;

    .line 957
    iget-wide v0, v2, Lcom/google/android/apps/gmm/p/f/k;->a:J

    const-wide/high16 v8, -0x8000000000000000L

    cmp-long v0, v0, v8

    if-nez v0, :cond_16

    const/4 v0, 0x1

    :goto_6
    if-eqz v0, :cond_1

    iput-wide p1, v2, Lcom/google/android/apps/gmm/p/f/k;->a:J

    iput v3, v2, Lcom/google/android/apps/gmm/p/f/k;->b:F

    iput v5, v2, Lcom/google/android/apps/gmm/p/f/k;->c:F

    :cond_1
    if-eqz v0, :cond_2

    .line 959
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/p/f/l;->b()V

    .line 961
    :cond_2
    monitor-exit v4

    :goto_7
    return-void

    .line 808
    :cond_3
    iget v3, p0, Lcom/google/android/apps/gmm/p/f/h;->H:I

    goto :goto_0

    .line 810
    :pswitch_0
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v3, 0x42b40000    # 90.0f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_5

    .line 811
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/h;->x:Z

    if-eqz v0, :cond_4

    const/high16 v0, -0x3d4c0000    # -90.0f

    :goto_8
    add-float/2addr v1, v0

    .line 812
    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v0

    neg-float v0, v0

    const/high16 v3, 0x43340000    # 180.0f

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    sub-float v2, v3, v2

    mul-float/2addr v0, v2

    move v2, v1

    goto :goto_2

    .line 811
    :cond_4
    const/high16 v0, 0x42b40000    # 90.0f

    goto :goto_8

    .line 814
    :cond_5
    const/high16 v0, 0x42b40000    # 90.0f

    add-float/2addr v1, v0

    .line 815
    neg-float v0, v2

    move v2, v1

    .line 817
    goto :goto_2

    .line 819
    :pswitch_1
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v3, 0x42b40000    # 90.0f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_7

    .line 820
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/h;->x:Z

    if-eqz v0, :cond_6

    const/high16 v0, -0x3d4c0000    # -90.0f

    :goto_9
    sub-float/2addr v1, v0

    .line 821
    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v0

    const/high16 v3, 0x43340000    # 180.0f

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    sub-float v2, v3, v2

    mul-float/2addr v0, v2

    move v2, v1

    goto/16 :goto_2

    .line 820
    :cond_6
    const/high16 v0, 0x42b40000    # 90.0f

    goto :goto_9

    .line 823
    :cond_7
    const/high16 v0, 0x42b40000    # 90.0f

    sub-float/2addr v1, v0

    move v0, v2

    move v2, v1

    .line 826
    goto/16 :goto_2

    .line 828
    :pswitch_2
    const/high16 v2, 0x43340000    # 180.0f

    add-float/2addr v1, v2

    .line 829
    neg-float v0, v0

    goto/16 :goto_1

    .line 835
    :cond_8
    iput-wide p1, p0, Lcom/google/android/apps/gmm/p/f/h;->D:J

    .line 837
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->q:Landroid/hardware/Sensor;

    if-ne p3, v0, :cond_b

    .line 840
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->r:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->C:[F

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/p/f/h;->r:[F

    const/4 v5, 0x4

    invoke-static {v0, v1, v2, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 845
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->E:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/h;->r:[F

    invoke-static {v0, v1}, Landroid/hardware/SensorManager;->getRotationMatrixFromVector([F[F)V

    .line 870
    :cond_9
    iget v0, p0, Lcom/google/android/apps/gmm/p/f/h;->H:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->s:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    :goto_a
    packed-switch v0, :pswitch_data_1

    .line 884
    const/4 v1, 0x1

    .line 885
    const/4 v0, 0x2

    .line 887
    :goto_b
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->E:[F

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/f/h;->F:[F

    invoke-static {v2, v1, v0, v3}, Landroid/hardware/SensorManager;->remapCoordinateSystem([FII[F)Z

    .line 889
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->F:[F

    const/4 v1, 0x7

    aget v0, v0, v1

    sget v1, Lcom/google/android/apps/gmm/p/f/h;->t:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->F:[F

    const/16 v1, 0x8

    aget v0, v0, v1

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_d

    .line 908
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->F:[F

    const/4 v1, 0x1

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/f/h;->E:[F

    invoke-static {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->remapCoordinateSystem([FII[F)Z

    .line 910
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->E:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/h;->G:[F

    invoke-static {v0, v1}, Landroid/hardware/SensorManager;->getOrientation([F[F)[F

    .line 919
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->G:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    const v1, 0x42652ee0

    mul-float/2addr v0, v1

    const/high16 v1, 0x42b40000    # 90.0f

    sub-float/2addr v0, v1

    .line 931
    :goto_c
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/h;->G:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    const v2, 0x42652ee0

    mul-float/2addr v1, v2

    move v2, v1

    goto/16 :goto_2

    .line 850
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->m:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->A:[F

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/p/f/h;->m:[F

    const/4 v5, 0x3

    invoke-static {v0, v1, v2, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 852
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->n:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->B:[F

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/p/f/h;->n:[F

    const/4 v5, 0x3

    invoke-static {v0, v1, v2, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 857
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->E:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->l:[F

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/f/h;->k:[F

    invoke-static {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->getRotationMatrix([F[F[F[F)Z

    move-result v0

    .line 859
    if-nez v0, :cond_9

    .line 860
    monitor-exit v4

    goto/16 :goto_7

    .line 961
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 870
    :cond_c
    :try_start_1
    iget v0, p0, Lcom/google/android/apps/gmm/p/f/h;->H:I

    goto :goto_a

    .line 872
    :pswitch_3
    const/4 v1, 0x2

    .line 873
    const/16 v0, 0x81

    .line 874
    goto :goto_b

    .line 876
    :pswitch_4
    const/16 v1, 0x82

    .line 877
    const/4 v0, 0x1

    .line 878
    goto :goto_b

    .line 880
    :pswitch_5
    const/16 v1, 0x81

    .line 881
    const/16 v0, 0x82

    .line 882
    goto :goto_b

    .line 923
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->F:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/h;->G:[F

    invoke-static {v0, v1}, Landroid/hardware/SensorManager;->getOrientation([F[F)[F

    .line 925
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->G:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    const v1, 0x42652ee0

    mul-float/2addr v0, v1

    goto :goto_c

    .line 935
    :cond_e
    array-length v1, v3

    const/4 v5, 0x3

    if-ge v1, v5, :cond_f

    const/4 v1, 0x1

    goto/16 :goto_3

    :cond_f
    const/4 v1, 0x0

    :goto_d
    const/4 v5, 0x3

    if-ge v1, v5, :cond_12

    aget v5, v3, v1

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-nez v5, :cond_10

    aget v5, v3, v1

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const v6, 0x358637bd    # 1.0E-6f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_11

    :cond_10
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    :cond_12
    const/4 v1, 0x1

    goto/16 :goto_3

    .line 942
    :cond_13
    invoke-static {v2}, Lcom/google/android/apps/gmm/shared/c/s;->b(F)F

    move-result v1

    .line 943
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/p/f/l;->a(F)F

    move-result v1

    .line 944
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->y:Lcom/google/android/apps/gmm/p/f/a;

    invoke-virtual {v2, p1, p2, v1}, Lcom/google/android/apps/gmm/p/f/a;->a(JF)F

    move-result v1

    .line 945
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    iput v1, v2, Lcom/google/android/apps/gmm/p/f/l;->e:F

    goto/16 :goto_4

    .line 952
    :cond_14
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    const/high16 v2, -0x3b860000    # -1000.0f

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->z:Lcom/google/android/apps/gmm/p/f/a;

    .line 953
    invoke-virtual {v2, p1, p2, v0}, Lcom/google/android/apps/gmm/p/f/a;->a(JF)F

    move-result v0

    .line 952
    :cond_15
    iput v0, v1, Lcom/google/android/apps/gmm/p/f/l;->f:F

    goto/16 :goto_5

    .line 957
    :cond_16
    const/high16 v0, -0x3b860000    # -1000.0f

    cmpl-float v0, v5, v0

    if-nez v0, :cond_18

    const/4 v0, 0x1

    move v1, v0

    :goto_e
    iget v0, v2, Lcom/google/android/apps/gmm/p/f/k;->c:F

    const/high16 v7, -0x3b860000    # -1000.0f

    cmpl-float v0, v0, v7

    if-nez v0, :cond_19

    const/4 v0, 0x1

    :goto_f
    if-ne v1, v0, :cond_17

    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, v3, v0

    if-nez v0, :cond_1a

    const/4 v0, 0x1

    move v1, v0

    :goto_10
    iget v0, v2, Lcom/google/android/apps/gmm/p/f/k;->b:F

    const/high16 v7, -0x40800000    # -1.0f

    cmpl-float v0, v0, v7

    if-nez v0, :cond_1b

    const/4 v0, 0x1

    :goto_11
    if-eq v1, v0, :cond_1c

    :cond_17
    const/4 v0, 0x1

    goto/16 :goto_6

    :cond_18
    const/4 v0, 0x0

    move v1, v0

    goto :goto_e

    :cond_19
    const/4 v0, 0x0

    goto :goto_f

    :cond_1a
    const/4 v0, 0x0

    move v1, v0

    goto :goto_10

    :cond_1b
    const/4 v0, 0x0

    goto :goto_11

    :cond_1c
    sget-object v0, Lcom/google/android/apps/gmm/p/f/j;->a:[I

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/p/b/l;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    const/high16 v0, 0x40000000    # 2.0f

    :goto_12
    iget v1, v2, Lcom/google/android/apps/gmm/p/f/k;->b:F

    sub-float v1, v3, v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/shared/c/s;->c(F)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v6, v2, Lcom/google/android/apps/gmm/p/f/k;->c:F

    sub-float v6, v5, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    cmpl-float v1, v1, v0

    if-gtz v1, :cond_1d

    cmpl-float v0, v6, v0

    if-lez v0, :cond_1e

    :cond_1d
    const/4 v0, 0x1

    goto/16 :goto_6

    :pswitch_6
    const v0, 0x3e4ccccd    # 0.2f

    goto :goto_12

    :cond_1e
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 808
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 870
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch

    .line 957
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/p/b/j;)V
    .locals 3

    .prologue
    .line 1073
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1074
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/f/l;->b:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1075
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/p/b/l;)V
    .locals 4

    .prologue
    const/4 v0, 0x3

    .line 458
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 462
    :try_start_0
    const-string v1, "goldfish"

    sget-object v3, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 463
    monitor-exit v2

    .line 522
    :goto_0
    return-void

    .line 467
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/p/f/h;->v:Z

    if-nez v1, :cond_1

    .line 468
    monitor-exit v2

    goto :goto_0

    .line 522
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 472
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/h;->w:Lcom/google/android/apps/gmm/p/b/l;

    if-ne v1, p1, :cond_2

    .line 473
    monitor-exit v2

    goto :goto_0

    .line 475
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/f/h;->w:Lcom/google/android/apps/gmm/p/b/l;

    .line 482
    iget v1, p1, Lcom/google/android/apps/gmm/p/b/l;->d:I

    if-nez v1, :cond_5

    move v1, v0

    .line 485
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->d:Landroid/content/Context;

    const-string v3, "sensor"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/f/h;->O:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 492
    const/16 v0, 0xb

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/p/f/h;->a(II)Landroid/hardware/Sensor;

    move-result-object v0

    .line 493
    if-eqz v0, :cond_6

    .line 494
    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->q:Landroid/hardware/Sensor;

    .line 497
    const/4 v0, 0x2

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/p/f/h;->a(II)Landroid/hardware/Sensor;

    move-result-object v0

    .line 499
    if-eqz v0, :cond_4

    .line 500
    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->i:Landroid/hardware/Sensor;

    .line 503
    :cond_4
    monitor-exit v2

    goto :goto_0

    .line 482
    :cond_5
    iget v0, p1, Lcom/google/android/apps/gmm/p/b/l;->d:I

    move v1, v0

    goto :goto_1

    .line 506
    :cond_6
    const/4 v0, 0x2

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/p/f/h;->a(II)Landroid/hardware/Sensor;

    move-result-object v0

    .line 507
    const/4 v3, 0x1

    invoke-direct {p0, v1, v3}, Lcom/google/android/apps/gmm/p/f/h;->a(II)Landroid/hardware/Sensor;

    move-result-object v3

    .line 508
    if-eqz v0, :cond_7

    if-eqz v3, :cond_7

    .line 509
    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->h:Landroid/hardware/Sensor;

    .line 510
    iput-object v3, p0, Lcom/google/android/apps/gmm/p/f/h;->j:Landroid/hardware/Sensor;

    .line 511
    monitor-exit v2

    goto :goto_0

    .line 512
    :cond_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_a

    .line 514
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->d:Landroid/content/Context;

    const-string v3, "sensor"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/f/h;->O:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 517
    :cond_a
    const/4 v0, 0x3

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/p/f/h;->a(II)Landroid/hardware/Sensor;

    move-result-object v0

    .line 518
    if-eqz v0, :cond_b

    .line 519
    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->f:Landroid/hardware/Sensor;

    .line 520
    sget-object v1, Lcom/google/android/apps/gmm/p/f/h;->u:Ljava/util/List;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/h;->x:Z

    .line 522
    :cond_b
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 442
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 443
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->d:Landroid/content/Context;

    const-string v2, "sensor"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->e:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->O:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 444
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/h;->v:Z

    .line 448
    sget-object v0, Lcom/google/android/apps/gmm/p/b/l;->a:Lcom/google/android/apps/gmm/p/b/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->w:Lcom/google/android/apps/gmm/p/b/l;

    .line 449
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->q:Landroid/hardware/Sensor;

    .line 450
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->h:Landroid/hardware/Sensor;

    .line 451
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->j:Landroid/hardware/Sensor;

    .line 452
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->f:Landroid/hardware/Sensor;

    .line 453
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Lcom/google/android/apps/gmm/p/b/j;)V
    .locals 2

    .prologue
    .line 1080
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1081
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/f/l;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1082
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final b(Landroid/hardware/Sensor;)Z
    .locals 10

    .prologue
    const-wide/high16 v8, -0x8000000000000000L

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 649
    iget-object v4, p0, Lcom/google/android/apps/gmm/p/f/h;->a:Ljava/lang/Object;

    monitor-enter v4

    .line 652
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->f:Landroid/hardware/Sensor;

    if-ne p1, v2, :cond_0

    .line 653
    monitor-exit v4

    .line 713
    :goto_0
    return v0

    .line 656
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->q:Landroid/hardware/Sensor;

    if-ne p1, v2, :cond_6

    .line 658
    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/f/h;->D:J

    cmp-long v2, v2, v8

    if-nez v2, :cond_1

    .line 659
    monitor-exit v4

    goto :goto_0

    .line 714
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v2, v1

    .line 663
    :goto_1
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/p/f/h;->r:[F

    const/4 v3, 0x4

    if-ge v2, v3, :cond_3

    .line 664
    iget-object v3, p0, Lcom/google/android/apps/gmm/p/f/h;->r:[F

    aget v3, v3, v2

    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    iget-object v5, p0, Lcom/google/android/apps/gmm/p/f/h;->C:[F

    aget v5, v5, v2

    .line 665
    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-eq v3, v5, :cond_2

    .line 666
    monitor-exit v4

    goto :goto_0

    .line 663
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 674
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->w:Lcom/google/android/apps/gmm/p/b/l;

    sget-object v3, Lcom/google/android/apps/gmm/p/b/l;->c:Lcom/google/android/apps/gmm/p/b/l;

    if-ne v2, v3, :cond_4

    sget v2, Lcom/google/android/apps/gmm/p/f/h;->M:F

    float-to-double v2, v2

    .line 677
    :goto_2
    iget-object v5, p0, Lcom/google/android/apps/gmm/p/f/h;->r:[F

    iget-object v6, p0, Lcom/google/android/apps/gmm/p/f/h;->C:[F

    invoke-static {v5, v6}, Lcom/google/android/apps/gmm/p/f/h;->b([F[F)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    float-to-double v6, v5

    cmpg-double v2, v6, v2

    if-gez v2, :cond_5

    .line 679
    monitor-exit v4

    goto :goto_0

    .line 674
    :cond_4
    sget v2, Lcom/google/android/apps/gmm/p/f/h;->L:F

    float-to-double v2, v2

    goto :goto_2

    .line 683
    :cond_5
    monitor-exit v4

    move v0, v1

    goto :goto_0

    .line 688
    :cond_6
    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/f/h;->o:J

    iget-wide v6, p0, Lcom/google/android/apps/gmm/p/f/h;->p:J

    sub-long/2addr v2, v6

    .line 689
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 690
    sget-wide v6, Lcom/google/android/apps/gmm/p/f/h;->c:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_7

    .line 691
    monitor-exit v4

    move v0, v1

    goto :goto_0

    .line 695
    :cond_7
    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/f/h;->D:J

    cmp-long v2, v2, v8

    if-nez v2, :cond_8

    .line 696
    monitor-exit v4

    goto :goto_0

    .line 705
    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->w:Lcom/google/android/apps/gmm/p/b/l;

    sget-object v3, Lcom/google/android/apps/gmm/p/b/l;->c:Lcom/google/android/apps/gmm/p/b/l;

    if-ne v2, v3, :cond_a

    sget v2, Lcom/google/android/apps/gmm/p/f/h;->K:F

    float-to-double v2, v2

    .line 707
    :goto_3
    iget-object v5, p0, Lcom/google/android/apps/gmm/p/f/h;->m:[F

    iget-object v6, p0, Lcom/google/android/apps/gmm/p/f/h;->A:[F

    invoke-static {v5, v6}, Lcom/google/android/apps/gmm/p/f/h;->b([F[F)F

    move-result v5

    float-to-double v6, v5

    cmpg-double v5, v6, v2

    if-ltz v5, :cond_9

    iget-object v5, p0, Lcom/google/android/apps/gmm/p/f/h;->n:[F

    iget-object v6, p0, Lcom/google/android/apps/gmm/p/f/h;->B:[F

    .line 708
    invoke-static {v5, v6}, Lcom/google/android/apps/gmm/p/f/h;->b([F[F)F

    move-result v5

    float-to-double v6, v5

    cmpg-double v2, v6, v2

    if-gez v2, :cond_b

    .line 709
    :cond_9
    monitor-exit v4

    goto/16 :goto_0

    .line 705
    :cond_a
    sget v2, Lcom/google/android/apps/gmm/p/f/h;->J:F

    float-to-double v2, v2

    goto :goto_3

    .line 713
    :cond_b
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final c()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1087
    iget-object v3, p0, Lcom/google/android/apps/gmm/p/f/h;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 1088
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/p/f/h;->N:Z

    if-nez v2, :cond_1

    .line 1089
    iget-object v4, p0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/f/h;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v5, "android.hardware.sensor.compass"

    invoke-virtual {v2, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/16 v2, 0xb

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/p/f/h;->a(I)Landroid/hardware/Sensor;

    move-result-object v2

    if-eqz v2, :cond_2

    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    :cond_0
    :goto_1
    iput-boolean v0, v4, Lcom/google/android/apps/gmm/p/f/l;->c:Z

    .line 1090
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/h;->N:Z

    .line 1092
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/h;->b:Lcom/google/android/apps/gmm/p/f/l;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/p/f/l;->c:Z

    monitor-exit v3

    return v0

    :cond_2
    move v2, v1

    .line 1089
    goto :goto_0

    :cond_3
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/p/f/h;->a(I)Landroid/hardware/Sensor;

    move-result-object v2

    if-eqz v2, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_4

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/p/f/h;->a(I)Landroid/hardware/Sensor;

    move-result-object v2

    if-eqz v2, :cond_7

    move v2, v0

    :goto_3
    if-nez v2, :cond_0

    :cond_4
    const/4 v2, 0x3

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/p/f/h;->a(I)Landroid/hardware/Sensor;

    move-result-object v2

    if-eqz v2, :cond_8

    move v2, v0

    :goto_4
    if-nez v2, :cond_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v2, v1

    goto :goto_2

    :cond_7
    move v2, v1

    goto :goto_3

    :cond_8
    move v2, v1

    goto :goto_4

    .line 1093
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
