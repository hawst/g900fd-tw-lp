.class public abstract Lcom/google/android/apps/gmm/map/o/c/a;
.super Lcom/google/android/apps/gmm/map/o/c/g;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/o/h;

.field public final b:F


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/map/o/h;F)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/o/c/g;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/c/a;->a:Lcom/google/android/apps/gmm/map/o/h;

    .line 25
    iput p2, p0, Lcom/google/android/apps/gmm/map/o/c/a;->b:F

    .line 26
    return-void
.end method


# virtual methods
.method protected abstract U_()Lcom/google/android/apps/gmm/v/by;
.end method

.method public final a()F
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/c/a;->e:F

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/v/by;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/c/a;->U_()Lcom/google/android/apps/gmm/v/by;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/ap;)Z
    .locals 3

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/c/a;->U_()Lcom/google/android/apps/gmm/v/by;

    move-result-object v0

    .line 36
    if-nez v0, :cond_0

    .line 37
    const/4 v0, 0x0

    .line 43
    :goto_0
    return v0

    .line 40
    :cond_0
    iget v1, v0, Lcom/google/android/apps/gmm/v/by;->c:I

    iget v2, v0, Lcom/google/android/apps/gmm/v/by;->a:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/apps/gmm/map/o/c/a;->e:F

    .line 41
    iget v1, v0, Lcom/google/android/apps/gmm/v/by;->d:I

    iget v2, v0, Lcom/google/android/apps/gmm/v/by;->b:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/apps/gmm/map/o/c/a;->f:F

    .line 42
    iget-object v1, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/bw;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/v/bz;->a(Lcom/google/android/apps/gmm/v/by;)V

    .line 43
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/c/a;->f:F

    return v0
.end method
