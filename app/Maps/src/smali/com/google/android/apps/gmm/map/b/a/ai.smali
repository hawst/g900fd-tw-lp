.class public abstract Lcom/google/android/apps/gmm/map/b/a/ai;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/map/b/a/ai;",
        ">;"
    }
.end annotation


# static fields
.field private static final I:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            ">;"
        }
    .end annotation
.end field

.field private static final J:J

.field public static final a:J

.field public static final b:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final c:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final d:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final e:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final f:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final g:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final h:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final i:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final j:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final k:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final l:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final m:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final n:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final o:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final p:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final q:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final r:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final s:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final t:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final u:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final v:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final w:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public static final x:Lcom/google/android/apps/gmm/map/b/a/ai;


# instance fields
.field public final A:Ljava/lang/String;

.field public final B:I

.field public final C:Z

.field public final D:Z

.field public final E:Z

.field public final F:Z

.field public volatile G:I

.field public final H:Z

.field private final K:J

.field public final y:I

.field public final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0x80

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->I:Ljava/util/Map;

    .line 40
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xe

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/map/b/a/ai;->J:J

    .line 46
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/map/b/a/ai;->a:J

    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ak;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ak;-><init>(I)V

    .line 51
    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->b:I

    .line 52
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->f:Z

    .line 53
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ak;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ak;

    const/16 v1, 0x16

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ak;-><init>(I)V

    .line 60
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->f:Z

    .line 61
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ak;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ak;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ak;-><init>(I)V

    const-string v1, "_tran_base"

    .line 65
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    .line 66
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->f:Z

    .line 67
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ak;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->d:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 68
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/an;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/an;-><init>(I)V

    .line 70
    iput-boolean v5, v0, Lcom/google/android/apps/gmm/map/b/a/al;->e:Z

    .line 71
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/an;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->e:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/an;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/an;-><init>(I)V

    const-string v1, "_ter"

    .line 74
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    .line 75
    iput-boolean v5, v0, Lcom/google/android/apps/gmm/map/b/a/al;->e:Z

    .line 76
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/an;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 77
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/av;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/av;-><init>(I)V

    const-string v1, "_traf"

    .line 79
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    sget-wide v2, Lcom/google/android/apps/gmm/map/b/a/ai;->a:J

    .line 80
    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/al;->i:J

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/av;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 81
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/av;

    const/16 v1, 0x1d

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/av;-><init>(I)V

    .line 83
    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->b:I

    const-string v1, "_traf"

    .line 84
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    sget-wide v2, Lcom/google/android/apps/gmm/map/b/a/ai;->a:J

    .line 85
    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/al;->i:J

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/av;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 86
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/av;

    const/16 v1, 0x1f

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/av;-><init>(I)V

    .line 88
    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->b:I

    const-string v1, "_traf"

    .line 89
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    sget-wide v2, Lcom/google/android/apps/gmm/map/b/a/ai;->a:J

    .line 90
    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/al;->i:J

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/av;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->i:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 91
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/av;

    const/16 v1, 0x17

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/av;-><init>(I)V

    const-string v1, "_traf"

    .line 93
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    sget-wide v2, Lcom/google/android/apps/gmm/map/b/a/ai;->a:J

    .line 94
    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/al;->i:J

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/av;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->j:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/at;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/at;-><init>(I)V

    .line 97
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    sget-wide v2, Lcom/google/android/apps/gmm/map/b/a/ai;->J:J

    .line 98
    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/al;->i:J

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/at;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 99
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ak;

    const/16 v1, 0x12

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ak;-><init>(I)V

    .line 101
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/ak;->a:Z

    const-string v1, "_vec_bic"

    .line 102
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    .line 103
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ak;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->l:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 104
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/an;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/an;-><init>(I)V

    .line 106
    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/al;->c:I

    const-string v1, "_ter_bic"

    .line 107
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/an;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->m:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 108
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/an;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/an;-><init>(I)V

    .line 110
    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/al;->c:I

    const-string v1, "_hy_bic"

    .line 111
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/an;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->n:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 112
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/av;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/av;-><init>(I)V

    .line 114
    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->b:I

    const-string v1, "_bike"

    .line 115
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    .line 116
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/av;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 117
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/av;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/av;-><init>(I)V

    const-string v1, "_tran"

    .line 119
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    .line 120
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/av;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->p:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 121
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/av;

    const/16 v1, 0xe

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/av;-><init>(I)V

    .line 123
    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->b:I

    .line 124
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    const-string v1, "_inaka"

    .line 125
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/av;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->q:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 126
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ap;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ap;-><init>(I)V

    .line 128
    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->b:I

    const-string v1, "_labl"

    .line 129
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    .line 130
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->f:Z

    .line 131
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ap;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->r:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 132
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ap;

    const/16 v1, 0x15

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ap;-><init>(I)V

    const-string v1, "_tran_labl"

    .line 134
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    .line 135
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->f:Z

    .line 136
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ap;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->s:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 137
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/av;

    const/16 v1, 0x1b

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/av;-><init>(I)V

    const-string v1, "_my_maps"

    .line 139
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    .line 140
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/av;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->t:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 141
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/av;

    const/16 v1, 0x19

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/av;-><init>(I)V

    const-string v1, "_api"

    .line 143
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/av;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->u:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 144
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ar;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ar;-><init>(I)V

    const-string v1, "_psm"

    .line 146
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    .line 147
    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->b:I

    .line 148
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->f:Z

    .line 149
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ar;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 155
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/av;

    invoke-direct {v0, v5}, Lcom/google/android/apps/gmm/map/b/a/av;-><init>(I)V

    const-string v1, "_star"

    .line 157
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    .line 158
    iput-boolean v5, v0, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    .line 159
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/b/a/al;->h:Z

    .line 160
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/av;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->w:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 162
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/av;

    const/16 v1, 0x1a

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/av;-><init>(I)V

    const-string v1, "_spotlight"

    .line 164
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    .line 165
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/av;->a()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->x:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 162
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/b/a/al;)V
    .locals 2

    .prologue
    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->G:I

    .line 246
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/al;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->y:I

    .line 247
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/al;->c:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->z:I

    .line 248
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/b/a/al;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->A:Ljava/lang/String;

    .line 249
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/b/a/al;->e:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->C:Z

    .line 250
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/b/a/al;->f:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->D:Z

    .line 251
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/b/a/al;->g:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->E:Z

    .line 253
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->I:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->B:I

    .line 254
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/b/a/al;->h:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->F:Z

    .line 255
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->y:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->z:I

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 256
    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->I:Ljava/util/Map;

    invoke-interface {v1, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/b/a/al;->i:J

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->K:J

    .line 267
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/b/a/al;->j:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->H:Z

    .line 268
    return-void
.end method

.method public static a(I)Lcom/google/android/apps/gmm/map/b/a/ai;
    .locals 2

    .prologue
    .line 348
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->I:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ai;

    return-object v0
.end method

.method public static a()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            ">;"
        }
    .end annotation

    .prologue
    .line 355
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->I:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J
    .locals 9

    .prologue
    const-wide/16 v2, -0x1

    .line 467
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->K:J

    .line 468
    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->r()Lcom/google/r/b/a/ane;

    move-result-object v1

    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->y:I

    invoke-static {v0}, Lcom/google/r/b/a/amx;->a(I)Lcom/google/r/b/a/amx;

    move-result-object v4

    new-instance v0, Lcom/google/n/ai;

    iget-object v5, v1, Lcom/google/r/b/a/ane;->c:Ljava/util/List;

    sget-object v6, Lcom/google/r/b/a/ane;->d:Lcom/google/n/aj;

    invoke-direct {v0, v5, v6}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-wide v0, v2

    .line 469
    :goto_0
    cmp-long v4, v0, v2

    if-nez v4, :cond_4

    move-wide v0, v2

    .line 471
    :goto_1
    return-wide v0

    .line 468
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    iget-object v0, v1, Lcom/google/r/b/a/ane;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v1, Lcom/google/r/b/a/ane;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ed;->d()Lcom/google/r/b/a/ed;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ed;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ed;

    new-instance v6, Lcom/google/n/ai;

    iget-object v7, v0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    sget-object v8, Lcom/google/r/b/a/ed;->d:Lcom/google/n/aj;

    invoke-direct {v6, v7, v8}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    invoke-interface {v6, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v0, v0, Lcom/google/r/b/a/ed;->b:I

    int-to-long v4, v0

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    goto :goto_0

    :cond_3
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v1, v1, Lcom/google/r/b/a/ane;->b:I

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    goto :goto_0

    .line 471
    :cond_4
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_1
.end method

.method public abstract a(Lcom/google/android/apps/gmm/shared/net/a/b;)J
.end method

.method public abstract b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 393
    const/4 v0, 0x0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 400
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/ai;

    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->B:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ai;->B:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 407
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 415
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 528
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 377
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 379
    :try_start_0
    invoke-virtual {v3, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-ne p0, v4, :cond_0

    .line 380
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 386
    :goto_1
    return-object v0

    :catch_0
    move-exception v3

    .line 377
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 386
    :cond_1
    const-string v0, "unknown"

    goto :goto_1
.end method
