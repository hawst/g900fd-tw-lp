.class public Lcom/google/android/apps/gmm/aa/d/t;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Ljava/io/File;

.field c:Ljava/io/File;

.field d:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final e:Lcom/google/android/apps/gmm/shared/net/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/android/apps/gmm/aa/d/t;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/aa/d/t;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/net/ad;)V
    .locals 6

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/d/t;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 40
    const-string v0, "tts-temp"

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/d/t;->b:Ljava/io/File;

    .line 41
    iput-object p2, p0, Lcom/google/android/apps/gmm/aa/d/t;->e:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 43
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/d/t;->b:Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/t;->e:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x18

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "tts-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/d/t;->c:Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/t;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    sget-object v0, Lcom/google/android/apps/gmm/aa/d/t;->a:Ljava/lang/String;

    const-string v0, "Created new directory: "

    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/d/t;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/t;->e:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/aa/d/u;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/aa/d/u;-><init>(Lcom/google/android/apps/gmm/aa/d/t;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 44
    return-void

    .line 43
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
