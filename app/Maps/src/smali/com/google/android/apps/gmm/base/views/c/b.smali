.class public Lcom/google/android/apps/gmm/base/views/c/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/google/r/b/a/acy;

.field public final c:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, v1, v1, v0}, Lcom/google/android/apps/gmm/base/views/c/b;-><init>(Ljava/lang/String;Lcom/google/r/b/a/acy;F)V

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/r/b/a/acy;F)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/c/b;->a:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/views/c/b;->b:Lcom/google/r/b/a/acy;

    .line 26
    iput p3, p0, Lcom/google/android/apps/gmm/base/views/c/b;->c:F

    .line 27
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 31
    if-ne p1, p0, :cond_1

    .line 39
    :cond_0
    :goto_0
    return v0

    .line 33
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/base/views/c/b;

    if-eqz v2, :cond_7

    .line 34
    check-cast p1, Lcom/google/android/apps/gmm/base/views/c/b;

    .line 35
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/b;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/views/c/b;->a:Ljava/lang/String;

    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_5

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_2
    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/b;->b:Lcom/google/r/b/a/acy;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/views/c/b;->b:Lcom/google/r/b/a/acy;

    .line 36
    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_3
    move v2, v0

    :goto_2
    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/c/b;->c:F

    iget v3, p1, Lcom/google/android/apps/gmm/base/views/c/b;->c:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v2, v1

    .line 35
    goto :goto_1

    :cond_6
    move v2, v1

    .line 36
    goto :goto_2

    :cond_7
    move v0, v1

    .line 39
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 44
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/b;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/b;->b:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/c/b;->c:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
