.class public Lcom/google/android/apps/gmm/map/util/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final A:Lcom/google/b/c/dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dc",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/util/d;",
            ">;"
        }
    .end annotation
.end field

.field private static final B:[Ljava/lang/String;

.field public static final a:Z

.field public static final b:Z

.field public static final c:Z

.field public static final d:Z

.field public static final e:Z

.field public static final f:Z

.field public static final g:Z

.field public static final h:D

.field public static final i:I

.field public static final j:I

.field public static final k:Z

.field public static final l:Z

.field public static final m:Z

.field public static final n:Z

.field public static final o:Z

.field public static final p:Z

.field public static final q:Z

.field private static final r:Lcom/google/android/apps/gmm/map/util/d;

.field private static final s:Lcom/google/android/apps/gmm/map/util/d;

.field private static final t:Lcom/google/android/apps/gmm/map/util/d;

.field private static final u:Lcom/google/android/apps/gmm/map/util/d;

.field private static final v:Lcom/google/android/apps/gmm/map/util/d;

.field private static final w:Lcom/google/android/apps/gmm/map/util/d;

.field private static final x:Lcom/google/android/apps/gmm/map/util/d;

.field private static final y:Lcom/google/android/apps/gmm/map/util/d;

.field private static z:Lcom/google/android/apps/gmm/v/ao;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    const/4 v7, 0x1

    const/16 v12, 0x10

    const-wide/high16 v10, 0x4022000000000000L    # 9.0

    const/4 v4, 0x0

    .line 99
    new-instance v0, Lcom/google/android/apps/gmm/map/util/d;

    invoke-direct {v0, v2, v3, v4, v4}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    sput-object v0, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 103
    new-instance v1, Lcom/google/android/apps/gmm/map/util/e;

    const/16 v5, 0x800

    const/4 v6, 0x4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/util/e;-><init>(DIII)V

    .line 108
    sput-object v1, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    sput-object v1, Lcom/google/android/apps/gmm/map/util/c;->t:Lcom/google/android/apps/gmm/map/util/d;

    .line 111
    new-instance v0, Lcom/google/android/apps/gmm/map/util/d;

    invoke-direct {v0, v2, v3, v4, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    sput-object v0, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 115
    new-instance v0, Lcom/google/android/apps/gmm/map/util/d;

    const/16 v1, 0x8

    invoke-direct {v0, v10, v11, v4, v1}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    sput-object v0, Lcom/google/android/apps/gmm/map/util/c;->v:Lcom/google/android/apps/gmm/map/util/d;

    .line 119
    new-instance v0, Lcom/google/android/apps/gmm/map/util/d;

    const/16 v1, 0x58

    const/16 v5, 0x8

    invoke-direct {v0, v10, v11, v1, v5}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    sput-object v0, Lcom/google/android/apps/gmm/map/util/c;->w:Lcom/google/android/apps/gmm/map/util/d;

    .line 123
    new-instance v0, Lcom/google/android/apps/gmm/map/util/d;

    const-wide/high16 v8, 0x402e000000000000L    # 15.0

    const/16 v1, 0x8

    invoke-direct {v0, v8, v9, v4, v1}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    sput-object v0, Lcom/google/android/apps/gmm/map/util/c;->x:Lcom/google/android/apps/gmm/map/util/d;

    .line 133
    new-instance v0, Lcom/google/android/apps/gmm/map/util/d;

    const/16 v1, 0x40

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    sput-object v0, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 142
    new-instance v0, Lcom/google/b/c/dd;

    invoke-direct {v0}, Lcom/google/b/c/dd;-><init>()V

    const-string v1, "ASUS TRANSFORMER PAD TF300T"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const/4 v6, 0x4

    invoke-direct {v5, v2, v3, v4, v6}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 145
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GALAXY NEXUS"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const/16 v6, 0x8

    invoke-direct {v5, v2, v3, v4, v6}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 149
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "NEXUS 4"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const/16 v6, 0x8

    invoke-direct {v5, v2, v3, v4, v6}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 151
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "NEXUS 10"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const-wide/high16 v8, 0x4018000000000000L    # 6.0

    const/16 v6, 0x8

    invoke-direct {v5, v8, v9, v4, v6}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 153
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "FULL AOSP ON MANTA"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const-wide/high16 v8, 0x4018000000000000L    # 6.0

    invoke-direct {v5, v8, v9, v4, v4}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 155
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "NEXUS S"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const/4 v6, 0x2

    invoke-direct {v5, v2, v3, v4, v6}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 157
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "NEXUS 7"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const/16 v6, 0xc

    invoke-direct {v5, v2, v3, v4, v6}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 159
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "XOOM"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const/4 v6, 0x4

    invoke-direct {v5, v2, v3, v4, v6}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 163
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "DROID RAZR HD"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const/16 v6, 0x40

    invoke-direct {v5, v10, v11, v4, v6}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 166
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "XT907"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const/16 v6, 0x40

    invoke-direct {v5, v10, v11, v4, v6}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 168
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "XT890"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 173
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "MB865"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 175
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "XT910"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 177
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "DROID RAZR"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 178
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "Motorola RAZR MAXX"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 179
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "DROID BIONIC"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 180
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "201M"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 181
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "XT925"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 182
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "MB886"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 183
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "MZ617"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 184
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "XOOM 2"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 185
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "XT886"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 186
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "XT885"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 187
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "IS12M"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 188
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "MZ608"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 189
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "XT897"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 190
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "XOOM 2 ME"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 191
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "ME865"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 192
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "MZ616"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 193
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "MZ607"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 194
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "XT881"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 195
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "MZ609"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 196
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "XT901"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 197
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "XT905"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 198
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "Unknown"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->y:Lcom/google/android/apps/gmm/map/util/d;

    .line 201
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9100"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 204
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9100T"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 205
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9100G"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 206
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9100M"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 207
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9100P"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 208
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9210"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 209
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9210T"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 210
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "ISW11SC"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 211
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SC-02C"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 212
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SC-03D"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 213
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SCH-R760"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 214
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-I757M"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 215
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-I777"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 216
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-I927"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 217
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-T989"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 218
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-T989D"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 219
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SHV-E110S"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 220
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SHV-E120S"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 221
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SHW-M250K"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 222
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SHW-M250L"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 223
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SHW-M250S"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 224
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SPH-D710"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 225
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SPH-D710BST"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 226
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SPH-D710VMUB"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->u:Lcom/google/android/apps/gmm/map/util/d;

    .line 227
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SAMSUNG-SGH-I747"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const/16 v6, 0x58

    invoke-direct {v5, v10, v11, v6, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 230
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-N064"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    invoke-direct {v5, v10, v11, v4, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 231
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SC-06D"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    invoke-direct {v5, v10, v11, v4, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 232
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9300"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    invoke-direct {v5, v10, v11, v4, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 233
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9300T"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    invoke-direct {v5, v10, v11, v4, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 234
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9305N"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    invoke-direct {v5, v10, v11, v4, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 235
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9305T"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    invoke-direct {v5, v10, v11, v4, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 236
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SHV-E210K"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    invoke-direct {v5, v10, v11, v4, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 237
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SHV-E210L"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    invoke-direct {v5, v10, v11, v4, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 238
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SHV-E210S"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    invoke-direct {v5, v10, v11, v4, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 239
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-T999"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const/16 v6, 0x58

    invoke-direct {v5, v10, v11, v6, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 240
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SCH-R530"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    invoke-direct {v5, v10, v11, v4, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 241
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SCH-I535"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const/16 v6, 0x58

    invoke-direct {v5, v10, v11, v6, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 242
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SPH-L710"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const/16 v6, 0x58

    invoke-direct {v5, v10, v11, v6, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 243
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9308"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    invoke-direct {v5, v10, v11, v4, v12}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 244
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9500"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->v:Lcom/google/android/apps/gmm/map/util/d;

    .line 247
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SHV-E300K"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->v:Lcom/google/android/apps/gmm/map/util/d;

    .line 248
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SHV-E300L"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->v:Lcom/google/android/apps/gmm/map/util/d;

    .line 249
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SHV-E300S"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->v:Lcom/google/android/apps/gmm/map/util/d;

    .line 250
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9505"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->v:Lcom/google/android/apps/gmm/map/util/d;

    .line 251
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-I337"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->w:Lcom/google/android/apps/gmm/map/util/d;

    .line 252
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-M919"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->v:Lcom/google/android/apps/gmm/map/util/d;

    .line 253
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SCH-I545"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->w:Lcom/google/android/apps/gmm/map/util/d;

    .line 254
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SPH-L720"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->v:Lcom/google/android/apps/gmm/map/util/d;

    .line 255
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SCH-R970"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->v:Lcom/google/android/apps/gmm/map/util/d;

    .line 256
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9508"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->v:Lcom/google/android/apps/gmm/map/util/d;

    .line 257
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SCH-I959"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->v:Lcom/google/android/apps/gmm/map/util/d;

    .line 258
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-I9502"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->v:Lcom/google/android/apps/gmm/map/util/d;

    .line 259
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-N045"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->v:Lcom/google/android/apps/gmm/map/util/d;

    .line 260
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SC-04E"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->v:Lcom/google/android/apps/gmm/map/util/d;

    .line 261
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-N7100"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 264
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-N7102"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 265
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-N7105"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 266
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "GT-N7108"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 267
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SCH-I605"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 268
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SCH-R950"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 269
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-I317"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 270
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-I317M"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 271
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-T889"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 272
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-T889V"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 273
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SPH-L900"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 274
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SCH-N719"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 275
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SGH-N025"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 276
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SC-02E"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 277
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SHV-E250K"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 278
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SHV-E250L"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 279
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SHV-E250S"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 280
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "SAMSUNG-SGH-I317"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 281
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "F-02E"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 284
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "F-04E"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 285
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "F-05D"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 286
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "F-05E"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 287
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "F-10D"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 288
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "T-02D"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 289
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "ISW11F"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 290
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "FAR70B"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 291
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "M532"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 292
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "M702"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 293
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "HTC ONE X"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->t:Lcom/google/android/apps/gmm/map/util/d;

    .line 295
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "HTC ONE X+"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->t:Lcom/google/android/apps/gmm/map/util/d;

    .line 296
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "A100"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 298
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "A200"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 299
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "A500"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 300
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "A510"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 301
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "ISW13F"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 302
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "TF101"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 304
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "Transformer TF101"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 305
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "Transformer TF101G"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 306
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "ASUS Tranfsformer Pad TF300T"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 307
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "ASUS Tranfsformer Pad TF300TG"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 308
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "ZTE U930"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 310
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "Sony Tablet S"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 311
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "Iconia A500"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 312
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "Transformer Prime TF201"

    sget-object v5, Lcom/google/android/apps/gmm/map/util/c;->s:Lcom/google/android/apps/gmm/map/util/d;

    .line 313
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    const-string v1, "IS12S"

    new-instance v5, Lcom/google/android/apps/gmm/map/util/d;

    const/16 v6, 0x20

    invoke-direct {v5, v2, v3, v4, v6}, Lcom/google/android/apps/gmm/map/util/d;-><init>(DII)V

    .line 316
    invoke-virtual {v0, v1, v5}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    .line 318
    invoke-virtual {v0}, Lcom/google/b/c/dd;->a()Lcom/google/b/c/dc;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/util/c;->A:Lcom/google/b/c/dc;

    .line 325
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "MANTARAY"

    aput-object v1, v0, v4

    const-string v1, "D2UC"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/gmm/map/util/c;->B:[Ljava/lang/String;

    .line 398
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_4

    move v0, v7

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->l:Z

    .line 404
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_5

    move v0, v7

    :goto_1
    sput-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->m:Z

    .line 412
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_6

    move v0, v4

    :goto_2
    sput-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->n:Z

    .line 443
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 444
    sget-object v0, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 445
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 446
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 448
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x31

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v6, v8

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v6, v8

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v6, v8

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Model "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Product name "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Board name "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", Manufacturer "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 453
    sget-object v0, Lcom/google/android/apps/gmm/map/util/c;->A:Lcom/google/b/c/dc;

    invoke-virtual {v0, v3}, Lcom/google/b/c/dc;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/util/d;

    .line 455
    if-nez v0, :cond_0

    .line 456
    sget-object v0, Lcom/google/android/apps/gmm/map/util/c;->r:Lcom/google/android/apps/gmm/map/util/d;

    .line 461
    :cond_0
    const-string v1, "NEXUS 4"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    const-string v1, "HAMMERHEAD"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 462
    sget-object v0, Lcom/google/android/apps/gmm/map/util/c;->x:Lcom/google/android/apps/gmm/map/util/d;

    move-object v1, v0

    .line 465
    :goto_3
    sget-object v0, Lcom/google/android/apps/gmm/map/util/c;->B:[Ljava/lang/String;

    .line 466
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 467
    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->b:Z

    .line 473
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-gt v0, v2, :cond_9

    iget v0, v1, Lcom/google/android/apps/gmm/map/util/d;->c:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_9

    move v0, v7

    :goto_4
    sput-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->a:Z

    .line 477
    iget v0, v1, Lcom/google/android/apps/gmm/map/util/d;->c:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_a

    move v0, v7

    :goto_5
    sput-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->d:Z

    .line 479
    iget v0, v1, Lcom/google/android/apps/gmm/map/util/d;->c:I

    and-int/lit8 v0, v0, 0x40

    if-nez v0, :cond_b

    move v0, v7

    :goto_6
    sput-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->q:Z

    .line 485
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/map/util/c;->z:Lcom/google/android/apps/gmm/v/ao;

    if-nez v0, :cond_13

    .line 486
    new-instance v0, Lcom/google/android/apps/gmm/v/ao;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/ao;-><init>()V

    .line 487
    sput-object v0, Lcom/google/android/apps/gmm/map/util/c;->z:Lcom/google/android/apps/gmm/v/ao;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ao;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "(.*)NVIDIA(.*)"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 488
    :try_start_1
    sget-object v0, Lcom/google/android/apps/gmm/map/util/c;->z:Lcom/google/android/apps/gmm/v/ao;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ao;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v3, "(.*)TEGRA 3(.*)"

    invoke-virtual {v0, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 494
    :goto_7
    sget-object v3, Lcom/google/android/apps/gmm/map/util/c;->z:Lcom/google/android/apps/gmm/v/ao;

    if-nez v3, :cond_c

    move v3, v4

    .line 495
    :goto_8
    sput-boolean v3, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    .line 496
    iget v3, v1, Lcom/google/android/apps/gmm/map/util/d;->c:I

    and-int/lit8 v3, v3, 0x4

    if-nez v3, :cond_1

    if-eqz v2, :cond_e

    :cond_1
    move v2, v7

    :goto_9
    sput-boolean v2, Lcom/google/android/apps/gmm/map/util/c;->f:Z

    .line 498
    sput-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->g:Z

    .line 500
    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/util/d;->a:D

    sput-wide v2, Lcom/google/android/apps/gmm/map/util/c;->h:D

    .line 501
    iget v0, v1, Lcom/google/android/apps/gmm/map/util/d;->b:I

    sput v0, Lcom/google/android/apps/gmm/map/util/c;->i:I

    .line 502
    instance-of v0, v1, Lcom/google/android/apps/gmm/map/util/e;

    if-eqz v0, :cond_f

    move-object v0, v1

    .line 503
    nop

    nop

    iget v0, v0, Lcom/google/android/apps/gmm/map/util/e;->d:I

    sput v0, Lcom/google/android/apps/gmm/map/util/c;->j:I

    .line 510
    :goto_a
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v12, :cond_10

    move v0, v7

    .line 511
    :goto_b
    sput-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->k:Z

    .line 513
    iget v0, v1, Lcom/google/android/apps/gmm/map/util/d;->c:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_11

    move v0, v7

    :goto_c
    sput-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->o:Z

    .line 514
    iget v0, v1, Lcom/google/android/apps/gmm/map/util/d;->c:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_12

    move v0, v7

    :goto_d
    sput-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->p:Z

    .line 517
    const-string v0, "L"

    const-string v1, "ro.build.version.codename"

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/util/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    :cond_2
    move v4, v7

    .line 519
    :cond_3
    sput-boolean v4, Lcom/google/android/apps/gmm/map/util/c;->c:Z

    .line 521
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->b:Z

    sget-boolean v1, Lcom/google/android/apps/gmm/map/util/c;->a:Z

    sget-boolean v2, Lcom/google/android/apps/gmm/map/util/c;->d:Z

    sget-boolean v3, Lcom/google/android/apps/gmm/map/util/c;->f:Z

    sget-boolean v4, Lcom/google/android/apps/gmm/map/util/c;->g:Z

    sget-wide v6, Lcom/google/android/apps/gmm/map/util/c;->h:D

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v8, 0xef

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "VERTEX_ATTRIB_POINTER_BYTE_WORKAROUND "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " DOES_NOT_SUPPORT_NON_POWER_OF_TWO_TEXTURES "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DOES_SUPPORT_MULTI_ZOOM_ROADS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TEGRA_FORCE_NEW_CONTEXT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TEGRA_DONT_REPARENT_MAP "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " SHAKE_MAGNITUDE_THRESHOLD "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 528
    return-void

    :cond_4
    move v0, v4

    .line 398
    goto/16 :goto_0

    :cond_5
    move v0, v4

    .line 404
    goto/16 :goto_1

    .line 412
    :cond_6
    invoke-static {}, Landroid/media/audiofx/AudioEffect;->queryEffects()[Landroid/media/audiofx/AudioEffect$Descriptor;

    move-result-object v1

    array-length v2, v1

    move v0, v4

    :goto_e
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    iget-object v3, v3, Landroid/media/audiofx/AudioEffect$Descriptor;->type:Ljava/util/UUID;

    sget-object v5, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_LOUDNESS_ENHANCER:Ljava/util/UUID;

    invoke-virtual {v3, v5}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    move v0, v7

    goto/16 :goto_2

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    :cond_8
    move v0, v4

    goto/16 :goto_2

    :cond_9
    move v0, v4

    .line 473
    goto/16 :goto_4

    :cond_a
    move v0, v4

    .line 477
    goto/16 :goto_5

    :cond_b
    move v0, v4

    .line 479
    goto/16 :goto_6

    .line 491
    :catch_0
    move-exception v0

    move v2, v4

    :goto_f
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/gmm/map/util/c;->z:Lcom/google/android/apps/gmm/v/ao;

    move v0, v4

    goto/16 :goto_7

    .line 494
    :cond_c
    sget-object v3, Lcom/google/android/apps/gmm/map/util/c;->z:Lcom/google/android/apps/gmm/v/ao;

    .line 495
    iget v3, v3, Lcom/google/android/apps/gmm/v/ao;->e:I

    if-eqz v3, :cond_d

    move v3, v7

    goto/16 :goto_8

    :cond_d
    move v3, v4

    goto/16 :goto_8

    :cond_e
    move v2, v4

    .line 496
    goto/16 :goto_9

    .line 505
    :cond_f
    sput v4, Lcom/google/android/apps/gmm/map/util/c;->j:I

    goto/16 :goto_a

    :cond_10
    move v0, v4

    .line 510
    goto/16 :goto_b

    :cond_11
    move v0, v4

    .line 513
    goto/16 :goto_c

    :cond_12
    move v0, v4

    .line 514
    goto/16 :goto_d

    .line 491
    :catch_1
    move-exception v0

    goto :goto_f

    :cond_13
    move v0, v4

    move v2, v4

    goto/16 :goto_7

    :cond_14
    move-object v1, v0

    goto/16 :goto_3
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 532
    :try_start_0
    const-class v0, Lcom/google/android/apps/gmm/map/util/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 533
    const-string v1, "android.os.SystemProperties"

    invoke-virtual {v0, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 535
    const-string v1, "get"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 536
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 537
    :catch_0
    move-exception v0

    .line 538
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
