.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;


# static fields
.field private static final a:Z


# instance fields
.field private final b:Z

.field private final c:[F

.field private d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

.field private e:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

.field private f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

.field private g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->a:Z

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/at;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->c:[F

    .line 167
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->b:Z

    .line 169
    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->a:Z

    .line 172
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->b:Z

    if-eqz v0, :cond_0

    .line 175
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    .line 178
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    or-int/lit16 v0, v0, 0x1c2

    .line 199
    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 198
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a(IZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 201
    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 200
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;->a(IZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/NativeVertexDataBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 203
    return-void

    .line 178
    :cond_1
    const/16 v0, 0x600

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/v/ci;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;
    .locals 31
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "[",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/internal/c/cp;",
            "Lcom/google/android/apps/gmm/v/ad;",
            "Lcom/google/android/apps/gmm/v/bp;",
            "Lcom/google/android/apps/gmm/v/ci;",
            "Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;"
        }
    .end annotation

    .prologue
    .line 128
    new-instance v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/at;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/at;-><init>()V

    .line 129
    const/4 v5, 0x0

    .line 130
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->b()V

    .line 131
    :cond_0
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 132
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/m;

    .line 133
    instance-of v3, v2, Lcom/google/android/apps/gmm/map/internal/c/cg;

    if-eqz v3, :cond_1

    move-object v3, v2

    check-cast v3, Lcom/google/android/apps/gmm/map/internal/c/cg;

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/internal/c/cg;->b:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v7, v4, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v7, v7

    if-nez v7, :cond_5

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v4

    :goto_0
    iget-object v7, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v7, :cond_6

    const/4 v4, 0x0

    :goto_1
    if-gtz v4, :cond_7

    const/4 v3, 0x1

    :goto_2
    if-nez v3, :cond_9

    :cond_1
    move-object/from16 v24, v2

    .line 143
    :goto_3
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->c()V

    .line 145
    new-instance v27, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;

    .line 146
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/at;)V

    .line 147
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v28

    .line 149
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a()Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;

    move-result-object v29

    .line 151
    :goto_4
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 152
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v2

    .line 153
    move-object/from16 v0, v24

    if-eq v2, v0, :cond_18

    move-object v13, v2

    .line 155
    check-cast v13, Lcom/google/android/apps/gmm/map/internal/c/cg;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    int-to-float v12, v2

    iget-object v2, v13, Lcom/google/android/apps/gmm/map/internal/c/cg;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, v28

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v28

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    const/high16 v4, 0x43800000    # 256.0f

    div-float/2addr v3, v4

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v4, v4

    const/4 v5, 0x6

    if-gt v4, v5, :cond_b

    move-object v3, v2

    :goto_5
    move-object/from16 v0, v28

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v28

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v28

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v6, v2, v5

    iget-object v7, v13, Lcom/google/android/apps/gmm/map/internal/c/cg;->b:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v2, v2

    if-nez v2, :cond_f

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v2

    :goto_6
    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v5, :cond_10

    const/4 v5, 0x0

    :goto_7
    if-lez v5, :cond_4

    const/4 v5, 0x0

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v5, v8, v5

    iget v8, v5, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    const/high16 v5, 0x3f000000    # 0.5f

    move-object/from16 v0, v27

    iget-boolean v9, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->b:Z

    if-nez v9, :cond_2

    const v5, 0x3f2aaaab

    :cond_2
    int-to-float v6, v6

    mul-float/2addr v6, v8

    mul-float/2addr v5, v6

    const/high16 v6, 0x43800000    # 256.0f

    div-float v15, v5, v6

    invoke-static {v12}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v5, v6

    move-object/from16 v0, v27

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->b:Z

    if-nez v6, :cond_3

    add-float/2addr v15, v5

    :cond_3
    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->a(Lcom/google/android/apps/gmm/map/internal/c/bi;)I

    move-result v9

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v5, :cond_11

    const/4 v2, 0x0

    :goto_8
    const/4 v5, 0x1

    if-le v2, v5, :cond_12

    const/4 v2, 0x0

    const/4 v10, 0x1

    move/from16 v25, v2

    move/from16 v26, v9

    :goto_9
    iget v2, v13, Lcom/google/android/apps/gmm/map/internal/c/cg;->c:I

    const/4 v5, 0x1

    and-int/2addr v2, v5

    if-eqz v2, :cond_13

    const/4 v2, 0x1

    :goto_a
    if-eqz v2, :cond_14

    sget-object v11, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    :goto_b
    sget-boolean v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->a:Z

    move-object/from16 v0, v27

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->b:Z

    if-eqz v2, :cond_17

    const/4 v2, 0x0

    iget-object v5, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v6, 0x0

    iget-object v8, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v8, v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {v2, v6, v8}, Lcom/google/android/apps/gmm/shared/c/s;->a(III)I

    move-result v2

    aget-object v2, v5, v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v2, v2, v10

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    const/4 v5, 0x1

    iget-object v6, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v8, 0x0

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v7, v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {v5, v8, v7}, Lcom/google/android/apps/gmm/shared/c/s;->a(III)I

    move-result v5

    aget-object v5, v6, v5

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v5, v5, v10

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v5, v6

    move-object/from16 v0, v27

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->c:[F

    const/4 v7, 0x0

    aput v2, v6, v7

    move-object/from16 v0, v27

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->c:[F

    const/4 v6, 0x1

    aput v5, v2, v6

    const/high16 v2, 0x40000000    # 2.0f

    mul-float v5, v15, v2

    iget v2, v13, Lcom/google/android/apps/gmm/map/internal/c/cg;->c:I

    const/4 v6, 0x1

    and-int/2addr v2, v6

    if-eqz v2, :cond_15

    const/4 v2, 0x1

    :goto_c
    if-eqz v2, :cond_16

    const/high16 v2, -0x40800000    # -1.0f

    :goto_d
    mul-float/2addr v5, v2

    move-object/from16 v0, v27

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v27

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->c:[F

    move-object/from16 v2, v29

    invoke-virtual/range {v2 .. v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/internal/vector/gl/k;ZZIILcom/google/android/apps/gmm/map/internal/vector/gl/c;Z[F)I

    const/4 v2, -0x1

    move/from16 v0, v25

    if-eq v0, v2, :cond_4

    move-object/from16 v0, v27

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v27

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->c:[F

    move-object/from16 v2, v29

    move/from16 v9, v26

    move/from16 v10, v25

    invoke-virtual/range {v2 .. v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/internal/vector/gl/k;ZZIILcom/google/android/apps/gmm/map/internal/vector/gl/c;Z[F)I

    .line 158
    :cond_4
    :goto_e
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    goto/16 :goto_4

    .line 133
    :cond_5
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v7, 0x0

    aget-object v4, v4, v7

    goto/16 :goto_0

    :cond_6
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v4, v4

    goto/16 :goto_1

    :cond_7
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/cg;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/b/a/ab;)I

    move-result v4

    iget v7, v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/at;->a:I

    add-int/2addr v7, v4

    const/16 v8, 0x1000

    if-le v7, v8, :cond_8

    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_8
    iget v7, v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/at;->a:I

    add-int/2addr v4, v7

    iput v4, v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/at;->a:I

    iget v4, v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/at;->b:I

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->b(Lcom/google/android/apps/gmm/map/b/a/ab;)I

    move-result v3

    add-int/2addr v3, v4

    iput v3, v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/at;->b:I

    const/4 v3, 0x1

    goto/16 :goto_2

    .line 137
    :cond_9
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/c/m;->j()[I

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_f
    if-ge v2, v4, :cond_0

    aget v7, v3, v2

    .line 138
    if-ltz v7, :cond_a

    move-object/from16 v0, p1

    array-length v8, v0

    if-ge v7, v8, :cond_a

    .line 139
    aget-object v7, p1, v7

    move-object/from16 v0, p9

    invoke-interface {v0, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 137
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_f

    .line 155
    :cond_b
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v4, v4

    div-int/lit8 v14, v4, 0x3

    new-array v11, v14, [Z

    const/4 v4, 0x0

    const/4 v5, 0x1

    aput-boolean v5, v11, v4

    add-int/lit8 v4, v14, -0x1

    const/4 v5, 0x1

    aput-boolean v5, v11, v4

    mul-float/2addr v3, v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    add-int/lit8 v6, v14, -0x1

    new-instance v7, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v9, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v10, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v10}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(FIIILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;[Z)I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    if-ne v3, v14, :cond_c

    move-object v3, v2

    goto/16 :goto_5

    :cond_c
    mul-int/lit8 v3, v3, 0x3

    new-array v5, v3, [I

    const/4 v4, 0x0

    const/4 v3, 0x0

    move/from16 v30, v3

    move v3, v4

    move/from16 v4, v30

    :goto_10
    if-ge v4, v14, :cond_e

    aget-boolean v6, v11, v4

    if-eqz v6, :cond_d

    mul-int/lit8 v6, v4, 0x3

    add-int/lit8 v7, v3, 0x1

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v9, v6, 0x1

    aget v6, v8, v6

    aput v6, v5, v3

    add-int/lit8 v6, v7, 0x1

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v8, v9, 0x1

    aget v3, v3, v9

    aput v3, v5, v7

    add-int/lit8 v3, v6, 0x1

    iget-object v7, v2, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    aget v7, v7, v8

    aput v7, v5, v6

    :cond_d
    add-int/lit8 v4, v4, 0x1

    goto :goto_10

    :cond_e
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v2, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;-><init>([I)V

    move-object v3, v2

    goto/16 :goto_5

    :cond_f
    iget-object v2, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v5, 0x0

    aget-object v2, v2, v5

    goto/16 :goto_6

    :cond_10
    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v5, v5

    goto/16 :goto_7

    :cond_11
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v2, v2

    goto/16 :goto_8

    :cond_12
    const/4 v5, -0x1

    const/4 v2, -0x1

    const/4 v10, 0x0

    move/from16 v25, v2

    move/from16 v26, v5

    goto/16 :goto_9

    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_a

    :cond_14
    sget-object v11, Lcom/google/android/apps/gmm/map/internal/vector/gl/c;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/c;

    goto/16 :goto_b

    :cond_15
    const/4 v2, 0x0

    goto/16 :goto_c

    :cond_16
    const/high16 v2, 0x3f800000    # 1.0f

    goto/16 :goto_d

    :cond_17
    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v13, v29

    move-object v14, v3

    move-object/from16 v16, v4

    move/from16 v20, v9

    move/from16 v21, v10

    move-object/from16 v22, v11

    move/from16 v23, v12

    invoke-virtual/range {v13 .. v23}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/b/a/ab;FLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/internal/vector/gl/k;ZZIILcom/google/android/apps/gmm/map/internal/vector/gl/c;Z)I

    const/4 v2, -0x1

    move/from16 v0, v25

    if-eq v0, v2, :cond_4

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v13, v29

    move-object v14, v3

    move-object/from16 v16, v4

    move/from16 v20, v26

    move/from16 v21, v25

    move-object/from16 v22, v11

    move/from16 v23, v12

    invoke-virtual/range {v13 .. v23}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/b/a/ab;FLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/internal/vector/gl/k;ZZIILcom/google/android/apps/gmm/map/internal/vector/gl/c;Z)I

    goto/16 :goto_e

    .line 160
    :cond_18
    move-object/from16 v0, v27

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b()I

    move-result v2

    if-lez v2, :cond_1a

    move-object/from16 v0, v27

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v3, 0x5

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v2

    move-object/from16 v0, v27

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    new-instance v2, Lcom/google/android/apps/gmm/map/t/ar;

    sget-object v3, Lcom/google/android/apps/gmm/map/t/l;->a:Lcom/google/android/apps/gmm/map/t/l;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v0, v4, v5}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xf

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Traffic Outline"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    move-object/from16 v0, p7

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/r;

    const/4 v4, 0x1

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a:[Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v4, 0x207

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/v/bk;->a(I)Lcom/google/android/apps/gmm/v/bm;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v4, 0x302

    const/16 v5, 0x303

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    move-object/from16 v0, p8

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x0

    iget-boolean v4, v2, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v4, :cond_19

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_19
    int-to-byte v3, v3

    iput-byte v3, v2, Lcom/google/android/apps/gmm/v/aa;->w:B

    :cond_1a
    move-object/from16 v0, v27

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    const/4 v2, 0x0

    move-object/from16 v0, v27

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->g:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-object/from16 v0, v27

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b()I

    move-result v2

    if-lez v2, :cond_1c

    move-object/from16 v0, v27

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v3, 0x5

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v2

    move-object/from16 v0, v27

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    new-instance v2, Lcom/google/android/apps/gmm/map/t/ar;

    sget-object v3, Lcom/google/android/apps/gmm/map/t/l;->b:Lcom/google/android/apps/gmm/map/t/l;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v0, v4, v5}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xc

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Traffic Fill"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    move-object/from16 v0, p7

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/r;

    const/4 v4, 0x1

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a:[Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v4, 0x207

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/v/bk;->a(I)Lcom/google/android/apps/gmm/v/bm;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v4, 0x302

    const/16 v5, 0x303

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    move-object/from16 v0, p8

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x0

    iget-boolean v4, v2, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v4, :cond_1b

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1b
    int-to-byte v3, v3

    iput-byte v3, v2, Lcom/google/android/apps/gmm/v/aa;->w:B

    :cond_1c
    move-object/from16 v0, v27

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    const/4 v2, 0x0

    move-object/from16 v0, v27

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 162
    return-object v27

    :cond_1d
    move-object/from16 v24, v5

    goto/16 :goto_3
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-nez v2, :cond_1

    .line 404
    :goto_1
    add-int/2addr v0, v1

    return v0

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/as;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    .line 404
    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    goto :goto_1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 409
    const/16 v0, 0xb8

    return v0
.end method
