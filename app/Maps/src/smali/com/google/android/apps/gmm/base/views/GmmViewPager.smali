.class public abstract Lcom/google/android/apps/gmm/base/views/GmmViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/a/a;


# instance fields
.field private final h:Landroid/database/DataSetObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/base/views/y;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/y;-><init>(Lcom/google/android/apps/gmm/base/views/GmmViewPager;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->h:Landroid/database/DataSetObserver;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/base/views/y;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/y;-><init>(Lcom/google/android/apps/gmm/base/views/GmmViewPager;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->h:Landroid/database/DataSetObserver;

    .line 37
    return-void
.end method


# virtual methods
.method public e()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public final setAdapter(Landroid/support/v4/view/ag;)V
    .locals 2

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 45
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->a()Landroid/support/v4/view/ag;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    .line 47
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->h:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ag;->b(Landroid/database/DataSetObserver;)V

    .line 50
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/ag;)V

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->h:Landroid/database/DataSetObserver;

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ag;->a(Landroid/database/DataSetObserver;)V

    .line 53
    return-void
.end method
