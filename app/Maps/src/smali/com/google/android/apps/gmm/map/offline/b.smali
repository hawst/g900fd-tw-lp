.class final Lcom/google/android/apps/gmm/map/offline/b;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static a(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/d/c/a/f;Landroid/util/DisplayMetrics;Landroid/graphics/Rect;)Lcom/google/maps/b/n;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/b/n;",
            ">;",
            "Lcom/google/android/apps/gmm/map/internal/d/c/a/f;",
            "Landroid/util/DisplayMetrics;",
            "Landroid/graphics/Rect;",
            ")",
            "Lcom/google/maps/b/n;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/16 v9, 0x1000

    const/16 v8, 0x800

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 46
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 47
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v4

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v0, v5

    goto :goto_0

    .line 49
    :cond_2
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v1, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/n;

    .line 50
    if-nez v1, :cond_4

    :cond_3
    :goto_2
    move-object v1, v0

    .line 51
    goto :goto_1

    .line 50
    :cond_4
    if-nez v0, :cond_5

    move-object v0, v1

    goto :goto_2

    :cond_5
    iget v3, v1, Lcom/google/maps/b/n;->g:I

    invoke-static {v3}, Lcom/google/maps/b/l;->a(I)Lcom/google/maps/b/l;

    move-result-object v3

    if-nez v3, :cond_6

    sget-object v3, Lcom/google/maps/b/l;->b:Lcom/google/maps/b/l;

    :cond_6
    sget-object v7, Lcom/google/maps/b/l;->d:Lcom/google/maps/b/l;

    if-ne v3, v7, :cond_8

    iget v3, v0, Lcom/google/maps/b/n;->g:I

    invoke-static {v3}, Lcom/google/maps/b/l;->a(I)Lcom/google/maps/b/l;

    move-result-object v3

    if-nez v3, :cond_7

    sget-object v3, Lcom/google/maps/b/l;->b:Lcom/google/maps/b/l;

    :cond_7
    sget-object v7, Lcom/google/maps/b/l;->d:Lcom/google/maps/b/l;

    if-eq v3, v7, :cond_a

    move-object v0, v1

    goto :goto_2

    :cond_8
    iget v3, v0, Lcom/google/maps/b/n;->g:I

    invoke-static {v3}, Lcom/google/maps/b/l;->a(I)Lcom/google/maps/b/l;

    move-result-object v3

    if-nez v3, :cond_9

    sget-object v3, Lcom/google/maps/b/l;->b:Lcom/google/maps/b/l;

    :cond_9
    sget-object v7, Lcom/google/maps/b/l;->d:Lcom/google/maps/b/l;

    if-eq v3, v7, :cond_3

    :cond_a
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->a:Ljava/lang/String;

    if-eqz v3, :cond_25

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->a:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_25

    invoke-static {v1, p3}, Lcom/google/android/apps/gmm/map/offline/b;->a(Lcom/google/maps/b/n;Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-static {v0, p3}, Lcom/google/android/apps/gmm/map/offline/b;->a(Lcom/google/maps/b/n;Landroid/graphics/Rect;)Z

    move-result v3

    if-nez v3, :cond_f

    move-object v3, v1

    :goto_3
    if-nez v3, :cond_3a

    iget v3, v1, Lcom/google/maps/b/n;->j:I

    invoke-static {v3}, Lcom/google/maps/b/h;->a(I)Lcom/google/maps/b/h;

    move-result-object v3

    if-nez v3, :cond_b

    sget-object v3, Lcom/google/maps/b/h;->a:Lcom/google/maps/b/h;

    :cond_b
    iget v3, v3, Lcom/google/maps/b/h;->j:I

    iget v7, p2, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v3, v7, :cond_31

    iget v3, v0, Lcom/google/maps/b/n;->j:I

    invoke-static {v3}, Lcom/google/maps/b/h;->a(I)Lcom/google/maps/b/h;

    move-result-object v3

    if-nez v3, :cond_c

    sget-object v3, Lcom/google/maps/b/h;->a:Lcom/google/maps/b/h;

    :cond_c
    iget v3, v3, Lcom/google/maps/b/h;->j:I

    iget v7, p2, Landroid/util/DisplayMetrics;->densityDpi:I

    if-eq v3, v7, :cond_33

    move-object v0, v1

    :cond_d
    :goto_4
    if-nez v0, :cond_3

    move-object v0, v1

    goto :goto_2

    :cond_e
    invoke-static {v0, p3}, Lcom/google/android/apps/gmm/map/offline/b;->a(Lcom/google/maps/b/n;Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_f

    move-object v3, v0

    goto :goto_3

    :cond_f
    invoke-static {v1, p1}, Lcom/google/android/apps/gmm/map/offline/b;->a(Lcom/google/maps/b/n;Lcom/google/android/apps/gmm/map/internal/d/c/a/f;)Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/map/offline/b;->a(Lcom/google/maps/b/n;Lcom/google/android/apps/gmm/map/internal/d/c/a/f;)Z

    move-result v3

    if-nez v3, :cond_11

    move-object v3, v1

    goto :goto_3

    :cond_10
    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/map/offline/b;->a(Lcom/google/maps/b/n;Lcom/google/android/apps/gmm/map/internal/d/c/a/f;)Z

    move-result v3

    if-eqz v3, :cond_11

    move-object v3, v0

    goto :goto_3

    :cond_11
    iget v3, v1, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x800

    if-ne v3, v8, :cond_13

    move v3, v4

    :goto_5
    if-nez v3, :cond_12

    iget v3, v1, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x1000

    if-ne v3, v9, :cond_14

    move v3, v4

    :goto_6
    if-eqz v3, :cond_20

    :cond_12
    iget v3, v0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x800

    if-ne v3, v8, :cond_15

    move v3, v4

    :goto_7
    if-nez v3, :cond_17

    iget v3, v0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x1000

    if-ne v3, v9, :cond_16

    move v3, v4

    :goto_8
    if-nez v3, :cond_17

    move-object v3, v1

    goto :goto_3

    :cond_13
    move v3, v5

    goto :goto_5

    :cond_14
    move v3, v5

    goto :goto_6

    :cond_15
    move v3, v5

    goto :goto_7

    :cond_16
    move v3, v5

    goto :goto_8

    :cond_17
    iget v3, v1, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x800

    if-ne v3, v8, :cond_18

    move v3, v4

    :goto_9
    if-eqz v3, :cond_1b

    iget v3, v0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x800

    if-ne v3, v8, :cond_19

    move v3, v4

    :goto_a
    if-eqz v3, :cond_1b

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->a:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    iget v7, v1, Lcom/google/maps/b/n;->m:I

    if-ge v3, v7, :cond_1b

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->a:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    iget v7, v0, Lcom/google/maps/b/n;->m:I

    if-ge v3, v7, :cond_1b

    iget v3, v1, Lcom/google/maps/b/n;->n:I

    iget v7, v0, Lcom/google/maps/b/n;->n:I

    if-gt v3, v7, :cond_1a

    move-object v3, v1

    goto/16 :goto_3

    :cond_18
    move v3, v5

    goto :goto_9

    :cond_19
    move v3, v5

    goto :goto_a

    :cond_1a
    move-object v3, v0

    goto/16 :goto_3

    :cond_1b
    iget v3, v1, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x1000

    if-ne v3, v9, :cond_1c

    move v3, v4

    :goto_b
    if-eqz v3, :cond_1f

    iget v3, v0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x1000

    if-ne v3, v9, :cond_1d

    move v3, v4

    :goto_c
    if-eqz v3, :cond_1f

    iget v3, v1, Lcom/google/maps/b/n;->n:I

    iget v7, v0, Lcom/google/maps/b/n;->n:I

    if-lt v3, v7, :cond_1e

    move-object v3, v1

    goto/16 :goto_3

    :cond_1c
    move v3, v5

    goto :goto_b

    :cond_1d
    move v3, v5

    goto :goto_c

    :cond_1e
    move-object v3, v0

    goto/16 :goto_3

    :cond_1f
    move-object v3, v2

    goto/16 :goto_3

    :cond_20
    iget v3, v0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x800

    if-ne v3, v8, :cond_22

    move v3, v4

    :goto_d
    if-nez v3, :cond_21

    iget v3, v0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x800

    if-ne v3, v8, :cond_23

    move v3, v4

    :goto_e
    if-eqz v3, :cond_24

    :cond_21
    move-object v3, v0

    goto/16 :goto_3

    :cond_22
    move v3, v5

    goto :goto_d

    :cond_23
    move v3, v5

    goto :goto_e

    :cond_24
    move-object v3, v2

    goto/16 :goto_3

    :cond_25
    iget v3, v1, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x800

    if-ne v3, v8, :cond_27

    move v3, v4

    :goto_f
    if-nez v3, :cond_2b

    iget v3, v1, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x1000

    if-ne v3, v9, :cond_28

    move v3, v4

    :goto_10
    if-nez v3, :cond_2b

    iget v3, v0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x800

    if-ne v3, v8, :cond_29

    move v3, v4

    :goto_11
    if-nez v3, :cond_26

    iget v3, v0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x1000

    if-ne v3, v9, :cond_2a

    move v3, v4

    :goto_12
    if-eqz v3, :cond_2e

    :cond_26
    move-object v3, v1

    goto/16 :goto_3

    :cond_27
    move v3, v5

    goto :goto_f

    :cond_28
    move v3, v5

    goto :goto_10

    :cond_29
    move v3, v5

    goto :goto_11

    :cond_2a
    move v3, v5

    goto :goto_12

    :cond_2b
    iget v3, v0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x800

    if-ne v3, v8, :cond_2c

    move v3, v4

    :goto_13
    if-nez v3, :cond_2e

    iget v3, v0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v3, v3, 0x1000

    if-ne v3, v9, :cond_2d

    move v3, v4

    :goto_14
    if-nez v3, :cond_2e

    move-object v3, v0

    goto/16 :goto_3

    :cond_2c
    move v3, v5

    goto :goto_13

    :cond_2d
    move v3, v5

    goto :goto_14

    :cond_2e
    iget v3, v1, Lcom/google/maps/b/n;->m:I

    iget v7, v0, Lcom/google/maps/b/n;->m:I

    if-ge v3, v7, :cond_2f

    move-object v3, v1

    goto/16 :goto_3

    :cond_2f
    iget v3, v1, Lcom/google/maps/b/n;->m:I

    iget v7, v0, Lcom/google/maps/b/n;->m:I

    if-le v3, v7, :cond_30

    move-object v3, v0

    goto/16 :goto_3

    :cond_30
    move-object v3, v2

    goto/16 :goto_3

    :cond_31
    iget v3, v0, Lcom/google/maps/b/n;->j:I

    invoke-static {v3}, Lcom/google/maps/b/h;->a(I)Lcom/google/maps/b/h;

    move-result-object v3

    if-nez v3, :cond_32

    sget-object v3, Lcom/google/maps/b/h;->a:Lcom/google/maps/b/h;

    :cond_32
    iget v3, v3, Lcom/google/maps/b/h;->j:I

    iget v7, p2, Landroid/util/DisplayMetrics;->densityDpi:I

    if-eq v3, v7, :cond_d

    :cond_33
    iget v3, v1, Lcom/google/maps/b/n;->j:I

    invoke-static {v3}, Lcom/google/maps/b/h;->a(I)Lcom/google/maps/b/h;

    move-result-object v3

    if-nez v3, :cond_34

    sget-object v3, Lcom/google/maps/b/h;->a:Lcom/google/maps/b/h;

    :cond_34
    iget v7, v3, Lcom/google/maps/b/h;->j:I

    iget v3, v0, Lcom/google/maps/b/n;->j:I

    invoke-static {v3}, Lcom/google/maps/b/h;->a(I)Lcom/google/maps/b/h;

    move-result-object v3

    if-nez v3, :cond_35

    sget-object v3, Lcom/google/maps/b/h;->a:Lcom/google/maps/b/h;

    :cond_35
    iget v3, v3, Lcom/google/maps/b/h;->j:I

    if-le v7, v3, :cond_36

    move-object v0, v1

    goto/16 :goto_4

    :cond_36
    iget v3, v1, Lcom/google/maps/b/n;->j:I

    invoke-static {v3}, Lcom/google/maps/b/h;->a(I)Lcom/google/maps/b/h;

    move-result-object v3

    if-nez v3, :cond_37

    sget-object v3, Lcom/google/maps/b/h;->a:Lcom/google/maps/b/h;

    :cond_37
    iget v7, v3, Lcom/google/maps/b/h;->j:I

    iget v3, v0, Lcom/google/maps/b/n;->j:I

    invoke-static {v3}, Lcom/google/maps/b/h;->a(I)Lcom/google/maps/b/h;

    move-result-object v3

    if-nez v3, :cond_38

    sget-object v3, Lcom/google/maps/b/h;->a:Lcom/google/maps/b/h;

    :cond_38
    iget v3, v3, Lcom/google/maps/b/h;->j:I

    if-lt v7, v3, :cond_d

    move-object v0, v2

    goto/16 :goto_4

    .line 52
    :cond_39
    return-object v1

    :cond_3a
    move-object v0, v3

    goto/16 :goto_4
.end method

.method private static a(Lcom/google/maps/b/n;Landroid/graphics/Rect;)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 254
    iget v2, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    move v2, v1

    :goto_0
    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    move v2, v1

    :goto_1
    if-nez v2, :cond_3

    .line 263
    :cond_0
    :goto_2
    return v0

    :cond_1
    move v2, v0

    .line 254
    goto :goto_0

    :cond_2
    move v2, v0

    goto :goto_1

    .line 257
    :cond_3
    iget v2, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_6

    move v2, v1

    :goto_3
    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/maps/b/n;->b:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-gt v2, v3, :cond_0

    .line 260
    :cond_4
    iget v2, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_7

    move v2, v1

    :goto_4
    if-eqz v2, :cond_5

    iget v2, p0, Lcom/google/maps/b/n;->c:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-lt v2, v3, :cond_0

    :cond_5
    move v0, v1

    .line 263
    goto :goto_2

    :cond_6
    move v2, v0

    .line 257
    goto :goto_3

    :cond_7
    move v2, v0

    .line 260
    goto :goto_4
.end method

.method private static a(Lcom/google/maps/b/n;Lcom/google/android/apps/gmm/map/internal/d/c/a/f;)Z
    .locals 6

    .prologue
    const/16 v5, 0x1000

    const/16 v4, 0x800

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 273
    iget v2, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v2, v2, 0x800

    if-ne v2, v4, :cond_1

    move v2, v1

    :goto_0
    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v2, v2, 0x1000

    if-ne v2, v5, :cond_2

    move v2, v1

    :goto_1
    if-nez v2, :cond_3

    .line 288
    :cond_0
    :goto_2
    return v0

    :cond_1
    move v2, v0

    .line 273
    goto :goto_0

    :cond_2
    move v2, v0

    goto :goto_1

    .line 276
    :cond_3
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 280
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    .line 281
    iget v2, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v2, v2, 0x800

    if-ne v2, v4, :cond_6

    move v2, v1

    :goto_3
    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/maps/b/n;->m:I

    if-gt v2, v3, :cond_0

    .line 284
    :cond_4
    iget v2, p0, Lcom/google/maps/b/n;->a:I

    and-int/lit16 v2, v2, 0x1000

    if-ne v2, v5, :cond_7

    move v2, v1

    :goto_4
    if-eqz v2, :cond_5

    iget v2, p0, Lcom/google/maps/b/n;->n:I

    if-lt v2, v3, :cond_0

    :cond_5
    move v0, v1

    .line 288
    goto :goto_2

    :cond_6
    move v2, v0

    .line 281
    goto :goto_3

    :cond_7
    move v2, v0

    .line 284
    goto :goto_4
.end method
