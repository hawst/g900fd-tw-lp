.class public Lcom/google/android/apps/gmm/map/indoor/c/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/indoor/c/j;

.field final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/c;",
            ">;>;"
        }
    .end annotation
.end field

.field final c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field volatile d:J

.field private final e:Lcom/google/android/apps/gmm/map/util/b/g;

.field private f:Lcom/google/android/apps/gmm/map/indoor/d/c;

.field private g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/indoor/c/j;Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->b:Ljava/util/Map;

    .line 55
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->c:Ljava/util/Collection;

    .line 73
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->a:Lcom/google/android/apps/gmm/map/indoor/c/j;

    .line 74
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 75
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/f/o;)Lcom/google/android/apps/gmm/map/indoor/d/c;
    .locals 10
    .param p1    # Lcom/google/android/apps/gmm/map/f/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const-wide/high16 v8, 0x4033000000000000L    # 19.0

    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    .line 211
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    const/high16 v1, 0x41900000    # 18.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 212
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/map/indoor/d/c;->d:Lcom/google/android/apps/gmm/map/indoor/d/c;

    .line 237
    :cond_1
    return-object v1

    .line 215
    :cond_2
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 217
    invoke-interface {v4, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 220
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    float-to-double v0, v0

    cmpl-double v6, v0, v8

    if-lez v6, :cond_8

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    sub-double/2addr v0, v8

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    div-double v0, v2, v0

    :goto_1
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    invoke-static {v5, v0}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/y;I)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v5

    .line 222
    const/4 v2, 0x0

    .line 223
    sget-object v1, Lcom/google/android/apps/gmm/map/indoor/d/c;->d:Lcom/google/android/apps/gmm/map/indoor/d/c;

    .line 225
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/c;

    .line 226
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/d/c;->b:Lcom/google/android/apps/gmm/map/b/a/d;

    .line 227
    invoke-interface {v3, v5}, Lcom/google/android/apps/gmm/map/b/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 228
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/d/c;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/d/c;->a:Lcom/google/android/apps/gmm/map/indoor/d/a;

    if-eqz v3, :cond_6

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/d/c;->a:Lcom/google/android/apps/gmm/map/indoor/d/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/d/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v3, :cond_6

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/d/c;->a:Lcom/google/android/apps/gmm/map/indoor/d/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/d/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/d/c;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    :cond_4
    :goto_3
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/d/c;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v6, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->d(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    .line 229
    sget-object v6, Lcom/google/android/apps/gmm/map/indoor/d/c;->d:Lcom/google/android/apps/gmm/map/indoor/d/c;

    if-eq v1, v6, :cond_5

    cmpg-float v6, v3, v2

    if-gez v6, :cond_7

    :cond_5
    move v1, v3

    :goto_4
    move v2, v1

    move-object v1, v0

    .line 235
    goto :goto_2

    .line 228
    :cond_6
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/d/c;->b:Lcom/google/android/apps/gmm/map/b/a/d;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/b/a/d;->c()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v3

    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v3, v6}, Lcom/google/android/apps/gmm/map/b/a/ae;->b(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/d/c;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_3

    :cond_7
    move-object v0, v1

    move v1, v2

    goto :goto_4

    :cond_8
    move-wide v0, v2

    goto :goto_1
.end method

.method private b()V
    .locals 6

    .prologue
    .line 252
    invoke-static {}, Lcom/google/b/c/dn;->h()Lcom/google/b/c/dp;

    move-result-object v1

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 254
    invoke-virtual {v1, v0}, Lcom/google/b/c/dp;->b(Ljava/lang/Iterable;)Lcom/google/b/c/dp;

    goto :goto_0

    .line 256
    :cond_0
    invoke-virtual {v1}, Lcom/google/b/c/dp;->a()Lcom/google/b/c/dn;

    move-result-object v0

    .line 258
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->g:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lcom/google/b/c/dn;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 259
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v2, Lcom/google/android/apps/gmm/map/indoor/b/d;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/indoor/b/d;-><init>(Ljava/util/Collection;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 262
    :cond_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->g:Ljava/util/Set;

    .line 263
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->a:Lcom/google/android/apps/gmm/map/indoor/c/j;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/c;->a:Lcom/google/android/apps/gmm/map/indoor/d/a;

    if-eqz v0, :cond_2

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/indoor/c/j;->b:Lcom/google/android/apps/gmm/map/util/a/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/a/j;->a:Lcom/google/android/apps/gmm/map/util/a/a;

    check-cast v0, Lcom/google/android/apps/gmm/map/util/a/l;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/util/a/l;->a(Ljava/util/Collection;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/indoor/c/j;->c:Lcom/google/android/apps/gmm/map/util/a/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/a/j;->a:Lcom/google/android/apps/gmm/map/util/a/a;

    check-cast v0, Lcom/google/android/apps/gmm/map/util/a/l;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/util/a/l;->a(Ljava/util/Collection;)V

    .line 264
    return-void
.end method


# virtual methods
.method declared-synchronized a()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 134
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->b:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 135
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 138
    :cond_1
    monitor-exit p0

    return-object v1
.end method

.method declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/Collection;Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/c;",
            ">;",
            "Lcom/google/android/apps/gmm/map/f/o;",
            ")V"
        }
    .end annotation

    .prologue
    .line 125
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    invoke-direct {p0, p3}, Lcom/google/android/apps/gmm/map/indoor/c/a;->a(Lcom/google/android/apps/gmm/map/f/o;)Lcom/google/android/apps/gmm/map/indoor/d/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->f:Lcom/google/android/apps/gmm/map/indoor/d/c;

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v2, Lcom/google/android/apps/gmm/map/indoor/b/a;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/indoor/b/a;-><init>(Lcom/google/android/apps/gmm/map/indoor/d/c;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->f:Lcom/google/android/apps/gmm/map/indoor/d/c;

    .line 127
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/indoor/c/a;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    monitor-exit p0

    return-void

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Ljava/util/Collection;Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Lcom/google/android/apps/gmm/map/f/o;",
            ")V"
        }
    .end annotation

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->c:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-interface {p1, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->a:Lcom/google/android/apps/gmm/map/indoor/c/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/c/j;->a:Lcom/google/android/apps/gmm/map/util/a/l;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/l;->a(Ljava/util/Collection;)V

    .line 119
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/indoor/c/a;->a(Lcom/google/android/apps/gmm/map/f/o;)Lcom/google/android/apps/gmm/map/indoor/d/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->f:Lcom/google/android/apps/gmm/map/indoor/d/c;

    if-eq v0, v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v2, Lcom/google/android/apps/gmm/map/indoor/b/a;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/indoor/b/a;-><init>(Lcom/google/android/apps/gmm/map/indoor/d/c;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    :cond_3
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/a;->f:Lcom/google/android/apps/gmm/map/indoor/d/c;

    .line 120
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/indoor/c/a;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121
    monitor-exit p0

    return-void
.end method
