.class public Lcom/google/android/apps/gmm/navigation/navui/a/e;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/navigation/navui/d/f;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method

.method private a(I)Lcom/google/android/libraries/curvular/cs;
    .locals 12

    .prologue
    const/4 v9, 0x1

    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    const v8, 0xffffff

    .line 164
    const/4 v0, 0x5

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const-wide v2, 0x3fe147ae147ae148L    # 0.54

    .line 165
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->f:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const-wide/high16 v2, 0x4038000000000000L    # 24.0

    .line 166
    new-instance v4, Lcom/google/android/libraries/curvular/b;

    invoke-static {v2, v3}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_1

    double-to-int v2, v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v8

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v4, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v9

    const/4 v2, 0x2

    .line 167
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/libraries/curvular/bb;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/bb;-><init>(Lcom/google/android/libraries/curvular/ay;)V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/a/e;->p()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/libraries/curvular/br;->b(Lcom/google/android/libraries/curvular/am;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    const-wide/high16 v4, 0x4056000000000000L    # 88.0

    .line 168
    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v4, v5}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_2

    double-to-int v4, v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v6, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    const-wide/high16 v6, 0x4048000000000000L    # 48.0

    .line 169
    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_3

    double-to-int v6, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v8

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v5, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    .line 167
    invoke-static {v3, v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x3

    .line 171
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->ai:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x4

    sget v2, Lcom/google/android/apps/gmm/d;->aR:I

    .line 172
    new-instance v3, Lcom/google/android/apps/gmm/base/k/aq;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    invoke-direct {v3, p1, v2}, Lcom/google/android/apps/gmm/base/k/aq;-><init>(ILcom/google/android/libraries/curvular/aq;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    .line 164
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.ImageView"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    .line 166
    :cond_1
    mul-double/2addr v2, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v8

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x11

    iput v2, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    .line 168
    :cond_2
    mul-double/2addr v4, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x11

    iput v4, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    .line 169
    :cond_3
    mul-double/2addr v6, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v8

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v0, Landroid/util/TypedValue;->data:I

    goto :goto_2
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 14

    .prologue
    .line 78
    const/4 v0, 0x5

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 79
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    const/4 v2, -0x2

    .line 80
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x2

    const/16 v0, 0xa

    new-array v3, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 83
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x1

    .line 84
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/libraries/curvular/bb;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/bb;-><init>(Lcom/google/android/libraries/curvular/ay;)V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/a/e;->p()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/google/android/libraries/curvular/br;->b(Lcom/google/android/libraries/curvular/am;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    const-wide v6, 0x4061600000000000L    # 139.0

    .line 85
    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_3

    double-to-int v6, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v8, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    const-wide v8, 0x4058c00000000000L    # 99.0

    .line 88
    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_4

    double-to-int v8, v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v0, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v7, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    .line 84
    invoke-static {v5, v6, v0}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x2

    const-class v4, Lcom/google/android/apps/gmm/navigation/navui/a/f;

    .line 92
    sget-object v5, Lcom/google/android/apps/gmm/base/k/j;->c:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x3

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->d()Ljava/lang/Boolean;

    move-result-object v0

    const/4 v5, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v4, 0x4

    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->a()Ljava/util/List;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/gmm/base/k/j;->e:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v4, 0x5

    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->c()Lcom/google/android/apps/gmm/navigation/navui/d/a;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/gmm/base/k/j;->d:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v4, 0x6

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->k()Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/gmm/base/k/j;->f:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v4, 0x7

    .line 101
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->h()Lcom/google/android/apps/gmm/base/views/d;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/gmm/base/k/j;->R:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v4

    const/16 v4, 0x8

    const/4 v0, 0x6

    new-array v5, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const/4 v6, -0x2

    .line 104
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x1

    const/4 v6, -0x2

    .line 105
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x2

    const/4 v6, 0x3

    .line 106
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v6, 0x3

    const-wide/high16 v8, 0x4049000000000000L    # 50.0

    .line 107
    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_5

    double-to-int v8, v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v0, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v7, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bk:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x4

    sget v6, Lcom/google/android/apps/gmm/l;->an:I

    .line 108
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->w:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x5

    sget v6, Lcom/google/android/apps/gmm/k;->V:I

    .line 109
    invoke-direct {p0, v6}, Lcom/google/android/apps/gmm/navigation/navui/a/e;->a(I)Lcom/google/android/libraries/curvular/cs;

    move-result-object v6

    aput-object v6, v5, v0

    .line 103
    invoke-static {v5}, Lcom/google/android/apps/gmm/base/k/aa;->c([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v3, v4

    const/16 v4, 0x9

    const/4 v0, 0x6

    new-array v5, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const/4 v6, -0x2

    .line 112
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x1

    const/4 v6, -0x2

    .line 113
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x2

    const/4 v6, 0x5

    .line 114
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v6, 0x3

    const-wide/high16 v8, 0x4049000000000000L    # 50.0

    .line 115
    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_6

    double-to-int v8, v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v0, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v7, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bj:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x4

    sget v6, Lcom/google/android/apps/gmm/l;->ad:I

    .line 116
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->w:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x5

    sget v6, Lcom/google/android/apps/gmm/k;->W:I

    .line 117
    invoke-direct {p0, v6}, Lcom/google/android/apps/gmm/navigation/navui/a/e;->a(I)Lcom/google/android/libraries/curvular/cs;

    move-result-object v6

    aput-object v6, v5, v0

    .line 111
    invoke-static {v5}, Lcom/google/android/apps/gmm/base/k/aa;->e([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v3, v4

    .line 82
    invoke-static {v3}, Lcom/google/android/apps/gmm/base/k/aa;->d([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v2, 0x3

    const/4 v0, 0x6

    new-array v3, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 123
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    const/4 v4, -0x2

    .line 124
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    const/4 v4, 0x1

    .line 125
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x3

    .line 126
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->d()Ljava/lang/Boolean;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    const/16 v6, 0x8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v4, 0x4

    const/16 v0, 0x9

    new-array v5, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const/4 v6, -0x1

    .line 128
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v6, 0x1

    .line 129
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/libraries/curvular/bb;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/bb;-><init>(Lcom/google/android/libraries/curvular/ay;)V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/a/e;->p()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/google/android/libraries/curvular/br;->b(Lcom/google/android/libraries/curvular/am;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    const-wide/high16 v8, 0x4056000000000000L    # 88.0

    .line 130
    new-instance v10, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_7

    double-to-int v8, v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v0, Landroid/util/TypedValue;->data:I

    :goto_4
    invoke-direct {v10, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    const-wide/high16 v10, 0x4048000000000000L    # 48.0

    .line 131
    new-instance v9, Lcom/google/android/libraries/curvular/b;

    invoke-static {v10, v11}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_8

    double-to-int v10, v10

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v10, v11

    shl-int/lit8 v10, v10, 0x8

    or-int/lit8 v10, v10, 0x1

    iput v10, v0, Landroid/util/TypedValue;->data:I

    :goto_5
    invoke-direct {v9, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    .line 129
    invoke-static {v7, v8, v0}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x2

    const/16 v6, 0x11

    .line 134
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x3

    const/4 v6, 0x1

    .line 135
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->s:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v6, 0x4

    .line 137
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->j()Ljava/lang/Boolean;

    move-result-object v0

    .line 138
    sget v7, Lcom/google/android/apps/gmm/d;->ar:I

    invoke-static {v7}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    .line 139
    sget v8, Lcom/google/android/apps/gmm/d;->aq:I

    invoke-static {v8}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    .line 137
    invoke-static {v0, v7, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v6, 0x5

    .line 141
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->e()Ljava/lang/String;

    move-result-object v0

    sget-object v7, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v6, 0x6

    const-wide/high16 v8, 0x403b000000000000L    # 27.0

    .line 142
    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_9

    double-to-int v8, v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x2

    iput v8, v0, Landroid/util/TypedValue;->data:I

    :goto_6
    invoke-direct {v7, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x7

    .line 143
    sget v6, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v6}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/16 v0, 0x8

    sget-object v6, Lcom/google/android/libraries/curvular/z;->a:Landroid/graphics/Typeface;

    .line 144
    sget-object v7, Lcom/google/android/libraries/curvular/g;->U:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    .line 127
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v5}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v5, "android.widget.TextView"

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v4, 0x5

    const/4 v0, 0x3

    new-array v5, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const/4 v6, -0x1

    .line 146
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v6, 0x1

    const-wide/high16 v8, 0x4008000000000000L    # 3.0

    .line 147
    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_a

    double-to-int v8, v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v0, Landroid/util/TypedValue;->data:I

    :goto_7
    invoke-direct {v7, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x2

    sget v6, Lcom/google/android/apps/gmm/f;->gr:I

    .line 148
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    .line 145
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v5}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v5, "android.view.View"

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v3, v4

    .line 122
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v3}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.LinearLayout"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v2, 0x4

    const/4 v0, 0x2

    new-array v3, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v4, 0x0

    .line 154
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/libraries/curvular/bb;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/bb;-><init>(Lcom/google/android/libraries/curvular/ay;)V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/a/e;->p()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/google/android/libraries/curvular/br;->b(Lcom/google/android/libraries/curvular/am;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    const-wide/high16 v6, 0x404c000000000000L    # 56.0

    .line 155
    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_b

    double-to-int v6, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/util/TypedValue;->data:I

    :goto_8
    invoke-direct {v8, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->as:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    const-wide/high16 v8, 0x4030000000000000L    # 16.0

    .line 156
    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_c

    double-to-int v8, v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v0, Landroid/util/TypedValue;->data:I

    :goto_9
    invoke-direct {v7, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->as:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    .line 154
    invoke-static {v5, v6, v0}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    const-class v5, Lcom/google/android/apps/gmm/base/f/b;

    .line 158
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->l()Lcom/google/android/apps/gmm/navigation/commonui/c/l;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/c/l;->a()Lcom/google/android/apps/gmm/base/l/j;

    move-result-object v0

    .line 157
    new-instance v6, Lcom/google/android/libraries/curvular/ao;

    const/4 v7, 0x0

    invoke-static {v7, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-direct {v6, v5, v0}, Lcom/google/android/libraries/curvular/ao;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ah;)V

    aput-object v6, v3, v4

    .line 153
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v3}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.FrameLayout"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v2

    .line 78
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.FrameLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    .line 85
    :cond_3
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    .line 88
    :cond_4
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    .line 107
    :cond_5
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    .line 115
    :cond_6
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3

    .line 130
    :cond_7
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_4

    .line 131
    :cond_8
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v10, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v10, v11, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v10

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v10, v11

    shl-int/lit8 v10, v10, 0x8

    or-int/lit8 v10, v10, 0x11

    iput v10, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_5

    .line 142
    :cond_9
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x12

    iput v8, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_6

    .line 147
    :cond_a
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_7

    .line 155
    :cond_b
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_8

    .line 156
    :cond_c
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_9
.end method
