.class Lcom/google/android/apps/gmm/appwidget/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Landroid/widget/Button;

.field final synthetic b:Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;Landroid/widget/Button;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/android/apps/gmm/appwidget/b;->b:Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;

    iput-object p2, p0, Lcom/google/android/apps/gmm/appwidget/b;->a:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/appwidget/b;->b:Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;

    .line 171
    iget-object v0, v0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/appwidget/b;->b:Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;

    iget-object v0, v0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 172
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/appwidget/b;->a:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/gmm/appwidget/b;->a:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/gmm/appwidget/b;->b:Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/apps/gmm/d;->X:I

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 175
    return-void

    .line 171
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 173
    :cond_1
    sget v0, Lcom/google/android/apps/gmm/d;->L:I

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 162
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 166
    return-void
.end method
