.class public Lcom/google/android/apps/gmm/base/views/GmmProgressBar;
.super Landroid/widget/ProgressBar;
.source "PG"


# instance fields
.field a:J

.field public b:[Landroid/view/View;

.field public c:Ljava/lang/Runnable;

.field private final d:Landroid/os/Handler;

.field private e:Landroid/animation/AnimatorSet;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/base/views/x;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/x;-><init>(Lcom/google/android/apps/gmm/base/views/GmmProgressBar;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->d:Landroid/os/Handler;

    .line 54
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->a:J

    .line 55
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->b:[Landroid/view/View;

    .line 56
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->e:Landroid/animation/AnimatorSet;

    .line 61
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 68
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->a:J

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->d:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->d:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->d:Landroid/os/Handler;

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 73
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x1f4

    const/4 v6, 0x2

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->d:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 95
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->a:J

    sub-long/2addr v0, v2

    .line 96
    cmp-long v2, v0, v8

    if-gez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->a:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 100
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->setVisibility(I)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->d()V

    .line 110
    :cond_1
    :goto_0
    return-void

    .line 106
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->d:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 107
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->d:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->d:Landroid/os/Handler;

    invoke-static {v3, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v3

    sub-long v0, v8, v0

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->d:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->d:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->e:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->e:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->e:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 121
    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->setVisibility(I)V

    .line 122
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->b:[Landroid/view/View;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 123
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 125
    :cond_1
    return-void
.end method

.method d()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->c:Ljava/lang/Runnable;

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->b:[Landroid/view/View;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 153
    :goto_0
    return-void

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->b:[Landroid/view/View;

    array-length v0, v0

    new-array v4, v0, [Landroid/animation/Animator;

    .line 146
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->b:[Landroid/view/View;

    array-length v6, v5

    move v0, v1

    move v2, v1

    :goto_1
    if-ge v0, v6, :cond_2

    aget-object v7, v5, v0

    .line 147
    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Landroid/view/View;->setAlpha(F)V

    .line 148
    invoke-virtual {v7, v1}, Landroid/view/View;->setVisibility(I)V

    .line 149
    add-int/lit8 v3, v2, 0x1

    const-string v8, "alpha"

    const/4 v9, 0x1

    new-array v9, v9, [F

    const/high16 v10, 0x3f800000    # 1.0f

    aput v10, v9, v1

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    aput-object v7, v4, v2

    .line 146
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_1

    .line 151
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->e:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->e:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method
