.class public Lcom/google/android/apps/gmm/car/e/au;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# instance fields
.field final a:Lcom/google/android/apps/gmm/navigation/navui/o;

.field b:Lcom/google/android/apps/gmm/navigation/navui/d/k;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final c:Lcom/google/android/apps/gmm/car/ad;

.field private final d:Lcom/google/android/apps/gmm/navigation/navui/g;

.field private final e:Lcom/google/android/apps/gmm/car/e/i;

.field private final f:Lcom/google/android/apps/gmm/car/ax;

.field private final g:Lcom/google/android/apps/gmm/z/b/j;

.field private h:Lcom/google/android/apps/gmm/car/views/DefaultFocusingFrameLayout;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final i:Lcom/google/android/apps/gmm/car/views/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/o;Lcom/google/android/apps/gmm/car/e/i;)V
    .locals 5

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/z/b/j;

    sget-object v1, Lcom/google/b/f/t;->ae:Lcom/google/b/f/t;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/b/j;-><init>(Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/au;->g:Lcom/google/android/apps/gmm/z/b/j;

    .line 217
    new-instance v0, Lcom/google/android/apps/gmm/car/e/ax;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/ax;-><init>(Lcom/google/android/apps/gmm/car/e/au;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/au;->i:Lcom/google/android/apps/gmm/car/views/a;

    .line 53
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/car/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/au;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 54
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/navigation/navui/g;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/e/au;->d:Lcom/google/android/apps/gmm/navigation/navui/g;

    .line 55
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/navigation/navui/o;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/e/au;->a:Lcom/google/android/apps/gmm/navigation/navui/o;

    .line 56
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p4, Lcom/google/android/apps/gmm/car/e/i;

    iput-object p4, p0, Lcom/google/android/apps/gmm/car/e/au;->e:Lcom/google/android/apps/gmm/car/e/i;

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/car/ax;

    .line 59
    iget-object v1, p1, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    .line 60
    iget-object v2, p1, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    .line 61
    iget-object v3, p1, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    sget v4, Lcom/google/android/apps/gmm/e;->r:I

    iget-object v3, v3, Lcom/google/android/apps/gmm/car/v;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/car/ax;-><init>(Landroid/graphics/Point;Lcom/google/android/apps/gmm/car/v;I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/au;->f:Lcom/google/android/apps/gmm/car/ax;

    .line 62
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/au;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->s:Lcom/google/android/apps/gmm/car/d/a;

    new-instance v1, Lcom/google/android/apps/gmm/car/e/aw;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/e/aw;-><init>(Lcom/google/android/apps/gmm/car/e/au;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/d/a;->k:Lcom/google/android/apps/gmm/car/d/i;

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/au;->e:Lcom/google/android/apps/gmm/car/e/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/au;->f:Lcom/google/android/apps/gmm/car/ax;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/e/i;->a(Lcom/google/android/apps/gmm/car/ax;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/au;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/au;->g:Lcom/google/android/apps/gmm/z/b/j;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/o;)V

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/au;->h:Lcom/google/android/apps/gmm/car/views/DefaultFocusingFrameLayout;

    return-object v0
.end method

.method public final a()V
    .locals 22

    .prologue
    .line 71
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/e/au;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 72
    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    const-class v3, Lcom/google/android/apps/gmm/car/e/at;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v19

    .line 74
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/e/au;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v2}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 75
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 77
    new-instance v2, Lcom/google/android/apps/gmm/navigation/navui/views/d;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/e/au;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 78
    iget-object v3, v3, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v3

    const/4 v5, 0x1

    sget v6, Lcom/google/android/apps/gmm/e;->o:I

    .line 80
    invoke-virtual {v8, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    sget v7, Lcom/google/android/apps/gmm/e;->p:I

    .line 81
    invoke-virtual {v8, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/navigation/navui/views/d;-><init>(Lcom/google/android/apps/gmm/shared/c/a/j;Landroid/content/Context;ZFF)V

    .line 82
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    const v3, -0x460936

    invoke-virtual {v6, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    const v3, -0xf062a8

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    const v3, -0x9a3674

    invoke-virtual {v12, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    const v3, -0xf47fbd

    invoke-virtual {v13, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    const v5, -0x9090a

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v17, Landroid/graphics/Paint;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Paint;-><init>()V

    const v5, -0x626263

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v18, Landroid/graphics/Paint;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Paint;-><init>()V

    const v5, -0x363637

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v20, Landroid/graphics/Paint;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Paint;-><init>()V

    const v5, -0x7f7f80

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    sget v5, Lcom/google/android/apps/gmm/e;->n:I

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    sget v5, Lcom/google/android/apps/gmm/e;->p:I

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    new-instance v5, Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move-object v8, v7

    invoke-direct/range {v5 .. v10}, Lcom/google/android/apps/gmm/navigation/navui/views/f;-><init>(Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;FF)V

    new-instance v11, Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move-object v14, v13

    move v15, v9

    move/from16 v16, v10

    invoke-direct/range {v11 .. v16}, Lcom/google/android/apps/gmm/navigation/navui/views/f;-><init>(Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;FF)V

    new-instance v12, Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move-object v13, v3

    move-object/from16 v14, v17

    move-object/from16 v15, v17

    move/from16 v16, v9

    move/from16 v17, v10

    invoke-direct/range {v12 .. v17}, Lcom/google/android/apps/gmm/navigation/navui/views/f;-><init>(Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;FF)V

    new-instance v13, Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move-object/from16 v14, v18

    move-object/from16 v15, v20

    move-object/from16 v16, v20

    move/from16 v17, v9

    move/from16 v18, v10

    invoke-direct/range {v13 .. v18}, Lcom/google/android/apps/gmm/navigation/navui/views/f;-><init>(Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;FF)V

    new-instance v15, Lcom/google/android/apps/gmm/navigation/navui/views/g;

    invoke-direct {v15, v5, v11, v12, v13}, Lcom/google/android/apps/gmm/navigation/navui/views/g;-><init>(Lcom/google/android/apps/gmm/navigation/navui/views/f;Lcom/google/android/apps/gmm/navigation/navui/views/f;Lcom/google/android/apps/gmm/navigation/navui/views/f;Lcom/google/android/apps/gmm/navigation/navui/views/f;)V

    .line 83
    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/navui/views/a;->a:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v5, Lcom/google/android/apps/gmm/navigation/navui/views/b;

    invoke-direct {v5, v2}, Lcom/google/android/apps/gmm/navigation/navui/views/b;-><init>(Lcom/google/android/apps/gmm/navigation/navui/views/a;)V

    sget-object v6, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v3, v5, v6}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 85
    new-instance v3, Lcom/google/android/apps/gmm/navigation/navui/d/k;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/au;->d:Lcom/google/android/apps/gmm/navigation/navui/g;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/au;->a:Lcom/google/android/apps/gmm/navigation/navui/o;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/car/e/au;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 86
    iget-object v0, v5, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    move-object/from16 v18, v0

    .line 87
    new-instance v20, Lcom/google/android/apps/gmm/navigation/navui/d/l;

    sget v5, Lcom/google/android/apps/gmm/d;->aM:I

    sget v6, Lcom/google/android/apps/gmm/d;->y:I

    new-instance v7, Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/shared/c/c/j;-><init>()V

    const/4 v8, 0x0

    move-object/from16 v0, v20

    invoke-direct {v0, v5, v6, v7, v8}, Lcom/google/android/apps/gmm/navigation/navui/d/l;-><init>(IILcom/google/android/apps/gmm/shared/c/c/j;Z)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/car/e/au;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 88
    iget-boolean v0, v5, Lcom/google/android/apps/gmm/car/ad;->o:Z

    move/from16 v21, v0

    new-instance v5, Lcom/google/android/apps/gmm/navigation/navui/d/e;

    const/4 v6, 0x0

    const v7, -0x33000001    # -1.3421772E8f

    const v8, -0x6b000001

    const/high16 v9, 0x3f000000    # 0.5f

    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v11, 0x3f800000    # 1.0f

    const/high16 v12, 0x3f800000    # 1.0f

    const v13, -0x460936

    const v14, -0x32252c

    invoke-direct/range {v5 .. v14}, Lcom/google/android/apps/gmm/navigation/navui/d/e;-><init>(ZIIFFFFII)V

    move-object v6, v3

    move-object/from16 v7, v16

    move-object/from16 v8, v17

    move-object/from16 v9, v18

    move-object v10, v4

    move-object/from16 v11, v20

    move/from16 v12, v21

    move-object v13, v5

    move-object v14, v2

    invoke-direct/range {v6 .. v15}, Lcom/google/android/apps/gmm/navigation/navui/d/k;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Landroid/content/Context;Lcom/google/android/apps/gmm/navigation/navui/d/l;ZLcom/google/android/apps/gmm/navigation/navui/d/e;Lcom/google/android/apps/gmm/navigation/navui/views/d;Lcom/google/android/apps/gmm/navigation/navui/views/g;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/car/e/au;->b:Lcom/google/android/apps/gmm/navigation/navui/d/k;

    .line 91
    new-instance v3, Lcom/google/android/apps/gmm/car/e/av;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/car/e/av;-><init>(Lcom/google/android/apps/gmm/car/e/au;Lcom/google/android/libraries/curvular/ae;)V

    .line 97
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/e/au;->b:Lcom/google/android/apps/gmm/navigation/navui/d/k;

    if-nez v3, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_0
    move-object v2, v3

    check-cast v2, Ljava/lang/Runnable;

    iput-object v2, v4, Lcom/google/android/apps/gmm/navigation/navui/d/k;->h:Ljava/lang/Runnable;

    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/navui/d/k;->d:Lcom/google/android/apps/gmm/navigation/navui/d/g;

    if-nez v3, :cond_1

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_1
    move-object v2, v3

    check-cast v2, Ljava/lang/Runnable;

    iput-object v2, v4, Lcom/google/android/apps/gmm/navigation/navui/d/g;->q:Ljava/lang/Runnable;

    .line 98
    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    .line 99
    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    check-cast v2, Lcom/google/android/apps/gmm/car/views/DefaultFocusingFrameLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/e/au;->h:Lcom/google/android/apps/gmm/car/views/DefaultFocusingFrameLayout;

    .line 101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/e/au;->h:Lcom/google/android/apps/gmm/car/views/DefaultFocusingFrameLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/e/au;->i:Lcom/google/android/apps/gmm/car/views/a;

    iput-object v3, v2, Lcom/google/android/apps/gmm/car/views/DefaultFocusingFrameLayout;->a:Lcom/google/android/apps/gmm/car/views/a;

    .line 105
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/e/au;->h:Lcom/google/android/apps/gmm/car/views/DefaultFocusingFrameLayout;

    sget-object v3, Lcom/google/android/apps/gmm/car/e/at;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v2, v3}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    .line 107
    iget-object v2, v2, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setFocusable(Z)V

    .line 108
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/au;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->s:Lcom/google/android/apps/gmm/car/d/a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/d/a;->k:Lcom/google/android/apps/gmm/car/d/i;

    .line 137
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 141
    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/au;->h:Lcom/google/android/apps/gmm/car/views/DefaultFocusingFrameLayout;

    .line 142
    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/au;->b:Lcom/google/android/apps/gmm/navigation/navui/d/k;

    .line 143
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 147
    sget-object v0, Lcom/google/android/apps/gmm/car/m/e;->c:Lcom/google/android/apps/gmm/car/m/e;

    return-object v0
.end method
