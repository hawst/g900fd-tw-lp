.class public Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.super Landroid/app/Fragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/a/a;
.implements Lcom/google/android/apps/gmm/base/fragments/a/c;
.implements Lcom/google/android/apps/gmm/feedback/a/c;
.implements Lcom/google/android/apps/gmm/z/b/o;


# static fields
.field public static final h:Ljava/lang/String;


# instance fields
.field private a:Lcom/google/android/apps/gmm/map/d/a;

.field private b:Ljava/lang/Integer;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private c:Lcom/google/android/apps/gmm/map/f/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field public j:Lcom/google/android/apps/gmm/base/activities/c;

.field public k:Landroid/view/View;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public l:Lcom/google/android/apps/gmm/base/fragments/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 498
    return-void
.end method


# virtual methods
.method public final K_()V
    .locals 2

    .prologue
    .line 545
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->k:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->k:Landroid/view/View;

    .line 546
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->isResumed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 550
    :cond_0
    :goto_1
    return-void

    .line 545
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getView()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 549
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->b(Landroid/view/View;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/apps/gmm/base/j/c;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 125
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 126
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    goto :goto_0
.end method

.method public a(IIF)V
    .locals 0

    .prologue
    .line 422
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V
    .locals 3

    .prologue
    .line 240
    instance-of v0, p0, Lcom/google/android/apps/gmm/base/fragments/a/b;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 241
    check-cast v0, Lcom/google/android/apps/gmm/base/fragments/a/b;

    iput-object v0, p1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->l:Lcom/google/android/apps/gmm/base/fragments/a/b;

    .line 247
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 248
    return-void

    .line 243
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->h:Ljava/lang/String;

    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "You should use GmmActivity#pushGmmActivityFragment(GmmActivityFragment), instead."

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 646
    invoke-static {p1}, Lcom/google/b/a/bo;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 661
    :goto_0
    return-void

    .line 649
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/base/fragments/j;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/gmm/base/fragments/j;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0
.end method

.method public final a_(I)V
    .locals 1

    .prologue
    .line 594
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->b:Ljava/lang/Integer;

    .line 595
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 560
    invoke-static {p1}, Lcom/google/android/apps/gmm/a/a/b;->a(Landroid/view/View;)V

    .line 561
    return-void
.end method

.method public d_()V
    .locals 0

    .prologue
    .line 293
    return-void
.end method

.method public final e()Landroid/app/Fragment;
    .locals 0

    .prologue
    .line 470
    return-object p0
.end method

.method public e_()Lcom/google/android/apps/gmm/base/fragments/a/a;
    .locals 1

    .prologue
    .line 458
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    return-object v0
.end method

.method public f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 599
    sget-object v0, Lcom/google/b/f/t;->cF:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final g()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 525
    const/4 v0, 0x0

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 583
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 1

    .prologue
    .line 607
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->a:Lcom/google/android/apps/gmm/feedback/a/d;

    return-object v0
.end method

.method public n()Lcom/google/android/apps/gmm/base/g/c;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 618
    const/4 v0, 0x0

    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 139
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 140
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    iput-object p1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 143
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/z/a/b;->a()V

    .line 153
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 154
    if-eqz p1, :cond_1

    .line 156
    const-string v0, "fragmentResultListener"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "fragmentResultListener"

    invoke-virtual {v0, p1, v1}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/fragments/a/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->l:Lcom/google/android/apps/gmm/base/fragments/a/b;

    .line 162
    :cond_0
    const-string v0, "savedExpandingState"

    .line 163
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 165
    const-string v0, "ue3ActivationId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    const-string v0, "ue3ActivationId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->b:Ljava/lang/Integer;

    .line 169
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "restoreCameraPositionOnResume"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 177
    if-eqz p1, :cond_3

    .line 178
    const-string v0, "savedCameraPosition"

    .line 179
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->c:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 184
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 185
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/fragments/k;

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/l;->a:Lcom/google/android/apps/gmm/base/fragments/l;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/gmm/base/fragments/k;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/base/fragments/l;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 186
    return-void

    .line 181
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->c:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_0

    :cond_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 395
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 397
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/fragments/k;

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/l;->d:Lcom/google/android/apps/gmm/base/fragments/l;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/gmm/base/fragments/k;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/base/fragments/l;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 398
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 402
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->g:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 404
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 405
    const/4 v0, 0x1

    .line 407
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 4

    .prologue
    .line 297
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 299
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->a:Lcom/google/android/apps/gmm/map/d/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/d/a;->g:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 301
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-nez v0, :cond_0

    .line 306
    sget v0, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 309
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/fragments/k;

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/l;->c:Lcom/google/android/apps/gmm/base/fragments/l;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/gmm/base/fragments/k;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/base/fragments/l;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 312
    instance-of v0, p0, Lcom/google/android/apps/gmm/l/av;

    if-eqz v0, :cond_2

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    check-cast p0, Lcom/google/android/apps/gmm/l/av;

    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->j:Ljava/util/Stack;

    invoke-virtual {v0, p0}, Ljava/util/Stack;->remove(Ljava/lang/Object;)Z

    .line 315
    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 190
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 192
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/fragments/k;

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/l;->b:Lcom/google/android/apps/gmm/base/fragments/l;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/gmm/base/fragments/k;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/base/fragments/l;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 194
    new-instance v0, Lcom/google/android/apps/gmm/base/fragments/h;

    .line 195
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v1

    .line 196
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/gmm/base/fragments/h;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->a:Lcom/google/android/apps/gmm/map/d/a;

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->a:Lcom/google/android/apps/gmm/map/d/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/d/a;->g:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 218
    instance-of v0, p0, Lcom/google/android/apps/gmm/l/av;

    if-eqz v0, :cond_1

    .line 219
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    move-object v0, p0

    check-cast v0, Lcom/google/android/apps/gmm/l/av;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->j:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->c:Lcom/google/android/apps/gmm/map/f/a/a;

    if-eqz v0, :cond_2

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->c:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 224
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v1

    const/4 v2, 0x0

    iput v2, v1, Lcom/google/android/apps/gmm/map/a;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 227
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-static {v1, p0, v0}, Lcom/google/android/apps/gmm/z/i;->a(Landroid/view/View;Lcom/google/android/apps/gmm/z/b/o;Lcom/google/android/apps/gmm/map/c/a;)V

    .line 228
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 325
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 326
    const-string v0, "savedExpandingState"

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 328
    const-string v0, "ue3ActivationId"

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->l:Lcom/google/android/apps/gmm/base/fragments/a/b;

    if-eqz v0, :cond_1

    .line 332
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "fragmentResultListener"

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->l:Lcom/google/android/apps/gmm/base/fragments/a/b;

    check-cast v0, Landroid/app/Fragment;

    invoke-virtual {v1, p1, v2, v0}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    .line 336
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->c:Lcom/google/android/apps/gmm/map/f/a/a;

    if-eqz v0, :cond_2

    .line 342
    const-string v0, "savedCameraPosition"

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->c:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 344
    :cond_2
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 319
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/z/a/b;->a()V

    .line 321
    return-void
.end method
