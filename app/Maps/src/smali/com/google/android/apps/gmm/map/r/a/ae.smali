.class public Lcom/google/android/apps/gmm/map/r/a/ae;
.super Ljava/util/AbstractList;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Lcom/google/android/apps/gmm/map/r/a/w;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/apps/gmm/map/r/a/ae;


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/w;",
            ">;"
        }
    .end annotation
.end field

.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 27
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/r/a/ae;-><init>(Ljava/util/List;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/r/a/ae;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/r/a/w;)V
    .locals 2

    .prologue
    .line 45
    invoke-static {p1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/r/a/ae;-><init>(Ljava/util/List;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Ljava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/w;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 55
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56
    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 57
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p2, v0}, Lcom/google/b/a/aq;->a(II)I

    .line 59
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    .line 60
    iput p2, p0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    .line 61
    return-void
.end method

.method public static varargs a(I[Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/map/r/a/ae;
    .locals 2

    .prologue
    .line 134
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 135
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/ae;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 138
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-static {p1}, Lcom/google/b/c/cv;->a([Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/gmm/map/r/a/ae;-><init>(Ljava/util/List;I)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/f;Landroid/content/Context;I)Lcom/google/android/apps/gmm/map/r/a/ae;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 164
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 168
    new-instance v3, Ljava/util/ArrayList;

    if-ltz v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    const-wide/16 v4, 0x5

    int-to-long v6, v2

    add-long/2addr v4, v6

    div-int/lit8 v0, v2, 0xa

    int-to-long v6, v0

    add-long/2addr v4, v6

    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-lez v0, :cond_5

    const v0, 0x7fffffff

    :goto_1
    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 169
    :goto_2
    if-ge v0, v2, :cond_7

    .line 171
    invoke-static {p0, v0, p1}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/r/a/f;ILandroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/w;

    move-result-object v1

    .line 172
    if-eqz v1, :cond_4

    .line 173
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 168
    :cond_5
    const-wide/32 v6, -0x80000000

    cmp-long v0, v4, v6

    if-gez v0, :cond_6

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_6
    long-to-int v0, v4

    goto :goto_1

    .line 176
    :cond_7
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_8

    .line 177
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/ae;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 179
    :goto_3
    return-object v0

    :cond_8
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-direct {v0, v3, p2}, Lcom/google/android/apps/gmm/map/r/a/ae;-><init>(Ljava/util/List;I)V

    goto :goto_3
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/r/a/w;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 91
    iget v0, p0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/w;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 116
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 124
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 119
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 124
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
