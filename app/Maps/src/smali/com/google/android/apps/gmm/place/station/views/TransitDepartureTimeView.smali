.class public Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public d:Z

.field private final e:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x0

    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->setLayoutDirection(I)V

    .line 55
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->setOrientation(I)V

    .line 57
    invoke-static {p1}, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->a(Landroid/content/Context;)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->a:Landroid/widget/TextView;

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->a:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->addView(Landroid/view/View;)V

    .line 60
    invoke-static {p1}, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->a(Landroid/content/Context;)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->b:Landroid/widget/TextView;

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->b:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->addView(Landroid/view/View;)V

    .line 63
    invoke-static {p1}, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->a(Landroid/content/Context;)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->c:Landroid/widget/TextView;

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->c:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->addView(Landroid/view/View;)V

    .line 67
    new-array v0, v4, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->e:Landroid/animation/ValueAnimator;

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->e:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/google/android/apps/gmm/place/station/views/a;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/place/station/views/a;-><init>(Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->e:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->e:Landroid/animation/ValueAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 77
    return-void

    .line 67
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private static a(Landroid/content/Context;)Landroid/widget/TextView;
    .locals 3

    .prologue
    .line 80
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 81
    sget v1, Lcom/google/android/apps/gmm/m;->l:I

    invoke-virtual {v0, p0, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 82
    sget v1, Lcom/google/android/apps/gmm/k;->aa:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->e(I)Lcom/google/android/libraries/curvular/bl;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/libraries/curvular/bl;->f(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 83
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/d;->N:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 84
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 85
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 86
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->d:Z

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 122
    :goto_0
    return-void

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->e:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->a()V

    .line 129
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 137
    return-void
.end method
