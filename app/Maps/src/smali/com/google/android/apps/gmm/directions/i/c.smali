.class public Lcom/google/android/apps/gmm/directions/i/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/k;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z

.field private final c:Lcom/google/android/apps/gmm/directions/al;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLcom/google/android/apps/gmm/directions/al;)V
    .locals 0
    .param p3    # Lcom/google/android/apps/gmm/directions/al;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/c;->a:Ljava/lang/String;

    .line 20
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/directions/i/c;->b:Z

    .line 21
    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/i/c;->c:Lcom/google/android/apps/gmm/directions/al;

    .line 22
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/i/c;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/c;->c:Lcom/google/android/apps/gmm/directions/al;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/c;->c:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/al;->H_()V

    .line 39
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
