.class public Lcom/google/android/apps/gmm/navigation/b/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/navigation/b/e;

.field public final b:Lcom/google/android/apps/gmm/map/r/a/f;

.field public final c:I

.field public final d:Lcom/google/android/apps/gmm/navigation/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/gmm/navigation/b/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/r/a/f;I)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    sget-object v0, Lcom/google/android/apps/gmm/navigation/b/e;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/b/d;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    .line 86
    const-string v0, "directionsItem"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/r/a/f;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/b/d;->b:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 87
    iput p2, p0, Lcom/google/android/apps/gmm/navigation/b/d;->c:I

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/b/d;->d:Lcom/google/android/apps/gmm/navigation/b/a;

    .line 89
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/navigation/b/a;)V
    .locals 2

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    const-string v0, "not enabled"

    .line 93
    sget-object v0, Lcom/google/android/apps/gmm/navigation/b/e;->b:Lcom/google/android/apps/gmm/navigation/b/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/b/d;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    .line 94
    const-string v0, "item"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/navigation/b/a;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/b/d;->d:Lcom/google/android/apps/gmm/navigation/b/a;

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/b/d;->b:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 96
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/b/d;->c:I

    .line 97
    return-void
.end method

.method public static a(Landroid/net/Uri;Lcom/google/android/apps/gmm/x/a;)Lcom/google/android/apps/gmm/navigation/b/d;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 144
    const-string v1, "m"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/navigation/b/e;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/navigation/b/e;

    move-result-object v1

    .line 145
    sget-object v2, Lcom/google/android/apps/gmm/navigation/b/e;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    if-ne v1, v2, :cond_1

    .line 146
    const-string v1, "d"

    .line 147
    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/x/a;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/x/l;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/f;

    .line 148
    const-string v1, "idx"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 149
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 150
    new-instance v1, Lcom/google/android/apps/gmm/navigation/b/d;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/navigation/b/d;-><init>(Lcom/google/android/apps/gmm/map/r/a/f;I)V

    move-object v0, v1

    .line 153
    :goto_1
    return-object v0

    .line 147
    :cond_0
    invoke-virtual {p1, v1}, Lcom/google/android/apps/gmm/x/a;->a(Lcom/google/android/apps/gmm/x/l;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    goto :goto_0

    .line 151
    :cond_1
    sget-object v2, Lcom/google/android/apps/gmm/navigation/b/e;->b:Lcom/google/android/apps/gmm/navigation/b/e;

    if-ne v1, v2, :cond_3

    .line 152
    const-string v1, "fn"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/x/a;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/x/l;

    move-result-object v1

    if-nez v1, :cond_2

    :goto_2
    check-cast v0, Lcom/google/android/apps/gmm/navigation/b/a;

    .line 153
    new-instance v1, Lcom/google/android/apps/gmm/navigation/b/d;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/navigation/b/d;-><init>(Lcom/google/android/apps/gmm/navigation/b/a;)V

    move-object v0, v1

    goto :goto_1

    .line 152
    :cond_2
    invoke-virtual {p1, v1}, Lcom/google/android/apps/gmm/x/a;->a(Lcom/google/android/apps/gmm/x/l;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    goto :goto_2

    .line 155
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xe

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroid/os/Bundle;Lcom/google/android/apps/gmm/x/a;)Lcom/google/android/apps/gmm/navigation/b/d;
    .locals 4

    .prologue
    .line 184
    const-string v0, "m"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/b/e;

    .line 185
    sget-object v1, Lcom/google/android/apps/gmm/navigation/b/e;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    if-ne v0, v1, :cond_0

    .line 186
    const-string v0, "d"

    invoke-virtual {p1, p0, v0}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/f;

    .line 187
    const-string v1, "idx"

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 188
    new-instance v1, Lcom/google/android/apps/gmm/navigation/b/d;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/navigation/b/d;-><init>(Lcom/google/android/apps/gmm/map/r/a/f;I)V

    move-object v0, v1

    .line 191
    :goto_0
    return-object v0

    .line 189
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/navigation/b/e;->b:Lcom/google/android/apps/gmm/navigation/b/e;

    if-ne v0, v1, :cond_1

    .line 190
    const-string v0, "fn"

    invoke-virtual {p1, p0, v0}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/b/a;

    .line 191
    new-instance v1, Lcom/google/android/apps/gmm/navigation/b/d;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/navigation/b/d;-><init>(Lcom/google/android/apps/gmm/navigation/b/a;)V

    move-object v0, v1

    goto :goto_0

    .line 193
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xe

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/f;I)Lcom/google/android/apps/gmm/navigation/b/d;
    .locals 1

    .prologue
    .line 104
    new-instance v0, Lcom/google/android/apps/gmm/navigation/b/d;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/navigation/b/d;-><init>(Lcom/google/android/apps/gmm/map/r/a/f;I)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/navigation/b/a;)Lcom/google/android/apps/gmm/navigation/b/d;
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lcom/google/android/apps/gmm/navigation/b/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/b/d;-><init>(Lcom/google/android/apps/gmm/navigation/b/a;)V

    return-object v0
.end method
