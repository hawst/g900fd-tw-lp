.class public Lcom/google/android/apps/gmm/place/bz;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/libraries/curvular/bc;Lcom/google/android/apps/gmm/place/g/a/f;Lcom/google/android/apps/gmm/place/g/a/c;)V
    .locals 3

    .prologue
    .line 307
    const-class v0, Lcom/google/android/apps/gmm/place/c/s;

    new-instance v1, Lcom/google/android/apps/gmm/place/cb;

    .line 308
    invoke-interface {p1}, Lcom/google/android/apps/gmm/place/g/a/f;->d()Lcom/google/android/apps/gmm/place/g/a/d;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/place/cb;-><init>(Lcom/google/android/apps/gmm/place/g/a/d;)V

    .line 307
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 309
    if-eqz p2, :cond_1

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/g/a/c;->d()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 312
    const-class v0, Lcom/google/android/apps/gmm/base/f/bm;

    invoke-virtual {p0, v0, p2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/place/g/a/f;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 316
    invoke-interface {p1}, Lcom/google/android/apps/gmm/place/g/a/f;->d()Lcom/google/android/apps/gmm/place/g/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/d;->h()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 319
    const-class v0, Lcom/google/android/apps/gmm/base/f/bj;

    new-instance v1, Lcom/google/android/apps/gmm/place/cc;

    .line 320
    invoke-interface {p1}, Lcom/google/android/apps/gmm/place/g/a/f;->d()Lcom/google/android/apps/gmm/place/g/a/d;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/place/cc;-><init>(Lcom/google/android/apps/gmm/place/g/a/d;)V

    .line 319
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0

    .line 324
    :cond_2
    const-class v0, Lcom/google/android/apps/gmm/base/f/bi;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/libraries/curvular/bc;Ljava/util/List;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/curvular/bc;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/g/a/d;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 293
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 294
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 295
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 296
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/g/a/d;

    .line 297
    const-class v4, Lcom/google/android/apps/gmm/place/review/b/a;

    new-instance v5, Lcom/google/android/apps/gmm/place/ce;

    invoke-direct {v5, v0}, Lcom/google/android/apps/gmm/place/ce;-><init>(Lcom/google/android/apps/gmm/place/g/a/d;)V

    invoke-virtual {p0, v4, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 298
    add-int/lit8 v0, v2, -0x1

    if-ge v1, v0, :cond_0

    .line 299
    const-class v0, Lcom/google/android/apps/gmm/base/f/v;

    new-instance v4, Lcom/google/android/apps/gmm/place/ca;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/place/ca;-><init>()V

    invoke-virtual {p0, v0, v4}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 295
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 302
    :cond_1
    return-void
.end method
