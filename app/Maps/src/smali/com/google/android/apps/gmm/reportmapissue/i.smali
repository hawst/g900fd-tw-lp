.class public Lcom/google/android/apps/gmm/reportmapissue/i;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/reportmapissue/a/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 102
    return-void
.end method

.method private a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 93
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 94
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v0

    .line 95
    new-instance v2, Lcom/google/android/apps/gmm/reportmapissue/j;

    invoke-direct {v2, p0, p3, p4}, Lcom/google/android/apps/gmm/reportmapissue/j;-><init>(Lcom/google/android/apps/gmm/reportmapissue/i;Ljava/lang/String;I)V

    const/16 v3, 0x21

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 97
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hs;Lcom/google/maps/g/hq;)V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/a/c;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/gmm/reportmapissue/a/c;-><init>(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hs;Lcom/google/maps/g/hq;)V

    invoke-static {v1}, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->a(Lcom/google/android/apps/gmm/reportmapissue/a/c;)Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 37
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/navigation/g/b/k;Lcom/google/android/apps/gmm/navigation/navui/views/d;)V
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 121
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 122
    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;

    invoke-direct {v1, v0, p1, p2, p3}, Lcom/google/android/apps/gmm/reportmapissue/LaneMapIssueDialogFragment;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/navigation/g/b/k;Lcom/google/android/apps/gmm/navigation/navui/views/d;)V

    .line 125
    const-string v2, "LaneMapIssueDialogFragment"

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Landroid/app/DialogFragment;Ljava/lang/String;)Z

    .line 126
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 42
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 43
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    sget v0, Lcom/google/android/apps/gmm/l;->pn:I

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 48
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/l;->nw:I

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->fW:I

    const/4 v2, 0x0

    .line 49
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 52
    return-void
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 56
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 57
    sget v0, Lcom/google/android/apps/gmm/l;->nv:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 58
    sget v0, Lcom/google/android/apps/gmm/l;->hG:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 60
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 61
    invoke-static {v0}, Lcom/google/android/apps/gmm/util/q;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 62
    invoke-static {}, Lcom/google/android/apps/gmm/util/q;->b()Ljava/lang/String;

    move-result-object v5

    .line 64
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 65
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    sget v7, Lcom/google/android/apps/gmm/l;->po:I

    new-array v8, v10, [Ljava/lang/Object;

    .line 68
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v9

    .line 67
    invoke-virtual {v1, v7, v8}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 66
    invoke-virtual {v6, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 69
    const-string v0, " "

    invoke-virtual {v6, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 70
    sget v0, Lcom/google/android/apps/gmm/l;->pj:I

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v2, v7, v9

    .line 71
    invoke-virtual {v1, v0, v7}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 70
    invoke-virtual {v6, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 77
    :goto_0
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    .line 78
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/google/android/apps/gmm/d;->N:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-direct {v0, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 79
    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    const/16 v8, 0x21

    .line 77
    invoke-virtual {v6, v0, v9, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 80
    const-string v0, "\n"

    invoke-virtual {v6, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 81
    const-string v0, "\n"

    invoke-virtual {v6, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 82
    invoke-virtual {v6, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 84
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/d;->X:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 85
    invoke-direct {p0, v6, v2, v4, v0}, Lcom/google/android/apps/gmm/reportmapissue/i;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;I)V

    .line 86
    invoke-direct {p0, v6, v3, v5, v0}, Lcom/google/android/apps/gmm/reportmapissue/i;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;I)V

    .line 87
    return-object v6

    .line 73
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/l;->pk:I

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v2, v7, v9

    .line 74
    invoke-virtual {v1, v0, v7}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 73
    invoke-virtual {v6, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method
