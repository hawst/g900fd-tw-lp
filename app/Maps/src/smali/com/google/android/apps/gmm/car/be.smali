.class Lcom/google/android/apps/gmm/car/be;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/car/am;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/bc;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/bc;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/be;->a:Lcom/google/android/apps/gmm/car/bc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 204
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/be;->a:Lcom/google/android/apps/gmm/car/bc;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/bc;->q:Z

    .line 206
    return-void
.end method

.method public final a(IIIII)V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v0, 0x1

    const/16 v1, 0x200

    .line 155
    sget-object v3, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0xbb

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "carNavigationStatusStarted = true, minIntervalMs = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", instrumentClusterType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", imageHeight = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", imageWidth = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", imageColorDepthBits = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 160
    if-eq p2, v0, :cond_0

    if-eq p2, v5, :cond_0

    .line 162
    sget-object v3, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x32

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid newInstrumentClusterType value "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move p2, v0

    .line 166
    :cond_0
    if-lez p3, :cond_1

    if-le p3, v1, :cond_2

    .line 167
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x28

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid newImageHeight value "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 168
    if-le p3, v1, :cond_9

    move v0, v1

    :goto_0
    move p3, v0

    .line 171
    :cond_2
    if-lez p4, :cond_3

    if-le p4, v1, :cond_4

    .line 172
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid newImageWidth value "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 173
    if-le p4, v1, :cond_a

    :goto_1
    move p4, v1

    .line 176
    :cond_4
    const/16 v0, 0x8

    if-eq p5, v0, :cond_5

    const/16 v0, 0x10

    if-eq p5, v0, :cond_5

    const/16 v0, 0x20

    if-eq p5, v0, :cond_5

    .line 178
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x30

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Invalid newImageColorDepthBits value "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move p5, v2

    .line 182
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/be;->a:Lcom/google/android/apps/gmm/car/bc;

    iget-object v7, v0, Lcom/google/android/apps/gmm/car/bc;->e:Ljava/lang/Object;

    monitor-enter v7

    .line 183
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/be;->a:Lcom/google/android/apps/gmm/car/bc;

    iget v0, v0, Lcom/google/android/apps/gmm/car/bc;->u:I

    if-ne v0, p3, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/be;->a:Lcom/google/android/apps/gmm/car/bc;

    iget v0, v0, Lcom/google/android/apps/gmm/car/bc;->v:I

    if-ne v0, p4, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/be;->a:Lcom/google/android/apps/gmm/car/bc;

    .line 184
    iget v0, v0, Lcom/google/android/apps/gmm/car/bc;->w:I

    if-eq v0, p5, :cond_7

    .line 186
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/be;->a:Lcom/google/android/apps/gmm/car/bc;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bc;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 190
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/be;->a:Lcom/google/android/apps/gmm/car/bc;

    iput p1, v0, Lcom/google/android/apps/gmm/car/bc;->s:I

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/be;->a:Lcom/google/android/apps/gmm/car/bc;

    iput p2, v0, Lcom/google/android/apps/gmm/car/bc;->t:I

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/be;->a:Lcom/google/android/apps/gmm/car/bc;

    iput p3, v0, Lcom/google/android/apps/gmm/car/bc;->u:I

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/be;->a:Lcom/google/android/apps/gmm/car/bc;

    iput p4, v0, Lcom/google/android/apps/gmm/car/bc;->v:I

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/be;->a:Lcom/google/android/apps/gmm/car/bc;

    iput p5, v0, Lcom/google/android/apps/gmm/car/bc;->w:I

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/be;->a:Lcom/google/android/apps/gmm/car/bc;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/bc;->q:Z

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/be;->a:Lcom/google/android/apps/gmm/car/bc;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/car/bc;->h:Z

    if-nez v1, :cond_b

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/bc;->a(I)V

    .line 199
    :cond_8
    :goto_2
    monitor-exit v7

    return-void

    :cond_9
    move v0, v2

    .line 168
    goto :goto_0

    :cond_a
    move v1, v2

    .line 173
    goto :goto_1

    .line 198
    :cond_b
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/bc;->a(I)V

    iget v1, v0, Lcom/google/android/apps/gmm/car/bc;->k:I

    if-nez v1, :cond_c

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/bc;->l:Ljava/lang/String;

    if-nez v1, :cond_c

    iget v1, v0, Lcom/google/android/apps/gmm/car/bc;->m:I

    if-nez v1, :cond_c

    iget v1, v0, Lcom/google/android/apps/gmm/car/bc;->n:I

    if-nez v1, :cond_c

    iget v1, v0, Lcom/google/android/apps/gmm/car/bc;->p:I

    if-eqz v1, :cond_d

    :cond_c
    iget v1, v0, Lcom/google/android/apps/gmm/car/bc;->k:I

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/bc;->l:Ljava/lang/String;

    iget v3, v0, Lcom/google/android/apps/gmm/car/bc;->m:I

    iget v4, v0, Lcom/google/android/apps/gmm/car/bc;->n:I

    iget-object v5, v0, Lcom/google/android/apps/gmm/car/bc;->o:[B

    iget v6, v0, Lcom/google/android/apps/gmm/car/bc;->p:I

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/car/bc;->a(ILjava/lang/String;II[BI)V

    :cond_d
    iget v1, v0, Lcom/google/android/apps/gmm/car/bc;->i:I

    if-nez v1, :cond_e

    iget v1, v0, Lcom/google/android/apps/gmm/car/bc;->j:I

    if-eqz v1, :cond_8

    :cond_e
    iget v1, v0, Lcom/google/android/apps/gmm/car/bc;->i:I

    iget v2, v0, Lcom/google/android/apps/gmm/car/bc;->j:I

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/car/bc;->q:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_8

    :try_start_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bc;->c:Lcom/google/android/gms/car/ak;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/ak;->a(II)Z
    :try_end_1
    .catch Lcom/google/android/gms/car/ao; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    goto :goto_2

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
