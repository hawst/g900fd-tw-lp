.class public final Lcom/google/android/apps/gmm/map/r/a/n;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/maps/g/a/dd;)I
    .locals 2

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/o;->a:[I

    invoke-virtual {p0}, Lcom/google/maps/g/a/dd;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 31
    sget v0, Lcom/google/android/apps/gmm/f;->fQ:I

    :goto_0
    return v0

    .line 26
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/f;->fR:I

    goto :goto_0

    .line 28
    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/f;->fS:I

    goto :goto_0

    .line 24
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/util/List;)Lcom/google/maps/g/a/df;
    .locals 7
    .param p0    # Ljava/util/List;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/da;",
            ">;)",
            "Lcom/google/maps/g/a/df;"
        }
    .end annotation

    .prologue
    const/high16 v6, 0x10000

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 118
    if-eqz p0, :cond_5

    .line 119
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    .line 120
    iget v1, v0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v5, 0x2

    if-ne v1, v5, :cond_3

    move v1, v2

    :goto_0
    if-eqz v1, :cond_0

    .line 121
    iget v1, v0, Lcom/google/maps/g/a/da;->c:I

    invoke-static {v1}, Lcom/google/maps/g/a/dl;->a(I)Lcom/google/maps/g/a/dl;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/a/dl;->a:Lcom/google/maps/g/a/dl;

    :cond_1
    sget-object v5, Lcom/google/maps/g/a/dl;->i:Lcom/google/maps/g/a/dl;

    if-ne v1, v5, :cond_0

    .line 122
    iget v1, v0, Lcom/google/maps/g/a/da;->a:I

    and-int/2addr v1, v6

    if-ne v1, v6, :cond_4

    move v1, v2

    :goto_1
    if-eqz v1, :cond_0

    .line 123
    iget v0, v0, Lcom/google/maps/g/a/da;->r:I

    invoke-static {v0}, Lcom/google/maps/g/a/df;->a(I)Lcom/google/maps/g/a/df;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/maps/g/a/df;->a:Lcom/google/maps/g/a/df;

    .line 127
    :cond_2
    :goto_2
    return-object v0

    :cond_3
    move v1, v3

    .line 120
    goto :goto_0

    :cond_4
    move v1, v3

    .line 122
    goto :goto_1

    .line 127
    :cond_5
    sget-object v0, Lcom/google/maps/g/a/df;->a:Lcom/google/maps/g/a/df;

    goto :goto_2
.end method
