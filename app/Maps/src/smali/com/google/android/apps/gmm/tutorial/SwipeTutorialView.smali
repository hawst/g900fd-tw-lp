.class public Lcom/google/android/apps/gmm/tutorial/SwipeTutorialView;
.super Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;-><init>(Landroid/content/Context;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 33
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;->onLayout(ZIIII)V

    .line 35
    sget v0, Lcom/google/android/apps/gmm/g;->et:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/apps/gmm/tutorial/a;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/tutorial/a;-><init>(Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/SwipeTutorialView;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/SwipeTutorialView;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/tutorial/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/c;->b:Landroid/graphics/Rect;

    if-eqz v0, :cond_1

    .line 40
    sget v0, Lcom/google/android/apps/gmm/g;->eu:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/tutorial/SwipeTutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/SwipeTutorialView;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/tutorial/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/c;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 42
    sub-int v2, p4, p2

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 43
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/tutorial/SwipeTutorialView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/e;->bE:I

    .line 44
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v3, v4

    .line 42
    invoke-virtual {v1, v2, v3, p4, v0}, Landroid/view/View;->layout(IIII)V

    .line 47
    :cond_1
    return-void
.end method
