.class Lcom/google/android/apps/gmm/place/j/v;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/r/b/a/aje;

.field final b:Ljava/lang/String;

.field private final c:Lcom/google/android/apps/gmm/base/j/b;

.field private final d:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/android/apps/gmm/place/j/x;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/j/b;Lcom/google/android/apps/gmm/x/o;Lcom/google/r/b/a/aje;Ljava/lang/String;Lcom/google/android/apps/gmm/place/j/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/j/b;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/r/b/a/aje;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/place/j/x;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/v;->c:Lcom/google/android/apps/gmm/base/j/b;

    .line 53
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/j/v;->d:Lcom/google/android/apps/gmm/x/o;

    .line 54
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/j/v;->a:Lcom/google/r/b/a/aje;

    .line 55
    iput-object p4, p0, Lcom/google/android/apps/gmm/place/j/v;->b:Ljava/lang/String;

    .line 56
    iput-object p5, p0, Lcom/google/android/apps/gmm/place/j/v;->e:Lcom/google/android/apps/gmm/place/j/x;

    .line 57
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Z)V
    .locals 6
    .param p3    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 143
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    :cond_0
    move v2, v1

    :goto_0
    if-nez v2, :cond_6

    invoke-static {p4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object v4, v2

    .line 144
    :goto_1
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_7

    :cond_1
    move v2, v1

    :goto_2
    if-eqz v2, :cond_9

    .line 147
    if-eqz v4, :cond_9

    .line 148
    const-string v2, "panoid"

    invoke-virtual {v4, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    move-object v2, p2

    .line 152
    :goto_3
    if-nez p3, :cond_8

    .line 155
    if-eqz v4, :cond_2

    const-string v3, "ll"

    invoke-virtual {v4, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 156
    :cond_2
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    if-nez v0, :cond_8

    .line 157
    invoke-static {v3}, Lcom/google/android/apps/gmm/map/b/a/u;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/u;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_8

    .line 159
    iget v1, v0, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(II)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object p3

    move-object v3, p3

    .line 164
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/v;->c:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/v;->d:Lcom/google/android/apps/gmm/x/o;

    .line 165
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v4, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/j/v;->b()Lcom/google/b/f/t;

    move-result-object v5

    .line 164
    invoke-interface {v1, v0, v4, v5}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/v;->c:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->L()Lcom/google/android/apps/gmm/streetview/a/a;

    move-result-object v0

    new-instance v4, Lcom/google/android/apps/gmm/streetview/b/a;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/streetview/b/a;-><init>()V

    move-object v1, p1

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/gmm/streetview/a/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/streetview/b/a;Z)V

    .line 168
    return-void

    :cond_5
    move v2, v0

    .line 143
    goto :goto_0

    :cond_6
    move-object v4, v3

    goto :goto_1

    :cond_7
    move v2, v0

    .line 144
    goto :goto_2

    :cond_8
    move-object v3, p3

    goto :goto_4

    :cond_9
    move-object v2, p2

    goto :goto_3
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/v;->c:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 75
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/place/j/w;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/v;->e:Lcom/google/android/apps/gmm/place/j/x;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/j/x;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 77
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/v;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->I()V

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->r:Lcom/google/r/b/a/ade;

    iget-object v2, v2, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/aje;

    invoke-virtual {v2}, Lcom/google/r/b/a/aje;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->I()V

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/c;->r:Lcom/google/r/b/a/ade;

    iget-object v3, v3, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/r/b/a/aje;

    invoke-static {v3}, Lcom/google/android/apps/gmm/base/g/c;->a(Lcom/google/r/b/a/aje;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->I()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->r:Lcom/google/r/b/a/ade;

    iget-object v0, v0, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aje;

    invoke-virtual {v0}, Lcom/google/r/b/a/aje;->g()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/j/v;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Z)V

    goto :goto_0

    .line 80
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/v;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/v;->c:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/v;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v3, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/j/v;->b()Lcom/google/b/f/t;

    move-result-object v4

    invoke-interface {v2, v1, v3, v4}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/v;->c:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->C()Lcom/google/android/apps/gmm/photo/a/a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/photo/a/a;->a(Lcom/google/android/apps/gmm/base/g/c;)V

    goto/16 :goto_0

    .line 83
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/v;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->I()V

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->q:Lcom/google/r/b/a/ade;

    iget-object v2, v2, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/aje;

    invoke-virtual {v2}, Lcom/google/r/b/a/aje;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->I()V

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/c;->q:Lcom/google/r/b/a/ade;

    iget-object v3, v3, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/r/b/a/aje;

    invoke-static {v3}, Lcom/google/android/apps/gmm/base/g/c;->a(Lcom/google/r/b/a/aje;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->I()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->q:Lcom/google/r/b/a/ade;

    iget-object v0, v0, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aje;

    invoke-virtual {v0}, Lcom/google/r/b/a/aje;->g()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/j/v;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method b()Lcom/google/b/f/t;
    .locals 2

    .prologue
    .line 118
    sget-object v0, Lcom/google/android/apps/gmm/place/j/w;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/v;->e:Lcom/google/android/apps/gmm/place/j/x;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/j/x;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 126
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 120
    :pswitch_0
    sget-object v0, Lcom/google/b/f/t;->dg:Lcom/google/b/f/t;

    goto :goto_0

    .line 122
    :pswitch_1
    sget-object v0, Lcom/google/b/f/t;->dq:Lcom/google/b/f/t;

    goto :goto_0

    .line 124
    :pswitch_2
    sget-object v0, Lcom/google/b/f/t;->dz:Lcom/google/b/f/t;

    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
