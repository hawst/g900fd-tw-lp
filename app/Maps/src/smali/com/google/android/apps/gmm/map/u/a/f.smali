.class public Lcom/google/android/apps/gmm/map/u/a/f;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final d:Ljava/util/regex/Pattern;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, "[0-9]+[A-Z]?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/u/a/f;->d:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/u/a/f;->b:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/u/a/f;->a:Ljava/lang/String;

    .line 32
    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/f;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/f;->b:Ljava/lang/String;

    :goto_0
    sget-object v1, Lcom/google/android/apps/gmm/map/u/a/f;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/u/a/f;->c:Z

    .line 33
    return-void

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/f;->a:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 55
    if-ne p0, p1, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/u/a/f;

    if-eqz v2, :cond_5

    .line 59
    check-cast p1, Lcom/google/android/apps/gmm/map/u/a/f;

    .line 60
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/u/a/f;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/u/a/f;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/u/a/f;->a:Ljava/lang/String;

    if-nez v2, :cond_2

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/u/a/f;->a:Ljava/lang/String;

    if-eqz v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/u/a/f;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/u/a/f;->a:Ljava/lang/String;

    .line 62
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/u/a/f;->c:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/map/u/a/f;->c:Z

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 65
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/f;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 48
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/u/a/f;->c:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v2

    .line 49
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/u/a/f;->b:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 50
    return v0

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/f;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 48
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1

    .line 49
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/u/a/f;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/f;->b:Ljava/lang/String;

    return-object v0
.end method
