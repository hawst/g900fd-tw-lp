.class Lcom/google/android/apps/gmm/myplaces/c/d;
.super Lcom/google/android/apps/gmm/myplaces/c/b;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/myplaces/c/b",
        "<",
        "Lcom/google/android/apps/gmm/myplaces/c/k;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/gmm/myplaces/c/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic a(Lcom/google/android/apps/gmm/myplaces/c/f;)Lcom/google/e/a/a/a/b;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 22
    check-cast p1, Lcom/google/android/apps/gmm/myplaces/c/k;

    invoke-static {p1}, Lcom/google/android/apps/gmm/myplaces/c/d;->b(Lcom/google/android/apps/gmm/myplaces/c/f;)Lcom/google/e/a/a/a/b;

    move-result-object v1

    new-instance v2, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/an;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v2, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/16 v0, 0x8

    iget-wide v4, p1, Lcom/google/android/apps/gmm/myplaces/c/f;->f:J

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v0, p1, Lcom/google/android/apps/gmm/myplaces/c/k;->a:Ljava/lang/String;

    iget-object v3, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v8, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v0, 0x3

    iget-object v3, p1, Lcom/google/android/apps/gmm/myplaces/c/k;->b:Ljava/lang/String;

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/16 v0, 0xa

    iget-object v3, p1, Lcom/google/android/apps/gmm/myplaces/c/k;->c:Ljava/lang/String;

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-wide v4, p1, Lcom/google/android/apps/gmm/myplaces/c/f;->g:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    iget-wide v4, p1, Lcom/google/android/apps/gmm/myplaces/c/f;->g:J

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/myplaces/c/k;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eqz v0, :cond_1

    const/16 v0, 0xb

    iget-object v3, p1, Lcom/google/android/apps/gmm/myplaces/c/k;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/j;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/myplaces/c/k;->h:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_2

    const/4 v3, 0x5

    iget-object v0, p1, Lcom/google/android/apps/gmm/myplaces/c/k;->h:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_0
    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v3, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_2
    const/4 v3, 0x2

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v3, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v0, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v8, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    return-object v1

    :cond_3
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/b/a/u;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/v;->a(Lcom/google/android/apps/gmm/map/b/a/u;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/e/a/a/a/b;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 46
    const/16 v0, 0x1a

    invoke-virtual {p1, v2, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 49
    const/4 v3, 0x2

    invoke-static {v0, v3, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IZ)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v4, 0x5

    .line 50
    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_2

    move v3, v2

    :goto_0
    if-nez v3, :cond_0

    invoke-virtual {v0, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_1
    if-nez v0, :cond_4

    :cond_1
    move v0, v2

    :goto_2
    return v0

    :cond_2
    move v3, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/myplaces/c/f;
    .locals 11

    .prologue
    .line 22
    const/4 v0, 0x2

    const/16 v1, 0x1c

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v0, 0x7

    const-wide/16 v2, 0x0

    invoke-static {p1, v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IJ)J

    move-result-wide v4

    const/4 v0, 0x1

    const/16 v2, 0x1a

    invoke-virtual {p1, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    const/16 v2, 0x8

    const-wide/16 v6, 0x0

    invoke-static {v0, v2, v6, v7}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IJ)J

    move-result-wide v2

    const/4 v0, 0x1

    const/16 v6, 0x1a

    invoke-virtual {p1, v0, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    const/4 v6, 0x3

    invoke-static {v0, v6}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0xa

    invoke-static {v0, v7}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0xb

    invoke-static {v0, v8}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v8

    const/4 v10, 0x5

    if-eqz v0, :cond_3

    iget-object v9, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v9, v10}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v9

    invoke-static {v9}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v9

    if-lez v9, :cond_1

    const/4 v9, 0x1

    :goto_0
    if-nez v9, :cond_0

    invoke-virtual {v0, v10}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_2

    :cond_0
    const/4 v9, 0x1

    :goto_1
    if-eqz v9, :cond_3

    const/16 v9, 0x1a

    invoke-virtual {v0, v10, v9}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    :goto_2
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/v;->b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/u;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/u;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v9

    :goto_3
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/k;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/myplaces/c/k;-><init>(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/q;)V

    return-object v0

    :cond_1
    const/4 v9, 0x0

    goto :goto_0

    :cond_2
    const/4 v9, 0x0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const/4 v9, 0x0

    goto :goto_3
.end method

.method public final c(Lcom/google/e/a/a/a/b;)J
    .locals 4

    .prologue
    .line 73
    const/4 v0, 0x1

    const/16 v1, 0x1a

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 74
    const/16 v1, 0x8

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d(Lcom/google/e/a/a/a/b;)Lcom/google/b/h/c;
    .locals 2

    .prologue
    .line 31
    const/4 v0, 0x1

    const/16 v1, 0x1a

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 32
    const/16 v1, 0xb

    .line 33
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    .line 32
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    .line 34
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-static {v0, v1}, Lcom/google/b/h/c;->a(J)Lcom/google/b/h/c;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/u;
    .locals 6

    .prologue
    const/16 v5, 0x1a

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 39
    invoke-virtual {p1, v2, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 40
    const/4 v4, 0x5

    .line 41
    if-eqz v0, :cond_3

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_2

    move v3, v2

    :goto_0
    if-nez v3, :cond_0

    invoke-virtual {v0, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    :cond_0
    move v1, v2

    :cond_1
    if-eqz v1, :cond_3

    invoke-virtual {v0, v4, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 40
    :goto_1
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/v;->b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/u;

    move-result-object v0

    return-object v0

    :cond_2
    move v3, v1

    .line 41
    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
