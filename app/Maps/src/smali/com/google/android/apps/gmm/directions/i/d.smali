.class public Lcom/google/android/apps/gmm/directions/i/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/d;
.implements Ljava/io/Serializable;


# instance fields
.field public transient a:Lcom/google/android/apps/gmm/directions/i/h;

.field public transient b:Lcom/google/android/libraries/curvular/ag;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/d;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/r/b/a/afz;

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/directions/i/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/gmm/directions/i/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/maps/g/a/hm;Lcom/google/r/b/a/afz;)V
    .locals 5

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/i/d;->c:Lcom/google/r/b/a/afz;

    .line 123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/d;->d:Ljava/util/ArrayList;

    .line 124
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne p2, v0, :cond_0

    .line 125
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/d;->d:Ljava/util/ArrayList;

    new-instance v2, Lcom/google/android/apps/gmm/directions/i/i;

    sget v0, Lcom/google/android/apps/gmm/l;->fv:I

    .line 126
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 127
    iget-object v0, p3, Lcom/google/r/b/a/afz;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ao;->d()Lcom/google/maps/g/a/ao;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ao;

    iget-boolean v0, v0, Lcom/google/maps/g/a/ao;->b:Z

    new-instance v4, Lcom/google/android/apps/gmm/directions/i/e;

    invoke-direct {v4, p0}, Lcom/google/android/apps/gmm/directions/i/e;-><init>(Lcom/google/android/apps/gmm/directions/i/d;)V

    invoke-direct {v2, v3, v0, v4, p0}, Lcom/google/android/apps/gmm/directions/i/i;-><init>(Ljava/lang/String;ZLcom/google/android/apps/gmm/directions/i/j;Lcom/google/android/apps/gmm/directions/i/d;)V

    .line 125
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/d;->d:Ljava/util/ArrayList;

    new-instance v2, Lcom/google/android/apps/gmm/directions/i/i;

    sget v0, Lcom/google/android/apps/gmm/l;->fw:I

    .line 137
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 138
    iget-object v0, p3, Lcom/google/r/b/a/afz;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ao;->d()Lcom/google/maps/g/a/ao;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ao;

    iget-boolean v0, v0, Lcom/google/maps/g/a/ao;->c:Z

    new-instance v4, Lcom/google/android/apps/gmm/directions/i/f;

    invoke-direct {v4, p0}, Lcom/google/android/apps/gmm/directions/i/f;-><init>(Lcom/google/android/apps/gmm/directions/i/d;)V

    invoke-direct {v2, v3, v0, v4, p0}, Lcom/google/android/apps/gmm/directions/i/i;-><init>(Ljava/lang/String;ZLcom/google/android/apps/gmm/directions/i/j;Lcom/google/android/apps/gmm/directions/i/d;)V

    .line 136
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    :cond_0
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-eq p2, v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    if-eq p2, v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne p2, v0, :cond_2

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/d;->d:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/apps/gmm/directions/i/i;

    sget v2, Lcom/google/android/apps/gmm/l;->fu:I

    .line 151
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 152
    iget-boolean v3, p3, Lcom/google/r/b/a/afz;->d:Z

    new-instance v4, Lcom/google/android/apps/gmm/directions/i/g;

    invoke-direct {v4, p0}, Lcom/google/android/apps/gmm/directions/i/g;-><init>(Lcom/google/android/apps/gmm/directions/i/d;)V

    invoke-direct {v1, v2, v3, v4, p0}, Lcom/google/android/apps/gmm/directions/i/i;-><init>(Ljava/lang/String;ZLcom/google/android/apps/gmm/directions/i/j;Lcom/google/android/apps/gmm/directions/i/d;)V

    .line 150
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/b/c/cv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    new-instance v0, Lcom/google/b/c/cx;

    invoke-direct {v0}, Lcom/google/b/c/cx;-><init>()V

    .line 179
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/d;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Iterable;)Lcom/google/b/c/cx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    .line 193
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/d;->a:Lcom/google/android/apps/gmm/directions/i/h;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/d;->c:Lcom/google/r/b/a/afz;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/d;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/i/i;

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/i/i;->b:Lcom/google/android/apps/gmm/directions/i/j;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/i/i;->b:Lcom/google/android/apps/gmm/directions/i/j;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/directions/i/i;->a:Z

    invoke-interface {v4, v0, v1}, Lcom/google/android/apps/gmm/directions/i/j;->a(ZLcom/google/r/b/a/afz;)Lcom/google/r/b/a/afz;

    move-result-object v0

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    goto :goto_1

    :cond_1
    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/directions/i/h;->a(Lcom/google/r/b/a/afz;)V

    .line 194
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/d;->a:Lcom/google/android/apps/gmm/directions/i/h;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/i/h;->i()V

    .line 200
    const/4 v0, 0x0

    return-object v0
.end method
