.class public Lcom/google/android/apps/gmm/p/g/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/c/a;

.field final b:Lcom/google/android/gms/location/e;

.field c:Z

.field d:Z

.field private final e:Lcom/google/android/apps/gmm/p/g/b;

.field private final f:Lcom/google/android/apps/gmm/map/location/rawlocationevents/e;

.field private g:Z

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/location/rawlocationevents/e;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/g/a;->g:Z

    .line 104
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/p/g/a;->c:Z

    .line 105
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/p/g/a;->h:Z

    .line 106
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/p/g/a;->i:Z

    .line 108
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/p/g/a;->d:Z

    .line 112
    new-instance v0, Lcom/google/android/apps/gmm/p/g/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/p/g/b;-><init>(Lcom/google/android/apps/gmm/p/g/a;)V

    .line 113
    new-instance v1, Lcom/google/android/gms/location/e;

    .line 114
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->a()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0, v0}, Lcom/google/android/gms/location/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;)V

    .line 116
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/g/a;->a:Lcom/google/android/apps/gmm/map/c/a;

    .line 117
    iput-object v0, p0, Lcom/google/android/apps/gmm/p/g/a;->e:Lcom/google/android/apps/gmm/p/g/b;

    .line 118
    iput-object v1, p0, Lcom/google/android/apps/gmm/p/g/a;->b:Lcom/google/android/gms/location/e;

    .line 119
    iput-object p2, p0, Lcom/google/android/apps/gmm/p/g/a;->f:Lcom/google/android/apps/gmm/map/location/rawlocationevents/e;

    .line 120
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 143
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 144
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/g/a;->d:Z

    if-eqz v0, :cond_0

    .line 146
    const-string v0, "GCoreRawLocationEventPoster"

    const-string v1, "start() called when already started."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/g/a;->b:Lcom/google/android/gms/location/e;

    invoke-virtual {v0}, Lcom/google/android/gms/location/e;->c()V

    .line 150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/g/a;->d:Z

    .line 151
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/car/a/d;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 180
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/car/a/d;->a:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/g/a;->i:Z

    .line 181
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/g/a;->i:Z

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x16

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "inProjectedMode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 182
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/g/a;->c()V

    .line 183
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 174
    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->b:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/g/a;->h:Z

    .line 175
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/g/a;->c()V

    .line 176
    return-void

    :cond_2
    move v2, v0

    .line 174
    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 155
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 156
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/g/a;->d:Z

    if-nez v0, :cond_0

    .line 158
    const-string v0, "GCoreRawLocationEventPoster"

    const-string v1, "stop() called when already stopped."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/g/a;->b:Lcom/google/android/gms/location/e;

    invoke-virtual {v0}, Lcom/google/android/gms/location/e;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/g/a;->b:Lcom/google/android/gms/location/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/g/a;->e:Lcom/google/android/apps/gmm/p/g/b;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/e;->a(Lcom/google/android/gms/location/f;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/g/a;->b:Lcom/google/android/gms/location/e;

    invoke-virtual {v0}, Lcom/google/android/gms/location/e;->d()V

    .line 165
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/p/g/a;->d:Z

    .line 166
    return-void
.end method

.method c()V
    .locals 4

    .prologue
    .line 196
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/p/g/a;->g:Z

    .line 199
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/g/a;->c:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/g/a;->h:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/g/a;->i:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/g/a;->g:Z

    .line 202
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/g/a;->g:Z

    if-eq v1, v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/g/a;->f:Lcom/google/android/apps/gmm/map/location/rawlocationevents/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/e;->h()V

    .line 205
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/g/a;->g:Z

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x3c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "updateAvailability() oldAvailability: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " available: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 208
    :cond_1
    return-void

    .line 199
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 217
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 219
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/g/a;->g:Z

    return v0
.end method
