.class Lcom/google/android/apps/gmm/suggest/c;
.super Lcom/google/android/apps/gmm/suggest/o;
.source "PG"


# instance fields
.field private final i:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final j:Lcom/google/android/apps/gmm/suggest/e/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final k:I

.field private final l:Lcom/google/e/a/a/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/android/apps/gmm/suggest/e/a;Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/suggest/e/b;Lcom/google/android/apps/gmm/suggest/n;Lcom/google/android/apps/gmm/suggest/d/g;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/b/a/r;I)V
    .locals 12
    .param p3    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/suggest/e/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Lcom/google/android/apps/gmm/map/r/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p9    # Lcom/google/android/apps/gmm/map/b/a/r;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 46
    sget-object v3, Lcom/google/r/b/a/el;->bT:Lcom/google/r/b/a/el;

    sget-object v4, Lcom/google/r/b/a/b/be;->b:Lcom/google/e/a/a/a/d;

    move-object v2, p0

    move-object v5, p1

    move-object v6, p2

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/gmm/suggest/o;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/android/apps/gmm/suggest/e/a;Lcom/google/android/apps/gmm/suggest/n;Lcom/google/android/apps/gmm/suggest/d/g;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/b/a/r;)V

    .line 48
    iput-object p3, p0, Lcom/google/android/apps/gmm/suggest/c;->i:Lcom/google/e/a/a/a/b;

    .line 49
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/c;->j:Lcom/google/android/apps/gmm/suggest/e/b;

    .line 50
    move/from16 v0, p10

    iput v0, p0, Lcom/google/android/apps/gmm/suggest/c;->k:I

    .line 51
    new-instance v4, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/r/b/a/b/be;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v4, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/o;->a:Lcom/google/android/apps/gmm/suggest/e/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/suggest/e/a;->a:Ljava/lang/String;

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/o;->a:Lcom/google/android/apps/gmm/suggest/e/a;

    iget v3, v3, Lcom/google/android/apps/gmm/suggest/e/a;->b:I

    int-to-long v6, v3

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/c;->i:Lcom/google/e/a/a/a/b;

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/c;->j:Lcom/google/android/apps/gmm/suggest/e/b;

    if-eqz v2, :cond_c

    const/4 v5, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/c;->j:Lcom/google/android/apps/gmm/suggest/e/b;

    new-instance v6, Lcom/google/e/a/a/a/b;

    sget-object v3, Lcom/google/maps/g/a/a/a;->m:Lcom/google/e/a/a/a/d;

    invoke-direct {v6, v3}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/4 v3, 0x2

    iget v7, v2, Lcom/google/android/apps/gmm/suggest/e/b;->b:I

    int-to-long v8, v7

    invoke-static {v8, v9}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v7

    iget-object v8, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v8, v3, v7}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v2, v2, Lcom/google/android/apps/gmm/suggest/e/b;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-static {}, Lcom/google/maps/g/a/je;->newBuilder()Lcom/google/maps/g/a/jg;

    move-result-object v8

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_2

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-nez v3, :cond_1

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    iget v9, v8, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v9, v9, 0x1

    iput v9, v8, Lcom/google/maps/g/a/jg;->a:I

    iput-object v3, v8, Lcom/google/maps/g/a/jg;->b:Ljava/lang/Object;

    :cond_2
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/j;->toString()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_3
    iget v9, v8, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v9, v9, 0x2

    iput v9, v8, Lcom/google/maps/g/a/jg;->a:I

    iput-object v3, v8, Lcom/google/maps/g/a/jg;->c:Ljava/lang/Object;

    :cond_4
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v3, :cond_8

    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_5

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/q;->b()Lcom/google/maps/g/gy;

    move-result-object v3

    invoke-virtual {v8, v3}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/a/jg;

    :cond_5
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->f:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-eqz v3, :cond_9

    const/4 v3, 0x1

    :goto_3
    if-eqz v3, :cond_6

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->f:Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/indoor/d/f;->a()Lcom/google/maps/g/by;

    move-result-object v3

    invoke-virtual {v8, v3}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/by;)Lcom/google/maps/g/a/jg;

    :cond_6
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v3, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v2, v3, :cond_a

    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_7

    sget-object v2, Lcom/google/maps/g/a/jh;->a:Lcom/google/maps/g/a/jh;

    invoke-virtual {v8, v2}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/a/jh;)Lcom/google/maps/g/a/jg;

    sget-object v2, Lcom/google/maps/g/a/jj;->d:Lcom/google/maps/g/a/jj;

    invoke-virtual {v8, v2}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/a/jj;)Lcom/google/maps/g/a/jg;

    :cond_7
    invoke-virtual {v8}, Lcom/google/maps/g/a/jg;->g()Lcom/google/n/t;

    move-result-object v2

    sget-object v3, Lcom/google/maps/g/a/a/a;->b:Lcom/google/e/a/a/a/d;

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v6, v3, v2}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    goto/16 :goto_0

    :cond_8
    const/4 v3, 0x0

    goto :goto_2

    :cond_9
    const/4 v3, 0x0

    goto :goto_3

    :cond_a
    const/4 v2, 0x0

    goto :goto_4

    :cond_b
    iget-object v2, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v5, v6}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/c;->j:Lcom/google/android/apps/gmm/suggest/e/b;

    iget-object v3, v3, Lcom/google/android/apps/gmm/suggest/e/b;->c:Lcom/google/r/b/a/afz;

    sget-object v5, Lcom/google/maps/g/a/a/a;->f:Lcom/google/e/a/a/a/d;

    invoke-static {v3, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v3

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_c
    const/4 v2, 0x7

    iget v3, p0, Lcom/google/android/apps/gmm/suggest/c;->k:I

    int-to-long v6, v3

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/o;->d:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/o;->d:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v2

    if-eqz v2, :cond_d

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/o;->d:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v3

    sget-object v5, Lcom/google/o/b/a/a/a;->f:Lcom/google/e/a/a/a/d;

    invoke-static {v3, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v3

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_d
    iput-object v4, p0, Lcom/google/android/apps/gmm/suggest/c;->l:Lcom/google/e/a/a/a/b;

    .line 52
    return-void
.end method


# virtual methods
.method protected final declared-synchronized a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 12

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/o;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    .line 89
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->c(Lcom/google/e/a/a/a/b;I)Lcom/google/e/a/a/a/b;

    move-result-object v5

    .line 92
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v6

    .line 93
    if-eqz v5, :cond_d

    .line 94
    const/4 v0, 0x3

    invoke-static {v5, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v7

    .line 95
    const/4 v0, 0x1

    iget-object v1, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v8

    .line 96
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v8, :cond_d

    .line 97
    const/4 v0, 0x1

    const/16 v4, 0x1a

    invoke-virtual {v5, v0, v1, v4}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 99
    const/4 v4, 0x1

    .line 100
    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->c(Lcom/google/e/a/a/a/b;I)Lcom/google/e/a/a/a/b;

    move-result-object v4

    .line 99
    invoke-static {v4}, Lcom/google/android/apps/gmm/suggest/e/d;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/suggest/e/e;

    move-result-object v9

    .line 101
    if-eqz v9, :cond_c

    .line 102
    const/4 v4, 0x5

    .line 105
    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->c(Lcom/google/e/a/a/a/b;I)Lcom/google/e/a/a/a/b;

    move-result-object v10

    .line 104
    if-eqz v10, :cond_0

    iget-object v4, v10, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    sget-object v11, Lcom/google/r/b/a/b/bd;->a:Lcom/google/e/a/a/a/d;

    invoke-virtual {v4, v11}, Lcom/google/e/a/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v4, 0x1

    :goto_1
    if-nez v4, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 104
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    :try_start_1
    iput-object v10, v9, Lcom/google/android/apps/gmm/suggest/e/e;->m:Lcom/google/e/a/a/a/b;

    .line 106
    const/4 v4, 0x6

    .line 107
    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->c(Lcom/google/e/a/a/a/b;I)Lcom/google/e/a/a/a/b;

    move-result-object v10

    .line 106
    if-eqz v10, :cond_3

    iget-object v4, v10, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    sget-object v11, Lcom/google/r/b/a/b/ax;->a:Lcom/google/e/a/a/a/d;

    invoke-virtual {v4, v11}, Lcom/google/e/a/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    const/4 v4, 0x1

    :goto_2
    if-nez v4, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_4
    const/4 v4, 0x0

    goto :goto_2

    :cond_5
    iput-object v10, v9, Lcom/google/android/apps/gmm/suggest/e/e;->n:Lcom/google/e/a/a/a/b;

    .line 109
    const/4 v4, 0x2

    .line 110
    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->c(Lcom/google/e/a/a/a/b;I)Lcom/google/e/a/a/a/b;

    move-result-object v10

    .line 109
    if-eqz v10, :cond_6

    iget-object v4, v10, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    sget-object v11, Lcom/google/r/b/a/b/bd;->d:Lcom/google/e/a/a/a/d;

    invoke-virtual {v4, v11}, Lcom/google/e/a/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_6
    const/4 v4, 0x1

    :goto_3
    if-nez v4, :cond_8

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_7
    const/4 v4, 0x0

    goto :goto_3

    :cond_8
    iput-object v10, v9, Lcom/google/android/apps/gmm/suggest/e/e;->o:Lcom/google/e/a/a/a/b;

    .line 112
    const/4 v4, 0x3

    .line 113
    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->f(Lcom/google/e/a/a/a/b;I)[B

    move-result-object v4

    .line 112
    iput-object v4, v9, Lcom/google/android/apps/gmm/suggest/e/e;->p:[B

    .line 115
    const/4 v4, 0x4

    .line 116
    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->c(Lcom/google/e/a/a/a/b;I)Lcom/google/e/a/a/a/b;

    move-result-object v4

    .line 115
    if-eqz v4, :cond_9

    iget-object v0, v4, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    sget-object v10, Lcom/google/r/b/a/b/aq;->b:Lcom/google/e/a/a/a/d;

    invoke-virtual {v0, v10}, Lcom/google/e/a/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    const/4 v0, 0x1

    :goto_4
    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_4

    :cond_b
    iput-object v4, v9, Lcom/google/android/apps/gmm/suggest/e/e;->r:Lcom/google/e/a/a/a/b;

    .line 118
    iput-object v7, v9, Lcom/google/android/apps/gmm/suggest/e/e;->q:Ljava/lang/String;

    .line 120
    invoke-virtual {v9}, Lcom/google/android/apps/gmm/suggest/e/e;->a()Lcom/google/android/apps/gmm/suggest/e/d;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 96
    :cond_c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 123
    :cond_d
    invoke-virtual {v6}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/c;->f:Lcom/google/b/c/cv;

    .line 124
    const/4 v0, 0x2

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/suggest/c;->h:I

    .line 126
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->f(Lcom/google/e/a/a/a/b;I)[B

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-static {v0}, Lcom/google/n/f;->a([B)Lcom/google/n/f;

    move-result-object v5

    .line 128
    :goto_5
    const/4 v0, 0x4

    .line 130
    invoke-static {}, Lcom/google/b/f/bj;->d()Lcom/google/b/f/bj;

    move-result-object v1

    .line 128
    if-eqz p1, :cond_f

    const/16 v4, 0x19

    invoke-virtual {p1, v0, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/at;)Lcom/google/n/at;

    move-result-object v6

    :goto_6
    check-cast v6, Lcom/google/b/f/bj;

    .line 131
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/o;->b:Lcom/google/android/apps/gmm/suggest/d/g;

    iget-object v4, p0, Lcom/google/android/apps/gmm/suggest/c;->f:Lcom/google/b/c/cv;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/gmm/suggest/d/g;->a(JLcom/google/b/c/cv;Lcom/google/n/f;Lcom/google/b/f/bj;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 133
    const/4 v0, 0x0

    monitor-exit p0

    return-object v0

    .line 126
    :cond_e
    const/4 v5, 0x0

    goto :goto_5

    .line 128
    :cond_f
    const/4 v6, 0x0

    goto :goto_6
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/c;->l:Lcom/google/e/a/a/a/b;

    return-object v0
.end method
