.class public Lcom/google/android/apps/gmm/mapsactivity/b/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/mapsactivity/a/h;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/gmm/mapsactivity/b/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 39
    return-void
.end method

.method private static a(Lcom/google/r/b/a/bz;Lcom/google/o/h/a/ch;)V
    .locals 4

    .prologue
    .line 93
    invoke-static {}, Lcom/google/r/b/a/ce;->newBuilder()Lcom/google/r/b/a/ci;

    move-result-object v1

    .line 94
    new-instance v0, Lcom/google/n/ai;

    iget-object v2, p1, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    sget-object v3, Lcom/google/o/h/a/ch;->b:Lcom/google/n/aj;

    invoke-direct {v0, v2, v3}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/cf;

    .line 96
    iget v0, v0, Lcom/google/o/h/a/cf;->i:I

    invoke-static {v0}, Lcom/google/r/b/a/cc;->a(I)Lcom/google/r/b/a/cc;

    move-result-object v0

    .line 95
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v1}, Lcom/google/r/b/a/ci;->c()V

    iget-object v3, v1, Lcom/google/r/b/a/ci;->a:Ljava/util/List;

    iget v0, v0, Lcom/google/r/b/a/cc;->i:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 98
    :cond_1
    new-instance v0, Lcom/google/n/ai;

    iget-object v2, p1, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    sget-object v3, Lcom/google/o/h/a/ch;->d:Lcom/google/n/aj;

    invoke-direct {v0, v2, v3}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ji;

    .line 100
    iget v0, v0, Lcom/google/o/h/a/ji;->ah:I

    invoke-static {v0}, Lcom/google/r/b/a/dd;->a(I)Lcom/google/r/b/a/dd;

    move-result-object v0

    .line 99
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    invoke-virtual {v1}, Lcom/google/r/b/a/ci;->d()V

    iget-object v3, v1, Lcom/google/r/b/a/ci;->b:Ljava/util/List;

    iget v0, v0, Lcom/google/r/b/a/dd;->ah:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 102
    :cond_3
    invoke-virtual {p0}, Lcom/google/r/b/a/bz;->i()V

    iget-object v0, p0, Lcom/google/r/b/a/bz;->e:Ljava/util/List;

    invoke-virtual {v1}, Lcom/google/r/b/a/ci;->g()Lcom/google/n/t;

    move-result-object v1

    new-instance v2, Lcom/google/n/ao;

    invoke-direct {v2}, Lcom/google/n/ao;-><init>()V

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/google/n/ao;->d:Z

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/shared/net/c;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/shared/net/c",
            "<",
            "Lcom/google/r/b/a/tw;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 43
    invoke-static {}, Lcom/google/r/b/a/tr;->newBuilder()Lcom/google/r/b/a/tt;

    move-result-object v1

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/b/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 45
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v2

    invoke-static {}, Lcom/google/r/b/a/bu;->newBuilder()Lcom/google/r/b/a/bz;

    move-result-object v3

    invoke-static {}, Lcom/google/android/apps/gmm/cardui/a;->a()V

    sget-object v0, Lcom/google/android/apps/gmm/cardui/f/a;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bw;

    iget v0, v0, Lcom/google/o/h/a/bw;->f:I

    invoke-static {v0}, Lcom/google/r/b/a/ca;->a(I)Lcom/google/r/b/a/ca;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v3}, Lcom/google/r/b/a/bz;->c()V

    iget-object v5, v3, Lcom/google/r/b/a/bz;->c:Ljava/util/List;

    iget v0, v0, Lcom/google/r/b/a/ca;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/cardui/f/a;->a:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bu;

    iget v0, v0, Lcom/google/o/h/a/bu;->e:I

    invoke-static {v0}, Lcom/google/r/b/a/df;->a(I)Lcom/google/r/b/a/df;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    invoke-virtual {v3}, Lcom/google/r/b/a/bz;->d()V

    iget-object v5, v3, Lcom/google/r/b/a/bz;->d:Ljava/util/List;

    iget v0, v0, Lcom/google/r/b/a/df;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/google/r/b/a/cm;->b:Lcom/google/r/b/a/cm;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget v4, v3, Lcom/google/r/b/a/bz;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/google/r/b/a/bz;->a:I

    iget v0, v0, Lcom/google/r/b/a/cm;->c:I

    iput v0, v3, Lcom/google/r/b/a/bz;->b:I

    sget-object v0, Lcom/google/android/apps/gmm/util/b/l;->a:Lcom/google/android/apps/gmm/util/b/ac;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/util/b/ac;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ch;

    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/mapsactivity/b/c;->a(Lcom/google/r/b/a/bz;Lcom/google/o/h/a/ch;)V

    goto :goto_2

    :cond_5
    sget-object v0, Lcom/google/r/b/a/ck;->a:Lcom/google/r/b/a/ck;

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/bz;->a(Lcom/google/r/b/a/ck;)Lcom/google/r/b/a/bz;

    sget-object v0, Lcom/google/r/b/a/ck;->b:Lcom/google/r/b/a/ck;

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/bz;->a(Lcom/google/r/b/a/ck;)Lcom/google/r/b/a/bz;

    sget-object v0, Lcom/google/r/b/a/ck;->e:Lcom/google/r/b/a/ck;

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/bz;->a(Lcom/google/r/b/a/ck;)Lcom/google/r/b/a/bz;

    if-eqz v2, :cond_6

    const/4 v0, 0x2

    iget v2, v3, Lcom/google/r/b/a/bz;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, v3, Lcom/google/r/b/a/bz;->a:I

    iput v0, v3, Lcom/google/r/b/a/bz;->f:I

    :cond_6
    invoke-virtual {v3}, Lcom/google/r/b/a/bz;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bu;

    .line 44
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget-object v2, v1, Lcom/google/r/b/a/tt;->b:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/r/b/a/tt;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/r/b/a/tt;->a:I

    .line 46
    invoke-static {}, Lcom/google/android/apps/gmm/cardui/a/a;->a()Lcom/google/r/b/a/bn;

    move-result-object v0

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget-object v2, v1, Lcom/google/r/b/a/tt;->c:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/r/b/a/tt;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v1, Lcom/google/r/b/a/tt;->a:I

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/b/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 48
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    const-class v2, Lcom/google/r/b/a/tr;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/shared/net/r;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/b;

    move-result-object v0

    .line 49
    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, p1, v2}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 50
    invoke-virtual {v1}, Lcom/google/r/b/a/tt;->g()Lcom/google/n/t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 51
    return-void
.end method
