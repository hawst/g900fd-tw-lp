.class public Lcom/google/android/apps/gmm/place/VerticalCardLayout;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# instance fields
.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->a:Z

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->b:Z

    .line 26
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->setOrientation(I)V

    .line 27
    invoke-virtual {p0, p0}, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 28
    return-void
.end method


# virtual methods
.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->b:Z

    .line 40
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->b:Z

    .line 45
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 49
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->b:Z

    if-ne v0, v1, :cond_1

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->getChildCount()I

    move-result v2

    if-eqz v2, :cond_0

    if-ne v2, v1, :cond_3

    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->a:Z

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/gmm/f;->au:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 51
    :cond_0
    :goto_1
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->b:Z

    .line 53
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 54
    return-void

    .line 50
    :cond_2
    sget v0, Lcom/google/android/apps/gmm/f;->bk:I

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->a:Z

    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/apps/gmm/f;->av:I

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_3
    add-int/lit8 v0, v2, -0x1

    if-ge v1, v0, :cond_6

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->a:Z

    if-eqz v0, :cond_5

    sget v0, Lcom/google/android/apps/gmm/f;->at:I

    :goto_4
    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundResource(I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    sget v0, Lcom/google/android/apps/gmm/f;->bk:I

    goto :goto_2

    :cond_5
    sget v0, Lcom/google/android/apps/gmm/f;->bk:I

    goto :goto_4

    :cond_6
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/VerticalCardLayout;->a:Z

    if-eqz v0, :cond_7

    sget v0, Lcom/google/android/apps/gmm/f;->as:I

    :goto_5
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    :cond_7
    sget v0, Lcom/google/android/apps/gmm/f;->bk:I

    goto :goto_5
.end method
