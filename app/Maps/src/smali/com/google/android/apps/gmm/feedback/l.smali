.class public Lcom/google/android/apps/gmm/feedback/l;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/feedback/a/e;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field volatile b:Z

.field c:Z

.field f:Lcom/google/android/apps/gmm/feedback/e;

.field g:Z

.field h:Z

.field i:I

.field private j:Lcom/google/android/apps/gmm/feedback/y;

.field private k:Lcom/google/android/apps/gmm/shared/a/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final l:Lcom/google/android/apps/gmm/shared/a/e;

.field private final m:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const-class v0, Lcom/google/android/apps/gmm/feedback/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/feedback/l;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 123
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/l;->g:Z

    .line 130
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/l;->h:Z

    .line 135
    iput v0, p0, Lcom/google/android/apps/gmm/feedback/l;->i:I

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->k:Lcom/google/android/apps/gmm/shared/a/c;

    .line 146
    new-instance v0, Lcom/google/android/apps/gmm/feedback/m;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/feedback/m;-><init>(Lcom/google/android/apps/gmm/feedback/l;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->l:Lcom/google/android/apps/gmm/shared/a/e;

    .line 159
    new-instance v0, Lcom/google/android/apps/gmm/feedback/n;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/feedback/n;-><init>(Lcom/google/android/apps/gmm/feedback/l;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->m:Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/feedback/l;)V
    .locals 7

    .prologue
    .line 74
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/e;->d:Lcom/google/android/apps/gmm/feedback/a/d;

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/feedback/e;->c:Z

    sget-object v0, Lcom/google/android/apps/gmm/feedback/q;->b:[I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/feedback/a/d;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_0

    invoke-static {v3, v4, v1}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->a(Lcom/google/android/apps/gmm/x/a;ZLcom/google/android/apps/gmm/feedback/a/d;)Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;

    move-result-object v0

    move-object v1, v0

    :goto_1
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/gmm/feedback/o;

    invoke-direct {v3, p0, v2, v1}, Lcom/google/android/apps/gmm/feedback/o;-><init>(Lcom/google/android/apps/gmm/feedback/l;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v3, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/feedback/l;->i()Lcom/google/android/apps/gmm/feedback/a/d;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v5, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    if-eqz v5, :cond_1

    check-cast v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->n()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    :goto_2
    invoke-static {v3, v4, v1, v0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->a(Lcom/google/android/apps/gmm/x/a;ZLcom/google/android/apps/gmm/feedback/a/d;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/feedback/l;->a:Ljava/lang/String;

    const-string v5, "Topfragment should be a GmmActivityFragment"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0, v5, v6}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    goto :goto_2

    :pswitch_1
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->L()Lcom/google/android/apps/gmm/streetview/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/streetview/a/a;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/feedback/a/d;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    invoke-static {v3, v4, v1, v0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->a(Lcom/google/android/apps/gmm/x/a;ZLcom/google/android/apps/gmm/feedback/a/d;Ljava/lang/String;)Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->a(Lcom/google/android/apps/gmm/x/a;ZLcom/google/android/apps/gmm/feedback/a/d;)Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/feedback/l;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/gmm/feedback/l;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/gmm/feedback/l;)V
    .locals 3

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {}, Lcom/google/android/apps/gmm/feedback/ShakenDialog;->a()Lcom/google/android/apps/gmm/feedback/ShakenDialog;

    move-result-object v1

    const-string v2, "ShakenDialog"

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Landroid/app/DialogFragment;Ljava/lang/String;)Z

    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/gmm/feedback/l;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/gmm/feedback/l;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/gmm/feedback/l;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/gmm/feedback/l;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method private i()Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    .line 378
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/apps/gmm/feedback/a/c;

    if-eqz v1, :cond_0

    .line 379
    check-cast v0, Lcom/google/android/apps/gmm/feedback/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/feedback/a/c;->m()Lcom/google/android/apps/gmm/feedback/a/d;

    move-result-object v0

    .line 381
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->a:Lcom/google/android/apps/gmm/feedback/a/d;

    goto :goto_0
.end method


# virtual methods
.method public final Y_()V
    .locals 2

    .prologue
    .line 492
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/l;->m:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->j:Lcom/google/android/apps/gmm/feedback/y;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/feedback/y;->a()V

    .line 495
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/feedback/a/g;)Lcom/google/android/apps/gmm/feedback/a/f;
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/feedback/a/g;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/feedback/v;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/feedback/a/g;)Lcom/google/android/apps/gmm/feedback/a/f;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 611
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 612
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    .line 613
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;

    if-eqz v1, :cond_0

    .line 615
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 619
    :goto_0
    return-void

    .line 617
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/feedback/l;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x43

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "no need to popBackStack, feedback activity resultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 2

    .prologue
    .line 453
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 455
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 456
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 455
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/feedback/z;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;)Lcom/google/android/apps/gmm/feedback/z;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->j:Lcom/google/android/apps/gmm/feedback/y;

    .line 457
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 430
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/l;->b:Z

    .line 431
    new-instance v0, Lcom/google/android/apps/gmm/feedback/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/gmm/feedback/a/d;->n:Lcom/google/android/apps/gmm/feedback/a/d;

    move v3, v2

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/feedback/e;-><init>(Lcom/google/android/apps/gmm/base/activities/c;ZZLcom/google/android/apps/gmm/feedback/a/a;Lcom/google/android/apps/gmm/feedback/a/d;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    .line 433
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/feedback/e;->a()V

    .line 434
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 649
    if-eqz p1, :cond_0

    .line 650
    iget v0, p0, Lcom/google/android/apps/gmm/feedback/l;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/feedback/l;->i:I

    .line 654
    :goto_0
    return-void

    .line 652
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/feedback/l;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/feedback/l;->i:I

    goto :goto_0
.end method

.method public final a(ZLcom/google/android/apps/gmm/feedback/a/d;)V
    .locals 2
    .param p2    # Lcom/google/android/apps/gmm/feedback/a/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 444
    if-nez p2, :cond_0

    .line 445
    invoke-direct {p0}, Lcom/google/android/apps/gmm/feedback/l;->i()Lcom/google/android/apps/gmm/feedback/a/d;

    move-result-object p2

    .line 447
    :cond_0
    const/4 v0, 0x0

    .line 448
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1, p2, v0}, Lcom/google/android/apps/gmm/feedback/l;->a(ZZLcom/google/android/apps/gmm/feedback/a/d;Lcom/google/android/apps/gmm/feedback/a/a;)V

    .line 449
    return-void
.end method

.method public final a(ZZLcom/google/android/apps/gmm/feedback/a/d;Lcom/google/android/apps/gmm/feedback/a/a;)V
    .locals 7

    .prologue
    .line 422
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/l;->b:Z

    .line 423
    new-instance v0, Lcom/google/android/apps/gmm/feedback/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v6, 0x0

    move v2, p1

    move v3, p2

    move-object v4, p4

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/feedback/e;-><init>(Lcom/google/android/apps/gmm/base/activities/c;ZZLcom/google/android/apps/gmm/feedback/a/a;Lcom/google/android/apps/gmm/feedback/a/d;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    .line 425
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/feedback/e;->a()V

    .line 426
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 499
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->b()V

    .line 500
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 501
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/l;->m:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 502
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/l;->b:Z

    if-eqz v0, :cond_0

    .line 503
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/feedback/h;

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->h:Lcom/google/android/apps/gmm/feedback/i;

    iget-object v3, p0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/feedback/h;-><init>(Lcom/google/android/apps/gmm/feedback/i;Lcom/google/android/apps/gmm/feedback/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 506
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->j:Lcom/google/android/apps/gmm/feedback/y;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/feedback/y;->b()V

    .line 507
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 630
    new-instance v0, Lcom/google/android/apps/gmm/feedback/p;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/feedback/p;-><init>(Lcom/google/android/apps/gmm/feedback/l;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/feedback/v;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/feedback/a/g;)Lcom/google/android/apps/gmm/feedback/a/f;

    .line 645
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {}, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->a()Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 439
    return-void
.end method

.method public final d()V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 535
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    .line 536
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    if-nez v0, :cond_2

    move-object v1, v2

    .line 541
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    if-nez v0, :cond_4

    .line 543
    :goto_1
    sget-object v0, Lcom/google/android/apps/gmm/feedback/l;->a:Ljava/lang/String;

    const-string v0, "Feedback(shouldUseBundle="

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x21

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", shouldUseScreenshot=true"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 546
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->k:Lcom/google/android/apps/gmm/shared/a/c;

    if-eqz v0, :cond_d

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    .line 550
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move v3, v5

    :cond_1
    if-eqz v3, :cond_e

    .line 551
    const-string v0, "anonymous"

    move-object v3, v0

    .line 553
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->s()Lcom/google/r/b/a/gf;

    move-result-object v0

    .line 554
    iget v0, v0, Lcom/google/r/b/a/gf;->b:I

    .line 555
    if-eqz v1, :cond_b

    if-lez v0, :cond_b

    .line 556
    if-gtz v0, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "the longest edge limit must be >0."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 536
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    iget-object v1, v0, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    iget-object v1, v1, Lcom/google/android/apps/gmm/feedback/g;->c:Lcom/google/android/apps/gmm/feedback/a/f;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/g;->c:Lcom/google/android/apps/gmm/feedback/a/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/feedback/a/f;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    :cond_3
    move-object v1, v2

    goto/16 :goto_0

    .line 541
    :cond_4
    iget-object v6, p0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    iget-object v0, v6, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/g;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    if-eqz v0, :cond_5

    const-string v0, "Viewport link url"

    iget-object v7, v6, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v7, v7, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v7, v2}, Lcom/google/android/apps/gmm/map/t;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v0, v2}, Lcom/google/android/apps/gmm/feedback/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "CameraPosition"

    iget-object v2, v6, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/feedback/g;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/f/a/a;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v0, v2}, Lcom/google/android/apps/gmm/feedback/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v0, v6, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/g;->e:Lcom/google/android/apps/gmm/feedback/a/a;

    if-eqz v0, :cond_9

    iget-object v0, v6, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/g;->e:Lcom/google/android/apps/gmm/feedback/a/a;

    iget-object v7, v0, Lcom/google/android/apps/gmm/feedback/a/a;->b:Ljava/util/List;

    move v2, v3

    :goto_3
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    const-string v0, "Debug URL:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v8, v2, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0xb

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v4, v8, v0}, Lcom/google/android/apps/gmm/feedback/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_6
    iget-object v0, v6, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/g;->e:Lcom/google/android/apps/gmm/feedback/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/a/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/feedback/a/b;

    const-string v8, "proto: "

    iget-object v2, v0, Lcom/google/android/apps/gmm/feedback/a/b;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_7

    invoke-virtual {v8, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/a/b;->b:Ljava/lang/String;

    invoke-static {v4, v2, v0}, Lcom/google/android/apps/gmm/feedback/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_7
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    :cond_8
    iget-object v0, v6, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/g;->e:Lcom/google/android/apps/gmm/feedback/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/a/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/feedback/a/b;

    iget-object v7, v0, Lcom/google/android/apps/gmm/feedback/a/b;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/a/b;->b:Ljava/lang/String;

    invoke-static {v4, v7, v0}, Lcom/google/android/apps/gmm/feedback/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :cond_9
    const-string v0, "LocationState"

    iget-object v2, v6, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/feedback/g;->g:Ljava/lang/String;

    invoke-static {v4, v0, v2}, Lcom/google/android/apps/gmm/feedback/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "LocationScanState"

    iget-object v2, v6, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/feedback/g;->h:Ljava/lang/String;

    invoke-static {v4, v0, v2}, Lcom/google/android/apps/gmm/feedback/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "LocationRadius"

    iget-object v2, v6, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/feedback/g;->i:Ljava/lang/String;

    invoke-static {v4, v0, v2}, Lcom/google/android/apps/gmm/feedback/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "LocationFeedback"

    iget-object v2, v6, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/feedback/g;->j:Ljava/lang/String;

    invoke-static {v4, v0, v2}, Lcom/google/android/apps/gmm/feedback/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v4

    goto/16 :goto_1

    .line 556
    :cond_a
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    move-result v7

    if-gt v7, v0, :cond_c

    .line 557
    :goto_7
    sget-object v0, Lcom/google/android/apps/gmm/feedback/l;->a:Ljava/lang/String;

    .line 558
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x30

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "ScreenShot is resized to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "x"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 557
    :cond_b
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->k:Lcom/google/android/apps/gmm/shared/a/c;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/a/c;->a(Landroid/graphics/Bitmap;Landroid/os/Bundle;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 575
    :goto_8
    return-void

    .line 556
    :cond_c
    int-to-long v8, v4

    int-to-long v10, v0

    mul-long/2addr v8, v10

    int-to-long v10, v7

    div-long/2addr v8, v10

    long-to-int v4, v8

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    int-to-long v8, v6

    int-to-long v10, v0

    mul-long/2addr v8, v10

    int-to-long v6, v7

    div-long v6, v8, v6

    long-to-int v0, v6

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_7

    .line 565
    :catch_0
    move-exception v0

    .line 568
    const-string v1, "feedback fallen back"

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "gCore feedback failure: fallen back to gmm feedback"

    invoke-direct {v2, v3, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 570
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/l;->e()V

    goto :goto_8

    .line 573
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/l;->e()V

    goto :goto_8

    :cond_e
    move-object v3, v0

    goto/16 :goto_2
.end method

.method e()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 586
    iput-object v2, p0, Lcom/google/android/apps/gmm/feedback/l;->k:Lcom/google/android/apps/gmm/shared/a/c;

    .line 588
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    if-eqz v0, :cond_9

    .line 589
    iget-object v6, p0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v7, v6, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->k:Lcom/google/android/apps/gmm/feedback/a/d;

    iget-object v1, v7, Lcom/google/android/apps/gmm/feedback/g;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->l:Lcom/google/android/apps/gmm/feedback/a/d;

    iget-object v1, v7, Lcom/google/android/apps/gmm/feedback/g;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    if-ne v0, v1, :cond_2

    :cond_0
    new-instance v0, Lcom/google/userfeedback/android/api/UserFeedbackSpec;

    iget-object v1, v6, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    const-string v3, "com.google.android.apps.gmm:V *:S"

    const-string v4, "com.google.android.apps.gmm.USER_INITIATED_FEEDBACK_REPORT"

    const-string v5, "CurrentLocationReports"

    invoke-direct/range {v0 .. v5}, Lcom/google/userfeedback/android/api/UserFeedbackSpec;-><init>(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    :goto_0
    iget-object v0, v7, Lcom/google/android/apps/gmm/feedback/g;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/userfeedback/android/api/UserFeedbackSpec;->setSelectedAccount(Ljava/lang/String;)V

    iget-object v0, v7, Lcom/google/android/apps/gmm/feedback/g;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    if-eqz v0, :cond_1

    const-string v0, "Viewport link url"

    iget-object v3, v6, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/t;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/feedback/e;->a(Lcom/google/userfeedback/android/api/UserFeedbackSpec;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "CameraPosition"

    iget-object v2, v7, Lcom/google/android/apps/gmm/feedback/g;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/f/a/a;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/feedback/e;->a(Lcom/google/userfeedback/android/api/UserFeedbackSpec;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, v7, Lcom/google/android/apps/gmm/feedback/g;->e:Lcom/google/android/apps/gmm/feedback/a/a;

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    iget-object v2, v7, Lcom/google/android/apps/gmm/feedback/g;->e:Lcom/google/android/apps/gmm/feedback/a/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/feedback/a/a;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "Debug URL:"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v2, v2, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0xb

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v0}, Lcom/google/android/apps/gmm/feedback/e;->a(Lcom/google/userfeedback/android/api/UserFeedbackSpec;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/google/userfeedback/android/api/UserFeedbackSpec;

    iget-object v1, v6, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    const-string v3, "com.google.android.apps.gmm:V *:S"

    const-string v4, "com.google.android.apps.gmm.USER_INITIATED_FEEDBACK_REPORT"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/userfeedback/android/api/UserFeedbackSpec;-><init>(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_0

    :cond_3
    iget-object v0, v7, Lcom/google/android/apps/gmm/feedback/g;->e:Lcom/google/android/apps/gmm/feedback/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/a/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/feedback/a/b;

    const-string v4, "proto: "

    iget-object v2, v0, Lcom/google/android/apps/gmm/feedback/a/b;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v4, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/a/b;->b:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/feedback/e;->a(Lcom/google/userfeedback/android/api/UserFeedbackSpec;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    iget-object v0, v7, Lcom/google/android/apps/gmm/feedback/g;->e:Lcom/google/android/apps/gmm/feedback/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/a/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/feedback/a/b;

    iget-object v3, v0, Lcom/google/android/apps/gmm/feedback/a/b;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/a/b;->b:Ljava/lang/String;

    invoke-static {v1, v3, v0}, Lcom/google/android/apps/gmm/feedback/e;->a(Lcom/google/userfeedback/android/api/UserFeedbackSpec;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_6
    iget-object v0, v7, Lcom/google/android/apps/gmm/feedback/g;->c:Lcom/google/android/apps/gmm/feedback/a/f;

    if-eqz v0, :cond_7

    iget-object v0, v7, Lcom/google/android/apps/gmm/feedback/g;->c:Lcom/google/android/apps/gmm/feedback/a/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/feedback/a/f;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, v7, Lcom/google/android/apps/gmm/feedback/g;->c:Lcom/google/android/apps/gmm/feedback/a/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/feedback/a/f;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/userfeedback/android/api/UserFeedbackSpec;->setScreenshot(Landroid/graphics/Bitmap;)V

    :cond_7
    const-string v0, "LocationState"

    iget-object v2, v7, Lcom/google/android/apps/gmm/feedback/g;->g:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/feedback/e;->a(Lcom/google/userfeedback/android/api/UserFeedbackSpec;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "LocationScanState"

    iget-object v2, v7, Lcom/google/android/apps/gmm/feedback/g;->h:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/feedback/e;->a(Lcom/google/userfeedback/android/api/UserFeedbackSpec;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "LocationRadius"

    iget-object v2, v7, Lcom/google/android/apps/gmm/feedback/g;->i:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/feedback/e;->a(Lcom/google/userfeedback/android/api/UserFeedbackSpec;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "LocationFeedback"

    iget-object v2, v7, Lcom/google/android/apps/gmm/feedback/g;->j:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/feedback/e;->a(Lcom/google/userfeedback/android/api/UserFeedbackSpec;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/userfeedback/android/api/UserFeedback;

    invoke-direct {v0}, Lcom/google/userfeedback/android/api/UserFeedback;-><init>()V

    iget-object v2, v6, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v3, Lcom/google/android/apps/gmm/feedback/f;

    invoke-direct {v3, v6, v0, v1}, Lcom/google/android/apps/gmm/feedback/f;-><init>(Lcom/google/android/apps/gmm/feedback/e;Lcom/google/userfeedback/android/api/UserFeedback;Lcom/google/userfeedback/android/api/UserFeedbackSpec;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 593
    :cond_8
    :goto_5
    return-void

    .line 591
    :cond_9
    sget-object v0, Lcom/google/android/apps/gmm/feedback/l;->a:Ljava/lang/String;

    goto :goto_5
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 461
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->f()V

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->k:Lcom/google/android/apps/gmm/shared/a/c;

    if-eqz v0, :cond_0

    .line 488
    :goto_0
    return-void

    .line 469
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 473
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    .line 474
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->k:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 477
    :goto_1
    if-nez v0, :cond_2

    .line 478
    sget-object v0, Lcom/google/android/apps/gmm/feedback/l;->a:Ljava/lang/String;

    goto :goto_0

    .line 474
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 482
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    .line 483
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->l:Lcom/google/android/apps/gmm/shared/a/e;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/a/c;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/a/e;)Lcom/google/android/apps/gmm/shared/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->k:Lcom/google/android/apps/gmm/shared/a/c;

    goto :goto_0
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 511
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->g()V

    .line 512
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 516
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->h()V

    .line 517
    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->j:Lcom/google/android/apps/gmm/feedback/y;

    .line 518
    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/l;->k:Lcom/google/android/apps/gmm/shared/a/c;

    .line 519
    return-void
.end method
