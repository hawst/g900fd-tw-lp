.class Lcom/google/android/apps/gmm/streetview/internal/z;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ljava/lang/String;

.field b:I

.field c:I

.field d:I

.field private e:I


# direct methods
.method public constructor <init>(Ljava/lang/String;III)V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/z;->a:Ljava/lang/String;

    add-int/lit8 v0, p4, 0x1

    shl-int v0, p3, v0

    or-int/2addr v0, p2

    xor-int/2addr v0, p4

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/z;->e:I

    iput p2, p0, Lcom/google/android/apps/gmm/streetview/internal/z;->b:I

    iput p3, p0, Lcom/google/android/apps/gmm/streetview/internal/z;->c:I

    iput p4, p0, Lcom/google/android/apps/gmm/streetview/internal/z;->d:I

    .line 20
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 40
    if-ne p1, p0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v0

    .line 43
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/streetview/internal/z;

    if-nez v2, :cond_2

    move v0, v1

    .line 44
    goto :goto_0

    .line 46
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/streetview/internal/z;

    .line 47
    iget v2, p1, Lcom/google/android/apps/gmm/streetview/internal/z;->b:I

    iget v3, p0, Lcom/google/android/apps/gmm/streetview/internal/z;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p1, Lcom/google/android/apps/gmm/streetview/internal/z;->c:I

    iget v3, p0, Lcom/google/android/apps/gmm/streetview/internal/z;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p1, Lcom/google/android/apps/gmm/streetview/internal/z;->d:I

    iget v3, p0, Lcom/google/android/apps/gmm/streetview/internal/z;->d:I

    if-ne v2, v3, :cond_3

    iget-object v2, p1, Lcom/google/android/apps/gmm/streetview/internal/z;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/z;->a:Ljava/lang/String;

    .line 50
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/z;->e:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/z;->a:Ljava/lang/String;

    .line 61
    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/z;->d:I

    .line 62
    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/z;->b:I

    .line 63
    iget v3, p0, Lcom/google/android/apps/gmm/streetview/internal/z;->c:I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x34

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "panoid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "&zoom="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
