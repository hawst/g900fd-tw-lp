.class public final enum Lcom/google/android/apps/gmm/b/d;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/b/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/b/d;

.field public static final enum b:Lcom/google/android/apps/gmm/b/d;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/b/d;


# instance fields
.field c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 50
    new-instance v0, Lcom/google/android/apps/gmm/b/d;

    const-string v1, "APP_OPEN"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/b/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/b/d;->a:Lcom/google/android/apps/gmm/b/d;

    .line 51
    new-instance v0, Lcom/google/android/apps/gmm/b/d;

    const-string v1, "MY_LOCATION"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/apps/gmm/b/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/b/d;->b:Lcom/google/android/apps/gmm/b/d;

    .line 49
    new-array v0, v4, [Lcom/google/android/apps/gmm/b/d;

    sget-object v1, Lcom/google/android/apps/gmm/b/d;->a:Lcom/google/android/apps/gmm/b/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/b/d;->b:Lcom/google/android/apps/gmm/b/d;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/gmm/b/d;->d:[Lcom/google/android/apps/gmm/b/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput p3, p0, Lcom/google/android/apps/gmm/b/d;->c:I

    .line 57
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/b/d;
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/google/android/apps/gmm/b/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/b/d;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/b/d;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/apps/gmm/b/d;->d:[Lcom/google/android/apps/gmm/b/d;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/b/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/b/d;

    return-object v0
.end method
