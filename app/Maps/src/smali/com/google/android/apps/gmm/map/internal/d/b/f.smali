.class Lcom/google/android/apps/gmm/map/internal/d/b/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/b/k;


# static fields
.field private static final a:Lcom/google/android/apps/gmm/map/internal/d/b/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 406
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/m;

    const/16 v1, 0x26

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/m;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/f;->a:Lcom/google/android/apps/gmm/map/internal/d/b/d;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 485
    const/4 v0, 0x6

    return v0
.end method

.method public final a([B)Lcom/google/android/apps/gmm/map/internal/d/b/n;
    .locals 10

    .prologue
    const-wide v8, 0xffffffffL

    const/16 v2, 0x22

    const/16 v6, 0x20

    .line 466
    const/4 v0, 0x0

    .line 467
    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    .line 468
    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a([BI)I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 469
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/o;-><init>()V

    .line 470
    iput-object p0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 471
    const/4 v1, 0x5

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v2, v1

    const/16 v1, 0x9

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v4, v1

    and-long/2addr v4, v8

    shl-long/2addr v2, v6

    or-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->c:J

    .line 472
    const/16 v1, 0xd

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v2, v1

    const/16 v1, 0x11

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v4, v1

    and-long/2addr v4, v8

    shl-long/2addr v2, v6

    or-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->b:J

    .line 474
    const/16 v1, 0x15

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->f:I

    .line 475
    const/16 v1, 0x19

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->g:I

    .line 476
    const/16 v1, 0x1d

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->h:I

    .line 477
    const/16 v1, 0x21

    aget-byte v1, p1, v1

    iput-byte v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->i:B

    .line 478
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a()Lcom/google/android/apps/gmm/map/internal/d/b/n;

    move-result-object v0

    .line 480
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/d/b/n;[B)V
    .locals 8

    .prologue
    const/16 v7, 0x22

    const/16 v6, 0x20

    .line 450
    const/4 v0, 0x0

    const/16 v1, 0x26

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 452
    const/4 v0, 0x4

    const/4 v1, 0x6

    aput-byte v1, p2, v0

    .line 453
    const/4 v0, 0x5

    .line 454
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    .line 453
    shr-long v4, v2, v6

    long-to-int v1, v4

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    const/16 v0, 0x9

    long-to-int v1, v2

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 455
    const/16 v0, 0xd

    .line 456
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 455
    shr-long v4, v2, v6

    long-to-int v1, v4

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    const/16 v0, 0x11

    long-to-int v1, v2

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 457
    const/16 v0, 0x15

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->f:I

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 458
    const/16 v0, 0x19

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->g:I

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 459
    const/16 v0, 0x1d

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->h:I

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 460
    const/16 v0, 0x21

    iget-byte v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->i:B

    aput-byte v1, p2, v0

    .line 462
    invoke-static {p2, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a([BI)I

    move-result v0

    .line 461
    invoke-static {p2, v7, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 463
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 490
    const/16 v0, 0x26

    return v0
.end method

.method public final c()Lcom/google/android/apps/gmm/map/internal/d/b/d;
    .locals 1

    .prologue
    .line 495
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/f;->a:Lcom/google/android/apps/gmm/map/internal/d/b/d;

    return-object v0
.end method
