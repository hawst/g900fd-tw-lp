.class public final enum Lcom/google/android/apps/gmm/iamhere/c/q;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/iamhere/c/q;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/iamhere/c/q;

.field public static final enum b:Lcom/google/android/apps/gmm/iamhere/c/q;

.field public static final enum c:Lcom/google/android/apps/gmm/iamhere/c/q;

.field public static final enum d:Lcom/google/android/apps/gmm/iamhere/c/q;

.field public static final enum e:Lcom/google/android/apps/gmm/iamhere/c/q;

.field public static final enum f:Lcom/google/android/apps/gmm/iamhere/c/q;

.field public static final enum g:Lcom/google/android/apps/gmm/iamhere/c/q;

.field public static final enum h:Lcom/google/android/apps/gmm/iamhere/c/q;

.field private static final synthetic i:[Lcom/google/android/apps/gmm/iamhere/c/q;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/q;

    const-string v1, "NEUTRAL"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/iamhere/c/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->a:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 47
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/q;

    const-string v1, "LOW_CONFIDENCE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/iamhere/c/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->b:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/q;

    const-string v1, "HIGH_CONFIDENCE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/iamhere/c/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->c:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/q;

    const-string v1, "NO_CONFIDENCE"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/iamhere/c/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->d:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/q;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/iamhere/c/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->e:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 67
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/q;

    const-string v1, "CONNECTIVITY_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/c/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/q;

    const-string v1, "GAIA_ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/c/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->g:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/q;

    const-string v1, "CONFIRMED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/c/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 37
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/q;->a:Lcom/google/android/apps/gmm/iamhere/c/q;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/q;->b:Lcom/google/android/apps/gmm/iamhere/c/q;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/q;->c:Lcom/google/android/apps/gmm/iamhere/c/q;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/q;->d:Lcom/google/android/apps/gmm/iamhere/c/q;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/q;->e:Lcom/google/android/apps/gmm/iamhere/c/q;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/q;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/q;->g:Lcom/google/android/apps/gmm/iamhere/c/q;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->i:[Lcom/google/android/apps/gmm/iamhere/c/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Lcom/google/maps/g/ta;)Lcom/google/android/apps/gmm/iamhere/c/q;
    .locals 4

    .prologue
    .line 78
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/p;->a:[I

    invoke-virtual {p0}, Lcom/google/maps/g/ta;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 86
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->a:Ljava/lang/String;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled confidence level: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->e:Lcom/google/android/apps/gmm/iamhere/c/q;

    :goto_0
    return-object v0

    .line 80
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->c:Lcom/google/android/apps/gmm/iamhere/c/q;

    goto :goto_0

    .line 82
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->b:Lcom/google/android/apps/gmm/iamhere/c/q;

    goto :goto_0

    .line 84
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->d:Lcom/google/android/apps/gmm/iamhere/c/q;

    goto :goto_0

    .line 78
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/iamhere/c/q;
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/q;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/iamhere/c/q;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->i:[Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/iamhere/c/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/iamhere/c/q;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 96
    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/p;->b:[I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/c/q;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 106
    :goto_0
    :pswitch_0
    return v0

    .line 100
    :pswitch_1
    const/16 v0, 0x19

    goto :goto_0

    .line 102
    :pswitch_2
    const/16 v0, 0x46

    goto :goto_0

    .line 104
    :pswitch_3
    const/16 v0, 0x64

    goto :goto_0

    .line 96
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
