.class public Lcom/google/android/apps/gmm/startpage/w;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/startpage/ag;

.field public final c:Lcom/google/android/apps/gmm/startpage/e;

.field private final d:Lcom/google/android/apps/gmm/base/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/gmm/startpage/w;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/startpage/w;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;)V
    .locals 3
    .param p3    # Lcom/google/android/apps/gmm/map/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/startpage/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/startpage/e;-><init>()V

    new-instance v1, Lcom/google/android/apps/gmm/startpage/ag;

    new-instance v2, Lcom/google/android/apps/gmm/startpage/j;

    invoke-direct {v2, p2}, Lcom/google/android/apps/gmm/startpage/j;-><init>(Lcom/google/android/apps/gmm/base/a;)V

    invoke-direct {v1, p2, p3, p1, v2}, Lcom/google/android/apps/gmm/startpage/ag;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/Context;Lcom/google/android/apps/gmm/startpage/j;)V

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/apps/gmm/startpage/w;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/startpage/e;Lcom/google/android/apps/gmm/startpage/ag;)V

    .line 47
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/startpage/e;Lcom/google/android/apps/gmm/startpage/ag;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/w;->d:Lcom/google/android/apps/gmm/base/a;

    .line 54
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/w;->c:Lcom/google/android/apps/gmm/startpage/e;

    .line 55
    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    .line 56
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 91
    sget-object v0, Lcom/google/android/apps/gmm/startpage/w;->a:Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/ag;->d:Lcom/google/android/apps/gmm/startpage/j;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/startpage/j;->b:Lcom/google/o/h/a/da;

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/j;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/j;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/j;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/e;->d()V

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->c:Lcom/google/android/apps/gmm/startpage/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/e;->c()V

    .line 94
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/startpage/ac;Z)V
    .locals 5

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/ag;->e:Lcom/google/android/apps/gmm/startpage/y;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/startpage/ac;->a()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/startpage/ag;->a(Landroid/accounts/Account;)Z

    move-result v2

    invoke-virtual {v1, p1, v0, v2}, Lcom/google/android/apps/gmm/startpage/y;->b(Lcom/google/android/apps/gmm/startpage/ac;Lcom/google/android/apps/gmm/startpage/ae;Z)Lcom/google/android/apps/gmm/startpage/aa;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    .line 117
    :goto_0
    return-void

    .line 116
    :cond_0
    if-eqz p2, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/gmm/startpage/ag;->d:Lcom/google/android/apps/gmm/startpage/j;

    iget-object v2, v2, Lcom/google/android/apps/gmm/startpage/j;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->p:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/ag;->b(Lcom/google/android/apps/gmm/startpage/aa;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/startpage/b/a;)V
    .locals 6
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 160
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v2, v2, Lcom/google/android/apps/gmm/startpage/ag;->d:Lcom/google/android/apps/gmm/startpage/j;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/j;->a()Lcom/google/o/h/a/gt;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/o/h/a/gt;->h:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/w;->c:Lcom/google/android/apps/gmm/startpage/e;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/e;->a()V

    .line 161
    :goto_0
    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/b/a;->a:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v2, :cond_7

    .line 162
    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/b/a;->d:Lcom/google/android/apps/gmm/startpage/b/b;

    sget-object v3, Lcom/google/android/apps/gmm/startpage/b/b;->c:Lcom/google/android/apps/gmm/startpage/b/b;

    if-ne v2, v3, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->c:Lcom/google/android/apps/gmm/startpage/e;

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/b/a;->a:Lcom/google/android/apps/gmm/base/g/c;

    iget-wide v2, p1, Lcom/google/android/apps/gmm/startpage/b/a;->e:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/startpage/e;->a(Lcom/google/android/apps/gmm/base/g/c;J)V

    .line 173
    :cond_0
    :goto_2
    return-void

    .line 160
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/w;->c:Lcom/google/android/apps/gmm/startpage/e;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/e;->b()V

    goto :goto_0

    :cond_2
    move v2, v1

    .line 162
    goto :goto_1

    .line 164
    :cond_3
    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/b/a;->d:Lcom/google/android/apps/gmm/startpage/b/b;

    sget-object v3, Lcom/google/android/apps/gmm/startpage/b/b;->d:Lcom/google/android/apps/gmm/startpage/b/b;

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_3
    if-eqz v2, :cond_5

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->c:Lcom/google/android/apps/gmm/startpage/e;

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/b/a;->a:Lcom/google/android/apps/gmm/base/g/c;

    iget-wide v2, p1, Lcom/google/android/apps/gmm/startpage/b/a;->e:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/startpage/e;->b(Lcom/google/android/apps/gmm/base/g/c;J)V

    goto :goto_2

    :cond_4
    move v2, v1

    .line 164
    goto :goto_3

    .line 166
    :cond_5
    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/b/a;->d:Lcom/google/android/apps/gmm/startpage/b/b;

    sget-object v3, Lcom/google/android/apps/gmm/startpage/b/b;->b:Lcom/google/android/apps/gmm/startpage/b/b;

    if-ne v2, v3, :cond_6

    :goto_4
    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->c:Lcom/google/android/apps/gmm/startpage/e;

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/b/a;->a:Lcom/google/android/apps/gmm/base/g/c;

    .line 168
    iget-wide v2, p1, Lcom/google/android/apps/gmm/startpage/b/a;->e:J

    .line 167
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/startpage/e;->c(Lcom/google/android/apps/gmm/base/g/c;J)V

    goto :goto_2

    :cond_6
    move v0, v1

    .line 166
    goto :goto_4

    .line 170
    :cond_7
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/b/a;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->c:Lcom/google/android/apps/gmm/startpage/e;

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/b/a;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/b/a;->c:Ljava/lang/Integer;

    iget-wide v4, p1, Lcom/google/android/apps/gmm/startpage/b/a;->e:J

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/apps/gmm/startpage/e;->a(Ljava/lang/String;Ljava/lang/Integer;J)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/ag;->d:Lcom/google/android/apps/gmm/startpage/j;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/startpage/j;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/ag;->g:Lcom/google/android/apps/gmm/startpage/ak;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/startpage/ak;->i:Z

    iget-object v2, v0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/startpage/ak;->a(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 81
    :cond_0
    if-eqz v1, :cond_1

    .line 82
    sget-object v0, Lcom/google/android/apps/gmm/startpage/w;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/ag;->d:Lcom/google/android/apps/gmm/startpage/j;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/startpage/j;->b:Lcom/google/o/h/a/da;

    iget-object v2, v0, Lcom/google/android/apps/gmm/startpage/j;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    iget-object v2, v0, Lcom/google/android/apps/gmm/startpage/j;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/j;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/e;->d()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->c:Lcom/google/android/apps/gmm/startpage/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/e;->c()V

    .line 84
    :cond_1
    return v1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->d:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 99
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/ag;->f:Lcom/google/android/gms/location/reporting/c;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/ag;->f:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/c;->c()V

    :cond_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->d:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 104
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 107
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/startpage/ag;->i:Z

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/startpage/ag;->j:Z

    iget-object v0, v1, Lcom/google/android/apps/gmm/startpage/ag;->g:Lcom/google/android/apps/gmm/startpage/ak;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/startpage/ak;->i:Z

    iget-object v2, v0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/gmm/startpage/ak;->h:J

    iget-object v2, v0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/startpage/ak;->a(Lcom/google/android/apps/gmm/shared/c/f;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    iget-object v2, v1, Lcom/google/android/apps/gmm/startpage/ag;->c:Lcom/google/android/apps/gmm/map/t;

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/startpage/a;->a(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/startpage/a;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/startpage/ag;->h:Lcom/google/android/apps/gmm/startpage/a;

    iget-object v0, v1, Lcom/google/android/apps/gmm/startpage/ag;->g:Lcom/google/android/apps/gmm/startpage/ak;

    iget-object v2, v1, Lcom/google/android/apps/gmm/startpage/ag;->h:Lcom/google/android/apps/gmm/startpage/a;

    iput-object v2, v0, Lcom/google/android/apps/gmm/startpage/ak;->e:Lcom/google/android/apps/gmm/startpage/a;

    iget-object v0, v1, Lcom/google/android/apps/gmm/startpage/ag;->c:Lcom/google/android/apps/gmm/map/t;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/gmm/startpage/ai;

    iget-object v2, v1, Lcom/google/android/apps/gmm/startpage/ag;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/o;->z:Lcom/google/android/apps/gmm/v/cp;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/startpage/ai;-><init>(Lcom/google/android/apps/gmm/startpage/ag;Lcom/google/android/apps/gmm/v/cp;)V

    iput-object v0, v1, Lcom/google/android/apps/gmm/startpage/ag;->l:Lcom/google/android/apps/gmm/startpage/ai;

    iget-object v0, v1, Lcom/google/android/apps/gmm/startpage/ag;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/startpage/ag;->l:Lcom/google/android/apps/gmm/startpage/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    const/4 v4, 0x1

    invoke-direct {v3, v2, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    :cond_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aw:Lcom/google/android/apps/gmm/shared/b/c;

    sget-object v3, Lcom/google/o/h/a/gx;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Lcom/google/n/ax;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gx;

    sget-object v2, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1e

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "loaded latest guideEntryPoint="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v1, Lcom/google/android/apps/gmm/startpage/ag;->h:Lcom/google/android/apps/gmm/startpage/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/ag;->a()Lcom/google/android/apps/gmm/startpage/aj;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/gmm/startpage/ag;->a(Lcom/google/android/apps/gmm/startpage/a;Lcom/google/android/apps/gmm/startpage/aj;Lcom/google/o/h/a/gx;)Lcom/google/android/apps/gmm/startpage/d/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/ag;->a(Lcom/google/android/apps/gmm/startpage/d/a;)V

    .line 108
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/ag;->l:Lcom/google/android/apps/gmm/startpage/ai;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/ag;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/startpage/ag;->l:Lcom/google/android/apps/gmm/startpage/ai;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/startpage/ag;->l:Lcom/google/android/apps/gmm/startpage/ai;

    :cond_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aw:Lcom/google/android/apps/gmm/shared/b/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/ag;->k:Lcom/google/android/apps/gmm/startpage/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/d/a;->c:Lcom/google/o/h/a/gx;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Lcom/google/n/at;)V

    .line 112
    :cond_1
    return-void
.end method
