.class public Lcom/google/android/apps/gmm/startpage/f/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/startpage/e/b;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/o/h/a/hi;

.field c:Lcom/google/o/h/a/hl;

.field final d:Lcom/google/android/apps/gmm/startpage/e/c;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/google/android/libraries/curvular/cg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/startpage/e/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/gmm/startpage/f/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/startpage/f/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/o/h/a/hi;Ljava/lang/String;Lcom/google/android/apps/gmm/startpage/e/c;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/f/c;->b:Lcom/google/o/h/a/hi;

    .line 37
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/f/c;->e:Ljava/lang/String;

    .line 38
    iget-object v1, p1, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    invoke-static {v1}, Lcom/google/android/apps/gmm/startpage/f/c;->a(Ljava/util/List;)Lcom/google/o/h/a/hl;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/c;->c:Lcom/google/o/h/a/hl;

    .line 39
    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/f/c;->d:Lcom/google/android/apps/gmm/startpage/e/c;

    .line 40
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/c;->b:Lcom/google/o/h/a/hi;

    iget-object v1, v1, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/c;->f:Lcom/google/android/libraries/curvular/cg;

    .line 41
    return-void

    .line 40
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/startpage/f/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/startpage/f/d;-><init>(Lcom/google/android/apps/gmm/startpage/f/c;)V

    goto :goto_1
.end method

.method private static a(Ljava/util/List;)Lcom/google/o/h/a/hl;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/hl;",
            ">;)",
            "Lcom/google/o/h/a/hl;"
        }
    .end annotation

    .prologue
    .line 44
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    invoke-static {}, Lcom/google/o/h/a/hl;->g()Lcom/google/o/h/a/hl;

    move-result-object v0

    .line 52
    :goto_0
    return-object v0

    .line 47
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hl;

    .line 48
    iget-boolean v2, v0, Lcom/google/o/h/a/hl;->c:Z

    if-eqz v2, :cond_1

    goto :goto_0

    .line 52
    :cond_2
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hl;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/c;->c:Lcom/google/o/h/a/hl;

    invoke-virtual {v0}, Lcom/google/o/h/a/hl;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/c;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/c;->b:Lcom/google/o/h/a/hi;

    iget-object v1, v1, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 86
    const/4 v0, 0x0

    .line 88
    :goto_1
    return-object v0

    .line 85
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 88
    :cond_1
    sget v0, Lcom/google/android/apps/gmm/f;->ba:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_1
.end method

.method public final c()Lcom/google/android/libraries/curvular/cg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/startpage/e/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/c;->f:Lcom/google/android/libraries/curvular/cg;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 108
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/c;->b:Lcom/google/o/h/a/hi;

    iget v1, v1, Lcom/google/o/h/a/hi;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    if-nez v0, :cond_1

    .line 109
    const/4 v0, 0x0

    .line 114
    :goto_1
    return-object v0

    .line 108
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 111
    :cond_1
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/c;->e:Ljava/lang/String;

    .line 112
    iput-object v0, v2, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/startpage/f/c;->b:Lcom/google/o/h/a/hi;

    .line 113
    iget-object v0, v3, Lcom/google/o/h/a/hi;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    :goto_2
    iput-object v0, v2, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 114
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_1

    .line 113
    :cond_2
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object v1, v3, Lcom/google/o/h/a/hi;->c:Ljava/lang/Object;

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 93
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/c;->b:Lcom/google/o/h/a/hi;

    iget-object v1, v1, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
