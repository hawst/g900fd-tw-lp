.class public Lcom/google/android/apps/gmm/map/u/a/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/bo;


# instance fields
.field public final a:[Lcom/google/android/apps/gmm/map/u/a/e;

.field private final b:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field private final c:I

.field private final d:Lcom/google/android/apps/gmm/map/internal/c/bt;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/e/a/a/a/b;J)V
    .locals 9

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/u/a/c;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 106
    const/4 v0, 0x1

    const/16 v1, 0x15

    invoke-virtual {p2, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/u/a/c;->c:I

    .line 107
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bt;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    const-wide/16 v6, -0x1

    move-object v2, p1

    move-wide v4, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/internal/c/bt;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJ)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/u/a/c;->d:Lcom/google/android/apps/gmm/map/internal/c/bt;

    .line 109
    const/4 v0, 0x2

    iget-object v1, p2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    .line 110
    new-array v0, v0, [Lcom/google/android/apps/gmm/map/u/a/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    .line 116
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/u/a/c;->a(Lcom/google/e/a/a/a/b;)[Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    .line 117
    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/gmm/map/u/a/c;->a(Lcom/google/e/a/a/a/b;[Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 118
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/u/a/c;->b(Lcom/google/e/a/a/a/b;)V

    .line 119
    return-void
.end method

.method public static a([BII)I
    .locals 5

    .prologue
    .line 261
    new-instance v0, Lcom/google/android/apps/gmm/m/a/a;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/gmm/m/a/a;-><init>([BI)V

    .line 262
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/m/a/a;->skipBytes(I)I

    .line 263
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readInt()I

    move-result v1

    .line 264
    const v2, 0x45504752

    if-eq v1, v2, :cond_0

    .line 265
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x29

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "FORMAT_MAGIC expected. Found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 267
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readUnsignedShort()I

    move-result v1

    .line 268
    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 269
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Version mismatch: 1 expected, "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x11

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 272
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readInt()I

    move-result v0

    return v0
.end method

.method private static a([BLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/ab;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 471
    .line 472
    const/4 v0, 0x0

    .line 473
    if-eqz p0, :cond_3

    .line 474
    new-instance v0, Ljava/io/DataInputStream;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 475
    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v1

    .line 477
    :goto_0
    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/ad;

    add-int/lit8 v3, v1, 0x2

    invoke-direct {v5, v3}, Lcom/google/android/apps/gmm/map/b/a/ad;-><init>(I)V

    .line 478
    if-eqz p1, :cond_0

    .line 479
    invoke-virtual {v5, p1}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    :cond_0
    move v3, v2

    move v4, v2

    .line 482
    :goto_1
    if-ge v2, v1, :cond_1

    .line 483
    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/y;->b(Ljava/io/DataInput;)I

    move-result v6

    add-int/2addr v4, v6

    .line 484
    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/y;->b(Ljava/io/DataInput;)I

    move-result v6

    add-int/2addr v3, v6

    .line 485
    invoke-static {v4, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->c(II)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    .line 482
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 487
    :cond_1
    if-eqz p2, :cond_2

    .line 488
    invoke-virtual {v5, p2}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    .line 490
    :cond_2
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/b/a/ad;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v0

    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIIJ)Lcom/google/android/apps/gmm/map/u/a/c;
    .locals 10

    .prologue
    .line 205
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/gmm/map/u/a/c;->a([BII)I

    move-result v0

    .line 206
    add-int/lit8 v2, p2, 0xa

    .line 207
    const/16 v1, 0x20

    new-array v3, v1, [B

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    iget v5, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    invoke-static {v1, v4, v5, v0, v3}, Lcom/google/android/apps/gmm/map/internal/c/br;->a(IIII[B)V

    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/br;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/internal/c/br;-><init>()V

    const/16 v5, 0x100

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/br;->a:[B

    const/4 v1, 0x0

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/c/br;->b:[B

    const/4 v7, 0x0

    const/16 v8, 0x100

    invoke-static {v0, v1, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    const/16 v6, 0x100

    if-ge v1, v6, :cond_0

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/c/br;->b:[B

    aget-byte v6, v6, v1

    add-int/2addr v0, v6

    and-int/lit8 v6, v1, 0x1f

    aget-byte v6, v3, v6

    add-int/2addr v0, v6

    and-int/lit16 v0, v0, 0xff

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/c/br;->b:[B

    aget-byte v6, v6, v1

    iget-object v7, v4, Lcom/google/android/apps/gmm/map/internal/c/br;->b:[B

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/internal/c/br;->b:[B

    aget-byte v8, v8, v0

    aput-byte v8, v7, v1

    iget-object v7, v4, Lcom/google/android/apps/gmm/map/internal/c/br;->b:[B

    aput-byte v6, v7, v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput v0, v4, Lcom/google/android/apps/gmm/map/internal/c/br;->c:I

    const/4 v0, 0x0

    iput v0, v4, Lcom/google/android/apps/gmm/map/internal/c/br;->d:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/internal/c/br;->a(I)V

    sub-int v0, p3, v2

    invoke-virtual {v4, p1, v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/br;->a([BII)V

    .line 208
    sub-int v3, p3, v2

    .line 210
    const/4 v1, 0x0

    .line 212
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/h;->a(I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v1

    .line 213
    if-nez v1, :cond_1

    .line 214
    new-instance v0, Lcom/google/android/apps/gmm/map/util/f;

    const/4 v4, 0x0

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/map/util/f;-><init>(I)V

    move-object v1, v0

    .line 216
    :cond_1
    invoke-static {p1, v2, v3, v1}, Lcom/google/android/apps/gmm/map/util/g;->a([BIILcom/google/android/apps/gmm/map/util/f;)V

    .line 217
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v2

    invoke-static {p0, v0, v2, p4, p5}, Lcom/google/android/apps/gmm/map/u/a/c;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIJ)Lcom/google/android/apps/gmm/map/u/a/c;
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 224
    if-eqz v1, :cond_2

    .line 225
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    :cond_2
    :goto_1
    return-object v0

    .line 220
    :catch_0
    move-exception v0

    .line 221
    :try_start_1
    const-string v2, "RoadGraphPiece"

    invoke-virtual {v0}, Ljava/util/zip/DataFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 222
    if-eqz v1, :cond_3

    .line 225
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 224
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 225
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    :cond_4
    throw v0
.end method

.method private static a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIJ)Lcom/google/android/apps/gmm/map/u/a/c;
    .locals 5

    .prologue
    .line 179
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/maps/c/a/a/b;->e:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 180
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const/4 v2, 0x1

    new-instance v3, Lcom/google/e/a/a/a/c;

    invoke-direct {v3}, Lcom/google/e/a/a/a/c;-><init>()V

    invoke-virtual {v1, v0, p2, v2, v3}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;IZLcom/google/e/a/a/a/c;)I

    .line 182
    :try_start_0
    new-instance v0, Lcom/google/android/apps/gmm/map/u/a/c;

    invoke-direct {v0, p0, v1, p3, p4}, Lcom/google/android/apps/gmm/map/u/a/c;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/e/a/a/a/b;J)V
    :try_end_0
    .catch Lcom/google/android/apps/gmm/map/u/a/d; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :goto_0
    return-object v0

    .line 183
    :catch_0
    move-exception v0

    .line 184
    const-string v1, "RoadGraphPiece"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 185
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/e/a/a/a/b;[Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 21

    .prologue
    .line 313
    const/4 v2, 0x0

    move v12, v2

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    array-length v2, v2

    div-int/lit8 v2, v2, 0x2

    if-ge v12, v2, :cond_e

    .line 314
    shl-int/lit8 v15, v12, 0x1

    .line 315
    shl-int/lit8 v2, v12, 0x1

    add-int/lit8 v16, v2, 0x1

    .line 316
    const/4 v2, 0x2

    const/16 v3, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v3}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/e/a/a/a/b;

    .line 317
    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v9

    .line 319
    const/4 v3, 0x2

    const/16 v4, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1, v4}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/e/a/a/a/b;

    .line 320
    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v17

    .line 322
    const/4 v5, 0x0

    .line 323
    const/4 v4, 0x3

    const/16 v6, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12, v6}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/e/a/a/a/b;

    .line 325
    const/4 v7, 0x1

    iget-object v6, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v6, v7}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v6

    if-lez v6, :cond_3

    const/4 v6, 0x1

    :goto_1
    if-nez v6, :cond_0

    invoke-virtual {v4, v7}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_4

    :cond_0
    const/4 v6, 0x1

    :goto_2
    if-eqz v6, :cond_11

    .line 326
    const/4 v5, 0x1

    const/16 v6, 0x19

    invoke-virtual {v4, v5, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    move-object v7, v5

    .line 328
    :goto_3
    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v11

    .line 330
    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v10

    .line 333
    const/4 v4, 0x3

    iget-object v5, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v18

    const/4 v4, 0x1

    if-ne v10, v4, :cond_5

    const/4 v4, 0x1

    move v14, v4

    :goto_4
    if-lez v18, :cond_8

    move/from16 v0, v18

    new-array v6, v0, [Lcom/google/android/apps/gmm/map/u/a/f;

    const/4 v4, 0x0

    move v13, v4

    :goto_5
    move/from16 v0, v18

    if-ge v13, v0, :cond_9

    const/4 v4, 0x3

    const/16 v5, 0x15

    invoke-virtual {v2, v4, v13, v5}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v4, v4

    const/4 v5, 0x5

    const/16 v8, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4, v8}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/e/a/a/a/b;

    const/4 v5, 0x0

    const/16 v19, 0x2

    iget-object v8, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v8

    invoke-static {v8}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v8

    if-lez v8, :cond_6

    const/4 v8, 0x1

    :goto_6
    if-nez v8, :cond_1

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_7

    :cond_1
    const/4 v8, 0x1

    :goto_7
    if-eqz v8, :cond_2

    const/4 v5, 0x2

    const/16 v8, 0x1c

    invoke-virtual {v4, v5, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    :cond_2
    new-instance v8, Lcom/google/android/apps/gmm/map/u/a/f;

    const/16 v19, 0x1

    const/16 v20, 0x1c

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v8, v4, v5, v14}, Lcom/google/android/apps/gmm/map/u/a/f;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v8, v6, v13

    add-int/lit8 v4, v13, 0x1

    move v13, v4

    goto :goto_5

    .line 325
    :cond_3
    const/4 v6, 0x0

    goto/16 :goto_1

    :cond_4
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 333
    :cond_5
    const/4 v4, 0x0

    move v14, v4

    goto :goto_4

    :cond_6
    const/4 v8, 0x0

    goto :goto_6

    :cond_7
    const/4 v8, 0x0

    goto :goto_7

    :cond_8
    const/4 v4, 0x1

    new-array v6, v4, [Lcom/google/android/apps/gmm/map/u/a/f;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/gmm/map/u/a/e;->a:Lcom/google/android/apps/gmm/map/u/a/f;

    aput-object v5, v6, v4

    .line 335
    :cond_9
    const/4 v5, 0x0

    .line 336
    const/4 v4, 0x4

    .line 338
    aget-object v8, p2, v16

    .line 339
    aget-object v13, p2, v15

    .line 340
    if-nez v8, :cond_a

    if-nez v13, :cond_a

    .line 341
    new-instance v2, Lcom/google/android/apps/gmm/map/u/a/d;

    const-string v3, "Both polyline endpoints are missing for segment: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    aget-object v4, v4, v15

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/u/a/c;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0xa

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " in tile: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/u/a/d;-><init>(Ljava/lang/String;)V

    throw v2

    .line 345
    :cond_a
    invoke-static {v7, v8, v13}, Lcom/google/android/apps/gmm/map/u/a/c;->a([BLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v7

    .line 346
    iget-object v14, v7, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v14, v14

    div-int/lit8 v14, v14, 0x3

    const/16 v18, 0x2

    move/from16 v0, v18

    if-ge v14, v0, :cond_b

    .line 347
    new-instance v2, Lcom/google/android/apps/gmm/map/u/a/d;

    const-string v3, "Segment polyline had fewer than two points for segment: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    aget-object v4, v4, v15

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/u/a/c;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0xa

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " in tile: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/u/a/d;-><init>(Ljava/lang/String;)V

    throw v2

    .line 351
    :cond_b
    if-nez v8, :cond_d

    .line 352
    const/4 v5, 0x2

    .line 353
    const/4 v4, 0x5

    .line 359
    :cond_c
    :goto_8
    const/4 v8, 0x4

    const/4 v13, 0x0

    invoke-static {v2, v8, v13}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_10

    .line 361
    or-int/lit8 v8, v5, 0x8

    .line 363
    :goto_9
    const/4 v2, 0x4

    const/4 v5, 0x0

    invoke-static {v3, v2, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_f

    .line 365
    or-int/lit8 v4, v4, 0x8

    move v2, v4

    .line 368
    :goto_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    new-instance v3, Lcom/google/android/apps/gmm/map/u/a/e;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/u/a/c;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v4, v15}, Lcom/google/android/apps/gmm/map/u/a/e;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;I)J

    move-result-wide v4

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/gmm/map/u/a/e;-><init>(J[Lcom/google/android/apps/gmm/map/u/a/f;Lcom/google/android/apps/gmm/map/b/a/ab;IIII)V

    aput-object v3, v13, v15

    .line 370
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    new-instance v3, Lcom/google/android/apps/gmm/map/u/a/e;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/u/a/c;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move/from16 v0, v16

    invoke-static {v4, v0}, Lcom/google/android/apps/gmm/map/u/a/e;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;I)J

    move-result-wide v4

    move v8, v2

    move/from16 v9, v17

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/gmm/map/u/a/e;-><init>(J[Lcom/google/android/apps/gmm/map/u/a/f;Lcom/google/android/apps/gmm/map/b/a/ab;IIII)V

    aput-object v3, v13, v16

    .line 313
    add-int/lit8 v2, v12, 0x1

    move v12, v2

    goto/16 :goto_0

    .line 354
    :cond_d
    if-nez v13, :cond_c

    .line 355
    const/4 v5, 0x1

    .line 356
    const/4 v4, 0x6

    goto :goto_8

    .line 374
    :cond_e
    return-void

    :cond_f
    move v2, v4

    goto :goto_a

    :cond_10
    move v8, v5

    goto :goto_9

    :cond_11
    move-object v7, v5

    goto/16 :goto_3
.end method

.method private static a([B)[I
    .locals 5

    .prologue
    .line 446
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 447
    invoke-static {v1}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v2

    .line 448
    new-array v3, v2, [I

    .line 449
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 450
    invoke-static {v1}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    aput v4, v3, v0

    .line 449
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 452
    :cond_0
    return-object v3
.end method

.method private a(Lcom/google/e/a/a/a/b;)[Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 12

    .prologue
    const/16 v11, 0x15

    const/4 v10, 0x4

    const/4 v2, 0x0

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    array-length v0, v0

    new-array v4, v0, [Lcom/google/android/apps/gmm/map/b/a/y;

    .line 285
    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v10}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v5

    move v3, v2

    .line 286
    :goto_0
    if-ge v3, v5, :cond_1

    .line 287
    const/16 v0, 0x1a

    invoke-virtual {p1, v10, v3, v0}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 289
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v11}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v6, v6

    .line 290
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v11}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    long-to-int v1, v8

    .line 291
    invoke-static {v6, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(II)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    .line 292
    const/4 v6, 0x3

    const/16 v7, 0x19

    invoke-virtual {v0, v6, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/u/a/c;->a([B)[I

    move-result-object v6

    move v0, v2

    .line 294
    :goto_1
    array-length v7, v6

    if-ge v0, v7, :cond_0

    .line 295
    aget v7, v6, v0

    aput-object v1, v4, v7

    .line 294
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 286
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 298
    :cond_1
    return-object v4
.end method

.method private b(Lcom/google/e/a/a/a/b;)V
    .locals 16

    .prologue
    .line 384
    const/4 v1, 0x4

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v7

    .line 385
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 386
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 387
    const/4 v1, 0x0

    move v6, v1

    :goto_0
    if-ge v6, v7, :cond_5

    .line 388
    const/4 v1, 0x4

    const/16 v2, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6, v2}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/e/a/a/a/b;

    .line 389
    const/4 v2, 0x3

    const/16 v3, 0x19

    invoke-virtual {v1, v2, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/u/a/c;->a([B)[I

    move-result-object v8

    .line 391
    const/4 v2, 0x4

    const/16 v3, 0x19

    invoke-virtual {v1, v2, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/u/a/c;->a([B)[I

    move-result-object v9

    .line 392
    const/4 v2, 0x0

    .line 393
    array-length v1, v8

    new-array v10, v1, [Lcom/google/android/apps/gmm/map/u/a/a;

    .line 394
    const/4 v1, 0x0

    :goto_1
    array-length v3, v8

    if-ge v1, v3, :cond_4

    .line 395
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    aget v4, v8, v1

    aget-object v11, v3, v4

    .line 396
    const/4 v4, 0x0

    .line 397
    const/4 v3, 0x0

    move v15, v3

    move v3, v4

    move v4, v1

    move v1, v15

    :goto_2
    array-length v5, v8

    if-ge v1, v5, :cond_2

    .line 398
    array-length v5, v9

    if-lt v2, v5, :cond_1

    .line 401
    array-length v1, v8

    move v4, v1

    .line 417
    :cond_0
    :goto_3
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 404
    :cond_1
    aget v12, v9, v2

    .line 405
    if-eqz v12, :cond_0

    .line 407
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    aget v13, v8, v1

    xor-int/lit8 v13, v13, 0x1

    aget-object v13, v5, v13

    .line 416
    add-int/lit8 v5, v3, 0x1

    new-instance v14, Lcom/google/android/apps/gmm/map/u/a/a;

    invoke-direct {v14, v13, v12}, Lcom/google/android/apps/gmm/map/u/a/a;-><init>(Lcom/google/android/apps/gmm/map/u/a/e;I)V

    aput-object v14, v10, v3

    move v3, v5

    goto :goto_3

    .line 431
    :cond_2
    new-array v1, v3, [Lcom/google/android/apps/gmm/map/u/a/a;

    .line 432
    const/4 v3, 0x0

    const/4 v5, 0x0

    array-length v12, v1

    invoke-static {v10, v3, v1, v5, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 433
    array-length v3, v1

    if-nez v3, :cond_3

    sget-object v1, Lcom/google/android/apps/gmm/map/u/a/e;->b:[Lcom/google/android/apps/gmm/map/u/a/a;

    iput-object v1, v11, Lcom/google/android/apps/gmm/map/u/a/e;->f:[Lcom/google/android/apps/gmm/map/u/a/a;

    .line 394
    :goto_4
    add-int/lit8 v1, v4, 0x1

    goto :goto_1

    .line 433
    :cond_3
    iput-object v1, v11, Lcom/google/android/apps/gmm/map/u/a/e;->f:[Lcom/google/android/apps/gmm/map/u/a/a;

    goto :goto_4

    .line 387
    :cond_4
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_0

    .line 436
    :cond_5
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/c;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/u/a/e;)Lcom/google/android/apps/gmm/map/u/a/e;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 140
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 141
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    aget-object v2, v2, v0

    .line 142
    iget v3, v2, Lcom/google/android/apps/gmm/map/u/a/e;->c:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_0

    move v3, v4

    :goto_1
    if-eqz v3, :cond_2

    iget v3, v2, Lcom/google/android/apps/gmm/map/u/a/e;->c:I

    and-int/lit8 v3, v3, 0x4

    iget v5, p1, Lcom/google/android/apps/gmm/map/u/a/e;->c:I

    and-int/lit8 v5, v5, 0x4

    if-ne v3, v5, :cond_1

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/u/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v5, p1, Lcom/google/android/apps/gmm/map/u/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v4

    :goto_2
    if-eqz v3, :cond_2

    move-object v0, v2

    .line 146
    :goto_3
    return-object v0

    :cond_0
    move v3, v1

    .line 142
    goto :goto_1

    :cond_1
    move v3, v1

    goto :goto_2

    .line 140
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final b()Lcom/google/android/apps/gmm/map/b/a/ai;
    .locals 1

    .prologue
    .line 570
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 538
    iget v0, p0, Lcom/google/android/apps/gmm/map/u/a/c;->c:I

    return v0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/internal/c/bt;
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/c;->d:Lcom/google/android/apps/gmm/map/internal/c/bt;

    return-object v0
.end method
