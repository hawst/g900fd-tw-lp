.class public Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/a/a;
.implements Lcom/google/android/apps/gmm/z/b/o;


# instance fields
.field a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

.field private b:Ljava/lang/Integer;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method static a(Landroid/app/Activity;Landroid/app/Fragment;)V
    .locals 1

    .prologue
    .line 128
    check-cast p0, Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 129
    return-void
.end method

.method protected static a(Landroid/preference/PreferenceScreen;)V
    .locals 3

    .prologue
    .line 189
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 190
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/h;->ak:I

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 189
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 192
    :cond_0
    return-void
.end method


# virtual methods
.method public final K_()V
    .locals 1

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 150
    if-nez v0, :cond_0

    .line 154
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/a/a/b;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected a()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 165
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a_(I)V
    .locals 1

    .prologue
    .line 180
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->b:Ljava/lang/Integer;

    .line 181
    return-void
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lcom/google/b/f/t;->eB:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final g()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 59
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 61
    const/4 v1, 0x0

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->a()Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v2

    .line 63
    if-eqz v2, :cond_0

    .line 64
    new-instance v1, Lcom/google/android/apps/gmm/base/l/ao;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/base/l/ao;-><init>(Lcom/google/android/apps/gmm/base/views/c/g;)V

    .line 68
    :cond_0
    if-eqz v1, :cond_3

    .line 69
    new-instance v2, Lcom/google/android/apps/gmm/base/views/QuHeaderView;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/google/android/apps/gmm/base/views/QuHeaderView;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/l/a/ab;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    .line 75
    :goto_0
    if-eqz p1, :cond_1

    const-string v1, "ue3ActivationId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    const-string v1, "ue3ActivationId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->b:Ljava/lang/Integer;

    .line 79
    :cond_1
    if-eqz p1, :cond_4

    .line 80
    :goto_1
    if-eqz p1, :cond_2

    const-string v1, "allowSideInfoSheet"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_2
    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->c:Z

    .line 81
    return-void

    .line 71
    :cond_3
    new-instance v1, Lcom/google/android/apps/gmm/base/views/HeaderView;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/base/views/HeaderView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    .line 72
    iget-object v1, p0, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->setFragment(Landroid/app/Fragment;)V

    goto :goto_0

    .line 79
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_1

    .line 80
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 88
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 89
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    sget v0, Lcom/google/android/apps/gmm/d;->aR:I

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 92
    invoke-super {p0, p1, v1, p3}, Landroid/preference/PreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 93
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 97
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 99
    return-object v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->isResumed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 143
    :cond_0
    :goto_0
    return v0

    .line 137
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->g:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 141
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 104
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 107
    new-instance v1, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 108
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v4, v2, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v2, 0x0

    .line 109
    iget-object v3, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v2, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 110
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 111
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 112
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->O:Lcom/google/android/apps/gmm/base/a/a;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->c:Z

    .line 113
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    sget-object v0, Lcom/google/android/apps/gmm/base/activities/a/a;->c:Lcom/google/android/apps/gmm/base/activities/a/a;

    .line 114
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->t:Lcom/google/android/apps/gmm/base/activities/a/a;

    .line 115
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 116
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 120
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 122
    const-string v0, "ue3ActivationId"

    iget-object v1, p0, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 124
    :cond_0
    const-string v0, "allowSideInfoSheet"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 125
    return-void
.end method
