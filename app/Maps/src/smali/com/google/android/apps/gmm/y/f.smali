.class public Lcom/google/android/apps/gmm/y/f;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static c:Ljava/util/Calendar;

.field private static final d:Ljava/lang/String;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/y/g;",
            "Lcom/google/android/apps/gmm/y/c;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/google/maps/g/jl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/y/f;->c:Ljava/util/Calendar;

    .line 91
    const-class v0, Lcom/google/android/apps/gmm/y/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/y/f;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/r/b/a/acq;)V
    .locals 8
    .param p1    # Lcom/google/r/b/a/acq;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object v2, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    .line 96
    if-nez p1, :cond_1

    .line 97
    iput-object v2, p0, Lcom/google/android/apps/gmm/y/f;->a:Ljava/util/Map;

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/acq;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/pr;->d()Lcom/google/d/a/a/pr;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pr;

    .line 104
    iget v1, p1, Lcom/google/r/b/a/acq;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v5, 0x40

    if-ne v1, v5, :cond_2

    move v1, v4

    :goto_1
    if-nez v1, :cond_3

    .line 105
    iput-object v2, p0, Lcom/google/android/apps/gmm/y/f;->a:Ljava/util/Map;

    goto :goto_0

    :cond_2
    move v1, v3

    .line 104
    goto :goto_1

    .line 110
    :cond_3
    iget v1, p1, Lcom/google/r/b/a/acq;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v5, 0x80

    if-ne v1, v5, :cond_4

    move v1, v4

    :goto_2
    if-eqz v1, :cond_6

    .line 111
    iget-object v1, p1, Lcom/google/r/b/a/acq;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/je;->d()Lcom/google/maps/g/je;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/je;

    iget v1, v1, Lcom/google/maps/g/je;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_5

    move v1, v4

    :goto_3
    if-eqz v1, :cond_6

    .line 112
    iget-object v1, p1, Lcom/google/r/b/a/acq;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/je;->d()Lcom/google/maps/g/je;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/je;

    iget-object v1, v1, Lcom/google/maps/g/je;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jl;->d()Lcom/google/maps/g/jl;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/jl;

    :goto_4
    iput-object v1, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    .line 115
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/y/f;->a:Ljava/util/Map;

    .line 116
    invoke-static {}, Lcom/google/android/apps/gmm/y/g;->values()[Lcom/google/android/apps/gmm/y/g;

    move-result-object v4

    array-length v5, v4

    move v1, v3

    :goto_5
    if-ge v1, v5, :cond_7

    aget-object v3, v4, v1

    .line 117
    iget-object v6, p0, Lcom/google/android/apps/gmm/y/f;->a:Ljava/util/Map;

    new-instance v7, Lcom/google/android/apps/gmm/y/c;

    invoke-direct {v7, v3}, Lcom/google/android/apps/gmm/y/c;-><init>(Lcom/google/android/apps/gmm/y/g;)V

    invoke-interface {v6, v3, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_4
    move v1, v3

    .line 110
    goto :goto_2

    :cond_5
    move v1, v3

    .line 111
    goto :goto_3

    :cond_6
    move-object v1, v2

    .line 112
    goto :goto_4

    .line 121
    :cond_7
    invoke-static {v0}, Lcom/google/android/apps/gmm/y/f;->a(Lcom/google/d/a/a/pr;)Ljava/util/List;

    move-result-object v0

    .line 122
    invoke-static {v0}, Lcom/google/android/apps/gmm/y/f;->a(Ljava/util/List;)V

    .line 123
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/y/h;

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->a:Ljava/util/Map;

    iget-object v4, v1, Lcom/google/android/apps/gmm/y/h;->b:Lcom/google/android/apps/gmm/y/g;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/y/c;

    iget-object v4, v0, Lcom/google/android/apps/gmm/y/c;->b:Lcom/google/android/apps/gmm/y/g;

    iget-object v5, v1, Lcom/google/android/apps/gmm/y/h;->b:Lcom/google/android/apps/gmm/y/g;

    if-ne v4, v5, :cond_8

    iget-object v4, v0, Lcom/google/android/apps/gmm/y/c;->c:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object v2, v0, Lcom/google/android/apps/gmm/y/c;->e:Landroid/content/res/Configuration;

    iput-object v2, v0, Lcom/google/android/apps/gmm/y/c;->d:Ljava/util/List;

    goto :goto_6

    :cond_8
    sget-object v4, Lcom/google/android/apps/gmm/y/c;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/gmm/y/c;->b:Lcom/google/android/apps/gmm/y/g;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/y/h;->b:Lcom/google/android/apps/gmm/y/g;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x4a

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Tried to create a time interval with a mismatched day.  Expected "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " but got "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6
.end method

.method private static a(Lcom/google/d/a/a/pr;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/d/a/a/pr;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/y/h;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 270
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 271
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/d/a/a/pr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lcom/google/d/a/a/pr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/or;->g()Lcom/google/d/a/a/or;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/or;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 272
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/or;

    .line 273
    iget v1, v0, Lcom/google/d/a/a/or;->c:I

    invoke-static {v1}, Lcom/google/d/a/a/ou;->a(I)Lcom/google/d/a/a/ou;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/d/a/a/ou;->a:Lcom/google/d/a/a/ou;

    .line 274
    :cond_1
    sget-object v4, Lcom/google/d/a/a/ou;->b:Lcom/google/d/a/a/ou;

    if-ne v1, v4, :cond_3

    .line 276
    sget-object v1, Lcom/google/android/apps/gmm/y/f;->d:Ljava/lang/String;

    const-string v1, "Missing data for time component: "

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 280
    :cond_3
    invoke-virtual {v0}, Lcom/google/d/a/a/or;->d()Ljava/util/List;

    move-result-object v1

    .line 283
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v6, :cond_6

    .line 284
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pj;

    invoke-static {v0}, Lcom/google/android/apps/gmm/y/i;->b(Lcom/google/d/a/a/pj;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 286
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pj;

    invoke-static {v0}, Lcom/google/android/apps/gmm/y/h;->b(Lcom/google/d/a/a/pj;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 287
    :cond_4
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pj;

    invoke-static {v0}, Lcom/google/android/apps/gmm/y/i;->a(Lcom/google/d/a/a/pj;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 289
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pj;

    invoke-static {v0}, Lcom/google/android/apps/gmm/y/h;->a(Lcom/google/d/a/a/pj;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 291
    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/y/f;->d:Ljava/lang/String;

    goto :goto_1

    .line 294
    :cond_6
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_9

    .line 297
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pj;

    invoke-static {v0}, Lcom/google/android/apps/gmm/y/i;->b(Lcom/google/d/a/a/pj;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 298
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pj;

    invoke-static {v0}, Lcom/google/android/apps/gmm/y/i;->a(Lcom/google/d/a/a/pj;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 299
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pj;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/d/a/a/pj;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/y/h;->a(Lcom/google/d/a/a/pj;Lcom/google/d/a/a/pj;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 300
    :cond_7
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pj;

    invoke-static {v0}, Lcom/google/android/apps/gmm/y/i;->a(Lcom/google/d/a/a/pj;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 301
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pj;

    invoke-static {v0}, Lcom/google/android/apps/gmm/y/i;->b(Lcom/google/d/a/a/pj;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 302
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pj;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/d/a/a/pj;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/y/h;->a(Lcom/google/d/a/a/pj;Lcom/google/d/a/a/pj;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 304
    :cond_8
    sget-object v0, Lcom/google/android/apps/gmm/y/f;->d:Ljava/lang/String;

    goto/16 :goto_1

    .line 308
    :cond_9
    sget-object v0, Lcom/google/android/apps/gmm/y/f;->d:Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x33

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Expected 1 or 2 time intervals, but got "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 311
    :cond_a
    return-object v2
.end method

.method private static a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/y/h;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 321
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 322
    invoke-static {p0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 323
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 324
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/y/h;

    move-object v1, v0

    .line 325
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 326
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/y/h;

    .line 327
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/y/h;->a(Lcom/google/android/apps/gmm/y/h;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 328
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    :cond_0
    move-object v1, v0

    .line 331
    goto :goto_0

    .line 333
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/y/h;

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/y/h;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/y/h;->a(Lcom/google/android/apps/gmm/y/h;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 334
    invoke-interface {p0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 337
    :cond_2
    return-void
.end method

.method private static declared-synchronized d()I
    .locals 4

    .prologue
    .line 200
    const-class v1, Lcom/google/android/apps/gmm/y/f;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/y/f;->c:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 201
    sget-object v0, Lcom/google/android/apps/gmm/y/f;->c:Ljava/util/Calendar;

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private e()Lcom/google/android/apps/gmm/y/c;
    .locals 2

    .prologue
    .line 210
    invoke-static {}, Lcom/google/android/apps/gmm/y/f;->d()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/y/f;->a:Ljava/util/Map;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/y/f;->a:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/android/apps/gmm/y/g;->b(I)Lcom/google/android/apps/gmm/y/g;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/y/c;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v0, v0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    .line 156
    iget-object v2, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget-object v0, v2, Lcom/google/maps/g/jl;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    .line 158
    :goto_1
    return-object v0

    .line 155
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 156
    :cond_1
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v1, v2, Lcom/google/maps/g/jl;->f:Ljava/lang/Object;

    :cond_2
    move-object v0, v1

    goto :goto_1

    .line 158
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->a:Ljava/util/Map;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 235
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    .line 237
    :goto_1
    return-object v0

    .line 234
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 237
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/y/f;->e()Lcom/google/android/apps/gmm/y/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/y/c;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->a:Ljava/util/Map;

    if-eqz v0, :cond_1

    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/gmm/y/f;->e()Lcom/google/android/apps/gmm/y/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/y/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/y/h;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/y/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final c()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 358
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    .line 359
    iget v0, v0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_0
    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    .line 360
    iget v0, v0, Lcom/google/maps/g/jl;->d:I

    invoke-static {v0}, Lcom/google/maps/g/jq;->a(I)Lcom/google/maps/g/jq;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v3, v3, Lcom/google/maps/g/jl;->d:I

    invoke-static {v3}, Lcom/google/maps/g/jq;->a(I)Lcom/google/maps/g/jq;

    move-result-object v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    :cond_1
    sget-object v3, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    if-eq v0, v3, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    .line 361
    iget v0, v0, Lcom/google/maps/g/jl;->d:I

    invoke-static {v0}, Lcom/google/maps/g/jq;->a(I)Lcom/google/maps/g/jq;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v3, v3, Lcom/google/maps/g/jl;->d:I

    invoke-static {v3}, Lcom/google/maps/g/jq;->a(I)Lcom/google/maps/g/jq;

    move-result-object v3

    if-nez v3, :cond_3

    sget-object v3, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    :cond_3
    sget-object v3, Lcom/google/maps/g/jq;->b:Lcom/google/maps/g/jq;

    if-ne v0, v3, :cond_a

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    .line 362
    iget v0, v0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_1
    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    .line 363
    iget v0, v0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_2
    if-eqz v0, :cond_a

    .line 364
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v3, v0, Lcom/google/maps/g/jl;->e:I

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget-object v4, v0, Lcom/google/maps/g/jl;->b:Lcom/google/maps/g/jh;

    if-nez v4, :cond_8

    invoke-static {}, Lcom/google/maps/g/jh;->d()Lcom/google/maps/g/jh;

    move-result-object v0

    :goto_3
    iget-object v0, v0, Lcom/google/maps/g/jh;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    .line 366
    add-int/lit8 v0, v0, -0x1

    if-ne v3, v0, :cond_9

    move v0, v1

    .line 368
    :goto_4
    return v0

    :cond_5
    move v0, v2

    .line 359
    goto :goto_0

    :cond_6
    move v0, v2

    .line 362
    goto :goto_1

    :cond_7
    move v0, v2

    .line 363
    goto :goto_2

    .line 365
    :cond_8
    iget-object v0, v0, Lcom/google/maps/g/jl;->b:Lcom/google/maps/g/jh;

    goto :goto_3

    :cond_9
    move v0, v2

    .line 366
    goto :goto_4

    :cond_a
    move v0, v2

    .line 368
    goto :goto_4
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->a:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/y/f;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "(No open hours data)"

    goto :goto_0
.end method
