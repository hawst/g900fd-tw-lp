.class Lcom/google/android/apps/gmm/car/drawer/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/drawer/s;


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:I

.field final synthetic d:I

.field final synthetic e:Lcom/google/android/apps/gmm/car/drawer/g;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/drawer/g;IIII)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/drawer/h;->e:Lcom/google/android/apps/gmm/car/drawer/g;

    iput p2, p0, Lcom/google/android/apps/gmm/car/drawer/h;->a:I

    iput p3, p0, Lcom/google/android/apps/gmm/car/drawer/h;->b:I

    iput p4, p0, Lcom/google/android/apps/gmm/car/drawer/h;->c:I

    iput p5, p0, Lcom/google/android/apps/gmm/car/drawer/h;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/google/android/apps/gmm/car/drawer/h;->a:I

    invoke-static {v0}, Lcom/google/android/apps/gmm/car/n/c;->a(I)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/h;->e:Lcom/google/android/apps/gmm/car/drawer/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/car/drawer/h;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/h;->e:Lcom/google/android/apps/gmm/car/drawer/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/ad;->o:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/google/android/apps/gmm/car/drawer/h;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/h;->e:Lcom/google/android/apps/gmm/car/drawer/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    iget v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/h;->e:Lcom/google/android/apps/gmm/car/drawer/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/h;->e:Lcom/google/android/apps/gmm/car/drawer/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    new-instance v2, Lcom/google/android/apps/gmm/car/i/o;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/h;->e:Lcom/google/android/apps/gmm/car/drawer/g;

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/h;->e:Lcom/google/android/apps/gmm/car/drawer/g;

    .line 149
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v4, p0, Lcom/google/android/apps/gmm/car/drawer/h;->b:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/h;->e:Lcom/google/android/apps/gmm/car/drawer/g;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/drawer/g;->j:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/car/drawer/h;->d:I

    :goto_0
    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/apps/gmm/car/i/o;-><init>(Lcom/google/android/apps/gmm/car/ad;Ljava/lang/String;I)V

    .line 148
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 149
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 148
    :cond_1
    iget-object v0, v1, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/car/m/l;->a(Lcom/google/android/apps/gmm/car/m/h;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/h;->e:Lcom/google/android/apps/gmm/car/drawer/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/f;->a()V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/h;->e:Lcom/google/android/apps/gmm/car/drawer/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/g;->i:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/j;->a()V

    .line 152
    const/4 v0, 0x0

    return-object v0
.end method
