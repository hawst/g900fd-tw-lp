.class public Lcom/google/android/apps/gmm/invocation/b/s;
.super Lcom/google/android/apps/gmm/invocation/b/e;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/invocation/b/e;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 26
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/r/b/a/gb;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/r/b/a/gb;->c:Lcom/google/r/b/a/gb;

    return-object v0
.end method

.method protected final b(Lcom/google/e/a/a/a/b;)Ljava/lang/Runnable;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x0

    const/16 v7, 0x1a

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 31
    const/16 v2, 0xa

    .line 32
    if-eqz p1, :cond_6

    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v2}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_4

    move v0, v4

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p1, v2}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    :cond_0
    move v0, v4

    :goto_1
    if-eqz v0, :cond_6

    invoke-virtual {p1, v2, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 34
    :goto_2
    invoke-static {v0, v8}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v5

    .line 36
    if-eqz v0, :cond_9

    iget-object v2, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_7

    move v2, v4

    :goto_3
    if-nez v2, :cond_1

    invoke-virtual {v0, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_8

    :cond_1
    move v2, v4

    :goto_4
    if-eqz v2, :cond_9

    invoke-virtual {v0, v4, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    move-object v2, v0

    .line 38
    :goto_5
    if-eqz v2, :cond_c

    iget-object v0, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_a

    move v0, v4

    :goto_6
    if-nez v0, :cond_2

    invoke-virtual {v2, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_b

    :cond_2
    move v0, v4

    :goto_7
    if-eqz v0, :cond_c

    invoke-virtual {v2, v4, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    move-object v2, v0

    .line 40
    :goto_8
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/q;->b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v6

    .line 41
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_d

    :cond_3
    move v0, v4

    :goto_9
    if-eqz v0, :cond_e

    if-nez v6, :cond_e

    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/g;

    const-string v1, "No panoid or LatLng in response."

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/invocation/b/g;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v3

    .line 32
    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_2

    :cond_7
    move v2, v3

    .line 36
    goto :goto_3

    :cond_8
    move v2, v3

    goto :goto_4

    :cond_9
    move-object v2, v1

    goto :goto_5

    :cond_a
    move v0, v3

    .line 38
    goto :goto_6

    :cond_b
    move v0, v3

    goto :goto_7

    :cond_c
    move-object v2, v1

    goto :goto_8

    :cond_d
    move v0, v3

    .line 41
    goto :goto_9

    .line 44
    :cond_e
    if-eqz v2, :cond_12

    iget-object v0, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v8}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_10

    move v0, v4

    :goto_a
    if-nez v0, :cond_f

    invoke-virtual {v2, v8}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_11

    :cond_f
    move v0, v4

    :goto_b
    if-eqz v0, :cond_12

    invoke-virtual {v2, v8, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    :goto_c
    const/4 v1, 0x0

    invoke-static {v0, v4, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IF)F

    move-result v1

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-static {v0, v8, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IF)F

    move-result v0

    const/high16 v3, 0x42b40000    # 90.0f

    sub-float/2addr v0, v3

    const/4 v3, 0x4

    const/high16 v4, 0x42700000    # 60.0f

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IF)F

    move-result v2

    new-instance v3, Lcom/google/android/apps/gmm/streetview/b/a;

    invoke-direct {v3, v1, v0, v2}, Lcom/google/android/apps/gmm/streetview/b/a;-><init>(FFF)V

    .line 46
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/t;

    invoke-direct {v0, p0, v5, v6, v3}, Lcom/google/android/apps/gmm/invocation/b/t;-><init>(Lcom/google/android/apps/gmm/invocation/b/s;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/streetview/b/a;)V

    return-object v0

    :cond_10
    move v0, v3

    .line 44
    goto :goto_a

    :cond_11
    move v0, v3

    goto :goto_b

    :cond_12
    move-object v0, v1

    goto :goto_c
.end method
