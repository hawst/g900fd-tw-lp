.class public Lcom/google/android/apps/gmm/cardui/a/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcom/google/android/apps/gmm/cardui/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    const-class v0, Lcom/google/android/apps/gmm/cardui/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/cardui/a/a;->a:Ljava/lang/String;

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/cardui/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/cardui/a/c;-><init>()V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/s;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/s;-><init>()V

    if-eqz v1, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/i;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/i;-><init>()V

    if-eqz v1, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/o;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/o;-><init>()V

    if-eqz v1, :cond_2

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/r;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/r;-><init>()V

    if-eqz v1, :cond_3

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/ab;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/ab;-><init>()V

    if-eqz v1, :cond_4

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/ah;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/ah;-><init>()V

    if-eqz v1, :cond_5

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/k;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/k;-><init>()V

    if-eqz v1, :cond_6

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/g;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/g;-><init>()V

    if-eqz v1, :cond_7

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/q;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/q;-><init>()V

    if-eqz v1, :cond_8

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/z;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/z;-><init>()V

    if-eqz v1, :cond_9

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/ac;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/ac;-><init>()V

    if-eqz v1, :cond_a

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/ad;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/ad;-><init>()V

    if-eqz v1, :cond_b

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_b
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/y;-><init>()V

    if-eqz v1, :cond_c

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_c
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/ai;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/ai;-><init>()V

    if-eqz v1, :cond_d

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_d
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/ag;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/ag;-><init>()V

    if-eqz v1, :cond_e

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_e
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/af;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/af;-><init>()V

    if-eqz v1, :cond_f

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_f
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/l;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/l;-><init>()V

    if-eqz v1, :cond_10

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_10
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/m;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/m;-><init>()V

    if-eqz v1, :cond_11

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_11
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/w;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/w;-><init>()V

    if-eqz v1, :cond_12

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_12
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/j;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/j;-><init>()V

    if-eqz v1, :cond_13

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_13
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    sget-object v1, Lcom/google/o/h/a/g;->x:Lcom/google/o/h/a/g;

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/n;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/n;-><init>()V

    if-eqz v1, :cond_14

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_14
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Ljava/util/Set;)V

    sput-object v0, Lcom/google/android/apps/gmm/cardui/a/a;->b:Lcom/google/android/apps/gmm/cardui/a/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/r/b/a/bn;
    .locals 4

    .prologue
    .line 104
    :try_start_0
    invoke-static {}, Lcom/google/r/b/a/bn;->newBuilder()Lcom/google/r/b/a/bs;

    move-result-object v0

    .line 105
    invoke-static {}, Lcom/google/android/apps/gmm/cardui/a/a;->b()Lcom/google/o/h/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/o/h/a/d;->l()[B

    move-result-object v1

    const/4 v2, 0x0

    array-length v3, v1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/n/b;->a([BII)Lcom/google/n/b;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bs;

    invoke-virtual {v0}, Lcom/google/r/b/a/bs;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bn;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    return-object v0

    .line 107
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/cardui/a/a;->a:Ljava/lang/String;

    .line 109
    invoke-static {}, Lcom/google/r/b/a/bn;->d()Lcom/google/r/b/a/bn;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;Lcom/google/android/apps/gmm/cardui/c;Lcom/google/android/apps/gmm/cardui/b/a;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/gmm/cardui/a/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/o/h/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/cardui/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 141
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 143
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    if-eqz p1, :cond_2

    instance-of v0, p1, Landroid/app/Fragment;

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "delegate must be null or an instance of Fragment, but delegate="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    if-nez p2, :cond_5

    move-object v7, v1

    .line 146
    :goto_1
    if-eqz v7, :cond_a

    .line 147
    new-instance v0, Lcom/google/android/apps/gmm/cardui/a/b;

    move-object v1, p2

    move-object v2, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/cardui/a/b;-><init>(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/android/apps/gmm/util/b/b;Lcom/google/android/apps/gmm/cardui/c;Lcom/google/android/apps/gmm/cardui/b/a;)V

    .line 149
    invoke-interface {v7, v0}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Lcom/google/android/apps/gmm/cardui/a/f;)V

    .line 153
    :goto_2
    return-void

    .line 143
    :cond_5
    if-eqz p1, :cond_7

    invoke-interface {p1, p2}, Lcom/google/android/apps/gmm/cardui/a/d;->a(Lcom/google/o/h/a/a;)Lcom/google/android/apps/gmm/cardui/a/e;

    move-result-object v0

    if-eqz v0, :cond_6

    move-object v7, v0

    goto :goto_1

    :cond_6
    sget-object v0, Lcom/google/android/apps/gmm/cardui/a/a;->a:Ljava/lang/String;

    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/cardui/a/a;->b:Lcom/google/android/apps/gmm/cardui/a/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/a/c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/cardui/a/e;

    invoke-interface {v0, p2}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Lcom/google/o/h/a/a;)Z

    move-result v3

    if-eqz v3, :cond_8

    move-object v7, v0

    goto :goto_1

    :cond_9
    move-object v7, v1

    goto :goto_1

    .line 151
    :cond_a
    sget-object v0, Lcom/google/android/apps/gmm/cardui/a/a;->a:Ljava/lang/String;

    goto :goto_2
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;[BLcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V
    .locals 10
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # [B
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/android/apps/gmm/suggest/d/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 193
    .line 194
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v6, p0

    check-cast v6, Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v7, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p1

    move-object v5, p4

    move-object v8, v4

    move-object v9, p5

    .line 193
    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/gmm/cardui/a/s;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;[BLjava/lang/String;Lcom/google/o/h/a/nj;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/base/activities/c;ZLcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/suggest/d/e;)V

    .line 196
    return-void
.end method

.method public static b()Lcom/google/o/h/a/d;
    .locals 4

    .prologue
    .line 118
    sget-object v0, Lcom/google/android/apps/gmm/cardui/a/a;->b:Lcom/google/android/apps/gmm/cardui/a/c;

    invoke-static {}, Lcom/google/o/h/a/d;->newBuilder()Lcom/google/o/h/a/i;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/a/c;->b:Ljava/util/Set;

    invoke-virtual {v1}, Lcom/google/o/h/a/i;->c()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/g;

    iget-object v3, v1, Lcom/google/o/h/a/i;->a:Ljava/util/List;

    iget v0, v0, Lcom/google/o/h/a/g;->K:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/google/o/h/a/i;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/d;

    return-object v0
.end method
