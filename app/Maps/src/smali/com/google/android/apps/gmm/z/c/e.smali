.class Lcom/google/android/apps/gmm/z/c/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/shared/net/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/shared/net/c",
        "<",
        "Lcom/google/r/b/a/apm;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/z/b/a;",
            ">;"
        }
    .end annotation
.end field

.field final b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/apps/gmm/z/c/b;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/z/c/b;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/z/b/a;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 118
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/c/e;->c:Lcom/google/android/apps/gmm/z/c/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-object p2, p0, Lcom/google/android/apps/gmm/z/c/e;->a:Ljava/util/List;

    .line 120
    iput-object p3, p0, Lcom/google/android/apps/gmm/z/c/e;->b:Ljava/lang/String;

    .line 121
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 5

    .prologue
    .line 113
    if-eqz p2, :cond_2

    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/d;->b()Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v0

    :goto_0
    sget-object v1, Lcom/google/android/apps/gmm/shared/net/k;->g:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/k;->f:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/k;->m:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/k;->i:Lcom/google/android/apps/gmm/shared/net/k;

    if-ne v0, v1, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/e;->c:Lcom/google/android/apps/gmm/z/c/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/e;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/e;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/c/b;->b:Lcom/google/android/apps/gmm/z/c/k;

    iget-object v3, v0, Lcom/google/android/apps/gmm/z/c/k;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/z/c/l;

    invoke-direct {v4, v0, v1, v2}, Lcom/google/android/apps/gmm/z/c/l;-><init>(Lcom/google/android/apps/gmm/z/c/k;Ljava/util/List;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v3, v4, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/e;->c:Lcom/google/android/apps/gmm/z/c/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/z/c/b;->c:Lcom/google/android/apps/gmm/z/c/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/c/d;->d()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/z/c/b;->b:Lcom/google/android/apps/gmm/z/c/k;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/c/k;->d()Ljava/lang/String;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/z/c/k;->f:Z

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/z/c/b;->c:Lcom/google/android/apps/gmm/z/c/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/c/d;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/z/c/b;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/z/c/c;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/z/c/c;-><init>(Lcom/google/android/apps/gmm/z/c/b;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_1
.end method
