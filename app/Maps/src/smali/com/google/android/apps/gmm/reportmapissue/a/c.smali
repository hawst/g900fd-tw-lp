.class public Lcom/google/android/apps/gmm/reportmapissue/a/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/base/g/c;

.field public final b:Lcom/google/android/apps/gmm/reportmapissue/a/g;

.field public final c:Lcom/google/android/apps/gmm/reportmapissue/a/g;

.field public final d:Lcom/google/android/apps/gmm/reportmapissue/a/g;

.field public final e:Lcom/google/android/apps/gmm/reportmapissue/a/g;

.field public final f:Lcom/google/android/apps/gmm/reportmapissue/a/a;

.field public final g:Lcom/google/android/apps/gmm/reportmapissue/a/a;

.field public final h:Lcom/google/android/apps/gmm/reportmapissue/a/a;

.field public final i:Lcom/google/android/apps/gmm/reportmapissue/a/a;

.field public final j:Lcom/google/android/apps/gmm/reportmapissue/a/a;

.field public final k:Lcom/google/android/apps/gmm/reportmapissue/a/a;

.field public final l:Lcom/google/android/apps/gmm/reportmapissue/a/e;

.field public final m:Lcom/google/android/apps/gmm/reportmapissue/a/b;

.field public n:Ljava/lang/String;

.field public final o:Z

.field public final p:Z

.field public final q:Z

.field public final r:Ljava/lang/String;

.field public final s:Lcom/google/maps/g/hs;

.field public final t:Lcom/google/maps/g/hq;

.field public final u:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hs;Lcom/google/maps/g/hq;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->a:Lcom/google/android/apps/gmm/base/g/c;

    .line 46
    iput-object p2, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->s:Lcom/google/maps/g/hs;

    .line 47
    iput-object p3, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->t:Lcom/google/maps/g/hq;

    .line 49
    new-instance v5, Lcom/google/android/apps/gmm/reportmapissue/a/g;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v4, Lcom/google/maps/g/om;->f:Lcom/google/maps/g/om;

    if-ne v0, v4, :cond_9

    move v0, v2

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/om;->b:Lcom/google/maps/g/om;

    if-eq v1, v0, :cond_0

    sget-object v0, Lcom/google/maps/g/om;->a:Lcom/google/maps/g/om;

    if-eq v1, v0, :cond_0

    sget-object v0, Lcom/google/maps/g/om;->c:Lcom/google/maps/g/om;

    if-eq v1, v0, :cond_0

    sget-object v0, Lcom/google/maps/g/om;->d:Lcom/google/maps/g/om;

    if-ne v1, v0, :cond_a

    :cond_0
    move v0, v2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    sget-object v0, Lcom/google/android/apps/gmm/reportmapissue/a/d;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/maps/g/om;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v5, v6, v0}, Lcom/google/android/apps/gmm/reportmapissue/a/g;-><init>(Ljava/lang/Boolean;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->b:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    .line 50
    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/a/g;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v5, Lcom/google/maps/g/om;->f:Lcom/google/maps/g/om;

    if-ne v0, v5, :cond_d

    move v0, v2

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/om;->e:Lcom/google/maps/g/om;

    if-eq v4, v0, :cond_1

    sget-object v0, Lcom/google/maps/g/om;->d:Lcom/google/maps/g/om;

    if-ne v4, v0, :cond_e

    :cond_1
    move v0, v2

    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    sget-object v0, Lcom/google/android/apps/gmm/reportmapissue/a/d;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/maps/g/om;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_1

    const/16 v0, 0xa

    new-instance v5, Lcom/google/b/a/ab;

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->f:Lcom/google/n/aq;

    iput-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v6, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-direct {v1, v4, v0}, Lcom/google/android/apps/gmm/reportmapissue/a/g;-><init>(Ljava/lang/Boolean;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->c:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    .line 51
    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/a/g;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v4, Lcom/google/maps/g/om;->f:Lcom/google/maps/g/om;

    if-ne v0, v4, :cond_f

    move v0, v2

    :goto_6
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    sget-object v0, Lcom/google/android/apps/gmm/reportmapissue/a/d;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/maps/g/om;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->o()Ljava/lang/String;

    move-result-object v0

    :goto_7
    invoke-direct {v1, v4, v0}, Lcom/google/android/apps/gmm/reportmapissue/a/g;-><init>(Ljava/lang/Boolean;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->d:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    .line 52
    new-instance v5, Lcom/google/android/apps/gmm/reportmapissue/a/g;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/om;->f:Lcom/google/maps/g/om;

    if-ne v0, v1, :cond_11

    move v0, v2

    :goto_8
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/om;->d:Lcom/google/maps/g/om;

    if-ne v0, v1, :cond_12

    :cond_3
    move v0, v2

    :goto_9
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    sget-object v0, Lcom/google/android/apps/gmm/reportmapissue/a/d;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/maps/g/om;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->x()Lcom/google/maps/g/hg;

    move-result-object v0

    if-eqz v0, :cond_17

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->x()Lcom/google/maps/g/hg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    :goto_a
    invoke-direct {v5, v6, v0}, Lcom/google/android/apps/gmm/reportmapissue/a/g;-><init>(Ljava/lang/Boolean;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->e:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    .line 53
    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/a/b;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v4

    .line 54
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v6, Lcom/google/maps/g/om;->f:Lcom/google/maps/g/om;

    if-ne v0, v6, :cond_18

    move v0, v2

    :goto_b
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/maps/g/om;->d:Lcom/google/maps/g/om;

    if-eq v5, v0, :cond_4

    sget-object v0, Lcom/google/maps/g/om;->e:Lcom/google/maps/g/om;

    if-ne v5, v0, :cond_19

    :cond_4
    move v0, v2

    :goto_c
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v1, v4, v0, v5}, Lcom/google/android/apps/gmm/reportmapissue/a/b;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->m:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    .line 56
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/om;->a:Lcom/google/maps/g/om;

    if-ne v0, v1, :cond_1a

    move v0, v2

    :goto_d
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->p:Z

    .line 57
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/om;->b:Lcom/google/maps/g/om;

    if-eq v0, v1, :cond_5

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/om;->c:Lcom/google/maps/g/om;

    if-ne v0, v1, :cond_1b

    :cond_5
    move v0, v2

    :goto_e
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->q:Z

    .line 58
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->p:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->q:Z

    if-eqz v0, :cond_1c

    :cond_6
    move v0, v2

    .line 59
    :goto_f
    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v1, v0, v4}, Lcom/google/android/apps/gmm/reportmapissue/a/a;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->f:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    .line 62
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/om;->d:Lcom/google/maps/g/om;

    if-ne v0, v1, :cond_1d

    move v0, v2

    .line 63
    :goto_10
    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v1, v0, v4}, Lcom/google/android/apps/gmm/reportmapissue/a/a;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->g:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    .line 66
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/om;->a:Lcom/google/maps/g/om;

    if-ne v0, v1, :cond_1e

    move v0, v2

    .line 67
    :goto_11
    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v1, v0, v4}, Lcom/google/android/apps/gmm/reportmapissue/a/a;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->h:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    .line 70
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/om;->a:Lcom/google/maps/g/om;

    if-ne v0, v1, :cond_1f

    move v0, v2

    .line 71
    :goto_12
    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v1, v0, v4}, Lcom/google/android/apps/gmm/reportmapissue/a/a;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->i:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    .line 74
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/om;->a:Lcom/google/maps/g/om;

    if-ne v0, v1, :cond_20

    move v0, v2

    .line 75
    :goto_13
    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/a/a;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v1, v0, v4}, Lcom/google/android/apps/gmm/reportmapissue/a/a;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->j:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    .line 77
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/om;->f:Lcom/google/maps/g/om;

    if-ne v0, v1, :cond_21

    move v0, v2

    :goto_14
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 78
    if-eqz v1, :cond_22

    sget-object v0, Lcom/google/android/apps/gmm/reportmapissue/a/d;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/maps/g/om;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_4

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_15
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_22

    move v0, v2

    .line 79
    :goto_16
    new-instance v4, Lcom/google/android/apps/gmm/reportmapissue/a/a;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {v4, v1, v0}, Lcom/google/android/apps/gmm/reportmapissue/a/a;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    iput-object v4, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->k:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    .line 80
    new-instance v0, Lcom/google/android/apps/gmm/reportmapissue/a/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/reportmapissue/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->l:Lcom/google/android/apps/gmm/reportmapissue/a/e;

    .line 82
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/om;->a:Lcom/google/maps/g/om;

    if-ne v0, v1, :cond_23

    :goto_17
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->o:Z

    .line 83
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    .line 84
    iget-object v1, v0, Lcom/google/maps/g/oj;->c:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_24

    check-cast v1, Ljava/lang/String;

    :goto_18
    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->r:Ljava/lang/String;

    .line 85
    sget-object v2, Lcom/google/android/apps/gmm/reportmapissue/a/d;->a:[I

    iget v1, v0, Lcom/google/maps/g/oj;->b:I

    invoke-static {v1}, Lcom/google/maps/g/om;->a(I)Lcom/google/maps/g/om;

    move-result-object v1

    if-nez v1, :cond_7

    sget-object v1, Lcom/google/maps/g/om;->g:Lcom/google/maps/g/om;

    :cond_7
    invoke-virtual {v1}, Lcom/google/maps/g/om;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_5

    const/16 v0, 0x20

    new-instance v1, Lcom/google/b/a/ab;

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->a:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    if-nez v2, :cond_8

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v2, v2, Lcom/google/r/b/a/ads;->f:Lcom/google/n/aq;

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    :cond_8
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_19
    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/a/c;->u:Ljava/lang/String;

    .line 86
    return-void

    :cond_9
    move v0, v3

    .line 49
    goto/16 :goto_0

    :cond_a
    move v0, v3

    goto/16 :goto_1

    :pswitch_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->ac()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->ad()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :pswitch_2
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->ae()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :pswitch_3
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->af()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :pswitch_4
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget-object v0, v0, Lcom/google/maps/g/oj;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/eu;->d()Lcom/google/maps/g/eu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/eu;

    iget-object v1, v0, Lcom/google/maps/g/eu;->b:Ljava/lang/Object;

    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_b

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_2

    :cond_b
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_c

    iput-object v4, v0, Lcom/google/maps/g/eu;->b:Ljava/lang/Object;

    :cond_c
    move-object v0, v4

    goto/16 :goto_2

    :cond_d
    move v0, v3

    .line 50
    goto/16 :goto_3

    :cond_e
    move v0, v3

    goto/16 :goto_4

    :pswitch_5
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->ab()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :pswitch_6
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->ag()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :pswitch_7
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->ah()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_f
    move v0, v3

    .line 51
    goto/16 :goto_6

    :pswitch_8
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget-object v0, v0, Lcom/google/maps/g/oj;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/eu;->d()Lcom/google/maps/g/eu;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/eu;

    iget-object v0, v0, Lcom/google/maps/g/eu;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_10

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_7

    :cond_10
    const-string v0, ""

    goto/16 :goto_7

    :cond_11
    move v0, v3

    .line 52
    goto/16 :goto_8

    :cond_12
    move v0, v3

    goto/16 :goto_9

    :pswitch_9
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget-object v0, v0, Lcom/google/maps/g/oj;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/we;->d()Lcom/google/maps/g/we;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/we;

    iget-object v1, v0, Lcom/google/maps/g/we;->e:Ljava/lang/Object;

    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_13

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_a

    :cond_13
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_14

    iput-object v4, v0, Lcom/google/maps/g/we;->e:Ljava/lang/Object;

    :cond_14
    move-object v0, v4

    goto/16 :goto_a

    :pswitch_a
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget-object v0, v0, Lcom/google/maps/g/oj;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/eu;->d()Lcom/google/maps/g/eu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/eu;

    iget-object v1, v0, Lcom/google/maps/g/eu;->f:Ljava/lang/Object;

    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_15

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_a

    :cond_15
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_16

    iput-object v4, v0, Lcom/google/maps/g/eu;->f:Ljava/lang/Object;

    :cond_16
    move-object v0, v4

    goto/16 :goto_a

    :cond_17
    const/4 v0, 0x0

    goto/16 :goto_a

    :cond_18
    move v0, v3

    .line 54
    goto/16 :goto_b

    :cond_19
    move v0, v3

    goto/16 :goto_c

    :cond_1a
    move v0, v3

    .line 56
    goto/16 :goto_d

    :cond_1b
    move v0, v3

    .line 57
    goto/16 :goto_e

    :cond_1c
    move v0, v3

    .line 58
    goto/16 :goto_f

    :cond_1d
    move v0, v3

    .line 62
    goto/16 :goto_10

    :cond_1e
    move v0, v3

    .line 66
    goto/16 :goto_11

    :cond_1f
    move v0, v3

    .line 70
    goto/16 :goto_12

    :cond_20
    move v0, v3

    .line 74
    goto/16 :goto_13

    :cond_21
    move v0, v3

    .line 77
    goto/16 :goto_14

    .line 78
    :pswitch_b
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget-object v0, v0, Lcom/google/maps/g/oj;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/eu;->d()Lcom/google/maps/g/eu;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/eu;

    iget-boolean v0, v0, Lcom/google/maps/g/eu;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_15

    :cond_22
    move v0, v3

    goto/16 :goto_16

    :cond_23
    move v2, v3

    .line 82
    goto/16 :goto_17

    .line 84
    :cond_24
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_25

    iput-object v2, v0, Lcom/google/maps/g/oj;->c:Ljava/lang/Object;

    :cond_25
    move-object v1, v2

    goto/16 :goto_18

    .line 85
    :pswitch_c
    iget-object v0, v0, Lcom/google/maps/g/oj;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gg;->d()Lcom/google/maps/g/gg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gg;

    iget-object v1, v0, Lcom/google/maps/g/gg;->c:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_26

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_19

    :cond_26
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_27

    iput-object v2, v0, Lcom/google/maps/g/gg;->c:Ljava/lang/Object;

    :cond_27
    move-object v0, v2

    goto/16 :goto_19

    :pswitch_d
    iget-object v0, v0, Lcom/google/maps/g/oj;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/we;->d()Lcom/google/maps/g/we;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/we;

    iget-object v1, v0, Lcom/google/maps/g/we;->d:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_28

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_19

    :cond_28
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_29

    iput-object v2, v0, Lcom/google/maps/g/we;->d:Ljava/lang/Object;

    :cond_29
    move-object v0, v2

    goto/16 :goto_19

    :pswitch_e
    iget-object v0, v0, Lcom/google/maps/g/oj;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/eu;->d()Lcom/google/maps/g/eu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/eu;

    iget-object v1, v0, Lcom/google/maps/g/eu;->d:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_2a

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_19

    :cond_2a
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_2b

    iput-object v2, v0, Lcom/google/maps/g/eu;->d:Ljava/lang/Object;

    :cond_2b
    move-object v0, v2

    goto/16 :goto_19

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 50
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_6
        :pswitch_7
        :pswitch_5
    .end packed-switch

    .line 51
    :pswitch_data_2
    .packed-switch 0x5
        :pswitch_8
    .end packed-switch

    .line 52
    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 78
    :pswitch_data_4
    .packed-switch 0x5
        :pswitch_b
    .end packed-switch

    .line 85
    :pswitch_data_5
    .packed-switch 0x4
        :pswitch_d
        :pswitch_e
        :pswitch_c
    .end packed-switch
.end method
