.class public Lcom/google/android/apps/gmm/suggest/g/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/e;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/t;

.field private final b:Lcom/google/r/b/a/apq;

.field private final c:Lcom/google/android/apps/gmm/addaplace/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/t;Lcom/google/r/b/a/apq;Lcom/google/android/apps/gmm/addaplace/a/b;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/gmm/suggest/g/a;->a:Lcom/google/android/apps/gmm/map/t;

    .line 25
    iput-object p2, p0, Lcom/google/android/apps/gmm/suggest/g/a;->b:Lcom/google/r/b/a/apq;

    .line 26
    iput-object p3, p0, Lcom/google/android/apps/gmm/suggest/g/a;->c:Lcom/google/android/apps/gmm/addaplace/a/b;

    .line 27
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 39
    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/g/a;->b:Lcom/google/r/b/a/apq;

    iget-object v0, v2, Lcom/google/r/b/a/apq;->i:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lcom/google/r/b/a/apq;->i:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 50
    sget-object v0, Lcom/google/maps/g/hs;->m:Lcom/google/maps/g/hs;

    invoke-static {v0}, Lcom/google/android/apps/gmm/addaplace/a/a;->a(Lcom/google/maps/g/hs;)Lcom/google/android/apps/gmm/addaplace/a/a;

    move-result-object v2

    .line 51
    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/g/a;->c:Lcom/google/android/apps/gmm/addaplace/a/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/a;->a:Lcom/google/android/apps/gmm/map/t;

    .line 52
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->b()Lcom/google/maps/g/gy;

    move-result-object v0

    .line 51
    invoke-interface {v3, v2, v0}, Lcom/google/android/apps/gmm/addaplace/a/b;->a(Lcom/google/android/apps/gmm/addaplace/a/a;Lcom/google/maps/g/gy;)V

    .line 53
    return-object v1

    .line 52
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/b/f/t;->R:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method
