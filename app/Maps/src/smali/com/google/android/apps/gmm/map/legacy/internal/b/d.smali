.class public Lcom/google/android/apps/gmm/map/legacy/internal/b/d;
.super Lcom/google/android/apps/gmm/map/legacy/internal/b/b;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

.field b:Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

.field c:Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

.field final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/internal/b/a;",
            ">;"
        }
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/map/b/a/ae;

.field f:F

.field public final g:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

.field public final i:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

.field private j:Lcom/google/android/apps/gmm/map/legacy/internal/b/e;

.field private final k:Lcom/google/android/apps/gmm/map/t/l;

.field private final l:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ab;II[Lcom/google/android/apps/gmm/map/internal/c/aq;FLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/v/ad;ZZ)V
    .locals 6

    .prologue
    .line 102
    invoke-direct {p0, p7}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;-><init>(Lcom/google/android/apps/gmm/v/ad;)V

    .line 103
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    invoke-direct {v0, p1, p4}, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/aq;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d:Ljava/util/List;

    .line 106
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    const/16 v2, 0xff

    const/16 v3, 0xff

    const/16 v4, 0xff

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    move v1, p3

    move v2, p5

    move v4, p8

    move v5, p9

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;-><init>(IFIZZ)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    .line 108
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    const v1, 0x3e99999a    # 0.3f

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    const/16 v2, 0xff

    const/16 v3, 0xff

    const/16 v4, 0xff

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    move v1, p3

    move v2, p5

    move v4, p8

    move v5, p9

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;-><init>(IFIZZ)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    .line 110
    iput p2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->l:I

    .line 111
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->k:Lcom/google/android/apps/gmm/map/t/l;

    .line 112
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/map/legacy/internal/b/a;FI)Lcom/google/android/apps/gmm/map/legacy/internal/b/a;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v1, v1

    div-int/lit8 v2, v1, 0x3

    new-array v9, v2, [Z

    const/4 v1, 0x2

    if-gt v2, v1, :cond_0

    move v0, v3

    :goto_0
    if-ge v0, v2, :cond_1

    aput-boolean v10, v9, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    aput-boolean v10, v9, v3

    add-int/lit8 v1, v2, -0x1

    aput-boolean v10, v9, v1

    mul-float v1, p1, p1

    add-int/lit8 v4, v2, -0x1

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v7, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    move v2, p2

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(FIIILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;[Z)I

    .line 316
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    if-eqz v0, :cond_5

    move v0, v3

    .line 317
    :goto_1
    array-length v1, v9

    if-ge v0, v1, :cond_5

    .line 318
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    if-nez v1, :cond_3

    move v1, v3

    :goto_2
    if-eqz v1, :cond_2

    .line 319
    aput-boolean v10, v9, v0

    .line 317
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 318
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    aget-object v1, v1, v0

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ar;->a:Lcom/google/android/apps/gmm/map/internal/c/ar;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/aq;->a:I

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/ar;->c:I

    and-int/2addr v1, v2

    if-eqz v1, :cond_4

    move v1, v10

    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_2

    :cond_5
    move v0, v3

    move v1, v3

    .line 326
    :goto_3
    array-length v2, v9

    if-ge v0, v2, :cond_7

    .line 327
    aget-boolean v2, v9, v0

    if-eqz v2, :cond_6

    .line 328
    add-int/lit8 v1, v1, 0x1

    .line 326
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 332
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    if-ne v1, v0, :cond_8

    .line 354
    :goto_4
    return-object p0

    .line 336
    :cond_8
    const/4 v0, 0x0

    .line 337
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    if-eqz v2, :cond_9

    .line 338
    new-array v0, v1, [Lcom/google/android/apps/gmm/map/internal/c/aq;

    .line 340
    :cond_9
    mul-int/lit8 v1, v1, 0x3

    new-array v2, v1, [I

    .line 342
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    move v1, v3

    .line 343
    :goto_5
    array-length v5, v9

    if-ge v3, v5, :cond_c

    .line 344
    aget-boolean v5, v9, v3

    if-eqz v5, :cond_b

    .line 345
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v5, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 346
    mul-int/lit8 v5, v1, 0x3

    iget v6, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    aput v6, v2, v5

    add-int/lit8 v6, v5, 0x1

    iget v7, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    aput v7, v2, v6

    add-int/lit8 v5, v5, 0x2

    iget v6, v4, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    aput v6, v2, v5

    .line 347
    if-eqz v0, :cond_a

    .line 348
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    aget-object v5, v5, v3

    aput-object v5, v0, v1

    .line 350
    :cond_a
    add-int/lit8 v1, v1, 0x1

    .line 343
    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 353
    :cond_c
    new-instance p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    .line 354
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a([I)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/aq;)V

    goto :goto_4
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->b()V

    .line 179
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 5

    .prologue
    .line 120
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/r;

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->l:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a:[Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    aget-object v0, v0, v1

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    new-instance v3, Lcom/google/android/apps/gmm/map/t/q;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->k:Lcom/google/android/apps/gmm/map/t/l;

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/map/t/q;-><init>(Lcom/google/android/apps/gmm/map/t/k;)V

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/q;Lcom/google/android/apps/gmm/v/ci;)V

    .line 122
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    new-instance v3, Lcom/google/android/apps/gmm/map/t/q;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->k:Lcom/google/android/apps/gmm/map/t/l;

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/map/t/q;-><init>(Lcom/google/android/apps/gmm/map/t/k;)V

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/q;Lcom/google/android/apps/gmm/v/ci;)V

    .line 124
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/e;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/e;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/b/d;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/internal/b/e;

    .line 128
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/internal/b/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 129
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/af;)V
    .locals 0

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d()V

    .line 174
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a()V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a()V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/internal/b/e;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->h:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/internal/b/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 187
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/internal/b/e;

    .line 189
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/internal/b/e;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/internal/b/e;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/e;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/e;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 147
    :cond_0
    return-void
.end method
