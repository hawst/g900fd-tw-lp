.class public abstract Lcom/google/android/apps/gmm/v/bh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public f:Lcom/google/android/apps/gmm/v/n;

.field public g:I

.field public h:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bh;->f:Lcom/google/android/apps/gmm/v/n;

    .line 38
    iput p1, p0, Lcom/google/android/apps/gmm/v/bh;->h:I

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/v/bh;->g:I

    .line 42
    :goto_0
    and-int/lit8 v0, p1, 0x1

    if-eq v0, v1, :cond_0

    .line 43
    ushr-int/lit8 p1, p1, 0x1

    .line 44
    iget v0, p0, Lcom/google/android/apps/gmm/v/bh;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/bh;->g:I

    goto :goto_0

    .line 46
    :cond_0
    if-eq p1, v1, :cond_1

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only a single pass can be included in the mask"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_1
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/google/android/apps/gmm/v/aa;)V
.end method

.method public abstract a(Lcom/google/android/apps/gmm/v/ad;)V
.end method

.method public a(Lcom/google/android/apps/gmm/v/n;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bh;->f:Lcom/google/android/apps/gmm/v/n;

    if-eqz v0, :cond_0

    .line 57
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "setCamera can only be called once"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/bh;->f:Lcom/google/android/apps/gmm/v/n;

    .line 60
    return-void
.end method

.method public abstract a()Z
.end method

.method public abstract b(Lcom/google/android/apps/gmm/v/ad;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/v/ad;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(Lcom/google/android/apps/gmm/v/aa;)V
.end method
