.class public abstract Lcom/google/android/apps/gmm/myplaces/c/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final e:Lcom/google/android/apps/gmm/myplaces/c/h;

.field public final f:J

.field public final g:J


# direct methods
.method private constructor <init>(Lcom/google/android/apps/gmm/myplaces/c/h;JJ)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/c/f;->e:Lcom/google/android/apps/gmm/myplaces/c/h;

    .line 78
    iput-wide p2, p0, Lcom/google/android/apps/gmm/myplaces/c/f;->f:J

    .line 79
    iput-wide p4, p0, Lcom/google/android/apps/gmm/myplaces/c/f;->g:J

    .line 80
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;JJ)V
    .locals 6

    .prologue
    .line 84
    new-instance v1, Lcom/google/android/apps/gmm/myplaces/c/h;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/myplaces/c/h;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-wide v2, p2

    move-wide v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/myplaces/c/f;-><init>(Lcom/google/android/apps/gmm/myplaces/c/h;JJ)V

    .line 85
    return-void
.end method

.method public static a(Ljava/lang/String;J)Lcom/google/android/apps/gmm/myplaces/c/f;
    .locals 7

    .prologue
    .line 91
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/g;

    const-string v1, ""

    const-wide/16 v2, 0x0

    move-wide v4, p1

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/myplaces/c/g;-><init>(Ljava/lang/String;JJLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public a()Z
    .locals 4

    .prologue
    .line 130
    iget-wide v0, p0, Lcom/google/android/apps/gmm/myplaces/c/f;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
