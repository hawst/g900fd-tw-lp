.class public Lcom/google/android/apps/gmm/p/d/f;
.super Landroid/location/Location;
.source "PG"


# static fields
.field public static final a:J


# instance fields
.field public b:Lcom/google/o/b/a/aa;

.field public c:Lcom/google/o/b/a/h;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public d:I

.field public e:Lcom/google/o/b/a/h;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public f:I

.field public g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 24
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/p/d/f;->a:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0, p1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 38
    sget-object v0, Lcom/google/o/b/a/aa;->k:Lcom/google/o/b/a/aa;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/d/f;->b:Lcom/google/o/b/a/aa;

    .line 58
    const/16 v0, 0x44

    iput v0, p0, Lcom/google/android/apps/gmm/p/d/f;->f:I

    .line 67
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/o/b/a/v;
    .locals 8

    .prologue
    const-wide v6, 0x416312d000000000L    # 1.0E7

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 123
    invoke-static {}, Lcom/google/o/b/a/v;->newBuilder()Lcom/google/o/b/a/y;

    move-result-object v1

    sget-object v0, Lcom/google/o/b/a/ae;->b:Lcom/google/o/b/a/ae;

    .line 124
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v1, Lcom/google/o/b/a/y;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/o/b/a/y;->a:I

    iget v0, v0, Lcom/google/o/b/a/ae;->j:I

    iput v0, v1, Lcom/google/o/b/a/y;->b:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d/f;->b:Lcom/google/o/b/a/aa;

    .line 125
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v2, v1, Lcom/google/o/b/a/y;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/o/b/a/y;->a:I

    iget v0, v0, Lcom/google/o/b/a/aa;->ae:I

    iput v0, v1, Lcom/google/o/b/a/y;->c:I

    .line 126
    invoke-static {}, Lcom/google/o/b/a/l;->newBuilder()Lcom/google/o/b/a/n;

    move-result-object v0

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/d/f;->getLatitude()D

    move-result-wide v2

    mul-double/2addr v2, v6

    double-to-int v2, v2

    iget v3, v0, Lcom/google/o/b/a/n;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/google/o/b/a/n;->a:I

    iput v2, v0, Lcom/google/o/b/a/n;->b:I

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/d/f;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v6

    double-to-int v2, v2

    iget v3, v0, Lcom/google/o/b/a/n;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v0, Lcom/google/o/b/a/n;->a:I

    iput v2, v0, Lcom/google/o/b/a/n;->c:I

    .line 129
    invoke-virtual {v0}, Lcom/google/o/b/a/n;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/l;

    .line 126
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v2, v1, Lcom/google/o/b/a/y;->e:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/b/a/y;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, v1, Lcom/google/o/b/a/y;->a:I

    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/d/f;->getAccuracy()F

    move-result v0

    .line 131
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-lez v2, :cond_3

    .line 132
    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v2

    iget v2, v1, Lcom/google/o/b/a/y;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, v1, Lcom/google/o/b/a/y;->a:I

    iput v0, v1, Lcom/google/o/b/a/y;->f:F

    .line 134
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/p/d/f;->f:I

    iget v2, v1, Lcom/google/o/b/a/y;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, v1, Lcom/google/o/b/a/y;->a:I

    iput v0, v1, Lcom/google/o/b/a/y;->g:I

    .line 135
    invoke-static {}, Lcom/google/o/b/a/p;->newBuilder()Lcom/google/o/b/a/t;

    move-result-object v0

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/d/f;->hasBearing()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/d/f;->getBearing()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget v3, v0, Lcom/google/o/b/a/t;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v0, Lcom/google/o/b/a/t;->a:I

    iput v2, v0, Lcom/google/o/b/a/t;->b:I

    .line 139
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/d/f;->hasSpeed()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 141
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/d/f;->getSpeed()F

    move-result v2

    const v3, 0x40666666    # 3.6f

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget v3, v0, Lcom/google/o/b/a/t;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, v0, Lcom/google/o/b/a/t;->a:I

    iput v2, v0, Lcom/google/o/b/a/t;->c:I

    .line 143
    :cond_5
    invoke-virtual {v0}, Lcom/google/o/b/a/t;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/p;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    iget-object v2, v1, Lcom/google/o/b/a/y;->k:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/b/a/y;->a:I

    const/high16 v2, 0x40000

    or-int/2addr v0, v2

    iput v0, v1, Lcom/google/o/b/a/y;->a:I

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d/f;->c:Lcom/google/o/b/a/h;

    if-eqz v0, :cond_8

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d/f;->c:Lcom/google/o/b/a/h;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget-object v2, v1, Lcom/google/o/b/a/y;->i:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/b/a/y;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, v1, Lcom/google/o/b/a/y;->a:I

    .line 146
    iget v0, p0, Lcom/google/android/apps/gmm/p/d/f;->d:I

    int-to-float v0, v0

    const v2, 0x3a83126f    # 0.001f

    mul-float/2addr v0, v2

    iget v2, v1, Lcom/google/o/b/a/y;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, v1, Lcom/google/o/b/a/y;->a:I

    iput v0, v1, Lcom/google/o/b/a/y;->j:F

    .line 148
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d/f;->e:Lcom/google/o/b/a/h;

    if-eqz v0, :cond_a

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d/f;->e:Lcom/google/o/b/a/h;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    iget-object v2, v1, Lcom/google/o/b/a/y;->h:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/b/a/y;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, v1, Lcom/google/o/b/a/y;->a:I

    .line 151
    :cond_a
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/d/f;->g:Z

    if-eqz v0, :cond_b

    .line 152
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/d/f;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v2

    iget v0, v1, Lcom/google/o/b/a/y;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v1, Lcom/google/o/b/a/y;->a:I

    iput-wide v2, v1, Lcom/google/o/b/a/y;->d:J

    .line 154
    :cond_b
    invoke-virtual {v1}, Lcom/google/o/b/a/y;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/v;

    return-object v0
.end method
