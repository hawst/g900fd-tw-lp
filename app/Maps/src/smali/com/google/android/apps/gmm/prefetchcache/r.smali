.class public Lcom/google/android/apps/gmm/prefetchcache/r;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/prefetch/a/c;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/prefetchcache/api/c;",
            ">;"
        }
    .end annotation
.end field

.field c:[Landroid/accounts/Account;

.field d:Lcom/google/r/b/a/a/af;

.field final e:Lcom/google/android/apps/gmm/base/a;

.field final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/prefetchcache/api/d;",
            ">;"
        }
    .end annotation
.end field

.field g:Landroid/accounts/Account;

.field h:Lcom/google/android/apps/gmm/map/internal/c/cd;

.field private i:Lcom/google/android/apps/gmm/prefetch/a/b;

.field private final j:Lcom/google/android/apps/gmm/prefetch/a/f;

.field private k:J

.field private volatile l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/google/android/apps/gmm/prefetchcache/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/prefetch/a/f;Lcom/google/android/apps/gmm/base/a;)V
    .locals 2

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->k:J

    .line 97
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->j:Lcom/google/android/apps/gmm/prefetch/a/f;

    .line 98
    iput-object p2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->f:Ljava/util/List;

    .line 101
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/cd;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->h:Lcom/google/android/apps/gmm/map/internal/c/cd;

    .line 102
    return-void
.end method

.method public static declared-synchronized a(Lcom/google/android/apps/gmm/prefetch/a/f;Lcom/google/android/apps/gmm/base/a;)Lcom/google/android/apps/gmm/prefetchcache/r;
    .locals 2

    .prologue
    .line 107
    const-class v1, Lcom/google/android/apps/gmm/prefetchcache/r;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/android/apps/gmm/prefetchcache/r;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/prefetchcache/r;-><init>(Lcom/google/android/apps/gmm/prefetch/a/f;Lcom/google/android/apps/gmm/base/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    monitor-exit v1

    return-object v0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(ILcom/google/android/apps/gmm/prefetchcache/api/e;)V
    .locals 3

    .prologue
    .line 503
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->f:Ljava/util/List;

    monitor-enter v1

    .line 504
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetchcache/api/d;

    .line 505
    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/prefetchcache/api/d;->a(ILcom/google/android/apps/gmm/prefetchcache/api/e;)V

    goto :goto_0

    .line 507
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private static a(Ljava/util/List;Lcom/google/r/b/a/a/af;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/prefetchcache/api/c;",
            ">;",
            "Lcom/google/r/b/a/a/af;",
            ")V"
        }
    .end annotation

    .prologue
    .line 512
    monitor-enter p0

    .line 513
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 514
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->a()Lcom/google/r/b/a/a/af;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;Lcom/google/r/b/a/a/af;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    invoke-interface {p0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 516
    monitor-exit p0

    .line 519
    :goto_1
    return-void

    .line 513
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 519
    :cond_1
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized d(Lcom/google/r/b/a/a/af;)V
    .locals 4

    .prologue
    .line 589
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/prefetchcache/r;->a(Ljava/util/List;Lcom/google/r/b/a/a/af;)V

    .line 591
    invoke-static {}, Lcom/google/r/b/a/a/af;->newBuilder()Lcom/google/r/b/a/a/ah;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/r/b/a/a/ah;->a(Lcom/google/r/b/a/a/af;)Lcom/google/r/b/a/a/ah;

    move-result-object v0

    sget-object v1, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    .line 592
    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 589
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 592
    :cond_0
    :try_start_1
    iget v2, v0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/r/b/a/a/ah;->a:I

    iget v1, v1, Lcom/google/r/b/a/a/ai;->f:I

    iput v1, v0, Lcom/google/r/b/a/a/ah;->d:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    .line 593
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    iget v1, v0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Lcom/google/r/b/a/a/ah;->a:I

    iput-wide v2, v0, Lcom/google/r/b/a/a/ah;->e:J

    .line 594
    invoke-virtual {v0}, Lcom/google/r/b/a/a/ah;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    .line 596
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/prefetchcache/t;

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/prefetchcache/t;-><init>(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/c/a;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 597
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    .line 185
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/r;->c()V

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->k:J

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->l:I

    iput-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    iput-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->c:[Landroid/accounts/Account;

    .line 188
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/prefetch/a/b;)V
    .locals 0

    .prologue
    .line 524
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->i:Lcom/google/android/apps/gmm/prefetch/a/b;

    .line 525
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/prefetch/a/d;II)V
    .locals 3

    .prologue
    .line 529
    sub-int v0, p2, p3

    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, p2

    .line 531
    if-lez v0, :cond_1

    const/16 v1, 0x64

    if-ge v0, v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->l:I

    if-eq v0, v1, :cond_1

    .line 533
    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/d;->a:Lcom/google/android/apps/gmm/prefetch/a/d;

    if-ne p1, v1, :cond_2

    .line 534
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/api/e;->b:Lcom/google/android/apps/gmm/prefetchcache/api/e;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/r;->a(ILcom/google/android/apps/gmm/prefetchcache/api/e;)V

    .line 538
    :cond_0
    :goto_0
    iput v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->l:I

    .line 540
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onPrefetchUpdate for action "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " called: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 541
    return-void

    .line 535
    :cond_2
    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/d;->b:Lcom/google/android/apps/gmm/prefetch/a/d;

    if-ne p1, v1, :cond_0

    .line 536
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/api/e;->c:Lcom/google/android/apps/gmm/prefetchcache/api/e;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/r;->a(ILcom/google/android/apps/gmm/prefetchcache/api/e;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/prefetch/a/d;Lcom/google/android/apps/gmm/prefetch/a/e;)V
    .locals 4

    .prologue
    .line 545
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    if-nez v0, :cond_0

    .line 546
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 585
    :goto_0
    monitor-exit p0

    return-void

    .line 549
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    iget v0, v0, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v0}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_1
    sget-object v1, Lcom/google/r/b/a/a/ai;->b:Lcom/google/r/b/a/a/ai;

    if-eq v0, v1, :cond_3

    .line 550
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    .line 551
    iget v0, v0, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v0}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected state of area in progress: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 550
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 553
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->a:Lcom/google/android/apps/gmm/prefetch/a/e;

    if-ne p2, v0, :cond_5

    .line 558
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    const-string v0, "Removing canceled area: "

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    invoke-virtual {v1}, Lcom/google/r/b/a/a/af;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 559
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/r;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 545
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 558
    :cond_4
    :try_start_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 563
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    .line 564
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    .line 565
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->i:Lcom/google/android/apps/gmm/prefetch/a/b;

    .line 567
    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/e;->b:Lcom/google/android/apps/gmm/prefetch/a/e;

    if-eq p2, v1, :cond_6

    .line 568
    const/4 v1, -0x1

    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/s;->c:[I

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/prefetch/a/e;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/api/e;->e:Lcom/google/android/apps/gmm/prefetchcache/api/e;

    :goto_2
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/prefetchcache/r;->a(ILcom/google/android/apps/gmm/prefetchcache/api/e;)V

    .line 583
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/r;->c()V

    .line 584
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 568
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/api/e;->d:Lcom/google/android/apps/gmm/prefetchcache/api/e;

    goto :goto_2

    .line 570
    :cond_6
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/s;->b:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/prefetch/a/d;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_3

    .line 572
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/prefetchcache/r;->d(Lcom/google/r/b/a/a/af;)V

    .line 573
    const/16 v0, 0x64

    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/api/e;->b:Lcom/google/android/apps/gmm/prefetchcache/api/e;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/r;->a(ILcom/google/android/apps/gmm/prefetchcache/api/e;)V

    goto :goto_3

    .line 577
    :pswitch_2
    const/16 v0, 0x64

    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/api/e;->c:Lcom/google/android/apps/gmm/prefetchcache/api/e;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/r;->a(ILcom/google/android/apps/gmm/prefetchcache/api/e;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 568
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 570
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized a(Lcom/google/r/b/a/a/af;)V
    .locals 4

    .prologue
    .line 225
    monitor-enter p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->a()Lcom/google/r/b/a/a/af;

    move-result-object v0

    .line 227
    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;Lcom/google/r/b/a/a/af;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/prefetchcache/t;

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-direct {v2, p1, v3}, Lcom/google/android/apps/gmm/prefetchcache/t;-><init>(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/c/a;)V

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 232
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/r;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233
    monitor-exit p0

    return-void

    .line 225
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/r/b/a/a/af;)I
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 373
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    if-eqz v3, :cond_0

    move v3, v2

    :goto_0
    if-nez v3, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v3, v1

    goto :goto_0

    .line 374
    :cond_1
    :try_start_1
    iget v3, p1, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v3}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v3

    if-nez v3, :cond_2

    sget-object v3, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_2
    sget-object v4, Lcom/google/r/b/a/a/ai;->c:Lcom/google/r/b/a/a/ai;

    if-eq v3, v4, :cond_3

    .line 375
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    if-eqz v3, :cond_5

    .line 376
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    const-string v1, "Redownloads"

    .line 377
    :goto_2
    invoke-virtual {p1}, Lcom/google/r/b/a/a/af;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    .line 379
    invoke-virtual {v3}, Lcom/google/r/b/a/a/af;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x58

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " offline area ("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), while another offline area is being executed. offlineAreaInProgress = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391
    :goto_3
    monitor-exit p0

    return v0

    :cond_3
    move v2, v1

    .line 374
    goto :goto_1

    .line 376
    :cond_4
    :try_start_2
    const-string v1, "Adds"

    goto :goto_2

    .line 383
    :cond_5
    iget v0, p1, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v0}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_6
    sget-object v3, Lcom/google/r/b/a/a/ai;->c:Lcom/google/r/b/a/a/ai;

    if-eq v0, v3, :cond_8

    .line 385
    invoke-static {}, Lcom/google/r/b/a/a/af;->newBuilder()Lcom/google/r/b/a/a/ah;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/r/b/a/a/ah;->a(Lcom/google/r/b/a/a/af;)Lcom/google/r/b/a/a/ah;

    move-result-object v0

    sget-object v3, Lcom/google/r/b/a/a/ai;->c:Lcom/google/r/b/a/a/ai;

    if-nez v3, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget v4, v0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v0, Lcom/google/r/b/a/a/ah;->a:I

    iget v3, v3, Lcom/google/r/b/a/a/ai;->f:I

    iput v3, v0, Lcom/google/r/b/a/a/ah;->d:I

    invoke-virtual {v0}, Lcom/google/r/b/a/a/ah;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    .line 389
    :goto_4
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->l:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    invoke-static {}, Lcom/google/r/b/a/a/af;->newBuilder()Lcom/google/r/b/a/a/ah;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/a/ah;->a(Lcom/google/r/b/a/a/af;)Lcom/google/r/b/a/a/ah;

    move-result-object v0

    sget-object v3, Lcom/google/r/b/a/a/ai;->b:Lcom/google/r/b/a/a/ai;

    if-nez v3, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 387
    :cond_8
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    goto :goto_4

    .line 389
    :cond_9
    iget v4, v0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v0, Lcom/google/r/b/a/a/ah;->a:I

    iget v3, v3, Lcom/google/r/b/a/a/ai;->f:I

    iput v3, v0, Lcom/google/r/b/a/a/ah;->d:I

    invoke-virtual {v0}, Lcom/google/r/b/a/a/ah;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    iget-object v4, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/c/a;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/internal/c/av;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/android/apps/gmm/map/internal/c/av;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->h:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V

    iget-object v4, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->h:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/internal/c/cv;Lcom/google/android/apps/gmm/map/internal/c/cd;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_a

    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/d;->a:Lcom/google/android/apps/gmm/prefetch/a/d;

    sget-object v3, Lcom/google/android/apps/gmm/prefetch/a/e;->b:Lcom/google/android/apps/gmm/prefetch/a/e;

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/gmm/prefetchcache/r;->a(Lcom/google/android/apps/gmm/prefetch/a/d;Lcom/google/android/apps/gmm/prefetch/a/e;)V

    .line 390
    :goto_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/r;->c()V

    .line 391
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    if-eqz v2, :cond_b

    const-string v0, "Redownloads"

    .line 392
    :goto_6
    invoke-virtual {p1}, Lcom/google/r/b/a/a/af;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xe

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " offline area "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 391
    goto/16 :goto_3

    .line 389
    :cond_a
    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/gmm/prefetchcache/api/e;->b:Lcom/google/android/apps/gmm/prefetchcache/api/e;

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/gmm/prefetchcache/r;->a(ILcom/google/android/apps/gmm/prefetchcache/api/e;)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->j:Lcom/google/android/apps/gmm/prefetch/a/f;

    invoke-static {v0}, Lcom/google/b/c/es;->b(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-interface {v3, v4, p0}, Lcom/google/android/apps/gmm/prefetch/a/f;->a(Ljava/util/Queue;Lcom/google/android/apps/gmm/prefetch/a/c;)V

    sget-object v3, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    const-string v3, "Requested prefetcher to fetch %d tiles for %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget-object v5, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    invoke-virtual {v5}, Lcom/google/r/b/a/a/af;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_5

    .line 391
    :cond_b
    const-string v0, "Adds"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_6
.end method

.method public final declared-synchronized b()V
    .locals 4

    .prologue
    .line 236
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetchcache/api/c;

    .line 238
    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->d()Z

    move-result v3

    if-nez v3, :cond_0

    .line 239
    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->h()Lcom/google/android/apps/gmm/prefetchcache/api/c;

    move-result-object v0

    .line 241
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    invoke-interface {v3, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 236
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 243
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/r;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    monitor-exit p0

    return-void

    .line 236
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final c()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 252
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 253
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    .line 254
    invoke-static {}, Lcom/google/r/b/a/a/ab;->newBuilder()Lcom/google/r/b/a/a/ad;

    move-result-object v1

    .line 255
    monitor-enter p0

    .line 256
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetchcache/api/c;

    .line 257
    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->a()Lcom/google/r/b/a/a/af;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 263
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 257
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Lcom/google/r/b/a/a/ad;->c()V

    iget-object v3, v1, Lcom/google/r/b/a/a/ad;->b:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    invoke-direct {v4}, Lcom/google/n/ao;-><init>()V

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v4, Lcom/google/n/ao;->d:Z

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 259
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    if-eqz v0, :cond_5

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    invoke-virtual {v1}, Lcom/google/r/b/a/a/ad;->c()V

    iget-object v2, v1, Lcom/google/r/b/a/a/ad;->b:Ljava/util/List;

    new-instance v3, Lcom/google/n/ao;

    invoke-direct {v3}, Lcom/google/n/ao;-><init>()V

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/google/n/ao;->d:Z

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    :cond_5
    iget-wide v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->k:J

    iget v0, v1, Lcom/google/r/b/a/a/ad;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v1, Lcom/google/r/b/a/a/ad;->a:I

    iput-wide v2, v1, Lcom/google/r/b/a/a/ad;->c:J

    .line 263
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->ay:Lcom/google/android/apps/gmm/shared/b/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    .line 265
    invoke-virtual {v1}, Lcom/google/r/b/a/a/ad;->g()Lcom/google/n/t;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Lcom/google/n/at;)V

    .line 266
    :cond_6
    return-void
.end method

.method public final declared-synchronized c(Lcom/google/r/b/a/a/af;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 404
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    if-eqz v2, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v2, v0

    goto :goto_0

    .line 405
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    if-eqz v2, :cond_2

    .line 406
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/r/b/a/a/af;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    .line 408
    invoke-virtual {v2}, Lcom/google/r/b/a/a/af;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x5a

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "removeOfflineArea "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", while another offline area is being executed. offlineAreaInProgress = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423
    :goto_1
    monitor-exit p0

    return v0

    .line 412
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/prefetchcache/r;->a(Ljava/util/List;Lcom/google/r/b/a/a/af;)V

    .line 413
    iget v0, p1, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v0}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_3
    sget-object v2, Lcom/google/r/b/a/a/ai;->d:Lcom/google/r/b/a/a/ai;

    if-eq v0, v2, :cond_5

    .line 415
    invoke-static {}, Lcom/google/r/b/a/a/af;->newBuilder()Lcom/google/r/b/a/a/ah;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/r/b/a/a/ah;->a(Lcom/google/r/b/a/a/af;)Lcom/google/r/b/a/a/ah;

    move-result-object v0

    sget-object v2, Lcom/google/r/b/a/a/ai;->d:Lcom/google/r/b/a/a/ai;

    if-nez v2, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget v3, v0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v0, Lcom/google/r/b/a/a/ah;->a:I

    iget v2, v2, Lcom/google/r/b/a/a/ai;->f:I

    iput v2, v0, Lcom/google/r/b/a/a/ah;->d:I

    invoke-virtual {v0}, Lcom/google/r/b/a/a/ah;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    .line 419
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/r;->e()V

    .line 421
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/r;->c()V

    .line 422
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    const-string v0, "removeOfflineArea "

    invoke-virtual {p1}, Lcom/google/r/b/a/a/af;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_3
    move v0, v1

    .line 423
    goto :goto_1

    .line 417
    :cond_5
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    goto :goto_2

    .line 422
    :cond_6
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method public final declared-synchronized d()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 434
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    if-eqz v2, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v2, v0

    goto :goto_0

    .line 436
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->i:Lcom/google/android/apps/gmm/prefetch/a/b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_2

    .line 454
    :goto_1
    monitor-exit p0

    return v0

    .line 441
    :cond_2
    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    if-eqz v2, :cond_8

    .line 442
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    iget v2, v2, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v2}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_3
    sget-object v3, Lcom/google/r/b/a/a/ai;->c:Lcom/google/r/b/a/a/ai;

    if-eq v2, v3, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    .line 443
    iget v2, v2, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v2}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_4
    sget-object v3, Lcom/google/r/b/a/a/ai;->b:Lcom/google/r/b/a/a/ai;

    if-ne v2, v3, :cond_6

    .line 444
    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    .line 445
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->i:Lcom/google/android/apps/gmm/prefetch/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetch/a/b;->d()V

    move v0, v1

    .line 446
    goto :goto_1

    .line 448
    :cond_6
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    .line 449
    iget v1, v1, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v1}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v1

    if-nez v1, :cond_7

    sget-object v1, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_7
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x46

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "offlineAreaInProgress is not in status of TO_BE_ADDED or IN_PROGRESS: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 453
    :cond_8
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method e()V
    .locals 15

    .prologue
    .line 480
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->l:I

    .line 481
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    .line 482
    invoke-static {}, Lcom/google/r/b/a/a/af;->newBuilder()Lcom/google/r/b/a/a/ah;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/a/ah;->a(Lcom/google/r/b/a/a/af;)Lcom/google/r/b/a/a/ah;

    move-result-object v0

    sget-object v1, Lcom/google/r/b/a/a/ai;->b:Lcom/google/r/b/a/a/ai;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/r/b/a/a/ah;->a:I

    iget v1, v1, Lcom/google/r/b/a/a/ai;->f:I

    iput v1, v0, Lcom/google/r/b/a/a/ah;->d:I

    invoke-virtual {v0}, Lcom/google/r/b/a/a/ah;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    .line 484
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 485
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 486
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/c/a;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/c/av;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/android/apps/gmm/map/internal/c/av;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->h:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->h:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/internal/c/cv;Lcom/google/android/apps/gmm/map/internal/c/cd;)Ljava/util/Set;

    move-result-object v5

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->a()Lcom/google/r/b/a/a/af;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->a()Lcom/google/r/b/a/a/af;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-static {v0, v7}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/c/a;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/gmm/map/internal/c/av;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/android/apps/gmm/map/internal/c/av;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->h:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-virtual {v8, v7}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V

    iget-object v7, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->h:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-static {v2, v0, v7}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/internal/c/cv;Lcom/google/android/apps/gmm/map/internal/c/cd;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    iget-object v8, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->c:[Landroid/accounts/Account;

    array-length v9, v8

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v9, :cond_a

    aget-object v1, v8, v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    invoke-virtual {v1, v0}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x38

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "Loading tiles from completed offline areas for account: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v11

    sget-object v12, Lcom/google/android/apps/gmm/shared/b/c;->ay:Lcom/google/android/apps/gmm/shared/b/c;

    sget-object v13, Lcom/google/r/b/a/a/ab;->PARSER:Lcom/google/n/ax;

    invoke-static {}, Lcom/google/r/b/a/a/ab;->g()Lcom/google/r/b/a/a/ab;

    move-result-object v0

    invoke-virtual {v12}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-static {v12, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    const/4 v12, 0x0

    invoke-virtual {v11, v1, v12}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;[B)[B

    move-result-object v1

    invoke-static {v1, v13}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/ax;)Lcom/google/n/at;

    move-result-object v1

    if-nez v1, :cond_7

    :cond_2
    :goto_2
    check-cast v0, Lcom/google/r/b/a/a/ab;

    invoke-virtual {v0}, Lcom/google/r/b/a/a/ab;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_3
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    iget v1, v0, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v1}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_4
    sget-object v12, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    if-eq v1, v12, :cond_6

    iget v1, v0, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v1}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_5
    sget-object v12, Lcom/google/r/b/a/a/ai;->e:Lcom/google/r/b/a/a/ai;

    if-ne v1, v12, :cond_3

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/c/a;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v1

    iget-object v12, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v12}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v12

    invoke-static {v12}, Lcom/google/android/apps/gmm/map/internal/c/av;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/android/apps/gmm/map/internal/c/av;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->h:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-virtual {v13, v12}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V

    iget-object v12, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->h:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-static {v0, v1, v12}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/internal/c/cv;Lcom/google/android/apps/gmm/map/internal/c/cd;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_7
    move-object v0, v1

    goto :goto_2

    :cond_8
    invoke-interface {v7, v10}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    :cond_9
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    :cond_a
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_b
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    invoke-interface {v7, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_c
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 489
    :cond_d
    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 491
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/d;->b:Lcom/google/android/apps/gmm/prefetch/a/d;

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/e;->b:Lcom/google/android/apps/gmm/prefetch/a/e;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/r;->a(Lcom/google/android/apps/gmm/prefetch/a/d;Lcom/google/android/apps/gmm/prefetch/a/e;)V

    .line 500
    :goto_5
    return-void

    .line 493
    :cond_e
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/api/e;->c:Lcom/google/android/apps/gmm/prefetchcache/api/e;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/r;->a(ILcom/google/android/apps/gmm/prefetchcache/api/e;)V

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->j:Lcom/google/android/apps/gmm/prefetch/a/f;

    invoke-interface {v0, v3, v4, p0}, Lcom/google/android/apps/gmm/prefetch/a/f;->a(Ljava/util/Queue;Ljava/util/Queue;Lcom/google/android/apps/gmm/prefetch/a/c;)V

    .line 496
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    const-string v0, "Requested prefetcher to remove %d tiles for %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 497
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    .line 498
    invoke-virtual {v3}, Lcom/google/r/b/a/a/af;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 496
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_5
.end method

.method f()Lcom/google/r/b/a/a/ab;
    .locals 6

    .prologue
    .line 701
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->ay:Lcom/google/android/apps/gmm/shared/b/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    sget-object v4, Lcom/google/r/b/a/a/ab;->PARSER:Lcom/google/n/ax;

    .line 703
    invoke-static {}, Lcom/google/r/b/a/a/ab;->g()Lcom/google/r/b/a/a/ab;

    move-result-object v0

    .line 701
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;[B)[B

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/ax;)Lcom/google/n/at;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    check-cast v0, Lcom/google/r/b/a/a/ab;

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method g()Z
    .locals 4

    .prologue
    .line 710
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/as;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->k:J

    .line 711
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/r;->f()Lcom/google/r/b/a/a/ab;

    move-result-object v0

    .line 712
    iget-wide v0, v0, Lcom/google/r/b/a/a/ab;->e:J

    iget-wide v2, p0, Lcom/google/android/apps/gmm/prefetchcache/r;->k:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 713
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    .line 715
    const/4 v0, 0x1

    .line 717
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
