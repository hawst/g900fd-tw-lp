.class Lcom/google/android/apps/gmm/place/cc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/g/g;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/place/g/a/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/g/a/d;)V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/cc;->a:Lcom/google/android/apps/gmm/place/g/a/d;

    .line 128
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Float;)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cc;->a:Lcom/google/android/apps/gmm/place/g/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/d;->e()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cc;->a:Lcom/google/android/apps/gmm/place/g/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/d;->e()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 145
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cc;->a:Lcom/google/android/apps/gmm/place/g/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/d;->f()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cc;->a:Lcom/google/android/apps/gmm/place/g/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/d;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
