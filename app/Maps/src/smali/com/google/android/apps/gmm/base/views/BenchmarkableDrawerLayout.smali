.class public Lcom/google/android/apps/gmm/base/views/BenchmarkableDrawerLayout;
.super Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;
.source "PG"


# instance fields
.field private l:Z

.field private m:Z

.field private final n:Lcom/google/android/apps/gmm/map/e/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;-><init>(Landroid/content/Context;)V

    .line 28
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/BenchmarkableDrawerLayout;->l:Z

    .line 29
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/BenchmarkableDrawerLayout;->m:Z

    .line 37
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    .line 38
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->q()Lcom/google/android/apps/gmm/map/e/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/BenchmarkableDrawerLayout;->n:Lcom/google/android/apps/gmm/map/e/a;

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/base/views/BenchmarkableDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/BenchmarkableDrawerLayout;->l:Z

    .line 29
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/BenchmarkableDrawerLayout;->m:Z

    .line 47
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    .line 48
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->q()Lcom/google/android/apps/gmm/map/e/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/BenchmarkableDrawerLayout;->n:Lcom/google/android/apps/gmm/map/e/a;

    .line 49
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/BenchmarkableDrawerLayout;->m:Z

    if-nez v0, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/BenchmarkableDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/j/e;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/j/e;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 113
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/BenchmarkableDrawerLayout;->m:Z

    .line 116
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 124
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 75
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->onLayout(ZIIII)V

    .line 82
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .prologue
    .line 86
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->onMeasure(II)V

    .line 94
    return-void
.end method
