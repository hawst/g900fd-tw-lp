.class public Lcom/google/android/apps/gmm/navigation/navui/aa;
.super Lcom/google/android/apps/gmm/navigation/navui/f;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# static fields
.field private static final j:Ljava/lang/String;


# instance fields
.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Z

.field final i:Lcom/google/android/apps/gmm/base/activities/c;

.field private k:Z

.field private l:J

.field private m:Lcom/google/android/apps/gmm/navigation/util/a;

.field private n:Lcom/google/android/apps/gmm/navigation/util/a;

.field private o:Z

.field private p:I

.field private final q:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/google/android/apps/gmm/navigation/navui/aa;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/aa;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 107
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/navigation/navui/f;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;)V

    .line 80
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->h:Z

    .line 85
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/ab;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/ab;-><init>(Lcom/google/android/apps/gmm/navigation/navui/aa;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->q:Landroid/content/BroadcastReceiver;

    .line 108
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->i:Lcom/google/android/apps/gmm/base/activities/c;

    .line 110
    new-instance v0, Lcom/google/android/apps/gmm/navigation/util/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/util/a;-><init>(Lcom/google/android/apps/gmm/base/a;Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->m:Lcom/google/android/apps/gmm/navigation/util/a;

    .line 111
    new-instance v0, Lcom/google/android/apps/gmm/navigation/util/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/util/a;-><init>(Lcom/google/android/apps/gmm/base/a;Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->n:Lcom/google/android/apps/gmm/navigation/util/a;

    .line 112
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/navigation/navui/aa;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/navigation/navui/aa;->a(Z)V

    return-void
.end method

.method private a(Lcom/google/b/f/b/a/t;)V
    .locals 3

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->l:J

    .line 327
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->d:Z

    if-eqz v0, :cond_2

    .line 328
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->d:Z

    .line 329
    sget-object v0, Lcom/google/b/f/b/a/t;->b:Lcom/google/b/f/b/a/t;

    if-ne v0, p1, :cond_0

    .line 330
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->p:I

    .line 333
    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->i:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->i:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 337
    if-eqz p1, :cond_1

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/b/f/b/a/t;)V

    .line 339
    sget-object v0, Lcom/google/android/apps/gmm/navigation/navui/aa;->j:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Power savings ended due to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->k:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->m:Lcom/google/android/apps/gmm/navigation/util/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/util/a;->a()V

    .line 343
    :cond_2
    :goto_0
    return-void

    .line 341
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->n:Lcom/google/android/apps/gmm/navigation/util/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/util/a;->a()V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->k:Z

    if-ne p1, v0, :cond_0

    .line 238
    :goto_0
    return-void

    .line 223
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->d:Z

    if-nez v0, :cond_3

    .line 226
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->m:Lcom/google/android/apps/gmm/navigation/util/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/util/a;->b()V

    .line 227
    :goto_1
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->k:Z

    .line 228
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->k:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->m:Lcom/google/android/apps/gmm/navigation/util/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/util/a;->a()V

    .line 233
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->k:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->e:Z

    if-nez v0, :cond_4

    .line 234
    sget-object v0, Lcom/google/b/f/b/a/t;->c:Lcom/google/b/f/b/a/t;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/aa;->a(Lcom/google/b/f/b/a/t;)V

    goto :goto_0

    .line 226
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->n:Lcom/google/android/apps/gmm/navigation/util/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/util/a;->b()V

    goto :goto_1

    .line 228
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->n:Lcom/google/android/apps/gmm/navigation/util/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/util/a;->a()V

    goto :goto_2

    .line 230
    :cond_3
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->k:Z

    goto :goto_2

    .line 236
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->l:J

    goto :goto_0
.end method

.method private c()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 250
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->d:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->p:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    :cond_0
    move v0, v2

    .line 261
    :goto_0
    return v0

    .line 255
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->i:Lcom/google/android/apps/gmm/base/activities/c;

    .line 256
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_off_timeout"

    .line 255
    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    .line 260
    :goto_1
    iget-wide v4, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->l:J

    add-long/2addr v0, v4

    .line 261
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    .line 258
    :catch_0
    move-exception v0

    const-wide/32 v0, 0xea60

    goto :goto_1

    :cond_2
    move v0, v2

    .line 261
    goto :goto_0
.end method


# virtual methods
.method public final G_()V
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->i:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->q:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->f:Z

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 174
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->d:Z

    if-nez v0, :cond_2

    .line 175
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->m:Lcom/google/android/apps/gmm/navigation/util/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/util/a;->b()V

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->n:Lcom/google/android/apps/gmm/navigation/util/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/util/a;->b()V

    goto :goto_0

    .line 179
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->i:Lcom/google/android/apps/gmm/base/activities/c;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 180
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    sget-object v0, Lcom/google/b/f/b/a/t;->a:Lcom/google/b/f/b/a/t;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/aa;->a(Lcom/google/b/f/b/a/t;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 135
    if-eqz p1, :cond_0

    .line 136
    const-string v0, "PSC_ispk"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->d:Z

    .line 137
    const-string v0, "PSC_hrfpbk"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->g:Z

    .line 139
    const-string v0, "PCS_tck"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->p:I

    .line 141
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/base/e/f;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 373
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->f:Z

    if-eqz v0, :cond_0

    .line 377
    :goto_0
    return-void

    .line 376
    :cond_0
    sget-object v0, Lcom/google/b/f/b/a/t;->b:Lcom/google/b/f/b/a/t;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/aa;->a(Lcom/google/b/f/b/a/t;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/base/e/g;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 355
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->o:Z

    if-eqz v0, :cond_0

    .line 356
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->o:Z

    .line 369
    :goto_0
    return-void

    .line 359
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->f:Z

    if-eqz v0, :cond_1

    .line 360
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->f:Z

    goto :goto_0

    .line 364
    :cond_1
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/base/e/g;->a:Z

    if-nez v0, :cond_2

    .line 365
    sget-object v0, Lcom/google/b/f/b/a/t;->b:Lcom/google/b/f/b/a/t;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/aa;->a(Lcom/google/b/f/b/a/t;)V

    goto :goto_0

    .line 367
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->l:J

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/f;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 386
    sget-object v0, Lcom/google/b/f/b/a/t;->f:Lcom/google/b/f/b/a/t;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/aa;->a(Lcom/google/b/f/b/a/t;)V

    .line 387
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/g;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 381
    sget-object v0, Lcom/google/b/f/b/a/t;->e:Lcom/google/b/f/b/a/t;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/aa;->a(Lcom/google/b/f/b/a/t;)V

    .line 382
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/navui/PretendToBePluggedInEventForTesting;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 391
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->h:Z

    .line 392
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/navigation/navui/PretendToBePluggedInEventForTesting;->isPluggedIn()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/aa;->a(Z)V

    .line 393
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V
    .locals 6
    .param p2    # Lcom/google/android/apps/gmm/navigation/navui/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 189
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->d:Z

    if-eqz v2, :cond_2

    .line 190
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/g/b/f;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 191
    :cond_0
    sget-object v0, Lcom/google/b/f/b/a/t;->d:Lcom/google/b/f/b/a/t;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/aa;->a(Lcom/google/b/f/b/a/t;)V

    .line 201
    :cond_1
    :goto_0
    return-void

    .line 194
    :cond_2
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/navigation/navui/b/a;->a(Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->e:Z

    .line 195
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->k:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->e:Z

    .line 197
    :goto_1
    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/navui/aa;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 198
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->d:Z

    if-nez v2, :cond_1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->d:Z

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->k:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->m:Lcom/google/android/apps/gmm/navigation/util/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/navigation/util/a;->b()V

    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->i:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/z/a/b;->b(Landroid/app/Activity;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->i:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v2}, Lcom/google/android/apps/gmm/z/b/b;->a(Landroid/app/Activity;)Lcom/google/b/f/b/a/cy;

    move-result-object v2

    iget v3, v2, Lcom/google/b/f/b/a/cy;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    :goto_3
    if-eqz v0, :cond_6

    iget v0, v2, Lcom/google/b/f/b/a/cy;->c:F

    :goto_4
    const v1, 0x3e4ccccd    # 0.2f

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->i:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->i:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/ac;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/navigation/navui/ac;-><init>(Lcom/google/android/apps/gmm/navigation/navui/aa;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    const-wide/16 v4, 0x7530

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    goto :goto_0

    .line 196
    :cond_3
    invoke-virtual {p1, v1}, Lcom/google/android/apps/gmm/navigation/navui/b/a;->a(Z)Z

    move-result v2

    goto :goto_1

    .line 198
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->n:Lcom/google/android/apps/gmm/navigation/util/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/navigation/util/a;->b()V

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_4
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->i:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->q:Landroid/content/BroadcastReceiver;

    sget-object v2, Lcom/google/android/apps/gmm/map/h/a;->a:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->l:J

    .line 155
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->d:Z

    if-nez v0, :cond_1

    .line 156
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->m:Lcom/google/android/apps/gmm/navigation/util/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/util/a;->a()V

    .line 164
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 165
    return-void

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->n:Lcom/google/android/apps/gmm/navigation/util/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/util/a;->a()V

    goto :goto_0

    .line 162
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/aa;->a(Lcom/google/b/f/b/a/t;)V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 145
    const-string v0, "PSC_ispk"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 146
    const-string v0, "PSC_hrfpbk"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 147
    const-string v0, "PCS_tck"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/aa;->p:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 148
    return-void
.end method
