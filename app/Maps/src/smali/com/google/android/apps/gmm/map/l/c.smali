.class public Lcom/google/android/apps/gmm/map/l/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/lang/String;

.field private static final s:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lcom/google/android/apps/gmm/map/l/c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final c:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final d:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final e:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final f:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final g:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final h:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final i:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final j:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final k:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final l:Lcom/google/android/apps/gmm/map/b/a/ay;

.field public final m:Lcom/google/android/apps/gmm/map/b/a/ay;

.field public final n:Lcom/google/android/apps/gmm/map/b/a/ay;

.field public final o:Lcom/google/android/apps/gmm/map/b/a/ay;

.field public final p:Lcom/google/android/apps/gmm/map/b/a/ay;

.field public final q:Lcom/google/android/apps/gmm/map/b/a/ay;

.field public final r:Lcom/google/android/apps/gmm/map/b/a/ay;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/gmm/map/l/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/l/c;->a:Ljava/lang/String;

    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/map/l/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/l/d;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/l/c;->s:Ljava/lang/ThreadLocal;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 67
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 68
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 69
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 70
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 74
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->l:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 76
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->m:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 77
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->n:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 78
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    .line 79
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->o:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 80
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 81
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->q:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 82
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/c;->r:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 83
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ab;)I
    .locals 1

    .prologue
    .line 651
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    .line 652
    mul-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x6

    return v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/l/i;I)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 629
    sget-object v1, Lcom/google/android/apps/gmm/map/l/e;->a:[I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/l/i;->a:Lcom/google/android/apps/gmm/map/l/j;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/l/j;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 647
    :cond_0
    :goto_0
    return v0

    .line 631
    :pswitch_0
    iget v1, p0, Lcom/google/android/apps/gmm/map/l/i;->d:I

    .line 632
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/l/i;->e:[I

    .line 633
    if-eqz v1, :cond_0

    if-lez p1, :cond_0

    array-length v3, v2

    if-le v3, p1, :cond_0

    .line 636
    aget v0, v2, p1

    shl-int/lit8 v0, v0, 0x10

    const v2, 0x8000

    add-int/2addr v0, v2

    const/4 v2, 0x1

    shl-int v1, v2, v1

    div-int/2addr v0, v1

    goto :goto_0

    .line 639
    :pswitch_1
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    .line 641
    const/4 v0, -0x2

    if-ne p1, v0, :cond_1

    .line 642
    iget v0, p0, Lcom/google/android/apps/gmm/map/l/i;->h:I

    goto :goto_0

    .line 644
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/l/i;->g:I

    goto :goto_0

    .line 629
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a()Lcom/google/android/apps/gmm/map/l/c;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/google/android/apps/gmm/map/l/c;->s:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/l/c;

    return-object v0
.end method
