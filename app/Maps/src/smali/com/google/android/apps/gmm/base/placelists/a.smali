.class public Lcom/google/android/apps/gmm/base/placelists/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/mylocation/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a;->a:Ljava/util/HashMap;

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/placelists/a;->b:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/mylocation/b/a;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)",
            "Lcom/google/android/apps/gmm/mylocation/b/a;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a;->a:Ljava/util/HashMap;

    .line 66
    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/x/o;

    if-ne v1, p1, :cond_3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/b/a;

    .line 68
    :goto_0
    if-nez v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a;->b:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v0

    .line 70
    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/i;->i()Lcom/google/android/apps/gmm/mylocation/b/a;

    move-result-object v1

    .line 71
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->a(Lcom/google/android/apps/gmm/base/g/b;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 74
    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->c()Z

    move-result v1

    if-nez v1, :cond_2

    .line 75
    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->a()V

    .line 77
    :cond_2
    return-object v0

    .line 66
    :cond_3
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/g/c;->b(Lcom/google/android/apps/gmm/base/g/c;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/b/a;

    goto :goto_0

    :cond_4
    move-object v0, v3

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/b/a;

    .line 119
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->c()Z

    move-result v2

    if-nez v2, :cond_0

    .line 120
    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->a()V

    goto :goto_0

    .line 123
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/google/android/apps/gmm/x/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p0, p2}, Lcom/google/android/apps/gmm/base/placelists/a;->a(Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/mylocation/b/a;

    move-result-object v1

    .line 48
    instance-of v0, p1, Lcom/google/android/apps/gmm/place/PlacePageView;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 49
    check-cast v0, Lcom/google/android/apps/gmm/place/PlacePageView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageView;->e()Lcom/google/android/apps/gmm/place/af;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 50
    check-cast p1, Lcom/google/android/apps/gmm/place/PlacePageView;

    .line 51
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/place/PlacePageView;->e()Lcom/google/android/apps/gmm/place/af;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/af;->a:Lcom/google/android/apps/gmm/place/j/a;

    .line 50
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->a(Lcom/google/android/apps/gmm/place/b/a;)V

    .line 54
    :cond_0
    return-void
.end method
