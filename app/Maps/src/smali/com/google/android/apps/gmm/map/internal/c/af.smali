.class public Lcom/google/android/apps/gmm/map/internal/c/af;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/m;


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/b/a/ab;

.field public b:Lcom/google/android/apps/gmm/map/internal/c/be;

.field public c:I

.field private d:J

.field private final e:I

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:[I


# direct methods
.method private constructor <init>(JIILcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;II[I)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->d:J

    .line 58
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->e:I

    .line 59
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->f:I

    .line 60
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 61
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->b:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 62
    iput-object p8, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->g:Ljava/lang/String;

    .line 64
    iput p9, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->c:I

    .line 65
    iput-object p11, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->h:[I

    .line 67
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;)Lcom/google/android/apps/gmm/map/internal/c/af;
    .locals 13

    .prologue
    .line 83
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v6

    .line 86
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v1

    new-instance v9, Lcom/google/android/apps/gmm/map/internal/c/bk;

    invoke-direct {v9, v1, v0}, Lcom/google/android/apps/gmm/map/internal/c/bk;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bi;I)V

    .line 89
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v10

    .line 92
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v11

    .line 95
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v1

    .line 96
    new-array v12, v1, [I

    .line 97
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 98
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v2

    aput v2, v12, v0

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/af;

    .line 102
    iget v0, p2, Lcom/google/android/apps/gmm/map/internal/c/ay;->a:I

    int-to-long v2, v0

    .line 103
    iget v4, p2, Lcom/google/android/apps/gmm/map/internal/c/ay;->c:I

    .line 104
    iget v5, p2, Lcom/google/android/apps/gmm/map/internal/c/ay;->d:I

    .line 106
    iget-object v0, v9, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v0, :cond_1

    const/4 v7, 0x0

    .line 107
    :goto_1
    iget v8, v9, Lcom/google/android/apps/gmm/map/internal/c/bk;->b:I

    .line 108
    iget-object v0, v9, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v0, :cond_3

    const/4 v9, 0x0

    :goto_2
    invoke-direct/range {v1 .. v12}, Lcom/google/android/apps/gmm/map/internal/c/af;-><init>(JIILcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;II[I)V

    return-object v1

    .line 106
    :cond_1
    iget-object v0, v9, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v7, v7

    if-nez v7, :cond_2

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v7

    goto :goto_1

    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v7, 0x0

    aget-object v7, v0, v7

    goto :goto_1

    .line 108
    :cond_3
    iget-object v0, v9, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 154
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->d:J

    return-wide v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->b:Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v1, 0x2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    aget v0, v0, v1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->e:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->f:I

    return v0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->b:Lcom/google/android/apps/gmm/map/internal/c/be;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x5

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->c:I

    return v0
.end method

.method public final j()[I
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->h:[I

    return-object v0
.end method

.method public final k()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x2

    add-int/lit16 v0, v0, 0xa0

    add-int/lit8 v2, v0, 0x2c

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->g:Ljava/lang/String;

    .line 181
    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/af;->b:Lcom/google/android/apps/gmm/map/internal/c/be;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x4

    shl-int/lit8 v0, v0, 0x2

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x28

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/be;->f()I

    move-result v1

    goto :goto_1
.end method
