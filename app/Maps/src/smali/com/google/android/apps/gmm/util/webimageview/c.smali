.class final enum Lcom/google/android/apps/gmm/util/webimageview/c;
.super Lcom/google/android/apps/gmm/util/webimageview/b;
.source "PG"


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/util/webimageview/b;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;IILandroid/widget/ImageView$ScaleType;Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 165
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    if-ne p4, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->b:Lcom/google/android/apps/gmm/util/webimageview/i;

    :goto_0
    invoke-static {p2, p3, v0, p5, p1}, Lcom/google/android/apps/gmm/util/webimageview/h;->a(IILcom/google/android/apps/gmm/util/webimageview/i;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    if-ne p4, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->b:Lcom/google/android/apps/gmm/util/webimageview/i;

    goto :goto_0

    :cond_1
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    if-ne p4, v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->a:Lcom/google/android/apps/gmm/util/webimageview/i;

    goto :goto_0

    :cond_2
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    if-ne p4, v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->a:Lcom/google/android/apps/gmm/util/webimageview/i;

    goto :goto_0

    :cond_3
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_END:Landroid/widget/ImageView$ScaleType;

    if-ne p4, v0, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->a:Lcom/google/android/apps/gmm/util/webimageview/i;

    goto :goto_0

    :cond_4
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    if-ne p4, v0, :cond_5

    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->a:Lcom/google/android/apps/gmm/util/webimageview/i;

    goto :goto_0

    :cond_5
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    if-ne p4, v0, :cond_6

    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->e:Lcom/google/android/apps/gmm/util/webimageview/i;

    goto :goto_0

    :cond_6
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    if-ne p4, v0, :cond_8

    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->a:Ljava/lang/String;

    const-string v0, "Tried to set a WebImageView to MATRIX mode, which doesn\'t really make sense. Fetching full-resolution image from "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    :cond_7
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->c:Lcom/google/android/apps/gmm/util/webimageview/i;

    goto :goto_0
.end method
