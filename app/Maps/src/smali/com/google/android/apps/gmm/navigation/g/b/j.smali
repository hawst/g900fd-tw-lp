.class public Lcom/google/android/apps/gmm/navigation/g/b/j;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/r/a/ae;

.field public b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->c:I

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/g/b/i;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->c:I

    .line 78
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 79
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 80
    iget v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/i;->c:I

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->c:I

    .line 81
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/navigation/g/b/i;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    const-string v3, "routes"

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    const-string v3, "routeStates"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    array-length v3, v3

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "routes size == route states size"

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 88
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_4

    move v0, v1

    :goto_1
    const-string v3, "routes.hasSelected()"

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v2

    goto :goto_1

    .line 89
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 90
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->a()Lcom/google/android/apps/gmm/map/r/a/w;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v4, v4, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_2
    const-string v3, "selected route == guided route"

    .line 89
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move v0, v2

    .line 90
    goto :goto_2

    .line 92
    :cond_7
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->c:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/b/j;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    array-length v3, v3

    if-ge v0, v3, :cond_8

    :goto_3
    const-string v0, "betterRouteIndex in bounds"

    if-nez v1, :cond_9

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_8
    move v1, v2

    goto :goto_3

    .line 94
    :cond_9
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/b/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/g/b/i;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/j;)V

    return-object v0
.end method
