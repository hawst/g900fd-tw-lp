.class public final enum Lcom/google/android/apps/gmm/directions/dk;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/directions/dk;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/directions/dk;

.field public static final enum b:Lcom/google/android/apps/gmm/directions/dk;

.field private static final synthetic c:[Lcom/google/android/apps/gmm/directions/dk;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/directions/dk;

    const-string v1, "COLLAPSED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/dk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/dk;->a:Lcom/google/android/apps/gmm/directions/dk;

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/directions/dk;

    const-string v1, "EXPANDED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/directions/dk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/dk;->b:Lcom/google/android/apps/gmm/directions/dk;

    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/gmm/directions/dk;

    sget-object v1, Lcom/google/android/apps/gmm/directions/dk;->a:Lcom/google/android/apps/gmm/directions/dk;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/directions/dk;->b:Lcom/google/android/apps/gmm/directions/dk;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/gmm/directions/dk;->c:[Lcom/google/android/apps/gmm/directions/dk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/directions/dk;
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/android/apps/gmm/directions/dk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/dk;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/directions/dk;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/apps/gmm/directions/dk;->c:[Lcom/google/android/apps/gmm/directions/dk;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/directions/dk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/directions/dk;

    return-object v0
.end method
