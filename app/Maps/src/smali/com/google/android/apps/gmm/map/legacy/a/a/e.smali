.class public Lcom/google/android/apps/gmm/map/legacy/a/a/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/map/legacy/a/a/e;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/apps/gmm/map/util/a;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 1

    .prologue
    .line 540
    sget-object v0, Lcom/google/android/apps/gmm/map/util/a;->a:Lcom/google/android/apps/gmm/map/util/a;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/e;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/util/a;)V

    .line 541
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/util/a;)V
    .locals 5

    .prologue
    .line 549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 552
    const/4 v0, 0x1

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ai;->B:I

    shl-int/2addr v0, v1

    .line 553
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/ai;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 554
    sget-object v2, Lcom/google/android/apps/gmm/map/util/a;->a:Lcom/google/android/apps/gmm/map/util/a;

    if-eq p2, v2, :cond_0

    .line 555
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xb

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, " with mask "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 557
    :cond_0
    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->a:I

    .line 558
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->c:Ljava/lang/String;

    .line 559
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->b:Lcom/google/android/apps/gmm/map/util/a;

    .line 560
    return-void
.end method


# virtual methods
.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 520
    check-cast p1, Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->a:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->a:I

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->b:Lcom/google/android/apps/gmm/map/util/a;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->b:Lcom/google/android/apps/gmm/map/util/a;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    iget-wide v0, v1, Lcom/google/android/apps/gmm/map/util/a;->c:J

    invoke-static {v2, v3, v0, v1}, Lcom/google/b/h/d;->a(JJ)I

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 572
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->a:I

    if-ne v0, v1, :cond_0

    check-cast p1, Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->b:Lcom/google/android/apps/gmm/map/util/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->b:Lcom/google/android/apps/gmm/map/util/a;

    .line 573
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 578
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->a:I

    mul-int/lit8 v0, v0, 0x21

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->b:Lcom/google/android/apps/gmm/map/util/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/a;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;->c:Ljava/lang/String;

    return-object v0
.end method
