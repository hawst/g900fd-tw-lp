.class Lcom/google/android/apps/gmm/base/layout/o;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/layout/MainLayout;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;)V
    .locals 0

    .prologue
    .line 342
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/layout/o;->a:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/base/views/aj;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/o;->a:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->T:Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    .line 351
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/aj;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    .line 352
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/o;->a:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    if-eq v0, v2, :cond_0

    .line 354
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 358
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/layout/o;->a:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 362
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/layout/o;->a:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/layout/MainLayout;->T:Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    .line 366
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/o;->a:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i()V

    .line 369
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/indoor/b/c;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/layout/o;->a:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b()V

    .line 376
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/prefetchcache/api/g;)V
    .locals 7
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 381
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/layout/o;->a:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iget-object v0, p1, Lcom/google/android/apps/gmm/prefetchcache/api/g;->a:Lcom/google/android/apps/gmm/prefetchcache/api/h;

    sget-object v1, Lcom/google/android/apps/gmm/base/layout/n;->a:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/h;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 382
    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move v3, v4

    .line 381
    :goto_1
    if-eqz v3, :cond_1

    move v1, v2

    :goto_2
    iget-object v0, v5, Lcom/google/android/apps/gmm/base/layout/MainLayout;->p:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/prefetchcache/api/NextButton;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/NextButton;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_0

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, v5, Lcom/google/android/apps/gmm/base/layout/MainLayout;->Q:Landroid/animation/AnimatorSet;

    iget-object v0, v5, Lcom/google/android/apps/gmm/base/layout/MainLayout;->p:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/prefetchcache/api/NextButton;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/NextButton;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    if-eqz v3, :cond_2

    iget-object v1, v5, Lcom/google/android/apps/gmm/base/layout/MainLayout;->p:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    const-string v6, "translationY"

    new-array v4, v4, [F

    aput v0, v4, v2

    invoke-static {v1, v6, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    :goto_3
    iget-object v1, v5, Lcom/google/android/apps/gmm/base/layout/MainLayout;->Q:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v0, v5, Lcom/google/android/apps/gmm/base/layout/MainLayout;->Q:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/google/android/apps/gmm/base/layout/p;

    invoke-direct {v1, v5, v3}, Lcom/google/android/apps/gmm/base/layout/p;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, v5, Lcom/google/android/apps/gmm/base/layout/MainLayout;->Q:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    :pswitch_1
    move v3, v2

    goto :goto_1

    :cond_1
    const/4 v0, 0x4

    move v1, v0

    goto :goto_2

    :cond_2
    iget-object v0, v5, Lcom/google/android/apps/gmm/base/layout/MainLayout;->p:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    const-string v1, "translationY"

    new-array v4, v4, [F

    const/4 v6, 0x0

    aput v6, v4, v2

    invoke-static {v0, v1, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
