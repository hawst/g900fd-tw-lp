.class public Lcom/google/android/apps/gmm/directions/bs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field a:Lcom/google/maps/g/a/hc;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field b:J


# direct methods
.method private constructor <init>(Lcom/google/maps/g/a/hc;J)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/bs;->a:Lcom/google/maps/g/a/hc;

    .line 40
    iput-wide p2, p0, Lcom/google/android/apps/gmm/directions/bs;->b:J

    .line 41
    return-void
.end method

.method public static a(Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/directions/bs;
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 94
    iget-object v0, p0, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agt;->d()Lcom/google/r/b/a/agt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agt;

    .line 96
    iget v1, v0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_3

    move v1, v4

    :goto_0
    if-eqz v1, :cond_4

    .line 97
    iget v1, v0, Lcom/google/r/b/a/agt;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/hc;->a(I)Lcom/google/maps/g/a/hc;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/maps/g/a/hc;->a:Lcom/google/maps/g/a/hc;

    .line 102
    :cond_0
    :goto_1
    iget v2, v0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_5

    move v2, v4

    :goto_2
    if-eqz v2, :cond_7

    .line 103
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v6, v0, Lcom/google/r/b/a/agt;->d:J

    invoke-virtual {v2, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 104
    iget v6, v0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v6, v6, 0x2

    const/4 v7, 0x2

    if-ne v6, v7, :cond_6

    :goto_3
    if-eqz v4, :cond_2

    .line 105
    iget v0, v0, Lcom/google/r/b/a/agt;->c:I

    invoke-static {v0}, Lcom/google/maps/g/a/fu;->a(I)Lcom/google/maps/g/a/fu;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/fu;->a:Lcom/google/maps/g/a/fu;

    :cond_1
    sget-object v4, Lcom/google/maps/g/a/fu;->a:Lcom/google/maps/g/a/fu;

    if-ne v0, v4, :cond_8

    .line 106
    :cond_2
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    int-to-long v4, v0

    add-long/2addr v2, v4

    move-wide v8, v2

    move-object v2, v1

    move-wide v0, v8

    .line 117
    :goto_4
    new-instance v3, Lcom/google/android/apps/gmm/directions/bs;

    invoke-direct {v3, v2, v0, v1}, Lcom/google/android/apps/gmm/directions/bs;-><init>(Lcom/google/maps/g/a/hc;J)V

    return-object v3

    :cond_3
    move v1, v5

    .line 96
    goto :goto_0

    .line 99
    :cond_4
    sget-object v1, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    goto :goto_1

    :cond_5
    move v2, v5

    .line 102
    goto :goto_2

    :cond_6
    move v4, v5

    .line 104
    goto :goto_3

    .line 109
    :cond_7
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    .line 110
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    int-to-long v4, v0

    add-long/2addr v2, v4

    .line 113
    sget-object v0, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    if-ne v1, v0, :cond_8

    .line 114
    const/4 v1, 0x0

    move-wide v8, v2

    move-object v2, v1

    move-wide v0, v8

    goto :goto_4

    :cond_8
    move-wide v8, v2

    move-object v2, v1

    move-wide v0, v8

    goto :goto_4
.end method
