.class public Lcom/google/android/apps/gmm/mapsactivity/d/a/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/mapsactivity/a/d;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/mapsactivity/a/a;

.field private final b:Lcom/google/b/a/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/am",
            "<",
            "Lcom/google/r/b/a/lw;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/b/a/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/am",
            "<",
            "Lcom/google/r/b/a/lo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/apps/gmm/mapsactivity/a/a;Lcom/google/b/a/am;Lcom/google/b/a/am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/mapsactivity/a/a;",
            "Lcom/google/b/a/am",
            "<",
            "Lcom/google/r/b/a/lw;",
            ">;",
            "Lcom/google/b/a/am",
            "<",
            "Lcom/google/r/b/a/lo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p2, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/c;->b:Lcom/google/b/a/am;

    .line 45
    iput-object p1, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/c;->a:Lcom/google/android/apps/gmm/mapsactivity/a/a;

    .line 46
    iput-object p3, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/c;->c:Lcom/google/b/a/am;

    .line 47
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/mapsactivity/a/a;)Lcom/google/android/apps/gmm/mapsactivity/d/a/c;
    .locals 3

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/mapsactivity/d/a/c;

    .line 36
    sget-object v1, Lcom/google/b/a/a;->a:Lcom/google/b/a/a;

    .line 37
    sget-object v2, Lcom/google/b/a/a;->a:Lcom/google/b/a/a;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/gmm/mapsactivity/d/a/c;-><init>(Lcom/google/android/apps/gmm/mapsactivity/a/a;Lcom/google/b/a/am;Lcom/google/b/a/am;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/r/b/a/lo;)Lcom/google/android/apps/gmm/mapsactivity/a/d;
    .locals 4

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/mapsactivity/d/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/c;->a:Lcom/google/android/apps/gmm/mapsactivity/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/c;->b:Lcom/google/b/a/am;

    new-instance v3, Lcom/google/b/a/bb;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {v3, p1}, Lcom/google/b/a/bb;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/mapsactivity/d/a/c;-><init>(Lcom/google/android/apps/gmm/mapsactivity/a/a;Lcom/google/b/a/am;Lcom/google/b/a/am;)V

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/lw;)Lcom/google/android/apps/gmm/mapsactivity/a/d;
    .locals 4

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/apps/gmm/mapsactivity/d/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/c;->a:Lcom/google/android/apps/gmm/mapsactivity/a/a;

    new-instance v2, Lcom/google/b/a/bb;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {v2, p1}, Lcom/google/b/a/bb;-><init>(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/mapsactivity/d/a/c;->c:Lcom/google/b/a/am;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/mapsactivity/d/a/c;-><init>(Lcom/google/android/apps/gmm/mapsactivity/a/a;Lcom/google/b/a/am;Lcom/google/b/a/am;)V

    return-object v0
.end method
