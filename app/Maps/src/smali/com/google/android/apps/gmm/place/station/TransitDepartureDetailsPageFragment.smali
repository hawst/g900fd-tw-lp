.class public Lcom/google/android/apps/gmm/place/station/TransitDepartureDetailsPageFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"


# instance fields
.field private c:Lcom/google/android/apps/gmm/place/station/af;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/place/station/af;)Lcom/google/android/apps/gmm/place/station/TransitDepartureDetailsPageFragment;
    .locals 3

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/place/station/TransitDepartureDetailsPageFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/station/TransitDepartureDetailsPageFragment;-><init>()V

    .line 31
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 32
    const-string v2, "view_model"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 33
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/station/TransitDepartureDetailsPageFragment;->setArguments(Landroid/os/Bundle;)V

    .line 34
    return-object v0
.end method


# virtual methods
.method protected final b()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2

    .prologue
    .line 39
    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/TransitDepartureDetailsPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "view_model"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/station/af;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/TransitDepartureDetailsPageFragment;->c:Lcom/google/android/apps/gmm/place/station/af;

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/TransitDepartureDetailsPageFragment;->c:Lcom/google/android/apps/gmm/place/station/af;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/af;->a()Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 47
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/place/station/a/f;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 50
    iget-object v1, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/station/TransitDepartureDetailsPageFragment;->c:Lcom/google/android/apps/gmm/place/station/af;

    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 51
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 56
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 59
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/TransitDepartureDetailsPageFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    const/4 v1, 0x0

    .line 61
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 62
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 63
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 64
    return-void
.end method
