.class public Lcom/google/android/apps/gmm/place/d/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/d/e;


# instance fields
.field private final a:Lcom/google/maps/g/gu;

.field private final b:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method public constructor <init>(Lcom/google/maps/g/gu;Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/d/f;->a:Lcom/google/maps/g/gu;

    .line 24
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/d/f;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 25
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/f;->a:Lcom/google/maps/g/gu;

    iget-object v0, v0, Lcom/google/maps/g/gu;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fc;->i()Lcom/google/maps/g/fc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fc;

    invoke-virtual {v0}, Lcom/google/maps/g/fc;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/f;->a:Lcom/google/maps/g/gu;

    iget-object v0, v0, Lcom/google/maps/g/gu;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fc;->i()Lcom/google/maps/g/fc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fc;

    invoke-virtual {v0}, Lcom/google/maps/g/fc;->d()Ljava/lang/String;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/f;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->jW:I

    .line 46
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 47
    :goto_0
    return-object v0

    .line 46
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/d/f;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->jX:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 47
    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/f;->a:Lcom/google/maps/g/gu;

    iget-object v0, v0, Lcom/google/maps/g/gu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fc;->i()Lcom/google/maps/g/fc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fc;

    invoke-virtual {v0}, Lcom/google/maps/g/fc;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 29
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/f;->a:Lcom/google/maps/g/gu;

    iget v0, v0, Lcom/google/maps/g/gu;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    .line 30
    const-string v0, ""

    .line 33
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 29
    goto :goto_0

    .line 32
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/f;->a:Lcom/google/maps/g/gu;

    iget v0, v0, Lcom/google/maps/g/gu;->f:I

    .line 33
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/d/f;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/j;->C:I

    new-array v1, v1, [Ljava/lang/Object;

    .line 34
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 33
    invoke-virtual {v3, v4, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/f;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->ka:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/f;->a:Lcom/google/maps/g/gu;

    iget-object v0, v0, Lcom/google/maps/g/gu;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/d/f;->b:Lcom/google/android/apps/gmm/base/activities/c;

    const-string v2, "mail"

    .line 78
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    move-result-object v0

    .line 77
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 81
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/base/l/a/u;
    .locals 2

    .prologue
    .line 91
    new-instance v0, Lcom/google/android/apps/gmm/place/d/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/d/f;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/place/d/h;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/f;->a:Lcom/google/maps/g/gu;

    iget-object v0, v0, Lcom/google/maps/g/gu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fc;->i()Lcom/google/maps/g/fc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fc;

    invoke-virtual {v0}, Lcom/google/maps/g/fc;->d()Ljava/lang/String;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/f;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->jY:I

    .line 61
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    .line 61
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/d/f;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->jZ:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 62
    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
