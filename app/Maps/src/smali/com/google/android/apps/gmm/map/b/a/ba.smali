.class public Lcom/google/android/apps/gmm/map/b/a/ba;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/b/a/q;

.field public final b:Lcom/google/android/apps/gmm/map/b/a/q;

.field public final c:Lcom/google/android/apps/gmm/map/b/a/q;

.field public final d:Lcom/google/android/apps/gmm/map/b/a/q;

.field public final e:Lcom/google/android/apps/gmm/map/b/a/r;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/r;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 59
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 60
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 61
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 62
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->e:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 63
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 104
    if-ne p0, p1, :cond_1

    .line 112
    :cond_0
    :goto_0
    return v0

    .line 106
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/b/a/ba;

    if-nez v2, :cond_2

    move v0, v1

    .line 107
    goto :goto_0

    .line 109
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/ba;

    .line 110
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/ba;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/ba;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/ba;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 111
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/ba;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->e:Lcom/google/android/apps/gmm/map/b/a/r;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/ba;->e:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 112
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 91
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->e:Lcom/google/android/apps/gmm/map/b/a/r;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 118
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "nearLeft"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 119
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "nearRight"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 120
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "farLeft"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 121
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "farRight"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 122
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "latLngBounds"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ba;->e:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 123
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 124
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
