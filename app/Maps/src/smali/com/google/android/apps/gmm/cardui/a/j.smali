.class public Lcom/google/android/apps/gmm/cardui/a/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 26
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->h()Lcom/google/android/apps/gmm/cardui/c;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 27
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->g()Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 28
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->h()Lcom/google/android/apps/gmm/cardui/c;

    move-result-object v0

    .line 29
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->g()Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/apps/gmm/util/b/b;->a:Lcom/google/o/h/a/br;

    .line 28
    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/cardui/c;->a(Lcom/google/o/h/a/br;)V

    .line 34
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->q:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/fh;->d()Lcom/google/o/h/a/fh;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/fh;

    .line 35
    iget v4, v0, Lcom/google/o/h/a/fh;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v3, :cond_1

    move v4, v3

    :goto_0
    if-eqz v4, :cond_5

    .line 36
    invoke-static {}, Lcom/google/o/h/a/a;->newBuilder()Lcom/google/o/h/a/c;

    move-result-object v1

    iget-object v0, v0, Lcom/google/o/h/a/fh;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/pl;->d()Lcom/google/o/h/a/pl;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/pl;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move v4, v1

    .line 35
    goto :goto_0

    .line 36
    :cond_2
    iget-object v4, v1, Lcom/google/o/h/a/c;->d:Lcom/google/n/ao;

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v4, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/c;->a:I

    const/high16 v2, 0x20000

    or-int/2addr v0, v2

    iput v0, v1, Lcom/google/o/h/a/c;->a:I

    invoke-virtual {v1}, Lcom/google/o/h/a/c;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    move-object v2, v0

    .line 42
    :cond_3
    :goto_1
    if-eqz v2, :cond_4

    .line 43
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    .line 44
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->f()Lcom/google/android/apps/gmm/cardui/a/d;

    move-result-object v1

    .line 45
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->g()Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->h()Lcom/google/android/apps/gmm/cardui/c;

    move-result-object v4

    .line 46
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->i()Lcom/google/android/apps/gmm/cardui/b/a;

    move-result-object v5

    .line 43
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/cardui/a/a;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;Lcom/google/android/apps/gmm/cardui/c;Lcom/google/android/apps/gmm/cardui/b/a;)V

    .line 48
    :cond_4
    return-void

    .line 37
    :cond_5
    iget v4, v0, Lcom/google/o/h/a/fh;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_6

    move v1, v3

    :cond_6
    if-eqz v1, :cond_3

    .line 38
    invoke-static {}, Lcom/google/o/h/a/a;->newBuilder()Lcom/google/o/h/a/c;

    move-result-object v1

    .line 39
    iget-object v0, v0, Lcom/google/o/h/a/fh;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/jv;->d()Lcom/google/o/h/a/jv;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/jv;

    .line 38
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget-object v4, v1, Lcom/google/o/h/a/c;->c:Lcom/google/n/ao;

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v4, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/c;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, v1, Lcom/google/o/h/a/c;->a:I

    .line 39
    invoke-virtual {v1}, Lcom/google/o/h/a/c;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    move-object v2, v0

    goto :goto_1
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    sget-object v0, Lcom/google/o/h/a/g;->s:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 22
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    const v1, 0x8000

    .line 16
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
