.class public Lcom/google/android/apps/gmm/base/views/FiveStarTextView;
.super Lcom/google/android/apps/gmm/base/views/ExpandableTextView;
.source "PG"


# instance fields
.field public d:Ljava/lang/CharSequence;

.field public e:I

.field private final f:Landroid/graphics/drawable/Drawable;

.field private final g:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 66
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->d:Ljava/lang/CharSequence;

    .line 60
    iput v3, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->e:I

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/f;->fc:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->f:Landroid/graphics/drawable/Drawable;

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/f;->fa:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->g:Landroid/graphics/drawable/Drawable;

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->f:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->g:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 72
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 119
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->e:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->d:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->d:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->e:I

    sget v2, Lcom/google/android/apps/gmm/j;->b:I

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->d:Ljava/lang/CharSequence;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 120
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 127
    :goto_0
    return-void

    .line 122
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->e:I

    if-nez v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->d:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 125
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->e:I

    sget v2, Lcom/google/android/apps/gmm/j;->b:I

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public b()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    .line 130
    iget v4, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->e:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->d:Ljava/lang/CharSequence;

    if-nez v4, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    return-void

    .line 130
    :cond_0
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    const-string v0, "*"

    invoke-static {v0, v7}, Lcom/google/b/a/bv;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v0, "  "

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v7, :cond_2

    new-instance v5, Lcom/google/android/apps/gmm/base/views/r;

    if-ge v3, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->f:Landroid/graphics/drawable/Drawable;

    :goto_2
    invoke-direct {v5, p0, v0}, Lcom/google/android/apps/gmm/base/views/r;-><init>(Lcom/google/android/apps/gmm/base/views/FiveStarTextView;Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v3, 0x1

    const/16 v6, 0x12

    invoke-virtual {v2, v5, v3, v0, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->g:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    :cond_2
    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object v0, v2

    goto :goto_0
.end method
