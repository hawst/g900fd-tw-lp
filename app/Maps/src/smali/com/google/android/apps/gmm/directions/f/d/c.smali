.class public Lcom/google/android/apps/gmm/directions/f/d/c;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(J)J
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 174
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 176
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 177
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 178
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 179
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Lcom/google/maps/g/a/hm;Lcom/google/maps/g/a/hr;)Lcom/google/maps/g/a/ho;
    .locals 3

    .prologue
    .line 105
    invoke-static {}, Lcom/google/maps/g/a/ho;->newBuilder()Lcom/google/maps/g/a/hq;

    move-result-object v0

    .line 106
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v1, v0, Lcom/google/maps/g/a/hq;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/maps/g/a/hq;->a:I

    iget v1, p0, Lcom/google/maps/g/a/hm;->h:I

    iput v1, v0, Lcom/google/maps/g/a/hq;->b:I

    .line 107
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v1, v0, Lcom/google/maps/g/a/hq;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/maps/g/a/hq;->a:I

    iget v1, p1, Lcom/google/maps/g/a/hr;->d:I

    iput v1, v0, Lcom/google/maps/g/a/hq;->c:I

    const/4 v1, 0x0

    .line 108
    iget v2, v0, Lcom/google/maps/g/a/hq;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/maps/g/a/hq;->a:I

    iput-boolean v1, v0, Lcom/google/maps/g/a/hq;->d:Z

    .line 109
    invoke-virtual {v0}, Lcom/google/maps/g/a/hq;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ho;

    return-object v0
.end method

.method public static a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/afz;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 297
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 298
    :cond_0
    invoke-static {p2}, Lcom/google/r/b/a/afz;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v4

    .line 299
    iget v0, p2, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    move v0, v2

    :goto_0
    if-eqz v0, :cond_7

    .line 300
    iget-object v0, p2, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agt;->d()Lcom/google/r/b/a/agt;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agt;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move v0, v1

    .line 299
    goto :goto_0

    .line 300
    :cond_2
    invoke-static {v0}, Lcom/google/r/b/a/agt;->a(Lcom/google/r/b/a/agt;)Lcom/google/r/b/a/agw;

    move-result-object v5

    iget v3, v0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v2, :cond_4

    move v3, v2

    :goto_1
    if-eqz v3, :cond_6

    iget v0, v0, Lcom/google/r/b/a/agt;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/hc;->a(I)Lcom/google/maps/g/a/hc;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/maps/g/a/hc;->a:Lcom/google/maps/g/a/hc;

    :cond_3
    sget-object v3, Lcom/google/android/apps/gmm/directions/f/d/d;->a:[I

    invoke-virtual {v0}, Lcom/google/maps/g/a/hc;->ordinal()I

    move-result v6

    aget v3, v3, v6

    packed-switch v3, :pswitch_data_0

    sget-object v0, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    :goto_2
    :pswitch_0
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v3, v1

    goto :goto_1

    :pswitch_1
    sget-object v0, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    goto :goto_2

    :pswitch_2
    sget-object v0, Lcom/google/maps/g/a/hc;->c:Lcom/google/maps/g/a/hc;

    goto :goto_2

    :cond_5
    iget v3, v5, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v5, Lcom/google/r/b/a/agw;->a:I

    iget v0, v0, Lcom/google/maps/g/a/hc;->i:I

    iput v0, v5, Lcom/google/r/b/a/agw;->b:I

    :cond_6
    iget v0, v5, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, v5, Lcom/google/r/b/a/agw;->a:I

    iput-boolean v2, v5, Lcom/google/r/b/a/agw;->e:Z

    invoke-virtual {v5}, Lcom/google/r/b/a/agw;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agt;

    .line 299
    :goto_3
    invoke-virtual {v4, v0}, Lcom/google/r/b/a/agb;->a(Lcom/google/r/b/a/agt;)Lcom/google/r/b/a/agb;

    move-result-object v3

    .line 302
    invoke-static {p0}, Lcom/google/android/apps/gmm/directions/f/d/f;->c(Lcom/google/maps/g/a/hm;)Z

    move-result v0

    iget v4, v3, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, v3, Lcom/google/r/b/a/agb;->a:I

    iput-boolean v0, v3, Lcom/google/r/b/a/agb;->f:Z

    .line 303
    iget-object v0, p2, Lcom/google/r/b/a/afz;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ho;->d()Lcom/google/maps/g/a/ho;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ho;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 301
    :cond_7
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(Lcom/google/android/apps/gmm/shared/c/f;Ljava/util/TimeZone;)Lcom/google/r/b/a/agt;

    move-result-object v0

    goto :goto_3

    .line 303
    :cond_8
    iget v4, v0, Lcom/google/maps/g/a/ho;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_9

    move v1, v2

    :cond_9
    if-eqz v1, :cond_b

    iget v0, v0, Lcom/google/maps/g/a/ho;->c:I

    invoke-static {v0}, Lcom/google/maps/g/a/hr;->a(I)Lcom/google/maps/g/a/hr;

    move-result-object v0

    if-nez v0, :cond_a

    sget-object v0, Lcom/google/maps/g/a/hr;->a:Lcom/google/maps/g/a/hr;

    :cond_a
    :goto_4
    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(Lcom/google/maps/g/a/hm;Lcom/google/maps/g/a/hr;)Lcom/google/maps/g/a/ho;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/agb;->a(Lcom/google/maps/g/a/ho;)Lcom/google/r/b/a/agb;

    move-result-object v0

    .line 304
    iget v1, v0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v1, v1, 0x2000

    iput v1, v0, Lcom/google/r/b/a/agb;->a:I

    iput-boolean v2, v0, Lcom/google/r/b/a/agb;->i:Z

    .line 305
    invoke-virtual {v0}, Lcom/google/r/b/a/agb;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    return-object v0

    .line 303
    :cond_b
    sget-object v0, Lcom/google/maps/g/a/hr;->c:Lcom/google/maps/g/a/hr;

    goto :goto_4

    .line 300
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/google/maps/g/a/hm;Lcom/google/maps/g/a/hr;Lcom/google/android/apps/gmm/shared/c/f;Ljava/util/TimeZone;)Lcom/google/r/b/a/afz;
    .locals 4

    .prologue
    .line 91
    invoke-static {}, Lcom/google/r/b/a/afz;->newBuilder()Lcom/google/r/b/a/agb;

    move-result-object v0

    .line 92
    invoke-static {p2, p3}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(Lcom/google/android/apps/gmm/shared/c/f;Ljava/util/TimeZone;)Lcom/google/r/b/a/agt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/r/b/a/agb;->a(Lcom/google/r/b/a/agt;)Lcom/google/r/b/a/agb;

    move-result-object v1

    .line 93
    invoke-static {p0}, Lcom/google/android/apps/gmm/directions/f/d/f;->c(Lcom/google/maps/g/a/hm;)Z

    move-result v0

    iget v2, v1, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, v1, Lcom/google/r/b/a/agb;->a:I

    iput-boolean v0, v1, Lcom/google/r/b/a/agb;->f:Z

    const/4 v0, 0x1

    .line 94
    iget v2, v1, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, v1, Lcom/google/r/b/a/agb;->a:I

    iput-boolean v0, v1, Lcom/google/r/b/a/agb;->i:Z

    .line 95
    invoke-static {}, Lcom/google/maps/g/a/ho;->newBuilder()Lcom/google/maps/g/a/hq;

    move-result-object v0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/maps/g/a/hq;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/maps/g/a/hq;->a:I

    iget v2, p0, Lcom/google/maps/g/a/hm;->h:I

    iput v2, v0, Lcom/google/maps/g/a/hq;->b:I

    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v2, v0, Lcom/google/maps/g/a/hq;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/maps/g/a/hq;->a:I

    iget v2, p1, Lcom/google/maps/g/a/hr;->d:I

    iput v2, v0, Lcom/google/maps/g/a/hq;->c:I

    const/4 v2, 0x0

    iget v3, v0, Lcom/google/maps/g/a/hq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v0, Lcom/google/maps/g/a/hq;->a:I

    iput-boolean v2, v0, Lcom/google/maps/g/a/hq;->d:Z

    invoke-virtual {v0}, Lcom/google/maps/g/a/hq;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ho;

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/agb;->a(Lcom/google/maps/g/a/ho;)Lcom/google/r/b/a/agb;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lcom/google/r/b/a/agb;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    return-object v0
.end method

.method private static a(Lcom/google/android/apps/gmm/shared/c/f;Ljava/util/TimeZone;)Lcom/google/r/b/a/agt;
    .locals 6

    .prologue
    .line 48
    invoke-static {}, Lcom/google/r/b/a/agt;->newBuilder()Lcom/google/r/b/a/agw;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    .line 49
    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/r/b/a/agw;->a:I

    iget v1, v1, Lcom/google/maps/g/a/hc;->i:I

    iput v1, v0, Lcom/google/r/b/a/agw;->b:I

    sget-object v1, Lcom/google/maps/g/a/fu;->b:Lcom/google/maps/g/a/fu;

    .line 50
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v2, v0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/r/b/a/agw;->a:I

    iget v1, v1, Lcom/google/maps/g/a/fu;->c:I

    iput v1, v0, Lcom/google/r/b/a/agw;->c:I

    .line 51
    invoke-interface {p0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    iget v1, v0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/google/r/b/a/agw;->a:I

    iput-wide v2, v0, Lcom/google/r/b/a/agw;->d:J

    .line 52
    invoke-virtual {v0}, Lcom/google/r/b/a/agw;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agt;

    return-object v0
.end method

.method public static b(J)Ljava/util/Calendar;
    .locals 8

    .prologue
    .line 223
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v6

    .line 224
    invoke-virtual {v6}, Ljava/util/Calendar;->clear()V

    .line 225
    invoke-virtual {v6, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 227
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 228
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 229
    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v6, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x5

    .line 230
    invoke-virtual {v6, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v4, 0xb

    invoke-virtual {v6, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/16 v5, 0xc

    .line 231
    invoke-virtual {v6, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v7, 0xd

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 229
    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 233
    return-object v0
.end method
