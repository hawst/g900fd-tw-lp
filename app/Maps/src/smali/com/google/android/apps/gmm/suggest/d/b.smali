.class public final enum Lcom/google/android/apps/gmm/suggest/d/b;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/suggest/d/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/suggest/d/b;

.field public static final enum b:Lcom/google/android/apps/gmm/suggest/d/b;

.field public static final enum c:Lcom/google/android/apps/gmm/suggest/d/b;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/suggest/d/b;


# instance fields
.field final d:I

.field public final e:Lcom/google/b/f/t;

.field public final f:Lcom/google/b/f/cj;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v7, 0x3

    const/4 v10, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 29
    new-instance v0, Lcom/google/android/apps/gmm/suggest/d/b;

    const-string v1, "CLICKED_SUGGESTION"

    sget-object v4, Lcom/google/b/f/t;->gr:Lcom/google/b/f/t;

    sget-object v5, Lcom/google/b/f/cj;->e:Lcom/google/b/f/cj;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/suggest/d/b;-><init>(Ljava/lang/String;IILcom/google/b/f/t;Lcom/google/b/f/cj;)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/d/b;->a:Lcom/google/android/apps/gmm/suggest/d/b;

    .line 33
    new-instance v4, Lcom/google/android/apps/gmm/suggest/d/b;

    const-string v5, "ENTER_KEY"

    sget-object v8, Lcom/google/b/f/t;->cE:Lcom/google/b/f/t;

    sget-object v9, Lcom/google/b/f/cj;->f:Lcom/google/b/f/cj;

    move v6, v3

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/gmm/suggest/d/b;-><init>(Ljava/lang/String;IILcom/google/b/f/t;Lcom/google/b/f/cj;)V

    sput-object v4, Lcom/google/android/apps/gmm/suggest/d/b;->b:Lcom/google/android/apps/gmm/suggest/d/b;

    .line 37
    new-instance v8, Lcom/google/android/apps/gmm/suggest/d/b;

    const-string v9, "SPEECH_RECOGNITION"

    const/16 v11, 0xf

    sget-object v12, Lcom/google/b/f/t;->gn:Lcom/google/b/f/t;

    sget-object v13, Lcom/google/b/f/cj;->r:Lcom/google/b/f/cj;

    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/gmm/suggest/d/b;-><init>(Ljava/lang/String;IILcom/google/b/f/t;Lcom/google/b/f/cj;)V

    sput-object v8, Lcom/google/android/apps/gmm/suggest/d/b;->c:Lcom/google/android/apps/gmm/suggest/d/b;

    .line 28
    new-array v0, v7, [Lcom/google/android/apps/gmm/suggest/d/b;

    sget-object v1, Lcom/google/android/apps/gmm/suggest/d/b;->a:Lcom/google/android/apps/gmm/suggest/d/b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/suggest/d/b;->b:Lcom/google/android/apps/gmm/suggest/d/b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/suggest/d/b;->c:Lcom/google/android/apps/gmm/suggest/d/b;

    aput-object v1, v0, v10

    sput-object v0, Lcom/google/android/apps/gmm/suggest/d/b;->g:[Lcom/google/android/apps/gmm/suggest/d/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILcom/google/b/f/t;Lcom/google/b/f/cj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/b/f/t;",
            "Lcom/google/b/f/cj;",
            ")V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput p3, p0, Lcom/google/android/apps/gmm/suggest/d/b;->d:I

    .line 51
    iput-object p4, p0, Lcom/google/android/apps/gmm/suggest/d/b;->e:Lcom/google/b/f/t;

    .line 52
    iput-object p5, p0, Lcom/google/android/apps/gmm/suggest/d/b;->f:Lcom/google/b/f/cj;

    .line 53
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/suggest/d/b;
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/gmm/suggest/d/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/suggest/d/b;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/suggest/d/b;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/b;->g:[Lcom/google/android/apps/gmm/suggest/d/b;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/suggest/d/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/suggest/d/b;

    return-object v0
.end method
