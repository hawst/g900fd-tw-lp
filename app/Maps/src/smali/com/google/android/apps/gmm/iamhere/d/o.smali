.class public final enum Lcom/google/android/apps/gmm/iamhere/d/o;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/iamhere/d/o;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/iamhere/d/o;

.field public static final enum b:Lcom/google/android/apps/gmm/iamhere/d/o;

.field public static final enum c:Lcom/google/android/apps/gmm/iamhere/d/o;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/iamhere/d/o;


# instance fields
.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/o;

    const-string v1, "UNKNOWN"

    const-string v2, ""

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/iamhere/d/o;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/o;->a:Lcom/google/android/apps/gmm/iamhere/d/o;

    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/o;

    const-string v1, "ACTION"

    const-string v2, "ACTION_LAUNCH_NOTIFICATION_ACTIVITY"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/iamhere/d/o;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/o;->b:Lcom/google/android/apps/gmm/iamhere/d/o;

    .line 25
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/o;

    const-string v1, "DISAMBIGUATION"

    const-string v2, "ACTION_LAUNCH_DISAMBIGUATION_ACTIVITY"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/iamhere/d/o;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/o;->c:Lcom/google/android/apps/gmm/iamhere/d/o;

    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/iamhere/d/o;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/o;->a:Lcom/google/android/apps/gmm/iamhere/d/o;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/o;->b:Lcom/google/android/apps/gmm/iamhere/d/o;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/o;->c:Lcom/google/android/apps/gmm/iamhere/d/o;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/o;->e:[Lcom/google/android/apps/gmm/iamhere/d/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    const-class v0, Lcom/google/android/apps/gmm/iamhere/d/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/o;->d:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/apps/gmm/iamhere/d/o;
    .locals 5

    .prologue
    .line 34
    invoke-static {}, Lcom/google/android/apps/gmm/iamhere/d/o;->values()[Lcom/google/android/apps/gmm/iamhere/d/o;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 35
    iget-object v4, v0, Lcom/google/android/apps/gmm/iamhere/d/o;->d:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 39
    :goto_1
    return-object v0

    .line 34
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 39
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/o;->a:Lcom/google/android/apps/gmm/iamhere/d/o;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/iamhere/d/o;
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/gmm/iamhere/d/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/d/o;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/iamhere/d/o;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/o;->e:[Lcom/google/android/apps/gmm/iamhere/d/o;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/iamhere/d/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/iamhere/d/o;

    return-object v0
.end method
