.class public Lcom/google/android/apps/gmm/util/b/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/o/h/a/br;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final d:Lcom/google/o/h/a/qr;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final e:Lcom/google/o/h/a/od;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final f:F


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/util/b/b;

    const/high16 v4, 0x7fc00000    # NaNf

    move-object v2, v1

    move-object v3, v1

    move-object v5, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/util/b/b;-><init>(Lcom/google/o/h/a/br;Lcom/google/o/h/a/qr;Lcom/google/o/h/a/od;FLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/o/h/a/br;Lcom/google/o/h/a/qr;Lcom/google/o/h/a/od;FLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/o/h/a/br;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/o/h/a/qr;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/o/h/a/od;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/google/android/apps/gmm/util/b/b;->a:Lcom/google/o/h/a/br;

    .line 52
    iput-object p2, p0, Lcom/google/android/apps/gmm/util/b/b;->d:Lcom/google/o/h/a/qr;

    .line 53
    iput-object p3, p0, Lcom/google/android/apps/gmm/util/b/b;->e:Lcom/google/o/h/a/od;

    .line 54
    iput p4, p0, Lcom/google/android/apps/gmm/util/b/b;->f:F

    .line 55
    iput-object p5, p0, Lcom/google/android/apps/gmm/util/b/b;->b:Ljava/lang/String;

    .line 56
    iput-object p6, p0, Lcom/google/android/apps/gmm/util/b/b;->c:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public static a(Lcom/google/o/h/a/br;FLjava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 107
    new-instance v0, Lcom/google/android/apps/gmm/util/b/b;

    move-object v1, p0

    move-object v3, v2

    move v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/util/b/b;-><init>(Lcom/google/o/h/a/br;Lcom/google/o/h/a/qr;Lcom/google/o/h/a/od;FLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Lcom/google/o/h/a/br;Lcom/google/o/h/a/od;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;
    .locals 7

    .prologue
    .line 102
    new-instance v0, Lcom/google/android/apps/gmm/util/b/b;

    const/4 v2, 0x0

    const/high16 v4, 0x7fc00000    # NaNf

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/util/b/b;-><init>(Lcom/google/o/h/a/br;Lcom/google/o/h/a/qr;Lcom/google/o/h/a/od;FLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Lcom/google/o/h/a/br;Lcom/google/o/h/a/qr;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;
    .locals 7

    .prologue
    .line 97
    new-instance v0, Lcom/google/android/apps/gmm/util/b/b;

    const/4 v3, 0x0

    const/high16 v4, 0x7fc00000    # NaNf

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/util/b/b;-><init>(Lcom/google/o/h/a/br;Lcom/google/o/h/a/qr;Lcom/google/o/h/a/od;FLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Lcom/google/o/h/a/br;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 92
    new-instance v0, Lcom/google/android/apps/gmm/util/b/b;

    const/high16 v4, 0x7fc00000    # NaNf

    move-object v1, p0

    move-object v3, v2

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/util/b/b;-><init>(Lcom/google/o/h/a/br;Lcom/google/o/h/a/qr;Lcom/google/o/h/a/od;FLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
