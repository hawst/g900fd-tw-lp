.class public Lcom/google/android/apps/gmm/map/b/a/ag;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/e/a/a/a/b;


# direct methods
.method public constructor <init>(Lcom/google/e/a/a/a/b;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/b/a/ag;->a:Lcom/google/e/a/a/a/b;

    .line 24
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 33
    if-ne p0, p1, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v0

    .line 37
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/b/a/ag;

    if-nez v2, :cond_2

    move v0, v1

    .line 38
    goto :goto_0

    .line 41
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/ag;

    .line 43
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ag;->a:Lcom/google/e/a/a/a/b;

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ag;->a:Lcom/google/e/a/a/a/b;

    if-eqz v2, :cond_3

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ag;->a:Lcom/google/e/a/a/a/b;

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ag;->a:Lcom/google/e/a/a/a/b;

    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {v2, v3}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-static {v0, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0

    .line 46
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ag;->a:Lcom/google/e/a/a/a/b;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/ag;->a:Lcom/google/e/a/a/a/b;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 49
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method
