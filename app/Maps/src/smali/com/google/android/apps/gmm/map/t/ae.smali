.class public Lcom/google/android/apps/gmm/map/t/ae;
.super Lcom/google/android/apps/gmm/v/bh;
.source "PG"


# instance fields
.field private final a:[Lcom/google/android/apps/gmm/map/t/n;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(I)V
    .locals 5

    .prologue
    const/16 v4, 0x16

    const/4 v1, 0x0

    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/v/bh;-><init>(I)V

    .line 28
    new-array v0, v4, [Lcom/google/android/apps/gmm/map/t/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->a:[Lcom/google/android/apps/gmm/map/t/n;

    .line 34
    const/16 v0, 0x15

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->b:I

    .line 39
    iput v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->c:I

    move v0, v1

    .line 43
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ae;->a:[Lcom/google/android/apps/gmm/map/t/n;

    if-ge v0, v4, :cond_0

    .line 44
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ae;->a:[Lcom/google/android/apps/gmm/map/t/n;

    new-instance v3, Lcom/google/android/apps/gmm/map/t/n;

    invoke-direct {v3, p1, v1}, Lcom/google/android/apps/gmm/map/t/n;-><init>(IZ)V

    aput-object v3, v2, v0

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 3

    .prologue
    .line 74
    check-cast p1, Lcom/google/android/apps/gmm/map/t/ar;

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->a:[Lcom/google/android/apps/gmm/map/t/n;

    iget v1, p1, Lcom/google/android/apps/gmm/map/t/ar;->a:I

    aget-object v1, v0, v1

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/map/t/p;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/p;->c:Lcom/google/android/apps/gmm/map/t/k;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/t/k;->b()I

    move-result v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/v/bh;->a(Lcom/google/android/apps/gmm/v/aa;)V

    .line 77
    iget v0, p1, Lcom/google/android/apps/gmm/map/t/ar;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->b:I

    .line 78
    iget v0, p1, Lcom/google/android/apps/gmm/map/t/ar;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->c:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->c:I

    .line 79
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ad;)V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->f:Lcom/google/android/apps/gmm/v/n;

    if-nez v0, :cond_0

    .line 70
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->f:Lcom/google/android/apps/gmm/v/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/bi;->b(Lcom/google/android/apps/gmm/v/aq;)V

    .line 63
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->b:I

    :goto_1
    iget v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->c:I

    if-gt v0, v1, :cond_1

    .line 67
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->a:[Lcom/google/android/apps/gmm/map/t/n;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/map/t/n;->a(Lcom/google/android/apps/gmm/v/ad;)V

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->f:Lcom/google/android/apps/gmm/v/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/n;)V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/bh;->a(Lcom/google/android/apps/gmm/v/n;)V

    .line 51
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->a:[Lcom/google/android/apps/gmm/map/t/n;

    const/16 v1, 0x16

    if-ge v0, v1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->a:[Lcom/google/android/apps/gmm/map/t/n;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/map/t/n;->a(Lcom/google/android/apps/gmm/v/n;)V

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 158
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ae;->a:[Lcom/google/android/apps/gmm/map/t/n;

    move v1, v0

    :goto_0
    const/16 v3, 0x16

    if-ge v1, v3, :cond_1

    aget-object v3, v2, v1

    .line 159
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/t/n;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 163
    :goto_1
    return v0

    .line 158
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 163
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final b(Lcom/google/android/apps/gmm/v/ad;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/v/ad;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 118
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ae;->a:[Lcom/google/android/apps/gmm/map/t/n;

    const/4 v0, 0x0

    :goto_0
    const/16 v3, 0x16

    if-ge v0, v3, :cond_0

    aget-object v3, v2, v0

    .line 119
    invoke-virtual {v3, p1}, Lcom/google/android/apps/gmm/map/t/n;->b(Lcom/google/android/apps/gmm/v/ad;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 122
    :cond_0
    return-object v1
.end method

.method public final b(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 3

    .prologue
    .line 83
    check-cast p1, Lcom/google/android/apps/gmm/map/t/ar;

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->a:[Lcom/google/android/apps/gmm/map/t/n;

    iget v1, p1, Lcom/google/android/apps/gmm/map/t/ar;->a:I

    aget-object v1, v0, v1

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/map/t/p;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/p;->c:Lcom/google/android/apps/gmm/map/t/k;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/t/k;->b()I

    move-result v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/v/bh;->b(Lcom/google/android/apps/gmm/v/aa;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->a:[Lcom/google/android/apps/gmm/map/t/n;

    iget v1, p1, Lcom/google/android/apps/gmm/map/t/ar;->a:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t/n;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->b:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->c:I

    if-ne v0, v1, :cond_1

    .line 89
    const/16 v0, 0x15

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->b:I

    .line 90
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->c:I

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    iget v0, p1, Lcom/google/android/apps/gmm/map/t/ar;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->b:I

    if-ne v0, v1, :cond_2

    .line 96
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->b:I

    add-int/lit8 v0, v0, 0x1

    :goto_1
    iget v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->c:I

    if-gt v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->a:[Lcom/google/android/apps/gmm/map/t/n;

    const/16 v1, 0x16

    if-ge v0, v1, :cond_2

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->a:[Lcom/google/android/apps/gmm/map/t/n;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/t/n;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 98
    iput v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->b:I

    .line 103
    :cond_2
    iget v0, p1, Lcom/google/android/apps/gmm/map/t/ar;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->c:I

    if-ne v0, v1, :cond_0

    .line 105
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->c:I

    add-int/lit8 v0, v0, -0x1

    :goto_2
    iget v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->b:I

    if-lt v0, v1, :cond_0

    if-ltz v0, :cond_0

    .line 106
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ae;->a:[Lcom/google/android/apps/gmm/map/t/n;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/t/n;->a()Z

    move-result v1

    if-nez v1, :cond_4

    .line 107
    iput v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->c:I

    goto :goto_0

    .line 96
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 105
    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 143
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/ae;->h:I

    invoke-static {v0}, Lcom/google/android/apps/gmm/v/aa;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xe

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "GmmRenderBin["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
