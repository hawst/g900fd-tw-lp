.class public Lcom/google/android/apps/gmm/search/restriction/configuration/a;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/search/restriction/configuration/e;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/google/android/libraries/curvular/cs;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v8, 0x2

    const/4 v2, -0x1

    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 129
    const/4 v0, 0x7

    new-array v7, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 130
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v7, v6

    .line 131
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v7, v9

    .line 132
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v7, v8

    .line 135
    sget v0, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v7, v10

    const/4 v1, 0x4

    sget v0, Lcom/google/android/apps/gmm/l;->lJ:I

    .line 139
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 140
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    sget v0, Lcom/google/android/apps/gmm/l;->lP:I

    .line 141
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 142
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/search/restriction/configuration/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/configuration/e;->d()Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    const/4 v5, 0x0

    .line 138
    invoke-static {v2, v3, v4, v0, v5}, Lcom/google/android/apps/gmm/base/f/o;->a(Ljava/lang/CharSequence;Ljava/lang/Boolean;Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v7, v1

    const/4 v0, 0x5

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/google/android/libraries/curvular/cu;

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/restriction/configuration/a;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x0

    .line 149
    sget-object v3, Lcom/google/android/libraries/curvular/g;->C:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v9

    new-instance v2, Lcom/google/android/libraries/curvular/a;

    invoke-direct {v2, v6}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    .line 152
    sget-object v3, Lcom/google/android/libraries/curvular/g;->aA:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v8

    const/high16 v2, 0x3f800000    # 1.0f

    .line 155
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v10

    const/4 v2, 0x4

    .line 156
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v1, v2

    .line 147
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.ListView"

    sget-object v3, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v1

    aput-object v1, v7, v0

    const/4 v8, 0x6

    sget v0, Lcom/google/android/apps/gmm/l;->bg:I

    .line 161
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->aX:I

    .line 162
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 163
    iget-object v2, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/search/restriction/configuration/e;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/search/restriction/configuration/e;->c()Lcom/google/android/libraries/curvular/cf;

    move-result-object v2

    .line 164
    iget-object v3, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v3}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/curvular/ce;

    check-cast v3, Lcom/google/android/apps/gmm/search/restriction/configuration/e;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/search/restriction/configuration/e;->b()Lcom/google/android/libraries/curvular/cf;

    move-result-object v3

    .line 165
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 166
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    new-array v6, v6, [Lcom/google/android/libraries/curvular/cu;

    .line 160
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/base/f/ad;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/cf;Ljava/lang/Boolean;Ljava/lang/Boolean;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v7, v8

    .line 129
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v7}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.LinearLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 3

    .prologue
    .line 70
    check-cast p2, Lcom/google/android/apps/gmm/search/restriction/configuration/e;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/search/restriction/configuration/e;->a()Lcom/google/android/apps/gmm/search/restriction/d/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->g()Lcom/google/android/apps/gmm/base/l/a/h;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/l/a/h;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const-class v1, Lcom/google/android/apps/gmm/search/restriction/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->g()Lcom/google/android/apps/gmm/base/l/a/h;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_0
    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->l()Lcom/google/android/apps/gmm/search/restriction/d/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/search/restriction/d/d;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const-class v1, Lcom/google/android/apps/gmm/search/restriction/e/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->l()Lcom/google/android/apps/gmm/search/restriction/d/d;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->j()Lcom/google/android/apps/gmm/search/restriction/d/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/search/restriction/d/d;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    const-class v1, Lcom/google/android/apps/gmm/search/restriction/e/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->j()Lcom/google/android/apps/gmm/search/restriction/d/d;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_2
    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->k()Lcom/google/android/apps/gmm/search/restriction/d/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/search/restriction/d/d;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    const-class v1, Lcom/google/android/apps/gmm/search/restriction/e/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->k()Lcom/google/android/apps/gmm/search/restriction/d/d;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_3
    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->e()Lcom/google/android/apps/gmm/search/restriction/d/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/search/restriction/d/d;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    const-class v1, Lcom/google/android/apps/gmm/search/restriction/e/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->e()Lcom/google/android/apps/gmm/search/restriction/d/d;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_4
    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->f()Lcom/google/android/apps/gmm/search/restriction/d/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/search/restriction/d/d;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5

    const-class v1, Lcom/google/android/apps/gmm/search/restriction/e/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->f()Lcom/google/android/apps/gmm/search/restriction/d/d;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_5
    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->d()Lcom/google/android/apps/gmm/search/restriction/d/c;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/search/restriction/d/c;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_6

    const-class v1, Lcom/google/android/apps/gmm/search/restriction/e/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->d()Lcom/google/android/apps/gmm/search/restriction/d/c;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_6
    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->h()Lcom/google/android/apps/gmm/base/l/a/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/l/a/f;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_7

    const-class v1, Lcom/google/android/apps/gmm/base/f/aw;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->h()Lcom/google/android/apps/gmm/base/l/a/f;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_7
    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->i()Lcom/google/android/apps/gmm/base/l/a/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/l/a/f;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    const-class v1, Lcom/google/android/apps/gmm/base/f/aw;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/b;->i()Lcom/google/android/apps/gmm/base/l/a/f;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_8
    const-class v1, Lcom/google/android/apps/gmm/base/f/cf;

    invoke-virtual {p4, v1, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    return-void
.end method
