.class public final enum Lcom/google/android/apps/gmm/search/restriction/c/f;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/search/restriction/c/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/search/restriction/c/f;

.field public static final enum b:Lcom/google/android/apps/gmm/search/restriction/c/f;

.field public static final enum c:Lcom/google/android/apps/gmm/search/restriction/c/f;

.field public static final enum d:Lcom/google/android/apps/gmm/search/restriction/c/f;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/search/restriction/c/f;


# instance fields
.field e:Lcom/google/b/c/dn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dn",
            "<",
            "Lcom/google/maps/g/pz;",
            ">;"
        }
    .end annotation
.end field

.field final f:Lcom/google/b/f/t;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/f;

    const-string v1, "ANY"

    sget-object v2, Lcom/google/b/f/t;->v:Lcom/google/b/f/t;

    new-array v3, v5, [Lcom/google/maps/g/pz;

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/gmm/search/restriction/c/f;-><init>(Ljava/lang/String;ILcom/google/b/f/t;[Lcom/google/maps/g/pz;)V

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/f;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/f;

    const-string v1, "THREE_PLUS"

    sget-object v2, Lcom/google/b/f/t;->y:Lcom/google/b/f/t;

    new-array v3, v8, [Lcom/google/maps/g/pz;

    sget-object v4, Lcom/google/maps/g/pz;->c:Lcom/google/maps/g/pz;

    aput-object v4, v3, v5

    sget-object v4, Lcom/google/maps/g/pz;->d:Lcom/google/maps/g/pz;

    aput-object v4, v3, v6

    sget-object v4, Lcom/google/maps/g/pz;->e:Lcom/google/maps/g/pz;

    aput-object v4, v3, v7

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/gmm/search/restriction/c/f;-><init>(Ljava/lang/String;ILcom/google/b/f/t;[Lcom/google/maps/g/pz;)V

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/f;->b:Lcom/google/android/apps/gmm/search/restriction/c/f;

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/f;

    const-string v1, "FOUR_PLUS"

    sget-object v2, Lcom/google/b/f/t;->x:Lcom/google/b/f/t;

    new-array v3, v7, [Lcom/google/maps/g/pz;

    sget-object v4, Lcom/google/maps/g/pz;->d:Lcom/google/maps/g/pz;

    aput-object v4, v3, v5

    sget-object v4, Lcom/google/maps/g/pz;->e:Lcom/google/maps/g/pz;

    aput-object v4, v3, v6

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/apps/gmm/search/restriction/c/f;-><init>(Ljava/lang/String;ILcom/google/b/f/t;[Lcom/google/maps/g/pz;)V

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/f;->c:Lcom/google/android/apps/gmm/search/restriction/c/f;

    .line 46
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/f;

    const-string v1, "FIVE"

    sget-object v2, Lcom/google/b/f/t;->w:Lcom/google/b/f/t;

    new-array v3, v6, [Lcom/google/maps/g/pz;

    sget-object v4, Lcom/google/maps/g/pz;->e:Lcom/google/maps/g/pz;

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/apps/gmm/search/restriction/c/f;-><init>(Ljava/lang/String;ILcom/google/b/f/t;[Lcom/google/maps/g/pz;)V

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/f;->d:Lcom/google/android/apps/gmm/search/restriction/c/f;

    .line 36
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/search/restriction/c/f;

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/f;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/f;->b:Lcom/google/android/apps/gmm/search/restriction/c/f;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/f;->c:Lcom/google/android/apps/gmm/search/restriction/c/f;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/f;->d:Lcom/google/android/apps/gmm/search/restriction/c/f;

    aput-object v1, v0, v8

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/f;->g:[Lcom/google/android/apps/gmm/search/restriction/c/f;

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;ILcom/google/b/f/t;[Lcom/google/maps/g/pz;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/f/t;",
            "[",
            "Lcom/google/maps/g/pz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 80
    iput-object p3, p0, Lcom/google/android/apps/gmm/search/restriction/c/f;->f:Lcom/google/b/f/t;

    .line 81
    invoke-static {p4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/jp;->a(Ljava/lang/Iterable;)Lcom/google/b/c/dn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/f;->e:Lcom/google/b/c/dn;

    .line 82
    return-void
.end method

.method public static a(Ljava/util/Set;)Lcom/google/android/apps/gmm/search/restriction/c/f;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/maps/g/pz;",
            ">;)",
            "Lcom/google/android/apps/gmm/search/restriction/c/f;"
        }
    .end annotation

    .prologue
    .line 61
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/f;->values()[Lcom/google/android/apps/gmm/search/restriction/c/f;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 62
    iget-object v4, v3, Lcom/google/android/apps/gmm/search/restriction/c/f;->e:Lcom/google/b/c/dn;

    invoke-virtual {v4, p0}, Lcom/google/b/c/dn;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 63
    return-object v3

    .line 61
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x5a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "The Set "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not comprise only  of AllowedStars in one of the SelectionOption Set values."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/search/restriction/c/f;
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/android/apps/gmm/search/restriction/c/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/restriction/c/f;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/search/restriction/c/f;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/apps/gmm/search/restriction/c/f;->g:[Lcom/google/android/apps/gmm/search/restriction/c/f;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/search/restriction/c/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/search/restriction/c/f;

    return-object v0
.end method
