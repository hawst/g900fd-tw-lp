.class public Lcom/google/android/apps/gmm/place/facepile/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/facepile/a/a;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/apps/gmm/base/activities/c;

.field b:Ljava/lang/String;

.field c:Z

.field d:Ljava/lang/String;

.field private f:Lcom/google/maps/g/fg;

.field private g:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/gmm/place/facepile/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/facepile/b;->e:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/maps/g/fg;ZII)V
    .locals 3

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/facepile/b;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 43
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/facepile/b;->f:Lcom/google/maps/g/fg;

    .line 44
    iget-object v0, p2, Lcom/google/maps/g/fg;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/b;->b:Ljava/lang/String;

    .line 45
    invoke-virtual {p2}, Lcom/google/maps/g/fg;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/place/facepile/b;->e:Ljava/lang/String;

    const-string v1, "obfuscatedGaiaId is null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/b;->g:Ljava/util/concurrent/Callable;

    .line 46
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/place/facepile/b;->c:Z

    .line 47
    return-void

    .line 44
    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p2, Lcom/google/maps/g/fg;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0

    .line 45
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/place/facepile/c;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/gmm/place/facepile/c;-><init>(Lcom/google/android/apps/gmm/place/facepile/b;Lcom/google/maps/g/fg;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 4

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/k;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/facepile/b;->f:Lcom/google/maps/g/fg;

    invoke-virtual {v1}, Lcom/google/maps/g/fg;->g()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/util/webimageview/b;->a:Lcom/google/android/apps/gmm/util/webimageview/b;

    sget v3, Lcom/google/android/apps/gmm/f;->cD:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;I)V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/b;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/b;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/b;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/b;->f:Lcom/google/maps/g/fg;

    invoke-virtual {v0}, Lcom/google/maps/g/fg;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 89
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/b;->g:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    const/4 v0, 0x0

    return-object v0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
