.class Lcom/google/android/apps/gmm/map/internal/vector/u;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Z

.field b:Lcom/google/android/apps/gmm/map/internal/vector/t;

.field private c:Z

.field private d:I

.field private e:Z

.field private f:Z


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/vector/t;)V
    .locals 1

    .prologue
    .line 1661
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p1, Lcom/google/android/apps/gmm/map/internal/vector/t;->a:Z

    .line 1662
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->b:Lcom/google/android/apps/gmm/map/internal/vector/t;

    if-ne v0, p1, :cond_0

    .line 1663
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->b:Lcom/google/android/apps/gmm/map/internal/vector/t;

    .line 1665
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1666
    monitor-exit p0

    return-void

    .line 1661
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1719
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->e:Z

    if-nez v2, :cond_3

    .line 1720
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/u;->c()V

    .line 1721
    const/16 v2, 0x1f01

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v3

    .line 1722
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->d:I

    const/high16 v4, 0x20000

    if-ge v2, v4, :cond_0

    .line 1723
    const-string v2, "Q3Dimension MSM7500 "

    .line 1724
    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    move v2, v1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->a:Z

    .line 1725
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1728
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->a:Z

    if-eqz v2, :cond_1

    const-string v2, "Adreno"

    .line 1729
    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->f:Z

    .line 1736
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1738
    :cond_3
    monitor-exit p0

    return-void

    :cond_4
    move v2, v0

    .line 1724
    goto :goto_0

    .line 1719
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 1710
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->f:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 1714
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/u;->c()V

    .line 1715
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1714
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method c()V
    .locals 3

    .prologue
    const/high16 v2, 0x20000

    const/4 v1, 0x1

    .line 1741
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->c:Z

    if-nez v0, :cond_1

    .line 1745
    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->d:I

    .line 1746
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->d:I

    if-lt v0, v2, :cond_0

    .line 1747
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->a:Z

    .line 1753
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/u;->c:Z

    .line 1755
    :cond_1
    return-void
.end method
