.class public Lcom/google/android/apps/gmm/place/GeocodePlacePageView;
.super Lcom/google/android/apps/gmm/place/PlacePageView;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private c:Lcom/google/android/apps/gmm/place/r;

.field private d:Lcom/google/android/apps/gmm/place/q;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/place/PlacePageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->c:Lcom/google/android/apps/gmm/place/r;

    .line 36
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->d:Lcom/google/android/apps/gmm/place/q;

    .line 43
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    .line 44
    new-instance v1, Lcom/google/android/apps/gmm/place/bq;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/place/bq;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/place/af;)V

    .line 45
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/place/af;)V
    .locals 2

    .prologue
    .line 54
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    .line 56
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    .line 57
    const-class v1, Lcom/google/android/apps/gmm/place/c/k;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/place/r;

    .line 59
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/place/af;->L()Lcom/google/android/apps/gmm/base/l/a/q;

    move-result-object v1

    invoke-direct {v0, v1, p1, p0}, Lcom/google/android/apps/gmm/place/r;-><init>(Lcom/google/android/apps/gmm/base/l/a/q;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/place/PlacePageView;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->c:Lcom/google/android/apps/gmm/place/r;

    .line 60
    new-instance v0, Lcom/google/android/apps/gmm/place/q;

    iget-object v1, p2, Lcom/google/android/apps/gmm/place/af;->b:Lcom/google/android/apps/gmm/base/l/j;

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/gmm/place/q;-><init>(Lcom/google/android/apps/gmm/base/l/j;Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->d:Lcom/google/android/apps/gmm/place/q;

    .line 61
    invoke-static {p0, p2}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 62
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/apps/gmm/place/af;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->c:Lcom/google/android/apps/gmm/place/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/r;->a()V

    .line 85
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->k()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->setAddress(Ljava/util/List;)V

    .line 86
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/place/PlacePageView;->a(Z)V

    .line 98
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/place/af;->a(Z)V

    .line 99
    sget-object v0, Lcom/google/android/apps/gmm/place/c/j;->a:Lcom/google/android/libraries/curvular/bk;

    .line 100
    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_0

    .line 102
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/af;->J()V

    .line 107
    if-nez p1, :cond_1

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/af;->K()V

    .line 110
    :cond_1
    return-void

    .line 98
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ad_()V
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/google/android/apps/gmm/place/t;->a:Lcom/google/android/libraries/curvular/bk;

    .line 91
    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    .line 92
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/view/View;)V

    .line 93
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/google/android/apps/gmm/place/c/j;->d:Lcom/google/android/libraries/curvular/bk;

    .line 115
    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 116
    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->e:I

    .line 121
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->e:I

    return v0
.end method

.method public final e()Lcom/google/android/apps/gmm/place/af;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 132
    invoke-super {p0}, Lcom/google/android/apps/gmm/place/PlacePageView;->onAttachedToWindow()V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->c:Lcom/google/android/apps/gmm/place/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/r;->b()V

    .line 134
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->d:Lcom/google/android/apps/gmm/place/q;

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/q;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;)V

    .line 135
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 33
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/place/PlacePageView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->c:Lcom/google/android/apps/gmm/place/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/r;->c()V

    .line 128
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 139
    invoke-super {p0}, Lcom/google/android/apps/gmm/place/PlacePageView;->onDetachedFromWindow()V

    .line 140
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->c:Lcom/google/android/apps/gmm/place/r;

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/r;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 141
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;->d:Lcom/google/android/apps/gmm/place/q;

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/q;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 142
    return-void
.end method

.method public final setAddress(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    const/16 v0, 0xa

    new-instance v1, Lcom/google/b/a/ab;

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/b/a/ab;->a()Lcom/google/b/a/ab;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2, v1}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 67
    sget-object v1, Lcom/google/android/apps/gmm/place/c/j;->a:Lcom/google/android/libraries/curvular/bk;

    .line 68
    invoke-static {p0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v1

    .line 69
    if-eqz v1, :cond_0

    .line 70
    sget v2, Lcom/google/android/apps/gmm/l;->cd:I

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/place/g;->a(Landroid/view/View;Ljava/lang/CharSequence;I)V

    .line 73
    :cond_0
    return-void
.end method
