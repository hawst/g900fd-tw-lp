.class public Lcom/google/android/apps/gmm/map/internal/c/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/bo;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/internal/c/bt;

.field private final b:Lcom/google/android/apps/gmm/map/b/a/ai;

.field private final c:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)V
    .locals 1

    .prologue
    .line 22
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/internal/c/l;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;I)V

    .line 23
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;I)V
    .locals 9

    .prologue
    const-wide/16 v4, -0x1

    .line 26
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v6, v4

    move v8, p3

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/l;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;JJI)V

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;JJI)V
    .locals 11

    .prologue
    .line 33
    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/internal/c/l;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;JJII)V

    .line 35
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;JJII)V
    .locals 13

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/l;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 41
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/c/l;->c:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 42
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/bt;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/c/l;->c:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/l;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    const/4 v10, 0x1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move/from16 v11, p8

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/gmm/map/internal/c/bt;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJZI)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/l;->a:Lcom/google/android/apps/gmm/map/internal/c/bt;

    .line 44
    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/l;->d:I

    .line 45
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/l;->c:Lcom/google/android/apps/gmm/map/internal/c/bp;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/map/b/a/ai;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/l;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/l;->d:I

    return v0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/internal/c/bt;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/l;->a:Lcom/google/android/apps/gmm/map/internal/c/bt;

    return-object v0
.end method
