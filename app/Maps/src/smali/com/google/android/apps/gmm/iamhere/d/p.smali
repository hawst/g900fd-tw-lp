.class public Lcom/google/android/apps/gmm/iamhere/d/p;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field static final b:I

.field static e:Z


# instance fields
.field final c:Landroid/app/NotificationManager;

.field d:Lcom/google/android/apps/gmm/iamhere/c/a;

.field private final f:Landroid/content/BroadcastReceiver;

.field private final g:Ljava/lang/Runnable;

.field private final h:Landroid/os/Handler;

.field private final i:Landroid/content/Context;

.field private final j:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

.field private final k:Lcom/google/android/apps/gmm/shared/c/f;

.field private final l:Lcom/google/android/apps/gmm/iamhere/d/u;

.field private final m:Lcom/google/android/apps/gmm/z/a/b;

.field private final n:Lcom/google/android/apps/gmm/iamhere/d/m;

.field private final o:Lcom/google/android/apps/gmm/iamhere/d/y;

.field private final p:Lcom/google/android/apps/gmm/iamhere/d/t;

.field private final q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/a;",
            "Lcom/google/android/apps/gmm/iamhere/c/h;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private s:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    const-class v0, Lcom/google/android/apps/gmm/iamhere/d/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/p;->a:Ljava/lang/String;

    .line 108
    const v0, -0x21524111

    sput v0, Lcom/google/android/apps/gmm/iamhere/d/p;->b:I

    .line 135
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/gmm/iamhere/d/p;->e:Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/app/NotificationManager;Lcom/google/android/apps/gmm/map/internal/d/c/a/g;Lcom/google/android/apps/gmm/shared/c/f;Landroid/os/Handler;Lcom/google/android/apps/gmm/iamhere/d/u;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/iamhere/d/y;)V
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/q;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/q;-><init>(Lcom/google/android/apps/gmm/iamhere/d/p;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->f:Landroid/content/BroadcastReceiver;

    .line 110
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/r;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/r;-><init>(Lcom/google/android/apps/gmm/iamhere/d/p;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->g:Ljava/lang/Runnable;

    .line 129
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    .line 130
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->r:Ljava/util/Set;

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->s:Z

    .line 146
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->i:Landroid/content/Context;

    .line 147
    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->c:Landroid/app/NotificationManager;

    .line 148
    iput-object p3, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->j:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    .line 149
    iput-object p4, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->k:Lcom/google/android/apps/gmm/shared/c/f;

    .line 150
    iput-object p5, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->h:Landroid/os/Handler;

    .line 151
    iput-object p6, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->l:Lcom/google/android/apps/gmm/iamhere/d/u;

    .line 152
    iput-object p7, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->m:Lcom/google/android/apps/gmm/z/a/b;

    .line 153
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/m;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/iamhere/d/m;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->n:Lcom/google/android/apps/gmm/iamhere/d/m;

    .line 154
    iput-object p8, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->o:Lcom/google/android/apps/gmm/iamhere/d/y;

    .line 156
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/t;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/iamhere/d/t;-><init>(Lcom/google/android/apps/gmm/iamhere/d/p;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->p:Lcom/google/android/apps/gmm/iamhere/d/t;

    .line 157
    return-void
.end method

.method private a(Lcom/google/b/a/ar;J)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/a/ar",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/j;",
            ">;J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 378
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/iamhere/c/h;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/apps/gmm/iamhere/c/h;->a(J)Lcom/google/android/apps/gmm/iamhere/c/j;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/google/b/a/ar;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 379
    new-instance v4, Lcom/google/android/apps/gmm/iamhere/c/l;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/iamhere/c/a;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/h;

    invoke-direct {v4, v1, v0}, Lcom/google/android/apps/gmm/iamhere/c/l;-><init>(Lcom/google/android/apps/gmm/iamhere/c/a;Lcom/google/android/apps/gmm/iamhere/c/h;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 382
    :cond_3
    return-object v2
.end method


# virtual methods
.method final declared-synchronized a()Lcom/google/android/apps/gmm/iamhere/c/n;
    .locals 4

    .prologue
    .line 178
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 179
    new-instance v3, Lcom/google/android/apps/gmm/iamhere/c/l;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/iamhere/c/a;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/h;

    invoke-direct {v3, v1, v0}, Lcom/google/android/apps/gmm/iamhere/c/l;-><init>(Lcom/google/android/apps/gmm/iamhere/c/a;Lcom/google/android/apps/gmm/iamhere/c/h;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/iamhere/d/p;->a(Lcom/google/android/apps/gmm/iamhere/c/l;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 181
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/n;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->r:Ljava/util/Set;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/c/n;-><init>(Ljava/util/Map;Ljava/util/Set;)V

    .line 184
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 185
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->c:Landroid/app/NotificationManager;

    sget v2, Lcom/google/android/apps/gmm/iamhere/d/p;->b:I

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 186
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->p:Lcom/google/android/apps/gmm/iamhere/d/t;

    iget-object v2, v1, Lcom/google/android/apps/gmm/iamhere/d/t;->b:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->r:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 190
    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/p;->a:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 191
    monitor-exit p0

    return-object v0
.end method

.method declared-synchronized a(Lcom/google/android/apps/gmm/iamhere/c/a;)V
    .locals 12

    .prologue
    .line 320
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->k:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v7

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/h;

    .line 323
    if-nez v0, :cond_0

    .line 325
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/h;->a:Lcom/google/android/apps/gmm/iamhere/c/h;

    move-object v10, v0

    .line 327
    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/h;

    iget-object v1, v10, Lcom/google/android/apps/gmm/iamhere/c/h;->d:Lcom/google/android/apps/gmm/iamhere/c/r;

    iget-wide v2, v10, Lcom/google/android/apps/gmm/iamhere/c/h;->e:J

    iget-wide v4, v10, Lcom/google/android/apps/gmm/iamhere/c/h;->f:J

    iget-object v6, v10, Lcom/google/android/apps/gmm/iamhere/c/h;->g:Ljava/lang/Object;

    iget-object v9, v10, Lcom/google/android/apps/gmm/iamhere/c/h;->i:Lcom/google/b/c/dn;

    iget-wide v10, v10, Lcom/google/android/apps/gmm/iamhere/c/h;->j:J

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/gmm/iamhere/c/h;-><init>(Lcom/google/android/apps/gmm/iamhere/c/r;JJLjava/lang/Object;JLcom/google/b/c/dn;J)V

    .line 328
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->r:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 330
    sget-object v3, Lcom/google/j/d/a/w;->j:Lcom/google/j/d/a/w;

    sget-object v4, Lcom/google/b/f/t;->aX:Lcom/google/b/f/t;

    new-instance v1, Lcom/google/android/apps/gmm/iamhere/c/l;

    invoke-direct {v1, p1, v0}, Lcom/google/android/apps/gmm/iamhere/c/l;-><init>(Lcom/google/android/apps/gmm/iamhere/c/a;Lcom/google/android/apps/gmm/iamhere/c/h;)V

    .line 333
    invoke-static {v1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v5

    .line 330
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->l:Lcom/google/android/apps/gmm/iamhere/d/u;

    sget-object v2, Lcom/google/j/d/a/e;->g:Lcom/google/j/d/a/e;

    sget-object v6, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/gmm/iamhere/d/u;->a(Lcom/google/j/d/a/e;Lcom/google/j/d/a/w;Lcom/google/b/f/t;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/j;)Z

    .line 335
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->d:Lcom/google/android/apps/gmm/iamhere/c/a;

    .line 336
    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/p;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->r:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1a

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Dismissed #"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 337
    monitor-exit p0

    return-void

    .line 320
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move-object v10, v0

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/iamhere/c/l;)V
    .locals 16

    .prologue
    .line 225
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 226
    :cond_0
    :try_start_1
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/apps/gmm/iamhere/c/l;->b:Lcom/google/android/apps/gmm/iamhere/c/h;

    .line 227
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/iamhere/c/l;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/h;

    move-object v14, v0

    .line 229
    if-eqz v14, :cond_2

    .line 232
    iget-object v2, v14, Lcom/google/android/apps/gmm/iamhere/c/h;->i:Lcom/google/b/c/dn;

    iget-object v3, v15, Lcom/google/android/apps/gmm/iamhere/c/h;->i:Lcom/google/b/c/dn;

    invoke-static {v2, v3}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Lcom/google/android/apps/gmm/iamhere/c/h;

    iget-object v3, v14, Lcom/google/android/apps/gmm/iamhere/c/h;->d:Lcom/google/android/apps/gmm/iamhere/c/r;

    iget-wide v4, v14, Lcom/google/android/apps/gmm/iamhere/c/h;->e:J

    iget-wide v6, v14, Lcom/google/android/apps/gmm/iamhere/c/h;->f:J

    iget-object v8, v14, Lcom/google/android/apps/gmm/iamhere/c/h;->g:Ljava/lang/Object;

    iget-wide v9, v14, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    invoke-static {}, Lcom/google/b/c/dn;->g()Lcom/google/b/c/dn;

    move-result-object v11

    iget-wide v12, v15, Lcom/google/android/apps/gmm/iamhere/c/h;->e:J

    invoke-direct/range {v2 .. v13}, Lcom/google/android/apps/gmm/iamhere/c/h;-><init>(Lcom/google/android/apps/gmm/iamhere/c/r;JJLjava/lang/Object;JLcom/google/b/c/dn;J)V

    .line 233
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/iamhere/c/l;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    :goto_1
    sget-object v3, Lcom/google/android/apps/gmm/iamhere/d/p;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/iamhere/c/l;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x10

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Remove "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " : "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " + "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    monitor-exit p0

    return-void

    .line 232
    :cond_1
    :try_start_2
    new-instance v2, Lcom/google/android/apps/gmm/iamhere/c/h;

    iget-object v3, v14, Lcom/google/android/apps/gmm/iamhere/c/h;->d:Lcom/google/android/apps/gmm/iamhere/c/r;

    iget-wide v4, v14, Lcom/google/android/apps/gmm/iamhere/c/h;->e:J

    iget-wide v6, v14, Lcom/google/android/apps/gmm/iamhere/c/h;->f:J

    iget-object v8, v14, Lcom/google/android/apps/gmm/iamhere/c/h;->g:Ljava/lang/Object;

    iget-wide v9, v14, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    invoke-static {v11}, Lcom/google/b/c/dn;->a(Ljava/util/Collection;)Lcom/google/b/c/dn;

    move-result-object v11

    const-wide/16 v12, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/google/android/apps/gmm/iamhere/c/h;-><init>(Lcom/google/android/apps/gmm/iamhere/c/r;JJLjava/lang/Object;JLcom/google/b/c/dn;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :cond_2
    move-object v2, v15

    goto/16 :goto_1
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/iamhere/c/n;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/gmm/iamhere/c/n;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 163
    monitor-enter p0

    if-eqz p1, :cond_1

    .line 164
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v2

    iget-object v0, p1, Lcom/google/android/apps/gmm/iamhere/c/n;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/l;

    iget-object v4, v0, Lcom/google/android/apps/gmm/iamhere/c/l;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/l;->b:Lcom/google/android/apps/gmm/iamhere/c/h;

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 164
    :cond_0
    :try_start_1
    invoke-interface {v1, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->r:Ljava/util/Set;

    iget-object v1, p1, Lcom/google/android/apps/gmm/iamhere/c/n;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->c:Landroid/app/NotificationManager;

    sget v1, Lcom/google/android/apps/gmm/iamhere/d/p;->b:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->p:Lcom/google/android/apps/gmm/iamhere/d/t;

    iget-object v1, v0, Lcom/google/android/apps/gmm/iamhere/d/t;->b:Landroid/content/Context;

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/d/t;->a:Landroid/content/IntentFilter;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->i:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->f:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171
    monitor-exit p0

    return-void
.end method

.method public final a(Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V
    .locals 6
    .param p1    # Lcom/google/j/d/a/w;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/b/f/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/d/p;->d()Ljava/util/List;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->l:Lcom/google/android/apps/gmm/iamhere/d/u;

    sget-object v1, Lcom/google/j/d/a/e;->g:Lcom/google/j/d/a/e;

    sget-object v5, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/iamhere/d/u;->a(Lcom/google/j/d/a/e;Lcom/google/j/d/a/w;Lcom/google/b/f/t;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/j;)Z

    .line 299
    return-void
.end method

.method public final declared-synchronized b()V
    .locals 12

    .prologue
    const-wide v6, 0x7fffffffffffffffL

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 242
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/p;->a:Ljava/lang/String;

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->k:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v8

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/h;

    invoke-virtual {v0, v8, v9}, Lcom/google/android/apps/gmm/iamhere/c/h;->a(J)Lcom/google/android/apps/gmm/iamhere/c/j;

    move-result-object v0

    sget-object v4, Lcom/google/android/apps/gmm/iamhere/c/j;->d:Lcom/google/android/apps/gmm/iamhere/c/j;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/iamhere/c/j;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 245
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->r:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/h;

    if-eqz v0, :cond_3

    invoke-virtual {v0, v8, v9}, Lcom/google/android/apps/gmm/iamhere/c/h;->a(J)Lcom/google/android/apps/gmm/iamhere/c/j;

    move-result-object v0

    sget-object v4, Lcom/google/android/apps/gmm/iamhere/c/j;->b:Lcom/google/android/apps/gmm/iamhere/c/j;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/iamhere/c/j;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 247
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->h:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v4, v6

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/h;

    invoke-virtual {v0, v8, v9}, Lcom/google/android/apps/gmm/iamhere/c/h;->b(J)J

    move-result-wide v10

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    goto :goto_2

    :cond_5
    cmp-long v0, v4, v8

    if-lez v0, :cond_8

    .line 249
    :goto_3
    cmp-long v0, v4, v6

    if-eqz v0, :cond_6

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->h:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->g:Ljava/lang/Runnable;

    sub-long/2addr v4, v8

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 253
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->r:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    :goto_4
    if-eqz v0, :cond_a

    .line 254
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/p;->a:Ljava/lang/String;

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->c:Landroid/app/NotificationManager;

    sget v1, Lcom/google/android/apps/gmm/iamhere/d/p;->b:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 280
    :cond_7
    :goto_5
    monitor-exit p0

    return-void

    :cond_8
    move-wide v4, v6

    .line 248
    goto :goto_3

    :cond_9
    move v0, v2

    .line 253
    goto :goto_4

    .line 262
    :cond_a
    :try_start_2
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/j;->a:Lcom/google/android/apps/gmm/iamhere/c/j;

    invoke-static {v0}, Lcom/google/b/a/as;->a(Ljava/lang/Object;)Lcom/google/b/a/ar;

    move-result-object v0

    invoke-direct {p0, v0, v8, v9}, Lcom/google/android/apps/gmm/iamhere/d/p;->a(Lcom/google/b/a/ar;J)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/iamhere/d/ax;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 263
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 264
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/d/p;->c()V

    .line 265
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->d:Lcom/google/android/apps/gmm/iamhere/c/a;

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->c:Landroid/app/NotificationManager;

    sget v1, Lcom/google/android/apps/gmm/iamhere/d/p;->b:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 267
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/p;->a:Ljava/lang/String;

    goto :goto_5

    .line 271
    :cond_b
    const/4 v0, 0x0

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/l;

    iget-object v3, v0, Lcom/google/android/apps/gmm/iamhere/c/l;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    .line 272
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_c

    move v0, v1

    .line 273
    :goto_6
    iget-object v5, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->d:Lcom/google/android/apps/gmm/iamhere/c/a;

    if-eqz v5, :cond_d

    iget-object v5, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->d:Lcom/google/android/apps/gmm/iamhere/c/a;

    invoke-virtual {v5, v3}, Lcom/google/android/apps/gmm/iamhere/c/a;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->s:Z

    if-ne v5, v0, :cond_d

    .line 274
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/p;->a:Ljava/lang/String;

    goto :goto_5

    :cond_c
    move v0, v2

    .line 272
    goto :goto_6

    .line 278
    :cond_d
    iput-object v3, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->d:Lcom/google/android/apps/gmm/iamhere/c/a;

    .line 279
    iget-object v5, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->d:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->o:Lcom/google/android/apps/gmm/iamhere/d/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/d/y;->c()Z

    move-result v0

    if-nez v0, :cond_e

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/p;->a:Ljava/lang/String;

    goto :goto_5

    :cond_e
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/p;->a:Ljava/lang/String;

    const-string v0, "Showing notification "

    iget-object v3, v5, Lcom/google/android/apps/gmm/iamhere/c/a;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_f

    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->q:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/h;

    new-instance v3, Landroid/support/v4/app/ax;

    iget-object v6, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->i:Landroid/content/Context;

    invoke-direct {v3, v6}, Landroid/support/v4/app/ax;-><init>(Landroid/content/Context;)V

    sget v6, Lcom/google/android/apps/gmm/f;->bx:I

    invoke-virtual {v3, v6}, Landroid/support/v4/app/ax;->a(I)Landroid/support/v4/app/ax;

    move-result-object v3

    iget-object v6, v5, Lcom/google/android/apps/gmm/iamhere/c/a;->b:Ljava/lang/String;

    invoke-virtual {v3, v6}, Landroid/support/v4/app/ax;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/ax;

    move-result-object v3

    iget-wide v6, v0, Lcom/google/android/apps/gmm/iamhere/c/h;->f:J

    invoke-virtual {v3, v6, v7}, Landroid/support/v4/app/ax;->a(J)Landroid/support/v4/app/ax;

    move-result-object v6

    new-instance v0, Landroid/support/v4/app/av;

    invoke-direct {v0}, Landroid/support/v4/app/av;-><init>()V

    iget-object v3, v5, Lcom/google/android/apps/gmm/iamhere/c/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/support/v4/app/av;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/av;

    move-result-object v7

    move v3, v2

    :goto_8
    iget-object v0, v5, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-ge v3, v0, :cond_11

    iget-object v0, v5, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    invoke-virtual {v0, v3}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/d;

    iget-object v8, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->n:Lcom/google/android/apps/gmm/iamhere/d/m;

    sget-object v9, Lcom/google/android/apps/gmm/iamhere/d/o;->b:Lcom/google/android/apps/gmm/iamhere/d/o;

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/iamhere/d/a;->a(Lcom/google/android/apps/gmm/iamhere/c/d;I)Lcom/google/b/f/t;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v0, v10, v11}, Lcom/google/android/apps/gmm/iamhere/d/m;->a(Lcom/google/android/apps/gmm/iamhere/d/o;Lcom/google/android/apps/gmm/iamhere/c/d;Lcom/google/b/f/t;Z)Landroid/content/Intent;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->i:Landroid/content/Context;

    const/4 v10, 0x0

    const/high16 v11, 0x8000000

    invoke-static {v9, v10, v8, v11}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    if-nez v3, :cond_10

    invoke-virtual {v6, v8}, Landroid/support/v4/app/ax;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/ax;

    move-result-object v8

    iget-object v9, v0, Lcom/google/android/apps/gmm/iamhere/c/d;->b:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/support/v4/app/ax;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/d;->b:Ljava/lang/String;

    invoke-virtual {v7, v0}, Landroid/support/v4/app/av;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/av;

    :goto_9
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_8

    :cond_f
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_7

    :cond_10
    new-instance v9, Landroid/support/v4/app/at;

    invoke-static {v0}, Lcom/google/android/apps/gmm/iamhere/d/a;->a(Lcom/google/android/apps/gmm/iamhere/c/d;)I

    move-result v10

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/d;->b:Ljava/lang/String;

    invoke-direct {v9, v10, v0, v8}, Landroid/support/v4/app/at;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v6, v9}, Landroid/support/v4/app/ax;->a(Landroid/support/v4/app/at;)Landroid/support/v4/app/ax;

    goto :goto_9

    :cond_11
    sget-boolean v0, Lcom/google/android/apps/gmm/iamhere/d/p;->e:Z

    if-eqz v0, :cond_13

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_13

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->s:Z

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->s:Z

    if-eqz v0, :cond_12

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->n:Lcom/google/android/apps/gmm/iamhere/d/m;

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/d/o;->c:Lcom/google/android/apps/gmm/iamhere/d/o;

    const/4 v0, 0x0

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/l;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/d;

    sget-object v3, Lcom/google/b/f/t;->aZ:Lcom/google/b/f/t;

    const/4 v8, 0x0

    invoke-virtual {v1, v2, v0, v3, v8}, Lcom/google/android/apps/gmm/iamhere/d/m;->a(Lcom/google/android/apps/gmm/iamhere/d/o;Lcom/google/android/apps/gmm/iamhere/c/d;Lcom/google/b/f/t;Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->i:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/at;

    sget v2, Lcom/google/android/apps/gmm/f;->cg:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->i:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v8, -0x21524111

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v10, v11

    invoke-virtual {v3, v8, v9, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Landroid/support/v4/app/at;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v6, v1}, Landroid/support/v4/app/ax;->a(Landroid/support/v4/app/at;)Landroid/support/v4/app/ax;

    :cond_12
    invoke-static {v5}, Lcom/google/android/apps/gmm/iamhere/d/t;->a(Lcom/google/android/apps/gmm/iamhere/c/a;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->i:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/support/v4/app/ax;->b(Landroid/app/PendingIntent;)Landroid/support/v4/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->c:Landroid/app/NotificationManager;

    sget v1, Lcom/google/android/apps/gmm/iamhere/d/p;->b:I

    invoke-virtual {v6}, Landroid/support/v4/app/ax;->a()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/d/p;->c()V

    iget-object v0, v5, Lcom/google/android/apps/gmm/iamhere/c/a;->c:Landroid/net/Uri;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/s;

    invoke-direct {v0, p0, v5, v6, v7}, Lcom/google/android/apps/gmm/iamhere/d/s;-><init>(Lcom/google/android/apps/gmm/iamhere/d/p;Lcom/google/android/apps/gmm/iamhere/c/a;Landroid/support/v4/app/ax;Landroid/support/v4/app/av;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->j:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    iget-object v2, v5, Lcom/google/android/apps/gmm/iamhere/c/a;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/k;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_5

    :cond_13
    move v0, v2

    goto/16 :goto_a
.end method

.method c()V
    .locals 6

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->d:Lcom/google/android/apps/gmm/iamhere/c/a;

    if-eqz v0, :cond_1

    .line 289
    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->aX:Lcom/google/b/f/t;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/d/p;->d()Ljava/util/List;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->l:Lcom/google/android/apps/gmm/iamhere/d/u;

    sget-object v1, Lcom/google/j/d/a/e;->g:Lcom/google/j/d/a/e;

    sget-object v5, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/iamhere/d/u;->a(Lcom/google/j/d/a/e;Lcom/google/j/d/a/w;Lcom/google/b/f/t;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/j;)Z

    .line 291
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->d:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->d:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/d;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->m:Lcom/google/android/apps/gmm/z/a/b;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/iamhere/d/a;->a(Lcom/google/android/apps/gmm/iamhere/c/d;I)Lcom/google/b/f/t;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/l;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->m:Lcom/google/android/apps/gmm/z/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/z/a/b;->a()V

    .line 293
    :cond_1
    return-void
.end method

.method public final declared-synchronized d()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 343
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->k:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v0

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/j;->a:Lcom/google/android/apps/gmm/iamhere/c/j;

    invoke-static {v2}, Lcom/google/b/a/as;->a(Ljava/lang/Object;)Lcom/google/b/a/ar;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/gmm/iamhere/d/p;->a(Lcom/google/b/a/ar;J)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/iamhere/d/ax;->a(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 350
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/j;->a:Lcom/google/android/apps/gmm/iamhere/c/j;

    .line 351
    invoke-static {v0}, Lcom/google/b/a/as;->a(Ljava/lang/Object;)Lcom/google/b/a/ar;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/as;->a(Lcom/google/b/a/ar;)Lcom/google/b/a/ar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/p;->k:Lcom/google/android/apps/gmm/shared/c/f;

    .line 352
    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    .line 351
    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/gmm/iamhere/d/p;->a(Lcom/google/b/a/ar;J)Ljava/util/ArrayList;

    move-result-object v0

    .line 350
    invoke-static {v0}, Lcom/google/android/apps/gmm/iamhere/d/ax;->a(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
