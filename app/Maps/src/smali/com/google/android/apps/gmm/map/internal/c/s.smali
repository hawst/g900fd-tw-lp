.class public Lcom/google/android/apps/gmm/map/internal/c/s;
.super Lcom/google/android/apps/gmm/map/internal/c/cd;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/cd;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/c/cd;-><init>(Lcom/google/android/apps/gmm/map/internal/c/cd;)V

    .line 12
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can\'t add tile parameter to this collection."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bv;)V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can\'t remove tile parameter from this collection."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
