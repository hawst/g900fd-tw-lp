.class public Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/activities/y;
.implements Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;
.implements Lcom/google/android/apps/gmm/directions/bl;
.implements Lcom/google/android/apps/gmm/l/av;


# static fields
.field static final a:Ljava/lang/String;

.field private static final y:Lcom/google/b/a/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/ar",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/w;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:I

.field public c:Lcom/google/android/apps/gmm/map/r/a/f;

.field public d:Z

.field public e:Z

.field public f:Lcom/google/android/apps/gmm/map/r/a/e;

.field public g:[Lcom/google/android/apps/gmm/map/r/a/w;

.field public m:Lcom/google/maps/g/a/hm;

.field public n:Lcom/google/android/apps/gmm/map/r/a/ap;

.field public o:Lcom/google/android/apps/gmm/map/r/a/ap;

.field public p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

.field public q:Landroid/view/ViewGroup;

.field r:Ljava/lang/String;

.field private final s:Ljava/lang/Object;

.field private t:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/c;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcom/google/android/apps/gmm/base/l/j;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private v:Lcom/google/android/libraries/curvular/ae;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/base/l/j;",
            ">;"
        }
    .end annotation
.end field

.field private w:Z

.field private x:Lcom/google/android/apps/gmm/directions/bj;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const-class v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->a:Ljava/lang/String;

    .line 212
    new-instance v0, Lcom/google/android/apps/gmm/directions/j;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/j;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->y:Lcom/google/b/a/ar;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 131
    new-instance v0, Lcom/google/android/apps/gmm/directions/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/directions/g;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->s:Ljava/lang/Object;

    .line 197
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->w:Z

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/map/r/a/f;ILjava/lang/String;Landroid/app/Fragment$SavedState;ZZ)Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;
    .locals 3
    .param p4    # Landroid/app/Fragment$SavedState;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 243
    new-instance v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;-><init>()V

    .line 245
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 246
    const-string v2, "storageItem"

    invoke-virtual {p0, v1, v2, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 248
    const-string v2, "tripIndex"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 249
    const-string v2, "entryPointFragmentTransitionName"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const-string v2, "previousDSPState"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 255
    const-string v2, "autoLaunchNav"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 256
    const-string v2, "showFromMyLocation"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 258
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->setArguments(Landroid/os/Bundle;)V

    .line 260
    return-object v0
.end method

.method private a(Z)Lcom/google/android/apps/gmm/z/b/l;
    .locals 6

    .prologue
    .line 551
    if-eqz p1, :cond_1

    sget-object v0, Lcom/google/b/f/t;->aA:Lcom/google/b/f/t;

    move-object v1, v0

    .line 554
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->f:Lcom/google/android/apps/gmm/map/r/a/e;

    iget v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    .line 555
    if-ltz v3, :cond_0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v0, v0

    if-gt v0, v3, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/f/d/h;->d(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 554
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 556
    iput-object v2, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 557
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/f;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    .line 558
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0

    .line 551
    :cond_1
    sget-object v0, Lcom/google/b/f/t;->ay:Lcom/google/b/f/t;

    move-object v1, v0

    goto :goto_0

    .line 555
    :cond_2
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, v3

    if-nez v0, :cond_3

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    new-instance v5, Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-direct {v5, v0, v2}, Lcom/google/android/apps/gmm/map/r/a/ao;-><init>(Lcom/google/maps/g/a/hu;Lcom/google/android/apps/gmm/map/r/a/e;)V

    aput-object v5, v4, v3

    :cond_3
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, v3

    goto :goto_1
.end method

.method private a(IZ)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    .line 833
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    .line 834
    if-eqz v0, :cond_2

    .line 835
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->g:[Lcom/google/android/apps/gmm/map/r/a/w;

    aget-object v2, v2, p1

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 836
    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/directions/f/a/b;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/directions/f/a/b;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->g:[Lcom/google/android/apps/gmm/map/r/a/w;

    .line 838
    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(I[Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/map/r/a/ae;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/directions/f/a/b;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 839
    sget-object v5, Lcom/google/android/apps/gmm/directions/i;->a:[I

    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->f:Lcom/google/android/apps/gmm/map/r/a/e;

    if-ltz p1, :cond_0

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v0, v0

    if-gt v0, p1, :cond_3

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v6, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v6, :cond_5

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_1
    iget v0, v0, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    :cond_1
    invoke-virtual {v0}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/gmm/map/i/u;->a:Lcom/google/android/apps/gmm/map/i/u;

    :goto_2
    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/directions/f/a/b;->a(Lcom/google/android/apps/gmm/map/i/s;)Lcom/google/android/apps/gmm/directions/f/a/b;

    move-result-object v4

    .line 840
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/f;->a()Lcom/google/maps/g/a/hm;

    move-result-object v0

    sget-object v5, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-eq v0, v5, :cond_6

    move v0, v1

    :goto_3
    iput-boolean v0, v4, Lcom/google/android/apps/gmm/directions/f/a/b;->f:Z

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v2, v0

    .line 841
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, v4, Lcom/google/android/apps/gmm/directions/f/a/b;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 842
    iput-boolean p2, v4, Lcom/google/android/apps/gmm/directions/f/a/b;->d:Z

    .line 843
    iput-boolean v1, v4, Lcom/google/android/apps/gmm/directions/f/a/b;->g:Z

    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/v;->a:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 844
    iput-object v0, v4, Lcom/google/android/apps/gmm/directions/f/a/b;->l:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 845
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/a/a;

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/directions/f/a/a;-><init>(Lcom/google/android/apps/gmm/directions/f/a/b;)V

    .line 836
    invoke-interface {v3, v0}, Lcom/google/android/apps/gmm/directions/a/a;->a(Lcom/google/android/apps/gmm/directions/f/a/a;)V

    .line 847
    :cond_2
    return-void

    .line 839
    :cond_3
    iget-object v0, v6, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, p1

    if-nez v0, :cond_4

    iget-object v7, v6, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    new-instance v8, Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-direct {v8, v0, v6}, Lcom/google/android/apps/gmm/map/r/a/ao;-><init>(Lcom/google/maps/g/a/hu;Lcom/google/android/apps/gmm/map/r/a/e;)V

    aput-object v8, v7, p1

    :cond_4
    iget-object v0, v6, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, p1

    goto :goto_0

    :cond_5
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_1

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/map/i/r;->a:Lcom/google/android/apps/gmm/map/i/r;

    goto :goto_2

    .line 840
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 839
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;)V
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->a(Lcom/google/android/apps/gmm/directions/s;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;I)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;Lcom/google/android/apps/gmm/map/j/w;)V
    .locals 4

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v2

    const/4 v0, 0x1

    const-class v3, Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lcom/google/android/apps/gmm/map/j/w;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    const/4 v0, 0x2

    const-class v3, Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lcom/google/android/apps/gmm/map/j/w;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    const/4 v0, 0x0

    const-class v1, Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/gmm/map/j/w;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->b:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b(I)V

    return-void
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 876
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 878
    iget v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    if-ne v0, p1, :cond_1

    .line 894
    :cond_0
    :goto_0
    return-void

    .line 882
    :cond_1
    iput p1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    .line 884
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->b()I

    move-result v0

    if-eq v0, p1, :cond_2

    .line 885
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->setCurrentItem(I)V

    .line 888
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->a(IZ)V

    .line 891
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->u:Lcom/google/android/apps/gmm/base/l/j;

    if-eqz v0, :cond_0

    .line 892
    invoke-direct {p0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->i()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;)Z
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->l()Z

    move-result v0

    return v0
.end method

.method private i()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 530
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->m:Lcom/google/maps/g/a/hm;

    sget-object v3, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->g:[Lcom/google/android/apps/gmm/map/r/a/w;

    iget v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    aget-object v0, v0, v3

    .line 531
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->n:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    .line 530
    invoke-static {v3, v4, v0}, Lcom/google/android/apps/gmm/directions/g/c;->a(Lcom/google/android/apps/gmm/map/r/a/ao;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/p/b/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 532
    :goto_0
    if-eqz v0, :cond_1

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->u:Lcom/google/android/apps/gmm/base/l/j;

    sget v1, Lcom/google/android/apps/gmm/f;->ew:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/l/j;->a(I)V

    .line 534
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->u:Lcom/google/android/apps/gmm/base/l/j;

    sget v1, Lcom/google/android/apps/gmm/l;->av:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/l/j;->a(Ljava/lang/String;)V

    .line 535
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->u:Lcom/google/android/apps/gmm/base/l/j;

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->a(Z)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/l/j;->a(Lcom/google/android/apps/gmm/z/b/l;)V

    .line 541
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->v:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->u:Lcom/google/android/apps/gmm/base/l/j;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 542
    return-void

    :cond_0
    move v0, v2

    .line 530
    goto :goto_0

    .line 537
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->u:Lcom/google/android/apps/gmm/base/l/j;

    sget v2, Lcom/google/android/apps/gmm/f;->cB:I

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/base/l/j;->a(I)V

    .line 538
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->u:Lcom/google/android/apps/gmm/base/l/j;

    sget v2, Lcom/google/android/apps/gmm/l;->aw:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/base/l/j;->a(Ljava/lang/String;)V

    .line 539
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->u:Lcom/google/android/apps/gmm/base/l/j;

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->a(Z)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/l/j;->a(Lcom/google/android/apps/gmm/z/b/l;)V

    goto :goto_1
.end method


# virtual methods
.method public final J_()Landroid/net/Uri;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 908
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->m:Lcom/google/maps/g/a/hm;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->n:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->o:Lcom/google/android/apps/gmm/map/r/a/ap;

    sget-object v3, Lcom/google/android/apps/gmm/l/aa;->b:Lcom/google/android/apps/gmm/l/aa;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/l/e;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/l/aa;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 642
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 645
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->w:Z

    if-nez v0, :cond_1

    move v0, v1

    .line 646
    :goto_0
    iget v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->a(IZ)V

    .line 650
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    new-instance v2, Lcom/google/android/apps/gmm/directions/o;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/directions/o;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->setOnPageChangeListener(Landroid/support/v4/view/bz;)V

    .line 669
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->w:Z

    .line 670
    return-void

    .line 645
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IIF)V
    .locals 2

    .prologue
    .line 868
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->a(IIF)V

    .line 869
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 870
    iget v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->a(IZ)V

    .line 872
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 605
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V
    .locals 2

    .prologue
    .line 595
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->u:Lcom/google/android/apps/gmm/base/l/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->v:Lcom/google/android/libraries/curvular/ae;

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->u:Lcom/google/android/apps/gmm/base/l/j;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/l/j;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V

    .line 598
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->v:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->u:Lcom/google/android/apps/gmm/base/l/j;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 600
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    if-eqz v0, :cond_5

    .line 627
    new-instance v1, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;

    if-ne p4, v0, :cond_2

    sget-object v0, Lcom/google/r/b/a/a;->f:Lcom/google/r/b/a/a;

    :goto_0
    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    if-eq p2, p3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v2, Lcom/google/b/f/t;->aw:Lcom/google/b/f/t;

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->g:[Lcom/google/android/apps/gmm/map/r/a/w;

    iget v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    invoke-static {v3}, Lcom/google/android/apps/gmm/directions/f/d/h;->d(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v5

    iget v6, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/z/b/n;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/z/b/l;I)V

    .line 629
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p3, v0, :cond_3

    const/4 v0, 0x1

    move v2, v0

    .line 630
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->a()Landroid/support/v4/view/ag;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/u;

    move v3, v7

    :goto_2
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/b/a;->b:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    const/4 v4, 0x0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/b/a;->c:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_1

    sget v4, Lcom/google/android/apps/gmm/g;->ad:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v4, v1

    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/u;->d:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/directions/v;

    invoke-virtual {v1, v2, v4}, Lcom/google/android/apps/gmm/directions/v;->a(ZLandroid/view/View;)V

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 627
    :cond_2
    sget-object v0, Lcom/google/r/b/a/a;->h:Lcom/google/r/b/a/a;

    goto :goto_0

    :cond_3
    move v2, v7

    .line 629
    goto :goto_1

    .line 632
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_5

    .line 633
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    move v1, v7

    :goto_3
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_5

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/google/android/apps/gmm/g;->ab:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 636
    :cond_5
    return-void
.end method

.method protected final a(Lcom/google/android/apps/gmm/directions/s;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/directions/s;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 793
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->a()Landroid/support/v4/view/ag;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/u;

    iget v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/u;->d:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/directions/v;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/b/a;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0, v2}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/directions/v;->b(Lcom/google/android/apps/gmm/map/r/a/w;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 794
    if-eqz p1, :cond_0

    .line 795
    sget-object v0, Lcom/google/android/apps/gmm/directions/t;->a:Lcom/google/android/apps/gmm/directions/t;

    .line 797
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->z()Lcom/google/android/apps/gmm/navigation/b/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    iget v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/b/f;->a(Lcom/google/android/apps/gmm/map/r/a/f;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->e:Z

    .line 804
    :goto_0
    return-void

    .line 799
    :cond_1
    if-eqz p1, :cond_2

    .line 800
    sget-object v0, Lcom/google/android/apps/gmm/directions/t;->b:Lcom/google/android/apps/gmm/directions/t;

    .line 802
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/apps/gmm/map/r/a/ag;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/gmm/map/r/a/ag;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 818
    if-nez p1, :cond_0

    const/4 v0, 0x0

    move v1, v0

    .line 819
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 820
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    iget v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    .line 821
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v5

    .line 819
    invoke-static {v0, v3, v4, v1, v5}, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/map/r/a/f;IILcom/google/android/apps/gmm/o/a/c;)Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 822
    return-void

    .line 818
    :cond_0
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    move v1, v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 610
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 950
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 967
    :cond_0
    :goto_0
    return-void

    .line 955
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/e;

    iget v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->g:[Lcom/google/android/apps/gmm/map/r/a/w;

    .line 956
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(I[Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/map/r/a/ae;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/r/a/ae;)Lcom/google/r/b/a/agl;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/r/a/e;-><init>(Lcom/google/r/b/a/agl;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->f:Lcom/google/android/apps/gmm/map/r/a/e;

    .line 957
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->f:Lcom/google/android/apps/gmm/map/r/a/e;

    iget v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/g;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/r/a/g;-><init>(Lcom/google/android/apps/gmm/map/r/a/f;)V

    iput-object v1, v2, Lcom/google/android/apps/gmm/map/r/a/g;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/f;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/r/a/f;-><init>(Lcom/google/android/apps/gmm/map/r/a/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 960
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->a()Landroid/support/v4/view/ag;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/u;

    .line 961
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/u;->c()V

    .line 963
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    .line 964
    if-eqz v0, :cond_0

    .line 965
    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/a;->b()V

    goto :goto_0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 674
    sget-object v0, Lcom/google/b/f/t;->aN:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final m()Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 1

    .prologue
    .line 914
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->g:Lcom/google/android/apps/gmm/feedback/a/d;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 265
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 267
    if-nez p1, :cond_f

    .line 268
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    move-object v1, v0

    .line 270
    :goto_0
    const-string v0, "storageItem"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v3, "storageItem"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 271
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 273
    const-string v0, "tripIndex"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    iget v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/r/a/f;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->m:Lcom/google/maps/g/a/hm;

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/f;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->n:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/f;->d:Lcom/google/android/apps/gmm/map/r/a/ap;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->o:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 277
    const-string v0, "initialTransitionCompleted"

    .line 278
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->w:Z

    .line 279
    const-string v0, "entryPointFragmentTransitionName"

    .line 280
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->r:Ljava/lang/String;

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->r:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    move v0, v8

    :goto_1
    if-nez v0, :cond_7

    move v0, v8

    :goto_2
    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_2

    .line 282
    :cond_8
    const-string v0, "autoLaunchNav"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->d:Z

    .line 283
    const-string v0, "showFromMyLocation"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->e:Z

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->f:Lcom/google/android/apps/gmm/map/r/a/e;

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 292
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/f;->a()Lcom/google/maps/g/a/hm;

    move-result-object v0

    .line 291
    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/x;->a(Lcom/google/maps/g/a/hm;)Z

    move-result v0

    .line 293
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 294
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    .line 293
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(Lcom/google/android/apps/gmm/map/r/a/f;Landroid/content/Context;I)Lcom/google/android/apps/gmm/map/r/a/ae;

    move-result-object v1

    .line 295
    if-eqz v0, :cond_c

    .line 296
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/r/a/w;

    .line 297
    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->g:[Lcom/google/android/apps/gmm/map/r/a/w;

    .line 300
    new-instance v0, Lcom/google/android/apps/gmm/directions/bj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->g:[Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/apps/gmm/directions/bj;-><init>(Lcom/google/android/apps/gmm/base/activities/c;[Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/directions/bl;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->x:Lcom/google/android/apps/gmm/directions/bj;

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->m:Lcom/google/maps/g/a/hm;

    sget-object v1, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-eq v0, v1, :cond_a

    .line 303
    new-instance v0, Lcom/google/android/apps/gmm/directions/n;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v3, Lcom/google/android/apps/gmm/base/l/n;->c:Lcom/google/android/apps/gmm/base/l/n;

    sget-object v4, Lcom/google/android/apps/gmm/base/l/k;->c:Lcom/google/android/apps/gmm/base/l/k;

    sget v5, Lcom/google/android/apps/gmm/f;->ew:I

    sget v1, Lcom/google/android/apps/gmm/l;->av:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v9, -0x21524111

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/directions/n;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;Landroid/content/Context;Lcom/google/android/apps/gmm/base/l/n;Lcom/google/android/apps/gmm/base/l/k;ILjava/lang/String;Lcom/google/android/apps/gmm/z/b/l;ZI)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->u:Lcom/google/android/apps/gmm/base/l/j;

    .line 306
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->D_()Lcom/google/android/apps/gmm/map/i/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/e;->a:Lcom/google/r/b/a/agl;

    iget-object v1, v1, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    invoke-interface {v0, v1, v7}, Lcom/google/android/apps/gmm/map/i/a/a;->a(Ljava/util/Collection;Lcom/google/android/apps/gmm/map/i/a/b;)V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->x:Lcom/google/android/apps/gmm/directions/bj;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/bj;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/gmm/directions/bj;->e:J

    if-eqz p1, :cond_b

    const-string v1, "trafficRefreshedTime"

    iget-wide v2, v0, Lcom/google/android/apps/gmm/directions/bj;->e:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/gmm/directions/bj;->e:J

    .line 309
    :cond_b
    return-void

    .line 296
    :cond_c
    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->y:Lcom/google/b/a/ar;

    .line 298
    if-nez v1, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    new-instance v2, Lcom/google/b/c/ee;

    invoke-direct {v2, v1, v0}, Lcom/google/b/c/ee;-><init>(Ljava/lang/Iterable;Lcom/google/b/a/ar;)V

    const-class v0, Lcom/google/android/apps/gmm/map/r/a/w;

    .line 297
    invoke-static {v2}, Lcom/google/b/c/eb;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/b/c/in;->a(Ljava/lang/Class;I)[Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/r/a/w;

    goto/16 :goto_3

    :cond_f
    move-object v1, p1

    goto/16 :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->aN:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v1, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/directions/c/c;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    new-instance v2, Lcom/google/android/apps/gmm/directions/k;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/directions/k;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;)V

    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->t:Lcom/google/android/libraries/curvular/ae;

    .line 314
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 589
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    .line 590
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onDestroyView()V

    .line 591
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 563
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 565
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->s:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->setOnPageChangeListener(Landroid/support/v4/view/bz;)V

    .line 569
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->x:Lcom/google/android/apps/gmm/directions/bj;

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/bj;->f:Ljava/util/concurrent/ScheduledExecutorService;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/bj;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/bj;->f:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    iput-object v2, v1, Lcom/google/android/apps/gmm/directions/bj;->f:Ljava/util/concurrent/ScheduledExecutorService;

    .line 570
    return-void
.end method

.method public onResume()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 382
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v7, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 385
    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v1, :cond_d

    .line 386
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-object v1, v0

    .line 390
    :goto_0
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v1, v0, :cond_1

    move v0, v2

    .line 391
    :goto_1
    new-instance v4, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v4, v5}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->g:[Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/gmm/directions/p;

    invoke-direct {v5, p0, v0}, Lcom/google/android/apps/gmm/directions/p;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;Z)V

    if-nez v4, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move v0, v3

    .line 390
    goto :goto_1

    .line 391
    :cond_2
    if-nez v5, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    new-instance v0, Lcom/google/b/c/ef;

    invoke-direct {v0, v4, v5}, Lcom/google/b/c/ef;-><init>(Ljava/lang/Iterable;Lcom/google/b/a/aa;)V

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/lang/Iterable;)Lcom/google/b/c/cv;

    move-result-object v0

    new-instance v4, Lcom/google/android/apps/gmm/directions/u;

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->g:[Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-direct {v4, v5, v6, v0}, Lcom/google/android/apps/gmm/directions/u;-><init>(Landroid/content/Context;[Lcom/google/android/apps/gmm/map/r/a/w;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->setAdapter(Landroid/support/v4/view/ag;)V

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->q:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->q:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 392
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    iget v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->setCurrentItem(I)V

    .line 393
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->h:Z

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->u:Lcom/google/android/apps/gmm/base/l/j;

    if-eqz v0, :cond_6

    .line 396
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->q:Landroid/view/ViewGroup;

    if-nez v4, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v5, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v5, Lcom/google/android/apps/gmm/base/f/b;

    invoke-virtual {v0, v5, v4, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->v:Lcom/google/android/libraries/curvular/ae;

    .line 397
    invoke-direct {p0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->i()V

    .line 400
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->k:Landroid/view/View;

    .line 402
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 403
    iget-object v4, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v7, v4, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v2, v4, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 404
    iget-object v4, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v4, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 405
    iget-object v4, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v4, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->p:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;

    .line 406
    iget-object v5, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v4, v5, Lcom/google/android/apps/gmm/base/activities/p;->O:Lcom/google/android/apps/gmm/base/a/a;

    .line 407
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->b()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v4, v5, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    const-class v4, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    .line 408
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v4, v5, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->q:Landroid/view/ViewGroup;

    sget v5, Lcom/google/android/apps/gmm/g;->do:I

    .line 409
    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/gmm/base/activities/w;->a(Landroid/view/View;I)Lcom/google/android/apps/gmm/base/activities/w;

    move-result-object v4

    .line 410
    iget-object v0, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 411
    iget-object v1, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 413
    iget-object v0, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->m:Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    .line 414
    iget-object v0, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v8, v0, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    .line 417
    new-instance v1, Lcom/google/android/apps/gmm/base/activities/x;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/activities/x;-><init>()V

    .line 418
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->t:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/x;->a:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/x;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_7

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/x;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 420
    :cond_7
    iget-object v0, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v2, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->t:Lcom/google/android/libraries/curvular/ae;

    .line 421
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    sget-object v5, Lcom/google/android/apps/gmm/base/activities/ac;->b:Lcom/google/android/apps/gmm/base/activities/ac;

    invoke-virtual {v4, v0, v5}, Lcom/google/android/apps/gmm/base/activities/w;->a(Landroid/view/View;Lcom/google/android/apps/gmm/base/activities/ac;)Lcom/google/android/apps/gmm/base/activities/w;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->u:Lcom/google/android/apps/gmm/base/l/j;

    if-eqz v0, :cond_8

    move v0, v2

    .line 422
    :goto_2
    iget-object v6, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v0, v6, Lcom/google/android/apps/gmm/base/activities/p;->H:Z

    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/f;->a:Lcom/google/android/apps/gmm/map/b/a/f;

    .line 423
    iget-object v6, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v6, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    .line 424
    iget-object v0, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->F:Lcom/google/android/apps/gmm/base/activities/x;

    .line 426
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->d:Z

    if-eqz v0, :cond_9

    .line 429
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->d:Z

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/directions/m;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/directions/m;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;)V

    sget-object v4, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v4}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 443
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->s:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 445
    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->x:Lcom/google/android/apps/gmm/directions/bj;

    iget-object v0, v6, Lcom/google/android/apps/gmm/directions/bj;->f:Ljava/util/concurrent/ScheduledExecutorService;

    if-nez v0, :cond_a

    move v0, v2

    :goto_4
    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_8
    move v0, v3

    .line 421
    goto :goto_2

    .line 440
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    goto :goto_3

    :cond_a
    move v0, v3

    .line 445
    goto :goto_4

    :cond_b
    iget-object v0, v6, Lcom/google/android/apps/gmm/directions/bj;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, v6}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/f;

    const-string v1, "TrafficUpdate"

    sget-object v4, Lcom/google/android/apps/gmm/shared/c/a/p;->DIRECTIONS_DETAILS_ROUTE_TRAFFIC_UPDATE_FETCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-direct {v0, v1, v4, v7}, Lcom/google/android/apps/gmm/shared/c/a/f;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/apps/gmm/directions/bj;->f:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/google/android/apps/gmm/directions/bk;

    invoke-direct {v1, v6}, Lcom/google/android/apps/gmm/directions/bk;-><init>(Lcom/google/android/apps/gmm/directions/bj;)V

    iget-object v0, v6, Lcom/google/android/apps/gmm/directions/bj;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->n()Lcom/google/r/b/a/es;

    move-result-object v0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v0, v0, Lcom/google/r/b/a/es;->b:I

    int-to-long v8, v0

    invoke-virtual {v4, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    cmp-long v0, v4, v10

    if-gtz v0, :cond_c

    sget-object v0, Lcom/google/android/apps/gmm/directions/bj;->a:Ljava/lang/String;

    const-string v1, "Expected a traffic refresh interval greater than 0 but received %d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 446
    :goto_5
    return-void

    .line 445
    :cond_c
    iget-object v0, v6, Lcom/google/android/apps/gmm/directions/bj;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iget-wide v8, v6, Lcom/google/android/apps/gmm/directions/bj;->e:J

    sub-long/2addr v2, v8

    sub-long v2, v4, v2

    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iget-object v0, v6, Lcom/google/android/apps/gmm/directions/bj;->f:Ljava/util/concurrent/ScheduledExecutorService;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_5

    :cond_d
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 574
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 576
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "storageItem"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 577
    const-string v0, "tripIndex"

    iget v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 578
    const-string v0, "initialTransitionCompleted"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->w:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 579
    const-string v0, "entryPointFragmentTransitionName"

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    const-string v0, "autoLaunchNav"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 582
    const-string v0, "showFromMyLocation"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 584
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->x:Lcom/google/android/apps/gmm/directions/bj;

    const-string v1, "trafficRefreshedTime"

    iget-wide v2, v0, Lcom/google/android/apps/gmm/directions/bj;->e:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 585
    return-void
.end method
