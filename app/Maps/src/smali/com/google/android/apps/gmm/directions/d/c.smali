.class public Lcom/google/android/apps/gmm/directions/d/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 40
    const-class v0, Lcom/google/android/apps/gmm/directions/d/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/d/c;->a:Ljava/lang/String;

    .line 42
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/directions/d/c;->b:J

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/net/ad;)I
    .locals 1

    .prologue
    .line 228
    invoke-interface {p0}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    .line 229
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    .line 230
    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v0, v0, Lcom/google/r/b/a/qz;->n:I

    return v0
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/b/c;Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/android/apps/gmm/directions/f/c/b;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 236
    .line 237
    invoke-static {}, Lcom/google/android/apps/gmm/directions/f/c/b;->d()Lcom/google/android/apps/gmm/directions/f/c/b;

    sget-object v1, Lcom/google/android/apps/gmm/directions/f/c/b;->PARSER:Lcom/google/n/ax;

    .line 236
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;[B)[B

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/ax;)Lcom/google/n/at;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    check-cast v0, Lcom/google/android/apps/gmm/directions/f/c/b;

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/directions/f/c/b;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/f;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 242
    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agl;->d()Lcom/google/r/b/a/agl;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agl;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/r/a/e;-><init>(Lcom/google/r/b/a/agl;)V

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/b;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agd;->d()Lcom/google/r/b/a/agd;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agd;

    iget-object v0, v0, Lcom/google/r/b/a/agd;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/afj;->g()Lcom/google/r/b/a/afj;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afj;

    .line 245
    invoke-virtual {v0}, Lcom/google/r/b/a/afj;->d()Ljava/util/List;

    move-result-object v2

    .line 246
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_0

    .line 247
    const/4 v0, 0x0

    .line 256
    :goto_0
    return-object v0

    .line 250
    :cond_0
    new-instance v3, Lcom/google/android/apps/gmm/map/r/a/g;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/r/a/g;-><init>()V

    .line 251
    iput-object v1, v3, Lcom/google/android/apps/gmm/map/r/a/g;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    .line 252
    iget v1, v0, Lcom/google/r/b/a/afj;->c:I

    invoke-static {v1}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/a/hm;->f:Lcom/google/maps/g/a/hm;

    :cond_1
    iput-object v1, v3, Lcom/google/android/apps/gmm/map/r/a/g;->b:Lcom/google/maps/g/a/hm;

    .line 253
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/je;

    invoke-static {v1, p1}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Lcom/google/maps/g/a/je;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/apps/gmm/map/r/a/g;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 254
    const/4 v1, 0x1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/je;

    invoke-static {v1, p1}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Lcom/google/maps/g/a/je;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/apps/gmm/map/r/a/g;->d:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 255
    iget-object v0, v0, Lcom/google/r/b/a/afj;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    iput-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/g;->e:Lcom/google/r/b/a/afz;

    .line 256
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/f;

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/map/r/a/f;-><init>(Lcom/google/android/apps/gmm/map/r/a/g;)V

    goto :goto_0
.end method

.method private static a(Lcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/maps/g/a/je;
    .locals 6

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/a/ap;->c()Lcom/google/maps/g/a/je;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    .line 96
    invoke-static {v0}, Lcom/google/maps/g/a/je;->a(Lcom/google/maps/g/a/je;)Lcom/google/maps/g/a/jg;

    move-result-object v1

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-static {}, Lcom/google/maps/g/gy;->newBuilder()Lcom/google/maps/g/ha;

    move-result-object v2

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget v3, v2, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/maps/g/ha;->a:I

    iput-wide v4, v2, Lcom/google/maps/g/ha;->b:D

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget v0, v2, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v2, Lcom/google/maps/g/ha;->a:I

    iput-wide v4, v2, Lcom/google/maps/g/ha;->c:D

    invoke-virtual {v2}, Lcom/google/maps/g/ha;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    .line 96
    invoke-virtual {v1, v0}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/a/jg;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/google/maps/g/a/jg;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/je;

    .line 99
    :cond_0
    return-object v0

    .line 95
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/shared/net/ad;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/apps/gmm/map/r/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/r/b/a;",
            "Lcom/google/android/apps/gmm/shared/b/a;",
            "Lcom/google/android/apps/gmm/shared/net/ad;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/shared/b/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/gmm/directions/d/c;->b(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/shared/net/ad;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->aA:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    .line 148
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/ae;Lcom/google/android/apps/gmm/directions/f/c/e;JLcom/google/android/apps/gmm/shared/b/a;)V
    .locals 16

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 107
    .line 108
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/r/a/ae;)Lcom/google/r/b/a/agl;

    move-result-object v7

    if-nez v7, :cond_2

    .line 109
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    .line 110
    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/f/c/b;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agl;->d()Lcom/google/r/b/a/agl;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/agl;

    iget-object v6, v2, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v6, :cond_f

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v2

    :goto_1
    iget-object v6, v2, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    if-nez v6, :cond_10

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v2

    :goto_2
    iget-object v6, v2, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_1

    iget-object v6, v2, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x2

    if-lt v6, v7, :cond_1

    iget-object v2, v2, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/jm;

    iget-object v2, v2, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ja;->d()Lcom/google/maps/g/a/ja;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/ja;

    iget-object v2, v2, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/je;

    iget v2, v2, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v6, 0x4

    if-ne v2, v6, :cond_11

    move v2, v4

    :goto_3
    if-nez v2, :cond_12

    .line 112
    :cond_1
    :goto_4
    return-void

    .line 108
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    const/4 v6, -0x1

    if-eq v2, v6, :cond_3

    move v2, v4

    :goto_5
    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/r/a/w;

    move-object v6, v2

    :goto_6
    if-eqz v6, :cond_0

    invoke-static {}, Lcom/google/r/b/a/agd;->newBuilder()Lcom/google/r/b/a/agh;

    move-result-object v8

    invoke-static {}, Lcom/google/r/b/a/afj;->newBuilder()Lcom/google/r/b/a/afl;

    move-result-object v9

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v2, :cond_5

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v2, v2

    if-lez v2, :cond_5

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v2, v2, v5

    :goto_7
    invoke-static {v2}, Lcom/google/android/apps/gmm/directions/d/c;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/maps/g/a/je;

    move-result-object v2

    if-nez v2, :cond_6

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_3
    move v2, v5

    goto :goto_5

    :cond_4
    move-object v6, v3

    goto :goto_6

    :cond_5
    move-object v2, v3

    goto :goto_7

    :cond_6
    invoke-virtual {v9}, Lcom/google/r/b/a/afl;->c()V

    iget-object v10, v9, Lcom/google/r/b/a/afl;->b:Ljava/util/List;

    new-instance v11, Lcom/google/n/ao;

    invoke-direct {v11}, Lcom/google/n/ao;-><init>()V

    iget-object v12, v11, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v11, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v11, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v11, Lcom/google/n/ao;->d:Z

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v2, :cond_7

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v2, v2

    if-le v2, v4, :cond_7

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v2, v2, v4

    :goto_8
    invoke-static {v2}, Lcom/google/android/apps/gmm/directions/d/c;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/maps/g/a/je;

    move-result-object v2

    if-nez v2, :cond_8

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_7
    move-object v2, v3

    goto :goto_8

    :cond_8
    invoke-virtual {v9}, Lcom/google/r/b/a/afl;->c()V

    iget-object v10, v9, Lcom/google/r/b/a/afl;->b:Ljava/util/List;

    new-instance v11, Lcom/google/n/ao;

    invoke-direct {v11}, Lcom/google/n/ao;-><init>()V

    iget-object v12, v11, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v11, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v11, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v11, Lcom/google/n/ao;->d:Z

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    if-nez v2, :cond_9

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_9
    iget v10, v9, Lcom/google/r/b/a/afl;->a:I

    or-int/lit8 v10, v10, 0x2

    iput v10, v9, Lcom/google/r/b/a/afl;->a:I

    iget v2, v2, Lcom/google/maps/g/a/hm;->h:I

    iput v2, v9, Lcom/google/r/b/a/afl;->c:I

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/r/a/w;->a:Lcom/google/r/b/a/agl;

    iget-object v10, v2, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v10, :cond_a

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v2

    :goto_9
    iget-object v10, v2, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    if-nez v10, :cond_b

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v2

    :goto_a
    invoke-virtual {v9, v2}, Lcom/google/r/b/a/afl;->a(Lcom/google/maps/a/a;)Lcom/google/r/b/a/afl;

    move-result-object v2

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/r/a/w;->y:Lcom/google/r/b/a/afz;

    invoke-virtual {v2, v6}, Lcom/google/r/b/a/afl;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/afl;

    move-result-object v2

    iget-object v6, v8, Lcom/google/r/b/a/agh;->b:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/r/b/a/afl;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v9, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v6, Lcom/google/n/ao;->d:Z

    iget v2, v8, Lcom/google/r/b/a/agh;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v8, Lcom/google/r/b/a/agh;->a:I

    iget v2, v8, Lcom/google/r/b/a/agh;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, v8, Lcom/google/r/b/a/agh;->a:I

    iput-boolean v4, v8, Lcom/google/r/b/a/agh;->e:Z

    invoke-virtual {v8}, Lcom/google/r/b/a/agh;->g()Lcom/google/n/t;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/agd;

    invoke-static {}, Lcom/google/android/apps/gmm/directions/f/c/b;->newBuilder()Lcom/google/android/apps/gmm/directions/f/c/d;

    move-result-object v6

    if-nez v2, :cond_c

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_a
    iget-object v2, v2, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_9

    :cond_b
    iget-object v2, v2, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    goto :goto_a

    :cond_c
    iget-object v8, v6, Lcom/google/android/apps/gmm/directions/f/c/d;->b:Lcom/google/n/ao;

    iget-object v9, v8, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v8, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v8, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v8, Lcom/google/n/ao;->d:Z

    iget v2, v6, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v6, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    if-nez v7, :cond_d

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_d
    iget-object v2, v6, Lcom/google/android/apps/gmm/directions/f/c/d;->c:Lcom/google/n/ao;

    iget-object v8, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v2, v6, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v6, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    if-nez p1, :cond_e

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_e
    iget v2, v6, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v6, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/apps/gmm/directions/f/c/e;->c:I

    iput v2, v6, Lcom/google/android/apps/gmm/directions/f/c/d;->d:I

    iget v2, v6, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v6, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    move-wide/from16 v0, p2

    iput-wide v0, v6, Lcom/google/android/apps/gmm/directions/f/c/d;->e:J

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/directions/f/c/d;->g()Lcom/google/n/t;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/f/c/b;

    move-object v3, v2

    goto/16 :goto_0

    .line 110
    :cond_f
    iget-object v2, v2, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto/16 :goto_1

    :cond_10
    iget-object v2, v2, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    goto/16 :goto_2

    :cond_11
    move v2, v5

    goto/16 :goto_3

    :cond_12
    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aA:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Lcom/google/n/at;)V

    goto/16 :goto_4
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/b/a;)V
    .locals 3

    .prologue
    .line 118
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->aA:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v1, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Lcom/google/n/at;)V

    .line 119
    :cond_0
    return-void
.end method

.method private static b(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/shared/net/ad;)Z
    .locals 12
    .param p0    # Lcom/google/android/apps/gmm/map/r/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 154
    if-nez p0, :cond_0

    .line 155
    sget-object v0, Lcom/google/android/apps/gmm/directions/d/c;->a:Ljava/lang/String;

    .line 156
    const/4 v0, 0x0

    .line 218
    :goto_0
    return v0

    .line 160
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aA:Lcom/google/android/apps/gmm/shared/b/c;

    .line 162
    invoke-static {}, Lcom/google/android/apps/gmm/directions/f/c/b;->d()Lcom/google/android/apps/gmm/directions/f/c/b;

    sget-object v2, Lcom/google/android/apps/gmm/directions/f/c/b;->PARSER:Lcom/google/n/ax;

    const/4 v0, 0x0

    .line 160
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;[B)[B

    move-result-object v1

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/ax;)Lcom/google/n/at;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_1
    :goto_1
    check-cast v0, Lcom/google/android/apps/gmm/directions/f/c/b;

    .line 163
    if-nez v0, :cond_3

    .line 164
    sget-object v0, Lcom/google/android/apps/gmm/directions/d/c;->a:Ljava/lang/String;

    .line 165
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 160
    goto :goto_1

    .line 168
    :cond_3
    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    sget-wide v4, Lcom/google/android/apps/gmm/directions/d/c;->b:J

    .line 169
    iget-wide v6, v0, Lcom/google/android/apps/gmm/directions/f/c/b;->e:J

    add-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-lez v1, :cond_4

    .line 171
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->aA:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;)V

    .line 172
    sget-object v0, Lcom/google/android/apps/gmm/directions/d/c;->a:Ljava/lang/String;

    .line 173
    const/4 v0, 0x0

    goto :goto_0

    .line 177
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    .line 178
    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/ad;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/directions/d/c;->a(Lcom/google/android/apps/gmm/directions/f/c/b;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v1

    .line 184
    :try_start_0
    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/ad;->a()Landroid/content/Context;

    move-result-object v3

    const/4 v4, -0x1

    invoke-static {v1, v3, v4}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(Lcom/google/android/apps/gmm/map/r/a/f;Landroid/content/Context;I)Lcom/google/android/apps/gmm/map/r/a/ae;
    :try_end_0
    .catch Lcom/google/android/apps/gmm/shared/c/n; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 193
    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v1, v1, Lcom/google/r/b/a/qz;->n:I

    int-to-double v4, v1

    iget v1, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v6, v1

    const-wide v8, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    move-result-wide v6

    const-wide v10, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v6, v10

    mul-double/2addr v6, v8

    const-wide v8, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    .line 194
    sget-object v6, Lcom/google/android/apps/gmm/directions/d/d;->a:[I

    iget v1, v0, Lcom/google/android/apps/gmm/directions/f/c/b;->d:I

    invoke-static {v1}, Lcom/google/android/apps/gmm/directions/f/c/e;->a(I)Lcom/google/android/apps/gmm/directions/f/c/e;

    move-result-object v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/android/apps/gmm/directions/f/c/e;->a:Lcom/google/android/apps/gmm/directions/f/c/e;

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/f/c/e;->ordinal()I

    move-result v1

    aget v1, v6, v1

    packed-switch v1, :pswitch_data_0

    .line 217
    sget-object v0, Lcom/google/android/apps/gmm/directions/d/c;->a:Ljava/lang/String;

    .line 218
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 185
    :catch_0
    move-exception v0

    .line 187
    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aA:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {p1, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;)V

    .line 188
    const-string v1, "Failed to load saved directions"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 189
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 197
    :pswitch_0
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/r/a/ae;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    .line 198
    const/4 v3, 0x0

    invoke-virtual {v0, v2, v4, v5, v3}, Lcom/google/android/apps/gmm/map/r/a/w;->b(Lcom/google/android/apps/gmm/map/b/a/y;DZ)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_7

    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_6

    .line 199
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 198
    :cond_7
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ad;

    goto :goto_2

    .line 202
    :cond_8
    sget-object v0, Lcom/google/android/apps/gmm/directions/d/c;->a:Ljava/lang/String;

    .line 203
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 206
    :pswitch_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/c/b;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agl;->d()Lcom/google/r/b/a/agl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agl;

    iget-object v1, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v1, :cond_9

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    .line 207
    :goto_3
    iget-object v1, v0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    if-nez v1, :cond_a

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v0

    :goto_4
    const/4 v1, 0x0

    iget-object v0, v0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/jm;

    iget-object v0, v0, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ja;->d()Lcom/google/maps/g/a/ja;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ja;

    iget-object v0, v0, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/je;

    iget-object v0, v0, Lcom/google/maps/g/a/je;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    .line 208
    iget-wide v6, v0, Lcom/google/maps/g/gy;->b:D

    iget-wide v0, v0, Lcom/google/maps/g/gy;->c:D

    invoke-static {v6, v7, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    .line 209
    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    float-to-double v0, v0

    cmpg-double v0, v0, v4

    if-gez v0, :cond_b

    .line 210
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 206
    :cond_9
    iget-object v0, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_3

    .line 207
    :cond_a
    iget-object v0, v0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    goto :goto_4

    .line 212
    :cond_b
    sget-object v0, Lcom/google/android/apps/gmm/directions/d/c;->a:Ljava/lang/String;

    .line 213
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 194
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
