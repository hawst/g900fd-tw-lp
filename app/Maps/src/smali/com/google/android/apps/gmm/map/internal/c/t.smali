.class public Lcom/google/android/apps/gmm/map/internal/c/t;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final b:Lcom/google/android/apps/gmm/map/internal/c/t;

.field private static final c:Lcom/google/android/apps/gmm/map/internal/c/t;

.field private static final d:Lcom/google/android/apps/gmm/map/internal/c/t;

.field private static final e:Lcom/google/android/apps/gmm/map/internal/c/t;

.field private static final f:Lcom/google/android/apps/gmm/map/internal/c/t;

.field private static final g:Lcom/google/android/apps/gmm/map/internal/c/t;


# instance fields
.field public final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/high16 v15, -0x34000000    # -3.3554432E7f

    const/high16 v11, -0x80000000

    const/4 v6, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 47
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/t;

    const/high16 v2, 0x42f00000    # 120.0f

    const/high16 v4, -0x3dc00000    # -48.0f

    const/high16 v5, 0x42400000    # 48.0f

    const/high16 v8, 0x6f000000

    move v7, v1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/t;-><init>(FFZFFZFI)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/t;->b:Lcom/google/android/apps/gmm/map/internal/c/t;

    .line 69
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/t;

    const/high16 v2, 0x42700000    # 60.0f

    const/high16 v4, -0x3e400000    # -24.0f

    const/high16 v5, 0x41c00000    # 24.0f

    const/high16 v8, 0x6f000000

    move v7, v1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/t;-><init>(FFZFFZFI)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/t;->d:Lcom/google/android/apps/gmm/map/internal/c/t;

    .line 74
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/t;

    const/high16 v2, 0x41f00000    # 30.0f

    const/high16 v4, -0x3f400000    # -6.0f

    const/high16 v5, 0x40c00000    # 6.0f

    const/high16 v8, 0x6f000000

    move v7, v1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/t;-><init>(FFZFFZFI)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/t;->f:Lcom/google/android/apps/gmm/map/internal/c/t;

    .line 79
    new-instance v7, Lcom/google/android/apps/gmm/map/internal/c/t;

    const/high16 v8, 0x42400000    # 48.0f

    const/high16 v9, 0x42f00000    # 120.0f

    move v10, v6

    move v12, v1

    move v13, v3

    move v14, v1

    invoke-direct/range {v7 .. v15}, Lcom/google/android/apps/gmm/map/internal/c/t;-><init>(FFZFFZFI)V

    sput-object v7, Lcom/google/android/apps/gmm/map/internal/c/t;->c:Lcom/google/android/apps/gmm/map/internal/c/t;

    .line 84
    new-instance v7, Lcom/google/android/apps/gmm/map/internal/c/t;

    const/high16 v8, 0x41c00000    # 24.0f

    const/high16 v9, 0x42700000    # 60.0f

    move v10, v6

    move v12, v1

    move v13, v3

    move v14, v1

    invoke-direct/range {v7 .. v15}, Lcom/google/android/apps/gmm/map/internal/c/t;-><init>(FFZFFZFI)V

    sput-object v7, Lcom/google/android/apps/gmm/map/internal/c/t;->e:Lcom/google/android/apps/gmm/map/internal/c/t;

    .line 89
    new-instance v7, Lcom/google/android/apps/gmm/map/internal/c/t;

    const/high16 v8, 0x41800000    # 16.0f

    const/high16 v9, 0x42200000    # 40.0f

    move v10, v6

    move v12, v1

    move v13, v3

    move v14, v1

    invoke-direct/range {v7 .. v15}, Lcom/google/android/apps/gmm/map/internal/c/t;-><init>(FFZFFZFI)V

    sput-object v7, Lcom/google/android/apps/gmm/map/internal/c/t;->g:Lcom/google/android/apps/gmm/map/internal/c/t;

    .line 94
    return-void
.end method

.method private constructor <init>(FFZFFZFI)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput p8, p0, Lcom/google/android/apps/gmm/map/internal/c/t;->a:I

    .line 118
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/h;)Lcom/google/android/apps/gmm/map/internal/c/t;
    .locals 5

    .prologue
    const/high16 v4, 0x41940000    # 18.5f

    const/high16 v3, 0x41840000    # 16.5f

    const/4 v1, 0x0

    .line 135
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/h;->l:I

    const/4 v2, 0x4

    and-int/2addr v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    move-object v0, v1

    .line 163
    :goto_1
    return-object v0

    .line 135
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 139
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->b:Lcom/google/android/apps/gmm/map/internal/c/bv;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v0

    aget-object v0, v2, v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/w;

    .line 140
    if-nez v0, :cond_2

    move-object v0, v1

    .line 141
    goto :goto_1

    .line 144
    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    .line 146
    if-lez v0, :cond_5

    .line 147
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    int-to-float v0, v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_3

    .line 148
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/t;->f:Lcom/google/android/apps/gmm/map/internal/c/t;

    goto :goto_1

    .line 149
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    int-to-float v0, v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_4

    .line 150
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/t;->d:Lcom/google/android/apps/gmm/map/internal/c/t;

    goto :goto_1

    .line 152
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/t;->b:Lcom/google/android/apps/gmm/map/internal/c/t;

    goto :goto_1

    .line 154
    :cond_5
    if-gez v0, :cond_8

    .line 155
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    int-to-float v0, v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_6

    .line 156
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/t;->g:Lcom/google/android/apps/gmm/map/internal/c/t;

    goto :goto_1

    .line 157
    :cond_6
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    int-to-float v0, v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_7

    .line 158
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/t;->e:Lcom/google/android/apps/gmm/map/internal/c/t;

    goto :goto_1

    .line 160
    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/t;->c:Lcom/google/android/apps/gmm/map/internal/c/t;

    goto :goto_1

    :cond_8
    move-object v0, v1

    .line 163
    goto :goto_1
.end method
