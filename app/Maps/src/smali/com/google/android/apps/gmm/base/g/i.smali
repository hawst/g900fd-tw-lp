.class public Lcom/google/android/apps/gmm/base/g/i;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public c:Lcom/google/maps/a/e;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/f;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/Boolean;

.field public f:Z

.field public g:Lcom/google/r/b/a/ads;

.field public h:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->d:Ljava/util/Set;

    .line 453
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/g/i;->h:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/base/g/i;
    .locals 4

    .prologue
    .line 560
    if-nez p1, :cond_0

    .line 569
    :goto_0
    return-object p0

    .line 564
    :cond_0
    invoke-static {}, Lcom/google/maps/a/e;->newBuilder()Lcom/google/maps/a/g;

    move-result-object v0

    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    .line 565
    iget v1, v0, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/maps/a/g;->a:I

    iput-wide v2, v0, Lcom/google/maps/a/g;->c:D

    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    .line 566
    iget v1, v0, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/maps/a/g;->a:I

    iput-wide v2, v0, Lcom/google/maps/a/g;->b:D

    .line 567
    invoke-virtual {v0}, Lcom/google/maps/a/g;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    .line 568
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->c:Lcom/google/maps/a/e;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/base/g/i;
    .locals 8

    .prologue
    .line 573
    if-nez p1, :cond_0

    .line 582
    :goto_0
    return-object p0

    .line 577
    :cond_0
    invoke-static {}, Lcom/google/maps/a/e;->newBuilder()Lcom/google/maps/a/g;

    move-result-object v0

    .line 578
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v1

    const-wide v4, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    const-wide v6, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v2, v6

    mul-double/2addr v2, v4

    const-wide v4, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v2, v4

    iget v1, v0, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/maps/a/g;->a:I

    iput-wide v2, v0, Lcom/google/maps/a/g;->c:D

    .line 579
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v2

    iget v1, v0, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/maps/a/g;->a:I

    iput-wide v2, v0, Lcom/google/maps/a/g;->b:D

    .line 580
    invoke-virtual {v0}, Lcom/google/maps/a/g;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    .line 581
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->c:Lcom/google/maps/a/e;

    goto :goto_0
.end method

.method public final a(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/map/g/a;Z)Lcom/google/android/apps/gmm/base/g/i;
    .locals 0

    .prologue
    .line 542
    invoke-virtual {p0, p1, p3}, Lcom/google/android/apps/gmm/base/g/i;->a(Lcom/google/e/a/a/a/b;Z)Lcom/google/android/apps/gmm/base/g/i;

    .line 543
    invoke-virtual {p0, p2}, Lcom/google/android/apps/gmm/base/g/i;->a(Lcom/google/android/apps/gmm/map/g/a;)V

    .line 544
    return-object p0
.end method

.method public final a(Lcom/google/e/a/a/a/b;Z)Lcom/google/android/apps/gmm/base/g/i;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 462
    if-nez p1, :cond_0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->g:Lcom/google/r/b/a/ads;

    .line 463
    return-object p0

    .line 462
    :cond_0
    const/4 v1, 0x7

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->d(Lcom/google/e/a/a/a/b;I)[Lcom/google/e/a/a/a/b;

    move-result-object v1

    array-length v2, v1

    if-nez v2, :cond_1

    sget-object v1, Lcom/google/android/apps/gmm/base/g/g;->a:Ljava/lang/String;

    const-string v2, "TactileTextAdProto had no locations"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    aget-object v0, v1, v3

    const/4 v1, 0x1

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_1
    check-cast v0, Lcom/google/r/b/a/ads;

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method final a()Lcom/google/r/b/a/ads;
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x0

    const/4 v3, 0x1

    .line 591
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->g:Lcom/google/r/b/a/ads;

    if-nez v0, :cond_0

    .line 592
    invoke-static {}, Lcom/google/r/b/a/ads;->newBuilder()Lcom/google/r/b/a/adu;

    move-result-object v0

    move-object v1, v0

    .line 595
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 596
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 592
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->g:Lcom/google/r/b/a/ads;

    .line 593
    invoke-static {v0}, Lcom/google/r/b/a/ads;->a(Lcom/google/r/b/a/ads;)Lcom/google/r/b/a/adu;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 596
    :cond_1
    iget v2, v1, Lcom/google/r/b/a/adu;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v1, Lcom/google/r/b/a/adu;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/adu;->d:Ljava/lang/Object;

    .line 598
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->b:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 599
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->b:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget v2, v1, Lcom/google/r/b/a/adu;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/r/b/a/adu;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/adu;->c:Ljava/lang/Object;

    .line 601
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->c:Lcom/google/maps/a/e;

    if-eqz v0, :cond_6

    .line 602
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->c:Lcom/google/maps/a/e;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget-object v2, v1, Lcom/google/r/b/a/adu;->b:Lcom/google/n/ao;

    iget-object v5, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/r/b/a/adu;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/r/b/a/adu;->a:I

    .line 604
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 605
    invoke-static {}, Lcom/google/r/b/a/ir;->newBuilder()Lcom/google/r/b/a/it;

    move-result-object v5

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/b/a/l;->toString()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget v7, v5, Lcom/google/r/b/a/it;->a:I

    or-int/lit8 v7, v7, 0x1

    iput v7, v5, Lcom/google/r/b/a/it;->a:I

    iput-object v6, v5, Lcom/google/r/b/a/it;->b:Ljava/lang/Object;

    iget v6, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    const/high16 v7, -0x80000000

    if-eq v6, v7, :cond_8

    iget v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    iget v6, v5, Lcom/google/r/b/a/it;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, v5, Lcom/google/r/b/a/it;->a:I

    iput v0, v5, Lcom/google/r/b/a/it;->c:I

    :cond_8
    invoke-virtual {v5}, Lcom/google/r/b/a/it;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ir;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    invoke-virtual {v1}, Lcom/google/r/b/a/adu;->i()V

    iget-object v5, v1, Lcom/google/r/b/a/adu;->g:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    invoke-direct {v6}, Lcom/google/n/ao;-><init>()V

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v6, Lcom/google/n/ao;->d:Z

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 607
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    .line 609
    iget-object v0, v1, Lcom/google/r/b/a/adu;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ki;->i()Lcom/google/maps/g/ki;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ki;

    invoke-static {}, Lcom/google/maps/g/ki;->newBuilder()Lcom/google/maps/g/kk;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/kk;->a(Lcom/google/maps/g/ki;)Lcom/google/maps/g/kk;

    move-result-object v5

    .line 610
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget v2, v5, Lcom/google/maps/g/kk;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v5, Lcom/google/maps/g/kk;->a:I

    iput-boolean v0, v5, Lcom/google/maps/g/kk;->b:Z

    .line 613
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/g/i;->f:Z

    if-eqz v0, :cond_10

    .line 614
    invoke-virtual {v5}, Lcom/google/maps/g/kk;->d()Ljava/util/List;

    move-result-object v0

    .line 615
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    iput-object v2, v5, Lcom/google/maps/g/kk;->c:Ljava/util/List;

    iget v2, v5, Lcom/google/maps/g/kk;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, v5, Lcom/google/maps/g/kk;->a:I

    .line 616
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_b
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/tp;

    .line 617
    iget v2, v0, Lcom/google/maps/g/tp;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v7, 0x2

    if-ne v2, v7, :cond_e

    move v2, v3

    :goto_3
    if-eqz v2, :cond_d

    .line 618
    iget v2, v0, Lcom/google/maps/g/tp;->c:I

    invoke-static {v2}, Lcom/google/maps/g/ts;->a(I)Lcom/google/maps/g/ts;

    move-result-object v2

    if-nez v2, :cond_c

    sget-object v2, Lcom/google/maps/g/ts;->a:Lcom/google/maps/g/ts;

    :cond_c
    sget-object v7, Lcom/google/maps/g/ts;->b:Lcom/google/maps/g/ts;

    if-eq v2, v7, :cond_b

    .line 620
    :cond_d
    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    move v2, v4

    .line 617
    goto :goto_3

    .line 620
    :cond_f
    invoke-virtual {v5}, Lcom/google/maps/g/kk;->c()V

    iget-object v2, v5, Lcom/google/maps/g/kk;->c:Ljava/util/List;

    new-instance v7, Lcom/google/n/ao;

    invoke-direct {v7}, Lcom/google/n/ao;-><init>()V

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v7, Lcom/google/n/ao;->d:Z

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 624
    :cond_10
    iget-object v0, v1, Lcom/google/r/b/a/adu;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/maps/g/kk;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v5, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/r/b/a/adu;->a:I

    const/high16 v2, 0x2000000

    or-int/2addr v0, v2

    iput v0, v1, Lcom/google/r/b/a/adu;->a:I

    .line 627
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/g/i;->h:Z

    if-eqz v0, :cond_12

    iget v0, v1, Lcom/google/r/b/a/adu;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_13

    move v0, v3

    :goto_4
    if-nez v0, :cond_12

    .line 633
    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v0

    .line 632
    invoke-virtual {v1, v0}, Lcom/google/r/b/a/adu;->a(Lcom/google/r/b/a/acq;)Lcom/google/r/b/a/adu;

    .line 636
    :cond_12
    invoke-virtual {v1}, Lcom/google/r/b/a/adu;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    return-object v0

    :cond_13
    move v0, v4

    .line 627
    goto :goto_4
.end method

.method a(Lcom/google/android/apps/gmm/map/g/a;)V
    .locals 3

    .prologue
    .line 548
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/g/d;->i:Ljava/lang/String;

    const/16 v1, 0xa

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->a:Ljava/lang/String;

    .line 549
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/g/a;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/i;->b:Ljava/lang/String;

    .line 550
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/g/d;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/g/i;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/base/g/i;

    .line 552
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/g/a;->d:Lcom/google/android/apps/gmm/map/indoor/d/g;

    if-eqz v0, :cond_1

    .line 553
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/g/a;->d:Lcom/google/android/apps/gmm/map/indoor/d/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 554
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/i;->d:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 557
    :cond_1
    return-void
.end method
