.class public Lcom/google/android/apps/gmm/map/internal/d/r;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "Lcom/google/android/apps/gmm/map/internal/d/au;",
            ">;"
        }
    .end annotation
.end field

.field final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/au;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/map/internal/d/t;

.field final d:Lcom/google/android/apps/gmm/map/internal/d/s;

.field private final e:Lcom/google/android/apps/gmm/map/internal/d/ac;

.field private final f:Lcom/google/android/apps/gmm/shared/c/f;

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/android/apps/gmm/map/b/a/ai;

.field private i:Lcom/google/e/a/a/a/b;

.field private j:J

.field private k:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/internal/d/t;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/d/ac;",
            "Lcom/google/android/apps/gmm/shared/c/f;",
            "Lcom/google/android/apps/gmm/map/internal/d/t;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/au;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->e:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 88
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->f:Lcom/google/android/apps/gmm/shared/c/f;

    .line 89
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->c:Lcom/google/android/apps/gmm/map/internal/d/t;

    .line 90
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/b/c/hj;->a(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    .line 91
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    new-instance v3, Ljava/util/ArrayList;

    if-ltz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const-wide/16 v4, 0x5

    int-to-long v6, v2

    add-long/2addr v4, v6

    div-int/lit8 v0, v2, 0xa

    int-to-long v6, v0

    add-long/2addr v4, v6

    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    const v0, 0x7fffffff

    :goto_1
    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    .line 92
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->g:Ljava/util/Set;

    .line 93
    :goto_2
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 94
    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/au;

    .line 95
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 91
    :cond_2
    const-wide/32 v6, -0x80000000

    cmp-long v0, v4, v6

    if-gez v0, :cond_3

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_3
    long-to-int v0, v4

    goto :goto_1

    .line 98
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 99
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/s;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 101
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, p2}, Lcom/google/android/apps/gmm/map/internal/d/s;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/shared/net/a/b;Lcom/google/android/apps/gmm/shared/c/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->d:Lcom/google/android/apps/gmm/map/internal/d/s;

    .line 106
    :goto_3
    return-void

    .line 104
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->d:Lcom/google/android/apps/gmm/map/internal/d/s;

    goto :goto_3
.end method

.method private d(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/au;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/au;->a(Z)V

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->c:Lcom/google/android/apps/gmm/map/internal/d/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/t;->a()V

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->d:Lcom/google/android/apps/gmm/map/internal/d/s;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->d:Lcom/google/android/apps/gmm/map/internal/d/s;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/s;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/a/e;->d()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/s;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/e;->d()V

    .line 322
    :cond_0
    return-void
.end method

.method private declared-synchronized g()Z
    .locals 2

    .prologue
    .line 328
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->e:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 329
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    .line 330
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 331
    const/4 v0, 0x1

    .line 333
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 328
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/android/apps/gmm/map/b/a/ai;
    .locals 1

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/r;->g()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    const/4 v0, 0x0

    .line 126
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->h:Lcom/google/android/apps/gmm/map/b/a/ai;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/map/b/a/ai;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->c:Lcom/google/android/apps/gmm/map/internal/d/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/t;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    monitor-exit p0

    return-void

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/e/a/a/a/b;)V
    .locals 4

    .prologue
    .line 294
    monitor-enter p0

    const v1, 0xea60

    const/4 v0, 0x4

    const/16 v2, 0x15

    :try_start_0
    invoke-virtual {p1, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v0, v2

    mul-int/2addr v0, v1

    .line 295
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->f:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->k:J

    .line 296
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->k:J

    int-to-long v0, v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->j:J

    .line 297
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->i:Lcom/google/e/a/a/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    monitor-exit p0

    return-void

    .line 294
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 221
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/au;

    .line 223
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/au;->b(Z)V

    .line 221
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 225
    :cond_0
    return-void
.end method

.method public final declared-synchronized b()Lcom/google/android/apps/gmm/map/internal/d/au;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 137
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/r;->g()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 145
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 141
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->g:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/au;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 1

    .prologue
    .line 261
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 262
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/r;->d(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    monitor-exit p0

    return-void

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 251
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/au;

    .line 253
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/au;->a(Z)V

    .line 251
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 255
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 198
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/au;

    .line 200
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/au;->a()V

    .line 198
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 202
    :cond_0
    return-void
.end method

.method public final declared-synchronized c(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 1

    .prologue
    .line 269
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 270
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/r;->d(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    monitor-exit p0

    return-void

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Lcom/google/e/a/a/a/b;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 287
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->j:J

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->f:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->a()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 288
    const/4 v0, 0x0

    .line 290
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->i:Lcom/google/e/a/a/a/b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 287
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 2

    .prologue
    .line 306
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->f:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->k:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    monitor-exit p0

    return-void

    .line 306
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()J
    .locals 2

    .prologue
    .line 310
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/d/r;->k:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
