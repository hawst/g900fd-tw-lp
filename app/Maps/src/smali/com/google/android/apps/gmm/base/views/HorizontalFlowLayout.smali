.class public Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/views/ab;

.field private final b:Landroid/graphics/Rect;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/base/views/ab;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/ab;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->a:Lcom/google/android/apps/gmm/base/views/ab;

    .line 44
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->b:Landroid/graphics/Rect;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->c:Z

    .line 54
    return-void
.end method

.method private static a(III)I
    .locals 0

    .prologue
    .line 114
    sparse-switch p1, :sswitch_data_0

    move p0, p2

    .line 120
    :goto_0
    :sswitch_0
    return p0

    .line 118
    :sswitch_1
    invoke-static {p2, p0}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_0

    .line 114
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 151
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-direct {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 146
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->a:Lcom/google/android/apps/gmm/base/views/ab;

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getPaddingBottom()I

    move-result v4

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getWidth()I

    move-result v5

    .line 129
    invoke-static {p0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x1

    .line 126
    :goto_0
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/base/views/ab;->a(IIIIIZ)V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getChildCount()I

    move-result v0

    .line 132
    :goto_1
    if-ge v7, v0, :cond_2

    .line 133
    invoke-virtual {p0, v7}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 134
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    .line 135
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->a:Lcom/google/android/apps/gmm/base/views/ab;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->b:Landroid/graphics/Rect;

    invoke-virtual {v2, v1, v3}, Lcom/google/android/apps/gmm/base/views/ab;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 140
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->b:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 132
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_1
    move v6, v7

    .line 129
    goto :goto_0

    .line 142
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 67
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 68
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v10

    .line 70
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v11

    .line 71
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v12

    .line 73
    const/high16 v0, -0x80000000

    if-eq v10, v0, :cond_0

    const/high16 v0, 0x40000000    # 2.0f

    if-ne v10, v0, :cond_1

    :cond_0
    move v5, v7

    .line 77
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->a:Lcom/google/android/apps/gmm/base/views/ab;

    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getPaddingBottom()I

    move-result v4

    .line 80
    invoke-static {p0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v6, v8

    .line 77
    :goto_1
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/base/views/ab;->a(IIIIIZ)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getChildCount()I

    move-result v3

    move v2, v9

    .line 83
    :goto_2
    if-ge v2, v3, :cond_6

    .line 84
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 85
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_5

    .line 86
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 90
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    const/4 v5, -0x1

    if-eq v1, v5, :cond_3

    move v1, v8

    :goto_3
    const-string v5, "A child view in MultiLineLinearLayout has a layoutParam.height: MATCH_PARENT which is not supported"

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_1
    const v5, 0x7fffffff

    goto :goto_0

    :cond_2
    move v6, v9

    .line 80
    goto :goto_1

    :cond_3
    move v1, v9

    .line 90
    goto :goto_3

    .line 99
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getPaddingRight()I

    move-result v5

    add-int/2addr v1, v5

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    invoke-static {p1, v1, v5}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getChildMeasureSpec(III)I

    move-result v1

    .line 101
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->a:Lcom/google/android/apps/gmm/base/views/ab;

    .line 102
    iget v5, v5, Lcom/google/android/apps/gmm/base/views/ab;->e:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getPaddingBottom()I

    move-result v6

    add-int/2addr v5, v6

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    invoke-static {p2, v5, v0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->getChildMeasureSpec(III)I

    move-result v0

    .line 103
    invoke-virtual {v4, v1, v0}, Landroid/view/View;->measure(II)V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->a:Lcom/google/android/apps/gmm/base/views/ab;

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1}, Lcom/google/android/apps/gmm/base/views/ab;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 83
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 108
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->a:Lcom/google/android/apps/gmm/base/views/ab;

    .line 109
    iget v1, v0, Lcom/google/android/apps/gmm/base/views/ab;->f:I

    iget v2, v0, Lcom/google/android/apps/gmm/base/views/ab;->a:I

    add-int/2addr v1, v2

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/ab;->b:I

    add-int/2addr v0, v1

    invoke-static {v7, v10, v0}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->a(III)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->a:Lcom/google/android/apps/gmm/base/views/ab;

    .line 110
    iget v2, v1, Lcom/google/android/apps/gmm/base/views/ab;->g:I

    iget v3, v1, Lcom/google/android/apps/gmm/base/views/ab;->c:I

    add-int/2addr v2, v3

    iget v1, v1, Lcom/google/android/apps/gmm/base/views/ab;->d:I

    add-int/2addr v1, v2

    invoke-static {v11, v12, v1}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->a(III)I

    move-result v1

    .line 108
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/HorizontalFlowLayout;->setMeasuredDimension(II)V

    .line 111
    return-void
.end method
