.class Lcom/google/android/apps/gmm/feedback/u;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/n;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/feedback/t;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/feedback/t;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/android/apps/gmm/feedback/u;->a:Lcom/google/android/apps/gmm/feedback/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/u;->a:Lcom/google/android/apps/gmm/feedback/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/t;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/u;->a:Lcom/google/android/apps/gmm/feedback/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/t;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 128
    sget v0, Lcom/google/android/apps/gmm/l;->bg:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/feedback/u;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 133
    sget v0, Lcom/google/android/apps/gmm/l;->iV:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/feedback/u;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/u;->a:Lcom/google/android/apps/gmm/feedback/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/t;->b:Lcom/google/android/apps/gmm/feedback/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/c/a;->a:Lcom/google/android/apps/gmm/feedback/c/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/u;->a:Lcom/google/android/apps/gmm/feedback/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/t;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/u;->a:Lcom/google/android/apps/gmm/feedback/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/t;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-object v1

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/u;->a:Lcom/google/android/apps/gmm/feedback/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/t;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Ljava/lang/Runnable;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/u;->a:Lcom/google/android/apps/gmm/feedback/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/t;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/u;->a:Lcom/google/android/apps/gmm/feedback/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/t;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/u;->a:Lcom/google/android/apps/gmm/feedback/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/t;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    if-nez v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-object v2

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/u;->a:Lcom/google/android/apps/gmm/feedback/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/t;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 164
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->o()Lcom/google/android/apps/gmm/feedback/a/e;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/u;->a:Lcom/google/android/apps/gmm/feedback/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/feedback/t;->b:Lcom/google/android/apps/gmm/feedback/c/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/feedback/c/a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/feedback/a/e;->a(Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/u;->a:Lcom/google/android/apps/gmm/feedback/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/t;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    return-object v0
.end method
