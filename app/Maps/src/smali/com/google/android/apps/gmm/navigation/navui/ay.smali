.class public Lcom/google/android/apps/gmm/navigation/navui/ay;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final c:I

.field public static final d:I

.field public static final e:I

.field public static final g:Lcom/google/android/apps/gmm/shared/c/c/j;

.field private static final h:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/google/android/apps/gmm/shared/c/c/c;

.field public final f:Lcom/google/android/apps/gmm/shared/c/c/e;

.field private final i:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

.field private final j:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:Lcom/google/android/apps/gmm/map/r/a/ai;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 73
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/ay;->h:Ljava/util/Collection;

    .line 91
    sget v0, Lcom/google/android/apps/gmm/l;->eI:I

    sput v0, Lcom/google/android/apps/gmm/navigation/navui/ay;->c:I

    .line 93
    sget v0, Lcom/google/android/apps/gmm/l;->cU:I

    sput v0, Lcom/google/android/apps/gmm/navigation/navui/ay;->d:I

    .line 95
    sget v0, Lcom/google/android/apps/gmm/l;->cS:I

    sput v0, Lcom/google/android/apps/gmm/navigation/navui/ay;->e:I

    .line 99
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/c/j;-><init>()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/ay;->g:Lcom/google/android/apps/gmm/shared/c/c/j;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/c/c;Lcom/google/android/apps/gmm/map/internal/d/c/a/g;)V
    .locals 1

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->a:Landroid/content/Context;

    .line 140
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->b:Lcom/google/android/apps/gmm/shared/c/c/c;

    .line 141
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->i:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    .line 143
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->f:Lcom/google/android/apps/gmm/shared/c/c/e;

    .line 145
    sget v0, Lcom/google/android/apps/gmm/l;->dl:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(I)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->k:Ljava/util/HashSet;

    .line 146
    sget v0, Lcom/google/android/apps/gmm/l;->dm:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(I)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->j:Ljava/util/HashSet;

    .line 147
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/r/a/ag;ZZZ)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 810
    if-eqz p1, :cond_7

    .line 811
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    .line 812
    sget-object v0, Lcom/google/maps/g/a/ez;->b:Lcom/google/maps/g/a/ez;

    if-eq v3, v0, :cond_0

    if-eqz p3, :cond_2

    .line 813
    :cond_0
    sget v2, Lcom/google/android/apps/gmm/navigation/navui/ay;->c:I

    .line 826
    :cond_1
    :goto_0
    return v2

    .line 815
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v0, :cond_5

    .line 816
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    int-to-float v0, v0

    const/high16 v4, 0x42480000    # 50.0f

    cmpl-float v0, v0, v4

    if-lez v0, :cond_5

    move v0, v1

    .line 817
    :goto_1
    sget-object v4, Lcom/google/maps/g/a/ez;->f:Lcom/google/maps/g/a/ez;

    if-eq v3, v4, :cond_3

    sget-object v4, Lcom/google/maps/g/a/ez;->h:Lcom/google/maps/g/a/ez;

    if-ne v3, v4, :cond_6

    :cond_3
    :goto_2
    if-eqz v1, :cond_7

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 818
    :cond_4
    sget v2, Lcom/google/android/apps/gmm/navigation/navui/ay;->c:I

    goto :goto_0

    :cond_5
    move v0, v2

    .line 816
    goto :goto_1

    :cond_6
    move v1, v2

    .line 817
    goto :goto_2

    .line 821
    :cond_7
    if-eqz p3, :cond_8

    .line 822
    sget v2, Lcom/google/android/apps/gmm/navigation/navui/ay;->d:I

    goto :goto_0

    .line 823
    :cond_8
    if-eqz p2, :cond_1

    .line 824
    sget v2, Lcom/google/android/apps/gmm/navigation/navui/ay;->e:I

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 932
    const/4 v0, 0x0

    .line 934
    :goto_0
    const/16 v1, 0x20

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    if-lez v1, :cond_0

    if-le v1, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->k:Ljava/util/HashSet;

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    .line 935
    :goto_1
    if-ltz v1, :cond_1

    move v0, v1

    .line 939
    goto :goto_0

    .line 934
    :cond_0
    const/4 v1, -0x1

    goto :goto_1

    .line 940
    :cond_1
    return v0
.end method

.method private a(Lcom/google/android/apps/gmm/map/r/a/ai;)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 520
    monitor-enter p0

    .line 525
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->l:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->m:Lcom/google/android/apps/gmm/map/r/a/ai;

    if-ne v0, p1, :cond_0

    .line 526
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->l:Landroid/graphics/drawable/Drawable;

    monitor-exit p0

    .line 554
    :goto_0
    return-object v0

    .line 528
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 531
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->a:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 533
    sget v1, Lcom/google/android/apps/gmm/h;->Z:I

    const/4 v2, 0x0

    .line 534
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 537
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 540
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/ai;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    sget-object v2, Lcom/google/maps/g/a/fb;->a:Lcom/google/maps/g/a/fb;

    if-ne v1, v2, :cond_1

    .line 541
    sget v1, Lcom/google/android/apps/gmm/f;->aC:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 548
    :goto_1
    invoke-static {v0}, Lcom/google/android/apps/gmm/util/r;->e(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    .line 549
    monitor-enter p0

    .line 551
    :try_start_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->l:Landroid/graphics/drawable/Drawable;

    .line 552
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->m:Lcom/google/android/apps/gmm/map/r/a/ai;

    .line 553
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 528
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 542
    :cond_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/ai;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    sget-object v2, Lcom/google/maps/g/a/fb;->b:Lcom/google/maps/g/a/fb;

    if-ne v1, v2, :cond_2

    .line 543
    sget v1, Lcom/google/android/apps/gmm/f;->aD:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_1

    .line 545
    :cond_2
    sget v1, Lcom/google/android/apps/gmm/f;->aE:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ag;F)Landroid/text/Spannable;
    .locals 2

    .prologue
    .line 399
    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 400
    invoke-static {p0, v0, p2}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;F)V

    .line 401
    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ai;ZFLandroid/graphics/drawable/Drawable;)Landroid/text/Spannable;
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 443
    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    .line 444
    const v0, 0x3f99999a    # 1.2f

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/d;

    invoke-direct {v4, p4, v0}, Lcom/google/android/apps/gmm/shared/c/c/d;-><init>(Landroid/graphics/drawable/Drawable;F)V

    invoke-static {v4, v1}, Lcom/google/android/apps/gmm/shared/c/c/e;->a(Lcom/google/android/apps/gmm/shared/c/c/d;Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    .line 445
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ai;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move v1, v2

    :goto_0
    if-eqz v1, :cond_2

    .line 455
    :goto_1
    return-object v0

    .line 445
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 451
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ai;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v4, v3, v1}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    iget-object v1, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v5, v1, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v6, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v6, p3}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v1, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 452
    if-eqz p2, :cond_3

    .line 453
    iget-object v1, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v5, v1, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v1, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 455
    :cond_3
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v1, v3, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    const-string v0, " "

    const-string v2, "%s"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iput-object v2, v1, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    const-string v0, "%s"

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    const-string v2, "%s"

    invoke-virtual {v4, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iput-object v0, v1, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    const-string v0, "%s"

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/c/c;ILcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/maps/g/a/al;F)Landroid/text/Spannable;
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 418
    iget-object v8, p3, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    .line 419
    if-gtz p2, :cond_0

    .line 420
    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v1, p3, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {p0, v0, p5}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;F)V

    .line 427
    :goto_0
    return-object v0

    .line 423
    :cond_0
    sget-object v5, Lcom/google/android/apps/gmm/navigation/navui/ay;->g:Lcom/google/android/apps/gmm/shared/c/c/j;

    const/4 v6, 0x0

    move-object v0, p1

    move v1, p2

    move-object v2, p4

    move v4, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZZLcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    .line 424
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/gmm/l;->cT:I

    .line 425
    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v5, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v7

    aput-object v8, v1, v3

    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    const-string v1, "%s"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 426
    invoke-interface {v2}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v1, Lcom/google/android/apps/gmm/map/r/a/ai;

    invoke-interface {v2, v7, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    move v1, v7

    :goto_1
    array-length v0, v4

    if-ge v1, v0, :cond_1

    aget-object v0, v4, v1

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ai;

    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-interface {v2, v0}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {v2, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    const/16 v8, 0x21

    invoke-interface {v2, v5, v6, v7, v8}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    invoke-interface {v2, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 427
    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/ag;Z)Lcom/google/android/apps/gmm/navigation/navui/az;
    .locals 7
    .param p0    # Lcom/google/android/apps/gmm/map/r/a/ag;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 739
    if-nez p0, :cond_0

    .line 741
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/az;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/ay;->h:Ljava/util/Collection;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/navui/ay;->h:Ljava/util/Collection;

    invoke-direct {v0, v1, v3, v2, v2}, Lcom/google/android/apps/gmm/navigation/navui/az;-><init>(Ljava/util/Collection;Ljava/util/Collection;II)V

    .line 790
    :goto_0
    return-object v0

    .line 745
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->t:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v5

    .line 746
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->r:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v4

    .line 747
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 749
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->o:Lcom/google/android/apps/gmm/map/r/a/ai;

    if-eqz v0, :cond_1

    .line 750
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->o:Lcom/google/android/apps/gmm/map/r/a/ai;

    invoke-interface {v3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 752
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->q:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 757
    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    .line 758
    :goto_1
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v6

    if-lez v6, :cond_3

    move v6, v1

    .line 761
    :goto_2
    if-eqz v0, :cond_5

    .line 763
    if-eqz v6, :cond_4

    move-object v0, v3

    :goto_3
    move-object v3, v5

    .line 776
    :goto_4
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 778
    if-ne v3, v4, :cond_7

    .line 779
    :goto_5
    invoke-static {p0, v1, v2, v2}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/map/r/a/ag;ZZZ)I

    move-result v1

    .line 790
    :goto_6
    new-instance v4, Lcom/google/android/apps/gmm/navigation/navui/az;

    invoke-direct {v4, v3, v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/navui/az;-><init>(Ljava/util/Collection;Ljava/util/Collection;II)V

    move-object v0, v4

    goto :goto_0

    :cond_2
    move v0, v2

    .line 757
    goto :goto_1

    :cond_3
    move v6, v2

    .line 758
    goto :goto_2

    :cond_4
    move-object v0, v4

    .line 763
    goto :goto_3

    .line 764
    :cond_5
    if-eqz v6, :cond_6

    move-object v0, v4

    .line 766
    goto :goto_4

    :cond_6
    move-object v0, v3

    move-object v3, v4

    .line 770
    goto :goto_4

    :cond_7
    move v1, v2

    .line 778
    goto :goto_5

    .line 784
    :cond_8
    if-ne v3, v5, :cond_9

    move v6, v1

    .line 785
    :goto_7
    if-ne v0, v4, :cond_a

    move v4, v1

    .line 786
    :goto_8
    invoke-static {p0, v2, v2, v2}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/map/r/a/ag;ZZZ)I

    move-result v5

    .line 787
    if-nez p1, :cond_b

    :goto_9
    invoke-static {p0, v4, v1, v6}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/map/r/a/ag;ZZZ)I

    move-result v2

    move v1, v5

    goto :goto_6

    :cond_9
    move v6, v2

    .line 784
    goto :goto_7

    :cond_a
    move v4, v2

    .line 785
    goto :goto_8

    :cond_b
    move v1, v2

    .line 787
    goto :goto_9
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/ao;)Ljava/lang/CharSequence;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 902
    const-string v0, "\n"

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/maps/g/a/dl;

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/map/r/a/ao;Ljava/lang/String;[Lcom/google/maps/g/a/dl;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(Lcom/google/android/apps/gmm/map/r/a/ao;Ljava/lang/String;[Lcom/google/maps/g/a/dl;)Ljava/lang/CharSequence;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 908
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v0, v0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_2

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_1
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 909
    :cond_0
    const/4 v0, 0x0

    .line 925
    :goto_2
    return-object v0

    :cond_1
    move v0, v2

    .line 908
    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_1

    .line 911
    :cond_3
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    .line 912
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_7

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    move-object v1, v0

    .line 913
    :goto_3
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    move v3, v2

    .line 914
    :goto_4
    iget-object v0, v1, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_8

    .line 915
    iget-object v0, v1, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    .line 916
    iget v2, v0, Lcom/google/maps/g/a/da;->c:I

    invoke-static {v2}, Lcom/google/maps/g/a/dl;->a(I)Lcom/google/maps/g/a/dl;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/maps/g/a/dl;->a:Lcom/google/maps/g/a/dl;

    :cond_4
    invoke-interface {v5, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 917
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    .line 920
    if-lez v2, :cond_5

    .line 921
    invoke-virtual {v4, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 923
    :cond_5
    invoke-virtual {v0}, Lcom/google/maps/g/a/da;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 914
    :cond_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 912
    :cond_7
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    move-object v1, v0

    goto :goto_3

    :cond_8
    move-object v0, v4

    .line 925
    goto :goto_2
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/c/c/c;ILcom/google/maps/g/a/al;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 166
    sget-object v5, Lcom/google/android/apps/gmm/navigation/navui/ay;->g:Lcom/google/android/apps/gmm/shared/c/c/j;

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v4, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZZLcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ap;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 838
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v0

    .line 839
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_2

    .line 842
    :goto_1
    return-object v0

    .line 839
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 842
    :cond_2
    sget v0, Lcom/google/android/apps/gmm/l;->cV:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;"
        }
    .end annotation

    .prologue
    .line 559
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 597
    :goto_0
    return-object p0

    .line 564
    :cond_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 565
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ai;

    .line 566
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 567
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 571
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 572
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 573
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 574
    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 575
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ai;

    .line 576
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->c()Ljava/lang/String;

    move-result-object v1

    .line 577
    if-eqz v1, :cond_6

    .line 578
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->d()Ljava/lang/String;

    move-result-object v6

    .line 579
    if-nez v6, :cond_4

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 582
    :cond_4
    if-eqz v6, :cond_5

    .line 585
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v7, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 587
    :cond_5
    :goto_3
    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 588
    :cond_6
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v1

    .line 593
    if-eqz v1, :cond_3

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 594
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 585
    :cond_7
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    :cond_8
    move-object p0, v2

    .line 597
    goto/16 :goto_0
.end method

.method private a(I)Ljava/util/HashSet;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 988
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 989
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 990
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v1, v0

    .line 991
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 990
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 993
    :cond_0
    return-object v2
.end method

.method private static a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;F)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x1

    const/4 v0, 0x0

    .line 344
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v4

    .line 345
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const-class v2, Lcom/google/android/apps/gmm/map/r/a/ai;

    invoke-virtual {p1, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    move v1, v0

    .line 346
    :goto_0
    array-length v0, v5

    if-ge v1, v0, :cond_1

    .line 347
    aget-object v0, v5, v1

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ai;

    .line 350
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 352
    if-eqz v4, :cond_2

    .line 354
    const-class v2, Lcom/google/android/apps/gmm/navigation/navui/ay;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "#formatCuesWithIcons()"

    invoke-virtual {v2, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 355
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->c()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v2

    .line 356
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 357
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b()I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_2

    .line 358
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 362
    :goto_1
    if-eqz v2, :cond_0

    .line 363
    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    .line 364
    invoke-static {p0, v0, v9, p2, v2}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ai;ZFLandroid/graphics/drawable/Drawable;)Landroid/text/Spannable;

    move-result-object v2

    .line 363
    invoke-virtual {p1, v6, v7, v2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 371
    :goto_2
    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 346
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 367
    :cond_0
    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 368
    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    const/16 v8, 0x21

    .line 367
    invoke-virtual {p1, v2, v6, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_2

    .line 373
    :cond_1
    return-void

    :cond_2
    move-object v2, v3

    goto :goto_1
.end method

.method private static a(Landroid/text/Spannable;IIIF)V
    .locals 2

    .prologue
    const/16 v1, 0x21

    .line 301
    new-instance v0, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v0, p4}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-interface {p0, v0, p1, p2, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 302
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {p0, v0, p1, p2, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 303
    return-void
.end method

.method private b(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 962
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 964
    :goto_0
    const/16 v1, 0x20

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v1

    if-lez v1, :cond_0

    add-int/lit8 v2, v0, -0x1

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->j:Ljava/util/HashSet;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 965
    :goto_1
    if-ltz v1, :cond_1

    move v0, v1

    .line 969
    goto :goto_0

    .line 964
    :cond_0
    const/4 v1, -0x1

    goto :goto_1

    .line 970
    :cond_1
    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/r/a/ai;ZIZFFLcom/google/android/apps/gmm/map/internal/d/c/b/f;)Landroid/text/Spannable;
    .locals 8

    .prologue
    const/16 v7, 0x21

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 478
    const/4 v0, 0x0

    .line 479
    if-eqz p4, :cond_0

    .line 480
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ai;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 482
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->i:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    if-eqz v1, :cond_0

    .line 484
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "#formatStepCue()"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 485
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->i:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    .line 486
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ai;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, v1, p7}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v1

    .line 487
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 488
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b()I

    move-result v2

    const/4 v4, 0x3

    if-ne v2, v4, :cond_0

    .line 489
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 497
    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    .line 498
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->a:Landroid/content/Context;

    invoke-static {v1, p1, p2, p6, v0}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ai;ZFLandroid/graphics/drawable/Drawable;)Landroid/text/Spannable;

    move-result-object v0

    .line 502
    :cond_1
    :goto_1
    return-object v0

    .line 492
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ai;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 494
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/map/r/a/ai;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 499
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ai;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 500
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->cO:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "{0}"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v3, v2, 0x3

    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/ay;->a:Landroid/content/Context;

    sget v5, Lcom/google/android/apps/gmm/l;->cO:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2, v3, v1}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v2

    if-eqz p2, :cond_1

    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v6}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0, v3, v2, v1, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    .line 502
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Ljava/lang/String;)I

    move-result v2

    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/navigation/navui/ay;->b(Ljava/lang/String;)I

    move-result v1

    if-gt v1, v2, :cond_5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    move v2, v3

    :cond_5
    if-lez v2, :cond_6

    invoke-static {v0, v3, v2, p3, p5}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Landroid/text/Spannable;IIIF)V

    :cond_6
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_7

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v0, v1, v3, p3, p5}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Landroid/text/Spannable;IIIF)V

    :cond_7
    if-eqz p2, :cond_1

    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v6}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-interface {v0, v3, v2, v1, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1
.end method

.method public final a(Ljava/util/Collection;IILandroid/text/TextPaint;IZIZFFFLcom/google/android/apps/gmm/map/internal/d/c/b/f;)Ljava/lang/CharSequence;
    .locals 19
    .param p4    # Landroid/text/TextPaint;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;II",
            "Landroid/text/TextPaint;",
            "IZIZFFF",
            "Lcom/google/android/apps/gmm/map/internal/d/c/b/f;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 626
    sget v3, Lcom/google/android/apps/gmm/navigation/navui/ay;->c:I

    move/from16 v0, p5

    if-eq v0, v3, :cond_0

    sget v3, Lcom/google/android/apps/gmm/navigation/navui/ay;->d:I

    move/from16 v0, p5

    if-ne v0, v3, :cond_3

    .line 627
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/ay;->a:Landroid/content/Context;

    move/from16 v0, p5

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 632
    :goto_0
    new-instance v17, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 636
    const-string v4, "{0}"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 637
    add-int/lit8 v5, v4, 0x3

    .line 639
    if-lez v4, :cond_1

    .line 640
    const/4 v6, 0x0

    move-object/from16 v0, v17

    move/from16 v1, p7

    move/from16 v2, p10

    invoke-static {v0, v6, v4, v1, v2}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Landroid/text/Spannable;IIIF)V

    .line 642
    :cond_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v5, v6, :cond_2

    .line 644
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    .line 643
    move-object/from16 v0, v17

    move/from16 v1, p7

    move/from16 v2, p10

    invoke-static {v0, v5, v3, v1, v2}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Landroid/text/Spannable;IIIF)V

    .line 646
    :cond_2
    const-string v3, ""

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5, v3}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 650
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/ay;->a:Landroid/content/Context;

    sget v5, Lcom/google/android/apps/gmm/l;->cS:I

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 651
    const-string v15, " "

    .line 653
    sget v3, Lcom/google/android/apps/gmm/navigation/navui/ay;->e:I

    move/from16 v0, p5

    if-ne v0, v3, :cond_11

    .line 654
    const/4 v3, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v3, v1}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 655
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    move/from16 v0, p7

    invoke-direct {v3, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v5, 0x0

    .line 656
    invoke-interface/range {v16 .. v16}, Ljava/lang/CharSequence;->length()I

    move-result v6

    const/16 v7, 0x21

    .line 655
    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 658
    invoke-interface/range {v16 .. v16}, Ljava/lang/CharSequence;->length()I

    move-result v3

    add-int/2addr v3, v4

    .line 661
    :goto_1
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .line 662
    const/4 v6, 0x0

    .line 663
    const/4 v5, 0x0

    .line 666
    const/4 v4, 0x0

    move-object v11, v4

    move v12, v3

    move v13, v5

    move v14, v6

    .line 667
    :goto_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    move/from16 v0, p2

    if-ge v14, v0, :cond_e

    .line 668
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/r/a/ai;

    move-object/from16 v3, p0

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    move/from16 v9, p11

    move-object/from16 v10, p12

    .line 669
    invoke-virtual/range {v3 .. v10}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/map/r/a/ai;ZIZFFLcom/google/android/apps/gmm/map/internal/d/c/b/f;)Landroid/text/Spannable;

    move-result-object v5

    .line 672
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v3

    move/from16 v0, p2

    if-gt v3, v0, :cond_4

    .line 674
    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v5}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 675
    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v3

    add-int/2addr v3, v12

    .line 676
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_10

    .line 677
    const-string v5, "\n"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v5}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 678
    add-int/lit8 v3, v3, 0x1

    move v5, v13

    move v6, v14

    :goto_3
    move-object v11, v4

    move v12, v3

    move v13, v5

    move v14, v6

    .line 722
    goto :goto_2

    .line 629
    :cond_3
    const-string v3, "{0}"

    goto/16 :goto_0

    .line 680
    :cond_4
    if-nez v11, :cond_5

    .line 681
    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v5}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 682
    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v3

    add-int/2addr v3, v12

    move v5, v13

    move v6, v14

    goto :goto_3

    .line 689
    :cond_5
    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/r/a/ai;->c()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_6

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/r/a/ai;->e()Z

    move-result v3

    if-eqz v3, :cond_9

    :cond_6
    const/4 v3, 0x1

    :goto_4
    if-eqz v3, :cond_7

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/r/a/ai;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_f

    :cond_7
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/ai;->c()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_8

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/ai;->e()Z

    move-result v3

    if-eqz v3, :cond_a

    :cond_8
    const/4 v3, 0x1

    :goto_5
    if-nez v3, :cond_f

    move-object/from16 v3, v16

    .line 695
    :goto_6
    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v3}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 696
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v6

    add-int/2addr v6, v12

    .line 697
    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v5}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 698
    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v5

    add-int/2addr v5, v6

    .line 700
    if-eqz p4, :cond_c

    .line 701
    invoke-virtual/range {v17 .. v17}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    move-object/from16 v0, v17

    move-object/from16 v1, p4

    invoke-static {v0, v13, v6, v1}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;IILandroid/text/TextPaint;)F

    move-result v6

    move/from16 v0, p3

    int-to-float v7, v0

    cmpl-float v6, v6, v7

    if-lez v6, :cond_c

    .line 704
    add-int/lit8 v6, p2, -0x1

    if-ge v14, v6, :cond_b

    .line 706
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v6

    add-int/2addr v6, v12

    const-string v7, "\n"

    .line 705
    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v6, v7}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 707
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    sub-int v3, v5, v3

    .line 708
    add-int/lit8 v14, v14, 0x1

    .line 709
    add-int/lit8 v13, v12, 0x1

    move v5, v13

    move v6, v14

    goto/16 :goto_3

    .line 689
    :cond_9
    const/4 v3, 0x0

    goto :goto_4

    :cond_a
    const/4 v3, 0x0

    goto :goto_5

    .line 711
    :cond_b
    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v5}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    move-object/from16 v3, v17

    .line 723
    :goto_7
    return-object v3

    .line 714
    :cond_c
    if-eq v3, v15, :cond_d

    .line 715
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    move/from16 v0, p7

    invoke-direct {v6, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 717
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    add-int/2addr v3, v12

    const/16 v7, 0x21

    .line 715
    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v12, v3, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_d
    move v3, v5

    move v6, v14

    move v5, v13

    goto/16 :goto_3

    :cond_e
    move-object/from16 v3, v17

    .line 723
    goto :goto_7

    :cond_f
    move-object v3, v15

    goto :goto_6

    :cond_10
    move v5, v13

    move v6, v14

    goto/16 :goto_3

    :cond_11
    move v3, v4

    goto/16 :goto_1
.end method
