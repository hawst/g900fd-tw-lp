.class Lcom/google/android/apps/gmm/mapsactivity/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/shared/net/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/shared/net/c",
        "<",
        "Lcom/google/r/b/a/kx;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/apps/gmm/cardui/c;

.field b:Lcom/google/r/b/a/ks;

.field private final d:Lcom/google/android/apps/gmm/base/activities/c;

.field private final e:Lcom/google/android/apps/gmm/shared/net/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<",
            "Lcom/google/r/b/a/ks;",
            "Lcom/google/r/b/a/kx;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/android/apps/gmm/mapsactivity/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/gmm/mapsactivity/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/mapsactivity/c;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/r/b/a/ks;Lcom/google/android/apps/gmm/mapsactivity/d;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 52
    iput-object p2, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->b:Lcom/google/r/b/a/ks;

    .line 53
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    const-class v1, Lcom/google/r/b/a/ks;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->e:Lcom/google/android/apps/gmm/shared/net/b;

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->e:Lcom/google/android/apps/gmm/shared/net/b;

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 55
    iput-object p3, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->f:Lcom/google/android/apps/gmm/mapsactivity/d;

    .line 56
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->b:Lcom/google/r/b/a/ks;

    if-nez v0, :cond_0

    .line 83
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->f:Lcom/google/android/apps/gmm/mapsactivity/d;

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/mapsactivity/d;->a(Z)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->b:Lcom/google/r/b/a/ks;

    invoke-static {}, Lcom/google/r/b/a/ks;->newBuilder()Lcom/google/r/b/a/ku;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/ku;->a(Lcom/google/r/b/a/ks;)Lcom/google/r/b/a/ku;

    move-result-object v2

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j_()Lcom/google/android/apps/gmm/myplaces/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/myplaces/a/a;->a()Ljava/lang/String;

    move-result-object v1

    .line 71
    if-eqz v1, :cond_3

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->b:Lcom/google/r/b/a/ks;

    .line 73
    iget-object v0, v0, Lcom/google/r/b/a/ks;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/io;->d()Lcom/google/maps/g/io;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/io;

    invoke-static {}, Lcom/google/maps/g/io;->newBuilder()Lcom/google/maps/g/iq;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/iq;->a(Lcom/google/maps/g/io;)Lcom/google/maps/g/iq;

    move-result-object v0

    .line 74
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v3, v0, Lcom/google/maps/g/iq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v0, Lcom/google/maps/g/iq;->a:I

    iput-object v1, v0, Lcom/google/maps/g/iq;->b:Ljava/lang/Object;

    .line 75
    invoke-virtual {v0}, Lcom/google/maps/g/iq;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/io;

    .line 76
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v1, v2, Lcom/google/r/b/a/ku;->b:Lcom/google/n/ao;

    iget-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v1, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/r/b/a/ku;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/r/b/a/ku;->a:I

    .line 79
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/startpage/aa;->a(ZLandroid/content/res/Resources;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-static {}, Lcom/google/r/b/a/bu;->d()Lcom/google/r/b/a/bu;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_4

    :goto_1
    check-cast v0, Lcom/google/r/b/a/bu;

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/ku;->a(Lcom/google/r/b/a/bu;)Lcom/google/r/b/a/ku;

    .line 80
    invoke-static {}, Lcom/google/android/apps/gmm/cardui/a/a;->a()Lcom/google/r/b/a/bn;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/ku;->a(Lcom/google/r/b/a/bn;)Lcom/google/r/b/a/ku;

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->e:Lcom/google/android/apps/gmm/shared/net/b;

    invoke-virtual {v2}, Lcom/google/r/b/a/ku;->g()Lcom/google/n/t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/b;

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    .line 79
    goto :goto_1
.end method

.method public final synthetic a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 31
    check-cast p1, Lcom/google/r/b/a/kx;

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    if-eqz p1, :cond_1

    iput-object v3, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->b:Lcom/google/r/b/a/ks;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->f:Lcom/google/android/apps/gmm/mapsactivity/d;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/mapsactivity/d;->a(Z)V

    invoke-virtual {p1}, Lcom/google/r/b/a/kx;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/be;

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->a:Lcom/google/android/apps/gmm/cardui/c;

    invoke-static {}, Lcom/google/o/h/a/br;->newBuilder()Lcom/google/o/h/a/bt;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/r/b/a/be;->l()[B

    move-result-object v0

    const/4 v4, 0x0

    array-length v5, v0

    invoke-virtual {v3, v0, v4, v5}, Lcom/google/n/b;->a([BII)Lcom/google/n/b;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bt;

    invoke-virtual {v0}, Lcom/google/o/h/a/bt;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    invoke-virtual {p1}, Lcom/google/r/b/a/kx;->g()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/apps/gmm/cardui/c;->a(Lcom/google/o/h/a/br;Ljava/lang/String;Lcom/google/r/b/a/tf;)V
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/mapsactivity/c;->c:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/c;->a:Lcom/google/android/apps/gmm/cardui/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/cardui/c;->e()V

    :cond_1
    return-void
.end method
