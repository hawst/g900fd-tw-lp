.class Lcom/google/android/apps/gmm/map/internal/d/x;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:[B

.field final synthetic c:Lcom/google/android/apps/gmm/map/internal/d/w;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/w;Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/x;->c:Lcom/google/android/apps/gmm/map/internal/d/w;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/x;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/x;->b:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 271
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/x;->c:Lcom/google/android/apps/gmm/map/internal/d/w;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/x;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/x;->b:[B

    iget-object v3, v4, Lcom/google/android/apps/gmm/map/internal/d/w;->g:Lcom/google/android/apps/gmm/map/internal/d/b/r;

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/map/internal/d/b/r;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/internal/d/b/r;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g:Lcom/google/android/apps/gmm/map/internal/d/b/aa;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/aa;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b/z;

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/internal/d/b/b;->a(Ljava/lang/String;)J

    move-result-wide v6

    const/4 v1, 0x0

    invoke-static {v6, v7, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(JLjava/lang/String;)Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/z;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/z;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->b:[B

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/z;->b:[B

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/z;->e:Lcom/google/android/apps/gmm/map/util/f;

    array-length v6, v2

    invoke-virtual {v1, v6}, Lcom/google/android/apps/gmm/map/util/f;->a(I)V

    const/4 v6, 0x0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v1

    const/4 v7, 0x0

    array-length v8, v2

    invoke-static {v2, v6, v1, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Lcom/google/android/apps/gmm/map/util/f;->a(I)V

    :try_start_0
    iget-object v6, v3, Lcom/google/android/apps/gmm/map/internal/d/b/r;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    const/4 v1, 0x1

    new-array v7, v1, [Lcom/google/android/apps/gmm/map/internal/d/b/z;

    const/4 v1, 0x0

    aput-object v0, v7, v1

    if-nez v7, :cond_3

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    const-string v6, "DiskStyleTableCache"

    invoke-static {v6, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    iget-object v1, v3, Lcom/google/android/apps/gmm/map/internal/d/b/r;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/z;)Z

    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/maps/b/a/bi;

    invoke-direct {v0}, Lcom/google/maps/b/a/bi;-><init>()V

    const/4 v1, 0x0

    array-length v3, v2

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/maps/b/a/bi;->a([BII)V

    new-instance v1, Lcom/google/maps/b/a/bk;

    invoke-direct {v1, v0}, Lcom/google/maps/b/a/bk;-><init>(Lcom/google/maps/b/a/bi;)V

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bl;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/bl;-><init>()V

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/map/internal/c/bj;->a(Ljava/lang/Iterable;Lcom/google/android/apps/gmm/map/internal/c/bl;Z)Lcom/google/android/apps/gmm/map/internal/c/bj;

    move-result-object v0

    iget-object v1, v4, Lcom/google/android/apps/gmm/map/internal/d/w;->e:Ljava/util/Map;

    invoke-interface {v1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    iget-object v1, v4, Lcom/google/android/apps/gmm/map/internal/d/w;->f:Ljava/util/Set;

    monitor-enter v1

    :try_start_2
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/d/w;->f:Ljava/util/Set;

    invoke-interface {v0, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_1
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/d/w;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/d/z;

    monitor-enter v1

    :try_start_3
    iget-boolean v2, v1, Lcom/google/android/apps/gmm/map/internal/d/z;->b:Z

    if-nez v2, :cond_2

    const/4 v3, 0x1

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/d/z;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/internal/d/w;->e:Ljava/util/Map;

    invoke-interface {v8, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/internal/d/w;->g:Lcom/google/android/apps/gmm/map/internal/d/b/r;

    iget-boolean v9, v8, Lcom/google/android/apps/gmm/map/internal/d/b/r;->b:Z

    if-nez v9, :cond_8

    const/4 v2, 0x0

    :goto_3
    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_4
    if-eqz v2, :cond_2

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/map/internal/d/z;->b:Z

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, v4, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/d/ac;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/map/j/d;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/map/j/d;-><init>(I)V

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    :cond_2
    monitor-exit v1

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :cond_3
    :try_start_4
    array-length v8, v7

    if-ltz v8, :cond_4

    const/4 v1, 0x1

    :goto_5
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_4
    const/4 v1, 0x0

    goto :goto_5

    :cond_5
    const-wide/16 v10, 0x5

    int-to-long v12, v8

    add-long/2addr v10, v12

    div-int/lit8 v1, v8, 0xa

    int-to-long v8, v1

    add-long/2addr v8, v10

    const-wide/32 v10, 0x7fffffff

    cmp-long v1, v8, v10

    if-lez v1, :cond_6

    const v1, 0x7fffffff

    :goto_6
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v8, v7}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-virtual {v6, v8}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(Ljava/util/List;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    :cond_6
    const-wide/32 v10, -0x80000000

    cmp-long v1, v8, v10

    if-gez v1, :cond_7

    const/high16 v1, -0x80000000

    goto :goto_6

    :cond_7
    long-to-int v1, v8

    goto :goto_6

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    :catch_1
    move-exception v0

    :try_start_6
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/w;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    iget-object v1, v4, Lcom/google/android/apps/gmm/map/internal/d/w;->f:Ljava/util/Set;

    monitor-enter v1

    :try_start_7
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/d/w;->f:Ljava/util/Set;

    invoke-interface {v0, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    goto/16 :goto_1

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0

    :catchall_3
    move-exception v0

    iget-object v1, v4, Lcom/google/android/apps/gmm/map/internal/d/w;->f:Ljava/util/Set;

    monitor-enter v1

    :try_start_8
    iget-object v2, v4, Lcom/google/android/apps/gmm/map/internal/d/w;->f:Ljava/util/Set;

    invoke-interface {v2, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    throw v0

    :catchall_4
    move-exception v0

    :try_start_9
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v0

    :cond_8
    :try_start_a
    iget-object v8, v8, Lcom/google/android/apps/gmm/map/internal/d/b/r;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/d/b/b;->a(Ljava/lang/String;)J

    move-result-wide v10

    const/4 v2, 0x0

    invoke-virtual {v8, v10, v11, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c(JLjava/lang/String;)Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result v2

    goto/16 :goto_3

    .line 272
    :cond_9
    return-void

    :cond_a
    move v2, v3

    goto/16 :goto_4
.end method
