.class public Lcom/google/android/apps/gmm/base/k/n;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/libraries/curvular/au;

.field public final b:Lcom/google/android/libraries/curvular/au;

.field public final c:Lcom/google/android/libraries/curvular/au;

.field public final d:Lcom/google/android/libraries/curvular/au;

.field public final e:Lcom/google/android/libraries/curvular/au;

.field public final f:F

.field public final g:Z

.field public final h:Lcom/google/android/libraries/curvular/aq;

.field public final i:F

.field public final j:F

.field public final k:F

.field public final l:Landroid/graphics/Typeface;

.field public final m:I


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;FZLcom/google/android/libraries/curvular/aq;FFFLandroid/graphics/Typeface;I)V
    .locals 0

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/k/n;->a:Lcom/google/android/libraries/curvular/au;

    .line 223
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/k/n;->b:Lcom/google/android/libraries/curvular/au;

    .line 224
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/k/n;->c:Lcom/google/android/libraries/curvular/au;

    .line 225
    iput-object p4, p0, Lcom/google/android/apps/gmm/base/k/n;->d:Lcom/google/android/libraries/curvular/au;

    .line 226
    iput-object p5, p0, Lcom/google/android/apps/gmm/base/k/n;->e:Lcom/google/android/libraries/curvular/au;

    .line 227
    iput p6, p0, Lcom/google/android/apps/gmm/base/k/n;->f:F

    .line 228
    iput-boolean p7, p0, Lcom/google/android/apps/gmm/base/k/n;->g:Z

    .line 229
    iput-object p8, p0, Lcom/google/android/apps/gmm/base/k/n;->h:Lcom/google/android/libraries/curvular/aq;

    .line 230
    iput p9, p0, Lcom/google/android/apps/gmm/base/k/n;->i:F

    .line 231
    iput p10, p0, Lcom/google/android/apps/gmm/base/k/n;->j:F

    .line 232
    iput p11, p0, Lcom/google/android/apps/gmm/base/k/n;->k:F

    .line 233
    iput-object p12, p0, Lcom/google/android/apps/gmm/base/k/n;->l:Landroid/graphics/Typeface;

    .line 234
    iput p13, p0, Lcom/google/android/apps/gmm/base/k/n;->m:I

    .line 235
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 239
    if-ne p1, p0, :cond_1

    .line 257
    :cond_0
    :goto_0
    return v0

    .line 241
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/base/k/n;

    if-eqz v2, :cond_3

    .line 242
    check-cast p1, Lcom/google/android/apps/gmm/base/k/n;

    .line 243
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/n;->a:Lcom/google/android/libraries/curvular/au;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/n;->a:Lcom/google/android/libraries/curvular/au;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/n;->b:Lcom/google/android/libraries/curvular/au;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/n;->b:Lcom/google/android/libraries/curvular/au;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/n;->c:Lcom/google/android/libraries/curvular/au;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/n;->c:Lcom/google/android/libraries/curvular/au;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/n;->d:Lcom/google/android/libraries/curvular/au;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/n;->d:Lcom/google/android/libraries/curvular/au;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/n;->e:Lcom/google/android/libraries/curvular/au;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/n;->e:Lcom/google/android/libraries/curvular/au;

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/n;->f:F

    iget v3, p1, Lcom/google/android/apps/gmm/base/k/n;->f:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/k/n;->g:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/k/n;->g:Z

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/n;->h:Lcom/google/android/libraries/curvular/aq;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/n;->h:Lcom/google/android/libraries/curvular/aq;

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/n;->i:F

    iget v3, p1, Lcom/google/android/apps/gmm/base/k/n;->i:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/n;->j:F

    iget v3, p1, Lcom/google/android/apps/gmm/base/k/n;->j:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/n;->k:F

    iget v3, p1, Lcom/google/android/apps/gmm/base/k/n;->k:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/n;->l:Landroid/graphics/Typeface;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/n;->l:Landroid/graphics/Typeface;

    .line 254
    invoke-virtual {v2, v3}, Landroid/graphics/Typeface;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/n;->m:I

    iget v3, p1, Lcom/google/android/apps/gmm/base/k/n;->m:I

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 257
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 262
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/n;->a:Lcom/google/android/libraries/curvular/au;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/n;->b:Lcom/google/android/libraries/curvular/au;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/n;->c:Lcom/google/android/libraries/curvular/au;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/n;->d:Lcom/google/android/libraries/curvular/au;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/n;->e:Lcom/google/android/libraries/curvular/au;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/n;->f:F

    .line 267
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/k/n;->g:Z

    .line 268
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/n;->h:Lcom/google/android/libraries/curvular/aq;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/n;->i:F

    .line 270
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/n;->j:F

    .line 271
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/n;->k:F

    .line 272
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/n;->l:Landroid/graphics/Typeface;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/n;->m:I

    .line 274
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 262
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
