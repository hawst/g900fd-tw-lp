.class public Lcom/google/android/apps/gmm/v/by;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:F

.field public final f:F

.field public final g:F

.field public final h:F

.field public final i:I

.field j:Z

.field public final k:Lcom/google/android/apps/gmm/v/bw;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/bw;IIII)V
    .locals 8

    .prologue
    .line 46
    iget v6, p1, Lcom/google/android/apps/gmm/v/bw;->b:I

    iget v7, p1, Lcom/google/android/apps/gmm/v/bw;->c:I

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/v/by;-><init>(Lcom/google/android/apps/gmm/v/bw;IIIIII)V

    .line 47
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/v/bw;IIIIII)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    .line 53
    iput p2, p0, Lcom/google/android/apps/gmm/v/by;->a:I

    .line 54
    iput p3, p0, Lcom/google/android/apps/gmm/v/by;->b:I

    .line 55
    iput p4, p0, Lcom/google/android/apps/gmm/v/by;->c:I

    .line 56
    iput p5, p0, Lcom/google/android/apps/gmm/v/by;->d:I

    .line 57
    iput p6, p0, Lcom/google/android/apps/gmm/v/by;->i:I

    .line 58
    int-to-float v0, p2

    int-to-float v1, p6

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/v/by;->e:F

    .line 60
    int-to-float v0, p3

    int-to-float v1, p7

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/v/by;->f:F

    .line 61
    int-to-float v0, p4

    int-to-float v1, p6

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/v/by;->g:F

    .line 62
    int-to-float v0, p5

    int-to-float v1, p7

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/v/by;->h:F

    .line 63
    return-void
.end method
