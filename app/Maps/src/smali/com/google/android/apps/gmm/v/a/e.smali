.class public Lcom/google/android/apps/gmm/v/a/e;
.super Lcom/google/android/apps/gmm/v/a/d;
.source "PG"


# instance fields
.field private c:Lcom/google/android/apps/gmm/v/cn;

.field private d:Lcom/google/android/apps/gmm/v/cn;

.field private f:Lcom/google/android/apps/gmm/v/cn;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/cn;Lcom/google/android/apps/gmm/v/cn;)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/a/d;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/a/e;->c:Lcom/google/android/apps/gmm/v/cn;

    .line 16
    iput-object p2, p0, Lcom/google/android/apps/gmm/v/a/e;->d:Lcom/google/android/apps/gmm/v/cn;

    .line 17
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cn;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/a/e;->f:Lcom/google/android/apps/gmm/v/cn;

    .line 18
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/v/a/e;->reset()V

    .line 19
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/gmm/v/cj;F)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 23
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    .line 26
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/a/e;->f:Lcom/google/android/apps/gmm/v/cn;

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/a/e;->c:Lcom/google/android/apps/gmm/v/cn;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v2, v2, v5

    mul-float/2addr v2, v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/a/e;->d:Lcom/google/android/apps/gmm/v/cn;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v3, v3, v5

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/a/e;->c:Lcom/google/android/apps/gmm/v/cn;

    .line 27
    iget-object v3, v3, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v3, v3, v6

    mul-float/2addr v3, v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/a/e;->d:Lcom/google/android/apps/gmm/v/cn;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v4, v4, v6

    mul-float/2addr v4, p2

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/a/e;->c:Lcom/google/android/apps/gmm/v/cn;

    .line 28
    iget-object v4, v4, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v4, v4, v7

    mul-float/2addr v0, v4

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/a/e;->d:Lcom/google/android/apps/gmm/v/cn;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v4, v4, v7

    mul-float/2addr v4, p2

    add-float/2addr v0, v4

    .line 26
    iget-object v4, v1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aput v2, v4, v5

    iget-object v2, v1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aput v3, v2, v6

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aput v0, v1, v7

    .line 31
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    invoke-static {v0, v5}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 32
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/a/e;->f:Lcom/google/android/apps/gmm/v/cn;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v0, v0, v5

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/a/e;->f:Lcom/google/android/apps/gmm/v/cn;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v1, v1, v6

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/a/e;->f:Lcom/google/android/apps/gmm/v/cn;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v2, v2, v7

    iget-object v3, p1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    invoke-static {v3, v5, v0, v1, v2}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 33
    return-void
.end method
