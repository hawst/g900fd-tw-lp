.class public Lcom/google/android/apps/gmm/map/internal/c/cm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/bo;


# static fields
.field private static final m:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/m;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:[Ljava/lang/String;

.field public final b:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field final c:B

.field final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/m;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/google/android/apps/gmm/map/internal/c/bs;

.field public final f:[Lcom/google/android/apps/gmm/map/internal/c/i;

.field public final g:I

.field public final h:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public final i:[Lcom/google/android/apps/gmm/map/internal/c/cj;

.field public final j:Lcom/google/android/apps/gmm/map/internal/c/ct;

.field public final k:Z

.field private final l:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/cn;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/cn;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->m:Ljava/util/Comparator;

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/bp;IBI[Ljava/lang/String;[Ljava/lang/String;ILjava/util/List;Lcom/google/android/apps/gmm/map/b/a/ai;[Lcom/google/android/apps/gmm/map/internal/c/cj;JJI[Lcom/google/android/apps/gmm/map/internal/c/i;Z)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bs;",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "IBI[",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/m;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "[",
            "Lcom/google/android/apps/gmm/map/internal/c/cj;",
            "JJI[",
            "Lcom/google/android/apps/gmm/map/internal/c/i;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->e:Lcom/google/android/apps/gmm/map/internal/c/bs;

    .line 124
    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 125
    move/from16 v0, p4

    iput-byte v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->c:B

    .line 126
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->a:[Ljava/lang/String;

    .line 127
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->l:[Ljava/lang/String;

    .line 128
    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->g:I

    .line 129
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->d:Ljava/util/List;

    .line 130
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 131
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->i:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    .line 132
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/ct;

    move-object/from16 v4, p2

    move-object/from16 v5, p10

    move-wide/from16 v6, p12

    move-wide/from16 v8, p14

    move/from16 v10, p5

    move/from16 v11, p4

    move/from16 v12, p3

    move/from16 v13, p16

    invoke-direct/range {v3 .. v13}, Lcom/google/android/apps/gmm/map/internal/c/ct;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJIBII)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->j:Lcom/google/android/apps/gmm/map/internal/c/ct;

    .line 134
    if-nez p17, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_0
    check-cast p17, [Lcom/google/android/apps/gmm/map/internal/c/i;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->f:[Lcom/google/android/apps/gmm/map/internal/c/i;

    .line 135
    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->k:Z

    .line 136
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIILcom/google/android/apps/gmm/map/b/a/ai;JJLcom/google/android/apps/gmm/map/internal/c/o;I)Lcom/google/android/apps/gmm/map/internal/c/cm;
    .locals 17

    .prologue
    .line 428
    add-int/lit8 v4, p2, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a([BI)Z

    move-result v4

    if-nez v4, :cond_0

    .line 430
    new-instance v4, Lcom/google/android/apps/gmm/m/a/a;

    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-direct {v4, v0, v1}, Lcom/google/android/apps/gmm/m/a/a;-><init>([BI)V

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/m/a/a;->skipBytes(I)I

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/m/a/a;->readInt()I

    move-result v5

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/m/a/a;->readInt()I

    move-result v4

    add-int/lit8 v7, p2, 0x8

    new-instance v15, Lcom/google/android/apps/gmm/map/internal/c/cr;

    invoke-direct {v15, v5, v4}, Lcom/google/android/apps/gmm/map/internal/c/cr;-><init>(II)V

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v8, p3

    move-object/from16 v9, p4

    move-wide/from16 v10, p5

    move-wide/from16 v12, p7

    move-object/from16 v14, p9

    move/from16 v16, p10

    invoke-static/range {v5 .. v16}, Lcom/google/android/apps/gmm/map/internal/c/cm;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIILcom/google/android/apps/gmm/map/b/a/ai;JJLcom/google/android/apps/gmm/map/internal/c/o;Lcom/google/android/apps/gmm/map/internal/c/cr;I)Lcom/google/android/apps/gmm/map/internal/c/cm;

    move-result-object v4

    .line 433
    :goto_0
    return-object v4

    :cond_0
    move-object/from16 v0, p1

    move/from16 v1, p3

    move/from16 v2, p2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/cr;->a([BII)Lcom/google/android/apps/gmm/map/internal/c/cr;

    move-result-object v4

    add-int/lit8 v5, p2, 0x8

    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move/from16 v2, p3

    invoke-static {v0, v1, v5, v2, v4}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/b/a/ai;[BIILcom/google/android/apps/gmm/map/internal/c/cr;)Lcom/google/android/apps/gmm/map/internal/c/cs;

    move-result-object v15

    add-int/lit8 v7, p2, 0x1b

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v8, p3

    move-object/from16 v9, p4

    move-wide/from16 v10, p5

    move-wide/from16 v12, p7

    move-object/from16 v14, p9

    move/from16 v16, p10

    invoke-static/range {v5 .. v16}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIILcom/google/android/apps/gmm/map/b/a/ai;JJLcom/google/android/apps/gmm/map/internal/c/o;Lcom/google/android/apps/gmm/map/internal/c/cs;I)Lcom/google/android/apps/gmm/map/internal/c/cm;

    move-result-object v4

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIILcom/google/android/apps/gmm/map/b/a/ai;JJLcom/google/android/apps/gmm/map/internal/c/o;Lcom/google/android/apps/gmm/map/internal/c/cr;I)Lcom/google/android/apps/gmm/map/internal/c/cm;
    .locals 19

    .prologue
    .line 447
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a([BI)Z

    move-result v6

    if-nez v6, :cond_0

    .line 449
    invoke-static/range {p0 .. p11}, Lcom/google/android/apps/gmm/map/internal/c/cm;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIILcom/google/android/apps/gmm/map/b/a/ai;JJLcom/google/android/apps/gmm/map/internal/c/o;Lcom/google/android/apps/gmm/map/internal/c/cr;I)Lcom/google/android/apps/gmm/map/internal/c/cm;

    move-result-object v6

    .line 452
    :goto_0
    return-object v6

    :cond_0
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p10

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/b/a/ai;[BIILcom/google/android/apps/gmm/map/internal/c/cr;)Lcom/google/android/apps/gmm/map/internal/c/cs;

    move-result-object v17

    add-int/lit8 v9, p2, 0x13

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move/from16 v10, p3

    move-object/from16 v11, p4

    move-wide/from16 v12, p5

    move-wide/from16 v14, p7

    move-object/from16 v16, p9

    move/from16 v18, p11

    invoke-static/range {v7 .. v18}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIILcom/google/android/apps/gmm/map/b/a/ai;JJLcom/google/android/apps/gmm/map/internal/c/o;Lcom/google/android/apps/gmm/map/internal/c/cs;I)Lcom/google/android/apps/gmm/map/internal/c/cm;

    move-result-object v6

    goto :goto_0
.end method

.method private static a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIILcom/google/android/apps/gmm/map/b/a/ai;JJLcom/google/android/apps/gmm/map/internal/c/o;Lcom/google/android/apps/gmm/map/internal/c/cs;I)Lcom/google/android/apps/gmm/map/internal/c/cm;
    .locals 29

    .prologue
    .line 706
    move-object/from16 v0, p10

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cs;->a:I

    move/from16 v18, v0

    .line 707
    move-object/from16 v0, p10

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cs;->b:I

    move/from16 v20, v0

    .line 708
    move-object/from16 v0, p10

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/c/cs;->c:I

    .line 709
    move-object/from16 v0, p10

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/cs;->d:I

    .line 710
    move-object/from16 v0, p10

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/internal/c/cs;->e:J

    .line 711
    move-object/from16 v0, p10

    iget-byte v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cs;->f:B

    move/from16 v21, v0

    .line 713
    sub-int v11, p3, p2

    .line 715
    new-instance v12, Lcom/google/android/apps/gmm/map/internal/c/br;

    invoke-direct {v12}, Lcom/google/android/apps/gmm/map/internal/c/br;-><init>()V

    const/16 v2, 0x28

    new-array v10, v2, [B

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    invoke-static/range {v3 .. v10}, Lcom/google/android/apps/gmm/map/internal/c/br;->a(IIIIIJ[B)V

    const/16 v4, 0x100

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/br;->a:[B

    const/4 v3, 0x0

    iget-object v5, v12, Lcom/google/android/apps/gmm/map/internal/c/br;->b:[B

    const/4 v7, 0x0

    const/16 v8, 0x100

    invoke-static {v2, v3, v5, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_0
    const/16 v5, 0x100

    if-ge v3, v5, :cond_0

    iget-object v5, v12, Lcom/google/android/apps/gmm/map/internal/c/br;->b:[B

    aget-byte v5, v5, v3

    add-int/2addr v2, v5

    array-length v5, v10

    rem-int v5, v3, v5

    aget-byte v5, v10, v5

    add-int/2addr v2, v5

    and-int/lit16 v2, v2, 0xff

    iget-object v5, v12, Lcom/google/android/apps/gmm/map/internal/c/br;->b:[B

    aget-byte v5, v5, v3

    iget-object v7, v12, Lcom/google/android/apps/gmm/map/internal/c/br;->b:[B

    iget-object v8, v12, Lcom/google/android/apps/gmm/map/internal/c/br;->b:[B

    aget-byte v8, v8, v2

    aput-byte v8, v7, v3

    iget-object v7, v12, Lcom/google/android/apps/gmm/map/internal/c/br;->b:[B

    aput-byte v5, v7, v2

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    iput v2, v12, Lcom/google/android/apps/gmm/map/internal/c/br;->c:I

    const/4 v2, 0x0

    iput v2, v12, Lcom/google/android/apps/gmm/map/internal/c/br;->d:I

    invoke-virtual {v12, v4}, Lcom/google/android/apps/gmm/map/internal/c/br;->a(I)V

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v12, v0, v1, v11}, Lcom/google/android/apps/gmm/map/internal/c/br;->a([BII)V

    .line 716
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/util/h;->a(I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v2, Lcom/google/android/apps/gmm/map/util/f;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/util/f;-><init>(I)V

    .line 718
    :cond_1
    :try_start_0
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v1, v11, v2}, Lcom/google/android/apps/gmm/map/util/g;->a([BIILcom/google/android/apps/gmm/map/util/f;)V

    .line 720
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v3

    .line 721
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v4

    .line 722
    new-instance v27, Lcom/google/android/apps/gmm/m/a/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v5

    move-object/from16 v0, v27

    invoke-direct {v0, v3, v5}, Lcom/google/android/apps/gmm/m/a/a;-><init>([BI)V

    .line 723
    invoke-static/range {v27 .. v27}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Ljava/io/DataInput;)V

    invoke-static/range {v27 .. v27}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v3

    iget v5, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    if-ne v5, v7, :cond_2

    iget v5, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    if-ne v5, v7, :cond_2

    iget v5, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-eq v5, v7, :cond_4

    :cond_2
    new-instance v4, Ljava/io/IOException;

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x24

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Expected tile coords: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " but received "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 732
    :catch_0
    move-exception v3

    .line 733
    :try_start_1
    new-instance v5, Ljava/io/IOException;

    const-string v7, "Error in tile: "

    const-string v4, "coords=%s, protoVersion=%s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p0, v8, v9

    const/4 v9, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v8, v9

    invoke-static {v4, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_13

    invoke-virtual {v7, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-direct {v5, v4, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 736
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_3

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    :cond_3
    throw v3

    .line 723
    :cond_4
    if-eqz p9, :cond_5

    :try_start_2
    invoke-interface/range {p9 .. p9}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v3

    if-eqz v3, :cond_5

    move-object/from16 v0, p4

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/b/a/ai;->H:Z

    if-eqz v3, :cond_5

    const-string v3, "Legend error"

    new-instance v5, Ljava/lang/RuntimeException;

    invoke-static/range {p4 .. p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x37

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Unpacking binary "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " tile while global style tables in use"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v5}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v8, 0x0

    .line 726
    :goto_2
    move-object/from16 v0, v27

    iget v3, v0, Lcom/google/android/apps/gmm/m/a/a;->a:I

    if-eq v3, v4, :cond_11

    .line 727
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Byte stream not fully read for: "

    .line 728
    const-string v3, "coords=%s, protoVersion=%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p0, v7, v8

    const/4 v8, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_10

    invoke-virtual {v5, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_3
    invoke-direct {v4, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 723
    :cond_5
    invoke-interface/range {v27 .. v27}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v3

    if-lez v3, :cond_6

    add-int/lit16 v0, v3, 0x7d0

    move/from16 v16, v0

    :goto_4
    invoke-static/range {v27 .. v27}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v5

    new-array v14, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    :goto_5
    if-ge v3, v5, :cond_7

    invoke-interface/range {v27 .. v27}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v14, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_6
    const/16 v16, -0x1

    goto :goto_4

    :cond_7
    invoke-static/range {v27 .. v27}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v5

    new-array v15, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    :goto_6
    if-ge v3, v5, :cond_8

    invoke-interface/range {v27 .. v27}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v15, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_8
    invoke-static/range {v27 .. v27}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v5

    const/4 v3, 0x0

    :goto_7
    if-ge v3, v5, :cond_9

    invoke-interface/range {v27 .. v27}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_9
    move-object/from16 v0, v27

    invoke-static {v0, v6}, Lcom/google/android/apps/gmm/map/internal/c/bl;->a(Ljava/io/DataInput;I)Lcom/google/android/apps/gmm/map/internal/c/bl;

    move-result-object v9

    const/16 v3, 0xc

    if-lt v6, v3, :cond_a

    move-object/from16 v0, v27

    invoke-static {v0, v9}, Lcom/google/android/apps/gmm/map/internal/c/bj;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bl;)Lcom/google/android/apps/gmm/map/internal/c/bj;

    move-result-object v10

    :goto_8
    new-instance v5, Lcom/google/android/apps/gmm/map/internal/c/bs;

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v7, p0

    move-object/from16 v8, p4

    move-object/from16 v11, p9

    invoke-direct/range {v5 .. v13}, Lcom/google/android/apps/gmm/map/internal/c/bs;-><init>(ILcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bl;Lcom/google/android/apps/gmm/map/internal/c/bj;Lcom/google/android/apps/gmm/map/internal/c/o;II)V

    invoke-static/range {v27 .. v27}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v7

    if-ltz v7, :cond_b

    const/4 v3, 0x1

    :goto_9
    if-nez v3, :cond_c

    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3

    :cond_a
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    move-object/from16 v0, v27

    invoke-static {v0, v9, v3}, Lcom/google/android/apps/gmm/map/internal/c/bj;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bl;I)Lcom/google/android/apps/gmm/map/internal/c/bj;

    move-result-object v10

    goto :goto_8

    :cond_b
    const/4 v3, 0x0

    goto :goto_9

    :cond_c
    new-instance v17, Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    :goto_a
    if-ge v3, v7, :cond_d

    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-static {v0, v5, v1}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Ljava/util/Collection;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_d
    invoke-static/range {v27 .. v27}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v7

    new-array v0, v7, [Lcom/google/android/apps/gmm/map/internal/c/cj;

    move-object/from16 v19, v0

    const/4 v3, 0x0

    :goto_b
    if-ge v3, v7, :cond_e

    move-object/from16 v0, v27

    invoke-static {v0, v5}, Lcom/google/android/apps/gmm/map/internal/c/cj;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/cj;

    move-result-object v8

    aput-object v8, v19, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_e
    iget v3, v5, Lcom/google/android/apps/gmm/map/internal/c/bs;->a:I

    const/16 v7, 0xd

    if-lt v3, v7, :cond_f

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/c/cm;->m:Ljava/util/Comparator;

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_f
    new-instance v8, Lcom/google/android/apps/gmm/map/internal/c/cm;

    const/4 v3, 0x0

    new-array v0, v3, [Lcom/google/android/apps/gmm/map/internal/c/i;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object v9, v5

    move-object/from16 v10, p0

    move/from16 v11, v18

    move/from16 v12, v21

    move/from16 v13, v20

    move-object/from16 v18, p4

    move-wide/from16 v20, p5

    move-wide/from16 v22, p7

    move/from16 v24, p11

    invoke-direct/range {v8 .. v26}, Lcom/google/android/apps/gmm/map/internal/c/cm;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/bp;IBI[Ljava/lang/String;[Ljava/lang/String;ILjava/util/List;Lcom/google/android/apps/gmm/map/b/a/ai;[Lcom/google/android/apps/gmm/map/internal/c/cj;JJI[Lcom/google/android/apps/gmm/map/internal/c/i;Z)V

    goto/16 :goto_2

    .line 728
    :cond_10
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/util/zip/DataFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_3

    .line 736
    :cond_11
    if-eqz v2, :cond_12

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    :cond_12
    return-object v8

    .line 733
    :cond_13
    :try_start_3
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ai;[BII)Lcom/google/android/apps/gmm/map/internal/c/cs;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1018
    add-int/lit8 v0, p2, 0x8

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a([BI)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1020
    new-instance v4, Lcom/google/maps/b/a/k;

    invoke-direct {v4}, Lcom/google/maps/b/a/k;-><init>()V

    .line 1023
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/util/h;->a(I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v5

    if-nez v5, :cond_0

    new-instance v5, Lcom/google/android/apps/gmm/map/util/f;

    invoke-direct {v5, v1}, Lcom/google/android/apps/gmm/map/util/f;-><init>(I)V

    .line 1025
    :cond_0
    const/4 v6, 0x1

    :try_start_0
    invoke-static {p1, p3, p2}, Lcom/google/android/apps/gmm/map/internal/c/cr;->a([BII)Lcom/google/android/apps/gmm/map/internal/c/cr;

    move-result-object v7

    add-int/lit8 v2, p2, 0x8

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/b/a/ai;[BIILcom/google/maps/b/a/k;Lcom/google/android/apps/gmm/map/util/f;ZLcom/google/android/apps/gmm/map/internal/c/cr;)Lcom/google/android/apps/gmm/map/internal/c/cs;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1028
    if-eqz v5, :cond_1

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    .line 1031
    :cond_1
    :goto_0
    return-object v0

    .line 1028
    :catchall_0
    move-exception v0

    if-eqz v5, :cond_2

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    :cond_2
    throw v0

    .line 1031
    :cond_3
    invoke-static {p1, p3, p2}, Lcom/google/android/apps/gmm/map/internal/c/cr;->a([BII)Lcom/google/android/apps/gmm/map/internal/c/cr;

    move-result-object v0

    add-int/lit8 v1, p2, 0x8

    invoke-static {p0, p1, v1, p3, v0}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/b/a/ai;[BIILcom/google/android/apps/gmm/map/internal/c/cr;)Lcom/google/android/apps/gmm/map/internal/c/cs;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/apps/gmm/map/b/a/ai;[BIILcom/google/android/apps/gmm/map/internal/c/cr;)Lcom/google/android/apps/gmm/map/internal/c/cs;
    .locals 9

    .prologue
    .line 1089
    new-instance v0, Lcom/google/android/apps/gmm/m/a/a;

    invoke-direct {v0, p1, p3}, Lcom/google/android/apps/gmm/m/a/a;-><init>([BI)V

    .line 1090
    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/m/a/a;->skipBytes(I)I

    .line 1092
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Ljava/io/DataInput;)V

    .line 1094
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readUnsignedShort()I

    move-result v4

    .line 1097
    const/16 v1, 0xb

    if-eq v4, v1, :cond_0

    const/16 v1, 0xc

    if-eq v4, v1, :cond_0

    const/16 v1, 0xd

    if-eq v4, v1, :cond_0

    .line 1100
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Version mismatch: 11 .. 12 expected, "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1104
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readInt()I

    move-result v5

    .line 1106
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readLong()J

    move-result-wide v6

    .line 1108
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readUnsignedByte()I

    move-result v0

    int-to-byte v8, v0

    .line 1114
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1115
    iget v3, p4, Lcom/google/android/apps/gmm/map/internal/c/cr;->b:I

    .line 1116
    if-eqz v0, :cond_1

    move v3, v5

    .line 1120
    :cond_1
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/cs;

    iget v2, p4, Lcom/google/android/apps/gmm/map/internal/c/cr;->a:I

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/cs;-><init>(IIIIJB)V

    return-object v1
.end method

.method private static a(Lcom/google/android/apps/gmm/map/b/a/ai;[BIILcom/google/maps/b/a/k;Lcom/google/android/apps/gmm/map/util/f;ZLcom/google/android/apps/gmm/map/internal/c/cr;)Lcom/google/android/apps/gmm/map/internal/c/cs;
    .locals 11

    .prologue
    .line 1155
    sub-int v3, p3, p2

    move v2, p2

    .line 1157
    :goto_0
    add-int v4, p2, v3

    if-ge v2, v4, :cond_0

    :try_start_0
    aget-byte v4, p1, v2

    xor-int/lit8 v4, v4, 0x5f

    int-to-byte v4, v4

    aput-byte v4, p1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1158
    :cond_0
    move-object/from16 v0, p5

    invoke-static {p1, p2, v3, v0}, Lcom/google/android/apps/gmm/map/util/g;->a([BIILcom/google/android/apps/gmm/map/util/f;)V

    .line 1159
    if-eqz p6, :cond_1

    move v2, p2

    .line 1161
    :goto_1
    add-int v4, p2, v3

    if-ge v2, v4, :cond_1

    aget-byte v4, p1, v2

    xor-int/lit8 v4, v4, 0x5f

    int-to-byte v4, v4

    aput-byte v4, p1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1163
    :cond_1
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v2

    .line 1164
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v3

    .line 1165
    const/4 v4, 0x0

    invoke-virtual {p4, v2, v4, v3}, Lcom/google/maps/b/a/k;->a([BII)V

    .line 1166
    iget-object v2, p4, Lcom/google/maps/b/a/k;->d:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    int-to-byte v10, v2

    .line 1169
    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v2, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "psm"

    move-object v3, v2

    :goto_2
    new-instance v2, Lcom/google/maps/b/a/q;

    invoke-direct {v2, p4}, Lcom/google/maps/b/a/q;-><init>(Lcom/google/maps/b/a/k;)V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/b/a/az;

    invoke-virtual {v2}, Lcom/google/maps/b/a/az;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v2, v2, Lcom/google/maps/b/a/az;->a:Lcom/google/maps/b/a/cz;

    iget v5, v2, Lcom/google/maps/b/a/cz;->b:I

    .line 1170
    :goto_3
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/cs;

    move-object/from16 v0, p7

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/cr;->a:I

    const/4 v6, 0x0

    const-wide/16 v8, 0x0

    move v7, v5

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/gmm/map/internal/c/cs;-><init>(IIIIJB)V

    return-object v3

    .line 1169
    :cond_3
    const-string v2, "m"

    move-object v3, v2

    goto :goto_2

    :cond_4
    iget-object v2, p4, Lcom/google/maps/b/a/k;->e:Lcom/google/maps/b/a/cs;

    iget v2, v2, Lcom/google/maps/b/a/cs;->b:I

    if-lez v2, :cond_5

    const/4 v2, 0x0

    invoke-virtual {p4, v2}, Lcom/google/maps/b/a/k;->c(I)Lcom/google/maps/b/a/az;

    move-result-object v2

    iget-object v2, v2, Lcom/google/maps/b/a/az;->a:Lcom/google/maps/b/a/cz;

    iget v5, v2, Lcom/google/maps/b/a/cz;->b:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_3

    :cond_5
    const/4 v5, -0x1

    goto :goto_3

    .line 1172
    :catch_0
    move-exception v2

    .line 1174
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Error in parsing unified tile \'header\'"

    invoke-direct {v3, v4, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 1175
    :catch_1
    move-exception v2

    .line 1176
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Error in parsing unified tile \'header\'"

    invoke-direct {v3, v4, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method static a(Lcom/google/android/apps/gmm/map/b/a/ai;[BILcom/google/android/apps/gmm/map/internal/c/cr;)Lcom/google/android/apps/gmm/map/internal/c/cs;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1043
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a([BI)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1045
    new-instance v4, Lcom/google/maps/b/a/k;

    invoke-direct {v4}, Lcom/google/maps/b/a/k;-><init>()V

    .line 1048
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/util/h;->a(I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v5

    if-nez v5, :cond_0

    new-instance v5, Lcom/google/android/apps/gmm/map/util/f;

    invoke-direct {v5, v1}, Lcom/google/android/apps/gmm/map/util/f;-><init>(I)V

    .line 1050
    :cond_0
    :try_start_0
    array-length v3, p1

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v7, p3

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/b/a/ai;[BIILcom/google/maps/b/a/k;Lcom/google/android/apps/gmm/map/util/f;ZLcom/google/android/apps/gmm/map/internal/c/cr;)Lcom/google/android/apps/gmm/map/internal/c/cs;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1054
    if-eqz v5, :cond_1

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    .line 1057
    :cond_1
    :goto_0
    return-object v0

    .line 1054
    :catchall_0
    move-exception v0

    if-eqz v5, :cond_2

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    :cond_2
    throw v0

    .line 1057
    :cond_3
    array-length v0, p1

    invoke-static {p0, p1, p2, v0, p3}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/b/a/ai;[BIILcom/google/android/apps/gmm/map/internal/c/cr;)Lcom/google/android/apps/gmm/map/internal/c/cs;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ai;[B[B)Lcom/google/android/apps/gmm/map/internal/c/cs;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1230
    array-length v0, p2

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/cr;->a([BII)Lcom/google/android/apps/gmm/map/internal/c/cr;

    move-result-object v7

    .line 1232
    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a([BI)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1234
    new-instance v4, Lcom/google/maps/b/a/k;

    invoke-direct {v4}, Lcom/google/maps/b/a/k;-><init>()V

    .line 1237
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/util/h;->a(I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v5

    if-nez v5, :cond_0

    new-instance v5, Lcom/google/android/apps/gmm/map/util/f;

    invoke-direct {v5, v1}, Lcom/google/android/apps/gmm/map/util/f;-><init>(I)V

    .line 1239
    :cond_0
    const/4 v2, 0x0

    :try_start_0
    array-length v3, p1

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/b/a/ai;[BIILcom/google/maps/b/a/k;Lcom/google/android/apps/gmm/map/util/f;ZLcom/google/android/apps/gmm/map/internal/c/cr;)Lcom/google/android/apps/gmm/map/internal/c/cs;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1243
    if-eqz v5, :cond_1

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    .line 1246
    :cond_1
    :goto_0
    return-object v0

    .line 1243
    :catchall_0
    move-exception v0

    if-eqz v5, :cond_2

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    :cond_2
    throw v0

    .line 1246
    :cond_3
    array-length v0, p1

    invoke-static {p0, p1, v1, v0, v7}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/b/a/ai;[BIILcom/google/android/apps/gmm/map/internal/c/cr;)Lcom/google/android/apps/gmm/map/internal/c/cs;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/io/DataInput;)V
    .locals 4

    .prologue
    .line 999
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    .line 1000
    const v1, 0x44524154

    if-eq v0, v1, :cond_0

    .line 1001
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "TILE_MAGIC expected. Found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1003
    :cond_0
    return-void
.end method

.method static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInput;",
            "Lcom/google/android/apps/gmm/map/internal/c/bs;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 754
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v2

    .line 755
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v3

    invoke-static {p0}, Lcom/google/android/apps/gmm/map/internal/c/n;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/n;

    move-result-object v4

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bs;->a:I

    const/16 v5, 0xd

    if-lt v1, v5, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v1

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    const/high16 v5, -0x80000000

    xor-int/2addr v0, v5

    :goto_0
    new-instance v5, Lcom/google/android/apps/gmm/map/internal/c/ay;

    invoke-direct {v5, v3, v4, v1, v0}, Lcom/google/android/apps/gmm/map/internal/c/ay;-><init>(ILcom/google/android/apps/gmm/map/internal/c/n;II)V

    .line 757
    packed-switch v2, :pswitch_data_0

    .line 795
    :pswitch_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x21

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown feature type: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 759
    :pswitch_1
    invoke-static {v5}, Lcom/google/android/apps/gmm/map/internal/c/k;->a(Lcom/google/android/apps/gmm/map/internal/c/ay;)Lcom/google/android/apps/gmm/map/internal/c/k;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 793
    :goto_1
    return-void

    .line 762
    :pswitch_2
    invoke-static {p0, p1, v5, p2}, Lcom/google/android/apps/gmm/map/internal/c/az;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;Ljava/util/Collection;)V

    goto :goto_1

    .line 765
    :pswitch_3
    invoke-static {p0, p1, v5}, Lcom/google/android/apps/gmm/map/internal/c/h;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;)Lcom/google/android/apps/gmm/map/internal/c/h;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 768
    :pswitch_4
    invoke-static {p0, p1, v5}, Lcom/google/android/apps/gmm/map/internal/c/j;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;)Lcom/google/android/apps/gmm/map/internal/c/j;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 771
    :pswitch_5
    invoke-static {p0, p1, v5}, Lcom/google/android/apps/gmm/map/internal/c/af;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;)Lcom/google/android/apps/gmm/map/internal/c/af;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 774
    :pswitch_6
    invoke-static {p0, p1, v5}, Lcom/google/android/apps/gmm/map/internal/c/ax;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;)Lcom/google/android/apps/gmm/map/internal/c/ax;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 777
    :pswitch_7
    invoke-static {p0, p1, v5}, Lcom/google/android/apps/gmm/map/internal/c/an;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;)Lcom/google/android/apps/gmm/map/internal/c/an;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 780
    :pswitch_8
    invoke-static {p0, p1, v5, p2}, Lcom/google/android/apps/gmm/map/internal/c/ad;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;Ljava/util/Collection;)V

    goto :goto_1

    .line 783
    :pswitch_9
    invoke-static {p0, p1, v5, p2}, Lcom/google/android/apps/gmm/map/internal/c/cg;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;Ljava/util/Collection;)V

    goto :goto_1

    .line 786
    :pswitch_a
    invoke-static {p0, p1, v5, p2}, Lcom/google/android/apps/gmm/map/internal/c/ae;->b(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;Ljava/util/Collection;)V

    goto :goto_1

    .line 789
    :pswitch_b
    invoke-static {p0, p1, v5, p2}, Lcom/google/android/apps/gmm/map/internal/c/bc;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;Ljava/util/Collection;)V

    goto :goto_1

    .line 792
    :pswitch_c
    invoke-static {p0, p1, v5}, Lcom/google/android/apps/gmm/map/internal/c/ba;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;)Lcom/google/android/apps/gmm/map/internal/c/ba;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    move v1, v0

    goto :goto_0

    .line 757
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bo;)Z
    .locals 1

    .prologue
    .line 1260
    instance-of v0, p0, Lcom/google/android/apps/gmm/map/internal/c/l;

    return v0
.end method

.method private static a([BI)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 987
    move v0, v1

    :goto_0
    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/x;->a:[B

    const/4 v2, 0x4

    if-ge v0, v2, :cond_1

    .line 988
    add-int v2, p1, v0

    aget-byte v2, p0, v2

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/x;->a:[B

    aget-byte v3, v3, v0

    if-eq v2, v3, :cond_0

    .line 992
    :goto_1
    return v1

    .line 987
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 992
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private static b(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIILcom/google/android/apps/gmm/map/b/a/ai;JJLcom/google/android/apps/gmm/map/internal/c/o;Lcom/google/android/apps/gmm/map/internal/c/cr;I)Lcom/google/android/apps/gmm/map/internal/c/cm;
    .locals 21

    .prologue
    .line 483
    new-instance v6, Lcom/google/maps/b/a/k;

    invoke-direct {v6}, Lcom/google/maps/b/a/k;-><init>()V

    .line 486
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/util/h;->a(I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v7

    if-nez v7, :cond_0

    new-instance v7, Lcom/google/android/apps/gmm/map/util/f;

    const/4 v2, 0x0

    invoke-direct {v7, v2}, Lcom/google/android/apps/gmm/map/util/f;-><init>(I)V

    .line 488
    :cond_0
    const/4 v8, 0x0

    move-object/from16 v2, p4

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v9, p10

    :try_start_0
    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/b/a/ai;[BIILcom/google/maps/b/a/k;Lcom/google/android/apps/gmm/map/util/f;ZLcom/google/android/apps/gmm/map/internal/c/cr;)Lcom/google/android/apps/gmm/map/internal/c/cs;

    move-result-object v2

    .line 492
    iget-object v3, v6, Lcom/google/maps/b/a/k;->b:Lcom/google/maps/b/a/cz;

    iget v15, v3, Lcom/google/maps/b/a/cz;->b:I

    .line 493
    invoke-interface/range {p9 .. p9}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 494
    move-object/from16 v0, p4

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/b/a/ai;->H:Z

    if-eqz v3, :cond_3

    .line 495
    move-object/from16 v0, p9

    invoke-interface {v0, v15}, Lcom/google/android/apps/gmm/map/internal/c/o;->a(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 496
    const-string v2, "Legend error"

    new-instance v3, Ljava/lang/RuntimeException;

    invoke-static/range {p4 .. p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x2e

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "No style tables for Legend "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", epoch "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 499
    if-eqz v7, :cond_1

    invoke-static {v7}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    :cond_1
    const/4 v2, 0x0

    .line 644
    :cond_2
    :goto_0
    return-object v2

    .line 500
    :cond_3
    :try_start_1
    invoke-interface/range {p9 .. p9}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v3

    if-nez v3, :cond_5

    .line 501
    const-string v2, "Legend error"

    new-instance v3, Ljava/lang/RuntimeException;

    invoke-static/range {p4 .. p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x43

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Unpacking UWF "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", epoch "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " tile but global styles not in use"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 504
    if-eqz v7, :cond_4

    invoke-static {v7}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 507
    :cond_5
    :try_start_2
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/co;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/internal/c/co;-><init>()V

    .line 509
    move-object/from16 v0, p0

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/internal/c/co;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 510
    move-object/from16 v0, p4

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/internal/c/co;->i:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 511
    iget v3, v2, Lcom/google/android/apps/gmm/map/internal/c/cs;->a:I

    iput v3, v4, Lcom/google/android/apps/gmm/map/internal/c/co;->b:I

    .line 512
    iget v3, v2, Lcom/google/android/apps/gmm/map/internal/c/cs;->b:I

    iput v3, v4, Lcom/google/android/apps/gmm/map/internal/c/co;->d:I

    .line 513
    move-wide/from16 v0, p5

    iput-wide v0, v4, Lcom/google/android/apps/gmm/map/internal/c/co;->g:J

    .line 514
    move-wide/from16 v0, p7

    iput-wide v0, v4, Lcom/google/android/apps/gmm/map/internal/c/co;->k:J

    .line 515
    move/from16 v0, p11

    iput v0, v4, Lcom/google/android/apps/gmm/map/internal/c/co;->l:I

    .line 516
    iget-byte v2, v2, Lcom/google/android/apps/gmm/map/internal/c/cs;->f:B

    iput-byte v2, v4, Lcom/google/android/apps/gmm/map/internal/c/co;->c:B

    const/4 v2, 0x1

    .line 517
    iput-boolean v2, v4, Lcom/google/android/apps/gmm/map/internal/c/co;->n:Z

    .line 519
    new-instance v12, Lcom/google/android/apps/gmm/map/internal/c/bl;

    invoke-direct {v12}, Lcom/google/android/apps/gmm/map/internal/c/bl;-><init>()V

    .line 521
    new-instance v2, Lcom/google/maps/b/a/m;

    invoke-direct {v2, v6}, Lcom/google/maps/b/a/m;-><init>(Lcom/google/maps/b/a/k;)V

    const/4 v3, 0x0

    .line 520
    invoke-static {v2, v12, v3}, Lcom/google/android/apps/gmm/map/internal/c/bj;->a(Ljava/lang/Iterable;Lcom/google/android/apps/gmm/map/internal/c/bl;Z)Lcom/google/android/apps/gmm/map/internal/c/bj;

    move-result-object v13

    .line 522
    new-instance v8, Lcom/google/android/apps/gmm/map/internal/c/bs;

    const/4 v9, 0x0

    .line 524
    invoke-virtual {v6}, Lcom/google/maps/b/a/k;->a()Lcom/google/maps/b/a/co;

    move-result-object v2

    iget-object v2, v2, Lcom/google/maps/b/a/co;->a:Lcom/google/maps/b/a/cz;

    iget v0, v2, Lcom/google/maps/b/a/cz;->b:I

    move/from16 v16, v0

    move-object/from16 v10, p0

    move-object/from16 v11, p4

    move-object/from16 v14, p9

    invoke-direct/range {v8 .. v16}, Lcom/google/android/apps/gmm/map/internal/c/bs;-><init>(ILcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bl;Lcom/google/android/apps/gmm/map/internal/c/bj;Lcom/google/android/apps/gmm/map/internal/c/o;II)V

    .line 525
    invoke-virtual {v6}, Lcom/google/maps/b/a/k;->a()Lcom/google/maps/b/a/co;

    move-result-object v2

    iget-object v2, v2, Lcom/google/maps/b/a/co;->b:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_7

    .line 526
    new-instance v2, Ljava/io/IOException;

    .line 527
    invoke-virtual {v6}, Lcom/google/maps/b/a/k;->a()Lcom/google/maps/b/a/co;

    move-result-object v3

    iget-object v3, v3, Lcom/google/maps/b/a/co;->b:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x2b

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Unknown vertex resolution "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " at : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 640
    :catch_0
    move-exception v2

    .line 642
    :try_start_3
    new-instance v3, Ljava/io/IOException;

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x2d

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Error in unpacking unified tile with coords: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 644
    :catchall_0
    move-exception v2

    if-eqz v7, :cond_6

    invoke-static {v7}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    :cond_6
    throw v2

    .line 529
    :cond_7
    :try_start_4
    iput-object v8, v4, Lcom/google/android/apps/gmm/map/internal/c/co;->f:Lcom/google/android/apps/gmm/map/internal/c/bs;

    .line 531
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 532
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 533
    invoke-virtual {v6}, Lcom/google/maps/b/a/k;->i()Lcom/google/maps/b/a/br;

    move-result-object v2

    new-instance v9, Lcom/google/maps/b/a/bt;

    invoke-direct {v9, v2}, Lcom/google/maps/b/a/bt;-><init>(Lcom/google/maps/b/a/br;)V

    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/b/a/bq;

    .line 534
    iget-object v10, v2, Lcom/google/maps/b/a/bq;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v10, v10, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v10, :cond_8

    .line 537
    invoke-virtual {v2}, Lcom/google/maps/b/a/bq;->b()Lcom/google/maps/b/a/cu;

    move-result-object v10

    const/4 v11, 0x2

    .line 538
    iget-object v12, v8, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    .line 537
    invoke-static {v10, v11, v12}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(Lcom/google/maps/b/a/cu;ILcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v10

    .line 542
    new-instance v11, Lcom/google/android/apps/gmm/map/internal/c/i;

    .line 543
    invoke-virtual {v2}, Lcom/google/maps/b/a/bq;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v12, Lcom/google/android/apps/gmm/map/b/a/ae;

    const/4 v13, 0x0

    .line 546
    invoke-virtual {v10, v13}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v13

    iget v13, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v14, v13

    const-wide v16, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double v14, v14, v16

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    invoke-static {v14, v15}, Ljava/lang/Math;->exp(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->atan(D)D

    move-result-wide v14

    const-wide v18, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double v14, v14, v18

    mul-double v14, v14, v16

    const-wide v16, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double v14, v14, v16

    const/4 v13, 0x0

    .line 547
    invoke-virtual {v10, v13}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v16

    .line 545
    invoke-static/range {v14 .. v17}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v13

    const/4 v14, 0x1

    .line 549
    invoke-virtual {v10, v14}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v14

    iget v14, v14, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v14, v14

    const-wide v16, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double v14, v14, v16

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    invoke-static {v14, v15}, Ljava/lang/Math;->exp(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->atan(D)D

    move-result-wide v14

    const-wide v18, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double v14, v14, v18

    mul-double v14, v14, v16

    const-wide v16, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double v14, v14, v16

    const/16 v16, 0x1

    .line 550
    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v16

    .line 548
    invoke-static/range {v14 .. v17}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v10

    invoke-direct {v12, v13, v10}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    invoke-direct {v11, v2, v12}, Lcom/google/android/apps/gmm/map/internal/c/i;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/ae;)V

    .line 542
    invoke-interface {v3, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 553
    :cond_8
    invoke-virtual {v2}, Lcom/google/maps/b/a/bq;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 557
    :cond_9
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/apps/gmm/map/internal/c/i;

    invoke-interface {v3, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/apps/gmm/map/internal/c/i;

    .line 556
    iput-object v2, v4, Lcom/google/android/apps/gmm/map/internal/c/co;->m:[Lcom/google/android/apps/gmm/map/internal/c/i;

    .line 559
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v5, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 558
    iput-object v2, v4, Lcom/google/android/apps/gmm/map/internal/c/co;->h:[Ljava/lang/String;

    .line 561
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 562
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    .line 563
    invoke-virtual {v6}, Lcom/google/maps/b/a/k;->g()Lcom/google/maps/b/a/bv;

    move-result-object v2

    new-instance v3, Lcom/google/maps/b/a/bx;

    invoke-direct {v3, v2}, Lcom/google/maps/b/a/bx;-><init>(Lcom/google/maps/b/a/bv;)V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_a
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/b/a/bu;

    .line 564
    iget-object v3, v2, Lcom/google/maps/b/a/bu;->a:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    invoke-virtual {v8, v3}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v3

    .line 565
    iget-object v10, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v10, v10

    if-nez v10, :cond_b

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v3

    .line 566
    :goto_3
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/be;->o:[F

    if-eqz v3, :cond_a

    .line 567
    invoke-static {v2, v8}, Lcom/google/android/apps/gmm/map/internal/c/ax;->a(Lcom/google/maps/b/a/bu;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/ax;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 565
    :cond_b
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v10, 0x0

    aget-object v3, v3, v10

    goto :goto_3

    .line 570
    :cond_c
    invoke-virtual {v6}, Lcom/google/maps/b/a/k;->d()Lcom/google/maps/b/a/f;

    move-result-object v2

    new-instance v3, Lcom/google/maps/b/a/h;

    invoke-direct {v3, v2}, Lcom/google/maps/b/a/h;-><init>(Lcom/google/maps/b/a/f;)V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_d
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/b/a/e;

    .line 571
    iget-object v3, v2, Lcom/google/maps/b/a/e;->b:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    invoke-virtual {v8, v3}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v3

    iget-object v10, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v10, v10

    if-nez v10, :cond_f

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v3

    .line 572
    :goto_5
    iget-object v10, v3, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    if-nez v10, :cond_e

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v3, :cond_e

    .line 573
    iget-object v3, v2, Lcom/google/maps/b/a/e;->f:Lcom/google/maps/b/a/cr;

    iget-boolean v3, v3, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v3, :cond_d

    .line 574
    :cond_e
    invoke-static {v2, v8}, Lcom/google/android/apps/gmm/map/internal/c/h;->a(Lcom/google/maps/b/a/e;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/h;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 571
    :cond_f
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v10, 0x0

    aget-object v3, v3, v10

    goto :goto_5

    .line 577
    :cond_10
    invoke-virtual {v6}, Lcom/google/maps/b/a/k;->e()Lcom/google/maps/b/a/dh;

    move-result-object v2

    new-instance v3, Lcom/google/maps/b/a/dj;

    invoke-direct {v3, v2}, Lcom/google/maps/b/a/dj;-><init>(Lcom/google/maps/b/a/dh;)V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_11
    :goto_6
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/b/a/de;

    .line 578
    iget-object v3, v2, Lcom/google/maps/b/a/de;->d:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    invoke-virtual {v8, v3}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v3

    .line 579
    iget-object v10, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v10, v10

    if-nez v10, :cond_13

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v3

    .line 580
    :goto_7
    iget-object v10, v3, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    if-nez v10, :cond_12

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/be;->h:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-eqz v3, :cond_11

    .line 581
    :cond_12
    iget-object v3, v2, Lcom/google/maps/b/a/de;->a:Lcom/google/maps/b/a/cs;

    iget v3, v3, Lcom/google/maps/b/a/cs;->b:I

    if-lez v3, :cond_14

    .line 582
    invoke-static {v2, v8}, Lcom/google/android/apps/gmm/map/internal/c/j;->a(Lcom/google/maps/b/a/de;Lcom/google/android/apps/gmm/map/internal/c/bs;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/j;

    .line 583
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 579
    :cond_13
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v10, 0x0

    aget-object v3, v3, v10

    goto :goto_7

    .line 585
    :cond_14
    iget-object v3, v2, Lcom/google/maps/b/a/de;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v3, v3, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v3, :cond_11

    .line 586
    invoke-static {v2, v8}, Lcom/google/android/apps/gmm/map/internal/c/bc;->a(Lcom/google/maps/b/a/de;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/bc;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 590
    :cond_15
    invoke-virtual {v6}, Lcom/google/maps/b/a/k;->b()Lcom/google/maps/b/a/bb;

    move-result-object v2

    new-instance v3, Lcom/google/maps/b/a/bd;

    invoke-direct {v3, v2}, Lcom/google/maps/b/a/bd;-><init>(Lcom/google/maps/b/a/bb;)V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_16
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/b/a/ba;

    .line 591
    iget-object v3, v2, Lcom/google/maps/b/a/ba;->f:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    invoke-virtual {v8, v3}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v3

    .line 592
    iget-object v10, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v10, v10

    if-nez v10, :cond_17

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v3

    .line 593
    :goto_9
    iget-object v10, v3, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-eqz v10, :cond_16

    .line 596
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/c/be;->e()Z

    move-result v3

    if-eqz v3, :cond_18

    .line 597
    invoke-static {v2, v8}, Lcom/google/android/apps/gmm/map/internal/c/ad;->a(Lcom/google/maps/b/a/ba;Lcom/google/android/apps/gmm/map/internal/c/bs;)[Lcom/google/android/apps/gmm/map/internal/c/ad;

    move-result-object v3

    array-length v10, v3

    const/4 v2, 0x0

    :goto_a
    if-ge v2, v10, :cond_16

    aget-object v11, v3, v2

    .line 598
    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 597
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    .line 592
    :cond_17
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v10, 0x0

    aget-object v3, v3, v10

    goto :goto_9

    .line 601
    :cond_18
    invoke-static {v2, v8}, Lcom/google/android/apps/gmm/map/internal/c/az;->a(Lcom/google/maps/b/a/ba;Lcom/google/android/apps/gmm/map/internal/c/bs;)[Lcom/google/android/apps/gmm/map/internal/c/az;

    move-result-object v3

    array-length v10, v3

    const/4 v2, 0x0

    :goto_b
    if-ge v2, v10, :cond_16

    aget-object v11, v3, v2

    .line 602
    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 601
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 607
    :cond_19
    invoke-virtual {v6}, Lcom/google/maps/b/a/k;->f()Lcom/google/maps/b/a/ap;

    move-result-object v2

    new-instance v3, Lcom/google/maps/b/a/ar;

    invoke-direct {v3, v2}, Lcom/google/maps/b/a/ar;-><init>(Lcom/google/maps/b/a/ap;)V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1a
    :goto_c
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/b/a/ao;

    .line 608
    iget-object v3, v2, Lcom/google/maps/b/a/ao;->i:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    invoke-virtual {v8, v3}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v3

    .line 609
    iget-object v10, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v10, v10

    if-nez v10, :cond_1b

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v3

    .line 610
    :goto_d
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    if-eqz v3, :cond_1a

    .line 611
    iget-object v3, v2, Lcom/google/maps/b/a/ao;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v3, v3, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v3, :cond_1c

    .line 612
    invoke-static {v2, v8}, Lcom/google/android/apps/gmm/map/internal/c/an;->a(Lcom/google/maps/b/a/ao;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/an;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 609
    :cond_1b
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v10, 0x0

    aget-object v3, v3, v10

    goto :goto_d

    .line 613
    :cond_1c
    iget-object v3, v2, Lcom/google/maps/b/a/ao;->c:Lcom/google/maps/b/a/cr;

    iget-boolean v3, v3, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v3, :cond_1a

    .line 614
    invoke-static {v2, v8}, Lcom/google/android/apps/gmm/map/internal/c/ae;->a(Lcom/google/maps/b/a/ao;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/ae;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 618
    :cond_1d
    invoke-virtual {v6}, Lcom/google/maps/b/a/k;->h()Lcom/google/maps/b/a/ce;

    move-result-object v2

    new-instance v3, Lcom/google/maps/b/a/cg;

    invoke-direct {v3, v2}, Lcom/google/maps/b/a/cg;-><init>(Lcom/google/maps/b/a/ce;)V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1e
    :goto_e
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_21

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/b/a/cd;

    .line 619
    iget-object v3, v2, Lcom/google/maps/b/a/cd;->a:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    invoke-virtual {v8, v3}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v3

    .line 620
    iget-object v10, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v10, v10

    if-nez v10, :cond_20

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v3

    .line 621
    :goto_f
    iget v10, v3, Lcom/google/android/apps/gmm/map/internal/c/be;->m:I

    if-nez v10, :cond_1f

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/be;->n:I

    if-eqz v3, :cond_1e

    .line 622
    :cond_1f
    invoke-static {v2, v8}, Lcom/google/android/apps/gmm/map/internal/c/ba;->a(Lcom/google/maps/b/a/cd;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/ba;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_e

    .line 620
    :cond_20
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v10, 0x0

    aget-object v3, v3, v10

    goto :goto_f

    .line 627
    :cond_21
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/cm;->m:Ljava/util/Comparator;

    invoke-static {v5, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 628
    iput-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/co;->e:Ljava/util/List;

    .line 630
    iget-object v2, v6, Lcom/google/maps/b/a/k;->c:Lcom/google/maps/b/a/cs;

    iget v2, v2, Lcom/google/maps/b/a/cs;->b:I

    if-lez v2, :cond_23

    .line 631
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 632
    new-instance v2, Lcom/google/maps/b/a/o;

    invoke-direct {v2, v6}, Lcom/google/maps/b/a/o;-><init>(Lcom/google/maps/b/a/k;)V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_10
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_22

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/b/a/ca;

    .line 633
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/cj;->a(Lcom/google/maps/b/a/ca;Ljava/util/List;)V

    goto :goto_10

    .line 635
    :cond_22
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_23

    .line 636
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/apps/gmm/map/internal/c/cj;

    invoke-interface {v3, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/apps/gmm/map/internal/c/cj;

    iput-object v2, v4, Lcom/google/android/apps/gmm/map/internal/c/co;->j:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    .line 639
    :cond_23
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/c/co;->a()Lcom/google/android/apps/gmm/map/internal/c/cm;
    :try_end_4
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    .line 644
    if-eqz v7, :cond_2

    invoke-static {v7}, Lcom/google/android/apps/gmm/map/util/h;->a(Lcom/google/android/apps/gmm/map/util/f;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 1

    .prologue
    .line 842
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    return-object v0
.end method

.method public a(I)Lcom/google/android/apps/gmm/map/internal/c/m;
    .locals 1

    .prologue
    .line 883
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/m;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/map/b/a/ai;
    .locals 1

    .prologue
    .line 1252
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 847
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->j:Lcom/google/android/apps/gmm/map/internal/c/ct;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/ct;->i:I

    return v0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/internal/c/bt;
    .locals 1

    .prologue
    .line 837
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->j:Lcom/google/android/apps/gmm/map/internal/c/ct;

    return-object v0
.end method

.method public e()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 856
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public f()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->l:[Ljava/lang/String;

    return-object v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public h()Lcom/google/android/apps/gmm/map/internal/c/cp;
    .locals 1

    .prologue
    .line 891
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/cq;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/c/cq;-><init>(Lcom/google/android/apps/gmm/map/internal/c/cm;)V

    return-object v0
.end method
