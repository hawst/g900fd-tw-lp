.class public abstract Lcom/google/android/apps/gmm/v/bi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:[I

.field public volatile h:Lcom/google/android/apps/gmm/v/bj;

.field public i:Z

.field public j:[F

.field final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/v/n;",
            ">;"
        }
    .end annotation
.end field

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Lcom/google/android/apps/gmm/v/be;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 90
    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/gmm/v/bi;-><init>(II)V

    .line 91
    return-void
.end method

.method public constructor <init>(II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/v/bj;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/gmm/v/bj;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->h:Lcom/google/android/apps/gmm/v/bj;

    .line 49
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/v/bi;->i:Z

    .line 54
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->j:[F

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->k:Ljava/util/List;

    .line 66
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/v/bi;->l:Z

    .line 72
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/v/bi;->m:Z

    .line 77
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/v/bi;->n:Z

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->o:Lcom/google/android/apps/gmm/v/be;

    .line 84
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->a:[I

    .line 97
    new-instance v0, Lcom/google/android/apps/gmm/v/bj;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/gmm/v/bj;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->h:Lcom/google/android/apps/gmm/v/bj;

    .line 98
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v2, v2, v2, v0}, Lcom/google/android/apps/gmm/v/bi;->a(FFFF)V

    .line 99
    return-void
.end method


# virtual methods
.method public final a(FFFF)V
    .locals 2

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/bi;->i:Z

    if-eqz v0, :cond_0

    .line 107
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->j:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->j:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->j:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->j:[F

    const/4 v1, 0x3

    aput p4, v0, v1

    .line 119
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 243
    new-instance v0, Lcom/google/android/apps/gmm/v/bj;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/gmm/v/bj;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->h:Lcom/google/android/apps/gmm/v/bj;

    .line 245
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bi;->k:Ljava/util/List;

    monitor-enter v1

    .line 246
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/n;

    .line 250
    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/apps/gmm/v/n;->a(Lcom/google/android/apps/gmm/v/bi;II)V

    goto :goto_0

    .line 253
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/v/aq;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->j:[F

    aget v0, v0, v5

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bi;->j:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/bi;->j:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/bi;->j:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    iget v4, p1, Lcom/google/android/apps/gmm/v/aq;->c:F

    cmpl-float v4, v0, v4

    if-nez v4, :cond_0

    iget v4, p1, Lcom/google/android/apps/gmm/v/aq;->d:F

    cmpl-float v4, v1, v4

    if-nez v4, :cond_0

    iget v4, p1, Lcom/google/android/apps/gmm/v/aq;->e:F

    cmpl-float v4, v2, v4

    if-nez v4, :cond_0

    iget v4, p1, Lcom/google/android/apps/gmm/v/aq;->f:F

    cmpl-float v4, v3, v4

    if-eqz v4, :cond_1

    :cond_0
    iput v0, p1, Lcom/google/android/apps/gmm/v/aq;->c:F

    iput v1, p1, Lcom/google/android/apps/gmm/v/aq;->d:F

    iput v2, p1, Lcom/google/android/apps/gmm/v/aq;->e:F

    iput v3, p1, Lcom/google/android/apps/gmm/v/aq;->f:F

    iget v0, p1, Lcom/google/android/apps/gmm/v/aq;->c:F

    iget v1, p1, Lcom/google/android/apps/gmm/v/aq;->d:F

    iget v2, p1, Lcom/google/android/apps/gmm/v/aq;->e:F

    iget v3, p1, Lcom/google/android/apps/gmm/v/aq;->f:F

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 196
    :cond_1
    const/16 v0, 0xb98

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bi;->a:[I

    invoke-static {v0, v1, v5}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 197
    const/4 v0, -0x1

    invoke-static {v0}, Landroid/opengl/GLES20;->glStencilMask(I)V

    .line 198
    invoke-static {v5}, Landroid/opengl/GLES20;->glClearStencil(I)V

    .line 199
    const/16 v0, 0x4500

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 201
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/bi;->l:Z

    if-eqz v0, :cond_2

    .line 202
    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 205
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->a:[I

    aget v0, v0, v5

    invoke-static {v0}, Landroid/opengl/GLES20;->glStencilMask(I)V

    .line 209
    return-void
.end method

.method public b(Lcom/google/android/apps/gmm/v/aq;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->h:Lcom/google/android/apps/gmm/v/bj;

    iget v0, v0, Lcom/google/android/apps/gmm/v/bj;->a:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bi;->h:Lcom/google/android/apps/gmm/v/bj;

    iget v1, v1, Lcom/google/android/apps/gmm/v/bj;->b:I

    invoke-virtual {p1, v2, v2, v0, v1}, Lcom/google/android/apps/gmm/v/aq;->a(IIII)V

    .line 228
    return-void
.end method

.method public f()I
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->h:Lcom/google/android/apps/gmm/v/bj;

    iget v0, v0, Lcom/google/android/apps/gmm/v/bj;->a:I

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->h:Lcom/google/android/apps/gmm/v/bj;

    iget v0, v0, Lcom/google/android/apps/gmm/v/bj;->b:I

    return v0
.end method
