.class public Lcom/google/android/apps/gmm/startpage/f/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/startpage/e/a;


# instance fields
.field private final a:Lcom/google/o/h/a/hb;

.field private final b:Ljava/lang/Integer;

.field private final c:Lcom/google/android/apps/gmm/base/views/c/k;

.field private final d:Lcom/google/android/apps/gmm/base/views/c/k;

.field private final e:Lcom/google/android/apps/gmm/startpage/e/b;

.field private final f:Lcom/google/android/apps/gmm/startpage/e/b;

.field private final g:Lcom/google/android/apps/gmm/startpage/e/e;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/o/h/a/hb;Ljava/lang/Integer;Lcom/google/android/apps/gmm/base/views/c/k;Lcom/google/android/apps/gmm/base/views/c/k;Lcom/google/android/apps/gmm/startpage/e/b;Lcom/google/android/apps/gmm/startpage/e/b;Lcom/google/android/apps/gmm/startpage/e/e;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/f/a;->a:Lcom/google/o/h/a/hb;

    .line 44
    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/f/a;->b:Ljava/lang/Integer;

    .line 45
    iput-object p4, p0, Lcom/google/android/apps/gmm/startpage/f/a;->c:Lcom/google/android/apps/gmm/base/views/c/k;

    .line 46
    iput-object p5, p0, Lcom/google/android/apps/gmm/startpage/f/a;->d:Lcom/google/android/apps/gmm/base/views/c/k;

    .line 47
    iput-object p6, p0, Lcom/google/android/apps/gmm/startpage/f/a;->e:Lcom/google/android/apps/gmm/startpage/e/b;

    .line 48
    iput-object p7, p0, Lcom/google/android/apps/gmm/startpage/f/a;->f:Lcom/google/android/apps/gmm/startpage/e/b;

    .line 49
    iput-object p8, p0, Lcom/google/android/apps/gmm/startpage/f/a;->g:Lcom/google/android/apps/gmm/startpage/e/e;

    .line 50
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/o/h/a/hb;Ljava/lang/String;Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/android/apps/gmm/startpage/e/c;Lcom/google/android/apps/gmm/map/s/a;)Lcom/google/android/apps/gmm/startpage/f/a;
    .locals 9

    .prologue
    const/16 v8, 0xfa

    const/4 v7, 0x0

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/startpage/f/a;

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v1, p1, Lcom/google/o/h/a/hb;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/dt;->d()Lcom/google/o/h/a/dt;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/dt;

    sget v3, Lcom/google/android/apps/gmm/d;->b:I

    .line 57
    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/base/views/c/k;

    .line 60
    iget-object v1, p1, Lcom/google/o/h/a/hb;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/hv;

    invoke-virtual {v1}, Lcom/google/o/h/a/hv;->d()Ljava/lang/String;

    move-result-object v2

    .line 61
    iget-object v1, p1, Lcom/google/o/h/a/hb;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/hv;

    iget v1, v1, Lcom/google/o/h/a/hv;->d:I

    invoke-static {v1}, Lcom/google/o/h/a/ia;->a(I)Lcom/google/o/h/a/ia;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/o/h/a/ia;->a:Lcom/google/o/h/a/ia;

    :cond_0
    invoke-static {v1}, Lcom/google/android/apps/gmm/base/views/b/a;->a(Lcom/google/o/h/a/ia;)Lcom/google/android/apps/gmm/util/webimageview/b;

    move-result-object v1

    invoke-direct {v4, v2, v1, v7, v8}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;II)V

    new-instance v5, Lcom/google/android/apps/gmm/base/views/c/k;

    .line 64
    iget-object v1, p1, Lcom/google/o/h/a/hb;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/he;->d()Lcom/google/o/h/a/he;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/he;

    iget-object v2, v1, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    if-nez v2, :cond_4

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v1

    :goto_2
    invoke-virtual {v1}, Lcom/google/o/h/a/hv;->d()Ljava/lang/String;

    move-result-object v2

    .line 66
    iget-object v1, p1, Lcom/google/o/h/a/hb;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/he;->d()Lcom/google/o/h/a/he;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/he;

    iget-object v6, v1, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    if-nez v6, :cond_5

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v1

    :goto_3
    iget v1, v1, Lcom/google/o/h/a/hv;->d:I

    invoke-static {v1}, Lcom/google/o/h/a/ia;->a(I)Lcom/google/o/h/a/ia;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/o/h/a/ia;->a:Lcom/google/o/h/a/ia;

    .line 65
    :cond_1
    invoke-static {v1}, Lcom/google/android/apps/gmm/base/views/b/a;->a(Lcom/google/o/h/a/ia;)Lcom/google/android/apps/gmm/util/webimageview/b;

    move-result-object v1

    invoke-direct {v5, v2, v1, v7, v8}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;II)V

    new-instance v6, Lcom/google/android/apps/gmm/startpage/f/c;

    .line 69
    iget-object v1, p1, Lcom/google/o/h/a/hb;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hi;->d()Lcom/google/o/h/a/hi;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/hi;

    invoke-direct {v6, v1, p2, p4}, Lcom/google/android/apps/gmm/startpage/f/c;-><init>(Lcom/google/o/h/a/hi;Ljava/lang/String;Lcom/google/android/apps/gmm/startpage/e/c;)V

    new-instance v7, Lcom/google/android/apps/gmm/startpage/f/c;

    .line 71
    iget-object v1, p1, Lcom/google/o/h/a/hb;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hi;->d()Lcom/google/o/h/a/hi;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/hi;

    invoke-direct {v7, v1, p2, p4}, Lcom/google/android/apps/gmm/startpage/f/c;-><init>(Lcom/google/o/h/a/hi;Ljava/lang/String;Lcom/google/android/apps/gmm/startpage/e/c;)V

    new-instance v8, Lcom/google/android/apps/gmm/startpage/f/g;

    invoke-direct {v8, p0, p3, p1, p5}, Lcom/google/android/apps/gmm/startpage/f/g;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/o/h/a/hb;Lcom/google/android/apps/gmm/map/s/a;)V

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/startpage/f/a;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/o/h/a/hb;Ljava/lang/Integer;Lcom/google/android/apps/gmm/base/views/c/k;Lcom/google/android/apps/gmm/base/views/c/k;Lcom/google/android/apps/gmm/startpage/e/b;Lcom/google/android/apps/gmm/startpage/e/b;Lcom/google/android/apps/gmm/startpage/e/e;)V

    return-object v0

    .line 57
    :cond_2
    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Landroid/content/res/Resources;Lcom/google/o/h/a/dt;)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto/16 :goto_1

    .line 64
    :cond_4
    iget-object v1, v1, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    goto :goto_2

    .line 66
    :cond_5
    iget-object v1, v1, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    goto :goto_3
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/a;->a:Lcom/google/o/h/a/hb;

    iget v0, v0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/a;->a:Lcom/google/o/h/a/hb;

    invoke-virtual {v0}, Lcom/google/o/h/a/hb;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/a;->a:Lcom/google/o/h/a/hb;

    iget-object v0, v0, Lcom/google/o/h/a/hb;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/he;->d()Lcom/google/o/h/a/he;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/he;

    iget-object v1, v0, Lcom/google/o/h/a/he;->b:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :goto_0
    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0

    :cond_1
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iput-object v2, v0, Lcom/google/o/h/a/he;->b:Ljava/lang/Object;

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method public final d()Ljava/lang/Integer;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/a;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/a;->c:Lcom/google/android/apps/gmm/base/views/c/k;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/a;->d:Lcom/google/android/apps/gmm/base/views/c/k;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/startpage/e/b;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/a;->e:Lcom/google/android/apps/gmm/startpage/e/b;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/gmm/startpage/e/b;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/a;->f:Lcom/google/android/apps/gmm/startpage/e/b;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 81
    const-class v0, Lcom/google/android/apps/gmm/startpage/f/a;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i()Lcom/google/android/apps/gmm/startpage/e/e;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/a;->g:Lcom/google/android/apps/gmm/startpage/e/e;

    return-object v0
.end method
