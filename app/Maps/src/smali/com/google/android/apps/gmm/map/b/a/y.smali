.class public Lcom/google/android/apps/gmm/map/b/a/y;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/map/b/a/y;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput p1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 68
    iput p2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 69
    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput p1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 73
    iput p2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 74
    iput p3, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 79
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 80
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 81
    return-void
.end method

.method public static a(D)D
    .locals 4

    .prologue
    .line 215
    const-wide v0, 0x41731680b1202bfeL    # 2.0015115070354454E7

    const-wide v2, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v2, p0

    .line 216
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    .line 217
    const-wide/high16 v2, 0x41c0000000000000L    # 5.36870912E8

    div-double v0, v2, v0

    return-wide v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)D
    .locals 4

    .prologue
    .line 765
    invoke-virtual {p1, p0}, Lcom/google/android/apps/gmm/map/b/a/y;->g(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    .line 766
    iget v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-double v2, v1

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    const-wide v2, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v0, v2

    .line 768
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    .line 769
    const-wide v2, 0x4076800000000000L    # 360.0

    add-double/2addr v0, v2

    .line 771
    :cond_0
    return-wide v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F
    .locals 1

    .prologue
    .line 920
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0, p3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;ZLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 921
    invoke-virtual {p2, p3}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    return v0
.end method

.method public static a(I)I
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 949
    move v0, p0

    :goto_0
    const/high16 v1, -0x20000000

    if-ge v0, v1, :cond_0

    .line 950
    add-int/2addr v0, v2

    goto :goto_0

    .line 952
    :cond_0
    :goto_1
    const/high16 v1, 0x20000000

    if-lt v0, v1, :cond_1

    .line 953
    sub-int/2addr v0, v2

    goto :goto_1

    .line 955
    :cond_1
    return v0
.end method

.method public static a(DD)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 8

    .prologue
    const-wide v4, 0x41a45f306dc9c883L    # 1.708913188941079E8

    const-wide v2, 0x3f91df46a2529d39L    # 0.017453292519943295

    .line 120
    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 121
    mul-double v0, p0, v2

    mul-double/2addr v2, p2

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v7, v2

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v0, v2

    const-wide v2, 0x3fe921fb54442d18L    # 0.7853981633974483

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    mul-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    const-wide/32 v2, -0x80000000

    const-wide/32 v4, 0x7fffffff

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/shared/c/s;->a(JJJ)J

    move-result-wide v0

    long-to-int v0, v0

    iput v7, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v0, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v0, 0x0

    iput v0, v6, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 122
    return-object v6
.end method

.method public static a(FF)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 8

    .prologue
    .line 156
    const v0, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v0, p0

    const/high16 v1, 0x43340000    # 180.0f

    div-float/2addr v0, v1

    .line 157
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    float-to-double v2, p1

    float-to-double v4, v0

    .line 158
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-int v2, v2

    float-to-double v4, p1

    float-to-double v6, v0

    .line 159
    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-int v0, v4

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    return-object v1
.end method

.method public static a(II)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 6

    .prologue
    const-wide v4, 0x3e7ad7f29abcaf48L    # 1.0E-7

    .line 90
    int-to-double v0, p0

    mul-double/2addr v0, v4

    int-to-double v2, p1

    mul-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/n;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 6

    .prologue
    const-wide v4, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    .line 130
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/n;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/n;->b:I

    int-to-double v2, v0

    mul-double/2addr v2, v4

    int-to-double v0, v1

    mul-double/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 4

    .prologue
    .line 126
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 4

    .prologue
    .line 226
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    return-object v0
.end method

.method public static a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 6

    .prologue
    const/16 v2, 0x15

    const-wide v4, 0x3e7ad7f29abcaf48L    # 1.0E-7

    .line 139
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v1, v0

    .line 140
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v0, v2

    .line 141
    int-to-double v2, v1

    mul-double/2addr v2, v4

    int-to-double v0, v0

    mul-double/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/maps/b/a/bm;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 6

    .prologue
    const-wide v4, 0x3e7ad7f29abcaf48L    # 1.0E-7

    .line 150
    iget-object v0, p0, Lcom/google/maps/b/a/bm;->a:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 151
    iget-object v1, p0, Lcom/google/maps/b/a/bm;->b:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    .line 152
    int-to-double v2, v0

    mul-double/2addr v2, v4

    int-to-double v0, v1

    mul-double/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/maps/b/a/cu;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 290
    iget-object v0, p0, Lcom/google/maps/b/a/cu;->a:[B

    iget v1, p0, Lcom/google/maps/b/a/cu;->b:I

    iget v2, p0, Lcom/google/maps/b/a/cu;->c:I

    .line 291
    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->d:I

    move v5, v4

    .line 290
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/b/a/o;->a([BIIIII)[I

    move-result-object v0

    .line 293
    aget v1, v0, v6

    rsub-int v1, v1, 0x1000

    aput v1, v0, v6

    .line 294
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    if-gez v1, :cond_0

    .line 295
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    neg-int v1, v1

    .line 296
    aget v2, v0, v7

    shr-int/2addr v2, v1

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->a:I

    add-int/2addr v2, v3

    .line 297
    aget v0, v0, v6

    shr-int/2addr v0, v1

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->b:I

    add-int/2addr v1, v0

    .line 298
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 303
    :goto_0
    return-object v0

    .line 300
    :cond_0
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    .line 301
    aget v2, v0, v7

    shl-int/2addr v2, v1

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->a:I

    add-int/2addr v2, v3

    .line 302
    aget v0, v0, v6

    shl-int/2addr v0, v1

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->b:I

    add-int/2addr v1, v0

    .line 303
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    goto :goto_0
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 3

    .prologue
    .line 239
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    if-gez v0, :cond_0

    .line 240
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    neg-int v0, v0

    .line 241
    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v1

    shr-int/2addr v1, v0

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->a:I

    add-int/2addr v1, v2

    .line 242
    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v2

    shr-int v0, v2, v0

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->b:I

    add-int/2addr v2, v0

    .line 243
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 248
    :goto_0
    return-object v0

    .line 245
    :cond_0
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    .line 246
    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v1

    shl-int/2addr v1, v0

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->a:I

    add-int/2addr v1, v2

    .line 247
    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v2

    shl-int v0, v2, v0

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->b:I

    add-int/2addr v2, v0

    .line 248
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 1

    .prologue
    .line 713
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 714
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 715
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 716
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 2

    .prologue
    .line 841
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v0, v1

    iput v0, p3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 842
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v0, v1

    iput v0, p3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 843
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    add-int/2addr v0, v1

    iput v0, p3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 844
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 2

    .prologue
    .line 653
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v0, v1

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 654
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v0, v1

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 655
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    add-int/2addr v0, v1

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 656
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;ZLcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 2

    .prologue
    .line 887
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    .line 889
    if-eqz p3, :cond_2

    .line 892
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    .line 893
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 902
    :goto_0
    return-void

    .line 894
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    .line 895
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto :goto_0

    .line 897
    :cond_1
    invoke-static {p0, p1, v0, p4}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    goto :goto_0

    .line 900
    :cond_2
    invoke-static {p0, p1, v0, p4}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    goto :goto_0
.end method

.method static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;[II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 357
    mul-int/lit8 v0, p3, 0x3

    .line 358
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    if-gez v1, :cond_0

    .line 359
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    neg-int v1, v1

    .line 360
    add-int/lit8 v2, v0, 0x1

    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v3

    shr-int/2addr v3, v1

    iget v4, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->a:I

    add-int/2addr v3, v4

    aput v3, p2, v0

    .line 361
    add-int/lit8 v0, v2, 0x1

    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v3

    shr-int v1, v3, v1

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->b:I

    add-int/2addr v1, v3

    aput v1, p2, v2

    .line 362
    aput v5, p2, v0

    .line 369
    :goto_0
    return-void

    .line 364
    :cond_0
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    .line 365
    add-int/lit8 v2, v0, 0x1

    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v3

    shl-int/2addr v3, v1

    iget v4, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->a:I

    add-int/2addr v3, v4

    aput v3, p2, v0

    .line 366
    add-int/lit8 v0, v2, 0x1

    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v3

    shl-int v1, v3, v1

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->b:I

    add-int/2addr v1, v3

    aput v1, p2, v2

    .line 367
    aput v5, p2, v0

    goto :goto_0
.end method

.method static a(Lcom/google/maps/b/a/cu;ILcom/google/android/apps/gmm/map/b/a/aw;)[I
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 377
    iget-object v0, p0, Lcom/google/maps/b/a/cu;->a:[B

    iget v1, p0, Lcom/google/maps/b/a/cu;->b:I

    iget v2, p0, Lcom/google/maps/b/a/cu;->c:I

    .line 378
    iget v3, p2, Lcom/google/android/apps/gmm/map/b/a/aw;->d:I

    const/4 v4, 0x2

    shl-int/lit8 v5, p1, 0x1

    .line 377
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/b/a/o;->a([BIIIII)[I

    move-result-object v2

    .line 379
    array-length v0, v2

    div-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x3

    new-array v3, v0, [I

    move v0, v6

    move v1, v6

    .line 381
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 383
    add-int/lit8 v4, v0, 0x1

    aget v4, v2, v4

    rsub-int v4, v4, 0x1000

    .line 384
    iget v5, p2, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    if-gez v5, :cond_0

    .line 385
    iget v5, p2, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    neg-int v5, v5

    .line 386
    add-int/lit8 v7, v1, 0x1

    aget v8, v2, v0

    shr-int/2addr v8, v5

    iget v9, p2, Lcom/google/android/apps/gmm/map/b/a/aw;->a:I

    add-int/2addr v8, v9

    aput v8, v3, v1

    .line 387
    add-int/lit8 v8, v7, 0x1

    shr-int v1, v4, v5

    iget v4, p2, Lcom/google/android/apps/gmm/map/b/a/aw;->b:I

    add-int/2addr v1, v4

    aput v1, v3, v7

    .line 388
    add-int/lit8 v1, v8, 0x1

    aput v6, v3, v8

    .line 381
    :goto_1
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 390
    :cond_0
    iget v5, p2, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    .line 391
    add-int/lit8 v7, v1, 0x1

    aget v8, v2, v0

    shl-int/2addr v8, v5

    iget v9, p2, Lcom/google/android/apps/gmm/map/b/a/aw;->a:I

    add-int/2addr v8, v9

    aput v8, v3, v1

    .line 392
    add-int/lit8 v8, v7, 0x1

    shl-int v1, v4, v5

    iget v4, p2, Lcom/google/android/apps/gmm/map/b/a/aw;->b:I

    add-int/2addr v1, v4

    aput v1, v3, v7

    .line 393
    add-int/lit8 v1, v8, 0x1

    aput v6, v3, v8

    goto :goto_1

    .line 396
    :cond_1
    return-object v3
.end method

.method public static b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F
    .locals 4

    .prologue
    .line 792
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v0, v0

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    .line 793
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v1, v1

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 794
    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v2, v2

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    .line 795
    add-float/2addr v0, v1

    add-float/2addr v0, v2

    return v0
.end method

.method public static b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F
    .locals 1

    .prologue
    .line 930
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0, p3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;ZLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 931
    invoke-virtual {p2, p3}, Lcom/google/android/apps/gmm/map/b/a/y;->d(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    return v0
.end method

.method public static b(II)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 6

    .prologue
    const-wide v4, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    .line 100
    int-to-double v0, p0

    mul-double/2addr v0, v4

    int-to-double v2, p1

    mul-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 2

    .prologue
    .line 746
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/y;->i()F

    move-result v0

    .line 747
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    div-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 748
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    div-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 749
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    div-float v0, v1, v0

    float-to-int v0, v0

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 750
    return-void
.end method

.method public static b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 2

    .prologue
    .line 681
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v0, v1

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 682
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v0, v1

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 683
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    sub-int/2addr v0, v1

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 684
    return-void
.end method

.method public static b(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;[II)V
    .locals 5

    .prologue
    .line 409
    mul-int/lit8 v0, p3, 0x3

    .line 410
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    if-gez v1, :cond_0

    .line 411
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    neg-int v1, v1

    .line 412
    add-int/lit8 v2, v0, 0x1

    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v3

    shr-int/2addr v3, v1

    iget v4, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->a:I

    add-int/2addr v3, v4

    aput v3, p2, v0

    .line 413
    add-int/lit8 v0, v2, 0x1

    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v3

    shr-int/2addr v3, v1

    iget v4, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->b:I

    add-int/2addr v3, v4

    aput v3, p2, v2

    .line 414
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    shr-int v1, v2, v1

    aput v1, p2, v0

    .line 421
    :goto_0
    return-void

    .line 416
    :cond_0
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->c:I

    .line 417
    add-int/lit8 v2, v0, 0x1

    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v3

    shl-int/2addr v3, v1

    iget v4, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->a:I

    add-int/2addr v3, v4

    aput v3, p2, v0

    .line 418
    add-int/lit8 v0, v2, 0x1

    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v3

    shl-int/2addr v3, v1

    iget v4, p1, Lcom/google/android/apps/gmm/map/b/a/aw;->b:I

    add-int/2addr v3, v4

    aput v3, p2, v2

    .line 419
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    shl-int v1, v2, v1

    aput v1, p2, v0

    goto :goto_0
.end method

.method public static c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F
    .locals 7

    .prologue
    .line 867
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    .line 868
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    .line 869
    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    .line 870
    iget v3, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v4, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    .line 871
    iget v4, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v5, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    .line 872
    iget v5, p2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v6, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    .line 874
    mul-float/2addr v3, v0

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    mul-float v4, v2, v5

    add-float/2addr v3, v4

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    mul-float v1, v2, v2

    add-float/2addr v0, v1

    div-float v0, v3, v0

    return v0
.end method

.method public static c(II)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 6

    .prologue
    const-wide v4, 0x3ee4f8b588e368f1L    # 1.0E-5

    .line 110
    int-to-double v0, p0

    mul-double/2addr v0, v4

    int-to-double v2, p1

    mul-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    return-object v0
.end method

.method public static l()I
    .locals 1

    .prologue
    .line 1106
    const/16 v0, 0x18

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    .line 169
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v0, v0

    const-wide v2, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    const-wide v4, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v0, v4

    mul-double/2addr v0, v2

    const-wide v2, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v0, v2

    const-wide v2, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final a(F)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 3

    .prologue
    .line 702
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 703
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    .line 706
    :goto_0
    return-object p0

    :cond_0
    div-float v0, p1, v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto :goto_0
.end method

.method public final b()I
    .locals 6

    .prologue
    .line 174
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v0, v0

    const-wide v2, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    const-wide v4, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v0, v4

    mul-double/2addr v0, v2

    const-wide v2, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v0, v2

    const-wide v2, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 1

    .prologue
    .line 508
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 509
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 510
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 512
    return-object p0
.end method

.method public final c(Lcom/google/android/apps/gmm/map/b/a/y;)F
    .locals 4

    .prologue
    .line 626
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    .line 627
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    .line 628
    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    .line 629
    mul-float/2addr v0, v0

    .line 630
    mul-float/2addr v1, v1

    .line 631
    mul-float/2addr v2, v2

    .line 632
    add-float/2addr v0, v1

    add-float/2addr v0, v2

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public final c()I
    .locals 4

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v0

    const-wide v2, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 23
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final d(Lcom/google/android/apps/gmm/map/b/a/y;)F
    .locals 4

    .prologue
    .line 640
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    .line 641
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    .line 642
    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    .line 643
    mul-float/2addr v0, v0

    .line 644
    mul-float/2addr v1, v1

    .line 645
    mul-float/2addr v2, v2

    .line 646
    add-float/2addr v0, v1

    add-float/2addr v0, v2

    return v0
.end method

.method public final d()I
    .locals 4

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v0

    const-wide v2, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final d(II)V
    .locals 1

    .prologue
    .line 531
    iput p1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 532
    iput p2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 533
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 534
    return-void
.end method

.method public final e()D
    .locals 6

    .prologue
    const-wide v4, 0x4076800000000000L    # 360.0

    .line 201
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-double v0, v0

    const-wide v2, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v0, v2

    .line 202
    const-wide v2, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v0, v2

    .line 203
    :goto_0
    const-wide v2, 0x4066800000000000L    # 180.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    .line 204
    sub-double/2addr v0, v4

    goto :goto_0

    .line 206
    :cond_0
    :goto_1
    const-wide v2, -0x3f99800000000000L    # -180.0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_1

    .line 207
    add-double/2addr v0, v4

    goto :goto_1

    .line 209
    :cond_1
    return-wide v0
.end method

.method public final e(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 5

    .prologue
    .line 662
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v4, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    add-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1070
    instance-of v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v1, :cond_0

    .line 1071
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1072
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1074
    :cond_0
    return v0
.end method

.method public final f()D
    .locals 6

    .prologue
    .line 222
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v0, v0

    const-wide v2, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    const-wide v4, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v0, v4

    mul-double/2addr v0, v2

    const-wide v2, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v0, v2

    const-wide v2, 0x41731680b1202bfeL    # 2.0015115070354454E7

    const-wide v4, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x41c0000000000000L    # 5.36870912E8

    div-double v0, v2, v0

    return-wide v0
.end method

.method public final f(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 2

    .prologue
    .line 671
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 672
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 673
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 674
    return-object p0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 470
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    return v0
.end method

.method public final g(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 5

    .prologue
    .line 690
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v4, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    sub-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 474
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    return v0
.end method

.method public final h(Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v2, 0x20000000

    const/high16 v0, -0x20000000

    .line 975
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    :goto_0
    if-ge v1, v0, :cond_0

    add-int/2addr v1, v3

    goto :goto_0

    :cond_0
    :goto_1
    if-lt v1, v2, :cond_1

    sub-int/2addr v1, v3

    goto :goto_1

    :cond_1
    iput v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 976
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-ge v1, v0, :cond_2

    :goto_2
    iput v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 977
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 978
    return-void

    .line 976
    :cond_2
    if-lt v1, v2, :cond_3

    const v0, 0x1fffffff

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 1079
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 1080
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 1081
    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 1082
    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    shr-int/lit8 v3, v2, 0xd

    xor-int/2addr v0, v3

    .line 1083
    sub-int/2addr v1, v2

    sub-int/2addr v1, v0

    shl-int/lit8 v3, v0, 0x8

    xor-int/2addr v1, v3

    .line 1084
    sub-int/2addr v2, v0

    sub-int/2addr v2, v1

    shr-int/lit8 v3, v1, 0xd

    xor-int/2addr v2, v3

    .line 1085
    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    shr-int/lit8 v3, v2, 0xc

    xor-int/2addr v0, v3

    .line 1086
    sub-int/2addr v1, v2

    sub-int/2addr v1, v0

    shl-int/lit8 v3, v0, 0x10

    xor-int/2addr v1, v3

    .line 1087
    sub-int/2addr v2, v0

    sub-int/2addr v2, v1

    shr-int/lit8 v3, v1, 0x5

    xor-int/2addr v2, v3

    .line 1088
    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    shr-int/lit8 v3, v2, 0x3

    xor-int/2addr v0, v3

    .line 1089
    sub-int/2addr v1, v2

    sub-int/2addr v1, v0

    shl-int/lit8 v3, v0, 0xa

    xor-int/2addr v1, v3

    .line 1090
    sub-int v0, v2, v0

    sub-int/2addr v0, v1

    shr-int/lit8 v1, v1, 0xf

    xor-int/2addr v0, v1

    .line 1091
    return v0
.end method

.method public final i()F
    .locals 4

    .prologue
    .line 605
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    .line 606
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 607
    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    .line 608
    add-float/2addr v0, v1

    add-float/2addr v0, v2

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public final i(Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1020
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    :goto_0
    const/high16 v1, -0x20000000

    if-ge v0, v1, :cond_0

    add-int/2addr v0, v2

    goto :goto_0

    :cond_0
    :goto_1
    const/high16 v1, 0x20000000

    if-lt v0, v1, :cond_1

    sub-int/2addr v0, v2

    goto :goto_1

    :cond_1
    iput v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 1021
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 1022
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 1023
    return-void
.end method

.method public final j()Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 8

    .prologue
    .line 1061
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v1

    const-wide v4, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    const-wide v6, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v2, v6

    mul-double/2addr v2, v4

    const-wide v4, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v2, v4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    return-object v0
.end method

.method public final j(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1036
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v0, v1

    .line 1037
    const/high16 v1, 0x20000000

    if-le v0, v1, :cond_1

    .line 1038
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    move-object p0, v0

    .line 1042
    :cond_0
    :goto_0
    return-object p0

    .line 1039
    :cond_1
    const/high16 v1, -0x20000000

    if-ge v0, v1, :cond_0

    .line 1040
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 10

    .prologue
    .line 1065
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%f,%f"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v4

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v8

    mul-double/2addr v4, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k(Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 4

    .prologue
    const/high16 v3, 0x20000000

    const v2, 0x1fffffff

    const/high16 v1, -0x20000000

    .line 1050
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-ge v0, v1, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    iput v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 1051
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-ge v0, v1, :cond_2

    :goto_1
    iput v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 1052
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 1053
    return-void

    .line 1050
    :cond_1
    if-lt v0, v3, :cond_0

    move v0, v2

    goto :goto_0

    .line 1051
    :cond_2
    if-lt v0, v3, :cond_3

    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1057
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x25

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
