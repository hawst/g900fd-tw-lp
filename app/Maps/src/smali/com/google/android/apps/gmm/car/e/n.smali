.class Lcom/google/android/apps/gmm/car/e/n;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/e/l;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/e/m;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/e/m;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/e/n;->a:Lcom/google/android/apps/gmm/car/e/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 219
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/n;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/car/e/m;->l:Z

    if-eqz v1, :cond_1

    .line 220
    sget-object v1, Lcom/google/android/apps/gmm/car/e/m;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/n;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/e/m;->g:Lcom/google/android/apps/gmm/car/e/as;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Start on nav focus. @"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/n;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/e/m;->d:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/n;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/e/m;->n:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 222
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/n;->a:Lcom/google/android/apps/gmm/car/e/m;

    iput-object v0, v1, Lcom/google/android/apps/gmm/car/e/m;->m:Lcom/google/android/apps/gmm/car/e/t;

    .line 223
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/n;->a:Lcom/google/android/apps/gmm/car/e/m;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/car/e/m;->l:Z

    .line 224
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/n;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/e/m;->f:Lcom/google/android/apps/gmm/car/e/ap;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/n;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/e/m;->i:Lcom/google/android/apps/gmm/car/bm;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/n;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget v3, v3, Lcom/google/android/apps/gmm/car/e/m;->j:I

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/e/ap;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v1

    iget-object v4, v2, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    if-eqz v4, :cond_0

    iget-object v4, v2, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/directions/a/c;->o()Z

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    :goto_0
    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/navigation/b/d;->a(Lcom/google/android/apps/gmm/map/r/a/f;I)Lcom/google/android/apps/gmm/navigation/b/d;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/navigation/b/c;->a(Lcom/google/android/apps/gmm/navigation/b/d;)V

    .line 226
    :cond_1
    return-void

    .line 224
    :cond_2
    iget-object v0, v2, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/n;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/e/m;->l:Z

    if-eqz v0, :cond_0

    .line 233
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t happen."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235
    :cond_0
    return-void
.end method
