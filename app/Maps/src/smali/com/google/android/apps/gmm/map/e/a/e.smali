.class public final enum Lcom/google/android/apps/gmm/map/e/a/e;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/e/a/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/e/a/e;

.field public static final enum b:Lcom/google/android/apps/gmm/map/e/a/e;

.field public static final enum c:Lcom/google/android/apps/gmm/map/e/a/e;

.field public static final enum d:Lcom/google/android/apps/gmm/map/e/a/e;

.field public static final enum e:Lcom/google/android/apps/gmm/map/e/a/e;

.field public static final enum f:Lcom/google/android/apps/gmm/map/e/a/e;

.field public static final enum g:Lcom/google/android/apps/gmm/map/e/a/e;

.field public static final enum h:Lcom/google/android/apps/gmm/map/e/a/e;

.field private static final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/e/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic k:[Lcom/google/android/apps/gmm/map/e/a/e;


# instance fields
.field public i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 9
    new-instance v1, Lcom/google/android/apps/gmm/map/e/a/e;

    const-string v2, "BEHAVIOR"

    const-string v3, "Behavior"

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/map/e/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/e/a/e;->a:Lcom/google/android/apps/gmm/map/e/a/e;

    .line 10
    new-instance v1, Lcom/google/android/apps/gmm/map/e/a/e;

    const-string v2, "EMPTY"

    const-string v3, "empty"

    invoke-direct {v1, v2, v5, v3}, Lcom/google/android/apps/gmm/map/e/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/e/a/e;->b:Lcom/google/android/apps/gmm/map/e/a/e;

    .line 11
    new-instance v1, Lcom/google/android/apps/gmm/map/e/a/e;

    const-string v2, "GC"

    const-string v3, "GC"

    invoke-direct {v1, v2, v6, v3}, Lcom/google/android/apps/gmm/map/e/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/e/a/e;->c:Lcom/google/android/apps/gmm/map/e/a/e;

    .line 12
    new-instance v1, Lcom/google/android/apps/gmm/map/e/a/e;

    const-string v2, "LABELER"

    const-string v3, "Labeler"

    invoke-direct {v1, v2, v7, v3}, Lcom/google/android/apps/gmm/map/e/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/e/a/e;->d:Lcom/google/android/apps/gmm/map/e/a/e;

    .line 13
    new-instance v1, Lcom/google/android/apps/gmm/map/e/a/e;

    const-string v2, "MAIN_UI"

    const-string v3, "MainUi"

    invoke-direct {v1, v2, v8, v3}, Lcom/google/android/apps/gmm/map/e/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/e/a/e;->e:Lcom/google/android/apps/gmm/map/e/a/e;

    .line 14
    new-instance v1, Lcom/google/android/apps/gmm/map/e/a/e;

    const-string v2, "RENDERER"

    const/4 v3, 0x5

    const-string v4, "Renderer"

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/e/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/e/a/e;->f:Lcom/google/android/apps/gmm/map/e/a/e;

    .line 15
    new-instance v1, Lcom/google/android/apps/gmm/map/e/a/e;

    const-string v2, "RENDER_BIN"

    const/4 v3, 0x6

    const-string v4, "RenderBin"

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/e/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/e/a/e;->g:Lcom/google/android/apps/gmm/map/e/a/e;

    .line 16
    new-instance v1, Lcom/google/android/apps/gmm/map/e/a/e;

    const-string v2, "TILES"

    const/4 v3, 0x7

    const-string v4, "Tiles"

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/e/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/e/a/e;->h:Lcom/google/android/apps/gmm/map/e/a/e;

    .line 7
    const/16 v1, 0x8

    new-array v1, v1, [Lcom/google/android/apps/gmm/map/e/a/e;

    sget-object v2, Lcom/google/android/apps/gmm/map/e/a/e;->a:Lcom/google/android/apps/gmm/map/e/a/e;

    aput-object v2, v1, v0

    sget-object v2, Lcom/google/android/apps/gmm/map/e/a/e;->b:Lcom/google/android/apps/gmm/map/e/a/e;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/apps/gmm/map/e/a/e;->c:Lcom/google/android/apps/gmm/map/e/a/e;

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/android/apps/gmm/map/e/a/e;->d:Lcom/google/android/apps/gmm/map/e/a/e;

    aput-object v2, v1, v7

    sget-object v2, Lcom/google/android/apps/gmm/map/e/a/e;->e:Lcom/google/android/apps/gmm/map/e/a/e;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/google/android/apps/gmm/map/e/a/e;->f:Lcom/google/android/apps/gmm/map/e/a/e;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/google/android/apps/gmm/map/e/a/e;->g:Lcom/google/android/apps/gmm/map/e/a/e;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/google/android/apps/gmm/map/e/a/e;->h:Lcom/google/android/apps/gmm/map/e/a/e;

    aput-object v3, v1, v2

    sput-object v1, Lcom/google/android/apps/gmm/map/e/a/e;->k:[Lcom/google/android/apps/gmm/map/e/a/e;

    .line 18
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/google/android/apps/gmm/map/e/a/e;->j:Ljava/util/Map;

    .line 22
    invoke-static {}, Lcom/google/android/apps/gmm/map/e/a/e;->values()[Lcom/google/android/apps/gmm/map/e/a/e;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 23
    sget-object v4, Lcom/google/android/apps/gmm/map/e/a/e;->j:Ljava/util/Map;

    iget-object v5, v3, Lcom/google/android/apps/gmm/map/e/a/e;->i:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 25
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/e/a/e;->i:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/e/a/e;
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/google/android/apps/gmm/map/e/a/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/e/a/e;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/e/a/e;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/google/android/apps/gmm/map/e/a/e;->k:[Lcom/google/android/apps/gmm/map/e/a/e;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/e/a/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/e/a/e;

    return-object v0
.end method
