.class public final Lcom/google/android/apps/gmm/car/m/q;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# instance fields
.field public a:Lcom/google/android/apps/gmm/car/m/i;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/car/m/f;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/m/f;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/car/m/f;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/m/q;->b:Lcom/google/android/apps/gmm/car/m/f;

    .line 24
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/m/q;->b:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/car/m/i;->a(Lcom/google/android/apps/gmm/car/m/k;Lcom/google/android/apps/gmm/car/m/f;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/car/m/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/car/m/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    .line 37
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/i;->a()V

    .line 47
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/q;->b:Lcom/google/android/apps/gmm/car/m/f;

    iget v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    .line 52
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/i;->d()Lcom/google/android/apps/gmm/car/m/h;

    goto :goto_0

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/q;->b:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/f;->a()V

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    .line 59
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    invoke-static {v0}, Lcom/google/android/apps/gmm/car/m/n;->a(Lcom/google/android/apps/gmm/car/m/l;)Lcom/google/android/apps/gmm/car/m/e;

    move-result-object v0

    return-object v0
.end method
