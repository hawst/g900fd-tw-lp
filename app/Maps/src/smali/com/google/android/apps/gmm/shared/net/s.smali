.class public Lcom/google/android/apps/gmm/shared/net/s;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/google/android/apps/gmm/shared/net/r;


# static fields
.field static final a:J

.field private static final q:Lcom/google/android/apps/gmm/shared/net/i;


# instance fields
.field final b:Lcom/google/android/apps/gmm/shared/net/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/f",
            "<",
            "Lcom/google/android/apps/gmm/shared/net/i;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/shared/net/ad;

.field volatile d:Ljava/net/URL;

.field final e:Lcom/google/e/a/a/a/b;

.field final f:Ljava/lang/String;

.field final g:Ljava/lang/String;

.field final h:Ljava/lang/String;

.field final i:Ljava/lang/String;

.field final j:Lcom/google/android/apps/gmm/shared/net/ae;

.field volatile k:Lcom/google/android/apps/gmm/p/d/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field volatile l:Lcom/google/android/apps/gmm/p/d/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field volatile m:Lcom/google/android/apps/gmm/shared/net/q;

.field volatile n:J

.field volatile o:J

.field volatile p:I

.field private final r:Landroid/content/BroadcastReceiver;

.field private final s:Lcom/google/android/apps/gmm/shared/net/z;

.field private final t:Landroid/net/http/AndroidHttpClient;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private volatile u:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 121
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x4

    .line 122
    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/shared/net/s;->a:J

    .line 125
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/t;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/net/t;-><init>(Lcom/google/r/b/a/el;)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/s;->q:Lcom/google/android/apps/gmm/shared/net/i;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/shared/net/ad;Ljava/net/URL;Landroid/net/http/AndroidHttpClient;Lcom/google/e/a/a/a/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/shared/net/ae;)V
    .locals 6
    .param p3    # Landroid/net/http/AndroidHttpClient;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 411
    new-instance v2, Lcom/google/android/apps/gmm/shared/net/v;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/shared/net/v;-><init>(Lcom/google/android/apps/gmm/shared/net/s;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/shared/net/s;->r:Landroid/content/BroadcastReceiver;

    .line 432
    new-instance v2, Lcom/google/android/apps/gmm/shared/net/z;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/shared/net/z;-><init>(Lcom/google/android/apps/gmm/shared/net/s;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/shared/net/s;->s:Lcom/google/android/apps/gmm/shared/net/z;

    .line 447
    new-instance v2, Lcom/google/android/apps/gmm/shared/net/f;

    const/16 v3, 0x64

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/shared/net/f;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    .line 515
    iput-wide v4, p0, Lcom/google/android/apps/gmm/shared/net/s;->n:J

    .line 522
    iput-wide v4, p0, Lcom/google/android/apps/gmm/shared/net/s;->o:J

    .line 529
    iput v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->u:I

    .line 538
    iput v1, p0, Lcom/google/android/apps/gmm/shared/net/s;->p:I

    .line 552
    if-eqz p8, :cond_0

    invoke-virtual {p8}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 554
    :cond_2
    invoke-static {p2}, Lcom/google/android/apps/gmm/shared/net/s;->a(Ljava/net/URL;)V

    .line 556
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 557
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/net/s;->d:Ljava/net/URL;

    .line 558
    iput-object p3, p0, Lcom/google/android/apps/gmm/shared/net/s;->t:Landroid/net/http/AndroidHttpClient;

    .line 559
    iput-object p4, p0, Lcom/google/android/apps/gmm/shared/net/s;->e:Lcom/google/e/a/a/a/b;

    .line 560
    iput-object p5, p0, Lcom/google/android/apps/gmm/shared/net/s;->f:Ljava/lang/String;

    .line 561
    iput-object p6, p0, Lcom/google/android/apps/gmm/shared/net/s;->g:Ljava/lang/String;

    .line 562
    iput-object p7, p0, Lcom/google/android/apps/gmm/shared/net/s;->h:Ljava/lang/String;

    .line 563
    iput-object p8, p0, Lcom/google/android/apps/gmm/shared/net/s;->i:Ljava/lang/String;

    .line 564
    iput-object p9, p0, Lcom/google/android/apps/gmm/shared/net/s;->j:Lcom/google/android/apps/gmm/shared/net/ae;

    .line 565
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/net/ad;Ljava/lang/String;)Lcom/google/android/apps/gmm/shared/net/s;
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x5f

    const/16 v3, 0x2d

    .line 175
    .line 179
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xa

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v4, v6

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v4, v6

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "android:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "-"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/d/a;->c:Ljava/lang/String;

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, v5

    .line 175
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/shared/net/s;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/net/ad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/shared/net/s;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/net/ad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/shared/net/s;
    .locals 12

    .prologue
    .line 204
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 205
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v4

    .line 206
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    .line 208
    invoke-static {p1}, Lcom/google/android/apps/gmm/shared/net/g;->a(Lcom/google/android/apps/gmm/shared/net/ad;)Lcom/google/e/a/a/a/b;

    move-result-object v6

    .line 209
    const/4 v5, 0x4

    sget-object v2, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v7, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v7, v5, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 210
    const/16 v5, 0xa

    sget-object v2, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v7, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v7, v5, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 211
    const/16 v5, 0x1d

    .line 212
    iget-object v2, v4, Lcom/google/android/apps/gmm/map/h/d;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v2

    .line 211
    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    :goto_0
    iget-object v7, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v7, v5, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 213
    const/16 v5, 0x20

    .line 214
    iget-object v2, v4, Lcom/google/android/apps/gmm/map/h/d;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v4, "android.hardware.telephony"

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    .line 213
    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    :goto_1
    iget-object v4, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v5, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 215
    const/16 v4, 0x21

    .line 216
    invoke-static {}, Lcom/google/android/apps/gmm/map/h/d;->b()Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    .line 215
    :goto_2
    if-eqz v2, :cond_5

    sget-object v2, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    :goto_3
    iget-object v5, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v4, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 217
    const/16 v4, 0x16

    .line 219
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v3, 0x12c

    if-le v2, v3, :cond_6

    const/4 v2, 0x4

    .line 217
    :goto_4
    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v4, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 220
    const/4 v2, 0x5

    .line 221
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->o()Ljava/lang/String;

    move-result-object v3

    .line 220
    iget-object v4, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 222
    const/16 v2, 0x12

    const-string v3, "SYSTEM"

    iget-object v4, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 224
    const/16 v2, 0x1b

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 225
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 224
    iget-object v4, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 227
    const/4 v10, 0x0

    .line 249
    if-eqz p5, :cond_0

    if-eqz p6, :cond_0

    .line 250
    const/16 v2, 0x27

    iget-object v3, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    move-object/from16 v0, p5

    invoke-virtual {v3, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 252
    const/16 v2, 0x28

    iget-object v3, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    move-object/from16 v0, p6

    invoke-virtual {v3, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 257
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "logging_id2"

    .line 256
    invoke-static {v2, v3}, Lcom/google/android/c/c;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 258
    const/16 v3, 0x13

    iget-object v4, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v3, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 260
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->o()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x2c

    .line 261
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2c

    .line 262
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2c

    .line 263
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2c

    .line 264
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 265
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 266
    const-string v2, "API Header: "

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 269
    :cond_0
    :goto_5
    const/4 v5, 0x0

    .line 270
    const/4 v2, 0x0

    .line 272
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/net/a/b;->A()Lcom/google/r/b/a/pe;

    move-result-object v3

    .line 273
    iget-boolean v4, v3, Lcom/google/r/b/a/pe;->e:Z

    if-eqz v4, :cond_9

    .line 274
    new-instance v4, Lcom/b/a/p;

    invoke-direct {v4}, Lcom/b/a/p;-><init>()V

    .line 279
    iget v2, v3, Lcom/google/r/b/a/pe;->d:I

    int-to-long v2, v2

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 278
    invoke-virtual {v4, v2, v3, v7}, Lcom/b/a/p;->a(JLjava/util/concurrent/TimeUnit;)V

    .line 281
    new-instance v2, Lcom/google/android/apps/gmm/shared/net/u;

    invoke-direct {v2, v4}, Lcom/google/android/apps/gmm/shared/net/u;-><init>(Lcom/b/a/p;)V

    .line 300
    :cond_1
    :goto_6
    :try_start_0
    new-instance v4, Ljava/net/URL;

    const/4 v3, 0x0

    invoke-direct {v4, v3, p2, v2}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;Ljava/net/URLStreamHandler;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 306
    new-instance v2, Lcom/google/android/apps/gmm/shared/net/s;

    .line 325
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v7, "maps_client_id"

    const-string v8, "Web"

    invoke-static {v3, v7, v8}, Lcom/google/android/c/c;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 327
    new-instance v11, Lcom/google/android/apps/gmm/shared/net/ae;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->a()Landroid/content/Context;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v7

    move/from16 v0, p7

    invoke-direct {v11, v3, v7, v0}, Lcom/google/android/apps/gmm/shared/net/ae;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/f;Z)V

    move-object v3, p1

    move-object v7, p3

    move-object/from16 v8, p4

    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/gmm/shared/net/s;-><init>(Lcom/google/android/apps/gmm/shared/net/ad;Ljava/net/URL;Landroid/net/http/AndroidHttpClient;Lcom/google/e/a/a/a/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/shared/net/ae;)V

    .line 329
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/s;->d()V

    .line 330
    return-object v2

    .line 211
    :cond_2
    sget-object v2, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 213
    :cond_3
    sget-object v2, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    goto/16 :goto_1

    .line 216
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 215
    :cond_5
    sget-object v2, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    goto/16 :goto_3

    .line 219
    :cond_6
    const/16 v3, 0xc8

    if-le v2, v3, :cond_7

    const/4 v2, 0x3

    goto/16 :goto_4

    :cond_7
    const/4 v2, 0x1

    goto/16 :goto_4

    .line 266
    :cond_8
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 288
    :cond_9
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-ge v3, v4, :cond_1

    .line 292
    sget-object v2, Lcom/google/android/apps/gmm/shared/net/ab;->a:Ljava/lang/String;

    invoke-static {v2, p0}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;Landroid/content/Context;)Landroid/net/http/AndroidHttpClient;

    move-result-object v5

    .line 294
    invoke-static {v5, p0}, Lcom/google/android/apps/gmm/shared/net/ab;->a(Lorg/apache/http/client/HttpClient;Landroid/content/Context;)Ljava/net/URLStreamHandler;

    move-result-object v2

    goto :goto_6

    .line 301
    :catch_0
    move-exception v2

    .line 302
    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method static synthetic a(Ljava/util/Collection;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 101
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/net/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x2c

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/d/f;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1482
    const-string v1, ""

    .line 1483
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/d/f;

    .line 1485
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/p/d/f;->a()Lcom/google/o/b/a/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/b/a/v;->l()[B

    move-result-object v0

    const/16 v3, 0xb

    .line 1484
    invoke-static {v0, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    .line 1487
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1488
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1491
    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "w "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1493
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method private static a(Ljava/net/URL;)V
    .locals 3

    .prologue
    .line 1500
    const-string v0, "Using server %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 1503
    invoke-virtual {p0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1504
    return-void

    .line 1508
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Insecure server URLs are not allowed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static a(Lcom/google/r/b/a/el;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1440
    if-nez p0, :cond_0

    .line 1448
    :goto_0
    return v0

    .line 1443
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/shared/net/x;->b:[I

    invoke-virtual {p0}, Lcom/google/r/b/a/el;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1446
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1443
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private declared-synchronized f()V
    .locals 4

    .prologue
    .line 846
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->u:I

    iget v1, p0, Lcom/google/android/apps/gmm/shared/net/s;->p:I

    if-ge v0, v1, :cond_0

    .line 847
    iget v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->u:I

    .line 848
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/aa;

    iget v1, p0, Lcom/google/android/apps/gmm/shared/net/s;->u:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "network-thread-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/shared/net/aa;-><init>(Lcom/google/android/apps/gmm/shared/net/s;Ljava/lang/String;)V

    .line 849
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/aa;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 860
    :cond_0
    monitor-exit p0

    return-void

    .line 846
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 588
    iget-wide v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->o:J

    return-wide v0
.end method

.method public final a(J)Lcom/google/android/apps/gmm/shared/net/al;
    .locals 5

    .prologue
    .line 753
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/al;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 754
    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/apps/gmm/shared/net/al;-><init>(Lcom/google/android/apps/gmm/shared/c/f;J)V

    .line 756
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/ad;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/shared/net/w;

    const-string v3, "testConnection"

    invoke-direct {v2, p0, v3, v0}, Lcom/google/android/apps/gmm/shared/net/w;-><init>(Lcom/google/android/apps/gmm/shared/net/s;Ljava/lang/String;Lcom/google/android/apps/gmm/shared/net/al;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->NETWORK_THREADS:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 769
    return-object v0
.end method

.method public final a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q::",
            "Lcom/google/n/at;",
            "S::",
            "Lcom/google/n/at;",
            ">(TQ;",
            "Lcom/google/android/apps/gmm/shared/net/c",
            "<TS;>;",
            "Lcom/google/android/apps/gmm/shared/c/a/p;",
            ")",
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<TQ;TS;>;"
        }
    .end annotation

    .prologue
    .line 379
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/ad;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/gmm/shared/net/ag;->a(Lcom/google/android/apps/gmm/shared/net/a;Lcom/google/android/apps/gmm/shared/c/a/j;Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/b;

    move-result-object v0

    .line 380
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 381
    invoke-interface {v0, p2, p3}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 382
    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q::",
            "Lcom/google/n/at;",
            "S::",
            "Lcom/google/n/at;",
            ">(",
            "Ljava/lang/Class",
            "<TQ;>;)",
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<TQ;TS;>;"
        }
    .end annotation

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 372
    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    .line 371
    invoke-static {p0, v0, p1}, Lcom/google/android/apps/gmm/shared/net/ag;->a(Lcom/google/android/apps/gmm/shared/net/a;Lcom/google/android/apps/gmm/shared/c/a/j;Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/b;

    move-result-object v0

    return-object v0
.end method

.method a(Lcom/google/android/apps/gmm/shared/net/i;Ljava/io/DataInputStream;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 1

    .prologue
    .line 842
    invoke-virtual {p1, p2}, Lcom/google/android/apps/gmm/shared/net/i;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/p/d/f;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/gmm/p/d/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 740
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/s;->l:Lcom/google/android/apps/gmm/p/d/f;

    .line 741
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/q;)V
    .locals 1

    .prologue
    .line 724
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/s;->m:Lcom/google/android/apps/gmm/shared/net/q;

    .line 725
    if-eqz p1, :cond_0

    .line 726
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/f;->a()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/s;->q:Lcom/google/android/apps/gmm/shared/net/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/i;->i()V

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/s;->q:Lcom/google/android/apps/gmm/shared/net/i;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/shared/net/s;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 728
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q::",
            "Lcom/google/n/at;",
            "S::",
            "Lcom/google/n/at;",
            ">(",
            "Lcom/google/r/b/a/el;",
            "Ljava/lang/Class",
            "<TQ;>;",
            "Lcom/google/n/ax",
            "<TS;>;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 358
    sget-wide v6, Lcom/google/android/apps/gmm/shared/net/i;->p:J

    const/4 v9, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, v4

    move v8, v4

    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/gmm/shared/net/ag;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;ZZJIZ)V

    .line 366
    return-void
.end method

.method public final a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q::",
            "Lcom/google/n/at;",
            "S::",
            "Lcom/google/n/at;",
            ">(",
            "Lcom/google/r/b/a/el;",
            "Ljava/lang/Class",
            "<TQ;>;",
            "Lcom/google/n/ax",
            "<TS;>;Z)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 345
    sget-wide v6, Lcom/google/android/apps/gmm/shared/net/i;->p:J

    const/4 v9, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    move v8, v4

    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/gmm/shared/net/ag;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;ZZJIZ)V

    .line 353
    return-void
.end method

.method public final a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;ZZJIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q::",
            "Lcom/google/n/at;",
            "S::",
            "Lcom/google/n/at;",
            ">(",
            "Lcom/google/r/b/a/el;",
            "Ljava/lang/Class",
            "<TQ;>;",
            "Lcom/google/n/ax",
            "<TS;>;ZZJIZ)V"
        }
    .end annotation

    .prologue
    .line 337
    invoke-static/range {p1 .. p9}, Lcom/google/android/apps/gmm/shared/net/ag;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;ZZJIZ)V

    .line 339
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 676
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/s;->d:Ljava/net/URL;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;Ljava/net/URLStreamHandler;)V

    .line 677
    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/net/s;->a(Ljava/net/URL;)V

    .line 678
    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->d:Ljava/net/URL;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 681
    return-void

    .line 679
    :catch_0
    move-exception v0

    .line 680
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1415
    :try_start_0
    const-string v0, "connectivity"

    .line 1416
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1417
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 1418
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1421
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1418
    goto :goto_0

    .line 1421
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/i;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 774
    iget-object v3, p1, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    .line 775
    sget-object v0, Lcom/google/r/b/a/el;->o:Lcom/google/r/b/a/el;

    if-eq v3, v0, :cond_0

    sget-object v0, Lcom/google/r/b/a/el;->aj:Lcom/google/r/b/a/el;

    if-eq v3, v0, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "CookieRequest and ClientProperties2 are managed by GmmServer"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 779
    :cond_1
    sget-object v0, Lcom/google/r/b/a/el;->y:Lcom/google/r/b/a/el;

    if-eq v3, v0, :cond_2

    move v0, v1

    :goto_1
    const-string v4, "ClientProperties shouldn\'t be sent"

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 783
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/net/i;->ak_()Z

    move-result v0

    .line 784
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v4, v5, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v4

    if-ne v4, v1, :cond_4

    :goto_2
    if-nez v1, :cond_6

    invoke-static {v3}, Lcom/google/android/apps/gmm/shared/net/s;->a(Lcom/google/r/b/a/el;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 788
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->m:Lcom/google/android/apps/gmm/shared/net/q;

    if-nez v0, :cond_5

    const/4 v0, 0x0

    :goto_4
    iput-object v0, p1, Lcom/google/android/apps/gmm/shared/net/i;->r:Landroid/accounts/Account;

    .line 789
    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/gmm/shared/net/s;->a(Lcom/google/android/apps/gmm/shared/net/i;Z)Z

    move-result v0

    return v0

    :cond_4
    move v1, v2

    .line 784
    goto :goto_2

    .line 788
    :cond_5
    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/q;->a()Landroid/accounts/Account;

    move-result-object v0

    goto :goto_4

    :cond_6
    move v2, v0

    goto :goto_3
.end method

.method a(Lcom/google/android/apps/gmm/shared/net/i;Z)Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 793
    .line 794
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/shared/net/s;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 795
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->g:Lcom/google/android/apps/gmm/shared/net/k;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 798
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/l;->b:Lcom/google/android/apps/gmm/shared/net/l;

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p1, v0, v3}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V

    .line 799
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/shared/net/f;->a(Ljava/lang/Object;Z)Z

    move-result v0

    .line 800
    if-nez v0, :cond_0

    .line 801
    sget-object v3, Lcom/google/android/apps/gmm/shared/net/l;->c:Lcom/google/android/apps/gmm/shared/net/l;

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p1, v3, v4}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V

    .line 825
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 826
    new-array v3, v5, [Ljava/lang/Object;

    const-string v4, "Failed to queue request:"

    aput-object v4, v3, v1

    aput-object p1, v3, v2

    invoke-static {v3}, Lcom/google/android/apps/gmm/shared/c/p;->a([Ljava/lang/Object;)V

    .line 829
    :cond_1
    if-eqz v0, :cond_3

    if-nez p2, :cond_2

    iget v1, p0, Lcom/google/android/apps/gmm/shared/net/s;->u:I

    if-nez v1, :cond_3

    .line 830
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/gmm/shared/net/s;->f()V

    .line 833
    :cond_3
    return v0

    .line 804
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    sget-object v3, Lcom/google/android/apps/gmm/shared/net/k;->g:Lcom/google/android/apps/gmm/shared/net/k;

    invoke-virtual {p1, v0, v3}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/shared/net/k;)V

    move v0, v1

    goto :goto_0

    .line 807
    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/l;->b:Lcom/google/android/apps/gmm/shared/net/l;

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p1, v0, v3}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V

    .line 808
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/shared/net/f;->a(Ljava/lang/Object;Z)Z

    move-result v0

    .line 809
    if-nez v0, :cond_0

    .line 810
    new-array v3, v5, [Ljava/lang/Object;

    const-string v4, "Failed to insert %s. Will attempt retry."

    aput-object v4, v3, v1

    aput-object p1, v3, v2

    invoke-static {v3}, Lcom/google/android/apps/gmm/shared/c/p;->a([Ljava/lang/Object;)V

    .line 811
    sget-object v3, Lcom/google/android/apps/gmm/shared/net/l;->c:Lcom/google/android/apps/gmm/shared/net/l;

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p1, v3, v4}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V

    .line 814
    sget-object v3, Lcom/google/android/apps/gmm/shared/net/k;->n:Lcom/google/android/apps/gmm/shared/net/k;

    invoke-virtual {p1, v3}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 815
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/l;->b:Lcom/google/android/apps/gmm/shared/net/l;

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p1, v0, v3}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V

    .line 817
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/shared/net/f;->a(Ljava/lang/Object;Z)Z

    move v0, v2

    .line 818
    goto :goto_0

    .line 820
    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    sget-object v4, Lcom/google/android/apps/gmm/shared/net/k;->n:Lcom/google/android/apps/gmm/shared/net/k;

    invoke-virtual {p1, v3, v4}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/shared/net/k;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 745
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/f;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 746
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/s;->q:Lcom/google/android/apps/gmm/shared/net/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/i;->i()V

    .line 747
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/s;->q:Lcom/google/android/apps/gmm/shared/net/i;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/shared/net/s;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 749
    :cond_0
    return-void
.end method

.method declared-synchronized b(J)V
    .locals 5

    .prologue
    .line 1342
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/google/android/apps/gmm/shared/net/s;->n:J

    .line 1343
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->W:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1344
    :cond_0
    const-string v0, "%s received a new debug cookie: %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 1345
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1344
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1346
    monitor-exit p0

    return-void

    .line 1342
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Lcom/google/android/apps/gmm/shared/net/h;
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->j:Lcom/google/android/apps/gmm/shared/net/ae;

    return-object v0
.end method

.method d()V
    .locals 4

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 570
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/s;->r:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 574
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/s;->s:Lcom/google/android/apps/gmm/shared/net/z;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 575
    return-void
.end method

.method declared-synchronized e()J
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const-wide/16 v0, 0x0

    .line 1327
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v4

    .line 1328
    iget-wide v6, p0, Lcom/google/android/apps/gmm/shared/net/s;->n:J

    cmp-long v5, v6, v0

    if-nez v5, :cond_2

    .line 1329
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->W:Lcom/google/android/apps/gmm/shared/b/c;

    const-wide/16 v2, 0x0

    invoke-virtual {v4, v0, v2, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->n:J

    .line 1338
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->n:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 1330
    :cond_2
    :try_start_1
    sget-object v5, Lcom/google/android/apps/gmm/shared/b/c;->W:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, v4, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v6, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    :goto_1
    if-nez v2, :cond_0

    .line 1331
    const-string v2, "%s: debug cookie has been cleared; fetching a new one"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 1332
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    .line 1331
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 1335
    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->W:Lcom/google/android/apps/gmm/shared/b/c;

    const-wide/16 v6, 0x0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v4, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1327
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v2, v3

    .line 1330
    goto :goto_1
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 579
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->t:Landroid/net/http/AndroidHttpClient;

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/s;->t:Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 584
    :cond_0
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 636
    return-void
.end method
