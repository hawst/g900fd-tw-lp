.class public Lcom/google/android/apps/gmm/map/internal/d/b/t;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:[B

.field static final synthetic j:Z

.field private static final k:I

.field private static final l:I

.field private static m:I


# instance fields
.field private final A:Lcom/google/android/apps/gmm/shared/c/f;

.field private final B:Lcom/google/android/apps/gmm/map/internal/d/ac;

.field private final C:Lcom/google/android/apps/gmm/map/e/a;

.field private final D:Lcom/google/android/apps/gmm/map/internal/d/b/x;

.field b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

.field final c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

.field final d:Ljava/util/concurrent/locks/ReentrantLock;

.field final e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

.field f:Z

.field public final g:Lcom/google/android/apps/gmm/map/internal/d/b/aa;

.field final h:Lcom/google/android/apps/gmm/map/util/b;

.field volatile i:Z

.field private final n:Ljava/lang/String;

.field private final o:Ljava/io/File;

.field private p:Ljava/io/RandomAccessFile;

.field private final q:Lcom/google/android/apps/gmm/map/internal/d/b/u;

.field private final r:[Ljava/io/RandomAccessFile;

.field private final s:Lcom/google/android/apps/gmm/map/util/a/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/h",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/b/ad;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/b/ad;",
            ">;"
        }
    .end annotation
.end field

.field private u:I

.field private v:Z

.field private w:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private x:I

.field private final y:[B

.field private final z:[B


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x16

    const/4 v1, 0x0

    .line 252
    const-class v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->j:Z

    .line 280
    new-array v0, v1, [B

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a:[B

    .line 347
    sput v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->k:I

    .line 348
    const v0, 0x13f88

    sput v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->l:I

    .line 352
    sput v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->m:I

    return-void

    :cond_0
    move v0, v1

    .line 252
    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/b/w;Lcom/google/android/apps/gmm/map/internal/d/b/ag;Lcom/google/android/apps/gmm/map/internal/d/b/u;Ljava/io/RandomAccessFile;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/b/x;)V
    .locals 7

    .prologue
    .line 502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 426
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    .line 431
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    .line 437
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->u:I

    .line 438
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->v:Z

    .line 449
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->x:I

    .line 471
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    .line 477
    const/high16 v0, 0x20000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->y:[B

    .line 483
    const/high16 v0, 0x20000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->z:[B

    .line 503
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->n:Ljava/lang/String;

    .line 504
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    .line 505
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    .line 506
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->q:Lcom/google/android/apps/gmm/map/internal/d/b/u;

    .line 507
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    .line 508
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->o:Ljava/io/File;

    .line 509
    iput-object p8, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->D:Lcom/google/android/apps/gmm/map/internal/d/b/x;

    .line 510
    iget v0, p3, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    new-array v0, v0, [Ljava/io/RandomAccessFile;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    .line 511
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/h;

    const/16 v1, 0x800

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/d/b/w;->e:I

    mul-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/h;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    .line 512
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->t:Ljava/util/Set;

    .line 515
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 517
    const/4 v1, 0x0

    .line 518
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    if-ge v2, v1, :cond_6

    .line 519
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->b:[I

    aget v1, v1, v2

    const/4 v3, -0x1

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    .line 520
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aget v1, v1, v2

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->q:Lcom/google/android/apps/gmm/map/internal/d/b/u;

    mul-int/lit16 v3, v2, 0x400

    iget-object v4, v1, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    const/16 v5, 0x3fc

    new-instance v6, Ljava/util/zip/CRC32;

    invoke-direct {v6}, Ljava/util/zip/CRC32;-><init>()V

    invoke-virtual {v6, v4, v3, v5}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-virtual {v6}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v4

    long-to-int v4, v4

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    add-int/lit16 v3, v3, 0x3fc

    invoke-static {v1, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    if-ne v4, v1, :cond_4

    const/4 v1, 0x1

    :goto_3
    if-nez v1, :cond_1

    .line 521
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x2a

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Rebuilding inconsistent shard: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v0, "GenericDiskCache"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->n:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 522
    const/4 v0, 0x1

    .line 524
    :try_start_1
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(I)Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    move-result-object v1

    .line 525
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;)V

    .line 526
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->q:Lcom/google/android/apps/gmm/map/internal/d/b/u;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;)V

    .line 527
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 518
    :cond_1
    :goto_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_0

    .line 519
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 520
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    .line 521
    :cond_5
    :try_start_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 540
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 528
    :catch_0
    move-exception v1

    .line 529
    :try_start_3
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x1d

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Rebuilding shard: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 532
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e(I)V

    goto :goto_5

    .line 536
    :cond_6
    if-eqz v0, :cond_7

    .line 537
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 540
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 544
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->A:Lcom/google/android/apps/gmm/shared/c/f;

    .line 550
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->B:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 551
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/aa;

    const/16 v1, 0x3c

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v2

    invoke-direct {v0, v1, v2, p2}, Lcom/google/android/apps/gmm/map/internal/d/b/aa;-><init>(ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g:Lcom/google/android/apps/gmm/map/internal/d/b/aa;

    .line 553
    new-instance v0, Lcom/google/android/apps/gmm/map/util/b;

    const/4 v1, 0x5

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v2

    invoke-direct {v0, v1, v2, p2}, Lcom/google/android/apps/gmm/map/util/b;-><init>(ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    .line 555
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->C:Lcom/google/android/apps/gmm/map/e/a;

    .line 560
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/shared/net/a/b;)I
    .locals 2

    .prologue
    .line 731
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/a/b;->d()Lcom/google/android/apps/gmm/shared/net/a/t;

    move-result-object v0

    .line 732
    if-nez v0, :cond_0

    .line 733
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "VectorMapsParameters is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 735
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget v0, v0, Lcom/google/r/b/a/aqg;->h:I

    const v1, 0xffff

    and-int/2addr v0, v1

    return v0
.end method

.method private a(Z)I
    .locals 5

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 2159
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    if-ge v0, v2, :cond_3

    .line 2160
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aget v2, v2, v0

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_2

    .line 2193
    :cond_0
    :goto_2
    return v0

    :cond_1
    move v2, v1

    .line 2160
    goto :goto_1

    .line 2159
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2165
    :cond_3
    if-eqz p1, :cond_4

    .line 2166
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a(Ljava/util/Set;)I

    move-result v0

    .line 2167
    if-eq v0, v3, :cond_4

    .line 2168
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e(I)V

    goto :goto_2

    .line 2174
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    if-ge v0, v1, :cond_6

    .line 2177
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    .line 2179
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v0, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    add-int/lit8 v2, v0, 0x1

    iput v2, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    const/4 v3, 0x0

    aput v3, v2, v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->b:[I

    const/4 v3, 0x0

    aput v3, v2, v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->c:[I

    const/4 v3, 0x0

    aput v3, v2, v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    const/4 v3, 0x0

    aput v3, v2, v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    aget v2, v2, v0

    if-lez v2, :cond_5

    iget v2, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->g:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->g:I

    :cond_5
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    const/4 v2, 0x0

    aput v2, v1, v0

    .line 2180
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->q:Lcom/google/android/apps/gmm/map/internal/d/b/u;

    mul-int/lit16 v2, v0, 0x400

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    add-int/lit16 v3, v2, 0x3fc

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Ljava/util/Arrays;->fill([BIIB)V

    .line 2181
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->q:Lcom/google/android/apps/gmm/map/internal/d/b/u;

    add-int/lit8 v2, v0, 0x1

    iput v2, v1, Lcom/google/android/apps/gmm/map/internal/d/b/u;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2184
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0

    .line 2189
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a(Ljava/util/Set;)I

    move-result v0

    .line 2190
    if-eq v0, v3, :cond_0

    .line 2191
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e(I)V

    goto/16 :goto_2
.end method

.method static a([BII)I
    .locals 2

    .prologue
    .line 3493
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    .line 3494
    invoke-virtual {v0, p0, p1, p2}, Ljava/util/zip/CRC32;->update([BII)V

    .line 3495
    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v0

    .line 3496
    long-to-int v0, v0

    return v0
.end method

.method public static a(JLjava/lang/String;)Lcom/google/android/apps/gmm/map/internal/d/b/ab;
    .locals 2

    .prologue
    .line 1428
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a:[B

    .line 1430
    if-eqz p2, :cond_0

    .line 1431
    sget-object v0, Lcom/google/b/a/w;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 1434
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ab;-><init>(J[B)V

    return-object v1
.end method

.method private a(III)Lcom/google/android/apps/gmm/map/internal/d/b/ae;
    .locals 4

    .prologue
    .line 2244
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->e:I

    mul-int/lit8 v0, v0, 0x32

    div-int/lit8 v0, v0, 0x64

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/w;->e:I

    sub-int/2addr v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2247
    const v0, 0x7ffffff

    sub-int v2, v0, p3

    .line 2248
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    if-ge p1, v0, :cond_2

    .line 2249
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    aget v0, v0, p1

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    aget v0, v0, p1

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    .line 2252
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2253
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(I)Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    move-result-object v0

    .line 2255
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b()I

    move-result v3

    if-gt v3, v2, :cond_1

    .line 2261
    :goto_1
    return-object v0

    .line 2259
    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 2261
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;IILjava/util/Locale;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/b/x;)Lcom/google/android/apps/gmm/map/internal/d/b/t;
    .locals 13

    .prologue
    .line 579
    const/4 v8, 0x0

    .line 580
    const/4 v2, -0x1

    if-ne p2, v2, :cond_a

    .line 581
    const v2, 0x13f88

    .line 582
    const/4 v8, 0x1

    .line 586
    :goto_0
    const/4 v3, 0x4

    if-ge v2, v3, :cond_0

    .line 597
    const/4 v2, 0x4

    .line 599
    :cond_0
    if-nez v8, :cond_1

    const v3, 0x13f88

    if-le v2, v3, :cond_1

    .line 600
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Number of records must be between 4 and "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x13f88

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 603
    :cond_1
    const/4 v3, 0x4

    add-int/lit8 v4, v2, -0x1

    div-int/lit16 v4, v4, 0x199

    add-int/lit8 v4, v4, 0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 604
    add-int/lit8 v2, v2, -0x1

    div-int/2addr v2, v6

    add-int/lit8 v7, v2, 0x1

    .line 606
    sget-boolean v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->j:Z

    if-nez v2, :cond_3

    const/4 v2, 0x4

    if-lt v6, v2, :cond_2

    const/16 v2, 0x199

    if-le v6, v2, :cond_3

    :cond_2
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_3
    sget-boolean v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->j:Z

    if-nez v2, :cond_4

    if-nez v8, :cond_4

    const/16 v2, 0x23

    if-le v6, v2, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_4
    sget-boolean v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->j:Z

    if-nez v2, :cond_6

    if-lez v7, :cond_5

    const/16 v2, 0x199

    if-le v7, v2, :cond_6

    :cond_5
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_6
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v2, ".m"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object/from16 v0, p5

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v2, ".m"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    const/4 v3, 0x1

    move-object/from16 v0, p5

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/File;Ljava/lang/String;Z)Ljava/io/RandomAccessFile;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/map/internal/d/b/w;

    invoke-interface {p0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/net/a/b;->d()Lcom/google/android/apps/gmm/shared/net/a/t;

    move-result-object v4

    if-nez v4, :cond_9

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "VectorMapsParameters is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_7
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_9
    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget v4, v4, Lcom/google/r/b/a/aqg;->h:I

    const v5, 0xffff

    and-int/2addr v4, v5

    const/16 v5, 0x16

    shl-int/lit8 v4, v4, 0x10

    add-int/2addr v4, v5

    const/16 v5, 0x2000

    invoke-interface {p0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v10

    move/from16 v9, p3

    move-object/from16 v12, p4

    invoke-direct/range {v3 .. v12}, Lcom/google/android/apps/gmm/map/internal/d/b/w;-><init>(IIIIZIJLjava/util/Locale;)V

    new-instance v8, Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    invoke-direct {v8, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/ag;-><init>(I)V

    new-instance v9, Lcom/google/android/apps/gmm/map/internal/d/b/u;

    const/4 v4, 0x0

    invoke-interface {p0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v5

    invoke-direct {v9, v6, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/u;-><init>(IILcom/google/android/apps/gmm/map/util/a/b;)V

    invoke-static {v3, v8, v9, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/w;Lcom/google/android/apps/gmm/map/internal/d/b/ag;Lcom/google/android/apps/gmm/map/internal/d/b/u;Ljava/io/RandomAccessFile;)V

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/FileDescriptor;->sync()V

    new-instance v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;

    move-object v5, p0

    move-object v6, p1

    move-object v7, v3

    move-object v10, v2

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    invoke-direct/range {v4 .. v12}, Lcom/google/android/apps/gmm/map/internal/d/b/t;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/b/w;Lcom/google/android/apps/gmm/map/internal/d/b/ag;Lcom/google/android/apps/gmm/map/internal/d/b/u;Ljava/io/RandomAccessFile;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/b/x;)V

    return-object v4

    :cond_a
    move v2, p2

    goto/16 :goto_0
.end method

.method static a(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/b/x;)Lcom/google/android/apps/gmm/map/internal/d/b/t;
    .locals 12

    .prologue
    const v5, 0xffff

    const/16 v4, 0x2000

    const/4 v7, 0x0

    .line 675
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v0, ".m"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x1

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/File;Ljava/lang/String;Z)Ljava/io/RandomAccessFile;

    move-result-object v6

    .line 678
    new-array v0, v4, [B

    .line 679
    invoke-virtual {v6, v0, v7, v4}, Ljava/io/RandomAccessFile;->readFully([BII)V

    .line 680
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/d/b/w;

    invoke-direct {v3, v0, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/w;-><init>([BI)V

    .line 683
    const/16 v0, 0x16

    .line 684
    invoke-interface {p0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->d()Lcom/google/android/apps/gmm/shared/net/a/t;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "VectorMapsParameters is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 675
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 684
    :cond_1
    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget v1, v1, Lcom/google/r/b/a/aqg;->h:I

    and-int v9, v1, v5

    .line 685
    shl-int/lit8 v1, v9, 0x10

    add-int v10, v1, v0

    .line 687
    iget v1, v3, Lcom/google/android/apps/gmm/map/internal/d/b/w;->a:I

    shr-int/lit8 v1, v1, 0x10

    and-int v11, v1, v5

    .line 689
    iget v1, v3, Lcom/google/android/apps/gmm/map/internal/d/b/w;->a:I

    and-int/2addr v1, v5

    .line 691
    if-nez v11, :cond_2

    if-eq v1, v0, :cond_2

    .line 692
    new-instance v2, Ljava/io/IOException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit16 v5, v5, 0xa1

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Invalid Cache Header(1): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; cached sever schema is zero but client schema part doesn\'t match: cachedClientSchema = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", expectedClientSchema = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 696
    :cond_2
    if-eqz v11, :cond_3

    iget v0, v3, Lcom/google/android/apps/gmm/map/internal/d/b/w;->a:I

    if-ne v0, v10, :cond_4

    :cond_3
    iget v0, v3, Lcom/google/android/apps/gmm/map/internal/d/b/w;->c:I

    if-eq v0, v4, :cond_5

    .line 698
    :cond_4
    new-instance v0, Ljava/io/IOException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x54

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid Cache Header(2): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", expect expectedSchema="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mBlockSize=8192"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 703
    :cond_5
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v0, v3, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ag;-><init>(I)V

    .line 704
    invoke-virtual {v4, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->b(Ljava/io/RandomAccessFile;)V

    .line 707
    new-instance v5, Lcom/google/android/apps/gmm/map/internal/d/b/u;

    iget v0, v3, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    iget v1, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    .line 708
    invoke-interface {p0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v2

    invoke-direct {v5, v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/u;-><init>(IILcom/google/android/apps/gmm/map/util/a/b;)V

    .line 709
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    array-length v1, v1

    invoke-virtual {v6, v0, v7, v1}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 711
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;

    move-object v1, p0

    move-object v2, p1

    move-object v7, p2

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/internal/d/b/t;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/b/w;Lcom/google/android/apps/gmm/map/internal/d/b/ag;Lcom/google/android/apps/gmm/map/internal/d/b/u;Ljava/io/RandomAccessFile;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/b/x;)V

    .line 715
    if-nez v11, :cond_6

    if-eqz v9, :cond_6

    .line 716
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    invoke-direct {v0, v1, v10}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(II)V

    .line 721
    :cond_6
    return-object v0
.end method

.method private a(Ljava/io/RandomAccessFile;Lcom/google/android/apps/gmm/map/internal/d/b/ad;I)Lcom/google/android/apps/gmm/map/util/f;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x4

    .line 1571
    new-array v0, v4, [B

    .line 1572
    iget v1, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    iget v2, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    add-int/2addr v1, v2

    invoke-static {p1, v1, v0, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/RandomAccessFile;I[BI)V

    .line 1574
    invoke-static {v0, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    .line 1575
    iget v0, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    if-lt v0, v1, :cond_0

    if-gtz v1, :cond_2

    .line 1576
    :cond_0
    const-string v0, "GenericDiskCache"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->n:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v2, "Bad data length %d: File %s size %d, record: %s "

    new-array v3, v4, [Ljava/lang/Object;

    .line 1577
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v5

    const/4 v1, 0x1

    aput-object p1, v3, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x3

    aput-object p2, v3, v1

    .line 1576
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1578
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->t:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1579
    const/4 v0, 0x0

    .line 1588
    :goto_1
    return-object v0

    .line 1576
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1582
    :cond_2
    iget v0, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    add-int v2, v1, p3

    if-lt v0, v2, :cond_3

    .line 1583
    add-int v0, v1, p3

    .line 1585
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/util/b;->a(I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v0

    .line 1586
    iget v1, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    iget v2, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    add-int/2addr v1, v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v2

    .line 1587
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v3

    .line 1586
    invoke-static {p1, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/RandomAccessFile;I[BI)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method private static a(Ljava/io/File;Ljava/lang/String;Z)Ljava/io/RandomAccessFile;
    .locals 4

    .prologue
    .line 2611
    new-instance v1, Ljava/io/File;

    const-string v0, "cache_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, p0, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2612
    new-instance v2, Ljava/io/RandomAccessFile;

    if-eqz p2, :cond_1

    const-string v0, "rw"

    :goto_1
    invoke-direct {v2, v1, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v2

    .line 2611
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 2612
    :cond_1
    const-string v0, "r"

    goto :goto_1
.end method

.method private a(Ljava/util/Collection;)Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/b/z;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/b/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2435
    .line 2436
    new-instance v7, Ljava/util/HashSet;

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-direct {v7, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 2437
    new-instance v8, Ljava/util/HashSet;

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-direct {v8, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 2438
    new-instance v9, Ljava/util/HashMap;

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-direct {v9, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 2442
    new-instance v10, Ljava/util/ArrayList;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2443
    invoke-static {v10}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 2445
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2446
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;

    .line 2447
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->a:J

    const-wide/16 v12, -0x1

    cmp-long v4, v4, v12

    if-eqz v4, :cond_1

    .line 2448
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->b:[B

    array-length v4, v4

    const/16 v5, 0xff

    if-le v4, v5, :cond_2

    .line 2450
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2452
    :cond_2
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    invoke-interface {v8, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2454
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 2456
    iget v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->c:I

    if-lez v4, :cond_0

    .line 2457
    iget v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->c:I

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    invoke-static {v4, v2, v9}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(ILcom/google/android/apps/gmm/map/internal/d/b/ab;Ljava/util/Map;)V

    goto :goto_0

    .line 2460
    :cond_3
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v7, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2461
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    invoke-interface {v8, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2469
    :cond_4
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    if-ge v3, v2, :cond_c

    .line 2470
    const/4 v4, 0x0

    .line 2471
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 2472
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aget v5, v5, v3

    if-eqz v5, :cond_9

    const/4 v5, 0x1

    :goto_2
    if-eqz v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->q:Lcom/google/android/apps/gmm/map/internal/d/b/u;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    iget-object v2, v5, Lcom/google/android/apps/gmm/map/internal/d/b/u;->c:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    invoke-static {v12, v13, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a(J[I)[I

    move-result-object v11

    invoke-virtual {v5, v11, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a([II)Z

    move-result v11

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/d/b/u;->c:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z

    if-eqz v11, :cond_5

    .line 2473
    const/4 v2, 0x1

    .line 2477
    :goto_3
    if-eqz v2, :cond_b

    .line 2478
    const/4 v2, 0x0

    .line 2480
    :try_start_0
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(I)Lcom/google/android/apps/gmm/map/internal/d/b/ae;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    move-object v6, v2

    .line 2486
    :goto_4
    if-eqz v6, :cond_b

    .line 2487
    const/4 v4, 0x0

    .line 2488
    const/4 v2, 0x0

    :goto_5
    iget v5, v6, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    if-ge v2, v5, :cond_a

    .line 2489
    iget-object v5, v6, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v11, v2, 0x14

    add-int/lit8 v11, v11, 0x8

    invoke-static {v5, v11}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v12

    int-to-long v12, v12

    add-int/lit8 v11, v11, 0x4

    invoke-static {v5, v11}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v5

    int-to-long v14, v5

    const-wide v16, 0xffffffffL

    and-long v14, v14, v16

    const/16 v5, 0x20

    shl-long/2addr v12, v5

    or-long/2addr v12, v14

    .line 2490
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v7, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 2491
    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a:[B

    .line 2495
    invoke-virtual {v6, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->d(I)Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    move-result-object v11

    .line 2496
    iget-object v14, v6, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v15, v2, 0x14

    add-int/lit8 v15, v15, 0x8

    add-int/lit8 v15, v15, 0x8

    add-int/lit8 v15, v15, 0x4

    invoke-static {v14, v15}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v14

    ushr-int/lit8 v14, v14, 0x18

    if-lez v14, :cond_6

    .line 2498
    :try_start_1
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    .line 2505
    :cond_6
    new-instance v14, Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    invoke-direct {v14, v12, v13, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/ab;-><init>(J[B)V

    .line 2506
    invoke-interface {v8, v14}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 2509
    iget-object v4, v6, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v5, v2, 0x14

    add-int/lit8 v5, v5, 0x8

    add-int/lit8 v5, v5, 0x8

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v4

    and-int/lit8 v4, v4, 0x1f

    .line 2510
    if-lez v4, :cond_7

    .line 2512
    invoke-static {v4, v14, v9}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(ILcom/google/android/apps/gmm/map/internal/d/b/ab;Ljava/util/Map;)V

    .line 2515
    :cond_7
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v11}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Lcom/google/android/apps/gmm/map/internal/d/b/ad;)V

    .line 2516
    const/4 v4, 0x1

    .line 2488
    :cond_8
    :goto_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 2472
    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 2481
    :catch_0
    move-exception v4

    .line 2482
    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x30

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "removeOldRecordsAndFilterInsertions: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v4}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2483
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e(I)V

    move-object v6, v2

    goto/16 :goto_4

    .line 2520
    :cond_a
    if-eqz v4, :cond_b

    .line 2521
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Z)V

    .line 2469
    :cond_b
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_1

    .line 2533
    :cond_c
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2534
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    :goto_7
    if-ltz v4, :cond_e

    .line 2535
    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;

    .line 2536
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    invoke-interface {v9, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 2537
    if-eqz v3, :cond_d

    .line 2541
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v6, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->c:I

    .line 2540
    invoke-static {v3, v6}, Lcom/google/android/apps/gmm/map/internal/d/aj;->a(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 2542
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->c:I

    .line 2544
    :cond_d
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2534
    add-int/lit8 v2, v4, -0x1

    move v4, v2

    goto :goto_7

    .line 2501
    :catch_1
    move-exception v5

    goto :goto_6

    .line 2546
    :cond_e
    return-object v5

    :cond_f
    move v2, v4

    goto/16 :goto_3
.end method

.method private a(II)V
    .locals 11

    .prologue
    .line 931
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 933
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->a:I

    if-eq p2, v0, :cond_1

    .line 936
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->c:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->e:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->f:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->h:J

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->i:Ljava/util/Locale;

    move v2, p2

    move v7, p1

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/gmm/map/internal/d/b/w;-><init>(IIIIZIJLjava/util/Locale;)V

    .line 940
    const/16 v0, 0x2000

    new-array v0, v0, [B

    .line 941
    const/4 v2, 0x0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/b/w;->a()[B

    move-result-object v3

    const/4 v4, 0x0

    array-length v5, v3

    invoke-static {v3, v4, v0, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v2, v3

    .line 942
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    monitor-enter v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 943
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 944
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    invoke-virtual {v3, v0}, Ljava/io/RandomAccessFile;->write([B)V

    .line 945
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V

    .line 946
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 947
    :try_start_2
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 953
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 954
    return-void

    .line 946
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 949
    :catch_0
    move-exception v0

    .line 950
    :try_start_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b()V

    .line 951
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 953
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private static a(ILcom/google/android/apps/gmm/map/internal/d/b/ab;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/gmm/map/internal/d/b/ab;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/b/ab;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2417
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2418
    if-eqz v0, :cond_0

    .line 2420
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2419
    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/aj;->a(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2424
    :goto_0
    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2425
    return-void

    .line 2422
    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/b/ac;Lcom/google/android/apps/gmm/map/internal/d/b/ae;)V
    .locals 1

    .prologue
    .line 1986
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->a()V

    .line 1987
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Z)V

    .line 1988
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;Ljava/io/IOException;)V
    .locals 3

    .prologue
    .line 1091
    const-string v0, "GenericDiskCache"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->n:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0, p2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1092
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->t:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1093
    :cond_0
    return-void

    .line 1091
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Lcom/google/android/apps/gmm/map/internal/d/b/ad;)V
    .locals 6

    .prologue
    .line 1597
    .line 1599
    :try_start_0
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g(I)Ljava/io/RandomAccessFile;

    move-result-object v1

    .line 1601
    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1602
    :try_start_1
    iget v0, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    iget v2, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    add-int/2addr v0, v2

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 1603
    iget v0, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/util/b;->a(I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v0

    .line 1604
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v2

    .line 1605
    const/4 v3, 0x0

    iget v4, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Ljava/util/Arrays;->fill([BIIB)V

    .line 1606
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v2

    const/4 v3, 0x0

    iget v4, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 1607
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1608
    :cond_0
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V

    .line 1609
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1611
    :try_start_2
    iget v0, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->h:I

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v0, v0, 0x14

    add-int/lit8 v0, v0, 0x8

    const-wide/16 v2, -0x1

    const/16 v4, 0x20

    shr-long v4, v2, v4

    long-to-int v4, v4

    invoke-static {v1, v0, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    add-int/lit8 v0, v0, 0x4

    long-to-int v2, v2

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 1612
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    monitor-enter v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1613
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    iget-wide v2, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/util/a/h;->c(J)Ljava/lang/Object;

    .line 1614
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1620
    :cond_1
    :goto_0
    return-void

    .line 1609
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 1619
    :catch_0
    move-exception v0

    .line 1616
    if-eqz p2, :cond_1

    .line 1617
    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;Ljava/io/IOException;)V

    goto :goto_0

    .line 1614
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Lcom/google/android/apps/gmm/map/internal/d/b/ae;Lcom/google/android/apps/gmm/map/internal/d/b/ac;)V
    .locals 12

    .prologue
    .line 2207
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g(I)Ljava/io/RandomAccessFile;

    move-result-object v11

    .line 2208
    const/4 v0, 0x0

    :goto_0
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    if-ge v0, v1, :cond_4

    .line 2209
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v2, v0, 0x14

    add-int/lit8 v2, v2, 0x8

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v3

    int-to-long v4, v3

    add-int/lit8 v2, v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v2, v1

    const-wide v6, 0xffffffffL

    and-long/2addr v2, v6

    const/16 v1, 0x20

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v2, v0, 0x14

    add-int/lit8 v2, v2, 0x8

    add-int/lit8 v2, v2, 0x8

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    and-int/lit8 v1, v1, 0x1f

    if-lez v1, :cond_3

    .line 2210
    iget v1, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/w;->e:I

    if-ge v1, v2, :cond_0

    .line 2211
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a()I

    move-result v1

    const v2, 0x7ffffff

    if-lt v1, v2, :cond_1

    .line 2213
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Couldn\'t fit refcounted records into collecting shard"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2215
    :cond_1
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->d(I)Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    move-result-object v8

    .line 2216
    iget v1, v8, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    iget v2, v8, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/map/util/b;->a(I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v1

    .line 2218
    iget v2, v8, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v3

    .line 2219
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v4

    .line 2218
    invoke-static {v11, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/RandomAccessFile;I[BI)V

    .line 2220
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v3

    invoke-virtual {p3, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->a([BI)V

    .line 2221
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2222
    :cond_2
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    iget-wide v2, v8, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->a:J

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a()I

    move-result v4

    iget v5, v8, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    iget v6, v8, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    iget v7, v8, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->c:I

    iget v8, v8, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->f:I

    .line 2224
    iget v9, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    iget v10, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/gmm/map/internal/d/b/ad;-><init>(JIIIIIII)V

    .line 2226
    invoke-virtual {p2, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;)V

    .line 2208
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 2229
    :cond_4
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Z)V
    .locals 8

    .prologue
    .line 1031
    .line 1032
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    aget v0, v0, v1

    .line 1034
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    .line 1038
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a(I)V

    .line 1039
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f()V

    .line 1046
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1047
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    mul-int/lit16 v3, v3, 0x2000

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/w;->k:I

    add-int/2addr v3, v4

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 1048
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    const/4 v4, 0x0

    iget v5, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    const/4 v4, 0x4

    iget v5, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    const/4 v4, 0x0

    const/16 v5, 0x1ffc

    new-instance v6, Ljava/util/zip/CRC32;

    invoke-direct {v6}, Ljava/util/zip/CRC32;-><init>()V

    invoke-virtual {v6, v3, v4, v5}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-virtual {v6}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v4

    long-to-int v3, v4

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    const/16 v5, 0x1ffc

    invoke-static {v4, v5, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    invoke-virtual {v2, v3}, Ljava/io/RandomAccessFile;->write([B)V

    .line 1049
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/FileDescriptor;->sync()V

    .line 1050
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1053
    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->q:Lcom/google/android/apps/gmm/map/internal/d/b/u;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;)V

    .line 1054
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;)V

    .line 1055
    if-eqz p2, :cond_1

    .line 1056
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->u:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->u:I

    :goto_0
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    aput v0, v1, v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1061
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    .line 1065
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d(I)V

    .line 1066
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f()V

    .line 1067
    return-void

    .line 1050
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1061
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0

    .line 1056
    :cond_0
    :try_start_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v0, v4

    goto :goto_0

    .line 1058
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    aput v0, v1, v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1
.end method

.method private static a(Lcom/google/android/apps/gmm/map/internal/d/b/w;Lcom/google/android/apps/gmm/map/internal/d/b/ag;Lcom/google/android/apps/gmm/map/internal/d/b/u;Ljava/io/RandomAccessFile;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 765
    const/16 v1, 0x2000

    new-array v1, v1, [B

    .line 766
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/w;->a()[B

    move-result-object v2

    array-length v3, v2

    invoke-static {v2, v0, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v2, v2

    .line 767
    invoke-virtual {p3, v1}, Ljava/io/RandomAccessFile;->write([B)V

    .line 768
    invoke-virtual {p1, p3}, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a(Ljava/io/RandomAccessFile;)V

    .line 769
    :goto_0
    iget v1, p2, Lcom/google/android/apps/gmm/map/internal/d/b/u;->b:I

    if-ge v0, v1, :cond_0

    mul-int/lit16 v1, v0, 0x400

    iget-object v2, p2, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    const/16 v3, 0x3fc

    new-instance v4, Ljava/util/zip/CRC32;

    invoke-direct {v4}, Ljava/util/zip/CRC32;-><init>()V

    invoke-virtual {v4, v2, v1, v3}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-virtual {v4}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v2

    long-to-int v2, v2

    iget-object v3, p2, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    add-int/lit16 v1, v1, 0x3fc

    invoke-static {v3, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    invoke-virtual {p3, v0}, Ljava/io/RandomAccessFile;->write([B)V

    .line 770
    return-void
.end method

.method private static a(Ljava/io/File;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2616
    new-instance v1, Ljava/io/File;

    const-string v0, "cache_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, p0, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2617
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 2618
    return-void

    .line 2616
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/io/RandomAccessFile;I[BI)V
    .locals 2

    .prologue
    .line 3486
    monitor-enter p0

    .line 3487
    int-to-long v0, p1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 3488
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0, p3}, Ljava/io/RandomAccessFile;->readFully([BII)V

    .line 3489
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1195
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->b:[B

    .line 1196
    array-length v2, v1

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    if-eq v2, v3, :cond_0

    .line 1209
    :goto_0
    return v0

    .line 1200
    :cond_0
    array-length v2, v1

    if-nez v2, :cond_1

    .line 1202
    const/4 v0, 0x1

    goto :goto_0

    .line 1206
    :cond_1
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;)[B

    move-result-object v2

    .line 1207
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 1209
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;)[B
    .locals 4

    .prologue
    .line 1127
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    if-nez v0, :cond_0

    .line 1128
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a:[B

    .line 1136
    :goto_0
    return-object v0

    .line 1133
    :cond_0
    :try_start_0
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g(I)Ljava/io/RandomAccessFile;

    move-result-object v1

    .line 1134
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    new-array v0, v0, [B

    .line 1135
    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    array-length v3, v0

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/RandomAccessFile;I[BI)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1137
    :catch_0
    move-exception v0

    .line 1138
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;Ljava/io/IOException;)V

    .line 1139
    throw v0
.end method

.method private b(Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Lcom/google/android/apps/gmm/map/internal/d/b/ad;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1305
    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->a:J

    .line 1306
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    monitor-enter v2

    .line 1307
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/gmm/map/util/a/h;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    .line 1308
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1312
    if-eqz v0, :cond_0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 1316
    :cond_0
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aget v2, v2, v3

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_1

    .line 1319
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    monitor-enter v2

    .line 1320
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/gmm/map/util/a/h;->c(J)Ljava/lang/Object;

    .line 1321
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v1

    .line 1326
    :cond_1
    if-nez v0, :cond_2

    .line 1327
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c(Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Lcom/google/android/apps/gmm/map/internal/d/b/af;

    move-result-object v1

    .line 1328
    if-eqz v1, :cond_2

    .line 1329
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/b/af;->b:Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    .line 1332
    :cond_2
    return-object v0

    .line 1308
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1316
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 1321
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(I)Lcom/google/android/apps/gmm/map/internal/d/b/ae;
    .locals 6

    .prologue
    .line 1014
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    monitor-enter v1

    .line 1015
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    mul-int/lit16 v2, p1, 0x2000

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/d/b/w;->k:I

    add-int/2addr v2, v3

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 1016
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    const/16 v2, 0x2000

    new-array v2, v2, [B

    const/4 v3, 0x0

    const/16 v4, 0x2000

    invoke-virtual {v0, v2, v3, v4}, Ljava/io/RandomAccessFile;->readFully([BII)V

    const/4 v0, 0x0

    const/16 v3, 0x1ffc

    new-instance v4, Ljava/util/zip/CRC32;

    invoke-direct {v4}, Ljava/util/zip/CRC32;-><init>()V

    invoke-virtual {v4, v2, v0, v3}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-virtual {v4}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v4

    long-to-int v0, v4

    const/16 v3, 0x1ffc

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v3

    if-eq v0, v3, :cond_0

    new-instance v2, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x37

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unexpected checksum: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", expected: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1017
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1016
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;-><init>([B)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method private b(Lcom/google/android/apps/gmm/map/internal/d/b/ab;I)Lcom/google/android/apps/gmm/map/internal/d/b/af;
    .locals 12

    .prologue
    .line 1236
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->a:J

    .line 1237
    const/4 v0, 0x0

    .line 1240
    :try_start_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(I)Lcom/google/android/apps/gmm/map/internal/d/b/ae;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1257
    const-wide/16 v6, -0x1

    cmp-long v1, v2, v6

    if-nez v1, :cond_1

    .line 1258
    const/4 v0, 0x0

    .line 1296
    :cond_0
    :goto_0
    return-object v0

    .line 1241
    :catch_0
    move-exception v0

    .line 1242
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x43

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "lookupShardRecordIndexFromShard: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1247
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1249
    :try_start_1
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1251
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 1253
    const/4 v0, 0x0

    goto :goto_0

    .line 1251
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    throw v0

    .line 1260
    :cond_1
    iget v5, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    .line 1261
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v5, :cond_2

    .line 1262
    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v7, v1, 0x14

    add-int/lit8 v7, v7, 0x8

    invoke-static {v6, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v8

    int-to-long v8, v8

    add-int/lit8 v7, v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v6

    int-to-long v6, v6

    const-wide v10, 0xffffffffL

    and-long/2addr v6, v10

    const/16 v10, 0x20

    shl-long/2addr v8, v10

    or-long/2addr v6, v8

    .line 1263
    cmp-long v6, v2, v6

    if-nez v6, :cond_5

    .line 1264
    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->d(I)Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    move-result-object v6

    .line 1265
    invoke-direct {p0, v6, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1266
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/af;

    invoke-direct {v0, v4, v6, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/af;-><init>(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Lcom/google/android/apps/gmm/map/internal/d/b/ad;I)V

    .line 1271
    :cond_2
    if-eqz v0, :cond_0

    .line 1278
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v5, :cond_6

    .line 1279
    iget-object v2, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v3, v1, 0x14

    add-int/lit8 v3, v3, 0x8

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v6

    int-to-long v6, v6

    add-int/lit8 v3, v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v2

    int-to-long v2, v2

    const-wide v8, 0xffffffffL

    and-long/2addr v2, v8

    const/16 v8, 0x20

    shl-long/2addr v6, v8

    or-long/2addr v2, v6

    .line 1280
    const-wide/16 v6, -0x1

    cmp-long v6, v2, v6

    if-eqz v6, :cond_4

    .line 1281
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    monitor-enter v6

    .line 1282
    :try_start_2
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    invoke-virtual {v7, v2, v3}, Lcom/google/android/apps/gmm/map/util/a/h;->b(J)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_3

    .line 1283
    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->d(I)Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    move-result-object v7

    .line 1284
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    invoke-virtual {v8, v2, v3, v7}, Lcom/google/android/apps/gmm/map/util/a/h;->b(JLjava/lang/Object;)V

    .line 1286
    :cond_3
    monitor-exit v6

    .line 1278
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1261
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1286
    :catchall_1
    move-exception v0

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 1292
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    monitor-enter v1

    .line 1293
    :try_start_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/af;->b:Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    iget-wide v4, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->a:J

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/af;->b:Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    invoke-virtual {v2, v4, v5, v3}, Lcom/google/android/apps/gmm/map/util/a/h;->b(JLjava/lang/Object;)V

    .line 1294
    monitor-exit v1

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0
.end method

.method private b(Z)V
    .locals 13

    .prologue
    .line 2285
    .line 2288
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->x:I

    if-gt v0, v1, :cond_1

    .line 2380
    :cond_0
    :goto_0
    return-void

    .line 2293
    :cond_1
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    .line 2295
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->g:I

    sub-int/2addr v0, v1

    .line 2296
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    if-eqz v1, :cond_2

    .line 2297
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2300
    :cond_2
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->x:I

    if-ge v0, v1, :cond_0

    .line 2301
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2302
    const/4 v2, 0x0

    .line 2306
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x0

    :try_start_0
    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(III)Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    move-result-object v3

    .line 2307
    if-eqz v3, :cond_0

    .line 2311
    iget v0, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    .line 2312
    add-int/lit8 v1, v0, 0x1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    aget v0, v4, v0

    .line 2313
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b()I

    move-result v4

    .line 2312
    invoke-direct {p0, v1, v0, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(III)Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    move-result-object v0

    .line 2314
    if-eqz v0, :cond_0

    .line 2317
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Z)I

    move-result v4

    .line 2318
    const/4 v1, -0x1

    if-eq v4, v1, :cond_0

    .line 2323
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    invoke-direct {v1, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;-><init>(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2324
    :try_start_1
    new-instance v5, Lcom/google/android/apps/gmm/map/internal/d/b/ac;

    .line 2326
    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g(I)Ljava/io/RandomAccessFile;

    move-result-object v2

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->z:[B

    invoke-direct {v5, v2, v4, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/ac;-><init>(Ljava/io/RandomAccessFile;I[B)V

    .line 2327
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    move-object v2, v3

    .line 2329
    :goto_2
    if-eqz v2, :cond_5

    .line 2331
    invoke-direct {p0, v2, v1, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Lcom/google/android/apps/gmm/map/internal/d/b/ae;Lcom/google/android/apps/gmm/map/internal/d/b/ac;)V

    .line 2332
    iget v3, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2335
    invoke-interface {v4}, Ljava/util/Set;->size()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v3

    const/4 v6, 0x4

    if-ge v3, v6, :cond_5

    .line 2336
    if-eqz v0, :cond_4

    .line 2342
    const/4 v2, 0x0

    move-object v12, v2

    move-object v2, v0

    move-object v0, v12

    goto :goto_2

    .line 2293
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    goto :goto_1

    .line 2345
    :cond_4
    :try_start_2
    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    add-int/lit8 v2, v2, 0x1

    iget v3, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a()I

    move-result v6

    .line 2344
    invoke-direct {p0, v2, v3, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(III)Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    move-result-object v2

    goto :goto_2

    .line 2349
    :cond_5
    invoke-direct {p0, v5, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ac;Lcom/google/android/apps/gmm/map/internal/d/b/ae;)V

    .line 2350
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v3

    .line 2354
    :try_start_3
    invoke-direct {p0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(I)Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    move-result-object v4

    .line 2355
    const/4 v0, 0x0

    :goto_4
    iget v5, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    if-ge v0, v5, :cond_7

    .line 2356
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v6, v0, 0x14

    add-int/lit8 v6, v6, 0x8

    invoke-static {v5, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v7

    int-to-long v8, v7

    add-int/lit8 v6, v6, 0x4

    invoke-static {v5, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v5

    int-to-long v6, v5

    const-wide v10, 0xffffffffL

    and-long/2addr v6, v10

    const/16 v5, 0x20

    shl-long/2addr v8, v5

    or-long/2addr v6, v8

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_6

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v6, v0, 0x14

    add-int/lit8 v6, v6, 0x8

    add-int/lit8 v6, v6, 0x8

    invoke-static {v5, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v5

    and-int/lit8 v5, v5, 0x1f

    if-lez v5, :cond_6

    .line 2357
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v6, v0, 0x14

    add-int/lit8 v6, v6, 0x8

    const-wide/16 v8, -0x1

    const/16 v7, 0x20

    shr-long v10, v8, v7

    long-to-int v7, v10

    invoke-static {v5, v6, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    add-int/lit8 v6, v6, 0x4

    long-to-int v7, v8

    invoke-static {v5, v6, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 2355
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2360
    :cond_7
    const/4 v0, 0x0

    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Z)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_3

    .line 2362
    :catch_0
    move-exception v0

    :try_start_4
    invoke-direct {p0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e(I)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_3

    .line 2371
    :catch_1
    move-exception v0

    .line 2374
    :goto_5
    if-eqz v1, :cond_8

    .line 2375
    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e(I)V

    .line 2377
    :cond_8
    const-string v1, "Failed to combine refCounted records"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 2371
    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_5
.end method

.method private c(Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Lcom/google/android/apps/gmm/map/internal/d/b/af;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1343
    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->a:J

    .line 1344
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->q:Lcom/google/android/apps/gmm/map/internal/d/b/u;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->c:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    invoke-static {v4, v5, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a(J[I)[I

    move-result-object v3

    .line 1345
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    move v1, v2

    .line 1346
    :goto_0
    if-ge v1, v4, :cond_2

    .line 1347
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aget v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->q:Lcom/google/android/apps/gmm/map/internal/d/b/u;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a([II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1348
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(Lcom/google/android/apps/gmm/map/internal/d/b/ab;I)Lcom/google/android/apps/gmm/map/internal/d/b/af;

    move-result-object v0

    .line 1349
    if-eqz v0, :cond_1

    .line 1355
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 1347
    goto :goto_1

    .line 1346
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1354
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->q:Lcom/google/android/apps/gmm/map/internal/d/b/u;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->c:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z

    .line 1355
    const/4 v0, 0x0

    goto :goto_2
.end method

.method static c()Lcom/google/android/apps/gmm/map/internal/d/b/z;
    .locals 2

    .prologue
    .line 1813
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/z;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/z;-><init>(Z)V

    return-object v0
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 1074
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1076
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1078
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1081
    :cond_0
    return-void

    .line 1078
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private d()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1147
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->t:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 1163
    :cond_0
    return-void

    .line 1154
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->t:Ljava/util/Set;

    monitor-enter v1

    .line 1155
    :try_start_0
    new-instance v4, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->t:Ljava/util/Set;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1156
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->t:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 1157
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1159
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_0

    .line 1160
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    .line 1161
    :try_start_1
    iget v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(I)Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    move-result-object v1

    iget v2, v1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->h:I

    if-le v2, v6, :cond_2

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->h:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->d(I)Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    move-result-object v2

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->a:J

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->a:J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    cmp-long v2, v6, v8

    if-eqz v2, :cond_3

    .line 1159
    :cond_2
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1157
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1161
    :cond_3
    :try_start_3
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Lcom/google/android/apps/gmm/map/internal/d/b/ad;)V

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Z)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    move-object v2, v1

    const-string v1, "GenericDiskCache"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->n:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v6, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c(I)V

    goto :goto_1

    :cond_4
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private d(I)V
    .locals 7

    .prologue
    .line 2047
    .line 2048
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    monitor-enter v1

    .line 2049
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    mul-int/lit16 v2, p1, 0x400

    add-int/lit16 v2, v2, 0x4000

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 2050
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->q:Lcom/google/android/apps/gmm/map/internal/d/b/u;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    mul-int/lit16 v3, p1, 0x400

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    const/16 v5, 0x3fc

    new-instance v6, Ljava/util/zip/CRC32;

    invoke-direct {v6}, Ljava/util/zip/CRC32;-><init>()V

    invoke-virtual {v6, v4, v3, v5}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-virtual {v6}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v4

    long-to-int v4, v4

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    add-int/lit16 v3, v3, 0x3fc

    invoke-static {v5, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    mul-int/lit16 v3, p1, 0x400

    const/16 v4, 0x400

    invoke-virtual {v2, v0, v3, v4}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 2051
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V

    .line 2052
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1172
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 1184
    :cond_0
    :goto_0
    return-void

    .line 1176
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1181
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1183
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private e(I)V
    .locals 4

    .prologue
    .line 2556
    .line 2558
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;-><init>(I)V

    .line 2560
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Z)V

    .line 2561
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2562
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/h;->a()V

    .line 2563
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2564
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2568
    :goto_0
    return-void

    .line 2563
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 2565
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2566
    const-string v0, "GenericDiskCache"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->n:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private f()V
    .locals 4

    .prologue
    .line 2061
    .line 2062
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    monitor-enter v1

    .line 2063
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    const-wide/16 v2, 0x2000

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 2064
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a(Ljava/io/RandomAccessFile;)V

    .line 2065
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V

    .line 2066
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private f(I)V
    .locals 2

    .prologue
    .line 2578
    .line 2579
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e(I)V

    .line 2582
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    .line 2583
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 2584
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 2586
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->o:Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/File;Ljava/lang/String;)V

    .line 2587
    return-void
.end method

.method private g()Lcom/google/android/apps/gmm/map/internal/d/b/ae;
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v5, -0x1

    const/4 v1, 0x0

    .line 2083
    .line 2084
    sget-boolean v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    .line 2090
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    if-ge v0, v2, :cond_a

    .line 2092
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->c:[I

    aget v2, v2, v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/w;->e:I

    if-ge v2, v4, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->b:[I

    aget v2, v2, v0

    const v4, 0x7ffffff

    if-gt v2, v4, :cond_2

    .line 2096
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(I)Lcom/google/android/apps/gmm/map/internal/d/b/ae;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2110
    :goto_1
    if-ne v0, v5, :cond_7

    .line 2113
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->b()I

    move-result v0

    const/16 v4, 0x14

    if-ge v0, v4, :cond_3

    move v0, v1

    .line 2114
    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(Z)V

    .line 2118
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-boolean v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/w;->f:Z

    if-eqz v4, :cond_5

    if-eqz v0, :cond_5

    move v4, v1

    .line 2122
    :goto_3
    const/4 v6, 0x2

    if-ge v4, v6, :cond_5

    .line 2123
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a(Ljava/util/Set;)I

    move-result v6

    .line 2124
    if-eq v6, v5, :cond_1

    .line 2125
    invoke-direct {p0, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f(I)V

    .line 2122
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 2100
    :catch_0
    move-exception v2

    .line 2102
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v6, 0x1f

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "allocateShardToUse: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v2, v3

    .line 2104
    goto :goto_1

    .line 2090
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2113
    :cond_3
    invoke-static {}, Lcom/google/android/apps/gmm/shared/c/h;->b()J

    move-result-wide v6

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a()J

    move-result-wide v8

    add-long/2addr v6, v8

    long-to-double v6, v6

    const-wide/high16 v10, 0x3fd0000000000000L    # 0.25

    mul-double/2addr v6, v10

    double-to-long v6, v6

    cmp-long v0, v6, v8

    if-gez v0, :cond_4

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_2

    .line 2130
    :cond_5
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Z)I

    move-result v0

    .line 2131
    if-ne v0, v5, :cond_7

    .line 2134
    const-string v0, "GenericDiskCache"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->n:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    const-string v2, "Tile store full, unable to allocate shard"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 2147
    :goto_5
    return-object v0

    .line 2134
    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    :cond_7
    move v1, v0

    .line 2139
    if-eqz v2, :cond_8

    .line 2140
    iget v0, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->c:[I

    aget v3, v3, v1

    if-eq v0, v3, :cond_9

    .line 2142
    :cond_8
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;-><init>(I)V

    .line 2146
    :goto_6
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_9
    move-object v0, v2

    goto :goto_6

    :cond_a
    move-object v2, v3

    move v0, v5

    goto/16 :goto_1
.end method

.method private g(I)Ljava/io/RandomAccessFile;
    .locals 5

    .prologue
    .line 2594
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    monitor-enter v1

    .line 2595
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 2596
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->o:Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/File;Ljava/lang/String;Z)Ljava/io/RandomAccessFile;

    move-result-object v2

    aput-object v2, v0, p1

    .line 2598
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    aget-object v0, v0, p1

    monitor-exit v1

    return-object v0

    .line 2599
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private h(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2606
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 806
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 808
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    move v1, v0

    :goto_0
    iget v3, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    if-ge v0, v3, :cond_0

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aget v3, v3, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 810
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    return v1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method a(IIILcom/google/android/apps/gmm/map/internal/d/b/y;)I
    .locals 10

    .prologue
    .line 3651
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->A:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    .line 3655
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(I)Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    move-result-object v4

    .line 3656
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g(I)Ljava/io/RandomAccessFile;

    move-result-object v1

    .line 3657
    const/4 v0, 0x0

    .line 3658
    :goto_0
    iget v5, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    if-ge p2, v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->A:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v6

    sub-long/2addr v6, v2

    int-to-long v8, p3

    cmp-long v5, v6, v8

    if-gez v5, :cond_1

    .line 3660
    invoke-virtual {v4, p2}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->d(I)Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    move-result-object v5

    .line 3661
    iget-wide v6, v5, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->a:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-eqz v6, :cond_0

    .line 3663
    const/4 v6, 0x0

    invoke-direct {p0, v1, v5, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/RandomAccessFile;Lcom/google/android/apps/gmm/map/internal/d/b/ad;I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v6

    .line 3664
    if-eqz v6, :cond_0

    invoke-interface {p4, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/y;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 3666
    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Lcom/google/android/apps/gmm/map/internal/d/b/ad;)V

    .line 3667
    const/4 v0, 0x1

    .line 3659
    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 3672
    :cond_1
    iget v1, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-lt p2, v1, :cond_4

    .line 3673
    const/4 v1, -0x1

    .line 3676
    :goto_1
    if-eqz v0, :cond_2

    .line 3677
    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_2
    move v0, v1

    .line 3690
    :goto_2
    return v0

    .line 3679
    :catch_0
    move-exception v0

    .line 3680
    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x37

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Exception reading "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " shard "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " record "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3684
    :try_start_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 3688
    :goto_4
    const/4 v0, -0x1

    goto :goto_2

    .line 3685
    :catch_1
    move-exception v1

    .line 3686
    const-string v0, "GenericDiskCache"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->n:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 3679
    :catch_2
    move-exception v0

    move p2, v1

    goto :goto_3

    :cond_4
    move v1, p2

    goto :goto_1
.end method

.method final a(JLjava/lang/String;I)I
    .locals 15

    .prologue
    .line 2000
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 2001
    :try_start_0
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a:[B

    if-eqz p3, :cond_0

    sget-object v2, Lcom/google/b/a/w;->a:Ljava/nio/charset/Charset;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    :cond_0
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    move-wide/from16 v0, p1

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/ab;-><init>(J[B)V

    .line 2004
    invoke-direct {p0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c(Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Lcom/google/android/apps/gmm/map/internal/d/b/af;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2005
    if-nez v2, :cond_1

    .line 2006
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    const/4 v9, -0x1

    .line 2038
    :goto_0
    return v9

    .line 2009
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/af;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    .line 2010
    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    aget v13, v3, v4

    .line 2012
    iget-object v12, v2, Lcom/google/android/apps/gmm/map/internal/d/b/af;->b:Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    .line 2014
    and-int/lit8 v9, p4, 0x1f

    .line 2015
    iget v3, v12, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-ne v3, v9, :cond_2

    .line 2016
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 2019
    :cond_2
    :try_start_2
    iget v3, v12, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->c:I

    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/map/internal/d/aj;->a(II)I

    move-result v3

    and-int/lit8 v9, v3, 0x1f

    .line 2022
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    iget-wide v4, v12, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->a:J

    iget v6, v12, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    iget v7, v12, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    iget v8, v12, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    iget v10, v12, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->f:I

    iget v11, v12, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    iget v12, v12, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->h:I

    invoke-direct/range {v3 .. v12}, Lcom/google/android/apps/gmm/map/internal/d/b/ad;-><init>(JIIIIIII)V

    .line 2024
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/af;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    iget v5, v2, Lcom/google/android/apps/gmm/map/internal/d/b/af;->c:I

    invoke-virtual {v4, v3, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;I)V

    .line 2025
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/af;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    const/4 v5, 0x1

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Z)V

    .line 2026
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    monitor-enter v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2027
    :try_start_3
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    iget-wide v6, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->a:J

    invoke-virtual {v5, v6, v7, v3}, Lcom/google/android/apps/gmm/map/util/a/h;->b(JLjava/lang/Object;)V

    .line 2028
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2031
    if-nez v13, :cond_3

    :try_start_4
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/af;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    .line 2032
    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    aget v2, v3, v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 2033
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(Z)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2036
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 2028
    :catchall_0
    move-exception v2

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2036
    :catchall_1
    move-exception v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2
.end method

.method final a(Ljava/util/List;)I
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/b/z;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1629
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->D:Lcom/google/android/apps/gmm/map/internal/d/b/x;

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/b/x;->a:Lcom/google/android/apps/gmm/map/internal/d/b/x;

    if-eq v2, v3, :cond_0

    .line 1630
    const/4 v3, 0x0

    .line 1685
    :goto_0
    return v3

    .line 1632
    :cond_0
    const/4 v3, 0x0

    .line 1633
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1635
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v2, :cond_1

    .line 1636
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    const/4 v3, -0x1

    goto :goto_0

    .line 1638
    :cond_1
    const/4 v2, 0x0

    :try_start_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v15

    move v14, v2

    :goto_1
    if-ge v14, v15, :cond_3

    .line 1639
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;

    .line 1640
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c(Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Lcom/google/android/apps/gmm/map/internal/d/b/af;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v16

    .line 1641
    const/4 v4, 0x0

    .line 1642
    if-eqz v16, :cond_4

    .line 1644
    :try_start_2
    move-object/from16 v0, v16

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/d/b/af;->b:Lcom/google/android/apps/gmm/map/internal/d/b/ad;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1645
    :try_start_3
    iget v4, v13, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g(I)Ljava/io/RandomAccessFile;

    move-result-object v4

    .line 1648
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    .line 1651
    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v13, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/RandomAccessFile;Lcom/google/android/apps/gmm/map/internal/d/b/ad;I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v5

    .line 1653
    iget v6, v13, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->f:I

    .line 1654
    iget-object v7, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    .line 1653
    invoke-static {v6, v5, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a(ILcom/google/android/apps/gmm/map/util/f;Lcom/google/android/apps/gmm/map/util/f;)I

    move-result v10

    .line 1655
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/util/f;->c()V

    .line 1658
    monitor-enter v4
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1659
    :try_start_4
    iget v5, v13, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    iget v6, v13, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    add-int/2addr v5, v6

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 1660
    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v5

    const/4 v6, 0x0

    .line 1661
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v2

    .line 1660
    invoke-virtual {v4, v5, v6, v2}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 1662
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/FileDescriptor;->sync()V

    .line 1663
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1664
    add-int/lit8 v2, v3, 0x1

    .line 1667
    :try_start_5
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    iget-wide v4, v13, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->a:J

    iget v6, v13, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    iget v7, v13, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    iget v8, v13, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    iget v9, v13, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->c:I

    iget v11, v13, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    iget v12, v13, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->h:I

    invoke-direct/range {v3 .. v12}, Lcom/google/android/apps/gmm/map/internal/d/b/ad;-><init>(JIIIIIII)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1669
    :try_start_6
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/af;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    move-object/from16 v0, v16

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/af;->c:I

    invoke-virtual {v4, v3, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;I)V

    .line 1670
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/af;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ae;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;Z)V

    .line 1671
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    monitor-enter v5
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1672
    :try_start_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    iget-wide v6, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->a:J

    invoke-virtual {v4, v6, v7, v3}, Lcom/google/android/apps/gmm/map/util/a/h;->b(JLjava/lang/Object;)V

    .line 1673
    monitor-exit v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1674
    :try_start_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1638
    :cond_2
    :goto_2
    add-int/lit8 v3, v14, 0x1

    move v14, v3

    move v3, v2

    goto/16 :goto_1

    .line 1663
    :catchall_0
    move-exception v2

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :try_start_a
    throw v2
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 1675
    :catch_0
    move-exception v2

    move-object v4, v13

    move-object/from16 v17, v2

    move v2, v3

    move-object/from16 v3, v17

    .line 1676
    :goto_3
    if-eqz v4, :cond_2

    .line 1677
    :try_start_b
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;Ljava/io/IOException;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_2

    .line 1683
    :catchall_1
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2

    .line 1673
    :catchall_2
    move-exception v4

    :try_start_c
    monitor-exit v5
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :try_start_d
    throw v4
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 1675
    :catch_1
    move-exception v4

    move-object/from16 v17, v4

    move-object v4, v3

    move-object/from16 v3, v17

    goto :goto_3

    .line 1683
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    .line 1675
    :catch_2
    move-exception v2

    move-object/from16 v17, v2

    move v2, v3

    move-object/from16 v3, v17

    goto :goto_3

    :catch_3
    move-exception v3

    move-object v4, v13

    goto :goto_3

    :cond_4
    move v2, v3

    goto :goto_2
.end method

.method final a(JLjava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/aw;)Lcom/google/android/apps/gmm/map/util/f;
    .locals 3

    .prologue
    .line 1469
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a:[B

    if-eqz p3, :cond_0

    sget-object v0, Lcom/google/b/a/w;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p3, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    invoke-direct {v1, p1, p2, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ab;-><init>(J[B)V

    .line 1470
    invoke-virtual {p4}, Lcom/google/android/apps/gmm/map/internal/d/aw;->c()B

    move-result v0

    .line 1469
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ab;I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v0

    return-object v0
.end method

.method a(Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Lcom/google/android/apps/gmm/map/util/f;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1490
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 1492
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 1493
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1530
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e()V

    :goto_0
    return-object v0

    .line 1495
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Lcom/google/android/apps/gmm/map/internal/d/b/ad;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 1496
    if-eqz v3, :cond_1

    .line 1499
    :try_start_2
    iget v1, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g(I)Ljava/io/RandomAccessFile;

    move-result-object v2

    .line 1500
    iget v1, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/map/util/b;->a(I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v1

    .line 1501
    iget v4, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    iget v5, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    add-int/2addr v4, v5

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v5

    .line 1502
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v6

    .line 1501
    invoke-static {v2, v4, v5, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/RandomAccessFile;I[BI)V

    .line 1506
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v2

    .line 1505
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a([B)Lcom/google/android/apps/gmm/map/internal/d/b/k;

    move-result-object v2

    .line 1507
    if-eqz v2, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->D:Lcom/google/android/apps/gmm/map/internal/d/b/x;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/b/x;->a:Lcom/google/android/apps/gmm/map/internal/d/b/x;

    if-ne v4, v5, :cond_2

    .line 1508
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->c()Lcom/google/android/apps/gmm/map/internal/d/b/d;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/d;->a(Lcom/google/android/apps/gmm/map/util/f;)I

    move-result v2

    .line 1515
    :goto_1
    iget v4, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->f:I

    if-eq v2, v4, :cond_3

    .line 1516
    new-instance v1, Ljava/io/IOException;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->n:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x29

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Checksum mismatch for cache "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " :  record ["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1523
    :catch_0
    move-exception v1

    .line 1524
    if-eqz v3, :cond_1

    :try_start_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->t:Ljava/util/Set;

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1527
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1530
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e()V

    goto/16 :goto_0

    .line 1512
    :cond_2
    :try_start_4
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/b/c;->g:Lcom/google/android/apps/gmm/map/internal/d/b/d;

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/d;->a(Lcom/google/android/apps/gmm/map/util/f;)I

    move-result v2

    goto :goto_1

    .line 1521
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v5, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->u:I

    if-ltz v2, :cond_4

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->u:I

    :goto_2
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    aput v2, v4, v5
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1529
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1530
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e()V

    move-object v0, v1

    goto/16 :goto_0

    .line 1521
    :cond_4
    :try_start_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    long-to-int v2, v6

    goto :goto_2

    .line 1529
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1530
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e()V

    throw v0
.end method

.method a(Lcom/google/android/apps/gmm/map/internal/d/b/ab;I)Lcom/google/android/apps/gmm/map/util/f;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1538
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 1540
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 1541
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1565
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e()V

    :goto_0
    return-object v0

    .line 1543
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    move-result-object v3

    .line 1544
    if-eqz v3, :cond_3

    .line 1546
    iget v1, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    if-nez v1, :cond_1

    .line 1547
    iget v0, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/util/b;->a(I)Lcom/google/android/apps/gmm/map/util/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1564
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1565
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e()V

    goto :goto_0

    .line 1552
    :cond_1
    :try_start_2
    iget v1, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g(I)Ljava/io/RandomAccessFile;

    move-result-object v1

    .line 1553
    invoke-direct {p0, v1, v3, p2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/RandomAccessFile;Lcom/google/android/apps/gmm/map/internal/d/b/ad;I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v1

    .line 1556
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget v5, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->u:I

    if-ltz v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->u:I

    :goto_1
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    aput v2, v4, v5
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1564
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1565
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e()V

    move-object v0, v1

    goto :goto_0

    .line 1556
    :cond_2
    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    long-to-int v2, v6

    goto :goto_1

    .line 1558
    :catch_0
    move-exception v1

    .line 1559
    :try_start_4
    invoke-direct {p0, v3, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;Ljava/io/IOException;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1562
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1565
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e()V

    goto :goto_0

    .line 1564
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1565
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e()V

    throw v0
.end method

.method final a(I)V
    .locals 1

    .prologue
    .line 964
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->a:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(II)V

    .line 965
    return-void
.end method

.method final a(ILjava/util/Locale;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 901
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g:Lcom/google/android/apps/gmm/map/internal/d/b/aa;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/aa;->a(F)I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/util/b;->a(F)I

    .line 902
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 906
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f:Z

    .line 907
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 909
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    monitor-enter v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->s:Lcom/google/android/apps/gmm/map/util/a/h;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/a/h;->a()V

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->t:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->o:Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/File;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 910
    :catch_0
    move-exception v0

    .line 911
    :try_start_6
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b()V

    .line 912
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 914
    :catchall_1
    move-exception v0

    :try_start_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 917
    :catchall_2
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 909
    :cond_1
    :try_start_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->o:Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->n:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v0, ".m"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/File;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->o:Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->n:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v0, ".m"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/io/File;Ljava/lang/String;Z)Ljava/io/RandomAccessFile;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->B:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/shared/net/a/b;)I

    move-result v0

    const/16 v2, 0x16

    shl-int/lit8 v0, v0, 0x10

    add-int/2addr v2, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->c:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->e:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->f:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->A:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v8

    move v7, p1

    move-object v10, p2

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/gmm/map/internal/d/b/w;-><init>(IIIIZIJLjava/util/Locale;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->b:[I

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->c:[I

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->g:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->q:Lcom/google/android/apps/gmm/map/internal/d/b/u;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ag;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->q:Lcom/google/android/apps/gmm/map/internal/d/b/u;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/w;Lcom/google/android/apps/gmm/map/internal/d/b/ag;Lcom/google/android/apps/gmm/map/internal/d/b/u;Ljava/io/RandomAccessFile;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f:Z
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 914
    :try_start_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 917
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 918
    return-void

    .line 909
    :cond_2
    :try_start_a
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/d/b/z;)Z
    .locals 1

    .prologue
    .line 1830
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/internal/d/b/z;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g:Lcom/google/android/apps/gmm/map/internal/d/b/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/aa;->a(Lcom/google/android/apps/gmm/map/internal/d/b/z;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/util/List;)I
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/b/z;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1886
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->C:Lcom/google/android/apps/gmm/map/e/a;

    if-eqz v2, :cond_0

    .line 1887
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->C:Lcom/google/android/apps/gmm/map/e/a;

    sget-object v3, Lcom/google/android/apps/gmm/map/e/a/e;->h:Lcom/google/android/apps/gmm/map/e/a/e;

    const-string v4, "DiskCacheBatchedCommit"

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/gmm/map/e/a;->a(Lcom/google/android/apps/gmm/map/e/a/e;Ljava/lang/String;BLjava/lang/Object;)V

    .line 1891
    :cond_0
    const/4 v4, 0x0

    .line 1892
    const/4 v3, 0x0

    .line 1894
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1896
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_2

    .line 1897
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    .line 1964
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1966
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->C:Lcom/google/android/apps/gmm/map/e/a;

    if-eqz v2, :cond_1

    .line 1967
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->C:Lcom/google/android/apps/gmm/map/e/a;

    sget-object v3, Lcom/google/android/apps/gmm/map/e/a/e;->h:Lcom/google/android/apps/gmm/map/e/a/e;

    const-string v4, "DiskCacheBatchedCommit"

    const/4 v5, 0x2

    new-instance v6, Lcom/google/android/apps/gmm/map/e/a/g;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8}, Lcom/google/android/apps/gmm/map/e/a/g;-><init>(II)V

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/gmm/map/e/a;->a(Lcom/google/android/apps/gmm/map/e/a/e;Ljava/lang/String;BLjava/lang/Object;)V

    :cond_1
    const/4 v2, -0x1

    .line 1971
    :goto_0
    return v2

    .line 1899
    :cond_2
    :try_start_1
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v18

    .line 1902
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d()V

    .line 1904
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    .line 1905
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g()Lcom/google/android/apps/gmm/map/internal/d/b/ae;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 1906
    if-nez v6, :cond_4

    .line 1907
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    .line 1964
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1966
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->C:Lcom/google/android/apps/gmm/map/e/a;

    if-eqz v2, :cond_3

    .line 1967
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->C:Lcom/google/android/apps/gmm/map/e/a;

    sget-object v3, Lcom/google/android/apps/gmm/map/e/a/e;->h:Lcom/google/android/apps/gmm/map/e/a/e;

    const-string v4, "DiskCacheBatchedCommit"

    const/4 v5, 0x2

    new-instance v6, Lcom/google/android/apps/gmm/map/e/a/g;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8}, Lcom/google/android/apps/gmm/map/e/a/g;-><init>(II)V

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/gmm/map/e/a;->a(Lcom/google/android/apps/gmm/map/e/a/e;Ljava/lang/String;BLjava/lang/Object;)V

    :cond_3
    const/4 v2, -0x1

    goto :goto_0

    .line 1909
    :cond_4
    :try_start_2
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 1910
    new-instance v5, Lcom/google/android/apps/gmm/map/internal/d/b/ac;

    .line 1911
    iget v2, v6, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g(I)Ljava/io/RandomAccessFile;

    move-result-object v2

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->y:[B

    invoke-direct {v5, v2, v7, v8}, Lcom/google/android/apps/gmm/map/internal/d/b/ac;-><init>(Ljava/io/RandomAccessFile;I[B)V

    .line 1912
    const/4 v2, 0x0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v20

    move v15, v2

    move v13, v3

    move v14, v4

    move-object v3, v5

    move-object v4, v6

    :goto_1
    move/from16 v0, v20

    if-ge v15, v0, :cond_9

    .line 1913
    :try_start_3
    move-object/from16 v0, v18

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;

    .line 1916
    iget v5, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->c:I

    and-int/lit8 v9, v5, 0x1f

    .line 1918
    iget v5, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v6, v6, Lcom/google/android/apps/gmm/map/internal/d/b/w;->e:I

    if-ge v5, v6, :cond_5

    .line 1919
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a()I

    move-result v5

    const v6, 0x7ffffff

    if-le v5, v6, :cond_c

    .line 1920
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ac;Lcom/google/android/apps/gmm/map/internal/d/b/ae;)V

    .line 1921
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->clear()V

    .line 1923
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g()Lcom/google/android/apps/gmm/map/internal/d/b/ae;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v17

    .line 1924
    if-nez v17, :cond_7

    .line 1925
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    .line 1964
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1966
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->C:Lcom/google/android/apps/gmm/map/e/a;

    if-eqz v2, :cond_6

    .line 1967
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->C:Lcom/google/android/apps/gmm/map/e/a;

    sget-object v3, Lcom/google/android/apps/gmm/map/e/a/e;->h:Lcom/google/android/apps/gmm/map/e/a/e;

    const-string v4, "DiskCacheBatchedCommit"

    const/4 v5, 0x2

    new-instance v6, Lcom/google/android/apps/gmm/map/e/a/g;

    invoke-direct {v6, v14, v13}, Lcom/google/android/apps/gmm/map/e/a/g;-><init>(II)V

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/gmm/map/e/a;->a(Lcom/google/android/apps/gmm/map/e/a/e;Ljava/lang/String;BLjava/lang/Object;)V

    :cond_6
    const/4 v2, -0x1

    goto/16 :goto_0

    .line 1927
    :cond_7
    :try_start_4
    new-instance v16, Lcom/google/android/apps/gmm/map/internal/d/b/ac;

    .line 1928
    move-object/from16 v0, v17

    iget v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g(I)Ljava/io/RandomAccessFile;

    move-result-object v3

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->y:[B

    move-object/from16 v0, v16

    invoke-direct {v0, v3, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/ac;-><init>(Ljava/io/RandomAccessFile;I[B)V

    .line 1931
    :goto_2
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->b:[B

    array-length v4, v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->a([BI)V

    .line 1932
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->a([BI)V

    .line 1933
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->e:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->e:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->a([BI)V

    .line 1937
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v3

    .line 1936
    invoke-static {v3}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a([B)Lcom/google/android/apps/gmm/map/internal/d/b/k;

    move-result-object v3

    .line 1938
    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->D:Lcom/google/android/apps/gmm/map/internal/d/b/x;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/b/x;->a:Lcom/google/android/apps/gmm/map/internal/d/b/x;

    if-ne v4, v5, :cond_8

    .line 1939
    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->c()Lcom/google/android/apps/gmm/map/internal/d/b/d;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/d;->a(Lcom/google/android/apps/gmm/map/internal/d/b/z;)I

    move-result v10

    .line 1946
    :goto_3
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->a:J

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a()I

    move-result v6

    .line 1947
    iget-object v7, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->b:[B

    array-length v7, v7

    .line 1948
    iget-object v8, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v8

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->e:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v11

    add-int/2addr v8, v11

    .line 1949
    move-object/from16 v0, v17

    iget v11, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    move-object/from16 v0, v17

    iget v12, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    invoke-direct/range {v3 .. v12}, Lcom/google/android/apps/gmm/map/internal/d/b/ad;-><init>(JIIIIIII)V

    .line 1951
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;)V

    .line 1952
    iget-wide v4, v3, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1956
    add-int/lit8 v3, v14, 0x1

    .line 1957
    :try_start_5
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->b:[B

    array-length v4, v4

    .line 1958
    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->e:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/f;->b()I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v13, v2

    .line 1912
    add-int/lit8 v2, v15, 0x1

    move v15, v2

    move-object/from16 v4, v17

    move v14, v3

    move-object/from16 v3, v16

    goto/16 :goto_1

    .line 1943
    :cond_8
    :try_start_6
    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/b/c;->g:Lcom/google/android/apps/gmm/map/internal/d/b/d;

    invoke-interface {v3, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/d;->a(Lcom/google/android/apps/gmm/map/internal/d/b/z;)I

    move-result v10

    goto :goto_3

    .line 1961
    :cond_9
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ac;Lcom/google/android/apps/gmm/map/internal/d/b/ae;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1963
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    .line 1964
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1966
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->C:Lcom/google/android/apps/gmm/map/e/a;

    if-eqz v2, :cond_a

    .line 1967
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->C:Lcom/google/android/apps/gmm/map/e/a;

    sget-object v3, Lcom/google/android/apps/gmm/map/e/a/e;->h:Lcom/google/android/apps/gmm/map/e/a/e;

    const-string v4, "DiskCacheBatchedCommit"

    const/4 v5, 0x2

    new-instance v6, Lcom/google/android/apps/gmm/map/e/a/g;

    invoke-direct {v6, v14, v13}, Lcom/google/android/apps/gmm/map/e/a/g;-><init>(II)V

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/gmm/map/e/a;->a(Lcom/google/android/apps/gmm/map/e/a/e;Ljava/lang/String;BLjava/lang/Object;)V

    .line 1971
    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1963
    :catchall_0
    move-exception v2

    :goto_4
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->w:Ljava/util/Set;

    .line 1964
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1966
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->C:Lcom/google/android/apps/gmm/map/e/a;

    if-eqz v5, :cond_b

    .line 1967
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->C:Lcom/google/android/apps/gmm/map/e/a;

    sget-object v6, Lcom/google/android/apps/gmm/map/e/a/e;->h:Lcom/google/android/apps/gmm/map/e/a/e;

    const-string v7, "DiskCacheBatchedCommit"

    const/4 v8, 0x2

    new-instance v9, Lcom/google/android/apps/gmm/map/e/a/g;

    invoke-direct {v9, v4, v3}, Lcom/google/android/apps/gmm/map/e/a/g;-><init>(II)V

    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/e/a;->a(Lcom/google/android/apps/gmm/map/e/a/e;Ljava/lang/String;BLjava/lang/Object;)V

    :cond_b
    throw v2

    .line 1963
    :catchall_1
    move-exception v2

    move v3, v13

    move v4, v14

    goto :goto_4

    :catchall_2
    move-exception v2

    move v4, v3

    move v3, v13

    goto :goto_4

    :cond_c
    move-object/from16 v16, v3

    move-object/from16 v17, v4

    goto/16 :goto_2
.end method

.method final b(JLjava/lang/String;)Lcom/google/android/apps/gmm/map/util/f;
    .locals 3

    .prologue
    .line 1445
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a:[B

    if-eqz p3, :cond_0

    sget-object v0, Lcom/google/b/a/w;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p3, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    invoke-direct {v1, p1, p2, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ab;-><init>(J[B)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v0

    return-object v0
.end method

.method final b()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 843
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 845
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_0

    .line 884
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 885
    :goto_0
    return-void

    .line 851
    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f:Z

    .line 852
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 858
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->f()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 863
    :goto_1
    :try_start_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->p:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 867
    :goto_2
    const/4 v1, 0x0

    :goto_3
    :try_start_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 868
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    aget-object v2, v2, v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v2, :cond_1

    .line 870
    :try_start_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 874
    :goto_4
    :try_start_6
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->r:[Ljava/io/RandomAccessFile;

    const/4 v3, 0x0

    aput-object v3, v2, v1

    .line 867
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 877
    :cond_2
    if-eqz v0, :cond_3

    .line 878
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 881
    :catchall_0
    move-exception v0

    :try_start_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 884
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 881
    :cond_3
    :try_start_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 884
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 859
    :catch_0
    move-exception v0

    goto :goto_1

    .line 871
    :catch_1
    move-exception v0

    goto :goto_4

    .line 864
    :catch_2
    move-exception v0

    goto :goto_2
.end method

.method public final c(JLjava/lang/String;)Z
    .locals 3

    .prologue
    .line 1692
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 1694
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a:[B

    if-eqz p3, :cond_0

    sget-object v0, Lcom/google/b/a/w;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p3, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    invoke-direct {v1, p1, p2, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ab;-><init>(J[B)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Lcom/google/android/apps/gmm/map/internal/d/b/ad;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1696
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1697
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e()V

    return v0

    .line 1694
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1696
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1697
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->e()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 10

    .prologue
    .line 3479
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->n:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/w;->i:Ljava/util/Locale;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/map/internal/d/b/w;->f:Z

    .line 3480
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v6, v6, Lcom/google/android/apps/gmm/map/internal/d/b/w;->e:I

    mul-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v6, v6, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x5e

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " ver:"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " locale: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " auto:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " size:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " max:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " max_shards:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
