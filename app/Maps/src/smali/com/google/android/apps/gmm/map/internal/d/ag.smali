.class public Lcom/google/android/apps/gmm/map/internal/d/ag;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:J


# instance fields
.field public final b:Lcom/google/android/apps/gmm/map/b/a/ai;

.field final c:Lcom/google/android/apps/gmm/map/internal/d/bd;

.field private final d:D

.field private e:Z

.field private final f:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/af;",
            "Lcom/google/android/apps/gmm/map/internal/d/af;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/af;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/af;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/ad;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/google/android/apps/gmm/shared/c/f;

.field private final k:Lcom/google/android/apps/gmm/map/internal/d/ac;

.field private final l:Lcom/google/android/apps/gmm/map/internal/d/r;

.field private final m:Lcom/google/android/apps/gmm/map/internal/d/ah;

.field private n:Landroid/accounts/Account;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 61
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->a:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/internal/d/bd;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/d/r;)V
    .locals 6

    .prologue
    .line 158
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/d/ag;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/internal/d/bd;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/d/r;Z)V

    .line 160
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/internal/d/bd;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/d/r;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x12c

    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->k:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 166
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->c:Lcom/google/android/apps/gmm/map/internal/d/bd;

    .line 167
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 168
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->l:Lcom/google/android/apps/gmm/map/internal/d/r;

    .line 169
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->j:Lcom/google/android/apps/gmm/shared/c/f;

    .line 170
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/e;

    const-string v1, "perTileFetchCache"

    .line 171
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v2

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->f:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 172
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->g:Ljava/util/List;

    .line 173
    const/4 v0, 0x2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->h:Ljava/util/List;

    .line 174
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->i:Ljava/util/ArrayDeque;

    .line 175
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/ah;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/d/ah;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ag;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->m:Lcom/google/android/apps/gmm/map/internal/d/ah;

    .line 176
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->e:Z

    .line 177
    const-wide v0, 0x4092aaaaaaaaaaabL    # 1194.6666666666667

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->d:D

    .line 180
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/af;Lcom/google/android/apps/gmm/map/b/a/ai;Ljava/util/List;Z)Lcom/google/android/apps/gmm/map/internal/d/ad;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/d/af;",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;Z)",
            "Lcom/google/android/apps/gmm/map/internal/d/ad;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    const-wide/16 v2, 0x0

    .line 519
    const/4 v7, 0x0

    .line 520
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->l:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v0, :cond_0

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->l:Lcom/google/android/apps/gmm/map/internal/d/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/r;->d()Lcom/google/e/a/a/a/b;

    move-result-object v7

    .line 524
    :cond_0
    if-eqz p4, :cond_3

    .line 525
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/af;->a:Lcom/google/android/apps/gmm/map/b/a/bc;

    .line 526
    cmpl-double v0, v4, v2

    if-lez v0, :cond_1

    cmpl-double v0, v4, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Expand factors cannot be negative or zero"

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v6

    goto :goto_0

    :cond_2
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v0, v2, v0

    int-to-double v2, v0

    mul-double/2addr v2, v4

    div-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v0, v2

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    mul-double/2addr v2, v4

    div-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(II)Lcom/google/android/apps/gmm/map/b/a/bc;

    move-result-object v0

    .line 528
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/af;->b:I

    .line 527
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/af;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->j:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->k:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v4

    invoke-direct {v2, v0, v1, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/af;-><init>(Lcom/google/android/apps/gmm/map/b/a/bc;IJ)V

    .line 532
    :goto_1
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/ad;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->m:Lcom/google/android/apps/gmm/map/internal/d/ah;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/internal/d/ad;-><init>(Lcom/google/android/apps/gmm/map/internal/d/af;Lcom/google/android/apps/gmm/map/internal/d/af;Lcom/google/android/apps/gmm/map/b/a/ai;Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/d/ae;ZLcom/google/e/a/a/a/b;)V

    return-object v0

    :cond_3
    move-object v2, p1

    .line 530
    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/c/bt;J)Z
    .locals 10

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 383
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->j:Lcom/google/android/apps/gmm/shared/c/f;

    iget-wide v6, p1, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-ltz v5, :cond_2

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    iget-wide v6, p1, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    move v4, v1

    :goto_0
    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->k:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 385
    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v4

    .line 384
    iget-wide v6, p1, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    cmp-long v5, v6, v2

    if-nez v5, :cond_3

    :goto_1
    cmp-long v2, p2, v2

    if-lez v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v4, v0

    .line 383
    goto :goto_0

    .line 384
    :cond_3
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    iget-object v5, p1, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/net/a/b;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/af;)Z
    .locals 4

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->j:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->k:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v0

    .line 264
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/internal/d/af;->c:J

    sub-long/2addr v0, v2

    sget-wide v2, Lcom/google/android/apps/gmm/map/internal/d/ag;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/d/af;Ljava/util/List;ZZ)Z
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/d/af;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;ZZ)Z"
        }
    .end annotation

    .prologue
    .line 277
    monitor-enter p0

    const/4 v3, 0x0

    .line 278
    :try_start_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 281
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v5, v2

    :goto_0
    if-ltz v5, :cond_1f

    .line 282
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->g:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/af;

    .line 283
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->f:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->j:Lcom/google/android/apps/gmm/shared/c/f;

    .line 284
    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v8

    iget-wide v10, v2, Lcom/google/android/apps/gmm/map/internal/d/af;->c:J

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x0

    cmp-long v4, v8, v10

    if-ltz v4, :cond_2

    .line 285
    :cond_0
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    :cond_1
    add-int/lit8 v2, v5, -0x1

    move v5, v2

    goto :goto_0

    .line 290
    :cond_2
    if-eqz p3, :cond_3

    .line 291
    iget-wide v8, v2, Lcom/google/android/apps/gmm/map/internal/d/af;->c:J

    .line 292
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/google/android/apps/gmm/map/internal/d/af;->c:J

    cmp-long v4, v8, v10

    if-ltz v4, :cond_1

    .line 293
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->j:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v8

    iget-wide v10, v2, Lcom/google/android/apps/gmm/map/internal/d/af;->c:J

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x0

    cmp-long v4, v8, v10

    if-ltz v4, :cond_4

    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_1

    .line 298
    const/4 v2, 0x1

    .line 303
    :goto_2
    const/4 v3, 0x0

    :goto_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_14

    .line 304
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->g:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 305
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->f:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 293
    :cond_4
    iget v4, v2, Lcom/google/android/apps/gmm/map/internal/d/af;->b:I

    move-object/from16 v0, p1

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/d/af;->b:I

    if-eq v4, v7, :cond_5

    const/4 v2, 0x0

    goto :goto_1

    :cond_5
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/d/af;->a:Lcom/google/android/apps/gmm/map/b/a/bc;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/af;->a:Lcom/google/android/apps/gmm/map/b/a/bc;

    iget-boolean v8, v4, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    if-nez v8, :cond_8

    iget-boolean v8, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    if-nez v8, :cond_8

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v9, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v9, v9, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-gt v8, v9, :cond_7

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v9, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v9, v9, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-gt v8, v9, :cond_7

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v9, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v9, v9, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-lt v8, v9, :cond_7

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v8, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-lt v4, v8, :cond_7

    const/4 v4, 0x1

    :goto_4
    if-eqz v4, :cond_13

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/af;->a:Lcom/google/android/apps/gmm/map/b/a/bc;

    iget-object v8, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v4, v8}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v4

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/internal/d/af;->a:Lcom/google/android/apps/gmm/map/b/a/bc;

    iget-object v9, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v8

    if-eqz v4, :cond_13

    if-eqz v8, :cond_13

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/internal/d/af;->a:Lcom/google/android/apps/gmm/map/b/a/bc;

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v4, v8}, Lcom/google/android/apps/gmm/map/b/a/y;->g(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    iget v8, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-double v8, v8

    iget-object v10, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v11, v10, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v11, v11, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v10, v10, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v10, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v10, v11, v10

    int-to-double v10, v10

    div-double/2addr v8, v10

    const-wide v10, 0x3fb999999999999aL    # 0.1

    cmpg-double v8, v8, v10

    if-ltz v8, :cond_6

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v8, v4

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v10, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v10, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v10

    int-to-double v10, v4

    div-double/2addr v8, v10

    const-wide v10, 0x3fb999999999999aL    # 0.1

    cmpg-double v4, v8, v10

    if-gez v4, :cond_10

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_7
    const/4 v4, 0x0

    goto :goto_4

    :cond_8
    iget-boolean v8, v4, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    if-eqz v8, :cond_a

    iget-boolean v8, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    if-eqz v8, :cond_a

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v9, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v9, v9, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-gt v8, v9, :cond_9

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v8, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-lt v4, v8, :cond_9

    const/4 v4, 0x1

    goto :goto_4

    :cond_9
    const/4 v4, 0x0

    goto :goto_4

    :cond_a
    iget-boolean v8, v4, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    if-eqz v8, :cond_d

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v9, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v9, v9, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-le v8, v9, :cond_b

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v8, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-lt v4, v8, :cond_c

    :cond_b
    const/4 v4, 0x1

    goto/16 :goto_4

    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_d
    iget-object v8, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v9, v4, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v9, v9, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-le v8, v9, :cond_e

    iget-object v8, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-lt v8, v4, :cond_f

    :cond_e
    const/4 v4, 0x1

    goto/16 :goto_4

    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_10
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/af;->a:Lcom/google/android/apps/gmm/map/b/a/bc;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->g(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    iget v4, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-double v8, v4

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v10, v4, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v10, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v4, v10, v4

    int-to-double v10, v4

    div-double/2addr v8, v10

    const-wide v10, 0x3fb999999999999aL    # 0.1

    cmpg-double v4, v8, v10

    if-ltz v4, :cond_11

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v8, v2

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, v4

    int-to-double v10, v2

    div-double/2addr v8, v10

    const-wide v10, 0x3fb999999999999aL    # 0.1

    cmpg-double v2, v8, v10

    if-gez v2, :cond_12

    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_12
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 308
    :cond_14
    if-nez v2, :cond_15

    if-eqz p4, :cond_1e

    .line 309
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->l:Lcom/google/android/apps/gmm/map/internal/d/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_16

    const/4 v2, 0x0

    :goto_5
    if-eqz v2, :cond_1e

    :cond_15
    const/4 v2, 0x1

    :goto_6
    monitor-exit p0

    return v2

    :cond_16
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->l:Lcom/google/android/apps/gmm/map/internal/d/r;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/r;->f()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->l:Lcom/google/android/apps/gmm/map/internal/d/r;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->l:Lcom/google/android/apps/gmm/map/internal/d/r;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v5, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v4, v5, :cond_17

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/r;->d:Lcom/google/android/apps/gmm/map/internal/d/s;

    move-object v4, v3

    :goto_7
    const/4 v3, 0x0

    move v7, v3

    :goto_8
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v7, v3, :cond_1d

    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/internal/c/bp;

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v8, :cond_18

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v8, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v8

    if-eqz v8, :cond_18

    const/4 v6, 0x1

    invoke-interface {v8}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v10, v11}, Lcom/google/android/apps/gmm/map/internal/d/ag;->a(Lcom/google/android/apps/gmm/map/internal/c/bt;J)Z

    move-result v8

    if-eqz v8, :cond_18

    const/4 v2, 0x0

    goto :goto_5

    :cond_17
    const/4 v3, 0x0

    move-object v4, v3

    goto :goto_7

    :cond_18
    if-nez v6, :cond_1a

    if-eqz v12, :cond_1a

    invoke-interface {v12, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v5

    if-eqz v5, :cond_19

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v10, v11}, Lcom/google/android/apps/gmm/map/internal/d/ag;->a(Lcom/google/android/apps/gmm/map/internal/c/bt;J)Z

    move-result v6

    if-eqz v6, :cond_1a

    :cond_19
    const/4 v2, 0x0

    goto :goto_5

    :cond_1a
    if-eqz v5, :cond_1c

    if-eqz v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->k:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v6

    iget-wide v8, v5, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    const-wide/16 v14, -0x1

    cmp-long v8, v8, v14

    if-nez v8, :cond_1b

    const-wide/16 v8, -0x1

    :goto_9
    invoke-virtual {v4, v3, v8, v9}, Lcom/google/android/apps/gmm/map/internal/d/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;J)Z

    move-result v3

    if-eqz v3, :cond_1c

    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_1b
    iget-wide v8, v5, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/net/a/b;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v14

    sub-long/2addr v8, v14

    goto :goto_9

    :cond_1c
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    goto :goto_8

    :cond_1d
    const/4 v2, 0x1

    goto/16 :goto_5

    :cond_1e
    const/4 v2, 0x0

    goto/16 :goto_6

    .line 277
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_1f
    move v2, v3

    goto/16 :goto_2
.end method

.method private declared-synchronized b(Lcom/google/android/apps/gmm/map/internal/d/ad;)V
    .locals 2

    .prologue
    .line 233
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/ad;->b:Lcom/google/android/apps/gmm/map/internal/d/af;

    .line 234
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->f:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v1, v0, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 236
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->k:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    monitor-exit p0

    return-void

    .line 233
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c(Lcom/google/android/apps/gmm/map/internal/d/ad;)V
    .locals 4

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->size()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/ad;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/ad;->b:Lcom/google/android/apps/gmm/map/internal/d/af;

    invoke-direct {p0, v3}, Lcom/google/android/apps/gmm/map/internal/d/ag;->a(Lcom/google/android/apps/gmm/map/internal/d/af;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->i:Ljava/util/ArrayDeque;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayDeque;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    .line 245
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 246
    return-void
.end method

.method private declared-synchronized d(Lcom/google/android/apps/gmm/map/internal/d/ad;)Z
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const-wide/16 v6, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 320
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/ad;->b:Lcom/google/android/apps/gmm/map/internal/d/af;

    .line 321
    iget-object v8, v0, Lcom/google/android/apps/gmm/map/internal/d/af;->a:Lcom/google/android/apps/gmm/map/b/a/bc;

    move v3, v1

    .line 322
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->h:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/af;

    .line 324
    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/af;->a:Lcom/google/android/apps/gmm/map/b/a/bc;

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v4, v8, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v9

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v4, v8, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v10

    if-ge v10, v9, :cond_0

    move-wide v4, v6

    .line 326
    :goto_1
    iget-object v0, v8, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v9, v8, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v9, v9, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v0, v9

    int-to-double v10, v0

    div-double/2addr v4, v10

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v9, v9, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-int v0, v9, v0

    int-to-double v10, v0

    div-double/2addr v4, v10

    .line 327
    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    cmpl-double v0, v4, v10

    if-lez v0, :cond_4

    move v0, v1

    .line 331
    :goto_2
    monitor-exit p0

    return v0

    .line 324
    :cond_0
    :try_start_1
    iget-boolean v0, v5, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    iget-boolean v4, v8, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    if-ne v0, v4, :cond_1

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v4, v8, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v11, v8, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v11, v11, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v0, v11}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-boolean v5, v5, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    if-nez v5, :cond_2

    iget-boolean v5, v8, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    if-nez v5, :cond_2

    if-le v4, v0, :cond_2

    move-wide v4, v6

    goto :goto_1

    :cond_1
    iget-boolean v0, v5, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    if-eqz v0, :cond_3

    invoke-static {v5, v8}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(Lcom/google/android/apps/gmm/map/b/a/bc;Lcom/google/android/apps/gmm/map/b/a/bc;)[I

    move-result-object v0

    const/4 v4, 0x0

    aget v4, v0, v4

    const/4 v5, 0x1

    aget v0, v0, v5

    :cond_2
    :goto_3
    sub-int v5, v10, v9

    int-to-double v10, v5

    sub-int/2addr v0, v4

    add-int/2addr v0, v12

    const/high16 v4, 0x40000000    # 2.0f

    rem-int/2addr v0, v4

    int-to-double v4, v0

    mul-double/2addr v4, v10

    goto :goto_1

    :cond_3
    invoke-static {v8, v5}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(Lcom/google/android/apps/gmm/map/b/a/bc;Lcom/google/android/apps/gmm/map/b/a/bc;)[I

    move-result-object v0

    const/4 v4, 0x0

    aget v4, v0, v4

    const/4 v5, 0x1

    aget v0, v0, v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 322
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 331
    goto :goto_2

    .line 320
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method declared-synchronized a()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 487
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    rsub-int/lit8 v0, v0, 0x2

    .line 488
    const/4 v3, 0x2

    if-lt v0, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/ad;

    .line 492
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/ad;->a:Lcom/google/android/apps/gmm/map/internal/d/af;

    invoke-direct {p0, v3}, Lcom/google/android/apps/gmm/map/internal/d/ag;->a(Lcom/google/android/apps/gmm/map/internal/d/af;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/ad;->d:Ljava/util/List;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/apps/gmm/map/internal/d/ag;->a(Lcom/google/android/apps/gmm/map/internal/d/af;Ljava/util/List;ZZ)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    if-eqz v1, :cond_0

    .line 494
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/ad;->a:Lcom/google/android/apps/gmm/map/internal/d/af;

    .line 495
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/ad;->e:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 496
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/ad;->d:Ljava/util/List;

    const/4 v4, 0x1

    .line 493
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/ag;->a(Lcom/google/android/apps/gmm/map/internal/d/af;Lcom/google/android/apps/gmm/map/b/a/ai;Ljava/util/List;Z)Lcom/google/android/apps/gmm/map/internal/d/ad;

    move-result-object v1

    .line 498
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/ag;->d(Lcom/google/android/apps/gmm/map/internal/d/ad;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 499
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/ag;->b(Lcom/google/android/apps/gmm/map/internal/d/ad;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 505
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    move v1, v2

    .line 492
    goto :goto_0

    .line 501
    :cond_2
    :try_start_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/ag;->c(Lcom/google/android/apps/gmm/map/internal/d/ad;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 487
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/accounts/Account;)V
    .locals 1
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 190
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/ag;->b()V

    .line 191
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->n:Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    monitor-exit p0

    return-void

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/b/a/bc;Ljava/util/List;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "Lcom/google/android/apps/gmm/map/b/a/bc;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 207
    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->n:Landroid/accounts/Account;

    if-eqz v4, :cond_0

    .line 209
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    .line 230
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 212
    :cond_1
    const/4 v4, 0x0

    :try_start_1
    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v5, v4, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    .line 214
    invoke-static {v5}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b(I)I

    move-result v10

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v4, v6, v4

    int-to-double v8, v4

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v6

    int-to-double v6, v4

    const-wide/high16 v12, 0x4070000000000000L    # 256.0

    mul-double/2addr v12, v8

    int-to-double v14, v10

    div-double/2addr v12, v14

    const-wide/high16 v14, 0x4070000000000000L    # 256.0

    mul-double/2addr v14, v6

    int-to-double v0, v10

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    const/4 v4, 0x0

    const-wide v16, 0x4092aaaaaaaaaaabL    # 1194.6666666666667

    cmpl-double v11, v12, v16

    if-lez v11, :cond_2

    const-wide v8, 0x4092aaaaaaaaaaabL    # 1194.6666666666667

    int-to-double v12, v10

    mul-double/2addr v8, v12

    const-wide/high16 v12, 0x4070000000000000L    # 256.0

    div-double/2addr v8, v12

    const/4 v4, 0x1

    :cond_2
    const-wide v12, 0x4092aaaaaaaaaaabL    # 1194.6666666666667

    cmpl-double v11, v14, v12

    if-lez v11, :cond_3

    const-wide v6, 0x4092aaaaaaaaaaabL    # 1194.6666666666667

    int-to-double v10, v10

    mul-double/2addr v6, v10

    const-wide/high16 v10, 0x4070000000000000L    # 256.0

    div-double/2addr v6, v10

    const/4 v4, 0x1

    :cond_3
    if-eqz v4, :cond_5

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v8, v10

    double-to-int v4, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v8

    double-to-int v6, v6

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v6}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(II)Lcom/google/android/apps/gmm/map/b/a/bc;

    move-result-object v4

    .line 215
    :goto_1
    new-instance v6, Lcom/google/android/apps/gmm/map/internal/d/af;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->j:Lcom/google/android/apps/gmm/shared/c/f;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->k:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v8

    invoke-direct {v6, v4, v5, v8, v9}, Lcom/google/android/apps/gmm/map/internal/d/af;-><init>(Lcom/google/android/apps/gmm/map/b/a/bc;IJ)V

    .line 218
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->e:Z

    if-nez v4, :cond_4

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v6, v1, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/ag;->a(Lcom/google/android/apps/gmm/map/internal/d/af;Ljava/util/List;ZZ)Z

    move-result v4

    if-nez v4, :cond_0

    .line 220
    :cond_4
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->e:Z

    .line 221
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v6, v1, v2, v4}, Lcom/google/android/apps/gmm/map/internal/d/ag;->a(Lcom/google/android/apps/gmm/map/internal/d/af;Lcom/google/android/apps/gmm/map/b/a/ai;Ljava/util/List;Z)Lcom/google/android/apps/gmm/map/internal/d/ad;

    move-result-object v5

    .line 223
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/ag;->h:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v6, 0x2

    if-ge v4, v6, :cond_6

    const/4 v4, 0x1

    .line 224
    :goto_2
    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/gmm/map/internal/d/ag;->d(Lcom/google/android/apps/gmm/map/internal/d/ad;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 225
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/gmm/map/internal/d/ag;->b(Lcom/google/android/apps/gmm/map/internal/d/ad;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 207
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 214
    :cond_5
    :try_start_2
    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-direct {v4, v6}, Lcom/google/android/apps/gmm/map/b/a/bc;-><init>(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    goto :goto_1

    .line 223
    :cond_6
    const/4 v4, 0x0

    goto :goto_2

    .line 227
    :cond_7
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/gmm/map/internal/d/ag;->c(Lcom/google/android/apps/gmm/map/internal/d/ad;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/d/ad;)V
    .locals 12

    .prologue
    .line 401
    monitor-enter p0

    :try_start_0
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/d/ad;->b:Lcom/google/android/apps/gmm/map/internal/d/af;

    .line 402
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->g:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 426
    :cond_0
    monitor-exit p0

    return-void

    .line 408
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->h:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 409
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->g:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 410
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->f:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    iget-object v6, p1, Lcom/google/android/apps/gmm/map/internal/d/ad;->c:Ljava/util/List;

    .line 414
    if-eqz v6, :cond_0

    .line 415
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_0

    .line 416
    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/maps/b/cc;

    move-object v3, v0

    .line 417
    iget-object v2, v3, Lcom/google/maps/b/cc;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/h/e;->d()Lcom/google/maps/h/e;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/h/e;

    iget-object v5, v3, Lcom/google/maps/b/cc;->e:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_3

    iget v7, v3, Lcom/google/maps/b/cc;->d:I

    div-int/2addr v5, v7

    new-instance v8, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v9, v2, Lcom/google/maps/h/e;->b:I

    iget v10, v2, Lcom/google/maps/h/e;->c:I

    iget v11, v2, Lcom/google/maps/h/e;->d:I

    add-int/2addr v5, v11

    add-int/lit8 v5, v5, -0x1

    invoke-direct {v8, v9, v10, v5}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    new-instance v5, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v9, v2, Lcom/google/maps/h/e;->b:I

    iget v10, v2, Lcom/google/maps/h/e;->c:I

    add-int/2addr v7, v10

    add-int/lit8 v7, v7, -0x1

    iget v2, v2, Lcom/google/maps/h/e;->d:I

    invoke-direct {v5, v9, v7, v2}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v7, v8, Lcom/google/android/apps/gmm/map/internal/c/bp;->e:I

    iget v8, v8, Lcom/google/android/apps/gmm/map/internal/c/bp;->f:I

    invoke-direct {v2, v7, v8}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    const/high16 v7, 0x40000000    # 2.0f

    iget v8, v5, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    shr-int/2addr v7, v8

    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v9, v5, Lcom/google/android/apps/gmm/map/internal/c/bp;->e:I

    add-int/2addr v9, v7

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bp;->f:I

    add-int/2addr v5, v7

    invoke-direct {v8, v9, v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-direct {v5, v2, v8}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-direct {v2, v5}, Lcom/google/android/apps/gmm/map/b/a/bc;-><init>(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    move-object v5, v2

    .line 418
    :goto_1
    if-eqz v5, :cond_2

    .line 420
    iget-object v2, v3, Lcom/google/maps/b/cc;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/h/e;->d()Lcom/google/maps/h/e;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/h/e;

    iget v2, v2, Lcom/google/maps/h/e;->b:I

    .line 419
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/d/af;

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->j:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v9, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->k:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v8

    invoke-direct {v3, v5, v2, v8, v9}, Lcom/google/android/apps/gmm/map/internal/d/af;-><init>(Lcom/google/android/apps/gmm/map/b/a/bc;IJ)V

    .line 421
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 422
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->f:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v2, v3, v3}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415
    :cond_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_0

    .line 417
    :cond_3
    const/4 v2, 0x0

    move-object v5, v2

    goto :goto_1

    .line 401
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/d/ad;Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 2

    .prologue
    .line 470
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/ad;->b:Lcom/google/android/apps/gmm/map/internal/d/af;

    .line 473
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 474
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 475
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->f:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 477
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->d:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->f:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->g:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq p2, v0, :cond_0

    .line 480
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/ag;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 482
    :cond_0
    monitor-exit p0

    return-void

    .line 470
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 578
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->f:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/e;->d()V

    .line 579
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 580
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ag;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 582
    monitor-exit p0

    return-void

    .line 578
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
