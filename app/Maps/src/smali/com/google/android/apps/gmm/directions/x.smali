.class Lcom/google/android/apps/gmm/directions/x;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/apps/gmm/directions/x;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/x;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/map/r/a/e;ILcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;ZLcom/google/maps/g/a/hm;Z)Lcom/google/android/apps/gmm/directions/v;
    .locals 9
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 33
    .line 34
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v0, v0

    if-gt v0, p1, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/f/d/h;->b(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/hm;

    move-result-object v0

    move-object v1, p2

    move-object v2, p0

    move v3, p1

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move/from16 v7, p7

    .line 36
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/directions/x;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/e;ILcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;ZZ)Lcom/google/android/apps/gmm/directions/v;

    move-result-object v1

    .line 39
    if-nez v1, :cond_4

    move-object v1, p6

    move-object v2, p2

    move-object v3, p0

    move v4, p1

    move-object v5, p3

    move-object v6, p4

    move v7, p5

    move/from16 v8, p7

    .line 40
    invoke-static/range {v1 .. v8}, Lcom/google/android/apps/gmm/directions/x;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/e;ILcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;ZZ)Lcom/google/android/apps/gmm/directions/v;

    move-result-object v1

    .line 43
    if-nez v1, :cond_1

    .line 44
    sget-object v2, Lcom/google/android/apps/gmm/directions/x;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x28

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unsupported travel mode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " and fallback: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    move-object v0, v1

    .line 50
    :goto_1
    return-object v0

    .line 34
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, p1

    if-nez v0, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-direct {v2, v0, p0}, Lcom/google/android/apps/gmm/map/r/a/ao;-><init>(Lcom/google/maps/g/a/hu;Lcom/google/android/apps/gmm/map/r/a/e;)V

    aput-object v2, v1, p1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, p1

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 50
    goto :goto_1
.end method

.method private static a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/e;ILcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;ZZ)Lcom/google/android/apps/gmm/directions/v;
    .locals 7
    .param p0    # Lcom/google/maps/g/a/hm;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 64
    if-nez p0, :cond_0

    .line 85
    :goto_0
    return-object v0

    .line 67
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/directions/y;->a:[I

    invoke-virtual {p0}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 69
    :pswitch_0
    new-instance v0, Lcom/google/android/apps/gmm/directions/ba;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/ba;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/e;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;ZZ)V

    goto :goto_0

    .line 73
    :pswitch_1
    new-instance v0, Lcom/google/android/apps/gmm/directions/dm;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/dm;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/e;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;ZZ)V

    goto :goto_0

    .line 77
    :pswitch_2
    new-instance v0, Lcom/google/android/apps/gmm/directions/a;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/a;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/e;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;ZZ)V

    goto :goto_0

    .line 81
    :pswitch_3
    new-instance v0, Lcom/google/android/apps/gmm/directions/dc;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/dc;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/e;ILcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Z)V

    goto :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/google/maps/g/a/hm;)Z
    .locals 3
    .param p0    # Lcom/google/maps/g/a/hm;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 94
    if-nez p0, :cond_0

    .line 104
    :goto_0
    return v0

    .line 97
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/directions/y;->a:[I

    invoke-virtual {p0}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 102
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 97
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
