.class public Lcom/google/android/apps/gmm/startpage/ap;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/startpage/a/e;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/google/android/apps/gmm/startpage/w;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/apps/gmm/startpage/ap;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/startpage/ap;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 50
    return-void
.end method


# virtual methods
.method public final Y_()V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/w;->d()V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/i;->d()Lcom/google/android/apps/gmm/mylocation/b/f;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/i;->d()Lcom/google/android/apps/gmm/mylocation/b/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/f;->b()Lcom/google/android/apps/gmm/map/s/a;

    .line 99
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 3

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    if-nez v0, :cond_0

    .line 81
    new-instance v1, Lcom/google/android/apps/gmm/startpage/w;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 82
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-direct {v1, p1, v0, v2}, Lcom/google/android/apps/gmm/startpage/w;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/w;->b()V

    .line 88
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 89
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/base/e/c;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 168
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/w;->a(Ljava/lang/String;)Z

    .line 169
    return-void

    .line 168
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/s/b;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 116
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/s/b;->a:Lcom/google/android/apps/gmm/map/s/a;

    .line 117
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/startpage/a/a;)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/startpage/ag;->a(Lcom/google/android/apps/gmm/startpage/a/a;)V

    .line 189
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/startpage/a/d;Z)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    check-cast p1, Lcom/google/android/apps/gmm/startpage/ac;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/startpage/w;->a(Lcom/google/android/apps/gmm/startpage/ac;Z)V

    .line 125
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/ag;->e:Lcom/google/android/apps/gmm/startpage/y;

    iput-boolean p1, v0, Lcom/google/android/apps/gmm/startpage/y;->d:Z

    .line 75
    return-void
.end method

.method public final a(Lcom/google/o/h/a/dq;)Z
    .locals 3

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/startpage/x;->a:[I

    invoke-virtual {p1}, Lcom/google/o/h/a/dq;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/gmm/startpage/w;->a:Ljava/lang/String;

    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/ag;->d:Lcom/google/android/apps/gmm/startpage/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/j;->a()Lcom/google/o/h/a/gt;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/o/h/a/gt;->b:Z

    goto :goto_0

    :pswitch_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/ag;->d:Lcom/google/android/apps/gmm/startpage/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/j;->a()Lcom/google/o/h/a/gt;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/o/h/a/gt;->c:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/w;->e()V

    .line 104
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->b()V

    .line 105
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/startpage/a/a;)V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/startpage/ag;->b(Lcom/google/android/apps/gmm/startpage/a/a;)V

    .line 194
    return-void
.end method

.method public final b(Z)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/w;->b:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v4, v0, Lcom/google/android/apps/gmm/startpage/ag;->k:Lcom/google/android/apps/gmm/startpage/d/a;

    .line 130
    if-eqz v4, :cond_0

    iget-object v0, v4, Lcom/google/android/apps/gmm/startpage/d/a;->c:Lcom/google/o/h/a/gx;

    if-nez v0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;

    if-nez v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;

    if-nez v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    .line 141
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/w;->b(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/iamhere/a/b;->b(Z)Lcom/google/android/apps/gmm/iamhere/c/o;

    move-result-object v5

    .line 147
    iget-object v0, v4, Lcom/google/android/apps/gmm/startpage/d/a;->c:Lcom/google/o/h/a/gx;

    .line 148
    if-eqz p1, :cond_2

    iget-object v0, v0, Lcom/google/o/h/a/gx;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kh;

    .line 151
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v2, Lcom/google/android/apps/gmm/cardui/b/b;->c:Lcom/google/android/apps/gmm/cardui/b/b;

    new-instance v6, Lcom/google/android/apps/gmm/startpage/a/b;

    .line 157
    iget-object v7, v4, Lcom/google/android/apps/gmm/startpage/d/a;->a:Lcom/google/o/h/a/hb;

    iget-object v8, v4, Lcom/google/android/apps/gmm/startpage/d/a;->b:Ljava/lang/String;

    iget-object v4, v4, Lcom/google/android/apps/gmm/startpage/d/a;->d:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-direct {v6, v7, v8, v4}, Lcom/google/android/apps/gmm/startpage/a/b;-><init>(Lcom/google/o/h/a/hb;Ljava/lang/String;Lcom/google/android/apps/gmm/startpage/d/e;)V

    move-object v4, v3

    .line 149
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/cardui/a/o;->a(Lcom/google/o/h/a/kh;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/o/h/a/od;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/android/apps/gmm/startpage/a/b;)V

    goto :goto_0

    .line 148
    :cond_2
    iget-object v0, v0, Lcom/google/o/h/a/gx;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kh;

    goto :goto_1
.end method

.method public final c()Lcom/google/android/apps/gmm/startpage/a/c;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/w;->c:Lcom/google/android/apps/gmm/startpage/e;

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/w;->a()V

    .line 70
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ap;->b:Lcom/google/android/apps/gmm/startpage/w;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/w;->c()V

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 111
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->h()V

    .line 112
    return-void
.end method
