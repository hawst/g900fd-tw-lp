.class public Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;
.super Lcom/google/android/apps/gmm/place/PlacePageSubPageFragment;
.source "PG"


# instance fields
.field private c:Lcom/google/android/apps/gmm/place/bx;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/PlacePageSubPageFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)",
            "Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;-><init>()V

    .line 73
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "placemark"

    invoke-virtual {p0, v1, v2, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 74
    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/gmm/base/g/c;)Landroid/view/View;
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;->getView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 134
    sget v0, Lcom/google/android/apps/gmm/l;->aV:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final b()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2

    .prologue
    .line 123
    .line 124
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->aV:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 123
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    return-object v0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/google/b/f/t;->dY:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/place/PlacePageSubPageFragment;->onCreate(Landroid/os/Bundle;)V

    .line 80
    new-instance v0, Lcom/google/android/apps/gmm/place/bx;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/bx;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;->c:Lcom/google/android/apps/gmm/place/bx;

    .line 81
    new-instance v1, Lcom/google/android/apps/gmm/place/ac;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v3, Lcom/google/android/apps/gmm/base/placelists/o;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/base/placelists/o;-><init>(Lcom/google/android/apps/gmm/z/a/b;)V

    invoke-direct {v1, v2, p0}, Lcom/google/android/apps/gmm/place/ac;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Landroid/app/Fragment;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;->c:Lcom/google/android/apps/gmm/place/bx;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;->e:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/apps/gmm/place/bx;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V

    .line 86
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/place/c/y;

    .line 106
    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 108
    iget-object v1, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;->c:Lcom/google/android/apps/gmm/place/bx;

    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 110
    iget-object v1, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 111
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;->c:Lcom/google/android/apps/gmm/place/bx;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iput-object v0, v2, Lcom/google/android/apps/gmm/place/bx;->a:Lcom/google/android/libraries/curvular/ag;

    .line 113
    return-object v1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0}, Lcom/google/android/apps/gmm/place/PlacePageSubPageFragment;->onPause()V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;->c:Lcom/google/android/apps/gmm/place/bx;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 99
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0}, Lcom/google/android/apps/gmm/place/PlacePageSubPageFragment;->onResume()V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;->c:Lcom/google/android/apps/gmm/place/bx;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 93
    return-void
.end method
