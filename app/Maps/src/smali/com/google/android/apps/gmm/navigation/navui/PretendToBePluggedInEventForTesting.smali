.class public Lcom/google/android/apps/gmm/navigation/navui/PretendToBePluggedInEventForTesting;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/h/b;
.end annotation

.annotation runtime Lcom/google/android/apps/gmm/util/replay/c;
    a = "pretend-to-be-plugged-in"
.end annotation


# instance fields
.field private final isPluggedIn:Z


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "is-plugged-in"
        .end annotation
    .end param

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/PretendToBePluggedInEventForTesting;->isPluggedIn:Z

    .line 40
    return-void

    .line 39
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public hasPluggedIn()Z
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/f;
        a = "is-plugged-in"
    .end annotation

    .prologue
    .line 49
    const/4 v0, 0x1

    return v0
.end method

.method public isPluggedIn()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "is-plugged-in"
    .end annotation

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/PretendToBePluggedInEventForTesting;->isPluggedIn:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
