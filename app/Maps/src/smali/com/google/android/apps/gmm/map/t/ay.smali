.class public Lcom/google/android/apps/gmm/map/t/ay;
.super Lcom/google/android/apps/gmm/map/t/aw;
.source "PG"


# instance fields
.field public h:I

.field public i:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 145
    const-string v0, "precision mediump float;\nvarying vec2 vTextureCoord;\nuniform sampler2D sTexture0;\nuniform sampler2D sTexture1;\nuniform sampler2D sTexture2;\nuniform lowp vec3 uFillColor;\nuniform lowp vec3 uBlurColor;\nuniform lowp float uBlurScale;\nuniform mediump float uScaledAARange;\nuniform float brightnessScale;\nvoid main() {\n  lowp float coverage = texture2D(sTexture0, vTextureCoord).a;\n  lowp float alpha = smoothstep(-uScaledAARange, uScaledAARange, coverage - 0.5);\n  lowp float blur1 = texture2D(sTexture1, vTextureCoord).a;\n  lowp float blur2 = texture2D(sTexture2, vTextureCoord).a;\n  lowp float blur = mix(blur1, blur2, uBlurScale);\n  lowp vec3 c = mix(uBlurColor, uFillColor, blur);\n  gl_FragColor = vec4(c * brightnessScale, alpha);\n}\n"

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/t/aw;-><init>(Ljava/lang/String;)V

    .line 146
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 2

    .prologue
    .line 150
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/map/t/aw;->a(I)V

    .line 151
    const-string v0, "uScaledAARange"

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/t/ay;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/ay;->h:I

    .line 152
    const-string v0, "sTexture2"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/ay;->i:I

    .line 153
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/ay;->i:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 154
    return-void
.end method
