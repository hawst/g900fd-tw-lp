.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;
.super Lcom/google/android/apps/gmm/v/bp;
.source "PG"


# instance fields
.field public a:F

.field public b:[F

.field public c:F

.field public d:F

.field private final e:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Lcom/google/android/apps/gmm/v/ao;)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 79
    iget v0, p2, Lcom/google/android/apps/gmm/v/ao;->e:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    const-class v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/bp;-><init>(Ljava/lang/Class;)V

    .line 48
    const v0, 0x3e4ccccd    # 0.2f

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->a:F

    .line 64
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->b:[F

    .line 66
    iput v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->c:F

    .line 76
    iput v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->d:F

    .line 81
    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 79
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-class v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/b;

    goto :goto_1

    .line 83
    :cond_2
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->d:Z

    if-nez v0, :cond_3

    .line 84
    iget v0, p2, Lcom/google/android/apps/gmm/v/ao;->e:I

    if-eqz v0, :cond_3

    .line 86
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->a()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->e:F

    .line 87
    return-void
.end method

.method public static a(IF)F
    .locals 2

    .prologue
    .line 123
    const/4 v0, 0x1

    float-to-int v1, p1

    sub-int/2addr v1, p0

    shl-int/2addr v0, v1

    int-to-float v0, v0

    return v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V
    .locals 4

    .prologue
    .line 142
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/v/bp;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->c:I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->b:[F

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glUniform3fv(II[FI)V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->d:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->d:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->e:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->c:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->f:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->e:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->g:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/a;->a:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 168
    return-void
.end method
