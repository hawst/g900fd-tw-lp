.class public Lcom/google/android/apps/gmm/place/c/e;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/place/ae;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, -0x1

    const/4 v7, -0x2

    const/4 v1, 0x0

    .line 39
    const/4 v0, 0x7

    new-array v2, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 40
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v9

    .line 41
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v10

    const/4 v0, 0x2

    .line 43
    invoke-static {}, Lcom/google/android/apps/gmm/place/c/c;->a()Lcom/google/android/libraries/curvular/cs;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/apps/gmm/base/f/aq;

    .line 44
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->C()Lcom/google/android/apps/gmm/base/l/a/o;

    move-result-object v0

    new-instance v5, Lcom/google/android/libraries/curvular/ao;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-direct {v5, v4, v0}, Lcom/google/android/libraries/curvular/ao;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ah;)V

    aput-object v5, v2, v3

    const/4 v3, 0x4

    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/c/e;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v4

    const/4 v0, 0x5

    new-array v5, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->H()Lcom/google/android/apps/gmm/place/riddler/d/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/riddler/d/a;->j()Lcom/google/android/libraries/curvular/c/h;

    move-result-object v0

    .line 48
    sget-object v6, Lcom/google/android/libraries/curvular/c/a;->f:Lcom/google/android/libraries/curvular/c/a;

    invoke-static {v6, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v5, v9

    sget-object v0, Lcom/google/android/apps/gmm/place/t;->a:Lcom/google/android/libraries/curvular/bk;

    .line 51
    sget-object v6, Lcom/google/android/libraries/curvular/g;->Z:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v5, v10

    const/4 v0, 0x2

    .line 53
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x3

    .line 54
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x4

    sget v6, Lcom/google/android/apps/gmm/d;->aR:I

    .line 56
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    .line 46
    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/base/k/ao;->a(Lcom/google/android/libraries/curvular/am;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v3, 0x5

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/c/e;->p()Z

    move-result v0

    if-nez v0, :cond_1

    const-class v4, Lcom/google/android/apps/gmm/place/riddler/b/a;

    .line 60
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->H()Lcom/google/android/apps/gmm/place/riddler/d/a;

    move-result-object v5

    .line 59
    new-instance v0, Lcom/google/android/libraries/curvular/ao;

    invoke-static {v1, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lcom/google/android/libraries/curvular/ao;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ah;)V

    :goto_0
    aput-object v0, v2, v3

    const/4 v3, 0x6

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/c/e;->p()Z

    move-result v0

    if-nez v0, :cond_0

    const-class v4, Lcom/google/android/apps/gmm/place/riddler/b/c;

    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->H()Lcom/google/android/apps/gmm/place/riddler/d/a;

    move-result-object v5

    .line 62
    new-instance v0, Lcom/google/android/libraries/curvular/ao;

    invoke-static {v1, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-direct {v0, v4, v1}, Lcom/google/android/libraries/curvular/ao;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ah;)V

    move-object v1, v0

    :cond_0
    aput-object v1, v2, v3

    .line 39
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.FrameLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, v1

    .line 59
    goto :goto_0
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 31
    check-cast p2, Lcom/google/android/apps/gmm/place/ae;

    new-instance v0, Lcom/google/android/apps/gmm/place/c/a;

    invoke-direct {v0, p2, p4, p3}, Lcom/google/android/apps/gmm/place/c/a;-><init>(Lcom/google/android/apps/gmm/place/ae;Lcom/google/android/libraries/curvular/bc;Landroid/content/Context;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->t()Lcom/google/android/apps/gmm/place/i/b;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v4, Lcom/google/android/apps/gmm/place/c/f;

    iget-object v5, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/place/ae;->t()Lcom/google/android/apps/gmm/place/i/b;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_0
    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/ae;->G()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1c

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->f()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v1

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->e()Lcom/google/android/apps/gmm/place/a/b/a;

    move-result-object v0

    if-eqz v0, :cond_10

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->e()Lcom/google/android/apps/gmm/place/a/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/a/b/a;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_10

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v4, Lcom/google/android/apps/gmm/place/a/a/d;

    iget-object v5, v1, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/place/ae;->e()Lcom/google/android/apps/gmm/place/a/b/a;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/c/a;->b()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->o()Lcom/google/android/apps/gmm/place/d/j;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->o()Lcom/google/android/apps/gmm/place/d/j;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/d/j;->a()Ljava/util/List;

    move-result-object v1

    const-class v4, Lcom/google/android/apps/gmm/place/d/a/a;

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/place/c/a;->a(Ljava/util/List;Ljava/lang/Class;Z)Z

    move-result v1

    iget-object v4, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/ae;->o()Lcom/google/android/apps/gmm/place/d/j;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/d/j;->b()Ljava/util/List;

    move-result-object v4

    const-class v5, Lcom/google/android/apps/gmm/place/d/a/a;

    invoke-virtual {v0, v4, v5, v1}, Lcom/google/android/apps/gmm/place/c/a;->a(Ljava/util/List;Ljava/lang/Class;Z)Z

    move-result v1

    iget-object v4, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/ae;->o()Lcom/google/android/apps/gmm/place/d/j;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/d/j;->c()Ljava/util/List;

    move-result-object v4

    const-class v5, Lcom/google/android/apps/gmm/place/d/a/c;

    invoke-virtual {v0, v4, v5, v1}, Lcom/google/android/apps/gmm/place/c/a;->a(Ljava/util/List;Ljava/lang/Class;Z)Z

    move-result v1

    iget-object v4, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/ae;->o()Lcom/google/android/apps/gmm/place/d/j;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/d/j;->d()Ljava/util/List;

    move-result-object v4

    const-class v5, Lcom/google/android/apps/gmm/place/d/a/b;

    invoke-virtual {v0, v4, v5, v1}, Lcom/google/android/apps/gmm/place/c/a;->a(Ljava/util/List;Ljava/lang/Class;Z)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v4, Lcom/google/android/apps/gmm/place/d/a/g;

    iget-object v5, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v4, Lcom/google/android/apps/gmm/place/c/q;

    new-instance v5, Lcom/google/android/apps/gmm/place/facepile/j;

    iget-object v6, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/place/ae;->j()Lcom/google/android/apps/gmm/place/facepile/a/c;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/gmm/place/facepile/j;-><init>(Lcom/google/android/apps/gmm/place/facepile/a/c;)V

    invoke-virtual {v1, v4, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_3
    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/place/c/a;->a(Z)Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->d()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v4

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->k()Lcom/google/android/apps/gmm/place/j/g;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->k()Lcom/google/android/apps/gmm/place/j/g;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/j/g;->a:Lcom/google/android/apps/gmm/x/o;

    if-eqz v1, :cond_12

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/g;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Y()Lcom/google/android/apps/gmm/hotels/a/c;

    move-result-object v0

    if-eqz v0, :cond_11

    move v0, v2

    :goto_1
    if-eqz v0, :cond_12

    move v0, v2

    :goto_2
    if-eqz v0, :cond_4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v1, Lcom/google/android/apps/gmm/base/f/ay;

    iget-object v5, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/place/ae;->k()Lcom/google/android/apps/gmm/place/j/g;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    :cond_4
    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->l()Lcom/google/android/apps/gmm/place/reservation/b/c;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->l()Lcom/google/android/apps/gmm/place/reservation/b/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/reservation/b/c;->d()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v1, Lcom/google/android/apps/gmm/place/reservation/a/d;

    iget-object v5, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/place/ae;->l()Lcom/google/android/apps/gmm/place/reservation/b/c;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    :cond_5
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/place/c/a;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/place/c/a;->g()Lcom/google/android/apps/gmm/place/c/a;

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v1, Lcom/google/android/apps/gmm/base/f/av;

    iget-object v5, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/place/ae;->p()Lcom/google/android/apps/gmm/base/l/v;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_6
    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->m()Lcom/google/android/apps/gmm/place/i/g;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->m()Lcom/google/android/apps/gmm/place/i/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/i/g;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v1, Lcom/google/android/apps/gmm/place/c/w;

    iget-object v5, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/place/ae;->m()Lcom/google/android/apps/gmm/place/i/g;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_7
    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->x()Lcom/google/android/apps/gmm/base/l/a/y;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->x()Lcom/google/android/apps/gmm/base/l/a/y;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/j/aa;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/j/aa;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v5

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/aa;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v1

    if-nez v1, :cond_8

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v1

    if-eqz v1, :cond_13

    :cond_8
    move v1, v2

    :goto_3
    if-nez v1, :cond_15

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/shared/net/a/h;->d:Z

    if-eqz v1, :cond_15

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget v0, v0, Lcom/google/r/b/a/ads;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_14

    move v0, v2

    :goto_4
    if-eqz v0, :cond_15

    move v0, v2

    :goto_5
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/place/c/a;->c()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    :cond_9
    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v1, Lcom/google/android/apps/gmm/base/f/by;

    iget-object v2, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/ae;->x()Lcom/google/android/apps/gmm/base/l/a/y;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_a
    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->y()Lcom/google/android/apps/gmm/place/g/a/e;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->q()Lcom/google/android/apps/gmm/place/g/a/b;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->y()Lcom/google/android/apps/gmm/place/g/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/e;->h()Lcom/google/android/apps/gmm/place/g/a/c;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->y()Lcom/google/android/apps/gmm/place/g/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/e;->i()Lcom/google/android/apps/gmm/place/g/a/f;

    move-result-object v0

    if-nez v0, :cond_16

    :cond_b
    move-object v0, v4

    :goto_6
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->z()Lcom/google/android/apps/gmm/place/i/l;

    move-result-object v1

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->z()Lcom/google/android/apps/gmm/place/i/l;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/i/l;->b()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/base/f/cl;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/ae;->z()Lcom/google/android/apps/gmm/place/i/l;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_c
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->w()Lcom/google/android/apps/gmm/place/h/b/d;

    move-result-object v1

    if-eqz v1, :cond_d

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->w()Lcom/google/android/apps/gmm/place/h/b/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/h/b/d;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/place/h/a/a;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/ae;->w()Lcom/google/android/apps/gmm/place/h/b/d;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_d
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->h()Lcom/google/android/apps/gmm/place/b;

    move-result-object v1

    if-eqz v1, :cond_e

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->h()Lcom/google/android/apps/gmm/place/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/b;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_e

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/place/c/d;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/ae;->h()Lcom/google/android/apps/gmm/place/b;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/b;->a()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Ljava/util/List;)V

    :cond_e
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->g()Lcom/google/android/apps/gmm/place/a/b/b;

    move-result-object v1

    if-eqz v1, :cond_f

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->g()Lcom/google/android/apps/gmm/place/a/b/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/a/b/b;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/place/a/a/e;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/ae;->g()Lcom/google/android/apps/gmm/place/a/b/b;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_f
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->g()Lcom/google/android/apps/gmm/place/c/a;

    :goto_7
    return-void

    :cond_10
    move v0, v3

    goto/16 :goto_0

    :cond_11
    move v0, v3

    goto/16 :goto_1

    :cond_12
    move v0, v3

    goto/16 :goto_2

    :cond_13
    move v1, v3

    goto/16 :goto_3

    :cond_14
    move v0, v3

    goto/16 :goto_4

    :cond_15
    move v0, v3

    goto/16 :goto_5

    :cond_16
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->y()Lcom/google/android/apps/gmm/place/g/a/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/g/a/e;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x5

    if-le v0, v2, :cond_17

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/g/a/e;->k()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_17

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/base/f/bt;

    new-instance v5, Lcom/google/android/apps/gmm/base/l/al;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/base/l/al;-><init>()V

    iget-object v6, v4, Lcom/google/android/apps/gmm/place/c/a;->c:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/gmm/l;->ma:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/apps/gmm/base/l/al;->a:Lcom/google/android/libraries/curvular/ah;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/g/a/e;->d()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v6

    invoke-static {v9, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/apps/gmm/base/l/al;->f:Lcom/google/android/libraries/curvular/ah;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/l/al;->a()Lcom/google/android/apps/gmm/base/l/ak;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/base/f/au;

    iget-object v5, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/place/ae;->y()Lcom/google/android/apps/gmm/place/g/a/e;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/gmm/place/g/a/e;->g()Lcom/google/android/apps/gmm/base/l/a/v;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_17
    iget-object v2, v4, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->q()Lcom/google/android/apps/gmm/place/g/a/b;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/gmm/place/g/a/b;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_8
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/g/a/a;

    const-class v7, Lcom/google/android/apps/gmm/place/c/u;

    new-instance v8, Lcom/google/android/apps/gmm/place/cd;

    invoke-direct {v8, v0}, Lcom/google/android/apps/gmm/place/cd;-><init>(Lcom/google/android/apps/gmm/place/g/a/a;)V

    invoke-virtual {v2, v7, v8}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_8

    :cond_18
    invoke-interface {v5}, Lcom/google/android/apps/gmm/place/g/a/b;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_19

    const-class v0, Lcom/google/android/apps/gmm/base/f/cf;

    invoke-virtual {v2, v0, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    const-class v0, Lcom/google/android/apps/gmm/base/f/t;

    invoke-virtual {v2, v0, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_19
    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/g/a/e;->k()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    iget-object v2, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/ae;->y()Lcom/google/android/apps/gmm/place/g/a/e;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/g/a/e;->i()Lcom/google/android/apps/gmm/place/g/a/f;

    move-result-object v2

    iget-object v5, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/place/ae;->y()Lcom/google/android/apps/gmm/place/g/a/e;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/gmm/place/g/a/e;->h()Lcom/google/android/apps/gmm/place/g/a/c;

    move-result-object v5

    invoke-static {v0, v2, v5}, Lcom/google/android/apps/gmm/place/bz;->a(Lcom/google/android/libraries/curvular/bc;Lcom/google/android/apps/gmm/place/g/a/f;Lcom/google/android/apps/gmm/place/g/a/c;)V

    :cond_1a
    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/g/a/e;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1b

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/base/f/bt;

    new-instance v5, Lcom/google/android/apps/gmm/base/l/al;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/base/l/al;-><init>()V

    iget-object v6, v4, Lcom/google/android/apps/gmm/place/c/a;->c:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/gmm/l;->aV:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/apps/gmm/base/l/al;->a:Lcom/google/android/libraries/curvular/ah;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/g/a/e;->e()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v6

    invoke-static {v9, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    iput-object v6, v5, Lcom/google/android/apps/gmm/base/l/al;->f:Lcom/google/android/libraries/curvular/ah;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/l/al;->a()Lcom/google/android/apps/gmm/base/l/ak;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/g/a/e;->a()Ljava/util/List;

    move-result-object v2

    const/4 v5, 0x4

    invoke-static {v0, v2, v5}, Lcom/google/android/apps/gmm/place/bz;->a(Lcom/google/android/libraries/curvular/bc;Ljava/util/List;I)V

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->y()Lcom/google/android/apps/gmm/place/g/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/e;->c()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v2, v4, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v5, Lcom/google/android/apps/gmm/base/f/ae;

    new-instance v0, Lcom/google/android/apps/gmm/base/l/al;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/l/al;-><init>()V

    iget-object v6, v4, Lcom/google/android/apps/gmm/place/c/a;->c:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/gmm/l;->iD:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    iput-object v6, v0, Lcom/google/android/apps/gmm/base/l/al;->a:Lcom/google/android/libraries/curvular/ah;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/g/a/e;->c()Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v9, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    iput-object v6, v0, Lcom/google/android/apps/gmm/base/l/al;->c:Lcom/google/android/libraries/curvular/ah;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/l/al;->a(Lcom/google/android/libraries/curvular/ce;)Lcom/google/android/apps/gmm/base/l/al;

    move-result-object v6

    const-class v0, Lcom/google/android/apps/gmm/place/g/a/e;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/g/a/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/e;->b()Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-static {v0, v3}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/apps/gmm/base/l/al;->e:Lcom/google/android/libraries/curvular/b/i;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/g/a/e;->f()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/apps/gmm/base/l/al;->f:Lcom/google/android/libraries/curvular/ah;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/l/al;->a()Lcom/google/android/apps/gmm/base/l/ak;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_1b
    move-object v0, v4

    goto/16 :goto_6

    :cond_1c
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/place/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    goto/16 :goto_7
.end method

.method protected final b()Ljava/lang/reflect/Type;
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/android/apps/gmm/place/ae;

    return-object v0
.end method
