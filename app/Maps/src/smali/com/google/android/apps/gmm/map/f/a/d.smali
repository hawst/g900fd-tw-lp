.class public final enum Lcom/google/android/apps/gmm/map/f/a/d;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/f/a/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/f/a/d;

.field public static final enum b:Lcom/google/android/apps/gmm/map/f/a/d;

.field public static final enum c:Lcom/google/android/apps/gmm/map/f/a/d;

.field public static final enum d:Lcom/google/android/apps/gmm/map/f/a/d;

.field public static final enum e:Lcom/google/android/apps/gmm/map/f/a/d;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/map/f/a/d;


# instance fields
.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/d;

    const-string v1, "TARGET_POINT"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/gmm/map/f/a/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->a:Lcom/google/android/apps/gmm/map/f/a/d;

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/d;

    const-string v1, "ZOOM"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/apps/gmm/map/f/a/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->b:Lcom/google/android/apps/gmm/map/f/a/d;

    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/d;

    const-string v1, "TILT"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/android/apps/gmm/map/f/a/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->c:Lcom/google/android/apps/gmm/map/f/a/d;

    .line 33
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/d;

    const-string v1, "BEARING"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/android/apps/gmm/map/f/a/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->d:Lcom/google/android/apps/gmm/map/f/a/d;

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/d;

    const-string v1, "LOOK_AHEAD"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/android/apps/gmm/map/f/a/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->e:Lcom/google/android/apps/gmm/map/f/a/d;

    .line 29
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/f/a/d;

    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->a:Lcom/google/android/apps/gmm/map/f/a/d;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->b:Lcom/google/android/apps/gmm/map/f/a/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->c:Lcom/google/android/apps/gmm/map/f/a/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->d:Lcom/google/android/apps/gmm/map/f/a/d;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->e:Lcom/google/android/apps/gmm/map/f/a/d;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->g:[Lcom/google/android/apps/gmm/map/f/a/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput p3, p0, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    .line 40
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/f/a/d;
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/gmm/map/f/a/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/f/a/d;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/f/a/d;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->g:[Lcom/google/android/apps/gmm/map/f/a/d;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/f/a/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/f/a/d;

    return-object v0
.end method
