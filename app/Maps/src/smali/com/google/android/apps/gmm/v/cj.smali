.class public Lcom/google/android/apps/gmm/v/cj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:[F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    .line 21
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 22
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/16 v1, 0xc

    aput p1, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/16 v1, 0xd

    aput p2, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/16 v1, 0xe

    aput p3, v0, v1

    .line 31
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/16 v4, 0x10

    const/4 v3, 0x0

    .line 205
    new-array v0, v4, [F

    .line 206
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    invoke-static {v0, v3, v1, v3}, Landroid/opengl/Matrix;->invertM([FI[FI)Z

    move-result v1

    .line 207
    if-eqz v1, :cond_0

    .line 208
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    invoke-static {v0, v3, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 210
    :cond_0
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 215
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    :goto_0
    const/16 v3, 0x10

    if-ge v0, v3, :cond_1

    aget v3, v1, v0

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x11

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    rem-int/lit8 v3, v0, 0x4

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
