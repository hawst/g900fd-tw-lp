.class public Lcom/google/android/apps/gmm/map/r/c/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/maps/g/a/ca;",
            "Lcom/google/b/f/t;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/maps/g/a/ca;",
            "Lcom/google/b/f/t;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/maps/g/a/ca;",
            "Lcom/google/r/b/a/anz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 38
    const-class v0, Lcom/google/maps/g/a/ca;

    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->a:Ljava/util/EnumMap;

    .line 40
    const-class v0, Lcom/google/maps/g/a/ca;

    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->b:Ljava/util/EnumMap;

    .line 42
    const-class v0, Lcom/google/maps/g/a/ca;

    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->c:Ljava/util/EnumMap;

    .line 44
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->b:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/b/f/t;->ge:Lcom/google/b/f/t;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->c:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/b/f/t;->gf:Lcom/google/b/f/t;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->e:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/b/f/t;->gg:Lcom/google/b/f/t;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->a:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/b/f/t;->gh:Lcom/google/b/f/t;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->d:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/b/f/t;->gg:Lcom/google/b/f/t;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->b:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/b/f/t;->b:Lcom/google/b/f/t;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->c:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/b/f/t;->c:Lcom/google/b/f/t;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->e:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/b/f/t;->d:Lcom/google/b/f/t;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->a:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/b/f/t;->e:Lcom/google/b/f/t;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->d:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/b/f/t;->d:Lcom/google/b/f/t;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->c:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->b:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/r/b/a/anz;->b:Lcom/google/r/b/a/anz;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->c:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->c:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/r/b/a/anz;->c:Lcom/google/r/b/a/anz;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->c:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->e:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/r/b/a/anz;->e:Lcom/google/r/b/a/anz;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->c:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->a:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/r/b/a/anz;->a:Lcom/google/r/b/a/anz;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->c:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ca;->d:Lcom/google/maps/g/a/ca;

    sget-object v2, Lcom/google/r/b/a/anz;->d:Lcom/google/r/b/a/anz;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    return-void
.end method

.method public static a(Lcom/google/r/b/a/anw;Z)Lcom/google/android/apps/gmm/map/internal/c/an;
    .locals 14

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 166
    .line 167
    iget-object v0, p0, Lcom/google/r/b/a/anw;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ia;->d()Lcom/google/r/b/a/ia;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ia;

    iget v1, v0, Lcom/google/r/b/a/ia;->b:I

    .line 168
    iget-object v0, p0, Lcom/google/r/b/a/anw;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ia;->d()Lcom/google/r/b/a/ia;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ia;

    iget v0, v0, Lcom/google/r/b/a/ia;->c:I

    .line 166
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(II)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    .line 169
    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v4, v5, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    .line 170
    const/4 v0, 0x1

    new-array v13, v0, [Lcom/google/android/apps/gmm/map/internal/c/a;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/a;

    const/4 v4, 0x0

    move v5, v3

    move v6, v3

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/internal/c/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;IFLcom/google/android/apps/gmm/map/b/a/y;FFF)V

    aput-object v0, v13, v2

    .line 174
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 176
    new-instance v8, Lcom/google/android/apps/gmm/map/internal/c/aa;

    iget-object v0, p0, Lcom/google/r/b/a/anw;->j:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    :goto_0
    const/4 v2, 0x4

    invoke-direct {v8, v0, v2}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(Ljava/lang/String;I)V

    .line 177
    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/z;

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/d;->b:Lcom/google/android/apps/gmm/map/internal/c/d;

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/gmm/map/internal/c/z;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/d;)V

    .line 180
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/cf;

    .line 181
    iget-wide v4, p0, Lcom/google/r/b/a/anw;->b:J

    .line 182
    invoke-virtual {p0}, Lcom/google/r/b/a/anw;->h()Ljava/lang/String;

    move-result-object v6

    .line 183
    invoke-virtual {p0}, Lcom/google/r/b/a/anw;->d()Ljava/lang/String;

    move-result-object v7

    .line 185
    invoke-virtual {p0}, Lcom/google/r/b/a/anw;->g()Ljava/lang/String;

    move-result-object v9

    .line 186
    iget v0, p0, Lcom/google/r/b/a/anw;->c:I

    invoke-static {v0}, Lcom/google/r/b/a/anz;->a(I)Lcom/google/r/b/a/anz;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/r/b/a/anz;->e:Lcom/google/r/b/a/anz;

    :cond_0
    iget v0, v0, Lcom/google/r/b/a/anz;->f:I

    invoke-static {v0}, Lcom/google/maps/g/a/ca;->a(I)Lcom/google/maps/g/a/ca;

    move-result-object v10

    .line 188
    iget-object v0, p0, Lcom/google/r/b/a/anw;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/dn;->d()Lcom/google/maps/g/a/dn;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v12

    check-cast v12, Lcom/google/maps/g/a/dn;

    move v11, p1

    invoke-direct/range {v3 .. v12}, Lcom/google/android/apps/gmm/map/internal/c/cf;-><init>(JLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/aa;Ljava/lang/String;Lcom/google/maps/g/a/ca;ZLcom/google/maps/g/a/dn;)V

    .line 189
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/an;->l()Lcom/google/android/apps/gmm/map/internal/c/ao;

    move-result-object v0

    .line 191
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 192
    iput-object v13, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->i:[Lcom/google/android/apps/gmm/map/internal/c/a;

    .line 193
    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    const/16 v1, 0x810

    .line 194
    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->u:I

    .line 195
    invoke-virtual {p0}, Lcom/google/r/b/a/anw;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->w:Ljava/lang/String;

    .line 196
    iput-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->B:Lcom/google/android/apps/gmm/map/internal/c/cf;

    .line 197
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/ao;->a()Lcom/google/android/apps/gmm/map/internal/c/an;

    move-result-object v0

    return-object v0

    .line 176
    :cond_1
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v2, p0, Lcom/google/r/b/a/anw;->j:Ljava/lang/Object;

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method public static a(Lcom/google/maps/g/a/da;Lcom/google/android/apps/gmm/map/i/a/a;Landroid/content/res/Resources;)Lcom/google/r/b/a/anw;
    .locals 14

    .prologue
    .line 88
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 89
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 90
    :cond_3
    iget v0, p0, Lcom/google/maps/g/a/da;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 91
    :cond_5
    if-nez p1, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 93
    :cond_6
    iget-object v0, p0, Lcom/google/maps/g/a/da;->n:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    :goto_3
    invoke-static {v0}, Lcom/google/b/h/d;->a(Ljava/lang/String;)J

    move-result-wide v2

    .line 94
    sget-object v1, Lcom/google/r/b/a/acy;->s:Lcom/google/r/b/a/acy;

    .line 95
    iget-object v0, p0, Lcom/google/maps/g/a/da;->p:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/i;

    invoke-virtual {v0}, Lcom/google/maps/g/a/i;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/gmm/map/i/a/a;->a(Ljava/lang/String;Lcom/google/r/b/a/acy;)Ljava/lang/String;

    move-result-object v4

    .line 96
    iget-object v0, p0, Lcom/google/maps/g/a/da;->q:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/i;

    invoke-virtual {v0}, Lcom/google/maps/g/a/i;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/gmm/map/i/a/a;->a(Ljava/lang/String;Lcom/google/r/b/a/acy;)Ljava/lang/String;

    move-result-object v5

    .line 97
    if-eqz v4, :cond_7

    if-nez v5, :cond_a

    .line 98
    :cond_7
    const/4 v0, 0x0

    .line 100
    :goto_4
    return-object v0

    .line 93
    :cond_8
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    iput-object v1, p0, Lcom/google/maps/g/a/da;->n:Ljava/lang/Object;

    :cond_9
    move-object v0, v1

    goto :goto_3

    .line 102
    :cond_a
    iget v0, p0, Lcom/google/maps/g/a/da;->o:I

    invoke-static {v0}, Lcom/google/maps/g/a/ca;->a(I)Lcom/google/maps/g/a/ca;

    move-result-object v0

    if-nez v0, :cond_b

    sget-object v0, Lcom/google/maps/g/a/ca;->e:Lcom/google/maps/g/a/ca;

    move-object v1, v0

    .line 103
    :goto_5
    iget-object v0, p0, Lcom/google/maps/g/a/da;->l:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/maps/g/gy;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v6

    .line 104
    iget-object v0, p0, Lcom/google/maps/g/a/da;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/maps/g/gy;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v7

    .line 105
    iget-object v0, p0, Lcom/google/maps/g/a/da;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/dh;->d()Lcom/google/maps/g/a/dh;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/dh;

    iget v8, v0, Lcom/google/maps/g/a/dh;->b:I

    .line 106
    iget-object v0, p0, Lcom/google/maps/g/a/da;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/dh;->d()Lcom/google/maps/g/a/dh;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/dh;

    iget v9, v0, Lcom/google/maps/g/a/dh;->c:I

    .line 109
    invoke-virtual {p0}, Lcom/google/maps/g/a/da;->d()Ljava/lang/String;

    move-result-object v10

    .line 110
    invoke-virtual {p0}, Lcom/google/maps/g/a/da;->h()Ljava/lang/String;

    move-result-object v11

    .line 111
    iget-object v0, p0, Lcom/google/maps/g/a/da;->s:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/dn;->d()Lcom/google/maps/g/a/dn;

    move-result-object v12

    invoke-virtual {v0, v12}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/dn;

    .line 100
    if-nez v1, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    move-object v1, v0

    .line 102
    goto :goto_5

    .line 100
    :cond_c
    if-nez v6, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    if-nez v7, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/b/a/q;->e()Lcom/google/r/b/a/ia;

    move-result-object v6

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/b/a/q;->e()Lcom/google/r/b/a/ia;

    move-result-object v7

    sget-object v12, Lcom/google/android/apps/gmm/map/r/c/a;->c:Ljava/util/EnumMap;

    invoke-virtual {v12, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/anz;

    if-nez v1, :cond_f

    sget-object v1, Lcom/google/r/b/a/anz;->e:Lcom/google/r/b/a/anz;

    :cond_f
    invoke-static {}, Lcom/google/r/b/a/anw;->newBuilder()Lcom/google/r/b/a/any;

    move-result-object v12

    iget v13, v12, Lcom/google/r/b/a/any;->a:I

    or-int/lit8 v13, v13, 0x1

    iput v13, v12, Lcom/google/r/b/a/any;->a:I

    iput-wide v2, v12, Lcom/google/r/b/a/any;->b:J

    if-nez v1, :cond_10

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_10
    iget v2, v12, Lcom/google/r/b/a/any;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v12, Lcom/google/r/b/a/any;->a:I

    iget v1, v1, Lcom/google/r/b/a/anz;->f:I

    iput v1, v12, Lcom/google/r/b/a/any;->c:I

    if-nez v6, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_11
    iget-object v1, v12, Lcom/google/r/b/a/any;->d:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/n/ao;->d:Z

    iget v1, v12, Lcom/google/r/b/a/any;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v12, Lcom/google/r/b/a/any;->a:I

    if-nez v7, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_12
    iget-object v1, v12, Lcom/google/r/b/a/any;->e:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/n/ao;->d:Z

    iget v1, v12, Lcom/google/r/b/a/any;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v12, Lcom/google/r/b/a/any;->a:I

    iget v1, v12, Lcom/google/r/b/a/any;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, v12, Lcom/google/r/b/a/any;->a:I

    iput v8, v12, Lcom/google/r/b/a/any;->f:I

    iget v1, v12, Lcom/google/r/b/a/any;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, v12, Lcom/google/r/b/a/any;->a:I

    iput v9, v12, Lcom/google/r/b/a/any;->g:I

    if-nez v4, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_13
    iget v1, v12, Lcom/google/r/b/a/any;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, v12, Lcom/google/r/b/a/any;->a:I

    iput-object v4, v12, Lcom/google/r/b/a/any;->i:Ljava/lang/Object;

    if-nez v5, :cond_14

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_14
    iget v1, v12, Lcom/google/r/b/a/any;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, v12, Lcom/google/r/b/a/any;->a:I

    iput-object v5, v12, Lcom/google/r/b/a/any;->j:Ljava/lang/Object;

    if-nez v11, :cond_15

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_15
    iget v1, v12, Lcom/google/r/b/a/any;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, v12, Lcom/google/r/b/a/any;->a:I

    iput-object v11, v12, Lcom/google/r/b/a/any;->h:Ljava/lang/Object;

    if-nez v10, :cond_16

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_16
    iget v1, v12, Lcom/google/r/b/a/any;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, v12, Lcom/google/r/b/a/any;->a:I

    iput-object v10, v12, Lcom/google/r/b/a/any;->k:Ljava/lang/Object;

    if-nez v0, :cond_17

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_17
    iget-object v1, v12, Lcom/google/r/b/a/any;->l:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, v12, Lcom/google/r/b/a/any;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, v12, Lcom/google/r/b/a/any;->a:I

    invoke-virtual {v12}, Lcom/google/r/b/a/any;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/anw;

    goto/16 :goto_4
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 207
    if-nez p0, :cond_1

    .line 208
    const/4 p0, 0x0

    .line 210
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const-string v0, "http"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https:"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_2
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
