.class Lcom/google/android/apps/gmm/place/riddler/d/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/riddler/c/c;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/riddler/c/c;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic c:Lcom/google/android/apps/gmm/x/o;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lcom/google/android/apps/gmm/place/riddler/d/g;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/riddler/d/g;Lcom/google/android/apps/gmm/x/o;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 466
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/riddler/d/j;->e:Lcom/google/android/apps/gmm/place/riddler/d/g;

    iput-object p2, p0, Lcom/google/android/apps/gmm/place/riddler/d/j;->c:Lcom/google/android/apps/gmm/x/o;

    iput-object p3, p0, Lcom/google/android/apps/gmm/place/riddler/d/j;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 467
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/j;->c:Lcom/google/android/apps/gmm/x/o;

    .line 468
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->aj()Lcom/google/android/apps/gmm/place/riddler/c/e;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/riddler/c/e;->a:Lcom/google/b/c/cv;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/j;->a:Lcom/google/b/c/cv;

    .line 471
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/j;->a:Lcom/google/b/c/cv;

    .line 472
    invoke-virtual {v1}, Lcom/google/b/c/cv;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/j;->b:Ljava/util/List;

    .line 471
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/j;->a:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/riddler/c/c;

    .line 478
    iget-object v2, v0, Lcom/google/android/apps/gmm/place/riddler/c/c;->d:Lcom/google/n/f;

    .line 479
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/riddler/d/j;->e:Lcom/google/android/apps/gmm/place/riddler/d/g;

    iget-object v3, v3, Lcom/google/android/apps/gmm/place/riddler/d/g;->h:Lcom/google/android/apps/gmm/place/riddler/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/riddler/d/j;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Lcom/google/android/apps/gmm/place/riddler/a;->b(Ljava/lang/String;Lcom/google/n/f;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 480
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/riddler/d/j;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 487
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/j;->e:Lcom/google/android/apps/gmm/place/riddler/d/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/riddler/d/j;->b:Ljava/util/List;

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/riddler/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/gmm/place/riddler/d/k;

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/gmm/place/riddler/d/k;-><init>(Lcom/google/android/apps/gmm/place/riddler/d/g;Ljava/util/List;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v3, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 488
    return-void
.end method
