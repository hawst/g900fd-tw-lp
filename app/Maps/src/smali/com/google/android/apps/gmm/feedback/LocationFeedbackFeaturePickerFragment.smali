.class public Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;
.super Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/map/b/a/q;

.field b:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;-><init>()V

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/feedback/r;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/feedback/r;-><init>(Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;->b:Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/views/q;->o()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;Z)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/base/views/q;->b(Z)V

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 66
    sget v0, Lcom/google/android/apps/gmm/l;->hW:I

    return v0
.end method

.method protected final a_()V
    .locals 5

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-static {}, Lcom/google/maps/g/gy;->newBuilder()Lcom/google/maps/g/ha;

    move-result-object v1

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget v4, v1, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v1, Lcom/google/maps/g/ha;->a:I

    iput-wide v2, v1, Lcom/google/maps/g/ha;->b:D

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget v0, v1, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v1, Lcom/google/maps/g/ha;->a:I

    iput-wide v2, v1, Lcom/google/maps/g/ha;->c:D

    invoke-virtual {v1}, Lcom/google/maps/g/ha;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->l:Lcom/google/android/apps/gmm/base/fragments/a/b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->l:Lcom/google/android/apps/gmm/base/fragments/a/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/fragments/a/b;->a(Ljava/lang/Object;)V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 90
    return-void
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 71
    sget v0, Lcom/google/android/apps/gmm/l;->hS:I

    return v0
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 76
    sget v0, Lcom/google/android/apps/gmm/d;->aM:I

    return v0
.end method

.method protected final d()I
    .locals 1

    .prologue
    .line 81
    sget v0, Lcom/google/android/apps/gmm/l;->fW:I

    return v0
.end method

.method protected final f()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFeaturePickerFragment;->b:Ljava/lang/Object;

    return-object v0
.end method
