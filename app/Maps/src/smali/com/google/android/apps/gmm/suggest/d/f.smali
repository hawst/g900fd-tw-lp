.class Lcom/google/android/apps/gmm/suggest/d/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field i:I

.field j:I

.field k:[I

.field l:Lcom/google/n/f;

.field m:Lcom/google/b/f/bj;

.field final synthetic n:Lcom/google/android/apps/gmm/suggest/d/e;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/suggest/d/e;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/suggest/d/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 138
    iput-object p1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->n:Lcom/google/android/apps/gmm/suggest/d/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput v0, p0, Lcom/google/android/apps/gmm/suggest/d/f;->a:I

    .line 124
    iput v0, p0, Lcom/google/android/apps/gmm/suggest/d/f;->b:I

    .line 125
    iput v0, p0, Lcom/google/android/apps/gmm/suggest/d/f;->c:I

    .line 126
    iput v0, p0, Lcom/google/android/apps/gmm/suggest/d/f;->d:I

    .line 127
    iput v0, p0, Lcom/google/android/apps/gmm/suggest/d/f;->e:I

    .line 128
    iput v0, p0, Lcom/google/android/apps/gmm/suggest/d/f;->f:I

    .line 129
    iput v0, p0, Lcom/google/android/apps/gmm/suggest/d/f;->g:I

    .line 130
    iput v0, p0, Lcom/google/android/apps/gmm/suggest/d/f;->h:I

    .line 131
    iput v0, p0, Lcom/google/android/apps/gmm/suggest/d/f;->i:I

    .line 132
    iput v0, p0, Lcom/google/android/apps/gmm/suggest/d/f;->j:I

    .line 133
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/e;->a:[I

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/f;->k:[I

    .line 134
    iput-object v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->l:Lcom/google/n/f;

    .line 135
    iput-object v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->m:Lcom/google/b/f/bj;

    .line 139
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/suggest/d/f;->a(Ljava/util/List;)V

    .line 140
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/suggest/d/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 144
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/suggest/d/g;

    .line 146
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/g;->d()Lcom/google/android/apps/gmm/suggest/e/a;

    move-result-object v1

    iget-wide v6, v1, Lcom/google/android/apps/gmm/suggest/e/a;->c:J

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->n:Lcom/google/android/apps/gmm/suggest/d/e;

    iget-wide v8, v1, Lcom/google/android/apps/gmm/suggest/d/e;->b:J

    sub-long/2addr v6, v8

    .line 147
    iget v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->a:I

    if-nez v1, :cond_0

    .line 148
    long-to-int v1, v6

    iput v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->a:I

    .line 150
    :cond_0
    long-to-int v1, v6

    iput v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->b:I

    .line 151
    iget v6, p0, Lcom/google/android/apps/gmm/suggest/d/f;->f:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/g;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    add-int/2addr v1, v6

    iput v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->f:I

    .line 152
    iget v6, p0, Lcom/google/android/apps/gmm/suggest/d/f;->g:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/g;->h()Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    :goto_2
    add-int/2addr v1, v6

    iput v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->g:I

    .line 153
    iget v6, p0, Lcom/google/android/apps/gmm/suggest/d/f;->h:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/g;->i()Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    :goto_3
    add-int/2addr v1, v6

    iput v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->h:I

    .line 154
    iget v6, p0, Lcom/google/android/apps/gmm/suggest/d/f;->i:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/g;->j()Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v2

    :goto_4
    add-int/2addr v1, v6

    iput v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->i:I

    .line 155
    iget v6, p0, Lcom/google/android/apps/gmm/suggest/d/f;->j:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/g;->k()Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v2

    :goto_5
    add-int/2addr v1, v6

    iput v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->j:I

    .line 156
    iget v6, p0, Lcom/google/android/apps/gmm/suggest/d/f;->e:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/g;->l()Z

    move-result v1

    if-eqz v1, :cond_8

    move v1, v2

    :goto_6
    add-int/2addr v1, v6

    iput v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->e:I

    .line 159
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/g;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 160
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/g;->m()I

    move-result v6

    .line 161
    iget v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->c:I

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->c:I

    .line 162
    iget v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->d:I

    add-int/2addr v1, v6

    iput v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->d:I

    .line 163
    if-gez v6, :cond_9

    const-string v1, "SearchboxStatsUe3Event"

    const-string v6, "Round trip time must not be negative"

    new-array v7, v3, [Ljava/lang/Object;

    invoke-static {v1, v6, v7}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v4

    .line 164
    :cond_1
    :goto_7
    if-ltz v1, :cond_2

    .line 165
    iget-object v6, p0, Lcom/google/android/apps/gmm/suggest/d/f;->k:[I

    aget v7, v6, v1

    add-int/lit8 v7, v7, 0x1

    aput v7, v6, v1

    .line 169
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/g;->e()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/suggest/d/f;->l:Lcom/google/n/f;

    .line 170
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/g;->f()Lcom/google/b/f/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/f;->m:Lcom/google/b/f/bj;

    goto/16 :goto_0

    :cond_3
    move v1, v3

    .line 151
    goto :goto_1

    :cond_4
    move v1, v3

    .line 152
    goto :goto_2

    :cond_5
    move v1, v3

    .line 153
    goto :goto_3

    :cond_6
    move v1, v3

    .line 154
    goto :goto_4

    :cond_7
    move v1, v3

    .line 155
    goto :goto_5

    :cond_8
    move v1, v3

    .line 156
    goto :goto_6

    :cond_9
    move v1, v3

    .line 163
    :goto_8
    sget-object v7, Lcom/google/android/apps/gmm/suggest/d/e;->a:[I

    const/16 v7, 0xa

    if-ge v1, v7, :cond_a

    sget-object v7, Lcom/google/android/apps/gmm/suggest/d/e;->a:[I

    aget v7, v7, v1

    if-lt v6, v7, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_a
    const-string v1, "SearchboxStatsUe3Event"

    const-string v6, "Should never reach here"

    new-array v7, v3, [Ljava/lang/Object;

    invoke-static {v1, v6, v7}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v4

    goto :goto_7

    .line 172
    :cond_b
    return-void
.end method


# virtual methods
.method final a()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 181
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    move v2, v1

    .line 183
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/gmm/suggest/d/f;->k:[I

    array-length v4, v4

    if-ge v0, v4, :cond_3

    .line 184
    iget-object v4, p0, Lcom/google/android/apps/gmm/suggest/d/f;->k:[I

    aget v4, v4, v0

    .line 185
    if-nez v4, :cond_0

    .line 186
    add-int/lit8 v2, v2, 0x1

    .line 183
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 190
    :cond_0
    if-ne v2, v7, :cond_1

    .line 191
    const-string v2, "0i"

    .line 197
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xb

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v1

    .line 198
    goto :goto_1

    .line 192
    :cond_1
    if-le v2, v7, :cond_2

    .line 193
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v5, 0xc

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 195
    :cond_2
    const-string v2, ""

    goto :goto_2

    .line 200
    :cond_3
    const-string v0, "i"

    new-instance v1, Lcom/google/b/a/ab;

    invoke-direct {v1, v0}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
