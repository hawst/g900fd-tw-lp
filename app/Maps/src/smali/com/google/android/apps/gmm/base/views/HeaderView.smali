.class public Lcom/google/android/apps/gmm/base/views/HeaderView;
.super Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# static fields
.field private static final e:Landroid/view/MenuItem;


# instance fields
.field private a:Landroid/app/Fragment;

.field private b:Landroid/content/Context;

.field private c:Landroid/widget/PopupMenu;

.field private d:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 217
    new-instance v0, Lcom/google/android/apps/gmm/base/views/z;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/z;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/HeaderView;->e:Landroid/view/MenuItem;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 265
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;-><init>(Landroid/content/Context;)V

    .line 208
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 209
    sget v0, Lcom/google/android/apps/gmm/h;->e:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->a(Landroid/content/Context;I)V

    .line 267
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/views/aa;)V
    .locals 3

    .prologue
    .line 275
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/aa;->a:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;-><init>(Landroid/content/Context;)V

    .line 208
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 209
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/aa;->a:Landroid/content/Context;

    .line 278
    iget v1, p1, Lcom/google/android/apps/gmm/base/views/aa;->c:I

    if-eqz v1, :cond_0

    .line 279
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p1, Lcom/google/android/apps/gmm/base/views/aa;->a:Landroid/content/Context;

    iget v2, p1, Lcom/google/android/apps/gmm/base/views/aa;->c:I

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 282
    :cond_0
    iget v1, p1, Lcom/google/android/apps/gmm/base/views/aa;->b:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/HeaderView;->a(Landroid/content/Context;I)V

    .line 284
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/aa;->d:Landroid/app/Fragment;

    if-eqz v0, :cond_1

    .line 285
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/aa;->d:Landroid/app/Fragment;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->setFragment(Landroid/app/Fragment;)V

    .line 287
    :cond_1
    return-void
.end method

.method private a()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 366
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/a;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/a;-><init>(Landroid/content/Context;)V

    .line 367
    sget v0, Lcom/google/android/apps/gmm/g;->aO:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_3

    .line 368
    :cond_0
    :goto_0
    sget v0, Lcom/google/android/apps/gmm/g;->dz:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_5

    .line 369
    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v2, Lcom/google/android/apps/gmm/l;->iK:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_6

    .line 370
    :cond_2
    :goto_3
    iget-object v0, v1, Lcom/google/android/apps/gmm/shared/c/c/a;->a:Ljava/lang/StringBuffer;

    return-object v0

    .line 367
    :cond_3
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v4, v1, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_0

    .line 368
    :cond_4
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    :cond_5
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v4, v1, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_2

    .line 369
    :cond_6
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v4, v1, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_3
.end method

.method private a(Landroid/content/Context;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 290
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->b:Landroid/content/Context;

    .line 291
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, p2, p0, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 292
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->getChildCount()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 293
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/views/HeaderView;->getChildAt(I)Landroid/view/View;

    .line 295
    sget v0, Lcom/google/android/apps/gmm/g;->ey:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 296
    if-eqz v0, :cond_2

    .line 297
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 299
    :cond_2
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 300
    sget v0, Lcom/google/android/apps/gmm/g;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 302
    :cond_3
    return-void
.end method

.method private b()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 553
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/m;->c:I

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x10100f5

    aput v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 556
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 558
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method


# virtual methods
.method public final a(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 531
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->a(Landroid/view/View;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;Z)Landroid/view/View;
    .locals 4

    .prologue
    .line 536
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->d:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 537
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->d:Landroid/view/ViewGroup;

    .line 539
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 540
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 542
    if-nez p2, :cond_1

    .line 544
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    .line 545
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->b()I

    move-result v2

    add-int/2addr v1, v2

    .line 546
    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 547
    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    .line 543
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 549
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->d:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 564
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/gmm/g;->ey:I

    if-ne v0, v1, :cond_1

    .line 565
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->a:Landroid/app/Fragment;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/HeaderView;->e:Landroid/view/MenuItem;

    invoke-virtual {v0, v1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 570
    :cond_0
    :goto_0
    return-void

    .line 566
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/gmm/g;->bH:I

    if-ne v0, v1, :cond_0

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->a:Landroid/app/Fragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->c:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 568
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->c:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 579
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->c:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_0

    .line 580
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->c:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    .line 582
    :cond_0
    return-void
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 574
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->a:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->a:Landroid/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setFragment(Landroid/app/Fragment;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 306
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->a:Landroid/app/Fragment;

    .line 307
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/Fragment;->setHasOptionsMenu(Z)V

    .line 308
    sget v0, Lcom/google/android/apps/gmm/g;->bH:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/widget/PopupMenu;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->b:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->c:Landroid/widget/PopupMenu;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->a:Landroid/app/Fragment;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->c:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->c:Landroid/widget/PopupMenu;

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->c:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/Menu;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->c:Landroid/widget/PopupMenu;

    invoke-virtual {v0, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 309
    :goto_0
    return-void

    .line 308
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v4, p0, Lcom/google/android/apps/gmm/base/views/HeaderView;->c:Landroid/widget/PopupMenu;

    goto :goto_0
.end method

.method public final setTitle(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 336
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget v2, Lcom/google/android/apps/gmm/g;->dS:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/views/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 337
    sget v0, Lcom/google/android/apps/gmm/g;->aO:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 338
    sget v0, Lcom/google/android/apps/gmm/g;->ey:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 339
    return-void

    :cond_0
    move v0, v1

    .line 336
    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 329
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget v2, Lcom/google/android/apps/gmm/g;->dS:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/views/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 330
    sget v0, Lcom/google/android/apps/gmm/g;->aO:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 331
    sget v0, Lcom/google/android/apps/gmm/g;->ey:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/HeaderView;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 332
    return-void

    :cond_0
    move v0, v1

    .line 329
    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    goto :goto_1
.end method
