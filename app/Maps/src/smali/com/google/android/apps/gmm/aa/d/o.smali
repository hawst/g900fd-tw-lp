.class public Lcom/google/android/apps/gmm/aa/d/o;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/aa/d/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/net/ad;)V
    .locals 7

    .prologue
    .line 26
    new-instance v1, Lcom/google/android/apps/gmm/aa/d/d;

    const-string v0, "tts-cache"

    const/4 v2, 0x0

    .line 28
    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    const-wide/32 v4, 0x3200000

    const/16 v6, 0x3e8

    move-object v2, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/aa/d/d;-><init>(Lcom/google/android/apps/gmm/shared/net/ad;Ljava/lang/String;JI)V

    .line 26
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/aa/d/o;-><init>(Lcom/google/android/apps/gmm/aa/d/d;)V

    .line 30
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/aa/d/d;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/d/o;->a:Lcom/google/android/apps/gmm/aa/d/d;

    .line 35
    return-void
.end method

.method static b(Lcom/google/android/apps/gmm/aa/d/l;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/l;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/l;->b:Ljava/util/Locale;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/aa/d/l;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 69
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/l;->d:Lcom/google/android/apps/gmm/aa/d/n;

    aput-object v2, v0, v1

    .line 68
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 70
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/aa/d/l;)Ljava/io/File;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 44
    invoke-static {p1}, Lcom/google/android/apps/gmm/aa/d/o;->b(Lcom/google/android/apps/gmm/aa/d/l;)Ljava/lang/String;

    move-result-object v1

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/o;->a:Lcom/google/android/apps/gmm/aa/d/d;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/aa/d/d;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/o;->a:Lcom/google/android/apps/gmm/aa/d/d;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/aa/d/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 48
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
