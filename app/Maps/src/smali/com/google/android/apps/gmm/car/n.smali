.class Lcom/google/android/apps/gmm/car/n;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/car/e;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/k;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/k;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/n;->a:Lcom/google/android/apps/gmm/car/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    .line 60
    sget-object v0, Lcom/google/android/apps/gmm/car/k;->a:Ljava/lang/String;

    .line 61
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/n;->a:Lcom/google/android/apps/gmm/car/k;

    sget-object v0, Lcom/google/android/apps/gmm/car/k;->a:Ljava/lang/String;

    iget-object v0, v2, Lcom/google/android/apps/gmm/car/k;->e:Lcom/google/android/gms/common/api/o;

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/k;->d:Lcom/google/android/apps/gmm/car/e;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/car/e;->b()Lcom/google/android/gms/car/f;

    move-result-object v3

    const-string v4, "gmm_package_name"

    invoke-interface {v3, v0, v4}, Lcom/google/android/gms/car/f;->a(Lcom/google/android/gms/common/api/o;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/k;->b:Landroid/app/Service;

    invoke-virtual {v3}, Landroid/app/Service;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/k;->b()V

    .line 62
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v3, v2, Lcom/google/android/apps/gmm/car/k;->c:Lcom/google/android/apps/gmm/car/f;

    iget-object v0, v2, Lcom/google/android/apps/gmm/car/k;->e:Lcom/google/android/gms/common/api/o;

    sget-object v4, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;

    iget-object v4, v3, Lcom/google/android/apps/gmm/car/f;->b:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v5

    if-nez v5, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iput-object v0, v3, Lcom/google/android/apps/gmm/car/f;->f:Lcom/google/android/gms/common/api/o;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s()Lcom/google/android/apps/gmm/car/a/g;

    move-result-object v0

    iget-object v5, v3, Lcom/google/android/apps/gmm/car/f;->s:Lcom/google/android/apps/gmm/car/a/h;

    iget-object v6, v0, Lcom/google/android/apps/gmm/car/a/g;->c:Ljava/lang/Object;

    monitor-enter v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iput-object v5, v0, Lcom/google/android/apps/gmm/car/a/g;->b:Lcom/google/android/apps/gmm/car/a/h;

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v0, v3, Lcom/google/android/apps/gmm/car/f;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v5, Lcom/google/android/apps/gmm/car/a/d;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Lcom/google/android/apps/gmm/car/a/d;-><init>(Z)V

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v0, v3, Lcom/google/android/apps/gmm/car/f;->d:Lcom/google/android/apps/gmm/car/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/e;->a()Lcom/google/android/gms/car/d;

    move-result-object v0

    iget-object v5, v3, Lcom/google/android/apps/gmm/car/f;->f:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0, v5}, Lcom/google/android/gms/car/d;->c(Lcom/google/android/gms/common/api/o;)Lcom/google/android/gms/car/aq;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_4
    .catch Lcom/google/android/gms/car/ao; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/google/android/gms/car/ap; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_0
    move-exception v0

    :try_start_5
    sget-object v0, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :goto_1
    :try_start_6
    iget-object v0, v3, Lcom/google/android/apps/gmm/car/f;->d:Lcom/google/android/apps/gmm/car/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/e;->b()Lcom/google/android/gms/car/f;

    move-result-object v0

    iget-object v5, v3, Lcom/google/android/apps/gmm/car/f;->f:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0, v5}, Lcom/google/android/gms/car/f;->b(Lcom/google/android/gms/common/api/o;)Lcom/google/android/gms/car/ag;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_6
    .catch Lcom/google/android/gms/car/ao; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_1
    move-exception v0

    :try_start_7
    sget-object v0, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_2
    :try_start_8
    iget-object v0, v3, Lcom/google/android/apps/gmm/car/f;->d:Lcom/google/android/apps/gmm/car/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/e;->b()Lcom/google/android/gms/car/f;

    move-result-object v0

    iget-object v5, v3, Lcom/google/android/apps/gmm/car/f;->f:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0, v5}, Lcom/google/android/gms/car/f;->a(Lcom/google/android/gms/common/api/o;)Lcom/google/android/gms/car/ak;

    move-result-object v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_8
    .catch Lcom/google/android/gms/car/ao; {:try_start_8 .. :try_end_8} :catch_2
    .catch Lcom/google/android/gms/car/ap; {:try_start_8 .. :try_end_8} :catch_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_2
    move-exception v0

    :try_start_9
    sget-object v0, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;

    :cond_2
    :goto_3
    new-instance v0, Lcom/google/android/apps/gmm/car/i/ab;

    iget-object v5, v3, Lcom/google/android/apps/gmm/car/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v5

    iget-object v6, v3, Lcom/google/android/apps/gmm/car/f;->c:Lcom/google/android/apps/gmm/base/a;

    iget-object v7, v3, Lcom/google/android/apps/gmm/car/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v7

    invoke-direct {v0, v5, v6, v7}, Lcom/google/android/apps/gmm/car/i/ab;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/shared/c/f;)V

    iput-object v0, v3, Lcom/google/android/apps/gmm/car/f;->l:Lcom/google/android/apps/gmm/car/i/ab;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/f;->l:Lcom/google/android/apps/gmm/car/i/ab;

    sget-object v5, Lcom/google/android/apps/gmm/car/i/ab;->a:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/apps/gmm/car/i/ab;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v5

    iget-object v6, v0, Lcom/google/android/apps/gmm/car/i/ab;->k:Ljava/lang/Object;

    invoke-interface {v5, v6}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    iget-object v5, v0, Lcom/google/android/apps/gmm/car/i/ab;->e:Lcom/google/android/apps/gmm/car/w;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/car/w;->a()V

    iget-boolean v5, v0, Lcom/google/android/apps/gmm/car/i/ab;->g:Z

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/car/i/ab;->h:Z

    iget-object v5, v0, Lcom/google/android/apps/gmm/car/i/ab;->c:Lcom/google/android/apps/gmm/car/bg;

    sget-object v6, Lcom/google/o/h/a/dq;->p:Lcom/google/o/h/a/dq;

    sget-object v7, Lcom/google/android/apps/gmm/startpage/af;->a:Lcom/google/android/apps/gmm/startpage/af;

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Lcom/google/android/apps/gmm/car/bg;->a(Lcom/google/o/h/a/dq;Lcom/google/android/apps/gmm/startpage/af;Z)Z

    move-result v5

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/car/i/ab;->g:Z

    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/f;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v3, v3, Lcom/google/android/apps/gmm/car/f;->q:Ljava/lang/Object;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/car/k;->f:Z

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    :try_start_a
    monitor-exit v6
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_3
    :try_start_c
    check-cast v0, Lcom/google/android/gms/car/aq;

    iput-object v0, v3, Lcom/google/android/apps/gmm/car/f;->h:Lcom/google/android/gms/car/aq;

    new-instance v0, Lcom/google/android/apps/gmm/car/p;

    iget-object v5, v3, Lcom/google/android/apps/gmm/car/f;->h:Lcom/google/android/gms/car/aq;

    iget-object v6, v3, Lcom/google/android/apps/gmm/car/f;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-direct {v0, v5, v6}, Lcom/google/android/apps/gmm/car/p;-><init>(Lcom/google/android/gms/car/aq;Lcom/google/android/apps/gmm/map/util/b/g;)V

    iput-object v0, v3, Lcom/google/android/apps/gmm/car/f;->i:Lcom/google/android/apps/gmm/car/p;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/f;->i:Lcom/google/android/apps/gmm/car/p;
    :try_end_c
    .catch Lcom/google/android/gms/car/ao; {:try_start_c .. :try_end_c} :catch_0
    .catch Lcom/google/android/gms/car/ap; {:try_start_c .. :try_end_c} :catch_7
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :try_start_d
    iget-object v5, v0, Lcom/google/android/apps/gmm/car/p;->b:Lcom/google/android/gms/car/aq;

    const/4 v6, 0x1

    const/4 v7, 0x3

    invoke-virtual {v5, v0, v6, v7}, Lcom/google/android/gms/car/aq;->a(Lcom/google/android/gms/car/as;II)Z
    :try_end_d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d .. :try_end_d} :catch_4
    .catch Lcom/google/android/gms/car/ao; {:try_start_d .. :try_end_d} :catch_5
    .catch Lcom/google/android/gms/car/ap; {:try_start_d .. :try_end_d} :catch_7
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :goto_4
    :try_start_e
    new-instance v0, Lcom/google/android/apps/gmm/car/aa;

    iget-object v5, v3, Lcom/google/android/apps/gmm/car/f;->h:Lcom/google/android/gms/car/aq;

    iget-object v6, v3, Lcom/google/android/apps/gmm/car/f;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-direct {v0, v5, v6}, Lcom/google/android/apps/gmm/car/aa;-><init>(Lcom/google/android/gms/car/aq;Lcom/google/android/apps/gmm/map/util/b/g;)V

    iput-object v0, v3, Lcom/google/android/apps/gmm/car/f;->j:Lcom/google/android/apps/gmm/car/aa;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/f;->j:Lcom/google/android/apps/gmm/car/aa;
    :try_end_e
    .catch Lcom/google/android/gms/car/ao; {:try_start_e .. :try_end_e} :catch_0
    .catch Lcom/google/android/gms/car/ap; {:try_start_e .. :try_end_e} :catch_7
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :try_start_f
    iget-object v5, v0, Lcom/google/android/apps/gmm/car/aa;->b:Lcom/google/android/gms/car/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/aa;->d:Lcom/google/android/gms/car/as;

    const/16 v6, 0xb

    const/4 v7, 0x3

    invoke-virtual {v5, v0, v6, v7}, Lcom/google/android/gms/car/aq;->a(Lcom/google/android/gms/car/as;II)Z
    :try_end_f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_f .. :try_end_f} :catch_3
    .catch Lcom/google/android/gms/car/ao; {:try_start_f .. :try_end_f} :catch_6
    .catch Lcom/google/android/gms/car/ap; {:try_start_f .. :try_end_f} :catch_7
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_1

    :catch_3
    move-exception v0

    :try_start_10
    sget-object v0, Lcom/google/android/apps/gmm/car/aa;->a:Ljava/lang/String;

    goto/16 :goto_1

    :catch_4
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/car/p;->a:Ljava/lang/String;

    goto :goto_4

    :catch_5
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/car/p;->a:Ljava/lang/String;

    goto :goto_4

    :catch_6
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/car/aa;->a:Ljava/lang/String;
    :try_end_10
    .catch Lcom/google/android/gms/car/ao; {:try_start_10 .. :try_end_10} :catch_0
    .catch Lcom/google/android/gms/car/ap; {:try_start_10 .. :try_end_10} :catch_7
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto/16 :goto_1

    :catch_7
    move-exception v0

    :try_start_11
    sget-object v0, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto/16 :goto_1

    :cond_4
    :try_start_12
    check-cast v0, Lcom/google/android/gms/car/ag;

    iput-object v0, v3, Lcom/google/android/apps/gmm/car/f;->g:Lcom/google/android/gms/car/ag;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/f;->k:Lcom/google/android/apps/gmm/car/az;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_5

    :cond_6
    new-instance v0, Lcom/google/android/apps/gmm/car/az;

    iget-object v5, v3, Lcom/google/android/apps/gmm/car/f;->g:Lcom/google/android/gms/car/ag;

    iget-object v6, v3, Lcom/google/android/apps/gmm/car/f;->r:Lcom/google/android/apps/gmm/car/bb;

    invoke-direct {v0, v5, v6}, Lcom/google/android/apps/gmm/car/az;-><init>(Lcom/google/android/gms/car/ag;Lcom/google/android/apps/gmm/car/bb;)V

    iput-object v0, v3, Lcom/google/android/apps/gmm/car/f;->k:Lcom/google/android/apps/gmm/car/az;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/f;->k:Lcom/google/android/apps/gmm/car/az;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/az;->a()V
    :try_end_12
    .catch Lcom/google/android/gms/car/ao; {:try_start_12 .. :try_end_12} :catch_1
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    goto/16 :goto_2

    :cond_7
    if-eqz v0, :cond_2

    :try_start_13
    new-instance v5, Lcom/google/android/apps/gmm/car/bc;

    iget-object v6, v3, Lcom/google/android/apps/gmm/car/f;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v7, v3, Lcom/google/android/apps/gmm/car/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v5, v6, v0, v7}, Lcom/google/android/apps/gmm/car/bc;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/gms/car/ak;Landroid/content/Context;)V

    iput-object v5, v3, Lcom/google/android/apps/gmm/car/f;->m:Lcom/google/android/apps/gmm/car/bc;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/f;->m:Lcom/google/android/apps/gmm/car/bc;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bc;->a()V
    :try_end_13
    .catch Lcom/google/android/gms/car/ao; {:try_start_13 .. :try_end_13} :catch_2
    .catch Lcom/google/android/gms/car/ap; {:try_start_13 .. :try_end_13} :catch_8
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    goto/16 :goto_3

    :catch_8
    move-exception v0

    :try_start_14
    sget-object v0, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    goto/16 :goto_3
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/google/android/apps/gmm/car/k;->a:Ljava/lang/String;

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/n;->a:Lcom/google/android/apps/gmm/car/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/k;->b()V

    .line 68
    return-void
.end method
