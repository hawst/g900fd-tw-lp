.class public Lcom/google/android/apps/gmm/place/PlacePageViewPager;
.super Lcom/google/android/apps/gmm/base/views/GmmViewPager;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/l;


# instance fields
.field final h:Landroid/view/LayoutInflater;

.field i:Lcom/google/android/apps/gmm/place/am;

.field j:Landroid/view/View;

.field k:Landroid/support/v4/widget/ContentLoadingProgressBar;

.field public l:Lcom/google/android/apps/gmm/place/an;

.field final m:Lcom/google/android/apps/gmm/util/a/e;

.field n:Lcom/google/android/apps/gmm/place/aa;

.field o:Z

.field p:Z

.field private q:Landroid/support/v4/view/bz;

.field private r:Lcom/google/android/apps/gmm/util/a/d;

.field private s:I

.field private final t:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 178
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;-><init>(Landroid/content/Context;)V

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->h:Landroid/view/LayoutInflater;

    .line 141
    new-instance v0, Lcom/google/android/apps/gmm/util/a/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/util/a/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->r:Lcom/google/android/apps/gmm/util/a/d;

    .line 142
    new-instance v0, Lcom/google/android/apps/gmm/util/a/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/util/a/e;-><init>()V

    .line 143
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/util/a/e;->c:Z

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->m:Lcom/google/android/apps/gmm/util/a/e;

    .line 147
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->o:Z

    .line 150
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->p:Z

    .line 157
    new-instance v0, Lcom/google/android/apps/gmm/place/ai;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/ai;-><init>(Lcom/google/android/apps/gmm/place/PlacePageViewPager;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->t:Ljava/lang/Object;

    .line 179
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->setClipChildren(Z)V

    new-instance v0, Lcom/google/android/apps/gmm/place/aj;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/aj;-><init>(Lcom/google/android/apps/gmm/place/PlacePageViewPager;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->q:Landroid/support/v4/view/bz;

    invoke-super {p0, v0}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->setOnPageChangeListener(Landroid/support/v4/view/bz;)V

    .line 180
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 183
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->h:Landroid/view/LayoutInflater;

    .line 141
    new-instance v0, Lcom/google/android/apps/gmm/util/a/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/util/a/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->r:Lcom/google/android/apps/gmm/util/a/d;

    .line 142
    new-instance v0, Lcom/google/android/apps/gmm/util/a/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/util/a/e;-><init>()V

    .line 143
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/util/a/e;->c:Z

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->m:Lcom/google/android/apps/gmm/util/a/e;

    .line 147
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->o:Z

    .line 150
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->p:Z

    .line 157
    new-instance v0, Lcom/google/android/apps/gmm/place/ai;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/ai;-><init>(Lcom/google/android/apps/gmm/place/PlacePageViewPager;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->t:Ljava/lang/Object;

    .line 184
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->setClipChildren(Z)V

    new-instance v0, Lcom/google/android/apps/gmm/place/aj;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/aj;-><init>(Lcom/google/android/apps/gmm/place/PlacePageViewPager;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->q:Landroid/support/v4/view/bz;

    invoke-super {p0, v0}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->setOnPageChangeListener(Landroid/support/v4/view/bz;)V

    .line 185
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/place/PlacePageViewPager;I)Z
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/am;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/am;->aa_()I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->b()I

    move-result v0

    .line 229
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/am;->aa_()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 230
    const/4 v0, 0x0

    .line 232
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/place/am;->a(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final K_()V
    .locals 2

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i()Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/a/a/a;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/a/a/a;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v1}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    invoke-static {p0}, Lcom/google/android/apps/gmm/a/a/b;->a(Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 238
    return-void
.end method

.method public final a()Landroid/support/v4/view/ag;
    .locals 2

    .prologue
    .line 530
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getAdapter not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;)Lcom/google/android/apps/gmm/place/PlacePageView;
    .locals 4

    .prologue
    .line 503
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 504
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 505
    instance-of v3, v0, Lcom/google/android/apps/gmm/place/PlacePageView;

    if-eqz v3, :cond_0

    .line 506
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 509
    check-cast v0, Lcom/google/android/apps/gmm/place/PlacePageView;

    .line 512
    :goto_1
    return-object v0

    .line 503
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 512
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 445
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->p:Z

    .line 449
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->a(Ljava/lang/Object;)Lcom/google/android/apps/gmm/place/PlacePageView;

    move-result-object v0

    .line 450
    if-eqz v0, :cond_0

    .line 451
    check-cast v0, Lcom/google/android/apps/gmm/place/l;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/place/l;->a(Z)V

    .line 455
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->h:Lcom/google/android/apps/gmm/util/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->r:Lcom/google/android/apps/gmm/util/a/d;

    new-instance v2, Lcom/google/android/apps/gmm/place/al;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/gmm/place/al;-><init>(Lcom/google/android/apps/gmm/place/PlacePageViewPager;Z)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/util/a/a;->a(Lcom/google/android/apps/gmm/util/a/d;Lcom/google/android/apps/gmm/util/a/d;)Lcom/google/android/apps/gmm/util/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->r:Lcom/google/android/apps/gmm/util/a/d;

    .line 468
    return-void
.end method

.method public final ab_()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 242
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final ac_()Z
    .locals 1

    .prologue
    .line 472
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->p:Z

    return v0
.end method

.method public final ad_()V
    .locals 1

    .prologue
    .line 486
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->a(Ljava/lang/Object;)Lcom/google/android/apps/gmm/place/PlacePageView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageView;->ad_()V

    .line 487
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 436
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->a(Ljava/lang/Object;)Lcom/google/android/apps/gmm/place/PlacePageView;

    move-result-object v0

    .line 437
    if-eqz v0, :cond_0

    .line 438
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v1, p1}, Lcom/google/android/apps/gmm/place/am;->a(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v1

    .line 439
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->l:Lcom/google/android/apps/gmm/place/an;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/place/PlacePageView;->a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V

    .line 441
    :cond_0
    return-void
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 477
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/am;->aa_()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/am;->aa_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->a(Ljava/lang/Object;)Lcom/google/android/apps/gmm/place/PlacePageView;

    move-result-object v0

    .line 478
    if-eqz v0, :cond_1

    .line 479
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageView;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->s:I

    .line 481
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->s:I

    return v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 313
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->b()I

    move-result v0

    .line 314
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/am;->aa_()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/am;->aa_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 317
    :cond_0
    return v0
.end method

.method public final g()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->k:Landroid/support/v4/widget/ContentLoadingProgressBar;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->k:Landroid/support/v4/widget/ContentLoadingProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/ContentLoadingProgressBar;->setVisibility(I)V

    .line 544
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->o:Z

    .line 546
    :try_start_0
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->a()Landroid/support/v4/view/ag;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ag;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->o:Z

    .line 550
    return-void

    .line 549
    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->o:Z

    throw v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 247
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->onAttachedToWindow()V

    .line 248
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->t:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 249
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->t:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 254
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->onDetachedFromWindow()V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->r:Lcom/google/android/apps/gmm/util/a/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->r:Lcom/google/android/apps/gmm/util/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/util/a/d;->b()V

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->m:Lcom/google/android/apps/gmm/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/util/a/e;->a()V

    .line 259
    return-void

    .line 255
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setAdapter(Lcom/google/android/apps/gmm/place/am;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 266
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    .line 267
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    .line 268
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    if-eqz v2, :cond_2

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    new-instance v0, Lcom/google/android/apps/gmm/place/ak;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/ak;-><init>(Lcom/google/android/apps/gmm/place/PlacePageViewPager;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->setAdapter(Landroid/support/v4/view/ag;)V

    .line 269
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v1, Lcom/google/android/apps/gmm/search/aq;

    .line 270
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/aq;->c()Lcom/google/android/apps/gmm/place/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->n:Lcom/google/android/apps/gmm/place/aa;

    .line 271
    return-void
.end method

.method public final setCurrentItem(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 291
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->o:Z

    .line 294
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->b()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->q:Landroid/support/v4/view/bz;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->q:Landroid/support/v4/view/bz;

    invoke-interface {v0, p1}, Landroid/support/v4/view/bz;->a(I)V

    .line 297
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->setCurrentItem(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->o:Z

    .line 301
    return-void

    .line 300
    :catchall_0
    move-exception v0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->o:Z

    throw v0
.end method

.method public final setOnPageChangeListener(Landroid/support/v4/view/bz;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->q:Landroid/support/v4/view/bz;

    .line 283
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->setOnPageChangeListener(Landroid/support/v4/view/bz;)V

    .line 284
    return-void
.end method
