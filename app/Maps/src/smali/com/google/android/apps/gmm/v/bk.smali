.class public Lcom/google/android/apps/gmm/v/bk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/apps/gmm/v/bm;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/apps/gmm/v/bl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bk;->a:Landroid/util/SparseArray;

    .line 19
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bk;->b:Landroid/util/SparseArray;

    .line 76
    return-void
.end method


# virtual methods
.method public final a(II)Lcom/google/android/apps/gmm/v/bl;
    .locals 3

    .prologue
    const/16 v2, 0x2bc

    .line 42
    if-le p1, v2, :cond_1

    add-int/lit16 v0, p1, -0x2bc

    .line 43
    :goto_0
    shl-int/lit8 v1, v0, 0x10

    .line 44
    if-le p2, v2, :cond_2

    add-int/lit16 v0, p2, -0x2bc

    .line 45
    :goto_1
    add-int/2addr v1, v0

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bk;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/bl;

    .line 48
    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/v/bl;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/gmm/v/bl;-><init>(II)V

    .line 50
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/bk;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 53
    :cond_0
    return-object v0

    :cond_1
    move v0, p1

    .line 42
    goto :goto_0

    :cond_2
    move v0, p2

    .line 44
    goto :goto_1
.end method

.method public final a(I)Lcom/google/android/apps/gmm/v/bm;
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bk;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/bm;

    .line 28
    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/google/android/apps/gmm/v/bm;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/v/bm;-><init>(I)V

    .line 30
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bk;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 33
    :cond_0
    return-object v0
.end method
