.class public Lcom/google/android/apps/gmm/map/indoor/d/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/indoor/d/d;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/gmm/map/indoor/d/d;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/indoor/d/b;->a:Lcom/google/android/apps/gmm/map/indoor/d/d;

    .line 17
    return-void
.end method

.method public static a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/indoor/d/b;
    .locals 2

    .prologue
    .line 21
    if-nez p0, :cond_0

    .line 22
    const/4 v0, 0x0

    .line 27
    :goto_0
    return-object v0

    .line 25
    :cond_0
    const/4 v0, 0x1

    .line 26
    const/16 v1, 0x1a

    invoke-virtual {p0, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 25
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/indoor/d/d;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/indoor/d/d;

    move-result-object v1

    .line 27
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/d/b;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/indoor/d/b;-><init>(Lcom/google/android/apps/gmm/map/indoor/d/d;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/maps/b/a/aa;)Lcom/google/android/apps/gmm/map/indoor/d/b;
    .locals 2

    .prologue
    .line 33
    if-nez p0, :cond_0

    .line 34
    const/4 v0, 0x0

    .line 39
    :goto_0
    return-object v0

    .line 38
    :cond_0
    invoke-virtual {p0}, Lcom/google/maps/b/a/aa;->a()Lcom/google/maps/b/a/ad;

    move-result-object v0

    .line 37
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/indoor/d/d;->a(Lcom/google/maps/b/a/ad;)Lcom/google/android/apps/gmm/map/indoor/d/d;

    move-result-object v1

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/d/b;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/indoor/d/b;-><init>(Lcom/google/android/apps/gmm/map/indoor/d/d;)V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 48
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/indoor/d/b;

    if-nez v2, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v0

    .line 52
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 53
    goto :goto_0

    .line 56
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/indoor/d/b;

    .line 58
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/b;->a:Lcom/google/android/apps/gmm/map/indoor/d/d;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/indoor/d/b;->a:Lcom/google/android/apps/gmm/map/indoor/d/d;

    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_0

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/b;->a:Lcom/google/android/apps/gmm/map/indoor/d/d;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
