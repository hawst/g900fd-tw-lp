.class public Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/i/h;


# instance fields
.field private c:Lcom/google/android/apps/gmm/directions/option/h;

.field private d:Lcom/google/android/apps/gmm/directions/i/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/maps/g/a/hm;Lcom/google/r/b/a/afz;Landroid/app/Fragment;)Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<",
            "L:Landroid/app/Fragment;",
            ":",
            "Lcom/google/android/apps/gmm/directions/option/h;",
            ">(",
            "Landroid/content/Context;",
            "Lcom/google/maps/g/a/hm;",
            "Lcom/google/r/b/a/afz;",
            "T",
            "L;",
            ")",
            "Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 44
    const-string v1, "view model"

    new-instance v2, Lcom/google/android/apps/gmm/directions/i/d;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/apps/gmm/directions/i/d;-><init>(Landroid/content/Context;Lcom/google/maps/g/a/hm;Lcom/google/r/b/a/afz;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 47
    invoke-virtual {p3}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "listener fragment"

    invoke-virtual {v1, v0, v2, p3}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    .line 48
    new-instance v1, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;-><init>()V

    .line 49
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 50
    return-object v1
.end method


# virtual methods
.method public final F_()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/google/r/b/a/afz;)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/option/h;

    sget-object v1, Lcom/google/b/f/t;->aI:Lcom/google/b/f/t;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/gmm/directions/option/h;->a(Lcom/google/r/b/a/afz;Lcom/google/b/f/t;)V

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;->dismiss()V

    .line 92
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;->dismiss()V

    .line 97
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 55
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "listener fragment"

    .line 59
    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/option/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/option/h;

    .line 60
    if-eqz p1, :cond_0

    :goto_0
    const-string v0, "view model"

    .line 61
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/i/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/d;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/d;

    iput-object p0, v0, Lcom/google/android/apps/gmm/directions/i/d;->a:Lcom/google/android/apps/gmm/directions/i/h;

    .line 63
    return-void

    :cond_0
    move-object p1, v1

    .line 60
    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/directions/c/j;

    .line 69
    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/d;

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iput-object v2, v1, Lcom/google/android/apps/gmm/directions/i/d;->b:Lcom/google/android/libraries/curvular/ag;

    .line 71
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 72
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/d;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 73
    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 84
    const-string v0, "view model"

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/d;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 85
    return-void
.end method
