.class public Lcom/google/android/apps/gmm/base/views/ac;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Landroid/os/Handler;

.field final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/google/android/apps/gmm/base/views/ae;

.field private final e:Lcom/google/android/apps/gmm/base/views/as;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/views/as;Lcom/google/android/apps/gmm/base/views/MultiColumnListView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;)V
    .locals 2

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/base/views/ad;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/base/views/ad;-><init>(Lcom/google/android/apps/gmm/base/views/ac;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ac;->a:Landroid/os/Handler;

    .line 44
    invoke-static {}, Lcom/google/b/c/hj;->e()Ljava/util/IdentityHashMap;

    move-result-object v0

    new-instance v1, Lcom/google/b/c/jt;

    invoke-direct {v1, v0}, Lcom/google/b/c/jt;-><init>(Ljava/util/Map;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/ac;->b:Ljava/util/Set;

    .line 45
    invoke-static {}, Lcom/google/b/c/hj;->e()Ljava/util/IdentityHashMap;

    move-result-object v0

    new-instance v1, Lcom/google/b/c/jt;

    invoke-direct {v1, v0}, Lcom/google/b/c/jt;-><init>(Ljava/util/Map;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/ac;->c:Ljava/util/Set;

    .line 105
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/ac;->e:Lcom/google/android/apps/gmm/base/views/as;

    .line 106
    new-instance v0, Lcom/google/android/apps/gmm/base/views/ae;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/apps/gmm/base/views/ae;-><init>(Lcom/google/android/apps/gmm/base/views/ac;Lcom/google/android/apps/gmm/base/views/MultiColumnListView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ac;->d:Lcom/google/android/apps/gmm/base/views/ae;

    .line 107
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/base/views/ac;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 22
    invoke-static {p1}, Lcom/google/android/apps/gmm/util/r;->d(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ac;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ac;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ac;->e:Lcom/google/android/apps/gmm/base/views/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ac;->e:Lcom/google/android/apps/gmm/base/views/as;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/base/views/as;->a(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ac;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
