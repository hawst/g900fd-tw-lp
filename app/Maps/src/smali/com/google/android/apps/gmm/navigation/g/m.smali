.class public Lcom/google/android/apps/gmm/navigation/g/m;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field private A:Lcom/google/android/apps/gmm/map/r/b/a;

.field private B:Lcom/google/android/apps/gmm/map/r/a/w;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private C:Lcom/google/android/apps/gmm/map/r/a/w;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final a:Lcom/google/android/apps/gmm/navigation/g/v;

.field b:Lcom/google/android/apps/gmm/navigation/g/s;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final c:Lcom/google/android/apps/gmm/map/util/b/g;

.field d:Lcom/google/android/apps/gmm/navigation/g/i;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/navigation/g/q;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final f:Lcom/google/android/apps/gmm/map/c/a;

.field public g:Z

.field final h:Lcom/google/android/apps/gmm/navigation/logging/e;

.field private final i:Landroid/content/Context;

.field private final j:Lcom/google/android/apps/gmm/navigation/g/h;

.field private final k:Lcom/google/android/apps/gmm/navigation/g/u;

.field private final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/g/s;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcom/google/android/apps/gmm/navigation/logging/h;

.field private final n:Lcom/google/android/apps/gmm/shared/net/a/l;

.field private final o:Lcom/google/android/apps/gmm/car/a/g;

.field private final p:Lcom/google/android/apps/gmm/z/a/b;

.field private q:Lcom/google/android/apps/gmm/navigation/i/v;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private r:Lcom/google/android/apps/gmm/navigation/g/r;

.field private s:Z

.field private t:J

.field private final u:Lcom/google/android/apps/gmm/map/internal/d/as;

.field private final v:Lcom/google/android/apps/gmm/shared/c/f;

.field private w:J

.field private final x:J

.field private y:J

.field private z:Lcom/google/n/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/car/a/g;Lcom/google/android/apps/gmm/navigation/g/h;Lcom/google/android/apps/gmm/map/internal/d/as;Lcom/google/android/apps/gmm/shared/net/a;Lcom/google/android/apps/gmm/shared/net/a/l;)V
    .locals 2

    .prologue
    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->q:Lcom/google/android/apps/gmm/navigation/i/v;

    .line 215
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->g:Z

    .line 288
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/g/m;->i:Landroid/content/Context;

    .line 289
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/g/m;->f:Lcom/google/android/apps/gmm/map/c/a;

    .line 290
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 291
    iput-object p5, p0, Lcom/google/android/apps/gmm/navigation/g/m;->j:Lcom/google/android/apps/gmm/navigation/g/h;

    .line 292
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/g/m;->p:Lcom/google/android/apps/gmm/z/a/b;

    .line 293
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/g/m;->o:Lcom/google/android/apps/gmm/car/a/g;

    .line 294
    iput-object p6, p0, Lcom/google/android/apps/gmm/navigation/g/m;->u:Lcom/google/android/apps/gmm/map/internal/d/as;

    .line 295
    iput-object p8, p0, Lcom/google/android/apps/gmm/navigation/g/m;->n:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 296
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->t:J

    .line 297
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->v:Lcom/google/android/apps/gmm/shared/c/f;

    .line 298
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/r;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/g/r;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->r:Lcom/google/android/apps/gmm/navigation/g/r;

    .line 299
    new-instance v0, Lcom/google/android/apps/gmm/navigation/logging/h;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-direct {v0, p3, v1}, Lcom/google/android/apps/gmm/navigation/logging/h;-><init>(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/shared/c/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->m:Lcom/google/android/apps/gmm/navigation/logging/h;

    .line 300
    new-instance v0, Lcom/google/android/apps/gmm/navigation/logging/e;

    invoke-direct {v0, p2}, Lcom/google/android/apps/gmm/navigation/logging/e;-><init>(Lcom/google/android/apps/gmm/shared/net/ad;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->h:Lcom/google/android/apps/gmm/navigation/logging/e;

    .line 301
    iget-object v0, p8, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v0, v0, Lcom/google/r/b/a/ou;->Q:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->w:J

    .line 302
    iget-object v0, p8, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v0, v0, Lcom/google/r/b/a/ou;->Z:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->x:J

    .line 304
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/u;

    invoke-direct {v0, p2, p7, p8}, Lcom/google/android/apps/gmm/navigation/g/u;-><init>(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/shared/net/a;Lcom/google/android/apps/gmm/shared/net/a/l;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->k:Lcom/google/android/apps/gmm/navigation/g/u;

    .line 305
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/v;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/navigation/g/v;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->a:Lcom/google/android/apps/gmm/navigation/g/v;

    .line 306
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    .line 307
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/navigation/g/s;
    .locals 3

    .prologue
    .line 1125
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/s;

    .line 1126
    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    if-ne v2, p1, :cond_0

    .line 1130
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/r/a/ae;Z)V
    .locals 6

    .prologue
    .line 997
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 998
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ae;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    .line 999
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/g/m;->n:Lcom/google/android/apps/gmm/shared/net/a/l;

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/apps/gmm/navigation/g/s;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/net/a/l;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1004
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ae;->a()Lcom/google/android/apps/gmm/map/r/a/w;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->z:I

    if-ltz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_1

    .line 1005
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->v:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->y:J

    .line 1009
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    iget v1, p1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/s;

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/gmm/navigation/g/m;->a(Lcom/google/android/apps/gmm/navigation/g/s;Z)V

    .line 1010
    return-void

    .line 1004
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/navigation/g/a/k;Lcom/google/android/apps/gmm/navigation/g/j;)V
    .locals 3

    .prologue
    .line 917
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/g/a/j;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/m;->d()Lcom/google/android/apps/gmm/navigation/g/b/i;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/gmm/navigation/g/a/j;-><init>(Lcom/google/android/apps/gmm/navigation/g/a/k;Lcom/google/android/apps/gmm/navigation/g/b/i;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 918
    if-eqz p2, :cond_0

    .line 919
    sget-object v0, Lcom/google/android/apps/gmm/navigation/g/p;->a:[I

    iget-object v1, p2, Lcom/google/android/apps/gmm/navigation/g/j;->a:Lcom/google/maps/g/a/z;

    invoke-virtual {v1}, Lcom/google/maps/g/a/z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 930
    iget-object v0, p2, Lcom/google/android/apps/gmm/navigation/g/j;->a:Lcom/google/maps/g/a/z;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Other routing failure: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 933
    :cond_0
    :pswitch_0
    return-void

    .line 919
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lcom/google/android/apps/gmm/navigation/g/j;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 809
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/g/s;->j:I

    const/4 v1, 0x2

    if-gt v0, v1, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    .line 810
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/g/s;->i:Z

    if-nez v0, :cond_6

    .line 811
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/j;->b:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 812
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 817
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 818
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 819
    const-string v0, "Received alternates. Currently navigating: "

    iget-object v1, v4, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 821
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v0

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    .line 822
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/j;->b:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    .line 823
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/m;->r:Lcom/google/android/apps/gmm/navigation/g/r;

    invoke-static {v4, v0, v5}, Lcom/google/android/apps/gmm/navigation/g/r;->a(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/gmm/navigation/g/r;->a(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_3
    if-eqz v1, :cond_4

    .line 824
    const-string v1, "  De-dup\'ing alternate: "

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_2

    :cond_0
    move v0, v3

    .line 809
    goto :goto_0

    .line 819
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move v1, v3

    .line 823
    goto :goto_3

    .line 824
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 826
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    new-instance v7, Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v8, p0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v9, p0, Lcom/google/android/apps/gmm/navigation/g/m;->n:Lcom/google/android/apps/gmm/shared/net/a/l;

    invoke-direct {v7, v0, v8, v9}, Lcom/google/android/apps/gmm/navigation/g/s;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/net/a/l;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 827
    const-string v1, "  Adding alternate: "

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_2

    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 831
    :cond_6
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/navigation/g/s;Z)V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v7, 0x0

    .line 1031
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    if-ne v0, p1, :cond_1

    .line 1071
    :cond_0
    :goto_0
    return-void

    .line 1035
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    if-eqz v0, :cond_2

    .line 1036
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iput-boolean v9, v0, Lcom/google/android/apps/gmm/navigation/g/s;->k:Z

    .line 1038
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    .line 1039
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 1045
    iget v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->z:I

    if-ltz v0, :cond_7

    move v0, v8

    :goto_1
    if-nez v0, :cond_3

    .line 1046
    iput-object v7, p0, Lcom/google/android/apps/gmm/navigation/g/m;->d:Lcom/google/android/apps/gmm/navigation/g/i;

    iput-object v7, p0, Lcom/google/android/apps/gmm/navigation/g/m;->e:Lcom/google/android/apps/gmm/navigation/g/q;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/g/m;->a()V

    .line 1048
    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/m;->C:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 1051
    :cond_3
    const-string v0, "Guiding "

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 1053
    :goto_2
    iput-object v7, p0, Lcom/google/android/apps/gmm/navigation/g/m;->B:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 1054
    iput-object v7, p0, Lcom/google/android/apps/gmm/navigation/g/m;->z:Lcom/google/n/f;

    .line 1056
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v2, Lcom/google/android/apps/gmm/navigation/g/a/h;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/m;->d()Lcom/google/android/apps/gmm/navigation/g/b/i;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/navigation/g/a/h;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/i;Lcom/google/android/apps/gmm/map/r/b/a;)V

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 1058
    if-eqz p2, :cond_4

    .line 1060
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/navigation/g/s;->a(Lcom/google/android/apps/gmm/map/r/b/a;)V

    .line 1064
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->j:Lcom/google/android/apps/gmm/navigation/g/h;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/g/h;->b:Z

    if-eqz v2, :cond_5

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/h;->a:Lcom/google/android/apps/gmm/replay/a/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/replay/a/a;->a()V

    iput-boolean v9, v0, Lcom/google/android/apps/gmm/navigation/g/h;->b:Z

    .line 1066
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->j:Lcom/google/android/apps/gmm/navigation/g/h;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    .line 1067
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    .line 1066
    iput-boolean v9, v0, Lcom/google/android/apps/gmm/navigation/g/h;->b:Z

    .line 1069
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iput-boolean v8, v0, Lcom/google/android/apps/gmm/navigation/g/s;->k:Z

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v2, :cond_9

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v2, :cond_9

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    if-nez v2, :cond_9

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/navigation/g/s;->a(D)V

    .line 1070
    :goto_3
    iget-object v10, p0, Lcom/google/android/apps/gmm/navigation/g/m;->m:Lcom/google/android/apps/gmm/navigation/logging/h;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v2, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v0, v2, :cond_b

    iput-object v1, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->c:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v0, :cond_6

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v0, :cond_a

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v0, v0

    if-lez v0, :cond_a

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v0, v0, v9

    :goto_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    :cond_6
    iget-object v0, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->e:Lcom/google/android/apps/gmm/navigation/logging/i;

    if-eqz v0, :cond_0

    iget v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->r:I

    int-to-double v0, v0

    iget-object v2, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->e:Lcom/google/android/apps/gmm/navigation/logging/i;

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, v2, Lcom/google/android/apps/gmm/navigation/logging/i;->e:I

    new-instance v0, Lcom/google/android/apps/gmm/z/e;

    iget-object v1, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->e:Lcom/google/android/apps/gmm/navigation/logging/i;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/logging/i;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->e:Lcom/google/android/apps/gmm/navigation/logging/i;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/logging/i;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->e:Lcom/google/android/apps/gmm/navigation/logging/i;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/logging/i;->c:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v4, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->e:Lcom/google/android/apps/gmm/navigation/logging/i;

    iget v4, v4, Lcom/google/android/apps/gmm/navigation/logging/i;->d:I

    iget-object v5, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->e:Lcom/google/android/apps/gmm/navigation/logging/i;

    iget v5, v5, Lcom/google/android/apps/gmm/navigation/logging/i;->e:I

    iget-object v6, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/z/e;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/r/b/a;IILcom/google/android/apps/gmm/shared/c/f;)V

    iget-object v1, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->a:Lcom/google/android/apps/gmm/z/a/b;

    new-array v2, v8, [Lcom/google/android/apps/gmm/z/b/a;

    aput-object v0, v2, v9

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/z/a/b;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    iput-object v7, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->e:Lcom/google/android/apps/gmm/navigation/logging/i;

    goto/16 :goto_0

    :cond_7
    move v0, v9

    .line 1045
    goto/16 :goto_1

    .line 1051
    :cond_8
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1069
    :cond_9
    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/navigation/g/t;->a:D

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/navigation/g/s;->b(D)V

    goto :goto_3

    :cond_a
    move-object v0, v7

    .line 1070
    goto :goto_4

    :cond_b
    iput-object v7, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->c:Lcom/google/android/apps/gmm/map/r/a/w;

    iput-object v7, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v7, v10, Lcom/google/android/apps/gmm/navigation/logging/h;->e:Lcom/google/android/apps/gmm/navigation/logging/i;

    goto/16 :goto_0
.end method

.method private b()Lcom/google/android/apps/gmm/navigation/g/s;
    .locals 10

    .prologue
    .line 538
    const/4 v1, 0x0

    .line 539
    const-wide/16 v2, 0x0

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/s;

    .line 542
    iget-wide v4, v0, Lcom/google/android/apps/gmm/navigation/g/s;->d:D

    .line 544
    if-eqz v1, :cond_0

    cmpg-double v7, v4, v2

    if-gez v7, :cond_2

    :cond_0
    move-object v2, v0

    move-wide v0, v4

    :goto_1
    move-wide v8, v0

    move-object v1, v2

    move-wide v2, v8

    .line 548
    goto :goto_0

    .line 549
    :cond_1
    return-object v1

    :cond_2
    move-wide v8, v2

    move-object v2, v1

    move-wide v0, v8

    goto :goto_1
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 557
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 558
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/s;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 560
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/m;->d()Lcom/google/android/apps/gmm/navigation/g/b/i;

    move-result-object v2

    .line 562
    if-eqz v0, :cond_0

    if-eq v0, v1, :cond_0

    .line 563
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/g/a/l;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/navigation/g/a/l;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/i;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 566
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/g/s;->i:Z

    if-eqz v0, :cond_1

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/g/a/i;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/navigation/g/a/i;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/i;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 568
    iput-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/m;->d:Lcom/google/android/apps/gmm/navigation/g/i;

    iput-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/m;->e:Lcom/google/android/apps/gmm/navigation/g/q;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/g/m;->a()V

    .line 571
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/g/a/d;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/navigation/g/a/d;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/i;Lcom/google/android/apps/gmm/map/r/b/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 574
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v1

    .line 575
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/s;

    .line 576
    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/navigation/g/s;->d:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 579
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/af;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/map/r/a/af;-><init>(Ljava/util/Map;)V

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 582
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->a:Lcom/google/android/apps/gmm/navigation/g/v;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->y:J

    .line 584
    return-void
.end method

.method private d()Lcom/google/android/apps/gmm/navigation/g/b/i;
    .locals 7

    .prologue
    const/4 v1, -0x1

    .line 1098
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1099
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [Lcom/google/android/apps/gmm/map/r/a/w;

    .line 1100
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v5, v0, [Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 1103
    const/4 v0, 0x0

    move v2, v1

    move v3, v1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1104
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/s;

    .line 1105
    iget-object v6, v0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    aput-object v6, v4, v1

    .line 1106
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/g/s;->a()Lcom/google/android/apps/gmm/navigation/g/b/k;

    move-result-object v6

    aput-object v6, v5, v1

    .line 1107
    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    if-ne v0, v6, :cond_1

    move v3, v1

    .line 1110
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/g/m;->B:Lcom/google/android/apps/gmm/map/r/a/w;

    if-ne v0, v6, :cond_2

    move v2, v1

    .line 1103
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1114
    :cond_3
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/b/j;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/g/b/j;-><init>()V

    .line 1115
    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(I[Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/map/r/a/ae;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/j;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 1116
    iput v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/j;->c:I

    .line 1117
    iput-object v5, v0, Lcom/google/android/apps/gmm/navigation/g/b/j;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 1118
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/g/b/j;->a()Lcom/google/android/apps/gmm/navigation/g/b/i;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1087
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/apps/gmm/navigation/g/m;->t:J

    .line 1088
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->q:Lcom/google/android/apps/gmm/navigation/i/v;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 1089
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->q:Lcom/google/android/apps/gmm/navigation/i/v;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/v;->a:Lcom/google/android/apps/gmm/navigation/i/x;

    const-string v2, "Searcher"

    const-string v3, "Stopping search"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/i/x;->a:Z

    .line 1090
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->q:Lcom/google/android/apps/gmm/navigation/i/v;

    .line 1092
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1088
    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 18
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 460
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v2, Lcom/google/android/apps/gmm/map/r/b/a;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 461
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    if-nez v2, :cond_1

    .line 492
    :cond_0
    :goto_0
    return-void

    .line 466
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/navigation/g/s;->i:Z

    if-nez v2, :cond_0

    .line 468
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/navigation/g/s;

    .line 469
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/navigation/g/s;->a(Lcom/google/android/apps/gmm/map/r/b/a;)V

    goto :goto_1

    .line 473
    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v2

    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/navigation/g/s;

    iget v4, v2, Lcom/google/android/apps/gmm/navigation/g/s;->j:I

    const/4 v6, 0x2

    if-gt v4, v6, :cond_4

    const/4 v4, 0x1

    :goto_3
    if-nez v4, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    if-ne v2, v4, :cond_5

    const/4 v2, 0x1

    move v3, v2

    goto :goto_2

    :cond_4
    const/4 v4, 0x0

    goto :goto_3

    :cond_5
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x46

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Dropping passed alternate route: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ". Location accuracy="

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "m."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_6
    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->t:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->v:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->t:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-lez v2, :cond_12

    :cond_7
    const/4 v2, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->C:Lcom/google/android/apps/gmm/map/r/a/w;

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->C:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v4, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v3, v4, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->q:Lcom/google/android/apps/gmm/navigation/i/v;

    if-eqz v3, :cond_13

    const/4 v3, 0x1

    :goto_5
    if-nez v3, :cond_8

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->s:Z

    if-nez v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->d:Lcom/google/android/apps/gmm/navigation/g/i;

    if-nez v3, :cond_14

    :cond_8
    const/4 v2, 0x0

    move v11, v2

    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->d:Lcom/google/android/apps/gmm/navigation/g/i;

    if-eqz v2, :cond_9

    if-eqz v11, :cond_15

    :cond_9
    const/4 v2, 0x1

    move v12, v2

    :goto_7
    if-nez v11, :cond_a

    if-eqz v12, :cond_f

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v3, Lcom/google/android/apps/gmm/navigation/g/a/e;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/navigation/g/m;->d()Lcom/google/android/apps/gmm/navigation/g/b/i;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/navigation/g/a/e;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/i;)V

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/navigation/g/m;->m:Lcom/google/android/apps/gmm/navigation/logging/h;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v0, v2, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    move-wide/from16 v16, v0

    iget-object v2, v13, Lcom/google/android/apps/gmm/navigation/logging/h;->c:Lcom/google/android/apps/gmm/map/r/a/w;

    if-eqz v2, :cond_c

    const-wide/16 v2, 0x0

    cmpl-double v2, v16, v2

    if-lez v2, :cond_c

    iget-object v2, v13, Lcom/google/android/apps/gmm/navigation/logging/h;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v2, :cond_c

    iget-object v8, v13, Lcom/google/android/apps/gmm/navigation/logging/h;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    const/4 v2, 0x1

    new-array v10, v2, [F

    invoke-virtual {v14}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v14}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v4

    iget-wide v6, v8, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v8, v8, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceBetween(DDDD[F)V

    const/4 v2, 0x0

    aget v2, v10, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_c

    iget-object v2, v13, Lcom/google/android/apps/gmm/navigation/logging/h;->c:Lcom/google/android/apps/gmm/map/r/a/w;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->t:I

    int-to-double v2, v2

    sub-double v2, v2, v16

    const-wide v4, 0x408f400000000000L    # 1000.0

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_c

    iget-object v3, v13, Lcom/google/android/apps/gmm/navigation/logging/h;->c:Lcom/google/android/apps/gmm/map/r/a/w;

    const-wide/high16 v4, 0x4039000000000000L    # 25.0

    sub-double v4, v16, v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/map/r/a/w;->a(D)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    const-wide/high16 v6, 0x4039000000000000L    # 25.0

    add-double v6, v6, v16

    invoke-virtual {v3, v6, v7}, Lcom/google/android/apps/gmm/map/r/a/w;->a(D)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    if-eqz v4, :cond_b

    if-nez v5, :cond_16

    :cond_b
    const/4 v2, 0x0

    :goto_8
    iput-object v2, v13, Lcom/google/android/apps/gmm/navigation/logging/h;->e:Lcom/google/android/apps/gmm/navigation/logging/i;

    :cond_c
    if-eqz v11, :cond_d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->i:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/g/m;->f:Lcom/google/android/apps/gmm/map/c/a;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/navigation/g/m;->u:Lcom/google/android/apps/gmm/map/internal/d/as;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->n:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/net/a/l;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/navigation/g/m;->C:Lcom/google/android/apps/gmm/map/r/a/w;

    new-instance v3, Lcom/google/android/apps/gmm/navigation/i/v;

    invoke-direct {v3, v2}, Lcom/google/android/apps/gmm/navigation/i/v;-><init>(Lcom/google/android/apps/gmm/shared/net/a/n;)V

    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v9

    new-instance v2, Lcom/google/android/apps/gmm/navigation/i/w;

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/navigation/i/w;-><init>(Lcom/google/android/apps/gmm/navigation/i/v;Landroid/content/Context;Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/internal/d/as;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/r/a/w;)V

    sget-object v4, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v9, v2, v4}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->q:Lcom/google/android/apps/gmm/navigation/i/v;

    :cond_d
    if-eqz v12, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->d:Lcom/google/android/apps/gmm/navigation/g/i;

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->d:Lcom/google/android/apps/gmm/navigation/g/i;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/navigation/g/i;->f()V

    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->k:Lcom/google/android/apps/gmm/navigation/g/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/g/m;->o:Lcom/google/android/apps/gmm/car/a/g;

    iget-boolean v5, v5, Lcom/google/android/apps/gmm/car/a/g;->a:Z

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/navigation/g/u;->a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/r/a/w;Z)Lcom/google/android/apps/gmm/navigation/g/i;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->d:Lcom/google/android/apps/gmm/navigation/g/i;

    .line 476
    :cond_f
    :goto_9
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/navigation/g/m;->c()V

    .line 479
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 480
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v4, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v3, v4, :cond_10

    .line 481
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v4, v3, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    double-to-int v3, v4

    .line 482
    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->t:I

    sub-int/2addr v2, v3

    .line 483
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->h:Lcom/google/android/apps/gmm/navigation/logging/e;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    sget-object v6, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-object v6, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->b:Lcom/google/r/b/a/vc;

    if-eqz v6, :cond_10

    iget-object v6, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->b:Lcom/google/r/b/a/vc;

    iget-boolean v6, v6, Lcom/google/r/b/a/vc;->b:Z

    if-nez v6, :cond_18

    .line 487
    :cond_10
    :goto_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v4, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v3, v4, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->v:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/gmm/navigation/g/m;->y:J

    sub-long/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->n:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v3, v3, Lcom/google/r/b/a/ou;->y:I

    int-to-long v6, v3

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/w;->n:Lcom/google/maps/g/a/fw;

    if-eqz v3, :cond_11

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-lez v3, :cond_11

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/maps/g/a/fw;)V

    .line 490
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->d:Lcom/google/android/apps/gmm/navigation/g/i;

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->e:Lcom/google/android/apps/gmm/navigation/g/q;

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget v2, v2, Lcom/google/android/apps/gmm/navigation/g/s;->j:I

    const/4 v3, 0x2

    if-gt v2, v3, :cond_20

    const/4 v2, 0x1

    :goto_b
    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v3, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->v:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->y:J

    sub-long/2addr v2, v4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->w:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->K:Lcom/google/n/f;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-boolean v3, v2, Lcom/google/android/apps/gmm/navigation/g/s;->k:Z

    if-nez v3, :cond_21

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    :goto_c
    double-to-int v5, v2

    const-string v2, "Requesting traffic update for current route and alternates and better trip. secondsUntilNextGuidance: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xb

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->k:Lcom/google/android/apps/gmm/navigation/g/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/navigation/g/m;->z:Lcom/google/n/f;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/navigation/g/m;->a:Lcom/google/android/apps/gmm/navigation/g/v;

    iget-object v7, v7, Lcom/google/android/apps/gmm/navigation/g/v;->b:Lcom/google/n/f;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/navigation/g/m;->o:Lcom/google/android/apps/gmm/car/a/g;

    iget-boolean v8, v8, Lcom/google/android/apps/gmm/car/a/g;->a:Z

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/apps/gmm/navigation/g/u;->a(Lcom/google/android/apps/gmm/navigation/g/s;Lcom/google/android/apps/gmm/map/r/b/a;ILcom/google/n/f;Lcom/google/n/f;Z)Lcom/google/android/apps/gmm/navigation/g/i;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/navigation/g/q;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-direct {v3, v2, v4}, Lcom/google/android/apps/gmm/navigation/g/q;-><init>(Lcom/google/android/apps/gmm/navigation/g/i;Lcom/google/android/apps/gmm/map/r/a/w;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->e:Lcom/google/android/apps/gmm/navigation/g/q;

    goto/16 :goto_0

    .line 473
    :cond_12
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_13
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_14
    if-eqz v2, :cond_8

    const/4 v2, 0x1

    move v11, v2

    goto/16 :goto_6

    :cond_15
    const/4 v2, 0x0

    move v12, v2

    goto/16 :goto_7

    :cond_16
    new-instance v2, Lcom/google/android/apps/gmm/navigation/logging/i;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/navigation/logging/i;-><init>()V

    iput-object v14, v2, Lcom/google/android/apps/gmm/navigation/logging/i;->c:Lcom/google/android/apps/gmm/map/r/b/a;

    iput-object v4, v2, Lcom/google/android/apps/gmm/navigation/logging/i;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v5, v2, Lcom/google/android/apps/gmm/navigation/logging/i;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Lcom/google/android/apps/gmm/map/r/a/w;->b(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v3, v4

    iput v3, v2, Lcom/google/android/apps/gmm/navigation/logging/i;->d:I

    goto/16 :goto_8

    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/navigation/g/m;->b()Lcom/google/android/apps/gmm/navigation/g/s;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/gmm/navigation/g/m;->a(Lcom/google/android/apps/gmm/navigation/g/s;Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v3

    float-to-int v3, v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x3d

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Drove onto alternate route: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ". Location accuracy="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "m."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 483
    :cond_18
    iget-object v6, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->g:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v6

    iget-boolean v8, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->f:Z

    if-nez v8, :cond_19

    iget-object v8, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->b:Lcom/google/r/b/a/vc;

    iget v8, v8, Lcom/google/r/b/a/vc;->f:I

    if-le v3, v8, :cond_10

    const/4 v8, 0x1

    iput-boolean v8, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->f:Z

    const-wide/16 v8, 0x3e8

    iget-object v10, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->b:Lcom/google/r/b/a/vc;

    iget-wide v10, v10, Lcom/google/r/b/a/vc;->h:J

    mul-long/2addr v8, v10

    add-long/2addr v8, v6

    iput-wide v8, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->d:J

    iput-wide v6, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->e:J

    sget-object v8, Lcom/google/android/apps/gmm/navigation/logging/e;->a:Ljava/lang/String;

    const-string v9, "Passed start scrubbing secion: %dm > %dm."

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v10, v11

    const/4 v3, 0x1

    iget-object v11, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->b:Lcom/google/r/b/a/vc;

    iget v11, v11, Lcom/google/r/b/a/vc;->f:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v3

    invoke-static {v8, v9, v10}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_19
    iget-wide v8, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->e:J

    cmp-long v3, v6, v8

    if-ltz v3, :cond_1e

    iget-object v3, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->b:Lcom/google/r/b/a/vc;

    iget v3, v3, Lcom/google/r/b/a/vc;->g:I

    if-le v2, v3, :cond_1e

    invoke-static {}, Lcom/google/p/a/b;->newBuilder()Lcom/google/p/a/d;

    move-result-object v3

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v2

    if-nez v2, :cond_1a

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_1a
    iget-object v8, v3, Lcom/google/p/a/d;->b:Lcom/google/n/ao;

    iget-object v9, v8, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v8, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v8, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v2, 0x1

    iput-boolean v2, v8, Lcom/google/n/ao;->d:Z

    iget v2, v3, Lcom/google/p/a/d;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v3, Lcom/google/p/a/d;->a:I

    iget-object v2, v5, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_1b

    iget-object v2, v5, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->b:Lcom/google/android/apps/gmm/map/internal/c/az;

    :goto_d
    if-eqz v2, :cond_1d

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/az;->e:I

    invoke-static {v2}, Lcom/google/android/apps/gmm/navigation/logging/e;->a(I)Lcom/google/d/a/a/mv;

    move-result-object v2

    if-nez v2, :cond_1c

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_1b
    const/4 v2, 0x0

    goto :goto_d

    :cond_1c
    iget v5, v3, Lcom/google/p/a/d;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v3, Lcom/google/p/a/d;->a:I

    iget v2, v2, Lcom/google/d/a/a/mv;->j:I

    iput v2, v3, Lcom/google/p/a/d;->c:I

    :cond_1d
    iget-object v2, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->c:Lcom/google/android/apps/gmm/shared/c/d;

    invoke-virtual {v3}, Lcom/google/p/a/d;->g()Lcom/google/n/t;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/d;->add(Ljava/lang/Object;)Z

    iget-object v2, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->b:Lcom/google/r/b/a/vc;

    iget v2, v2, Lcom/google/r/b/a/vc;->d:I

    int-to-long v2, v2

    add-long/2addr v2, v6

    iput-wide v2, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->e:J

    sget-object v2, Lcom/google/android/apps/gmm/navigation/logging/e;->a:Ljava/lang/String;

    const-string v3, "Added location to buffer."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v5}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1e
    iget-wide v2, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->d:J

    cmp-long v2, v6, v2

    if-ltz v2, :cond_10

    iget-object v2, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->c:Lcom/google/android/apps/gmm/shared/c/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/c/d;->size()I

    move-result v2

    if-lez v2, :cond_10

    invoke-static {}, Lcom/google/r/b/a/vg;->newBuilder()Lcom/google/r/b/a/vi;

    move-result-object v2

    iget-object v3, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->b:Lcom/google/r/b/a/vc;

    iget-object v3, v3, Lcom/google/r/b/a/vc;->c:Lcom/google/n/f;

    if-nez v3, :cond_1f

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_1f
    iget v5, v2, Lcom/google/r/b/a/vi;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v2, Lcom/google/r/b/a/vi;->a:I

    iput-object v3, v2, Lcom/google/r/b/a/vi;->b:Lcom/google/n/f;

    iget-object v3, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->c:Lcom/google/android/apps/gmm/shared/c/d;

    invoke-virtual {v2, v3}, Lcom/google/r/b/a/vi;->a(Ljava/lang/Iterable;)Lcom/google/r/b/a/vi;

    iget-object v3, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->g:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/net/ad;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v3

    const-class v5, Lcom/google/r/b/a/vg;

    invoke-interface {v3, v5}, Lcom/google/android/apps/gmm/shared/net/r;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/b;

    move-result-object v3

    iget-object v5, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->i:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v5, Lcom/google/android/apps/gmm/navigation/logging/e;->a:Ljava/lang/String;

    const-string v8, "Reporting batch with %d locations."

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->c:Lcom/google/android/apps/gmm/shared/c/d;

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/shared/c/d;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v5, v8, v9}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/google/r/b/a/vi;->g()Lcom/google/n/t;

    move-result-object v2

    invoke-interface {v3, v2}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/b;

    move-result-object v2

    new-instance v5, Lcom/google/android/apps/gmm/navigation/logging/g;

    invoke-direct {v5, v4, v3}, Lcom/google/android/apps/gmm/navigation/logging/g;-><init>(Lcom/google/android/apps/gmm/navigation/logging/e;Lcom/google/android/apps/gmm/shared/net/b;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v2, v5, v3}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    iget-object v2, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->c:Lcom/google/android/apps/gmm/shared/c/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/c/d;->clear()V

    const-wide/16 v2, 0x3e8

    iget-object v5, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->b:Lcom/google/r/b/a/vc;

    iget-wide v8, v5, Lcom/google/r/b/a/vc;->h:J

    mul-long/2addr v2, v8

    add-long/2addr v2, v6

    iput-wide v2, v4, Lcom/google/android/apps/gmm/navigation/logging/e;->d:J

    goto/16 :goto_a

    .line 490
    :cond_20
    const/4 v2, 0x0

    goto/16 :goto_b

    :cond_21
    iget v3, v2, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    iget-object v4, v2, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/navigation/g/t;->a:D

    iget-object v6, v2, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v6

    float-to-double v6, v6

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/gmm/navigation/g/s;->a(IDD)D

    move-result-wide v2

    goto/16 :goto_c
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/base/a/a;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 982
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->a:Lcom/google/android/apps/gmm/navigation/g/v;

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/v;->b:Lcom/google/n/f;

    iput-wide v2, v0, Lcom/google/android/apps/gmm/navigation/g/v;->c:J

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/v;->d:Lcom/google/maps/g/a/gc;

    iput-wide v2, v0, Lcom/google/android/apps/gmm/navigation/g/v;->e:J

    .line 983
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/base/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/g/m;->a(Lcom/google/android/apps/gmm/map/r/a/ae;Z)V

    .line 984
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/c;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 645
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->s:Z

    .line 647
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    if-nez v1, :cond_1

    .line 660
    :cond_0
    :goto_0
    return-void

    .line 655
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/m;->q:Lcom/google/android/apps/gmm/navigation/i/v;

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-nez v0, :cond_0

    .line 658
    sget-object v0, Lcom/google/android/apps/gmm/navigation/g/a/k;->a:Lcom/google/android/apps/gmm/navigation/g/a/k;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/g/m;->a(Lcom/google/android/apps/gmm/navigation/g/a/k;Lcom/google/android/apps/gmm/navigation/g/j;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/l;)V
    .locals 9
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 433
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/l;->a:Lcom/google/android/apps/gmm/navigation/g/i;

    .line 434
    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/g/l;->b:Lcom/google/android/apps/gmm/navigation/g/j;

    .line 443
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/g/m;->s:Z

    .line 445
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/m;->d:Lcom/google/android/apps/gmm/navigation/g/i;

    if-ne v0, v4, :cond_8

    .line 446
    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/g/j;->a:Lcom/google/maps/g/a/z;

    sget-object v4, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    if-ne v0, v4, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/g/m;->a()V

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/j;->b:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/a/ae;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    const-string v5, "New route received: "

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget v0, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    const/4 v4, -0x1

    if-eq v0, v4, :cond_3

    move v0, v2

    :goto_2
    if-nez v0, :cond_13

    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/ae;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/r/a/ae;-><init>(Ljava/util/List;I)V

    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->z:I

    if-ltz v1, :cond_4

    move v1, v2

    :goto_4
    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->a()Lcom/google/android/apps/gmm/map/r/a/w;

    move-result-object v1

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->z:I

    if-ltz v1, :cond_5

    move v1, v2

    :goto_5
    if-nez v1, :cond_6

    move v1, v2

    :goto_6
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/g/m;->a(Lcom/google/android/apps/gmm/map/r/a/ae;Z)V

    .line 447
    :goto_7
    iput-object v8, p0, Lcom/google/android/apps/gmm/navigation/g/m;->d:Lcom/google/android/apps/gmm/navigation/g/i;

    .line 456
    :goto_8
    return-void

    :cond_3
    move v0, v3

    .line 446
    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_4

    :cond_5
    move v1, v3

    goto :goto_5

    :cond_6
    move v1, v3

    goto :goto_6

    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/navigation/g/a/k;->c:Lcom/google/android/apps/gmm/navigation/g/a/k;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/g/m;->a(Lcom/google/android/apps/gmm/navigation/g/a/k;Lcom/google/android/apps/gmm/navigation/g/j;)V

    goto :goto_7

    .line 448
    :cond_8
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/m;->e:Lcom/google/android/apps/gmm/navigation/g/q;

    if-eqz v4, :cond_12

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/m;->e:Lcom/google/android/apps/gmm/navigation/g/q;

    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/g/q;->a:Lcom/google/android/apps/gmm/navigation/g/i;

    if-ne v0, v4, :cond_12

    .line 450
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/m;->e:Lcom/google/android/apps/gmm/navigation/g/q;

    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/g/j;->a:Lcom/google/maps/g/a/z;

    sget-object v5, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    if-ne v0, v5, :cond_a

    move v0, v2

    :goto_9
    if-nez v0, :cond_b

    const-wide/16 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/g/m;->w:J

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/g/m;->x:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->w:J

    .line 451
    :cond_9
    :goto_a
    iput-object v8, p0, Lcom/google/android/apps/gmm/navigation/g/m;->e:Lcom/google/android/apps/gmm/navigation/g/q;

    goto :goto_8

    :cond_a
    move v0, v3

    .line 450
    goto :goto_9

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->A:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v5, v4, Lcom/google/android/apps/gmm/navigation/g/q;->b:Lcom/google/android/apps/gmm/map/r/a/w;

    if-ne v0, v5, :cond_c

    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/g/j;->e:[Lcom/google/maps/g/a/fw;

    if-eqz v0, :cond_c

    array-length v5, v0

    if-nez v5, :cond_e

    :cond_c
    :goto_b
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/navigation/g/m;->a(Lcom/google/android/apps/gmm/navigation/g/j;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, v4, Lcom/google/android/apps/gmm/navigation/g/q;->b:Lcom/google/android/apps/gmm/map/r/a/w;

    if-ne v0, v3, :cond_d

    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/g/j;->f:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->z:Lcom/google/n/f;

    iget v0, v1, Lcom/google/android/apps/gmm/navigation/g/j;->d:I

    if-ltz v0, :cond_10

    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/g/j;->b:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/navigation/g/j;->d:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    const-string v1, "Received route around closure: "

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_f

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_c
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/g/m;->a(Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/navigation/g/s;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/navigation/g/s;->a()Lcom/google/android/apps/gmm/navigation/g/b/k;

    move-result-object v1

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/gmm/navigation/g/m;->a(Lcom/google/android/apps/gmm/navigation/g/s;Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->p:Lcom/google/android/apps/gmm/z/a/b;

    new-instance v2, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v3, Lcom/google/r/b/a/a;->h:Lcom/google/r/b/a/a;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    sget-object v3, Lcom/google/b/f/bc;->e:Lcom/google/b/f/bc;

    invoke-static {v3}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->l:Ljava/util/List;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/navigation/g/m;->a(Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/navigation/g/s;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v2, Lcom/google/android/apps/gmm/navigation/g/a/f;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/m;->d()Lcom/google/android/apps/gmm/navigation/g/b/i;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/google/android/apps/gmm/navigation/g/a/f;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/i;Lcom/google/android/apps/gmm/navigation/g/b/k;)V

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    :cond_d
    :goto_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->v:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->y:J

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->n:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v0, v0, Lcom/google/r/b/a/ou;->Q:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->w:J

    goto/16 :goto_a

    :cond_e
    array-length v5, v0

    if-ne v5, v2, :cond_c

    aget-object v0, v0, v3

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/maps/g/a/fw;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iput-object v8, v0, Lcom/google/android/apps/gmm/map/r/a/w;->o:Lcom/google/maps/g/a/gc;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->b:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v6, v5, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    invoke-virtual {v3, v6, v7}, Lcom/google/android/apps/gmm/map/r/a/w;->b(D)D

    move-result-wide v6

    iput-wide v6, v0, Lcom/google/android/apps/gmm/navigation/g/s;->d:D

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v3, Lcom/google/android/apps/gmm/navigation/g/a/o;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/m;->d()Lcom/google/android/apps/gmm/navigation/g/b/i;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/google/android/apps/gmm/navigation/g/a/o;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/i;)V

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto/16 :goto_b

    :cond_f
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_c

    :cond_10
    iget v0, v1, Lcom/google/android/apps/gmm/navigation/g/j;->c:I

    if-ltz v0, :cond_d

    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/g/j;->b:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/navigation/g/j;->c:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->B:Lcom/google/android/apps/gmm/map/r/a/w;

    const-string v0, "Received better trip: "

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/m;->B:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_11

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_e
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/g/a/g;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/m;->d()Lcom/google/android/apps/gmm/navigation/g/b/i;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/navigation/g/a/g;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/i;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_d

    :cond_11
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_e

    .line 454
    :cond_12
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x19

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Dropping route response: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    :cond_13
    move-object v0, v1

    goto/16 :goto_3
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/i/a/a;)V
    .locals 8
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 621
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->q:Lcom/google/android/apps/gmm/navigation/i/v;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 622
    iput-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/m;->q:Lcom/google/android/apps/gmm/navigation/i/v;

    .line 623
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/i/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 624
    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/i/a/a;->b:Lcom/google/android/apps/gmm/navigation/i/a/b;

    .line 626
    sget-object v3, Lcom/google/android/apps/gmm/navigation/i/a/b;->a:Lcom/google/android/apps/gmm/navigation/i/a/b;

    if-ne v2, v3, :cond_2

    .line 627
    iput-wide v6, p0, Lcom/google/android/apps/gmm/navigation/g/m;->t:J

    .line 628
    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/r/a/ae;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;)V

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/gmm/navigation/g/m;->a(Lcom/google/android/apps/gmm/map/r/a/ae;Z)V

    .line 638
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 621
    goto :goto_0

    .line 630
    :cond_2
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Finding offline route failed with status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 631
    iget-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->t:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_3

    .line 634
    sget-object v0, Lcom/google/android/apps/gmm/navigation/g/a/k;->b:Lcom/google/android/apps/gmm/navigation/g/a/k;

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/gmm/navigation/g/m;->a(Lcom/google/android/apps/gmm/navigation/g/a/k;Lcom/google/android/apps/gmm/navigation/g/j;)V

    .line 636
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->v:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->t:J

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/e;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 1014
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/e;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/g/m;->a(Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/navigation/g/s;

    move-result-object v0

    .line 1016
    if-eqz v0, :cond_0

    .line 1017
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/g/m;->a(Lcom/google/android/apps/gmm/navigation/g/s;Z)V

    .line 1019
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/util/replay/SetStateEvent;)V
    .locals 8
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 609
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/util/replay/SetStateEvent;->getUpdateTraffic()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->v:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    iget-wide v4, p0, Lcom/google/android/apps/gmm/navigation/g/m;->w:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/m;->y:J

    .line 614
    :cond_0
    return-void
.end method
