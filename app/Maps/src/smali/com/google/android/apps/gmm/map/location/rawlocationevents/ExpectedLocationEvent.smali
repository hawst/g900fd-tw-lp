.class public Lcom/google/android/apps/gmm/map/location/rawlocationevents/ExpectedLocationEvent;
.super Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/h/b;
.end annotation

.annotation runtime Lcom/google/android/apps/gmm/util/replay/c;
    a = "expected-location"
    b = .enum Lcom/google/android/apps/gmm/util/replay/d;->HIGH:Lcom/google/android/apps/gmm/util/replay/d;
.end annotation

.annotation runtime Lcom/google/android/apps/gmm/util/replay/j;
.end annotation


# direct methods
.method private constructor <init>(Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;-><init>(Landroid/location/Location;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;DDLjava/lang/Long;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;)V
    .locals 12
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "provider"
        .end annotation
    .end param
    .param p2    # D
        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "lat"
        .end annotation
    .end param
    .param p4    # D
        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "lng"
        .end annotation
    .end param
    .param p6    # Ljava/lang/Long;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "time"
        .end annotation
    .end param
    .param p7    # Ljava/lang/Double;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "altitude"
        .end annotation
    .end param
    .param p8    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "bearing"
        .end annotation
    .end param
    .param p9    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "speed"
        .end annotation
    .end param
    .param p10    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "accuracy"
        .end annotation
    .end param
    .param p11    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "numSatellites"
        .end annotation
    .end param

    .prologue
    .line 41
    const/4 v11, 0x0

    move-object v1, p1

    move-wide v2, p2

    move-wide/from16 v4, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/ExpectedLocationEvent;->buildLocation(Ljava/lang/String;DDLjava/lang/Long;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;)Landroid/location/Location;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/ExpectedLocationEvent;-><init>(Landroid/location/Location;)V

    .line 43
    return-void
.end method

.method public static fromLocation(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/location/rawlocationevents/ExpectedLocationEvent;
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/ExpectedLocationEvent;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/ExpectedLocationEvent;-><init>(Landroid/location/Location;)V

    return-object v0
.end method
