.class public Lcom/google/android/apps/gmm/map/internal/vector/gl/v;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final b:[F

.field private static final c:[F

.field private static final d:[F

.field private static final e:[F


# instance fields
.field private a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/gmm/v/co;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x14

    .line 61
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->b:[F

    .line 68
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->c:[F

    .line 75
    const/16 v0, 0xf

    new-array v0, v0, [F

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->d:[F

    .line 84
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->e:[F

    return-void

    .line 61
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 68
    :array_1
    .array-data 4
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 75
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 84
    :array_3
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        -0x3e200000    # -28.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x43800000    # 256.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x43800000    # 256.0f
        -0x3e200000    # -28.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->a:Ljava/util/HashMap;

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const-wide v8, 0x3fb015bfa0000000L    # 0.06283185631036758

    const/4 v2, 0x0

    .line 148
    invoke-static {v8, v9}, Ljava/lang/Math;->tan(D)D

    move-result-wide v6

    double-to-float v5, v6

    .line 150
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    double-to-float v6, v6

    .line 159
    invoke-interface {p0, v2, v2, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(FFF)V

    .line 160
    if-eqz p1, :cond_0

    .line 161
    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->c(I)V

    :cond_0
    move v1, v2

    move v3, v4

    .line 163
    :goto_0
    const/16 v7, 0x64

    if-ge v0, v7, :cond_2

    .line 164
    add-float v7, v3, v2

    add-float v8, v1, v2

    invoke-interface {p0, v7, v8, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(FFF)V

    .line 165
    if-eqz p1, :cond_1

    .line 166
    add-int/lit8 v7, v0, 0x1

    int-to-short v7, v7

    invoke-interface {p1, v7}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->c(I)V

    .line 171
    :cond_1
    neg-float v7, v1

    .line 175
    mul-float/2addr v7, v5

    add-float/2addr v7, v3

    .line 176
    mul-float/2addr v3, v5

    add-float/2addr v1, v3

    .line 179
    mul-float v3, v7, v6

    .line 180
    mul-float/2addr v1, v6

    .line 163
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 182
    :cond_2
    if-eqz p1, :cond_3

    .line 183
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->c(I)V

    .line 187
    :goto_1
    return-void

    .line 185
    :cond_3
    invoke-interface {p0, v4, v2, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(FFF)V

    goto :goto_1
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/gmm/v/co;
    .locals 11

    .prologue
    const/16 v6, 0x11

    const/4 v5, 0x5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/co;

    .line 196
    if-nez v0, :cond_0

    .line 197
    packed-switch p1, :pswitch_data_0

    .line 244
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 245
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    :cond_1
    return-object v0

    .line 199
    :pswitch_0
    new-instance v0, Lcom/google/android/apps/gmm/v/av;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->b:[F

    invoke-direct {v0, v1, v6, v5}, Lcom/google/android/apps/gmm/v/av;-><init>([FII)V

    goto :goto_0

    .line 204
    :pswitch_1
    new-instance v0, Lcom/google/android/apps/gmm/v/av;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->c:[F

    invoke-direct {v0, v1, v6, v5}, Lcom/google/android/apps/gmm/v/av;-><init>([FII)V

    goto :goto_0

    .line 209
    :pswitch_2
    const/16 v0, 0x64

    invoke-static {v0, v4, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-result-object v5

    .line 212
    const/4 v6, 0x0

    const-wide v8, 0x3fb015bfa0000000L    # 0.06283185631036758

    invoke-static {v8, v9}, Ljava/lang/Math;->tan(D)D

    move-result-wide v8

    double-to-float v7, v8

    const-wide v8, 0x3fb015bfa0000000L    # 0.06283185631036758

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v8, v8

    const/high16 v0, 0x3f800000    # 1.0f

    move v2, v3

    move v4, v0

    move v0, v1

    :goto_1
    const/16 v9, 0x64

    if-ge v0, v9, :cond_3

    add-float v9, v4, v3

    add-float v10, v2, v3

    invoke-interface {v5, v9, v10, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(FFF)V

    if-eqz v6, :cond_2

    int-to-short v9, v0

    invoke-interface {v6, v9}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->c(I)V

    :cond_2
    neg-float v9, v2

    mul-float/2addr v9, v7

    add-float/2addr v9, v4

    mul-float/2addr v4, v7

    add-float/2addr v2, v4

    mul-float v4, v9, v8

    mul-float/2addr v2, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 214
    :cond_3
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    .line 215
    const/4 v0, 0x2

    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v0

    .line 216
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    goto :goto_0

    .line 220
    :pswitch_3
    const/16 v0, 0x66

    invoke-static {v0, v4, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-result-object v2

    .line 223
    const/4 v0, 0x0

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;)V

    .line 225
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    .line 226
    const/4 v0, 0x6

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v0

    .line 227
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    goto :goto_0

    .line 231
    :pswitch_4
    new-instance v0, Lcom/google/android/apps/gmm/v/av;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->d:[F

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/v/av;-><init>([FII)V

    goto/16 :goto_0

    .line 237
    :pswitch_5
    new-instance v0, Lcom/google/android/apps/gmm/v/av;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->e:[F

    invoke-direct {v0, v1, v6, v5}, Lcom/google/android/apps/gmm/v/av;-><init>([FII)V

    goto/16 :goto_0

    .line 197
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
