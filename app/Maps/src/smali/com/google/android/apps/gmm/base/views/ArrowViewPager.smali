.class public Lcom/google/android/apps/gmm/base/views/ArrowViewPager;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field public a:Landroid/support/v4/view/ViewPager;

.field public b:Landroid/support/v4/view/ag;

.field c:Landroid/view/View;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field d:Landroid/view/View;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/base/views/f;

.field f:Lcom/google/android/apps/gmm/base/views/f;

.field public g:Z

.field public h:Lcom/google/android/apps/gmm/base/views/d;

.field i:Lcom/google/android/apps/gmm/base/views/e;

.field final j:Landroid/view/View$OnClickListener;

.field private final k:Landroid/view/ViewGroup$OnHierarchyChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 291
    new-instance v0, Lcom/google/android/apps/gmm/base/views/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/b;-><init>(Lcom/google/android/apps/gmm/base/views/ArrowViewPager;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->k:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    .line 312
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/c;-><init>(Lcom/google/android/apps/gmm/base/views/ArrowViewPager;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->j:Landroid/view/View$OnClickListener;

    .line 105
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a()V

    .line 106
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 291
    new-instance v0, Lcom/google/android/apps/gmm/base/views/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/b;-><init>(Lcom/google/android/apps/gmm/base/views/ArrowViewPager;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->k:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    .line 312
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/c;-><init>(Lcom/google/android/apps/gmm/base/views/ArrowViewPager;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->j:Landroid/view/View$OnClickListener;

    .line 110
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a()V

    .line 111
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 115
    new-instance v0, Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/a;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/base/views/a;-><init>(Lcom/google/android/apps/gmm/base/views/ArrowViewPager;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/bz;)V

    .line 131
    sget-object v0, Lcom/google/android/apps/gmm/base/views/e;->d:Lcom/google/android/apps/gmm/base/views/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->i:Lcom/google/android/apps/gmm/base/views/e;

    .line 133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->g:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a(I)V

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->k:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 135
    return-void
.end method

.method private static a(Landroid/view/View;FF)Z
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 211
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    move v2, v3

    .line 235
    :cond_0
    :goto_0
    return v2

    .line 216
    :cond_1
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 217
    check-cast v0, Landroid/view/ViewGroup;

    move v1, v2

    .line 218
    :goto_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 219
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 220
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_2

    .line 221
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v5, p1

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-virtual {v4}, Landroid/view/View;->getTranslationX()F

    move-result v6

    sub-float/2addr v5, v6

    .line 226
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v6, p2

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v6, v7

    invoke-virtual {v4}, Landroid/view/View;->getTranslationY()F

    move-result v7

    sub-float/2addr v6, v7

    .line 228
    cmpl-float v7, v5, v8

    if-ltz v7, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v7

    int-to-float v7, v7

    cmpg-float v7, v5, v7

    if-gtz v7, :cond_2

    cmpl-float v7, v6, v8

    if-ltz v7, :cond_2

    .line 229
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v7

    int-to-float v7, v7

    cmpg-float v7, v6, v7

    if-gtz v7, :cond_2

    .line 230
    invoke-static {v4, v5, v6}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a(Landroid/view/View;FF)Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v3

    .line 231
    goto :goto_0

    .line 218
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public a(I)V
    .locals 6

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->b:Landroid/support/v4/view/ag;

    if-nez v0, :cond_3

    move v0, v1

    .line 252
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->g:Z

    if-eqz v2, :cond_4

    if-lez p1, :cond_4

    move v2, v3

    .line 253
    :goto_1
    iget-boolean v5, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->g:Z

    if-eqz v5, :cond_5

    add-int/lit8 v5, v0, -0x1

    if-lt p1, v5, :cond_0

    if-nez v0, :cond_5

    .line 254
    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->c:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 255
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->c:Landroid/view/View;

    if-eqz v2, :cond_6

    move v0, v1

    :goto_3
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->d:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->d:Landroid/view/View;

    if-eqz v3, :cond_7

    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 260
    :cond_2
    return-void

    .line 251
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->b:Landroid/support/v4/view/ag;

    invoke-virtual {v0}, Landroid/support/v4/view/ag;->a()I

    move-result v0

    goto :goto_0

    :cond_4
    move v2, v1

    .line 252
    goto :goto_1

    :cond_5
    move v3, v1

    .line 253
    goto :goto_2

    :cond_6
    move v0, v4

    .line 255
    goto :goto_3

    :cond_7
    move v1, v4

    .line 258
    goto :goto_4
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 176
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 177
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a(Landroid/view/View;FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    const/4 v0, 0x0

    .line 200
    :goto_0
    return v0

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->e:Lcom/google/android/apps/gmm/base/views/f;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/base/views/f;->a(Lcom/google/android/apps/gmm/base/views/f;Landroid/view/MotionEvent;)Z

    move-result v0

    .line 182
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->f:Lcom/google/android/apps/gmm/base/views/f;

    invoke-static {v1, p1}, Lcom/google/android/apps/gmm/base/views/f;->a(Lcom/google/android/apps/gmm/base/views/f;Landroid/view/MotionEvent;)Z

    move-result v1

    .line 183
    if-nez v0, :cond_1

    if-eqz v1, :cond_4

    .line 184
    :cond_1
    if-eqz v0, :cond_3

    .line 185
    sget-object v0, Lcom/google/android/apps/gmm/base/views/e;->b:Lcom/google/android/apps/gmm/base/views/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->i:Lcom/google/android/apps/gmm/base/views/e;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 191
    :goto_1
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 198
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 199
    sget-object v1, Lcom/google/android/apps/gmm/base/views/e;->d:Lcom/google/android/apps/gmm/base/views/e;

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->i:Lcom/google/android/apps/gmm/base/views/e;

    goto :goto_0

    .line 187
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/base/views/e;->c:Lcom/google/android/apps/gmm/base/views/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->i:Lcom/google/android/apps/gmm/base/views/e;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_1

    .line 192
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 193
    sget-object v0, Lcom/google/android/apps/gmm/base/views/e;->a:Lcom/google/android/apps/gmm/base/views/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->i:Lcom/google/android/apps/gmm/base/views/e;

    goto :goto_2
.end method
