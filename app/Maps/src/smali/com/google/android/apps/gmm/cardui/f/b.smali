.class public Lcom/google/android/apps/gmm/cardui/f/b;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/o/h/a/hv;Lcom/google/o/h/a/a;)Lcom/google/o/h/a/iv;
    .locals 8
    .param p0    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/o/h/a/hv;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/o/h/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 51
    invoke-static {}, Lcom/google/o/h/a/gp;->newBuilder()Lcom/google/o/h/a/gr;

    move-result-object v0

    if-eqz p0, :cond_1

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/o/h/a/gr;->c()V

    iget-object v1, v0, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    invoke-interface {v1, p0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz p1, :cond_3

    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    invoke-virtual {v0}, Lcom/google/o/h/a/gr;->d()V

    iget-object v1, v0, Lcom/google/o/h/a/gr;->c:Lcom/google/n/aq;

    invoke-interface {v1, p1}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    :cond_3
    if-eqz p2, :cond_4

    invoke-virtual {v0, p2}, Lcom/google/o/h/a/gr;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/gr;

    :cond_4
    if-eqz p3, :cond_5

    invoke-virtual {v0, p3}, Lcom/google/o/h/a/gr;->a(Lcom/google/o/h/a/a;)Lcom/google/o/h/a/gr;

    :cond_5
    invoke-static {}, Lcom/google/o/h/a/jf;->newBuilder()Lcom/google/o/h/a/jh;

    move-result-object v1

    sget-object v2, Lcom/google/o/h/a/ji;->b:Lcom/google/o/h/a/ji;

    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    iget v3, v1, Lcom/google/o/h/a/jh;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/google/o/h/a/jh;->a:I

    iget v2, v2, Lcom/google/o/h/a/ji;->ah:I

    iput v2, v1, Lcom/google/o/h/a/jh;->b:I

    invoke-static {}, Lcom/google/o/h/a/iv;->newBuilder()Lcom/google/o/h/a/ix;

    move-result-object v2

    invoke-static {}, Lcom/google/o/h/a/ir;->newBuilder()Lcom/google/o/h/a/it;

    move-result-object v3

    iget-object v4, v3, Lcom/google/o/h/a/it;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/h/a/gr;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v4, Lcom/google/n/ao;->d:Z

    iget v0, v3, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v3, Lcom/google/o/h/a/it;->a:I

    iget-object v0, v2, Lcom/google/o/h/a/ix;->b:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/o/h/a/it;->g()Lcom/google/n/t;

    move-result-object v3

    iget-object v4, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/o/h/a/ix;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/o/h/a/ix;->a:I

    invoke-virtual {v2}, Lcom/google/o/h/a/ix;->c()V

    iget-object v0, v2, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    invoke-virtual {v1}, Lcom/google/o/h/a/jh;->g()Lcom/google/n/t;

    move-result-object v1

    new-instance v3, Lcom/google/n/ao;

    invoke-direct {v3}, Lcom/google/n/ao;-><init>()V

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v3, Lcom/google/n/ao;->d:Z

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lcom/google/o/h/a/ix;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    return-object v0
.end method
