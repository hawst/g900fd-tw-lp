.class Lcom/google/android/apps/gmm/streetview/internal/ad;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/streetview/internal/ac;

.field private final b:Lcom/google/android/apps/gmm/streetview/internal/ae;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/streetview/internal/ac;Lcom/google/android/apps/gmm/streetview/internal/ae;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->a:Lcom/google/android/apps/gmm/streetview/internal/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p2, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->b:Lcom/google/android/apps/gmm/streetview/internal/ae;

    .line 111
    iput-object p3, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->c:Ljava/lang/String;

    .line 112
    const-string v0, "persistentKey"

    if-nez p3, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 113
    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 119
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    .line 130
    const-string v0, "PanoramaManager.AsyncCacheRequest"

    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->a:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/ac;->a:Lcom/google/android/apps/gmm/streetview/internal/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->c:Ljava/lang/String;

    const-wide/32 v4, 0x6ddd00

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v6, 0x5

    if-ge v3, v6, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "persistentKey"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->b:Lcom/google/android/apps/gmm/streetview/internal/ae;

    invoke-interface {v0, v8, v1}, Lcom/google/android/apps/gmm/streetview/internal/ae;->a(ZLcom/google/android/apps/gmm/streetview/internal/t;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->a:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/ac;->a(Ljava/lang/Object;)V

    .line 156
    :goto_0
    return-void

    .line 132
    :cond_0
    const-wide/16 v6, 0x1

    cmp-long v3, v4, v6

    if-gez v3, :cond_1

    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "shelfLife"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146
    :catch_1
    move-exception v0

    move-object v0, v1

    :goto_1
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->b:Lcom/google/android/apps/gmm/streetview/internal/ae;

    const/4 v2, 0x1

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/streetview/internal/ae;->a(ZLcom/google/android/apps/gmm/streetview/internal/t;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->a:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/ac;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 132
    :cond_1
    :try_start_3
    iget-object v3, v0, Lcom/google/android/apps/gmm/streetview/internal/g;->a:Ljava/io/File;

    if-eqz v3, :cond_5

    if-eqz v2, :cond_5

    invoke-virtual {v0, v2, v4, v5}, Lcom/google/android/apps/gmm/streetview/internal/g;->b(Ljava/lang/String;J)[B

    move-result-object v0

    if-eqz v0, :cond_3

    .line 134
    :goto_2
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 136
    :try_start_4
    invoke-static {v2}, Lcom/google/android/apps/gmm/streetview/internal/t;->a(Ljava/io/InputStream;)Lcom/google/android/apps/gmm/streetview/internal/t;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v0

    .line 138
    if-eqz v2, :cond_2

    :try_start_5
    invoke-interface {v2}, Ljava/io/Closeable;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 154
    :cond_2
    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->b:Lcom/google/android/apps/gmm/streetview/internal/ae;

    invoke-interface {v1, v8, v0}, Lcom/google/android/apps/gmm/streetview/internal/ae;->a(ZLcom/google/android/apps/gmm/streetview/internal/t;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->a:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/ac;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 132
    :cond_3
    :try_start_6
    const-string v0, "read"

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Ljava/lang/InterruptedException;

    invoke-direct {v2, v0}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 154
    :catchall_0
    move-exception v0

    :goto_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->b:Lcom/google/android/apps/gmm/streetview/internal/ae;

    invoke-interface {v2, v8, v1}, Lcom/google/android/apps/gmm/streetview/internal/ae;->a(ZLcom/google/android/apps/gmm/streetview/internal/t;)V

    .line 155
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->a:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ad;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/streetview/internal/ac;->a(Ljava/lang/Object;)V

    .line 156
    throw v0

    :cond_4
    move-object v0, v1

    .line 132
    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_2

    .line 138
    :catchall_1
    move-exception v0

    if-eqz v2, :cond_6

    :try_start_7
    invoke-interface {v2}, Ljava/io/Closeable;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_6
    :goto_5
    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v2

    goto :goto_5

    .line 154
    :catchall_2
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    goto :goto_4

    .line 146
    :catch_4
    move-exception v2

    goto :goto_1
.end method
