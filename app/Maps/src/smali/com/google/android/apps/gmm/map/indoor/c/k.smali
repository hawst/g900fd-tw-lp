.class Lcom/google/android/apps/gmm/map/indoor/c/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field final synthetic b:Lcom/google/android/apps/gmm/map/b/a/ai;

.field final synthetic c:Lcom/google/android/apps/gmm/map/indoor/c/o;

.field final synthetic d:Lcom/google/android/apps/gmm/map/indoor/c/j;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/indoor/c/j;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/indoor/c/o;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/indoor/c/k;->d:Lcom/google/android/apps/gmm/map/indoor/c/j;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/indoor/c/k;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iput-object p3, p0, Lcom/google/android/apps/gmm/map/indoor/c/k;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iput-object p4, p0, Lcom/google/android/apps/gmm/map/indoor/c/k;->c:Lcom/google/android/apps/gmm/map/indoor/c/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 129
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/k;->d:Lcom/google/android/apps/gmm/map/indoor/c/j;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/k;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/k;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/indoor/c/k;->c:Lcom/google/android/apps/gmm/map/indoor/c/o;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/indoor/c/j;->a:Lcom/google/android/apps/gmm/map/util/a/l;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/util/a/l;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    invoke-interface {v4, v2, v6, v0}, Lcom/google/android/apps/gmm/map/indoor/c/o;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILjava/util/Collection;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/indoor/c/j;->a(Ljava/util/Collection;)V

    .line 130
    :goto_0
    return-void

    .line 129
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/c/l;

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/indoor/c/l;-><init>(Lcom/google/android/apps/gmm/map/indoor/c/j;Lcom/google/android/apps/gmm/map/indoor/c/o;)V

    new-instance v4, Lcom/google/android/apps/gmm/map/indoor/c/p;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/map/indoor/c/p;-><init>(Lcom/google/android/apps/gmm/map/indoor/c/o;)V

    iget-object v5, v1, Lcom/google/android/apps/gmm/map/indoor/c/j;->d:Lcom/google/android/apps/gmm/map/internal/d/bd;

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v0, :cond_1

    invoke-virtual {v5, v3}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v0

    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/d;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/indoor/c/j;->d:Lcom/google/android/apps/gmm/map/internal/d/bd;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v0, :cond_2

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v0

    :cond_2
    invoke-interface {v0, v2, v4}, Lcom/google/android/apps/gmm/map/internal/d/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    goto :goto_0

    :cond_3
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/indoor/c/j;->d:Lcom/google/android/apps/gmm/map/internal/d/bd;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v0, :cond_4

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v0

    :cond_4
    invoke-interface {v0, v2, v4, v6}, Lcom/google/android/apps/gmm/map/internal/d/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Z)V

    goto :goto_0
.end method
