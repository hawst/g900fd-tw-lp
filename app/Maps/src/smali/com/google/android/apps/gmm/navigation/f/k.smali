.class public Lcom/google/android/apps/gmm/navigation/f/k;
.super Lcom/google/android/apps/gmm/navigation/f/i;
.source "PG"


# instance fields
.field private final d:Lcom/google/android/apps/gmm/o/a/c;

.field private final e:I

.field private f:Ljava/lang/Boolean;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/f/q;Lcom/google/android/apps/gmm/navigation/f/r;Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/Context;Lcom/google/android/apps/gmm/navigation/c/f;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/o/a/c;)V
    .locals 6

    .prologue
    .line 157
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/f/i;-><init>(Lcom/google/android/apps/gmm/navigation/f/q;Lcom/google/android/apps/gmm/navigation/f/r;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/navigation/c/f;Lcom/google/android/apps/gmm/map/t;)V

    .line 162
    const-string v0, "layersController"

    if-nez p7, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p7, Lcom/google/android/apps/gmm/o/a/c;

    iput-object p7, p0, Lcom/google/android/apps/gmm/navigation/f/k;->d:Lcom/google/android/apps/gmm/o/a/c;

    .line 164
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    .line 166
    const/high16 v1, 0x42900000    # 72.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/f/k;->e:I

    .line 167
    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/k;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v0, v3, :cond_2

    :cond_0
    move v0, v2

    .line 216
    :goto_0
    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/f/k;->e:I

    .line 217
    :goto_1
    new-instance v3, Lcom/google/android/apps/gmm/directions/f/a/c;

    invoke-direct {v3, v1, v1, v1, v0}, Lcom/google/android/apps/gmm/directions/f/a/c;-><init>(IIII)V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/k;->b:Lcom/google/android/apps/gmm/navigation/c/f;

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/c/a;->f:Lcom/google/android/apps/gmm/directions/f/a/c;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/directions/f/a/c;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/c/a;->f:Lcom/google/android/apps/gmm/directions/f/a/c;

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/apps/gmm/navigation/c/a;->a(ZZZ)V

    .line 219
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 215
    goto :goto_0

    :cond_3
    move v0, v1

    .line 216
    goto :goto_1
.end method


# virtual methods
.method public final G_()V
    .locals 2

    .prologue
    .line 183
    invoke-super {p0}, Lcom/google/android/apps/gmm/navigation/f/i;->G_()V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/k;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/k;->d:Lcom/google/android/apps/gmm/o/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/f/k;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/o/a/c;->a(Z)Z

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/k;->f:Ljava/lang/Boolean;

    .line 188
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 177
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/navigation/f/i;->a(Landroid/content/res/Configuration;)V

    .line 178
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/f/k;->a(Z)V

    .line 179
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/f/t;)V
    .locals 5
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 194
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/f/k;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    .line 195
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/navigation/f/i;->a(Lcom/google/android/apps/gmm/navigation/f/t;)V

    .line 198
    iget-object v4, p1, Lcom/google/android/apps/gmm/navigation/f/t;->a:Lcom/google/android/apps/gmm/navigation/f/b/a;

    .line 199
    iget-object v0, v4, Lcom/google/android/apps/gmm/navigation/f/b/a;->e:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    .line 200
    iget-object v0, v4, Lcom/google/android/apps/gmm/navigation/f/b/a;->e:Lcom/google/android/apps/gmm/navigation/g/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/d;->d:Lcom/google/maps/g/a/hm;

    sget-object v4, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/k;->f:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/k;->d:Lcom/google/android/apps/gmm/o/a/c;

    sget-object v4, Lcom/google/android/apps/gmm/o/a/a;->a:Lcom/google/android/apps/gmm/o/a/a;

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/o/a/c;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/k;->f:Ljava/lang/Boolean;

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/k;->d:Lcom/google/android/apps/gmm/o/a/c;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/o/a/c;->a(Z)Z

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/k;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v0, v3, :cond_1

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/k;->b:Lcom/google/android/apps/gmm/navigation/c/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/navigation/c/f;->a(Lcom/google/android/apps/gmm/navigation/f/t;)V

    .line 208
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/navigation/f/k;->a(Z)V

    .line 210
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 199
    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 171
    invoke-super {p0}, Lcom/google/android/apps/gmm/navigation/f/i;->b()V

    .line 172
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/f/k;->a(Z)V

    .line 173
    return-void
.end method
