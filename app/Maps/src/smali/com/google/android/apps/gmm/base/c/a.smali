.class public final Lcom/google/android/apps/gmm/base/c/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field public final a:[F

.field private final b:Lcom/google/android/apps/gmm/map/b/a/a;

.field private final c:Lcom/google/android/apps/gmm/map/b/a/a;


# direct methods
.method private constructor <init>(FFFF)V
    .locals 10

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/16 v0, 0x19

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/c/a;->a:[F

    .line 75
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/a;

    const-wide/16 v2, 0x0

    float-to-double v4, p1

    float-to-double v6, p3

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/b/a/a;-><init>(DDDD)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/c/a;->b:Lcom/google/android/apps/gmm/map/b/a/a;

    .line 76
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/a;

    const-wide/16 v2, 0x0

    float-to-double v4, p2

    float-to-double v6, p4

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/b/a/a;-><init>(DDDD)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/c/a;->c:Lcom/google/android/apps/gmm/map/b/a/a;

    .line 78
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/c/a;->a:[F

    const/16 v1, 0x19

    if-ge v0, v1, :cond_0

    .line 79
    int-to-float v1, v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/c/a;->a:[F

    const/high16 v2, 0x41c00000    # 24.0f

    div-float/2addr v1, v2

    .line 80
    const/16 v2, 0xa

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/apps/gmm/base/c/a;->a(FIZ)F

    move-result v1

    .line 81
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/c/a;->a:[F

    aput v1, v2, v0

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_0
    return-void
.end method

.method private a(FIZ)F
    .locals 16

    .prologue
    .line 87
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/c/a;->a:[F

    const/high16 v2, 0x41c00000    # 24.0f

    mul-float v2, v2, p1

    float-to-int v2, v2

    .line 88
    if-eqz p3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/c/a;->a:[F

    aget v13, v3, v2

    .line 89
    :goto_0
    if-eqz p3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/c/a;->a:[F

    const/16 v3, 0x18

    if-ge v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/c/a;->a:[F

    add-int/lit8 v2, v2, 0x1

    aget v2, v3, v2

    move v12, v2

    .line 91
    :goto_1
    const/4 v2, 0x0

    move v14, v2

    :goto_2
    move/from16 v0, p2

    if-ge v14, v0, :cond_4

    .line 92
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/base/c/a;->b:Lcom/google/android/apps/gmm/map/b/a/a;

    float-to-double v2, v13

    iget-wide v4, v10, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v6, v10, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v8, v10, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v10, v10, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v2

    double-to-float v2, v2

    sub-float v15, v2, p1

    .line 93
    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v3, 0x38d1b717    # 1.0E-4f

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_2

    move v2, v13

    .line 106
    :goto_3
    return v2

    :cond_0
    move/from16 v13, p1

    .line 88
    goto :goto_0

    .line 89
    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    move v12, v2

    goto :goto_1

    .line 96
    :cond_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/base/c/a;->b:Lcom/google/android/apps/gmm/map/b/a/a;

    float-to-double v2, v13

    iget-wide v4, v10, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v6, v10, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v8, v10, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v10, v10, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->b(DDDDD)D

    move-result-wide v2

    double-to-float v2, v2

    .line 97
    const/4 v3, 0x0

    cmpl-float v3, v2, v3

    if-eqz v3, :cond_3

    .line 98
    div-float v2, v15, v2

    sub-float v2, v13, v2

    .line 91
    :goto_4
    add-int/lit8 v3, v14, 0x1

    move v14, v3

    move v13, v2

    goto :goto_2

    .line 102
    :cond_3
    sub-float v2, v12, v13

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v2, v13

    goto :goto_4

    :cond_4
    move v2, v13

    .line 106
    goto :goto_3
.end method

.method public static a(FFFF)Landroid/view/animation/Interpolator;
    .locals 4

    .prologue
    .line 50
    const/4 v0, 0x0

    .line 52
    :try_start_0
    sget-boolean v1, Lcom/google/android/apps/gmm/map/util/c;->c:Z

    if-eqz v1, :cond_0

    .line 54
    const-string v0, "android.view.animation.PathInterpolator"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 55
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 57
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Interpolator;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    :cond_0
    if-nez v0, :cond_1

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/base/c/a;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/c/a;-><init>(FFFF)V

    .line 67
    :cond_1
    :goto_0
    return-object v0

    .line 62
    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/apps/gmm/base/c/a;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/c/a;-><init>(FFFF)V

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 63
    new-instance v1, Lcom/google/android/apps/gmm/base/c/a;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/c/a;-><init>(FFFF)V

    throw v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 118
    if-ne p1, p0, :cond_1

    .line 127
    :cond_0
    :goto_0
    return v0

    .line 122
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/base/c/a;

    if-nez v2, :cond_2

    move v0, v1

    .line 123
    goto :goto_0

    .line 126
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/base/c/a;

    .line 127
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/c/a;->b:Lcom/google/android/apps/gmm/map/b/a/a;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/c/a;->b:Lcom/google/android/apps/gmm/map/b/a/a;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/c/a;->c:Lcom/google/android/apps/gmm/map/b/a/a;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/c/a;->c:Lcom/google/android/apps/gmm/map/b/a/a;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final getInterpolation(F)F
    .locals 10

    .prologue
    .line 113
    iget-object v8, p0, Lcom/google/android/apps/gmm/base/c/a;->c:Lcom/google/android/apps/gmm/map/b/a/a;

    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/gmm/base/c/a;->a(FIZ)F

    move-result v0

    float-to-double v0, v0

    iget-wide v2, v8, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v4, v8, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v6, v8, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v8, v8, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 132
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/c/a;->b:Lcom/google/android/apps/gmm/map/b/a/a;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/c/a;->c:Lcom/google/android/apps/gmm/map/b/a/a;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
