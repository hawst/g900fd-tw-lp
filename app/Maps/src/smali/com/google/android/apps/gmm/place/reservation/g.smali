.class public Lcom/google/android/apps/gmm/place/reservation/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/reservation/b/c;
.implements Lcom/google/android/apps/gmm/shared/net/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/place/br;",
        "Lcom/google/android/apps/gmm/place/reservation/b/c;",
        "Lcom/google/android/apps/gmm/shared/net/c",
        "<",
        "Lcom/google/r/b/a/z;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/activities/c;

.field public b:Lcom/google/android/apps/gmm/base/g/c;

.field public c:Z

.field public d:Z

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/e;",
            ">;"
        }
    .end annotation
.end field

.field f:Lcom/google/android/apps/gmm/place/reservation/b/b;

.field g:Lcom/google/android/apps/gmm/place/reservation/b/e;

.field h:Lcom/google/android/apps/gmm/z/b/l;

.field private final i:Lcom/google/android/apps/gmm/base/l/a/m;

.field private j:Landroid/content/Context;

.field private k:Lcom/google/android/apps/gmm/shared/net/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<",
            "Lcom/google/r/b/a/v;",
            "Lcom/google/r/b/a/z;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/google/maps/g/q;

.field private m:Z

.field private n:Ljava/lang/CharSequence;

.field private o:Lcom/google/r/b/a/v;

.field private p:Ljava/lang/String;

.field private q:Landroid/app/AlertDialog;

.field private r:Landroid/app/AlertDialog;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/Integer;

.field private u:Landroid/content/DialogInterface$OnClickListener;

.field private v:Lcom/google/android/apps/gmm/z/b/l;

.field private w:Lcom/google/android/apps/gmm/z/b/l;

.field private x:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Z)V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->e:Ljava/util/List;

    .line 93
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 94
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/place/reservation/g;->d:Z

    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/reservation/h;-><init>(Lcom/google/android/apps/gmm/place/reservation/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->i:Lcom/google/android/apps/gmm/base/l/a/m;

    .line 110
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/place/reservation/g;)V
    .locals 6

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->l:Lcom/google/maps/g/q;

    invoke-virtual {v1}, Lcom/google/maps/g/q;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/g;->f:Lcom/google/android/apps/gmm/place/reservation/b/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/reservation/b/b;->g()Ljava/util/Date;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/g;->g:Lcom/google/android/apps/gmm/place/reservation/b/e;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/reservation/b/e;->f()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/place/reservation/g;->x:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/place/reservation/ag;-><init>(Ljava/lang/String;Ljava/util/Date;IZ)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->F()Lcom/google/android/apps/gmm/place/reservation/ai;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/reservation/ai;->b:Lcom/google/android/apps/gmm/place/reservation/ah;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    new-instance v2, Ljava/util/Date;

    iget-object v3, v1, Lcom/google/android/apps/gmm/place/reservation/ah;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v2, v0, Lcom/google/android/apps/gmm/place/reservation/ag;->e:Ljava/util/Date;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/reservation/ag;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/reservation/ah;->c:Ljava/util/Map;

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/reservation/ag;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/place/reservation/b/b;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->f:Lcom/google/android/apps/gmm/place/reservation/b/b;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 115
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->p()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->q()Lcom/google/maps/g/q;

    move-result-object v0

    iget v0, v0, Lcom/google/maps/g/q;->d:I

    invoke-static {v0}, Lcom/google/maps/g/s;->a(I)Lcom/google/maps/g/s;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/s;->a:Lcom/google/maps/g/s;

    :cond_0
    sget-object v1, Lcom/google/maps/g/s;->a:Lcom/google/maps/g/s;

    if-ne v0, v1, :cond_2

    move v0, v5

    :goto_0
    if-nez v0, :cond_3

    .line 178
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v4

    .line 115
    goto :goto_0

    .line 119
    :cond_3
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->j:Landroid/content/Context;

    .line 120
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->b:Lcom/google/android/apps/gmm/base/g/c;

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->b:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->q()Lcom/google/maps/g/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->l:Lcom/google/maps/g/q;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->lv:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/gmm/place/reservation/g;->l:Lcom/google/maps/g/q;

    .line 123
    invoke-virtual {v6}, Lcom/google/maps/g/q;->g()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v4

    .line 122
    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->p:Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->k:Lcom/google/android/apps/gmm/shared/net/b;

    if-nez v0, :cond_4

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    const-class v1, Lcom/google/r/b/a/v;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->k:Lcom/google/android/apps/gmm/shared/net/b;

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->k:Lcom/google/android/apps/gmm/shared/net/b;

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 131
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->l:Lcom/google/maps/g/q;

    sget-object v1, Lcom/google/b/f/t;->du:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/reservation/ae;->a(Lcom/google/maps/g/q;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->v:Lcom/google/android/apps/gmm/z/b/l;

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->l:Lcom/google/maps/g/q;

    sget-object v1, Lcom/google/b/f/t;->dQ:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/reservation/ae;->a(Lcom/google/maps/g/q;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->h:Lcom/google/android/apps/gmm/z/b/l;

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->l:Lcom/google/maps/g/q;

    sget-object v1, Lcom/google/b/f/t;->dU:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/reservation/ae;->a(Lcom/google/maps/g/q;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->w:Lcom/google/android/apps/gmm/z/b/l;

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->u:Landroid/content/DialogInterface$OnClickListener;

    if-nez v0, :cond_5

    .line 140
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/reservation/i;-><init>(Lcom/google/android/apps/gmm/place/reservation/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->u:Landroid/content/DialogInterface$OnClickListener;

    .line 153
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->l:Lcom/google/maps/g/q;

    .line 154
    iget-object v1, v0, Lcom/google/maps/g/q;->h:Lcom/google/maps/g/u;

    if-nez v1, :cond_7

    invoke-static {}, Lcom/google/maps/g/u;->d()Lcom/google/maps/g/u;

    move-result-object v0

    :goto_2
    iget-object v1, v0, Lcom/google/maps/g/u;->b:Lcom/google/maps/g/x;

    if-nez v1, :cond_8

    invoke-static {}, Lcom/google/maps/g/x;->d()Lcom/google/maps/g/x;

    move-result-object v0

    move-object v1, v0

    .line 155
    :goto_3
    iget-object v0, v1, Lcom/google/maps/g/x;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    :goto_4
    invoke-static {v0}, Lcom/google/android/apps/gmm/place/reservation/ae;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    if-nez v0, :cond_b

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v2, 0xb

    const/16 v6, 0x13

    invoke-virtual {v0, v2, v6}, Ljava/util/Calendar;->set(II)V

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v4}, Ljava/util/Calendar;->set(II)V

    const/16 v2, 0xd

    invoke-virtual {v0, v2, v4}, Ljava/util/Calendar;->set(II)V

    const/16 v2, 0xe

    invoke-virtual {v0, v2, v4}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    move-object v2, v0

    .line 157
    :goto_5
    iget v1, v1, Lcom/google/maps/g/x;->b:I

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 160
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->F()Lcom/google/android/apps/gmm/place/reservation/ai;

    move-result-object v0

    .line 161
    iget-object v6, v0, Lcom/google/android/apps/gmm/place/reservation/ai;->b:Lcom/google/android/apps/gmm/place/reservation/ah;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->l:Lcom/google/maps/g/q;

    .line 162
    invoke-virtual {v0}, Lcom/google/maps/g/q;->d()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_6

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_c

    :cond_6
    move v0, v5

    :goto_6
    if-eqz v0, :cond_d

    move-object v0, v3

    .line 163
    :goto_7
    if-eqz v0, :cond_e

    .line 164
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/reservation/ag;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->x:Z

    .line 165
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/reservation/ag;->b:Ljava/util/Date;

    .line 166
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/reservation/ag;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 171
    :goto_8
    new-instance v4, Lcom/google/android/apps/gmm/place/reservation/c;

    invoke-direct {v4, p1, v2, v1}, Lcom/google/android/apps/gmm/place/reservation/c;-><init>(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)V

    iput-object v4, p0, Lcom/google/android/apps/gmm/place/reservation/g;->f:Lcom/google/android/apps/gmm/place/reservation/b/b;

    .line 172
    iput-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/g;->q:Landroid/app/AlertDialog;

    .line 174
    new-instance v1, Lcom/google/android/apps/gmm/place/reservation/k;

    invoke-direct {v1, p1, v0}, Lcom/google/android/apps/gmm/place/reservation/k;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->g:Lcom/google/android/apps/gmm/place/reservation/b/e;

    .line 175
    iput-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/g;->r:Landroid/app/AlertDialog;

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->b:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->d:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->c:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/reservation/g;->j()V

    iput-boolean v5, p0, Lcom/google/android/apps/gmm/place/reservation/g;->c:Z

    goto/16 :goto_1

    .line 154
    :cond_7
    iget-object v0, v0, Lcom/google/maps/g/q;->h:Lcom/google/maps/g/u;

    goto/16 :goto_2

    :cond_8
    iget-object v0, v0, Lcom/google/maps/g/u;->b:Lcom/google/maps/g/x;

    move-object v1, v0

    goto/16 :goto_3

    .line 155
    :cond_9
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_a

    iput-object v2, v1, Lcom/google/maps/g/x;->c:Ljava/lang/Object;

    :cond_a
    move-object v0, v2

    goto/16 :goto_4

    :cond_b
    move-object v2, v0

    goto :goto_5

    :cond_c
    move v0, v4

    .line 162
    goto :goto_6

    :cond_d
    iget-object v0, v6, Lcom/google/android/apps/gmm/place/reservation/ah;->c:Ljava/util/Map;

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/reservation/ag;

    goto :goto_7

    :cond_e
    move v0, v1

    move-object v1, v2

    goto :goto_8
.end method

.method public final synthetic a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 4

    .prologue
    .line 48
    check-cast p1, Lcom/google/r/b/a/z;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->m:Z

    if-eqz p2, :cond_1

    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/d;->b()Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->lw:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->n:Ljava/lang/CharSequence;

    :cond_0
    :goto_0
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    return-void

    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p1, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ad;->d()Lcom/google/r/b/a/ad;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ad;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/place/reservation/ae;->a(Ljava/util/List;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->n:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->n:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->lw:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->n:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/r/b/a/z;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/j;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/g;->e:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/place/reservation/j;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/gmm/place/reservation/j;-><init>(Lcom/google/android/apps/gmm/place/reservation/g;Lcom/google/r/b/a/j;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->n:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/gmm/base/l/a/m;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->i:Lcom/google/android/apps/gmm/base/l/a/m;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->b:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_0

    .line 260
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 262
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final f()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 272
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->f:Lcom/google/android/apps/gmm/place/reservation/b/b;

    if-eqz v0, :cond_2

    .line 278
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->w:Lcom/google/android/apps/gmm/z/b/l;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/reservation/g;->n()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->q:Landroid/app/AlertDialog;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/place/reservation/a/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/g;->j:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/gmm/l;->lx:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/gmm/l;->fW:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/g;->u:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->q:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->f:Lcom/google/android/apps/gmm/place/reservation/b/b;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->q:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 281
    :cond_2
    return-object v4
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->g:Lcom/google/android/apps/gmm/place/reservation/b/e;

    if-eqz v0, :cond_2

    .line 287
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->w:Lcom/google/android/apps/gmm/z/b/l;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/reservation/g;->n()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->r:Landroid/app/AlertDialog;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/place/reservation/a/b;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/g;->j:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/gmm/l;->ly:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/gmm/l;->fW:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/g;->u:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->r:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->g:Lcom/google/android/apps/gmm/place/reservation/b/e;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->r:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 290
    :cond_2
    return-object v4
.end method

.method public final i()Lcom/google/android/apps/gmm/place/reservation/b/e;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->g:Lcom/google/android/apps/gmm/place/reservation/b/e;

    return-object v0
.end method

.method public j()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->f:Lcom/google/android/apps/gmm/place/reservation/b/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->g:Lcom/google/android/apps/gmm/place/reservation/b/e;

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->f:Lcom/google/android/apps/gmm/place/reservation/b/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/reservation/b/b;->g()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/place/reservation/ae;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->s:Ljava/lang/String;

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->g:Lcom/google/android/apps/gmm/place/reservation/b/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/reservation/b/e;->f()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->t:Ljava/lang/Integer;

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->n:Ljava/lang/CharSequence;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->o:Lcom/google/r/b/a/v;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/g;->s:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/reservation/g;->o:Lcom/google/r/b/a/v;

    .line 337
    iget-object v0, v4, Lcom/google/r/b/a/v;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->t:Ljava/lang/Integer;

    .line 338
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->o:Lcom/google/r/b/a/v;

    iget v1, v1, Lcom/google/r/b/a/v;->c:I

    if-ne v0, v1, :cond_4

    .line 353
    :goto_2
    return-void

    .line 337
    :cond_2
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object v1, v4, Lcom/google/r/b/a/v;->d:Ljava/lang/Object;

    :cond_3
    move-object v0, v1

    goto :goto_1

    .line 342
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->n:Ljava/lang/CharSequence;

    .line 344
    invoke-static {}, Lcom/google/r/b/a/v;->newBuilder()Lcom/google/r/b/a/x;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->l:Lcom/google/maps/g/q;

    .line 345
    iget-object v1, v1, Lcom/google/maps/g/q;->g:Lcom/google/n/f;

    if-nez v1, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget v3, v0, Lcom/google/r/b/a/x;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/google/r/b/a/x;->a:I

    iput-object v1, v0, Lcom/google/r/b/a/x;->b:Lcom/google/n/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->s:Ljava/lang/String;

    .line 346
    if-nez v1, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    iget v3, v0, Lcom/google/r/b/a/x;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v0, Lcom/google/r/b/a/x;->a:I

    iput-object v1, v0, Lcom/google/r/b/a/x;->d:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->t:Ljava/lang/Integer;

    .line 347
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v3, v0, Lcom/google/r/b/a/x;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v0, Lcom/google/r/b/a/x;->a:I

    iput v1, v0, Lcom/google/r/b/a/x;->c:I

    .line 348
    invoke-virtual {v0}, Lcom/google/r/b/a/x;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/v;

    .line 349
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->k:Lcom/google/android/apps/gmm/shared/net/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 350
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/place/reservation/g;->m:Z

    .line 351
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->o:Lcom/google/r/b/a/v;

    .line 352
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    goto :goto_2
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->n:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->n:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final m()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->v:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method n()V
    .locals 3

    .prologue
    .line 390
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->x:Z

    if-eqz v0, :cond_0

    .line 400
    :goto_0
    return-void

    .line 397
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/g;->l:Lcom/google/maps/g/q;

    sget-object v2, Lcom/google/b/f/t;->a:Lcom/google/b/f/t;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/place/reservation/ae;->a(Lcom/google/maps/g/q;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 399
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/g;->x:Z

    goto :goto_0
.end method
