.class public Lcom/google/android/apps/gmm/directions/f/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final j:Ljava/lang/String;


# instance fields
.field public final a:Lcom/google/maps/g/a/hm;

.field public final b:Lcom/google/r/b/a/afz;

.field public final c:Lcom/google/maps/g/a/al;

.field public final d:Ljava/lang/String;

.field public final e:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/google/maps/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final g:Lcom/google/o/b/a/v;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final h:Lcom/google/maps/g/hy;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/gmm/directions/f/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/a;->j:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/maps/g/a/hm;Lcom/google/r/b/a/afz;Lcom/google/b/c/cv;Lcom/google/maps/a/a;Lcom/google/o/b/a/v;Lcom/google/maps/g/a/al;Ljava/lang/String;Lcom/google/maps/g/hy;Z)V
    .locals 1
    .param p4    # Lcom/google/maps/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/o/b/a/v;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/maps/g/a/al;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/g/a/hm;",
            "Lcom/google/r/b/a/afz;",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            ">;",
            "Lcom/google/maps/a/a;",
            "Lcom/google/o/b/a/v;",
            "Lcom/google/maps/g/a/al;",
            "Ljava/lang/String;",
            "Lcom/google/maps/g/hy;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 73
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 74
    :cond_1
    invoke-virtual {p3}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    invoke-virtual {p3}, Lcom/google/b/c/cv;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/r/a/as;->a(I)I

    .line 78
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/f/a;->a:Lcom/google/maps/g/a/hm;

    .line 79
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/f/a;->b:Lcom/google/r/b/a/afz;

    .line 80
    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/f/a;->e:Lcom/google/b/c/cv;

    .line 81
    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/f/a;->f:Lcom/google/maps/a/a;

    .line 82
    iput-object p5, p0, Lcom/google/android/apps/gmm/directions/f/a;->g:Lcom/google/o/b/a/v;

    .line 83
    iput-object p6, p0, Lcom/google/android/apps/gmm/directions/f/a;->c:Lcom/google/maps/g/a/al;

    .line 84
    iput-object p7, p0, Lcom/google/android/apps/gmm/directions/f/a;->d:Ljava/lang/String;

    .line 85
    iput-object p8, p0, Lcom/google/android/apps/gmm/directions/f/a;->h:Lcom/google/maps/g/hy;

    .line 86
    iput-boolean p9, p0, Lcom/google/android/apps/gmm/directions/f/a;->i:Z

    .line 87
    return-void
.end method

.method public static a(Lcom/google/r/b/a/agd;Landroid/content/Context;)Lcom/google/android/apps/gmm/directions/f/a;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 191
    new-instance v5, Lcom/google/android/apps/gmm/directions/f/b;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/directions/f/b;-><init>()V

    .line 192
    iget-object v0, p0, Lcom/google/r/b/a/agd;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/afj;->g()Lcom/google/r/b/a/afj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afj;

    .line 193
    iget v1, v0, Lcom/google/r/b/a/afj;->c:I

    invoke-static {v1}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/maps/g/a/hm;->f:Lcom/google/maps/g/a/hm;

    :cond_0
    iput-object v1, v5, Lcom/google/android/apps/gmm/directions/f/b;->a:Lcom/google/maps/g/a/hm;

    .line 194
    iget v1, v0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2

    move v1, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 195
    iget-object v1, v0, Lcom/google/r/b/a/afj;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/afz;

    iput-object v1, v5, Lcom/google/android/apps/gmm/directions/f/b;->b:Lcom/google/r/b/a/afz;

    .line 197
    :cond_1
    invoke-virtual {v0}, Lcom/google/r/b/a/afj;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/je;

    .line 198
    invoke-static {v1, p1}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Lcom/google/maps/g/a/je;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    iget-object v6, v5, Lcom/google/android/apps/gmm/directions/f/b;->c:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move v1, v4

    .line 194
    goto :goto_0

    .line 200
    :cond_3
    iget v1, v0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_b

    move v1, v3

    :goto_2
    if-eqz v1, :cond_4

    .line 201
    iget-object v1, v0, Lcom/google/r/b/a/afj;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/a/a;

    iput-object v1, v5, Lcom/google/android/apps/gmm/directions/f/b;->d:Lcom/google/maps/a/a;

    .line 203
    :cond_4
    iget v1, v0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_c

    move v1, v3

    :goto_3
    if-eqz v1, :cond_6

    .line 204
    iget v1, v0, Lcom/google/r/b/a/afj;->g:I

    invoke-static {v1}, Lcom/google/maps/g/a/al;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/maps/g/a/al;->d:Lcom/google/maps/g/a/al;

    :cond_5
    iget v1, v1, Lcom/google/maps/g/a/al;->e:I

    invoke-static {v1}, Lcom/google/maps/g/a/al;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v1

    iput-object v1, v5, Lcom/google/android/apps/gmm/directions/f/b;->f:Lcom/google/maps/g/a/al;

    .line 206
    :cond_6
    iget v1, v0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_d

    move v1, v3

    :goto_4
    if-eqz v1, :cond_7

    .line 207
    iget-object v1, v0, Lcom/google/r/b/a/afj;->i:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_e

    check-cast v1, Ljava/lang/String;

    :goto_5
    iput-object v1, v5, Lcom/google/android/apps/gmm/directions/f/b;->g:Ljava/lang/String;

    .line 209
    :cond_7
    iget v1, v0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_10

    move v1, v3

    :goto_6
    if-eqz v1, :cond_8

    .line 210
    iget-object v1, v0, Lcom/google/r/b/a/afj;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/hy;

    iput-object v1, v5, Lcom/google/android/apps/gmm/directions/f/b;->h:Lcom/google/maps/g/hy;

    .line 212
    :cond_8
    iget v1, p0, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_11

    move v1, v3

    :goto_7
    if-eqz v1, :cond_9

    .line 213
    iget-object v1, p0, Lcom/google/r/b/a/agd;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/v;->d()Lcom/google/o/b/a/v;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/b/a/v;

    iput-object v1, v5, Lcom/google/android/apps/gmm/directions/f/b;->e:Lcom/google/o/b/a/v;

    .line 215
    :cond_9
    iget v1, v0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_12

    move v1, v3

    :goto_8
    if-eqz v1, :cond_a

    .line 216
    iget-boolean v0, v0, Lcom/google/r/b/a/afj;->p:Z

    iput-boolean v0, v5, Lcom/google/android/apps/gmm/directions/f/b;->i:Z

    .line 218
    :cond_a
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/directions/f/b;->a()Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v0

    return-object v0

    :cond_b
    move v1, v4

    .line 200
    goto/16 :goto_2

    :cond_c
    move v1, v4

    .line 203
    goto :goto_3

    :cond_d
    move v1, v4

    .line 206
    goto :goto_4

    .line 207
    :cond_e
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_f

    iput-object v2, v0, Lcom/google/r/b/a/afj;->i:Ljava/lang/Object;

    :cond_f
    move-object v1, v2

    goto :goto_5

    :cond_10
    move v1, v4

    .line 209
    goto :goto_6

    :cond_11
    move v1, v4

    .line 212
    goto :goto_7

    :cond_12
    move v1, v4

    .line 215
    goto :goto_8
.end method

.method public static a(Lcom/google/b/c/cv;Lcom/google/b/c/cv;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            ">;",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 162
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 163
    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 164
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v2

    .line 179
    :goto_0
    return v0

    .line 167
    :cond_2
    invoke-virtual {p0}, Lcom/google/b/c/cv;->size()I

    move-result v6

    .line 168
    invoke-virtual {p1}, Lcom/google/b/c/cv;->size()I

    move-result v0

    .line 169
    if-eq v6, v0, :cond_3

    move v0, v3

    .line 170
    goto :goto_0

    :cond_3
    move v4, v3

    .line 172
    :goto_1
    if-ge v4, v6, :cond_a

    .line 173
    invoke-virtual {p0, v4}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 174
    invoke-virtual {p1, v4}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 175
    iget-object v5, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v7, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v5, v7, :cond_6

    move v5, v2

    :goto_2
    if-eqz v5, :cond_4

    iget-object v5, v1, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v7, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v5, v7, :cond_7

    move v5, v2

    :goto_3
    if-nez v5, :cond_5

    :cond_4
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/r/a/ap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_5
    move v0, v2

    :goto_4
    if-nez v0, :cond_9

    move v0, v3

    .line 176
    goto :goto_0

    :cond_6
    move v5, v3

    .line 175
    goto :goto_2

    :cond_7
    move v5, v3

    goto :goto_3

    :cond_8
    move v0, v3

    goto :goto_4

    .line 172
    :cond_9
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_a
    move v0, v2

    .line 179
    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 133
    const-string v1, "travelMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a;->a:Lcom/google/maps/g/a/hm;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    const-string v1, "options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a;->b:Lcom/google/r/b/a/afz;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    const-string v1, "waypoints="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a;->e:Lcom/google/b/c/cv;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    const-string v1, "inputCamera="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a;->f:Lcom/google/maps/a/a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    const-string v1, "userLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a;->g:Lcom/google/o/b/a/v;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    const-string v1, "preferredTransitPattern="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    const-string v1, "loggingParams="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a;->h:Lcom/google/maps/g/hy;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    const-string v1, "restrictToIndashIncidents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/f/a;->i:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
