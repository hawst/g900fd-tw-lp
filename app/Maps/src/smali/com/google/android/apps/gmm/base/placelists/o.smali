.class public Lcom/google/android/apps/gmm/base/placelists/o;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/z/a/b;

.field b:Lcom/google/android/apps/gmm/base/g/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field c:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/z/a/b;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/placelists/o;->c:I

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/placelists/o;->a:Lcom/google/android/apps/gmm/z/a/b;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(ILcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 7

    .prologue
    .line 110
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/placelists/o;->b:Lcom/google/android/apps/gmm/base/g/c;

    iput p1, p0, Lcom/google/android/apps/gmm/base/placelists/o;->c:I

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/o;->b:Lcom/google/android/apps/gmm/base/g/c;

    if-nez v0, :cond_0

    const/4 v5, 0x0

    .line 114
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/o;->a:Lcom/google/android/apps/gmm/z/a/b;

    new-instance v1, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v2, Lcom/google/r/b/a/a;->f:Lcom/google/r/b/a/a;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    sget-object v2, Lcom/google/b/f/t;->cU:Lcom/google/b/f/t;

    iget v6, p0, Lcom/google/android/apps/gmm/base/placelists/o;->c:I

    move-object v3, p3

    move-object v4, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/z/b/n;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/z/b/l;I)V

    .line 122
    return-void

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/o;->b:Lcom/google/android/apps/gmm/base/g/c;

    .line 113
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v5

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v1, Lcom/google/r/b/a/a;->a:Lcom/google/r/b/a/a;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    sget-object v1, Lcom/google/b/f/t;->gc:Lcom/google/b/f/t;

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/google/android/apps/gmm/base/placelists/o;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 63
    return-void
.end method

.method protected final a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 7

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/o;->b:Lcom/google/android/apps/gmm/base/g/c;

    if-nez v0, :cond_1

    const/4 v5, 0x0

    .line 128
    :goto_0
    if-eq p3, p4, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/o;->a:Lcom/google/android/apps/gmm/z/a/b;

    iget v6, p0, Lcom/google/android/apps/gmm/base/placelists/o;->c:I

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/z/b/n;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/z/b/l;I)V

    .line 139
    :cond_0
    return-void

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/o;->b:Lcom/google/android/apps/gmm/base/g/c;

    .line 127
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v5

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 2

    .prologue
    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v1, Lcom/google/r/b/a/a;->a:Lcom/google/r/b/a/a;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    sget-object v1, Lcom/google/b/f/t;->df:Lcom/google/b/f/t;

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/google/android/apps/gmm/base/placelists/o;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 74
    return-void
.end method

.method public final c(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 2

    .prologue
    .line 96
    new-instance v0, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v1, Lcom/google/r/b/a/a;->f:Lcom/google/r/b/a/a;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    sget-object v1, Lcom/google/b/f/t;->cU:Lcom/google/b/f/t;

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/google/android/apps/gmm/base/placelists/o;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 98
    return-void
.end method
