.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/a/a;
.implements Lcom/google/android/apps/gmm/map/f/a;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/f/f;

.field b:Lcom/google/android/apps/gmm/map/f/a/a;

.field private final c:Lcom/google/android/apps/gmm/map/f/o;

.field private d:Lcom/google/android/apps/gmm/map/f/a/c;

.field private final e:Lcom/google/android/apps/gmm/map/f/a/c;

.field private f:J

.field private g:Z

.field private h:I

.field private i:Lcom/google/android/apps/gmm/map/f/z;

.field private j:Lcom/google/android/apps/gmm/map/f/i;

.field private final k:Lcom/google/android/apps/gmm/shared/c/f;

.field private final l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

.field private final m:Lcom/google/android/apps/gmm/map/f/l;

.field private final n:Lcom/google/android/apps/gmm/map/f/x;

.field private final o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;

.field private final p:Lcom/google/android/apps/gmm/map/f/c;

.field private final q:Lcom/google/android/apps/gmm/shared/net/ad;

.field private r:Lcom/google/android/apps/gmm/map/legacy/internal/vector/j;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/ad;Landroid/content/Context;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 2

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/f/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->d:Lcom/google/android/apps/gmm/map/f/a/c;

    .line 77
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->e:Lcom/google/android/apps/gmm/map/f/a/c;

    .line 183
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->c:Lcom/google/android/apps/gmm/map/f/o;

    .line 184
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->k:Lcom/google/android/apps/gmm/shared/c/f;

    .line 185
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->q:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 186
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->d:Lcom/google/android/apps/gmm/map/f/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/a/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    .line 189
    new-instance v0, Lcom/google/android/apps/gmm/map/f/f;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/f/f;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a:Lcom/google/android/apps/gmm/map/f/f;

    .line 191
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a:Lcom/google/android/apps/gmm/map/f/f;

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;-><init>(Lcom/google/android/apps/gmm/shared/net/ad;Landroid/content/Context;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/f/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    .line 193
    new-instance v0, Lcom/google/android/apps/gmm/map/f/l;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a:Lcom/google/android/apps/gmm/map/f/f;

    invoke-direct {v0, p4, p3, v1}, Lcom/google/android/apps/gmm/map/f/l;-><init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/f/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->m:Lcom/google/android/apps/gmm/map/f/l;

    .line 194
    new-instance v0, Lcom/google/android/apps/gmm/map/f/x;

    invoke-direct {v0, p4}, Lcom/google/android/apps/gmm/map/f/x;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->n:Lcom/google/android/apps/gmm/map/f/x;

    .line 195
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a:Lcom/google/android/apps/gmm/map/f/f;

    invoke-direct {v0, p4, p3, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;-><init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/f/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;

    .line 197
    new-instance v0, Lcom/google/android/apps/gmm/map/f/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a:Lcom/google/android/apps/gmm/map/f/f;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/f/c;-><init>(Lcom/google/android/apps/gmm/map/f/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->p:Lcom/google/android/apps/gmm/map/f/c;

    .line 199
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/map/f/a/a;)V
    .locals 7

    .prologue
    .line 337
    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->d:Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/f/a/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a:Lcom/google/android/apps/gmm/map/f/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->d:Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/f;->a(Lcom/google/android/apps/gmm/map/f/a/c;)V

    .line 340
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->d:Lcom/google/android/apps/gmm/map/f/a/c;

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->m:Lcom/google/android/apps/gmm/map/f/l;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, v6, v1}, Lcom/google/android/apps/gmm/map/f/l;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->m:Lcom/google/android/apps/gmm/map/f/l;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/f/l;->b(J)Lcom/google/android/apps/gmm/map/f/b;

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->m:Lcom/google/android/apps/gmm/map/f/l;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/b;)V

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->n:Lcom/google/android/apps/gmm/map/f/x;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, v6, v1}, Lcom/google/android/apps/gmm/map/f/x;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->n:Lcom/google/android/apps/gmm/map/f/x;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/f/x;->b(J)Lcom/google/android/apps/gmm/map/f/b;

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->n:Lcom/google/android/apps/gmm/map/f/x;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 351
    monitor-exit p0

    return-void

    .line 337
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(FI)F
    .locals 4

    .prologue
    const/high16 v0, 0x40000000    # 2.0f

    .line 466
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 467
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->a:Z

    iput p1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->b:F

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->o:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 468
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;

    int-to-long v2, p2

    iput-wide v2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->p:J

    .line 469
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/b;)V

    .line 470
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    const/high16 v1, 0x41a80000    # 21.0f

    :try_start_1
    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 466
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Lcom/google/android/apps/gmm/map/f/b;)V
    .locals 2

    .prologue
    .line 282
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->p:Lcom/google/android/apps/gmm/map/f/c;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/f/c;->a(Lcom/google/android/apps/gmm/map/f/b;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 283
    if-nez v0, :cond_0

    .line 290
    :goto_0
    monitor-exit p0

    return-void

    .line 288
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->e:Lcom/google/android/apps/gmm/map/f/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/a/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->p:Lcom/google/android/apps/gmm/map/f/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->e:Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/c;->a(Lcom/google/android/apps/gmm/map/f/a/c;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 282
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(F)F
    .locals 3

    .prologue
    .line 451
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 452
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a(F)F

    move-result v0

    .line 453
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454
    monitor-exit p0

    return v0

    .line 451
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(FFF)F
    .locals 3

    .prologue
    .line 435
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 436
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a(FFF)F

    move-result v0

    .line 437
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 438
    monitor-exit p0

    return v0

    .line 435
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(FFFI)F
    .locals 4

    .prologue
    const/high16 v0, 0x40000000    # 2.0f

    .line 497
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 498
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    add-float/2addr v2, p1

    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->a:Z

    iput v2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->b:F

    iput p2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->m:F

    iput p3, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->n:F

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->o:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 499
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;

    int-to-long v2, p4

    iput-wide v2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->p:J

    .line 500
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/b;)V

    .line 501
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    add-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    const/high16 v2, 0x41a80000    # 21.0f

    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 497
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(FI)F
    .locals 1

    .prologue
    .line 482
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    add-float/2addr v0, p1

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b(FI)F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(FF)V
    .locals 3

    .prologue
    .line 400
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a(FF)V

    .line 402
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403
    monitor-exit p0

    return-void

    .line 400
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/f/a/a;I)V
    .locals 6

    .prologue
    const/4 v3, -0x1

    .line 305
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a:Lcom/google/android/apps/gmm/map/f/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/f/f;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    .line 306
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->h:I

    .line 310
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->q:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 311
    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v1

    .line 312
    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget-boolean v1, v1, Lcom/google/r/b/a/ou;->G:Z

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 313
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/f/a/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 314
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 316
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->i:Lcom/google/android/apps/gmm/map/f/z;

    if-eqz v1, :cond_1

    .line 317
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->i:Lcom/google/android/apps/gmm/map/f/z;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/f/z;->b(Lcom/google/android/apps/gmm/map/f/a/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 322
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 323
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->m:Lcom/google/android/apps/gmm/map/f/l;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/gmm/map/f/l;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 324
    if-eq p2, v3, :cond_3

    .line 325
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->m:Lcom/google/android/apps/gmm/map/f/l;

    int-to-long v4, p2

    invoke-virtual {v2, v4, v5}, Lcom/google/android/apps/gmm/map/f/l;->b(J)Lcom/google/android/apps/gmm/map/f/b;

    .line 327
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->m:Lcom/google/android/apps/gmm/map/f/l;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/b;)V

    .line 329
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->n:Lcom/google/android/apps/gmm/map/f/x;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/gmm/map/f/x;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 330
    if-eq p2, v3, :cond_4

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->n:Lcom/google/android/apps/gmm/map/f/x;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/f/x;->b(J)Lcom/google/android/apps/gmm/map/f/b;

    .line 333
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->n:Lcom/google/android/apps/gmm/map/f/x;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 305
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/f/b;)V
    .locals 13

    .prologue
    .line 251
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->p:Lcom/google/android/apps/gmm/map/f/c;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/f/b;->W_()I

    move-result v5

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/f/b;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/d;->values()[Lcom/google/android/apps/gmm/map/f/a/d;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_5

    aget-object v6, v2, v1

    iget v0, v6, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    const/4 v7, 0x1

    shl-int/2addr v7, v0

    and-int/2addr v7, v5

    if-eqz v7, :cond_4

    iget-object v7, v4, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v7, v7, v0

    if-eqz v7, :cond_4

    iget-object v7, v4, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v0, v7, v0

    invoke-interface {v0, p1, v6}, Lcom/google/android/apps/gmm/map/f/b;->a(Lcom/google/android/apps/gmm/map/f/b;Lcom/google/android/apps/gmm/map/f/a/d;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/d;->values()[Lcom/google/android/apps/gmm/map/f/a/d;

    move-result-object v7

    array-length v8, v7

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v8, :cond_4

    aget-object v9, v7, v0

    iget-object v10, v4, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ne v9, v6, :cond_3

    const/4 v1, 0x0

    .line 253
    :cond_1
    :goto_2
    if-nez v1, :cond_a

    .line 272
    :cond_2
    :goto_3
    monitor-exit p0

    return-void

    .line 251
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_5
    const/4 v2, 0x0

    const/4 v1, 0x0

    :try_start_2
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/d;->values()[Lcom/google/android/apps/gmm/map/f/a/d;

    move-result-object v6

    array-length v7, v6

    const/4 v0, 0x0

    move v3, v0

    :goto_4
    if-ge v3, v7, :cond_8

    aget-object v0, v6, v3

    iget v8, v0, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    const/4 v9, 0x1

    shl-int/2addr v9, v8

    and-int v10, v5, v9

    if-eqz v10, :cond_10

    iget-object v10, v4, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v10, v10, v8

    if-eqz v10, :cond_6

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/f/b;->f()Z

    move-result v11

    if-nez v11, :cond_6

    invoke-interface {v10, p1, v0}, Lcom/google/android/apps/gmm/map/f/b;->a(Lcom/google/android/apps/gmm/map/f/b;Lcom/google/android/apps/gmm/map/f/a/d;)Z

    move-result v11

    if-eqz v11, :cond_10

    :cond_6
    if-eqz v10, :cond_7

    invoke-interface {v10, p1, v0}, Lcom/google/android/apps/gmm/map/f/b;->b(Lcom/google/android/apps/gmm/map/f/b;Lcom/google/android/apps/gmm/map/f/a/d;)V

    invoke-interface {v10}, Lcom/google/android/apps/gmm/map/f/b;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    if-eq v10, p1, :cond_7

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/f/c;->c:Ljava/util/HashSet;

    invoke-virtual {v0, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_7
    if-eq v10, p1, :cond_f

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aput-object p1, v0, v8

    or-int v0, v1, v9

    :goto_5
    or-int v1, v2, v9

    move v12, v0

    move v0, v1

    move v1, v12

    :goto_6
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_4

    :cond_8
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/f/c;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/f/b;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/f/c;->a(Lcom/google/android/apps/gmm/map/f/b;)I

    goto :goto_7

    :cond_9
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/f/c;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    if-eqz v2, :cond_1

    invoke-interface {p1, v2}, Lcom/google/android/apps/gmm/map/f/b;->a(I)V

    goto :goto_2

    .line 258
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->c:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    if-eqz v1, :cond_b

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_d

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/f/q;->c()V

    .line 261
    :cond_b
    :goto_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->e:Lcom/google/android/apps/gmm/map/f/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/a/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->p:Lcom/google/android/apps/gmm/map/f/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->e:Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/c;->a(Lcom/google/android/apps/gmm/map/f/a/c;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->g:Z

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->k:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->d()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->p:Lcom/google/android/apps/gmm/map/f/c;

    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    move v12, v0

    move-wide v0, v2

    move v2, v12

    :goto_9
    sget v3, Lcom/google/android/apps/gmm/map/f/a/a;->a:I

    if-ge v2, v3, :cond_e

    iget-object v3, v6, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v3, v3, v2

    if-eqz v3, :cond_c

    iget-object v3, v6, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v3, v3, v2

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/f/b;->d()J

    move-result-wide v8

    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    :cond_c
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 258
    :cond_d
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(I)V

    goto :goto_8

    .line 263
    :cond_e
    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->f:J

    .line 265
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->m:Lcom/google/android/apps/gmm/map/f/l;

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->m:Lcom/google/android/apps/gmm/map/f/l;

    .line 268
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/f/l;->a()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->i:Lcom/google/android/apps/gmm/map/f/z;

    if-eqz v0, :cond_2

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->i:Lcom/google/android/apps/gmm/map/f/z;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/f/z;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_3

    :cond_f
    move v0, v1

    goto/16 :goto_5

    :cond_10
    move v0, v2

    goto/16 :goto_6
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/f/d;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/map/f/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 606
    monitor-enter p0

    if-eqz p1, :cond_1

    .line 607
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/gmm/map/f/d;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 608
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/b;)V

    .line 609
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->c:Lcom/google/android/apps/gmm/map/f/o;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/d;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 611
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->c:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    if-eqz v2, :cond_2

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/q;->c:Lcom/google/android/apps/gmm/map/f/d;

    .line 612
    :cond_2
    if-eqz v0, :cond_0

    .line 613
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b(Lcom/google/android/apps/gmm/map/f/b;)V

    .line 614
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->c:Lcom/google/android/apps/gmm/map/f/o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/d;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 606
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/f/i;)V
    .locals 1

    .prologue
    .line 226
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->j:Lcom/google/android/apps/gmm/map/f/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    monitor-exit p0

    return-void

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/f/z;)V
    .locals 1

    .prologue
    .line 218
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->i:Lcom/google/android/apps/gmm/map/f/z;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    monitor-exit p0

    return-void

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a:Lcom/google/android/apps/gmm/map/f/f;

    iput-boolean p1, v0, Lcom/google/android/apps/gmm/map/f/f;->a:Z

    .line 231
    return-void
.end method

.method public final declared-synchronized a([F)V
    .locals 3

    .prologue
    .line 413
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 414
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a([F)V

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 416
    monitor-exit p0

    return-void

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 621
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(F)F
    .locals 3

    .prologue
    .line 555
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b(F)F

    move-result v0

    .line 557
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 558
    monitor-exit p0

    return v0

    .line 555
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(FFF)F
    .locals 3

    .prologue
    .line 543
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {v0, p3, p1, p2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b(FFF)F

    move-result v0

    .line 545
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 546
    monitor-exit p0

    return v0

    .line 543
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()I
    .locals 15

    .prologue
    .line 360
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->p:Lcom/google/android/apps/gmm/map/f/c;

    const/4 v0, 0x0

    :goto_0
    sget v2, Lcom/google/android/apps/gmm/map/f/a/a;->a:I

    if-ge v0, v2, :cond_1

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/f/b;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    move v6, v0

    .line 361
    :goto_1
    iget v7, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->h:I

    .line 362
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->p:Lcom/google/android/apps/gmm/map/f/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->k:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->d()J

    move-result-wide v4

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->d:Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/d;->values()[Lcom/google/android/apps/gmm/map/f/a/d;

    move-result-object v9

    array-length v10, v9

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v10, :cond_6

    aget-object v11, v9, v2

    iget v12, v11, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v0, v0, v12

    if-nez v0, :cond_2

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/f/c;->b:[I

    const/4 v1, 0x0

    aput v1, v0, v12

    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 360
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    move v6, v0

    goto :goto_1

    .line 362
    :cond_2
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v12, :cond_3

    iget-object v13, v3, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v13, v13, v1

    iget-object v14, v3, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v14, v14, v12

    if-ne v13, v14, :cond_5

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/f/c;->b:[I

    iget-object v13, v3, Lcom/google/android/apps/gmm/map/f/c;->b:[I

    aget v1, v13, v1

    aput v1, v0, v12

    const/4 v0, 0x1

    :cond_3
    if-nez v0, :cond_4

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/f/c;->b:[I

    iget-object v1, v3, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v1, v1, v12

    invoke-interface {v1, v4, v5}, Lcom/google/android/apps/gmm/map/f/b;->a(J)I

    move-result v1

    aput v1, v0, v12

    :cond_4
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v0, v0, v12

    invoke-interface {v0, v11}, Lcom/google/android/apps/gmm/map/f/b;->a(Lcom/google/android/apps/gmm/map/f/a/d;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v8, v11, v0}, Lcom/google/android/apps/gmm/map/f/a/c;->a(Lcom/google/android/apps/gmm/map/f/a/d;Ljava/lang/Object;)Lcom/google/android/apps/gmm/map/f/a/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 360
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 362
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    :try_start_1
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/f/c;->d:Lcom/google/android/apps/gmm/map/f/f;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/gmm/map/f/f;->a(Lcom/google/android/apps/gmm/map/f/a/c;)V

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/d;->values()[Lcom/google/android/apps/gmm/map/f/a/d;

    move-result-object v2

    array-length v4, v2

    const/4 v0, 0x0

    :goto_5
    if-ge v0, v4, :cond_8

    aget-object v5, v2, v0

    iget v5, v5, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    iget-object v8, v3, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v8, v8, v5

    if-eqz v8, :cond_7

    iget-object v8, v3, Lcom/google/android/apps/gmm/map/f/c;->b:[I

    aget v8, v8, v5

    if-nez v8, :cond_7

    iget-object v8, v3, Lcom/google/android/apps/gmm/map/f/c;->a:[Lcom/google/android/apps/gmm/map/f/b;

    aget-object v8, v8, v5

    invoke-virtual {v3, v8}, Lcom/google/android/apps/gmm/map/f/c;->a(Lcom/google/android/apps/gmm/map/f/b;)I

    :cond_7
    iget-object v8, v3, Lcom/google/android/apps/gmm/map/f/c;->b:[I

    aget v5, v8, v5

    or-int/2addr v1, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_8
    iput v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->h:I

    .line 364
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->d:Lcom/google/android/apps/gmm/map/f/a/c;

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->c:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 367
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->j:Lcom/google/android/apps/gmm/map/f/i;

    if-eqz v0, :cond_9

    .line 368
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->j:Lcom/google/android/apps/gmm/map/f/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/f/i;->a(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 371
    :cond_9
    if-eqz v7, :cond_a

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->h:I

    if-nez v0, :cond_a

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->q:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/j/j;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-direct {v1, v2, v6}, Lcom/google/android/apps/gmm/map/j/j;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;Z)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->i:Lcom/google/android/apps/gmm/map/f/z;

    if-eqz v0, :cond_a

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->i:Lcom/google/android/apps/gmm/map/f/z;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/f/z;->b(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 380
    :cond_a
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 381
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->h:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return v0
.end method

.method public final c()Lcom/google/android/apps/gmm/map/f/a/i;
    .locals 7

    .prologue
    .line 386
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->g:Z

    if-eqz v0, :cond_0

    .line 387
    new-instance v6, Lcom/google/android/apps/gmm/map/f/a/i;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->e:Lcom/google/android/apps/gmm/map/f/a/c;

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->f:J

    invoke-direct {v6, v0, v2, v3}, Lcom/google/android/apps/gmm/map/f/a/i;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;J)V

    move-object v0, v6

    .line 389
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized c(F)V
    .locals 6

    .prologue
    .line 587
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 588
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->c(F)V

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(Lcom/google/android/apps/gmm/map/f/b;)V

    .line 591
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->r:Lcom/google/android/apps/gmm/map/legacy/internal/vector/j;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a:Lcom/google/android/apps/gmm/map/f/f;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/f/f;->a:Z

    if-eqz v0, :cond_1

    .line 592
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->r:Lcom/google/android/apps/gmm/map/legacy/internal/vector/j;

    const-string v1, "Tilt: %.1f "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v4, v4, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    .line 593
    iget v5, v5, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a:F

    add-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    .line 592
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/j;->a:Landroid/widget/Toast;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/j;->a:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->cancel()V

    :cond_0
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/j;->b:Landroid/content/Context;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v3, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/j;->a:Landroid/widget/Toast;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/j;->a:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 595
    :cond_1
    monitor-exit p0

    return-void

    .line 587
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    .prologue
    .line 423
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->l:Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 424
    monitor-exit p0

    return-void

    .line 423
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
