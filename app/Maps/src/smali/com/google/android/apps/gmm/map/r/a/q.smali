.class public Lcom/google/android/apps/gmm/map/r/a/q;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/apps/gmm/map/r/a/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/r/a/q;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/b/a/ab;I)Lcom/google/b/a/an;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            "I)",
            "Lcom/google/b/a/an",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    const/4 v1, 0x2

    if-lt v0, v1, :cond_2

    .line 263
    if-lez p1, :cond_0

    add-int/lit8 v0, p1, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->c(I)F

    move-result v0

    .line 268
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v1, v1

    div-int/lit8 v1, v1, 0x3

    add-int/lit8 v1, v1, -0x1

    if-ge p1, v1, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/ab;->c(I)F

    move-result v1

    .line 273
    :goto_1
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    return-object v0

    .line 264
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/ab;->c(I)F

    move-result v0

    goto :goto_0

    .line 268
    :cond_1
    add-int/lit8 v1, p1, -0x1

    .line 269
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/b/a/ab;->c(I)F

    move-result v1

    goto :goto_1

    .line 271
    :cond_2
    const/4 v1, 0x0

    move v0, v1

    goto :goto_1
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/p;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 283
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/p;->a:Lcom/google/maps/g/a/ea;

    iget-object v0, v0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    move v0, v1

    move v2, v1

    .line 286
    :goto_0
    if-ge v0, v6, :cond_1

    .line 287
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/r/a/p;->a(I)Lcom/google/android/apps/gmm/map/r/a/al;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v3, v3, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    move v3, v1

    .line 288
    :goto_1
    if-ge v3, v7, :cond_0

    .line 289
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/r/a/p;->a(I)Lcom/google/android/apps/gmm/map/r/a/al;

    move-result-object v4

    invoke-virtual {v4, v3, v2}, Lcom/google/android/apps/gmm/map/r/a/al;->a(II)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    add-int/lit8 v4, v2, 0x1

    .line 288
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_1

    .line 286
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 293
    :cond_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/r/a/an;->a([Lcom/google/android/apps/gmm/map/r/a/ag;)Z

    move-result v0

    return v0
.end method

.method static a(Lcom/google/android/apps/gmm/map/r/a/p;Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/r/a/ap;)[Lcom/google/android/apps/gmm/map/r/a/ag;
    .locals 16

    .prologue
    .line 47
    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 48
    :cond_0
    if-nez p1, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 49
    :cond_1
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v1, v1

    div-int/lit8 v1, v1, 0x3

    if-gtz v1, :cond_2

    .line 50
    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/q;->a:Ljava/lang/String;

    const-string v2, "polyline should have at least one vertex."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 103
    :goto_0
    return-object v1

    .line 54
    :cond_2
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 56
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/p;->a:Lcom/google/maps/g/a/ea;

    iget-object v1, v1, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    .line 57
    const/4 v3, 0x0

    .line 58
    const/4 v2, 0x0

    .line 59
    const/4 v1, 0x0

    move v5, v1

    move v1, v2

    move-object v2, v3

    :goto_1
    if-ge v5, v10, :cond_12

    .line 60
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/r/a/p;->a(I)Lcom/google/android/apps/gmm/map/r/a/al;

    move-result-object v11

    .line 61
    iget-object v3, v11, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v3, v3, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v12

    .line 62
    const/4 v3, 0x0

    move v8, v3

    move v6, v1

    move-object v7, v2

    :goto_2
    if-ge v8, v12, :cond_11

    .line 63
    iget-object v1, v11, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v1, v1, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/eu;->d()Lcom/google/maps/g/a/eu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/eu;

    .line 65
    const/4 v3, 0x0

    .line 66
    const/4 v4, 0x0

    .line 67
    if-eqz v7, :cond_13

    .line 68
    iget-object v2, v7, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v2, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v2

    :goto_3
    iget v2, v2, Lcom/google/maps/g/a/ai;->a:I

    and-int/lit8 v2, v2, 0x1

    const/4 v13, 0x1

    if-ne v2, v13, :cond_5

    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_7

    .line 69
    iget-object v2, v7, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v2, :cond_6

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v2

    :goto_5
    iget v2, v2, Lcom/google/maps/g/a/ai;->b:I

    .line 73
    :goto_6
    iget-object v3, v7, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    if-nez v3, :cond_9

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v3

    :goto_7
    iget v3, v3, Lcom/google/maps/g/a/be;->a:I

    and-int/lit8 v3, v3, 0x1

    const/4 v13, 0x1

    if-ne v3, v13, :cond_a

    const/4 v3, 0x1

    :goto_8
    if-eqz v3, :cond_c

    .line 74
    iget-object v3, v7, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    if-nez v3, :cond_b

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v3

    :goto_9
    iget v3, v3, Lcom/google/maps/g/a/be;->b:I

    move v4, v2

    .line 86
    :goto_a
    iget v2, v1, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v7, 0x800

    if-ne v2, v7, :cond_e

    const/4 v2, 0x1

    :goto_b
    if-eqz v2, :cond_f

    .line 87
    iget v2, v1, Lcom/google/maps/g/a/eu;->p:I

    .line 88
    :goto_c
    if-ltz v2, :cond_3

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v7, v7

    div-int/lit8 v7, v7, 0x3

    if-lt v2, v7, :cond_10

    .line 89
    :cond_3
    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/q;->a:Ljava/lang/String;

    const-string v3, "compact_polyline_vertex_offset %d out of range [0..%d)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 90
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x1

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v5, v5

    div-int/lit8 v5, v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    .line 89
    invoke-static {v1, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/android/apps/gmm/map/r/a/ag;

    goto/16 :goto_0

    .line 68
    :cond_4
    iget-object v2, v7, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    goto :goto_4

    .line 69
    :cond_6
    iget-object v2, v7, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto :goto_5

    .line 71
    :cond_7
    sget-object v2, Lcom/google/android/apps/gmm/map/r/a/q;->a:Ljava/lang/String;

    const-string v2, "distance.meters is not set : "

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v14

    if-eqz v14, :cond_8

    invoke-virtual {v2, v13}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move v2, v3

    goto :goto_6

    :cond_8
    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move v2, v3

    goto :goto_6

    .line 73
    :cond_9
    iget-object v3, v7, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    goto :goto_7

    :cond_a
    const/4 v3, 0x0

    goto :goto_8

    .line 74
    :cond_b
    iget-object v3, v7, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    goto :goto_9

    .line 76
    :cond_c
    sget-object v3, Lcom/google/android/apps/gmm/map/r/a/q;->a:Ljava/lang/String;

    const-string v3, "duration.seconds is not set : "

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v13

    if-eqz v13, :cond_d

    invoke-virtual {v3, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move v3, v4

    move v4, v2

    goto/16 :goto_a

    :cond_d
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move v3, v4

    move v4, v2

    goto/16 :goto_a

    .line 86
    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_b

    .line 87
    :cond_f
    const/4 v2, -0x1

    goto/16 :goto_c

    .line 94
    :cond_10
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/map/r/a/q;->a(Lcom/google/android/apps/gmm/map/b/a/ab;I)Lcom/google/b/a/an;

    move-result-object v7

    .line 96
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v13

    iget-object v2, v7, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v14

    iget-object v2, v7, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 95
    move-object/from16 v0, p2

    invoke-static {v1, v13, v14, v2, v0}, Lcom/google/android/apps/gmm/map/r/a/aj;->a(Lcom/google/maps/g/a/eu;Lcom/google/android/apps/gmm/map/b/a/y;FFLcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/android/apps/gmm/map/r/a/aj;

    move-result-object v1

    .line 97
    invoke-static {v1, v6, v4, v3}, Lcom/google/android/apps/gmm/map/r/a/ag;->a(Lcom/google/android/apps/gmm/map/r/a/aj;III)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    add-int/lit8 v6, v6, 0x1

    .line 100
    iget-object v7, v1, Lcom/google/android/apps/gmm/map/r/a/aj;->a:Lcom/google/maps/g/a/fk;

    .line 62
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto/16 :goto_2

    .line 59
    :cond_11
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move-object v2, v7

    move v1, v6

    goto/16 :goto_1

    .line 103
    :cond_12
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-interface {v9, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/apps/gmm/map/r/a/ag;

    goto/16 :goto_0

    :cond_13
    move v15, v4

    move v4, v3

    move v3, v15

    goto/16 :goto_a
.end method

.method static a(Lcom/google/android/apps/gmm/map/r/a/p;Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/r/a/ap;Landroid/content/res/Resources;)[Lcom/google/android/apps/gmm/map/r/a/ag;
    .locals 22

    .prologue
    .line 127
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 130
    const/4 v3, 0x0

    .line 132
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/p;->a:Lcom/google/maps/g/a/ea;

    iget-object v1, v1, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v15

    .line 133
    const/4 v1, 0x0

    move v14, v1

    :goto_0
    if-ge v14, v15, :cond_16

    .line 134
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/apps/gmm/map/r/a/p;->a(I)Lcom/google/android/apps/gmm/map/r/a/al;

    move-result-object v16

    .line 135
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v1, v1, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/google/maps/g/a/fk;

    .line 139
    sget-object v1, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    .line 140
    iget v2, v10, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v2, v2, 0x1

    const/4 v4, 0x1

    if-ne v2, v4, :cond_5

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_0

    .line 141
    iget v1, v10, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    .line 144
    :cond_0
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v2, v2, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v17

    .line 146
    sget-object v2, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne v1, v2, :cond_9

    .line 147
    const/4 v1, 0x0

    move v13, v1

    move-object v11, v3

    :goto_2
    move/from16 v0, v17

    if-ge v13, v0, :cond_a

    .line 148
    const/16 v1, -0x3039

    move-object/from16 v0, v16

    invoke-virtual {v0, v13, v1}, Lcom/google/android/apps/gmm/map/r/a/al;->a(II)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v18

    .line 149
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 151
    if-nez v13, :cond_1

    if-eqz v11, :cond_1

    .line 152
    move-object/from16 v0, v18

    iget v1, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/r/a/q;->a(Lcom/google/android/apps/gmm/map/b/a/ab;I)Lcom/google/b/a/an;

    move-result-object v9

    .line 154
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 155
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v18

    iget v3, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 157
    iget-object v4, v11, Lcom/google/maps/g/a/hg;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v4

    check-cast v4, Lcom/google/maps/g/a/gu;

    invoke-virtual {v4}, Lcom/google/maps/g/a/gu;->d()Ljava/lang/String;

    move-result-object v4

    .line 156
    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/map/i/b/c;->a(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 158
    move-object/from16 v0, v18

    iget v5, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, v9, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Float;

    iget-object v9, v9, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Float;

    .line 153
    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/r/a/ag;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/y;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v1

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    const/4 v11, 0x0

    .line 168
    :cond_1
    const/4 v1, 0x1

    move/from16 v0, v17

    if-ne v0, v1, :cond_19

    if-eqz v12, :cond_2

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_6

    :cond_2
    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_19

    iget v1, v10, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_7

    const/4 v1, 0x1

    :goto_4
    if-eqz v1, :cond_19

    .line 169
    invoke-virtual {v10}, Lcom/google/maps/g/a/fk;->d()Ljava/lang/String;

    move-result-object v4

    .line 172
    :goto_5
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_8

    :cond_3
    const/4 v1, 0x1

    :goto_6
    if-nez v1, :cond_4

    .line 175
    move-object/from16 v0, v18

    iget v1, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/r/a/q;->a(Lcom/google/android/apps/gmm/map/b/a/ab;I)Lcom/google/b/a/an;

    move-result-object v1

    .line 176
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    .line 177
    move-object/from16 v0, v18

    iget v5, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, v1, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Float;

    iget-object v9, v1, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Float;

    move-object/from16 v1, v18

    .line 176
    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/r/a/ag;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/y;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v1

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    :cond_4
    add-int/lit8 v1, v13, 0x1

    move v13, v1

    goto/16 :goto_2

    .line 140
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 168
    :cond_6
    const/4 v1, 0x0

    goto :goto_3

    :cond_7
    const/4 v1, 0x0

    goto :goto_4

    .line 172
    :cond_8
    const/4 v1, 0x0

    goto :goto_6

    .line 182
    :cond_9
    sget-object v2, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-ne v1, v2, :cond_15

    .line 183
    if-gtz v17, :cond_b

    .line 184
    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/q;->a:Ljava/lang/String;

    move-object v11, v3

    .line 133
    :cond_a
    :goto_7
    add-int/lit8 v1, v14, 0x1

    move v14, v1

    move-object v3, v11

    goto/16 :goto_0

    .line 187
    :cond_b
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget v1, v1, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_c

    const/4 v1, 0x1

    :goto_8
    if-nez v1, :cond_d

    .line 188
    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/q;->a:Ljava/lang/String;

    move-object v11, v3

    .line 190
    goto :goto_7

    .line 187
    :cond_c
    const/4 v1, 0x0

    goto :goto_8

    .line 192
    :cond_d
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v1, v1, Lcom/google/maps/g/a/ff;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hg;->g()Lcom/google/maps/g/a/hg;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/google/maps/g/a/hg;

    .line 193
    const/4 v1, 0x0

    const/16 v2, -0x3039

    move-object/from16 v0, v16

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/r/a/al;->a(II)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v1

    .line 194
    iget-object v2, v11, Lcom/google/maps/g/a/hg;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/gu;

    invoke-virtual {v2}, Lcom/google/maps/g/a/gu;->d()Ljava/lang/String;

    move-result-object v5

    .line 195
    invoke-virtual {v10}, Lcom/google/maps/g/a/fk;->d()Ljava/lang/String;

    move-result-object v8

    .line 196
    invoke-static {v10}, Lcom/google/android/apps/gmm/map/i/b/b;->b(Lcom/google/maps/g/a/fk;)Ljava/lang/String;

    move-result-object v2

    .line 197
    if-nez v2, :cond_18

    .line 199
    invoke-static {v10}, Lcom/google/android/apps/gmm/map/i/b/b;->c(Lcom/google/maps/g/a/fk;)Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    .line 201
    :goto_9
    invoke-static {v10}, Lcom/google/android/apps/gmm/map/i/b/b;->a(Lcom/google/maps/g/a/fk;)Ljava/lang/String;

    move-result-object v6

    .line 202
    iget v2, v10, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v7, 0x20

    if-ne v2, v7, :cond_e

    const/4 v2, 0x1

    :goto_a
    if-eqz v2, :cond_10

    .line 204
    iget-object v2, v10, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    if-nez v2, :cond_f

    invoke-static {}, Lcom/google/maps/g/a/gy;->d()Lcom/google/maps/g/a/gy;

    move-result-object v2

    :goto_b
    iget v2, v2, Lcom/google/maps/g/a/gy;->d:I

    .line 203
    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/map/i/b/c;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v7

    .line 207
    :goto_c
    iget-object v2, v11, Lcom/google/maps/g/a/hg;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/bm;->h()Lcom/google/maps/g/a/bm;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/bm;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/i/b/c;->a(Lcom/google/maps/g/a/bm;)Ljava/lang/String;

    move-result-object v9

    .line 209
    if-nez v3, :cond_11

    .line 211
    move-object/from16 v0, p3

    invoke-static {v0, v4, v9, v5, v8}, Lcom/google/android/apps/gmm/map/i/b/c;->a(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 222
    :goto_d
    if-eqz v4, :cond_a

    .line 223
    iget v2, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/map/r/a/q;->a(Lcom/google/android/apps/gmm/map/b/a/ab;I)Lcom/google/b/a/an;

    move-result-object v9

    .line 225
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    .line 226
    iget v5, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    iget-object v8, v9, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Float;

    iget-object v9, v9, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Float;

    .line 224
    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/r/a/ag;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/y;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v1

    .line 228
    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 202
    :cond_e
    const/4 v2, 0x0

    goto :goto_a

    .line 204
    :cond_f
    iget-object v2, v10, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    goto :goto_b

    .line 203
    :cond_10
    const/4 v7, 0x0

    goto :goto_c

    .line 213
    :cond_11
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget v2, v2, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_12

    const/4 v2, 0x1

    :goto_e
    if-eqz v2, :cond_14

    .line 214
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v2, v2, Lcom/google/maps/g/a/ff;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hg;->g()Lcom/google/maps/g/a/hg;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/hg;

    iget v2, v2, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_13

    const/4 v2, 0x1

    :goto_f
    if-eqz v2, :cond_14

    .line 215
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v2, v2, Lcom/google/maps/g/a/ff;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hg;->g()Lcom/google/maps/g/a/hg;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/hg;

    iget-boolean v2, v2, Lcom/google/maps/g/a/hg;->l:Z

    if-eqz v2, :cond_14

    .line 216
    move-object/from16 v0, p3

    invoke-static {v0, v4, v9, v5, v8}, Lcom/google/android/apps/gmm/map/i/b/c;->c(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_d

    .line 213
    :cond_12
    const/4 v2, 0x0

    goto :goto_e

    .line 214
    :cond_13
    const/4 v2, 0x0

    goto :goto_f

    .line 219
    :cond_14
    move-object/from16 v0, p3

    invoke-static {v0, v4, v9, v5, v8}, Lcom/google/android/apps/gmm/map/i/b/c;->b(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_d

    .line 233
    :cond_15
    sget-object v2, Lcom/google/android/apps/gmm/map/r/a/q;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x23

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unsupported travel mode was found: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v11, v3

    goto/16 :goto_7

    .line 237
    :cond_16
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v13

    .line 238
    if-eqz v3, :cond_17

    .line 239
    iget-object v1, v3, Lcom/google/maps/g/a/hg;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/gu;

    invoke-virtual {v1}, Lcom/google/maps/g/a/gu;->d()Ljava/lang/String;

    move-result-object v13

    .line 243
    :cond_17
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v1, v1

    div-int/lit8 v1, v1, 0x3

    add-int/lit8 v8, v1, -0x1

    .line 244
    move-object/from16 v0, p1

    invoke-static {v0, v8}, Lcom/google/android/apps/gmm/map/r/a/q;->a(Lcom/google/android/apps/gmm/map/b/a/ab;I)Lcom/google/b/a/an;

    move-result-object v12

    .line 245
    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/aj;

    .line 246
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/b/a/ab;->c()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    sget-object v3, Lcom/google/maps/g/a/ez;->D:Lcom/google/maps/g/a/ez;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    iget-object v11, v12, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Float;

    .line 247
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    iget-object v12, v12, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v12, Ljava/lang/Float;

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v12

    .line 249
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v14

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v15

    .line 250
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v16

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-direct/range {v1 .. v20}, Lcom/google/android/apps/gmm/map/r/a/aj;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/maps/g/a/ez;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;IIIIIFFLjava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/maps/g/a/fk;Lcom/google/maps/g/by;Ljava/lang/String;)V

    .line 252
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/r/a/ag;->a(Lcom/google/android/apps/gmm/map/r/a/aj;III)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v1

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/apps/gmm/map/r/a/ag;

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/apps/gmm/map/r/a/ag;

    return-object v1

    :cond_18
    move-object v4, v2

    goto/16 :goto_9

    :cond_19
    move-object v4, v12

    goto/16 :goto_5
.end method
