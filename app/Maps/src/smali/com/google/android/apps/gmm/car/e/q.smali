.class Lcom/google/android/apps/gmm/car/e/q;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/e/m;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/e/m;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/e/q;->a:Lcom/google/android/apps/gmm/car/e/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/q;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/e/m;->l:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/q;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/m;->m:Lcom/google/android/apps/gmm/car/e/t;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 290
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/car/e/m;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/q;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/m;->g:Lcom/google/android/apps/gmm/car/e/as;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x29

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Timed out waiting for navigation focus. @"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/q;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/m;->m:Lcom/google/android/apps/gmm/car/e/t;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/e/t;->a()V

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/q;->a:Lcom/google/android/apps/gmm/car/e/m;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/e/m;->l:Z

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/q;->a:Lcom/google/android/apps/gmm/car/e/m;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/e/m;->m:Lcom/google/android/apps/gmm/car/e/t;

    .line 294
    return-void
.end method
