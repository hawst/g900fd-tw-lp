.class public Lcom/google/android/apps/gmm/navigation/h/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/navigation/h/d;

.field public b:Lcom/google/android/apps/gmm/navigation/h/d;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/map/internal/d/bd;)V
    .locals 9

    .prologue
    const/16 v8, 0xe

    const/4 v7, 0x1

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v0, :cond_1

    invoke-virtual {p2, v1}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v2

    :goto_0
    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/be;

    .line 38
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v6

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/navigation/h/d;

    new-instance v3, Lcom/google/android/apps/gmm/navigation/h/b;

    const/16 v1, 0xc8

    invoke-direct {v3, v8, v1, v7, v6}, Lcom/google/android/apps/gmm/navigation/h/b;-><init>(IIZLcom/google/android/apps/gmm/shared/net/a/b;)V

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    .line 42
    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    const-string v5, "vector"

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/h/d;-><init>(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/map/internal/d/ai;Lcom/google/android/apps/gmm/navigation/h/b;Ljava/util/EnumSet;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/a;->a:Lcom/google/android/apps/gmm/navigation/h/d;

    .line 45
    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 46
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v0, :cond_0

    invoke-virtual {p2, v1}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v2

    :goto_1
    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/al;

    .line 50
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    .line 51
    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v1, v0, Lcom/google/r/b/a/qz;->n:I

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/navigation/h/d;

    new-instance v3, Lcom/google/android/apps/gmm/navigation/h/b;

    invoke-direct {v3, v8, v1, v7, v6}, Lcom/google/android/apps/gmm/navigation/h/b;-><init>(IIZLcom/google/android/apps/gmm/shared/net/a/b;)V

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    .line 55
    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    const-string v5, "road graph"

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/h/d;-><init>(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/map/internal/d/ai;Lcom/google/android/apps/gmm/navigation/h/b;Ljava/util/EnumSet;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/a;->b:Lcom/google/android/apps/gmm/navigation/h/d;

    .line 58
    return-void

    :cond_0
    move-object v2, v0

    goto :goto_1

    :cond_1
    move-object v2, v0

    goto :goto_0
.end method
