.class Lcom/google/android/apps/gmm/map/internal/d/bb;
.super Lcom/google/android/apps/gmm/map/internal/d/aw;
.source "PG"


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/aw;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 217
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 259
    const/16 v0, 0x1000

    return v0
.end method

.method final a(Lcom/google/android/apps/gmm/map/internal/d/ac;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZLcom/google/android/apps/gmm/map/internal/d/r;Lcom/google/android/apps/gmm/map/internal/d/ag;)Lcom/google/android/apps/gmm/map/internal/d/as;
    .locals 10

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bb;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/b/a/ai;->C:Z

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    .line 230
    :goto_0
    const/16 v0, 0x100

    invoke-static {p2, v0}, Lcom/google/android/apps/gmm/map/internal/a/a;->a(Landroid/content/res/Resources;I)I

    move-result v3

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bb;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->w:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v0, v1, :cond_1

    .line 235
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/an;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/bb;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object v1, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/d/an;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/b/a/ai;IFLjava/util/Locale;)V

    .line 249
    :goto_1
    return-object v0

    .line 229
    :cond_0
    const/high16 v4, 0x3f800000    # 1.0f

    goto :goto_0

    .line 238
    :cond_1
    if-eqz p5, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bb;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/b/a/ai;->D:Z

    if-eqz v0, :cond_2

    const/4 v6, 0x1

    .line 239
    :goto_2
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/be;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/bb;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/bb;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 247
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/ai;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v8, p6

    :goto_3
    move-object v1, p1

    move-object v5, p3

    move-object v7, p4

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/internal/d/be;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/b/a/ai;IFLjava/util/Locale;ZLjava/io/File;Lcom/google/android/apps/gmm/map/internal/d/r;Lcom/google/android/apps/gmm/map/internal/d/ag;)V

    goto :goto_1

    .line 238
    :cond_2
    const/4 v6, 0x0

    goto :goto_2

    .line 247
    :cond_3
    const/4 v8, 0x0

    goto :goto_3
.end method

.method final b()Lcom/google/android/apps/gmm/map/internal/d/b/ap;
    .locals 2

    .prologue
    .line 254
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/bc;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/bb;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/bc;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    return-object v0
.end method

.method public final c()B
    .locals 1

    .prologue
    .line 264
    const/16 v0, 0x1b

    return v0
.end method
