.class public Lcom/google/android/apps/gmm/v/v;
.super Lcom/google/android/apps/gmm/v/ci;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/gmm/v/aa;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/ar;I)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    .line 19
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/v/ad;)V
    .locals 4

    .prologue
    .line 23
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/apps/gmm/v/aa;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/aa;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/v;->a:Lcom/google/android/apps/gmm/v/aa;

    .line 24
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/v;->a:Lcom/google/android/apps/gmm/v/aa;

    const-string v1, "Dummy entity for dash texture"

    iput-object v1, v0, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    .line 25
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/v;->a:Lcom/google/android/apps/gmm/v/aa;

    const v1, 0xffff

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ai;I)V

    .line 26
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/v;->a:Lcom/google/android/apps/gmm/v/aa;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    monitor-exit p0

    return-void

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/v/ad;)V
    .locals 4

    .prologue
    .line 31
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/v;->a:Lcom/google/android/apps/gmm/v/aa;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/v;->a:Lcom/google/android/apps/gmm/v/aa;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/v;->a:Lcom/google/android/apps/gmm/v/aa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    :cond_0
    monitor-exit p0

    return-void

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
