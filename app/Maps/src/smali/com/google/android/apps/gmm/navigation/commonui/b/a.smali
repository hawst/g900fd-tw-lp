.class public Lcom/google/android/apps/gmm/navigation/commonui/b/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/navigation/c/a/a;

.field public final b:Ljava/lang/Float;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final c:Z

.field public final d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/commonui/b/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/navigation/commonui/b/b",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->d:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v2, v3, :cond_3

    :cond_0
    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->b:Ljava/lang/Float;

    if-nez v2, :cond_2

    :cond_1
    move v0, v1

    .line 27
    :cond_2
    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x22

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "zoomOverride is not valid in mode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 26
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v2, v0

    goto :goto_0

    .line 28
    :cond_4
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    const-string v1, "mode"

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    check-cast v0, Lcom/google/android/apps/gmm/navigation/c/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    .line 29
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->b:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->b:Ljava/lang/Float;

    .line 30
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->c:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->c:Z

    .line 31
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->d:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->d:Z

    .line 32
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/b/a/ah;
    .locals 3

    .prologue
    .line 76
    .line 77
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "mode"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    .line 78
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "zoomOverride"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->b:Ljava/lang/Float;

    .line 79
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "skipAnimations"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->c:Z

    .line 80
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "uiIsRestricted"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->d:Z

    .line 81
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    return-object v0
.end method
