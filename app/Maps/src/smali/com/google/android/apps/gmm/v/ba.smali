.class public Lcom/google/android/apps/gmm/v/ba;
.super Lcom/google/android/apps/gmm/v/bh;
.source "PG"


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/v/bh;-><init>(I)V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ba;->a:Ljava/util/ArrayList;

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ba;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ad;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ba;->f:Lcom/google/android/apps/gmm/v/n;

    if-nez v0, :cond_0

    .line 53
    :goto_0
    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ba;->f:Lcom/google/android/apps/gmm/v/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/bi;->b(Lcom/google/android/apps/gmm/v/aq;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ba;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_3

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ba;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    .line 45
    iget v3, p0, Lcom/google/android/apps/gmm/v/ba;->h:I

    iget-byte v5, v0, Lcom/google/android/apps/gmm/v/aa;->u:B

    iget-byte v6, v0, Lcom/google/android/apps/gmm/v/aa;->w:B

    and-int/2addr v5, v6

    and-int/2addr v3, v5

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_1

    .line 49
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ba;->f:Lcom/google/android/apps/gmm/v/n;

    invoke-virtual {v0, p1, p0, v3}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/bh;Lcom/google/android/apps/gmm/v/n;)V

    .line 41
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v3, v2

    .line 45
    goto :goto_2

    .line 52
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ba;->f:Lcom/google/android/apps/gmm/v/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ba;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/v/ad;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/v/ad;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ba;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    .line 70
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/aa;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 71
    sget-object v3, Lcom/google/android/apps/gmm/v/ab;->d:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v0, p1, v3}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    goto :goto_0

    .line 73
    :cond_0
    sget-object v3, Lcom/google/android/apps/gmm/v/ab;->b:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v0, p1, v3}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    .line 74
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 77
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    .line 78
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ba;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 81
    :cond_2
    return-object v1
.end method

.method public final b(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ba;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 64
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 91
    iget v0, p0, Lcom/google/android/apps/gmm/v/ba;->h:I

    invoke-static {v0}, Lcom/google/android/apps/gmm/v/aa;->a(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x11

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "OpaqueRenderBin["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
