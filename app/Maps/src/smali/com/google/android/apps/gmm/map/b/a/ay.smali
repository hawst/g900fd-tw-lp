.class public final Lcom/google/android/apps/gmm/map/b/a/ay;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lcom/google/android/apps/gmm/map/b/a/ay;


# instance fields
.field public b:F

.field public c:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ay;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 48
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput p1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 72
    iput p2, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 73
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 2

    .prologue
    .line 307
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 308
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v1, v1

    iput v1, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 309
    iput v0, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 310
    return-object p1
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 2

    .prologue
    .line 173
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    sub-float/2addr v0, v1

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 174
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    sub-float/2addr v0, v1

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 175
    return-object p2
.end method

.method public static a([Lcom/google/android/apps/gmm/map/b/a/ay;FLcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 11

    .prologue
    .line 456
    float-to-double v0, p1

    const/4 v2, 0x0

    aget-object v2, p0, v2

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v2, v2

    const/4 v4, 0x1

    aget-object v4, p0, v4

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v4, v4

    const/4 v6, 0x2

    aget-object v6, p0, v6

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v6, v6

    const/4 v8, 0x3

    aget-object v8, p0, v8

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v8, v8

    .line 457
    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v0

    double-to-float v10, v0

    float-to-double v0, p1

    const/4 v2, 0x0

    aget-object v2, p0, v2

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v2, v2

    const/4 v4, 0x1

    aget-object v4, p0, v4

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v4, v4

    const/4 v6, 0x2

    aget-object v6, p0, v6

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v6, v6

    const/4 v8, 0x3

    aget-object v8, p0, v8

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v8, v8

    .line 459
    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 456
    iput v10, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 461
    return-object p2
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 3

    .prologue
    .line 165
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    add-int/2addr v1, v2

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v0, 0x0

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 166
    return-object p2
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;FLcom/google/android/apps/gmm/map/b/a/ay;)V
    .locals 2

    .prologue
    .line 356
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    sub-float/2addr v0, v1

    mul-float/2addr v0, p2

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    add-float/2addr v0, v1

    iput v0, p3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 357
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    sub-float/2addr v0, v1

    mul-float/2addr v0, p2

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    add-float/2addr v0, v1

    iput v0, p3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 358
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)Z
    .locals 10

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    const/4 v8, 0x0

    .line 390
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    sub-float/2addr v1, v2

    .line 391
    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    sub-float/2addr v2, v3

    .line 393
    iget v3, p3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    sub-float/2addr v3, v4

    .line 394
    iget v4, p3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v5, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    sub-float/2addr v4, v5

    .line 397
    mul-float v5, v1, v4

    mul-float v6, v2, v3

    sub-float/2addr v5, v6

    .line 398
    cmpl-float v6, v5, v8

    if-nez v6, :cond_1

    .line 418
    :cond_0
    :goto_0
    return v0

    .line 403
    :cond_1
    iget v6, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v7, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    sub-float/2addr v6, v7

    mul-float/2addr v4, v6

    iget v6, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v7, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    sub-float/2addr v6, v7

    mul-float/2addr v3, v6

    sub-float v3, v4, v3

    .line 404
    div-float/2addr v3, v5

    .line 405
    cmpg-float v4, v3, v8

    if-ltz v4, :cond_0

    cmpg-float v4, v9, v3

    if-ltz v4, :cond_0

    .line 410
    iget v4, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v6, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    sub-float/2addr v4, v6

    mul-float/2addr v4, v2

    iget v6, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v7, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, v1

    sub-float/2addr v4, v6

    .line 411
    neg-float v4, v4

    div-float/2addr v4, v5

    .line 412
    cmpg-float v5, v4, v8

    if-ltz v5, :cond_0

    cmpg-float v4, v9, v4

    if-ltz v4, :cond_0

    .line 416
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 417
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float v1, v3, v2

    add-float/2addr v0, v1

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 418
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a([Lcom/google/android/apps/gmm/map/b/a/ay;[Lcom/google/android/apps/gmm/map/b/a/ay;)[Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x3

    const/4 v10, 0x0

    .line 431
    aget-object v0, p1, v10

    aget-object v1, p0, v10

    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 432
    aget-object v8, p1, v12

    aget-object v0, p0, v10

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v0, v0

    aget-object v2, p0, v12

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v2, v2

    aget-object v4, p0, v13

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v4, v4

    aget-object v6, p0, v11

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v6, v6

    .line 433
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDD)D

    move-result-wide v0

    double-to-float v9, v0

    aget-object v0, p0, v10

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v0, v0

    aget-object v2, p0, v12

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v2, v2

    aget-object v4, p0, v13

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v4, v4

    aget-object v6, p0, v11

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v6, v6

    .line 435
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 432
    iput v9, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v0, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 437
    aget-object v8, p1, v13

    aget-object v0, p0, v10

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v0, v0

    aget-object v2, p0, v12

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v2, v2

    aget-object v4, p0, v13

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v4, v4

    aget-object v6, p0, v11

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v6, v6

    .line 438
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/b/a/a;->b(DDDD)D

    move-result-wide v0

    double-to-float v9, v0

    aget-object v0, p0, v10

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v0, v0

    aget-object v2, p0, v12

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v2, v2

    aget-object v4, p0, v13

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v4, v4

    aget-object v6, p0, v11

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v6, v6

    .line 440
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/b/a/a;->b(DDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 437
    iput v9, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v0, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 442
    aget-object v0, p1, v11

    aget-object v1, p0, v11

    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 444
    return-object p1
.end method

.method public static b(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)F
    .locals 5

    .prologue
    .line 238
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    sub-float/2addr v0, v1

    .line 239
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    sub-float/2addr v1, v2

    .line 240
    iget v2, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    sub-float/2addr v2, v3

    .line 241
    iget v3, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v4, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    sub-float/2addr v3, v4

    .line 242
    mul-float/2addr v0, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0
.end method

.method public static b([Lcom/google/android/apps/gmm/map/b/a/ay;FLcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 11

    .prologue
    .line 474
    float-to-double v0, p1

    const/4 v2, 0x0

    aget-object v2, p0, v2

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v2, v2

    const/4 v4, 0x1

    aget-object v4, p0, v4

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v4, v4

    const/4 v6, 0x2

    aget-object v6, p0, v6

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v6, v6

    const/4 v8, 0x3

    aget-object v8, p0, v8

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v8, v8

    .line 475
    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/b/a/a;->b(DDDDD)D

    move-result-wide v0

    double-to-float v10, v0

    float-to-double v0, p1

    const/4 v2, 0x0

    aget-object v2, p0, v2

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v2, v2

    const/4 v4, 0x1

    aget-object v4, p0, v4

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v4, v4

    const/4 v6, 0x2

    aget-object v6, p0, v6

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v6, v6

    const/4 v8, 0x3

    aget-object v8, p0, v8

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v8, v8

    .line 477
    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/b/a/a;->b(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 474
    iput v10, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 479
    return-object p2
.end method

.method public static c(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)V
    .locals 6

    .prologue
    .line 366
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 367
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 368
    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 369
    iget v3, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 370
    mul-float v4, v2, v0

    mul-float v5, v3, v1

    sub-float/2addr v4, v5

    iput v4, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 371
    mul-float/2addr v1, v2

    mul-float/2addr v0, v3

    add-float/2addr v0, v1

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 372
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    return v0
.end method

.method public final a(F)Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 3

    .prologue
    .line 515
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 525
    :goto_0
    return-object p0

    .line 520
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    div-float v0, p1, v0

    .line 522
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 523
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    goto :goto_0
.end method

.method public final a(FF)Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 0

    .prologue
    .line 84
    iput p1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 85
    iput p2, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 86
    return-object p0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 2

    .prologue
    .line 127
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 128
    return-object p0
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 2

    .prologue
    .line 182
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    return-object p0
.end method

.method public final c()Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 280
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    cmpl-float v1, v0, v3

    if-nez v1, :cond_0

    iput v3, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v3, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    :goto_0
    return-object p0

    :cond_0
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    div-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    div-float v0, v1, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 484
    if-ne p0, p1, :cond_1

    .line 491
    :cond_0
    :goto_0
    return v0

    .line 487
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 488
    goto :goto_0

    .line 490
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 491
    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 496
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 505
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
