.class Lcom/google/android/apps/gmm/o/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/common/api/q;
.implements Lcom/google/android/gms/common/api/r;
.implements Lcom/google/android/gms/common/api/v;
.implements Lcom/google/android/gms/people/accountswitcherview/b;
.implements Lcom/google/android/gms/people/accountswitcherview/c;
.implements Lcom/google/android/gms/people/accountswitcherview/d;
.implements Lcom/google/android/gms/people/accountswitcherview/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/q;",
        "Lcom/google/android/gms/common/api/r;",
        "Lcom/google/android/gms/common/api/v",
        "<",
        "Lcom/google/android/gms/people/c;",
        ">;",
        "Lcom/google/android/gms/people/accountswitcherview/b;",
        "Lcom/google/android/gms/people/accountswitcherview/c;",
        "Lcom/google/android/gms/people/accountswitcherview/d;",
        "Lcom/google/android/gms/people/accountswitcherview/e;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

.field final b:Lcom/google/android/apps/gmm/base/activities/c;

.field final c:Lcom/google/android/gms/common/api/o;

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/people/model/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/view/View;

.field private final f:Lcom/google/android/apps/gmm/o/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/o/d;Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/google/android/apps/gmm/o/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 84
    iput-object p2, p0, Lcom/google/android/apps/gmm/o/a;->f:Lcom/google/android/apps/gmm/o/d;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/a;->d:Ljava/util/List;

    .line 86
    iput-object p4, p0, Lcom/google/android/apps/gmm/o/a;->e:Landroid/view/View;

    .line 87
    iput-object p3, p0, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    .line 89
    new-instance v0, Lcom/google/android/gms/people/i;

    invoke-direct {v0}, Lcom/google/android/gms/people/i;-><init>()V

    const/16 v1, 0x50

    .line 90
    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/i;->a(I)Lcom/google/android/gms/people/i;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/google/android/gms/people/i;->a()Lcom/google/android/gms/people/h;

    move-result-object v0

    .line 92
    new-instance v1, Lcom/google/android/gms/common/api/p;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/p;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/people/f;->c:Lcom/google/android/gms/common/api/a;

    .line 93
    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/a;Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    .line 94
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/q;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    .line 95
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/p;->a()Lcom/google/android/gms/common/api/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/a;->c:Lcom/google/android/gms/common/api/o;

    .line 97
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/o/a;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 33
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v5}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setSelectedAccount(Lcom/google/android/gms/people/model/a;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/a;

    invoke-interface {v0}, Lcom/google/android/gms/people/model/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    invoke-virtual {v1, v0, v5}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setSelectedAccount(Lcom/google/android/gms/people/model/a;Z)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/o/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->nd:I

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {v0}, Lcom/google/android/gms/people/model/a;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v0, :cond_0

    .line 187
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/login/LoginDialog;->a(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x33

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "GCore api connection suspended. Reason: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 144
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 136
    new-instance v0, Lcom/google/android/gms/people/b;

    invoke-direct {v0}, Lcom/google/android/gms/people/b;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/b;->a(Z)Lcom/google/android/gms/people/b;

    move-result-object v0

    .line 137
    sget-object v1, Lcom/google/android/gms/people/f;->d:Lcom/google/android/gms/people/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/o/a;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/a;->a(Lcom/google/android/gms/common/api/o;Lcom/google/android/gms/people/b;)Lcom/google/android/gms/common/api/s;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/s;->a(Lcom/google/android/gms/common/api/v;)V

    .line 138
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 3

    .prologue
    .line 167
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x25

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "gCore api connection failed. Reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/u;)V
    .locals 5

    .prologue
    .line 33
    check-cast p1, Lcom/google/android/gms/people/c;

    invoke-interface {p1}, Lcom/google/android/gms/people/c;->f()Lcom/google/android/gms/people/model/b;

    move-result-object v1

    if-eqz v1, :cond_4

    instance-of v0, v1, Ljava/util/Collection;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    if-ltz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    move-object v2, v0

    :goto_1
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/a;

    new-instance v4, Lcom/google/android/apps/gmm/u/d;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/u/d;-><init>(Lcom/google/android/gms/people/model/a;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v2, v0

    goto :goto_1

    :cond_3
    iput-object v2, p0, Lcom/google/android/apps/gmm/o/a;->d:Ljava/util/List;

    invoke-virtual {v1}, Lcom/google/android/gms/people/model/b;->d()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/o/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/o/c;-><init>(Lcom/google/android/apps/gmm/o/a;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/people/model/a;)V
    .locals 4

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v0, :cond_0

    .line 178
    :goto_0
    return-void

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/login/a/a;->a(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/gmm/login/a/b;)V

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->f:Lcom/google/android/apps/gmm/o/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/d;->c()V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v0, :cond_0

    .line 196
    :goto_0
    return-void

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/login/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/gmm/login/a/b;)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 200
    iget-object v1, p0, Lcom/google/android/apps/gmm/o/a;->e:Landroid/view/View;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;Z)Z

    .line 202
    return-void

    .line 200
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setNavigationMode(I)V

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setNavigationMode(I)V

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/a;->e:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;Z)Z

    .line 243
    return-void
.end method
