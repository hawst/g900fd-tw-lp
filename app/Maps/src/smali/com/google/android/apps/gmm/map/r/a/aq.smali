.class public final Lcom/google/android/apps/gmm/map/r/a/aq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/r/a/ar;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/android/apps/gmm/map/b/a/j;

.field public d:Lcom/google/android/apps/gmm/map/b/a/q;

.field public e:Lcom/google/android/apps/gmm/map/indoor/d/f;

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:[B

.field i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/ar;->a:Lcom/google/android/apps/gmm/map/r/a/ar;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->a:Lcom/google/android/apps/gmm/map/r/a/ar;

    .line 469
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    .line 474
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/r/a/ap;)V
    .locals 1

    .prologue
    .line 476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/ar;->a:Lcom/google/android/apps/gmm/map/r/a/ar;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->a:Lcom/google/android/apps/gmm/map/r/a/ar;

    .line 469
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    .line 477
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->a:Lcom/google/android/apps/gmm/map/r/a/ar;

    .line 478
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    .line 479
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 480
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 481
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->f:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->e:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 482
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    .line 483
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->h:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    .line 484
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->i:[B

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->h:[B

    .line 485
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->i:Ljava/lang/String;

    .line 486
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 10

    .prologue
    .line 567
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->a:Lcom/google/android/apps/gmm/map/r/a/ar;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->e:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    iget-boolean v7, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->h:[B

    iget-object v9, p0, Lcom/google/android/apps/gmm/map/r/a/aq;->i:Ljava/lang/String;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/r/a/ap;-><init>(Lcom/google/android/apps/gmm/map/r/a/ar;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/indoor/d/f;Ljava/lang/String;Z[BLjava/lang/String;)V

    return-object v0
.end method
