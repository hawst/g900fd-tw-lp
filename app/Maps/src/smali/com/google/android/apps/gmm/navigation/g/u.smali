.class public Lcom/google/android/apps/gmm/navigation/g/u;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/shared/net/ad;

.field private final b:Lcom/google/android/apps/gmm/shared/net/r;

.field private final c:Lcom/google/android/apps/gmm/shared/net/a;

.field private final d:Lcom/google/android/apps/gmm/map/util/b/g;

.field private final e:Lcom/google/android/apps/gmm/shared/net/a/l;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/shared/net/a;Lcom/google/android/apps/gmm/shared/net/a/l;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/g/u;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 63
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/u;->b:Lcom/google/android/apps/gmm/shared/net/r;

    .line 64
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/g/u;->c:Lcom/google/android/apps/gmm/shared/net/a;

    .line 65
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/u;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 66
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/g/u;->e:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 67
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/r/a/w;ZZDLcom/google/n/f;)Lcom/google/android/apps/gmm/directions/f/a;
    .locals 8
    .param p6    # Lcom/google/n/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 144
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    .line 144
    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v4

    .line 146
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->y:Lcom/google/r/b/a/afz;

    iget-object v0, v0, Lcom/google/r/b/a/afz;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ho;->d()Lcom/google/maps/g/a/ho;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ho;

    iget v1, v0, Lcom/google/maps/g/a/ho;->c:I

    invoke-static {v1}, Lcom/google/maps/g/a/hr;->a(I)Lcom/google/maps/g/a/hr;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/maps/g/a/hr;->a:Lcom/google/maps/g/a/hr;

    :cond_0
    sget-object v5, Lcom/google/maps/g/a/hr;->c:Lcom/google/maps/g/a/hr;

    if-ne v1, v5, :cond_2

    iget v0, v0, Lcom/google/maps/g/a/ho;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/hm;->f:Lcom/google/maps/g/a/hm;

    :cond_1
    :goto_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/w;->y:Lcom/google/r/b/a/afz;

    invoke-static {v1}, Lcom/google/r/b/a/afz;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v1

    invoke-static {}, Lcom/google/maps/g/a/ho;->newBuilder()Lcom/google/maps/g/a/hq;

    move-result-object v5

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    goto :goto_0

    :cond_3
    iget v6, v5, Lcom/google/maps/g/a/hq;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v5, Lcom/google/maps/g/a/hq;->a:I

    iget v0, v0, Lcom/google/maps/g/a/hm;->h:I

    iput v0, v5, Lcom/google/maps/g/a/hq;->b:I

    sget-object v0, Lcom/google/maps/g/a/hr;->c:Lcom/google/maps/g/a/hr;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget v6, v5, Lcom/google/maps/g/a/hq;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, v5, Lcom/google/maps/g/a/hq;->a:I

    iget v0, v0, Lcom/google/maps/g/a/hr;->d:I

    iput v0, v5, Lcom/google/maps/g/a/hq;->c:I

    invoke-virtual {v5}, Lcom/google/maps/g/a/hq;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ho;

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/agb;->a(Lcom/google/maps/g/a/ho;)Lcom/google/r/b/a/agb;

    move-result-object v1

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    if-nez v0, :cond_5

    move-object v0, v2

    :goto_1
    if-eqz v0, :cond_9

    invoke-static {}, Lcom/google/maps/g/a/io;->newBuilder()Lcom/google/maps/g/a/iq;

    move-result-object v5

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    if-nez v0, :cond_6

    move-object v0, v2

    :goto_2
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->u:Lcom/google/n/f;

    goto :goto_1

    :cond_6
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->u:Lcom/google/n/f;

    goto :goto_2

    :cond_7
    iget v6, v5, Lcom/google/maps/g/a/iq;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v5, Lcom/google/maps/g/a/iq;->a:I

    iput-object v0, v5, Lcom/google/maps/g/a/iq;->b:Lcom/google/n/f;

    if-nez p2, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    if-eqz v0, :cond_a

    move v0, v3

    :goto_3
    if-eqz v0, :cond_8

    double-to-int v0, p4

    iget v6, v5, Lcom/google/maps/g/a/iq;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, v5, Lcom/google/maps/g/a/iq;->a:I

    iput v0, v5, Lcom/google/maps/g/a/iq;->c:I

    :cond_8
    iget-object v0, v1, Lcom/google/r/b/a/agb;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/maps/g/a/iq;->g()Lcom/google/n/t;

    move-result-object v5

    iget-object v6, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, v1, Lcom/google/r/b/a/agb;->a:I

    :cond_9
    if-eqz p6, :cond_c

    iget-object v0, v1, Lcom/google/r/b/a/agb;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ao;->d()Lcom/google/maps/g/a/ao;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ao;

    invoke-static {v0}, Lcom/google/maps/g/a/ao;->a(Lcom/google/maps/g/a/ao;)Lcom/google/maps/g/a/aq;

    move-result-object v0

    invoke-static {}, Lcom/google/maps/g/a/gf;->newBuilder()Lcom/google/maps/g/a/gh;

    move-result-object v5

    if-nez p6, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_3

    :cond_b
    iget v6, v5, Lcom/google/maps/g/a/gh;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v5, Lcom/google/maps/g/a/gh;->a:I

    iput-object p6, v5, Lcom/google/maps/g/a/gh;->b:Lcom/google/n/f;

    iget-object v6, v0, Lcom/google/maps/g/a/aq;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/maps/g/a/gh;->g()Lcom/google/n/t;

    move-result-object v5

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v6, Lcom/google/n/ao;->d:Z

    iget v5, v0, Lcom/google/maps/g/a/aq;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, v0, Lcom/google/maps/g/a/aq;->a:I

    iget-object v5, v1, Lcom/google/r/b/a/agb;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/maps/g/a/aq;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v5, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v1, Lcom/google/r/b/a/agb;->a:I

    :cond_c
    invoke-virtual {v1}, Lcom/google/r/b/a/agb;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    .line 148
    new-instance v1, Lcom/google/android/apps/gmm/directions/f/b;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/directions/f/b;-><init>()V

    .line 149
    invoke-static {p0}, Lcom/google/android/apps/gmm/navigation/g/u;->a(Lcom/google/android/apps/gmm/map/r/b/a;)Lcom/google/maps/a/a;

    move-result-object v5

    iput-object v5, v1, Lcom/google/android/apps/gmm/directions/f/b;->d:Lcom/google/maps/a/a;

    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v5

    iput-object v5, v1, Lcom/google/android/apps/gmm/directions/f/b;->e:Lcom/google/o/b/a/v;

    .line 151
    iget-object v5, p1, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    iput-object v5, v1, Lcom/google/android/apps/gmm/directions/f/b;->a:Lcom/google/maps/g/a/hm;

    .line 152
    iput-boolean p3, v1, Lcom/google/android/apps/gmm/directions/f/b;->i:Z

    .line 153
    iput-object v0, v1, Lcom/google/android/apps/gmm/directions/f/b;->b:Lcom/google/r/b/a/afz;

    .line 154
    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/f/b;->c:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v0, :cond_d

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v0, v0

    if-le v0, v3, :cond_d

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v0, v0, v3

    :goto_4
    iget-object v2, v1, Lcom/google/android/apps/gmm/directions/f/b;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/f/b;->a()Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v0

    return-object v0

    :cond_d
    move-object v0, v2

    .line 155
    goto :goto_4
.end method

.method private static a(Lcom/google/android/apps/gmm/map/r/b/a;)Lcom/google/maps/a/a;
    .locals 9

    .prologue
    const/16 v8, 0x3e8

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 70
    invoke-static {}, Lcom/google/maps/a/a;->newBuilder()Lcom/google/maps/a/c;

    move-result-object v0

    .line 71
    invoke-static {}, Lcom/google/maps/a/e;->newBuilder()Lcom/google/maps/a/g;

    move-result-object v1

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    iget v4, v1, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v1, Lcom/google/maps/a/g;->a:I

    iput-wide v2, v1, Lcom/google/maps/a/g;->c:D

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v2

    iget v4, v1, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v1, Lcom/google/maps/a/g;->a:I

    iput-wide v2, v1, Lcom/google/maps/a/g;->b:D

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getAltitude()D

    move-result-wide v2

    iget v4, v1, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v1, Lcom/google/maps/a/g;->a:I

    iput-wide v2, v1, Lcom/google/maps/a/g;->d:D

    .line 71
    iget-object v2, v0, Lcom/google/maps/a/c;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/maps/a/g;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/maps/a/c;->a:I

    .line 75
    invoke-static {}, Lcom/google/maps/a/i;->newBuilder()Lcom/google/maps/a/k;

    move-result-object v1

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v2

    iget v3, v1, Lcom/google/maps/a/k;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/google/maps/a/k;->a:I

    iput v2, v1, Lcom/google/maps/a/k;->b:F

    .line 77
    iget v2, v1, Lcom/google/maps/a/k;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/maps/a/k;->a:I

    iput v7, v1, Lcom/google/maps/a/k;->c:F

    .line 78
    iget v2, v1, Lcom/google/maps/a/k;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v1, Lcom/google/maps/a/k;->a:I

    iput v7, v1, Lcom/google/maps/a/k;->d:F

    .line 75
    iget-object v2, v0, Lcom/google/maps/a/c;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/maps/a/k;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/maps/a/c;->a:I

    .line 80
    invoke-static {}, Lcom/google/maps/a/m;->newBuilder()Lcom/google/maps/a/o;

    move-result-object v1

    .line 81
    iget v2, v1, Lcom/google/maps/a/o;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/maps/a/o;->a:I

    iput v8, v1, Lcom/google/maps/a/o;->b:I

    .line 82
    iget v2, v1, Lcom/google/maps/a/o;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/maps/a/o;->a:I

    iput v8, v1, Lcom/google/maps/a/o;->c:I

    .line 80
    iget-object v2, v0, Lcom/google/maps/a/c;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/maps/a/o;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/google/maps/a/c;->a:I

    const/high16 v1, 0x41a00000    # 20.0f

    .line 84
    iget v2, v0, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v0, Lcom/google/maps/a/c;->a:I

    iput v1, v0, Lcom/google/maps/a/c;->e:F

    .line 85
    invoke-virtual {v0}, Lcom/google/maps/a/c;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/a;

    return-object v0
.end method

.method private static a(Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/r/b/a/ahc;
    .locals 3

    .prologue
    .line 217
    invoke-static {}, Lcom/google/r/b/a/aha;->newBuilder()Lcom/google/r/b/a/ahc;

    move-result-object v0

    .line 219
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->I:Lcom/google/n/f;

    .line 220
    if-eqz v1, :cond_1

    .line 221
    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/r/b/a/ahc;->a:I

    iput-object v1, v0, Lcom/google/r/b/a/ahc;->c:Lcom/google/n/f;

    .line 224
    :cond_1
    return-object v0
.end method

.method private a(Lcom/google/android/apps/gmm/navigation/g/i;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 374
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/g/i;->d:Z

    if-nez v0, :cond_1

    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/g/i;->a:Lcom/google/r/b/a/agd;

    iget-object v0, v1, Lcom/google/r/b/a/agd;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/afj;->g()Lcom/google/r/b/a/afj;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afj;

    iget-object v2, v0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v5, 0x2

    if-ne v2, v5, :cond_0

    iget-object v1, v1, Lcom/google/r/b/a/agd;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/v;->d()Lcom/google/o/b/a/v;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/b/a/v;

    iget-object v2, v0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/je;

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/navigation/g/i;->a(Lcom/google/maps/g/a/je;Lcom/google/o/b/a/v;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    iget-object v0, v0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/je;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/navigation/g/i;->a(Lcom/google/maps/g/a/je;Lcom/google/o/b/a/v;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    float-to-double v0, v0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v6

    div-double/2addr v0, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    cmpg-double v0, v0, v6

    if-gez v0, :cond_0

    move v0, v4

    :goto_0
    if-eqz v0, :cond_1

    move v0, v3

    :goto_1
    if-eqz v0, :cond_2

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/u;->b:Lcom/google/android/apps/gmm/shared/net/r;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 389
    :goto_2
    return-void

    :cond_0
    move v0, v3

    .line 374
    goto :goto_0

    :cond_1
    move v0, v4

    goto :goto_1

    .line 384
    :cond_2
    const-string v0, "Router"

    const-string v1, "Invalid request."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 385
    sget-object v0, Lcom/google/maps/g/a/z;->e:Lcom/google/maps/g/a/z;

    .line 386
    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/g/j;->a(Lcom/google/maps/g/a/z;)Lcom/google/android/apps/gmm/navigation/g/j;

    move-result-object v0

    .line 387
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/u;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v2, Lcom/google/android/apps/gmm/navigation/g/l;

    invoke-direct {v2, p1, v0}, Lcom/google/android/apps/gmm/navigation/g/l;-><init>(Lcom/google/android/apps/gmm/navigation/g/i;Lcom/google/android/apps/gmm/navigation/g/j;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/r/a/w;Z)Lcom/google/android/apps/gmm/navigation/g/i;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 237
    const-wide/16 v4, 0x0

    move-object v0, p1

    move-object v1, p2

    move v3, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/navigation/g/u;->a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/r/a/w;ZZDLcom/google/n/f;)Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v1

    .line 241
    invoke-static {p2}, Lcom/google/android/apps/gmm/navigation/g/u;->a(Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/r/b/a/ahc;

    move-result-object v0

    sget-object v3, Lcom/google/maps/g/wq;->c:Lcom/google/maps/g/wq;

    .line 242
    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v4, v0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/r/b/a/ahc;->a:I

    iget v3, v3, Lcom/google/maps/g/wq;->h:I

    iput v3, v0, Lcom/google/r/b/a/ahc;->b:I

    .line 243
    invoke-virtual {v0}, Lcom/google/r/b/a/ahc;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aha;

    .line 245
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/u;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/net/ad;->a()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    sget-object v3, Lcom/google/r/b/a/acy;->s:Lcom/google/r/b/a/acy;

    invoke-static {v3}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v3

    invoke-static {v1, v0, v3, v6}, Lcom/google/android/apps/gmm/directions/d/a;->a(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/r/b/a/aha;Ljava/util/List;Ljava/util/List;)Lcom/google/r/b/a/agd;

    move-result-object v1

    .line 246
    new-instance v3, Lcom/google/android/apps/gmm/navigation/g/i;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/u;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 247
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/r/b/d;->a:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_0
    if-nez v0, :cond_2

    :goto_1
    invoke-direct {v3, v1, v4, v2}, Lcom/google/android/apps/gmm/navigation/g/i;-><init>(Lcom/google/r/b/a/agd;Lcom/google/android/apps/gmm/shared/net/ad;Z)V

    .line 249
    invoke-direct {p0, v3}, Lcom/google/android/apps/gmm/navigation/g/u;->a(Lcom/google/android/apps/gmm/navigation/g/i;)V

    .line 250
    return-object v3

    :cond_1
    move v0, v7

    .line 247
    goto :goto_0

    :cond_2
    move v2, v7

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;[Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/maps/g/a/hm;Lcom/google/r/b/a/afz;Z)Lcom/google/android/apps/gmm/navigation/g/i;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 102
    array-length v2, p2

    add-int/lit8 v2, v2, 0x1

    new-array v3, v2, [Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 103
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/q;

    .line 104
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    .line 103
    invoke-static {v8, v2}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v2

    aput-object v2, v3, v1

    .line 105
    array-length v2, p2

    invoke-static {p2, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 107
    new-instance v4, Lcom/google/android/apps/gmm/directions/f/b;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/directions/f/b;-><init>()V

    .line 108
    invoke-static {p1}, Lcom/google/android/apps/gmm/navigation/g/u;->a(Lcom/google/android/apps/gmm/map/r/b/a;)Lcom/google/maps/a/a;

    move-result-object v2

    iput-object v2, v4, Lcom/google/android/apps/gmm/directions/f/b;->d:Lcom/google/maps/a/a;

    .line 109
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v2

    iput-object v2, v4, Lcom/google/android/apps/gmm/directions/f/b;->e:Lcom/google/o/b/a/v;

    .line 110
    iput-object p3, v4, Lcom/google/android/apps/gmm/directions/f/b;->a:Lcom/google/maps/g/a/hm;

    .line 111
    iput-boolean p5, v4, Lcom/google/android/apps/gmm/directions/f/b;->i:Z

    .line 112
    iput-object p4, v4, Lcom/google/android/apps/gmm/directions/f/b;->b:Lcom/google/r/b/a/afz;

    .line 113
    array-length v5, v3

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v3, v2

    .line 114
    iget-object v7, v4, Lcom/google/android/apps/gmm/directions/f/b;->c:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 116
    :cond_0
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/directions/f/b;->a()Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v2

    .line 118
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/u;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/net/ad;->a()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    sget-object v3, Lcom/google/r/b/a/acy;->s:Lcom/google/r/b/a/acy;

    invoke-static {v3}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v3

    invoke-static {v2, v8, v3, v8}, Lcom/google/android/apps/gmm/directions/d/a;->a(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/r/b/a/aha;Ljava/util/List;Ljava/util/List;)Lcom/google/r/b/a/agd;

    move-result-object v3

    .line 120
    new-instance v4, Lcom/google/android/apps/gmm/navigation/g/i;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/g/u;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 121
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/r/b/d;->a:Z

    if-eqz v2, :cond_1

    move v2, v0

    :goto_1
    if-nez v2, :cond_2

    :goto_2
    invoke-direct {v4, v3, v5, v0}, Lcom/google/android/apps/gmm/navigation/g/i;-><init>(Lcom/google/r/b/a/agd;Lcom/google/android/apps/gmm/shared/net/ad;Z)V

    .line 123
    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/navigation/g/u;->a(Lcom/google/android/apps/gmm/navigation/g/i;)V

    .line 124
    return-object v4

    :cond_1
    move v2, v1

    .line 121
    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/g/s;Lcom/google/android/apps/gmm/map/r/b/a;ILcom/google/n/f;Lcom/google/n/f;Z)Lcom/google/android/apps/gmm/navigation/g/i;
    .locals 20
    .param p4    # Lcom/google/n/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/n/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 273
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->K:Lcom/google/n/f;

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 274
    :cond_0
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 277
    const/4 v4, 0x0

    .line 279
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v6, v2, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    move-object/from16 v2, p2

    move/from16 v5, p6

    move-object/from16 v8, p5

    .line 277
    invoke-static/range {v2 .. v8}, Lcom/google/android/apps/gmm/navigation/g/u;->a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/r/a/w;ZZDLcom/google/n/f;)Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v10

    .line 283
    invoke-static {v3}, Lcom/google/android/apps/gmm/navigation/g/u;->a(Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/r/b/a/ahc;

    move-result-object v11

    sget-object v2, Lcom/google/maps/g/wq;->f:Lcom/google/maps/g/wq;

    .line 284
    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_1
    iget v4, v11, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v11, Lcom/google/r/b/a/ahc;->a:I

    iget v2, v2, Lcom/google/maps/g/wq;->h:I

    iput v2, v11, Lcom/google/r/b/a/ahc;->b:I

    .line 285
    iget v2, v11, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, v11, Lcom/google/r/b/a/ahc;->a:I

    move/from16 v0, p3

    iput v0, v11, Lcom/google/r/b/a/ahc;->h:I

    .line 288
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/u;->e:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v2, v2, Lcom/google/r/b/a/ou;->T:I

    int-to-long v12, v2

    .line 289
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v4, v2, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/map/r/a/w;->b(D)D

    move-result-wide v4

    double-to-long v14, v4

    .line 290
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/u;->e:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 291
    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v2, v2, Lcom/google/r/b/a/ou;->R:I

    int-to-long v4, v2

    sub-long v8, v14, v4

    .line 292
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/u;->e:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 293
    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v2, v2, Lcom/google/r/b/a/ou;->S:I

    int-to-long v4, v2

    sub-long v4, v14, v4

    move-wide v6, v8

    .line 297
    :goto_0
    cmp-long v2, v6, v14

    if-gez v2, :cond_4

    .line 298
    const/4 v2, 0x0

    .line 299
    invoke-virtual {v3, v6, v7}, Lcom/google/android/apps/gmm/map/r/a/w;->b(J)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/gmm/map/b/a/y;->j()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/gmm/map/b/a/q;->b()Lcom/google/maps/g/gy;

    move-result-object v16

    .line 298
    if-nez v16, :cond_2

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_2
    invoke-virtual {v11}, Lcom/google/r/b/a/ahc;->d()V

    iget-object v0, v11, Lcom/google/r/b/a/ahc;->e:Ljava/util/List;

    move-object/from16 v17, v0

    new-instance v18, Lcom/google/n/ao;

    invoke-direct/range {v18 .. v18}, Lcom/google/n/ao;-><init>()V

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, v18

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v0, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 297
    add-long/2addr v6, v12

    goto :goto_0

    .line 306
    :cond_3
    invoke-virtual {v11}, Lcom/google/r/b/a/ahc;->i()V

    iget-object v7, v11, Lcom/google/r/b/a/ahc;->f:Ljava/util/List;

    new-instance v14, Lcom/google/n/ao;

    invoke-direct {v14}, Lcom/google/n/ao;-><init>()V

    iget-object v15, v14, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v14, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v6, 0x0

    iput-object v6, v14, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v6, 0x1

    iput-boolean v6, v14, Lcom/google/n/ao;->d:Z

    invoke-interface {v7, v2, v14}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 305
    add-long/2addr v4, v12

    :cond_4
    cmp-long v2, v4, v8

    if-gez v2, :cond_5

    .line 306
    const/4 v2, 0x0

    .line 307
    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/map/r/a/w;->b(J)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/b/a/y;->j()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/b/a/q;->b()Lcom/google/maps/g/gy;

    move-result-object v6

    .line 306
    if-nez v6, :cond_3

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 311
    :cond_5
    invoke-static {}, Lcom/google/r/b/a/ahd;->newBuilder()Lcom/google/r/b/a/ahf;

    move-result-object v2

    .line 313
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/w;->K:Lcom/google/n/f;

    .line 312
    if-nez v3, :cond_6

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_6
    iget v4, v2, Lcom/google/r/b/a/ahf;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v2, Lcom/google/r/b/a/ahf;->a:I

    iput-object v3, v2, Lcom/google/r/b/a/ahf;->b:Lcom/google/n/f;

    .line 314
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v4, v3, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    double-to-int v3, v4

    iget v4, v2, Lcom/google/r/b/a/ahf;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v2, Lcom/google/r/b/a/ahf;->a:I

    iput v3, v2, Lcom/google/r/b/a/ahf;->c:I

    .line 316
    invoke-virtual {v2}, Lcom/google/r/b/a/ahf;->g()Lcom/google/n/t;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/ahd;

    if-nez v2, :cond_7

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_7
    invoke-virtual {v11}, Lcom/google/r/b/a/ahc;->j()V

    iget-object v3, v11, Lcom/google/r/b/a/ahc;->i:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    invoke-direct {v4}, Lcom/google/n/ao;-><init>()V

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v2, 0x1

    iput-boolean v2, v4, Lcom/google/n/ao;->d:Z

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x0

    .line 317
    iget v3, v11, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, v11, Lcom/google/r/b/a/ahc;->a:I

    iput v2, v11, Lcom/google/r/b/a/ahc;->j:I

    .line 320
    if-eqz p4, :cond_9

    .line 321
    if-nez p4, :cond_8

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_8
    iget v2, v11, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, v11, Lcom/google/r/b/a/ahc;->a:I

    move-object/from16 v0, p4

    iput-object v0, v11, Lcom/google/r/b/a/ahc;->g:Lcom/google/n/f;

    .line 325
    :cond_9
    invoke-virtual {v11}, Lcom/google/r/b/a/ahc;->g()Lcom/google/n/t;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/aha;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/u;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/net/ad;->a()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    sget-object v3, Lcom/google/r/b/a/acy;->s:Lcom/google/r/b/a/acy;

    invoke-static {v3}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v10, v2, v3, v4}, Lcom/google/android/apps/gmm/directions/d/a;->a(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/r/b/a/aha;Ljava/util/List;Ljava/util/List;)Lcom/google/r/b/a/agd;

    move-result-object v2

    .line 326
    new-instance v3, Lcom/google/android/apps/gmm/navigation/g/i;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/u;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    const/4 v5, 0x0

    invoke-direct {v3, v2, v4, v5}, Lcom/google/android/apps/gmm/navigation/g/i;-><init>(Lcom/google/r/b/a/agd;Lcom/google/android/apps/gmm/shared/net/ad;Z)V

    .line 329
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/navigation/g/u;->a(Lcom/google/android/apps/gmm/navigation/g/i;)V

    .line 330
    return-object v3
.end method
