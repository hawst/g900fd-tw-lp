.class public Lcom/google/android/apps/gmm/y/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:I

.field public final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/apps/gmm/y/d;->a:Landroid/content/Context;

    .line 23
    iput p2, p0, Lcom/google/android/apps/gmm/y/d;->b:I

    .line 24
    iput p3, p0, Lcom/google/android/apps/gmm/y/d;->c:I

    .line 25
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/y/f;)Landroid/text/SpannableStringBuilder;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 105
    const-string v0, " "

    new-instance v1, Lcom/google/b/a/ab;

    invoke-direct {v1, v0}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/y/d;->a:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/y/f;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/google/android/apps/gmm/y/d;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->ks:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 110
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v2

    .line 111
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 112
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    iget v4, p0, Lcom/google/android/apps/gmm/y/d;->c:I

    invoke-direct {v1, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3, v1, v2, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 113
    return-object v3
.end method
