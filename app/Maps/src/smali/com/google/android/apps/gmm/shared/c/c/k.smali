.class public Lcom/google/android/apps/gmm/shared/c/c/k;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/google/android/apps/gmm/shared/c/c/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/c/k;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method public static a(Ljava/util/Calendar;)J
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 494
    invoke-virtual {p0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 497
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 498
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 499
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 501
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;)Landroid/text/Spanned;
    .locals 4

    .prologue
    .line 124
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/c/j;-><init>()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    invoke-static {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 141
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/l;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/shared/c/c/l;-><init>(I)V

    .line 143
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/c/m;->a:Lcom/google/android/apps/gmm/shared/c/c/m;

    if-ne p2, v1, :cond_0

    iget v1, v0, Lcom/google/android/apps/gmm/shared/c/c/l;->a:I

    if-lez v1, :cond_0

    .line 144
    sget-object p2, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    .line 148
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    .line 149
    iget v2, v0, Lcom/google/android/apps/gmm/shared/c/c/l;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v4, v2, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    iget-object v5, p3, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iput-object v2, v3, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 150
    iget v2, v0, Lcom/google/android/apps/gmm/shared/c/c/l;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v4, v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    iget-object v2, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v5, v2, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    iget-object v6, p3, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iput-object v2, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 151
    iget v2, v0, Lcom/google/android/apps/gmm/shared/c/c/l;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v5, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v5, v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    iget-object v2, v5, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v6, v2, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    iget-object v7, p3, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iput-object v2, v5, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 154
    sget-object v2, Lcom/google/android/apps/gmm/shared/c/c/m;->a:Lcom/google/android/apps/gmm/shared/c/c/m;

    if-ne p2, v2, :cond_1

    .line 155
    sget v0, Lcom/google/android/apps/gmm/l;->eF:I

    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v3, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    new-array v0, v10, [Ljava/lang/Object;

    aput-object v4, v0, v8

    aput-object v5, v0, v9

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    .line 181
    :goto_0
    const-string v1, "%s"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0

    .line 156
    :cond_1
    sget-object v2, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    if-ne p2, v2, :cond_4

    .line 157
    iget v2, v0, Lcom/google/android/apps/gmm/shared/c/c/l;->a:I

    if-lez v2, :cond_2

    .line 158
    sget v0, Lcom/google/android/apps/gmm/l;->cn:I

    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v5, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v5, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    new-array v0, v9, [Ljava/lang/Object;

    aput-object v3, v0, v8

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    .line 159
    sget v2, Lcom/google/android/apps/gmm/l;->cQ:I

    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v5, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    new-array v2, v9, [Ljava/lang/Object;

    aput-object v4, v2, v8

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    .line 160
    sget v3, Lcom/google/android/apps/gmm/l;->eG:I

    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v5, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v5, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v1, v3}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    new-array v1, v10, [Ljava/lang/Object;

    aput-object v0, v1, v8

    aput-object v2, v1, v9

    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    goto :goto_0

    .line 161
    :cond_2
    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/c/l;->b:I

    if-lez v0, :cond_3

    .line 162
    sget v0, Lcom/google/android/apps/gmm/l;->cQ:I

    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v3, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    new-array v0, v9, [Ljava/lang/Object;

    aput-object v4, v0, v8

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    .line 163
    sget v2, Lcom/google/android/apps/gmm/l;->cR:I

    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v4, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    new-array v2, v9, [Ljava/lang/Object;

    aput-object v5, v2, v8

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    .line 164
    sget v3, Lcom/google/android/apps/gmm/l;->eH:I

    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v5, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v5, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v1, v3}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    new-array v1, v10, [Ljava/lang/Object;

    aput-object v0, v1, v8

    aput-object v2, v1, v9

    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    goto/16 :goto_0

    .line 166
    :cond_3
    sget v0, Lcom/google/android/apps/gmm/l;->cR:I

    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v3, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    new-array v0, v9, [Ljava/lang/Object;

    aput-object v5, v0, v8

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    goto/16 :goto_0

    .line 169
    :cond_4
    iget v2, v0, Lcom/google/android/apps/gmm/shared/c/c/l;->a:I

    if-lez v2, :cond_5

    .line 170
    sget v2, Lcom/google/android/apps/gmm/j;->c:I

    iget v5, v0, Lcom/google/android/apps/gmm/shared/c/c/l;->a:I

    invoke-virtual {v1, v2, v5}, Lcom/google/android/apps/gmm/shared/c/c/e;->a(II)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v3, v5, v8

    invoke-virtual {v2, v5}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    .line 171
    sget v3, Lcom/google/android/apps/gmm/j;->d:I

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/c/l;->b:I

    invoke-virtual {v1, v3, v0}, Lcom/google/android/apps/gmm/shared/c/c/e;->a(II)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v4, v3, v8

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    .line 172
    sget v3, Lcom/google/android/apps/gmm/l;->eG:I

    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v5, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v5, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v1, v3}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    new-array v1, v10, [Ljava/lang/Object;

    aput-object v2, v1, v8

    aput-object v0, v1, v9

    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    goto/16 :goto_0

    .line 173
    :cond_5
    iget v2, v0, Lcom/google/android/apps/gmm/shared/c/c/l;->b:I

    if-lez v2, :cond_6

    .line 174
    sget v2, Lcom/google/android/apps/gmm/j;->d:I

    iget v3, v0, Lcom/google/android/apps/gmm/shared/c/c/l;->b:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/e;->a(II)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    .line 175
    sget v3, Lcom/google/android/apps/gmm/j;->e:I

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/c/l;->c:I

    invoke-virtual {v1, v3, v0}, Lcom/google/android/apps/gmm/shared/c/c/e;->a(II)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v5, v3, v8

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    .line 176
    sget v3, Lcom/google/android/apps/gmm/l;->eH:I

    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v5, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v5, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v1, v3}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    new-array v1, v10, [Ljava/lang/Object;

    aput-object v2, v1, v8

    aput-object v0, v1, v9

    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    goto/16 :goto_0

    .line 178
    :cond_6
    sget v2, Lcom/google/android/apps/gmm/j;->e:I

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/c/l;->c:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/e;->a(II)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    new-array v1, v9, [Ljava/lang/Object;

    aput-object v5, v1, v8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/maps/g/a/be;Lcom/google/android/apps/gmm/shared/c/c/m;)Ljava/lang/CharSequence;
    .locals 5
    .param p1    # Lcom/google/maps/g/a/be;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 100
    if-eqz p1, :cond_3

    .line 101
    iget v2, p1, Lcom/google/maps/g/a/be;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    .line 102
    iget v1, p1, Lcom/google/maps/g/a/be;->b:I

    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/shared/c/c/j;-><init>()V

    iget-object v3, v2, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v0}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p0, v1, p2, v2}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    .line 109
    :goto_1
    return-object v0

    :cond_0
    move v2, v1

    .line 101
    goto :goto_0

    .line 103
    :cond_1
    iget v2, p1, Lcom/google/maps/g/a/be;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    :goto_2
    if-eqz v0, :cond_3

    .line 104
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/c/k;->a:Ljava/lang/String;

    .line 105
    invoke-virtual {p1}, Lcom/google/maps/g/a/be;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 103
    goto :goto_2

    .line 108
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/c/k;->a:Ljava/lang/String;

    .line 109
    const-string v0, ""

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 248
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;JJLjava/util/TimeZone;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 347
    move-object v0, p0

    move-wide v1, p1

    move-object v3, p5

    move-wide v4, p3

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;JLjava/util/TimeZone;JLjava/util/TimeZone;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;JJLjava/util/TimeZone;I)Ljava/lang/String;
    .locals 9

    .prologue
    .line 414
    new-instance v1, Ljava/util/Formatter;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x32

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-direct {v1, v0}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 416
    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 417
    invoke-virtual {v0, p3, p4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 419
    invoke-virtual {p5}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    move v6, p6

    .line 414
    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    .line 419
    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;JLjava/util/TimeZone;I)Ljava/lang/String;
    .locals 9

    .prologue
    .line 295
    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p1

    move-object v6, p3

    move v7, p4

    invoke-static/range {v1 .. v7}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;JJLjava/util/TimeZone;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;JLjava/util/TimeZone;JLjava/util/TimeZone;)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    .line 363
    invoke-static {p3}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 364
    invoke-static {p6}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v1

    .line 365
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 366
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p4, p5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 377
    const-string v0, "%s \u2013 %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    mul-long v4, p1, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    mul-long v4, p4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/maps/g/a/fo;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 231
    iget v2, p1, Lcom/google/maps/g/a/fo;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    .line 232
    const-string v0, ""

    .line 234
    :goto_1
    return-object v0

    :cond_0
    move v2, v1

    .line 231
    goto :goto_0

    .line 236
    :cond_1
    iget-wide v2, p1, Lcom/google/maps/g/a/fo;->b:J

    .line 237
    iget v4, p1, Lcom/google/maps/g/a/fo;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    :goto_2
    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/maps/g/a/fo;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 234
    :goto_3
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 237
    goto :goto_2

    :cond_3
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    goto :goto_3
.end method

.method public static a(Lcom/google/maps/g/a/fo;)Ljava/util/Calendar;
    .locals 4

    .prologue
    .line 480
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 481
    iget v0, p0, Lcom/google/maps/g/a/fo;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 483
    invoke-virtual {p0}, Lcom/google/maps/g/a/fo;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 482
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 485
    :cond_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 486
    iget-wide v2, p0, Lcom/google/maps/g/a/fo;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 485
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 487
    return-object v1

    .line 481
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;IZ)[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 196
    .line 198
    if-nez p2, :cond_0

    .line 199
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;)Landroid/text/Spanned;

    move-result-object v1

    .line 200
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 218
    :goto_0
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    aput-object v1, v2, v4

    aput-object v0, v2, v5

    return-object v2

    .line 202
    :cond_0
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/16 v2, 0x3c

    if-le v1, v2, :cond_2

    .line 203
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;)Landroid/text/Spanned;

    move-result-object v0

    .line 204
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 205
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-lez p1, :cond_1

    sget v0, Lcom/google/android/apps/gmm/l;->da:I

    :goto_1
    new-array v3, v5, [Ljava/lang/Object;

    .line 207
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v4

    .line 205
    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget v0, Lcom/google/android/apps/gmm/l;->cZ:I

    goto :goto_1

    .line 209
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->cY:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 210
    const-string v2, "\\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 211
    aget-object v2, v1, v4

    .line 212
    array-length v3, v1

    if-le v3, v5, :cond_6

    .line 213
    aget-object v1, v1, v5

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_4

    :cond_3
    move v3, v5

    :goto_2
    if-eqz v3, :cond_5

    :goto_3
    move-object v1, v2

    goto :goto_0

    :cond_4
    move v3, v4

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_3

    :cond_6
    move-object v1, v2

    goto :goto_0
.end method
