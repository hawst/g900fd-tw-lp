.class public Lcom/google/android/apps/gmm/navigation/c/h;
.super Lcom/google/android/apps/gmm/navigation/c/a;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/navui/y;


# static fields
.field static final m:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/maps/g/a/hm;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field volatile l:Z

.field private n:Z

.field private o:Lcom/google/android/apps/gmm/navigation/g/b/k;

.field private p:[Lcom/google/android/apps/gmm/navigation/g/b/k;

.field private q:Lcom/google/android/apps/gmm/map/r/a/ag;

.field private r:Lcom/google/android/apps/gmm/map/f/t;

.field private final s:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 99
    const-class v0, Lcom/google/maps/g/a/hm;

    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    .line 101
    sput-object v0, Lcom/google/android/apps/gmm/navigation/c/h;->m:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    const v2, 0x47435000    # 50000.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/google/android/apps/gmm/navigation/c/h;->m:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    const v2, 0x463b8000    # 12000.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/google/android/apps/gmm/navigation/c/h;->m:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    const v2, 0x453b8000    # 3000.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/mylocation/b/f;Lcom/google/android/apps/gmm/navigation/d/a;ZZ)V
    .locals 8

    .prologue
    .line 125
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    .line 131
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v7

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    .line 125
    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/navigation/c/a;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/mylocation/b/f;Lcom/google/android/apps/gmm/navigation/d/a;ZLcom/google/android/apps/gmm/shared/net/a/b;)V

    .line 132
    new-instance v0, Lcom/google/android/apps/gmm/map/f/t;

    .line 133
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/f/t;-><init>(Lcom/google/android/apps/gmm/shared/c/f;Landroid/animation/TimeInterpolator;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/h;->r:Lcom/google/android/apps/gmm/map/f/t;

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/h;->r:Lcom/google/android/apps/gmm/map/f/t;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/f/t;->f:Z

    .line 135
    iput-boolean p7, p0, Lcom/google/android/apps/gmm/navigation/c/h;->s:Z

    .line 137
    return-void
.end method

.method private h()Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/h;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/h;->p:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/h;->p:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 218
    :cond_0
    const/4 v0, 0x0

    .line 230
    :goto_0
    return-object v0

    .line 220
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/c/h;->s:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/navigation/c/h;->m:Ljava/util/EnumMap;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/h;->h:Lcom/google/maps/g/a/hm;

    .line 221
    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 222
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/h;->p:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    array-length v1, v1

    new-array v3, v1, [Lcom/google/android/apps/gmm/map/b/a/ah;

    move v1, v2

    .line 223
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/c/h;->p:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    array-length v4, v4

    if-ge v1, v4, :cond_4

    .line 224
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/c/h;->p:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    aget-object v4, v4, v1

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/navigation/g/b/k;->a(F)Lcom/google/android/apps/gmm/map/b/a/ah;

    move-result-object v4

    aput-object v4, v3, v1

    .line 225
    aget-object v4, v3, v1

    if-nez v4, :cond_2

    .line 227
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/ah;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/c/h;->p:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v4, v5, v2}, Lcom/google/android/apps/gmm/map/b/a/ah;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;I)V

    aput-object v4, v3, v1

    .line 223
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 221
    :cond_3
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_1

    .line 230
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/c/h;->e()Lcom/google/android/apps/gmm/navigation/c/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/h;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/c/h;->c:Landroid/content/res/Resources;

    .line 231
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/c/h;->k:Landroid/graphics/Rect;

    .line 230
    invoke-interface {v0, v1, v3, v2, v4}, Lcom/google/android/apps/gmm/navigation/c/a/b;->a(Lcom/google/android/apps/gmm/map/r/b/a;[Lcom/google/android/apps/gmm/map/b/a/ah;Landroid/util/DisplayMetrics;Landroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/gmm/navigation/c/a/a;)V
    .locals 4

    .prologue
    .line 205
    sget-object v0, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne p1, v0, :cond_0

    .line 206
    new-instance v0, Lcom/google/android/apps/gmm/navigation/c/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/c/i;-><init>(Lcom/google/android/apps/gmm/navigation/c/h;)V

    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/c/h;->h()Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/c/h;->l:Z

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/c/h;->b:Lcom/google/android/apps/gmm/map/t;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v0, v3}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 208
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 146
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_3

    .line 147
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->k:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_2

    .line 153
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->k:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/c/h;->c()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/h;->b:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/h;->k:Landroid/graphics/Rect;

    invoke-static {v3, v0, v1}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/b/a/q;FLandroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/navigation/c/h;->a(Lcom/google/android/apps/gmm/map/a;Z)V

    .line 165
    :goto_2
    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->e:Lcom/google/android/apps/gmm/mylocation/b/f;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Lcom/google/android/apps/gmm/map/s/a;)V

    .line 188
    :goto_3
    return-void

    .line 146
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 153
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_1

    .line 159
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/c/h;->d()V

    goto :goto_2

    .line 169
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 175
    iget-boolean v3, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->f:Z

    if-eqz v3, :cond_7

    .line 176
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v3, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 177
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v3, :cond_5

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v3, v3

    if-le v3, v2, :cond_5

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v0, v0, v2

    :goto_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-boolean v2, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->c:Z

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/c/h;->c()V

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/c/h;->b:Lcom/google/android/apps/gmm/map/t;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v3

    if-nez v3, :cond_6

    :goto_5
    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/c/h;->k:Landroid/graphics/Rect;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/b/a/q;FLandroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/navigation/c/h;->a(Lcom/google/android/apps/gmm/map/a;Z)V

    goto :goto_3

    :cond_5
    move-object v0, v1

    goto :goto_4

    :cond_6
    iget-object v1, v3, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_5

    .line 181
    :cond_7
    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 182
    :cond_8
    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/h;->o:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 183
    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/h;->p:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 184
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/h;->o:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/h;->h:Lcom/google/maps/g/a/hm;

    .line 185
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->e:Z

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/c/h;->n:Z

    .line 186
    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/h;->q:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/h;->o:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/b;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {p0, p1, v1, v0}, Lcom/google/android/apps/gmm/navigation/c/h;->a(Lcom/google/android/apps/gmm/navigation/commonui/b/a;Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/b/a;)V

    goto/16 :goto_3
.end method

.method protected final a(ZZ)V
    .locals 3

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/c/h;->l:Z

    if-eqz v0, :cond_1

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 274
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/c/h;->n:Z

    if-nez v0, :cond_0

    .line 277
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/c/h;->h()Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v1

    .line 278
    if-eqz v1, :cond_0

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/h;->b:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 282
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/c/h;->r:Lcom/google/android/apps/gmm/map/f/t;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 283
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/c/h;->r:Lcom/google/android/apps/gmm/map/f/t;

    if-eqz p2, :cond_3

    const-wide/16 v0, 0x0

    :goto_2
    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/map/f/t;->b(J)Lcom/google/android/apps/gmm/map/f/b;

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/h;->b:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/h;->r:Lcom/google/android/apps/gmm/map/f/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/f/b;)V

    goto :goto_0

    .line 281
    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_1

    .line 283
    :cond_3
    const-wide/16 v0, 0x9c4

    goto :goto_2
.end method

.method protected final b(ZZ)V
    .locals 4

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/c/h;->e()Lcom/google/android/apps/gmm/navigation/c/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/h;->q:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/c/h;->k:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/c/h;->c:Landroid/content/res/Resources;

    .line 290
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 289
    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/navigation/c/a/b;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Landroid/graphics/Rect;Landroid/util/DisplayMetrics;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    .line 291
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/navigation/c/h;->a(ZZLcom/google/android/apps/gmm/map/f/a/a;)V

    .line 292
    return-void
.end method

.method protected final f()Lcom/google/android/apps/gmm/map/r/a/ag;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/h;->o:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/h;->o:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
