.class Lcom/google/android/apps/gmm/hotels/c/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/hotels/c/d;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/hotels/c/b;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/e/a/a/a/b;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/android/apps/gmm/z/b/l;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/hotels/c/b;Landroid/content/Context;Lcom/google/e/a/a/a/b;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/apps/gmm/hotels/c/c;->a:Lcom/google/android/apps/gmm/hotels/c/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    iput-object p2, p0, Lcom/google/android/apps/gmm/hotels/c/c;->b:Landroid/content/Context;

    .line 141
    iput-object p3, p0, Lcom/google/android/apps/gmm/hotels/c/c;->c:Lcom/google/e/a/a/a/b;

    .line 142
    iput-object p4, p0, Lcom/google/android/apps/gmm/hotels/c/c;->d:Ljava/lang/String;

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/c;->a:Lcom/google/android/apps/gmm/hotels/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/hotels/c/b;->a:Lcom/google/android/apps/gmm/base/g/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/c;->e:Lcom/google/android/apps/gmm/z/b/l;

    .line 144
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/c;->a:Lcom/google/android/apps/gmm/hotels/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/hotels/c/b;->a:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->cI:Lcom/google/b/f/t;

    aput-object v3, v0, v2

    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/c;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/c;->c:Lcom/google/e/a/a/a/b;

    const/4 v2, 0x7

    const/16 v3, 0x1c

    invoke-virtual {v0, v2, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/c;->c:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x1

    const/16 v2, 0x1c

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/c;->c:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x6

    .line 179
    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    .line 178
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    check-cast v0, Lcom/google/maps/g/hg;

    .line 180
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/c;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 182
    const/4 v0, 0x0

    return-object v0

    :cond_0
    move-object v0, v1

    .line 178
    goto :goto_0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/c;->c:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x3

    const/16 v2, 0x1c

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/c;->c:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x2

    const/16 v2, 0x1c

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 159
    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/c/c;->b:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->hb:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/c;->e:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method
