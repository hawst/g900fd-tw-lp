.class public Lcom/google/android/apps/gmm/map/util/b/e;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Lcom/google/b/b/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/b/o",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/reflect/Method;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 38
    invoke-static {}, Lcom/google/b/b/e;->a()Lcom/google/b/b/e;

    move-result-object v0

    .line 39
    sget-object v1, Lcom/google/b/b/av;->c:Lcom/google/b/b/av;

    invoke-virtual {v0, v1}, Lcom/google/b/b/e;->a(Lcom/google/b/b/av;)Lcom/google/b/b/e;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/util/b/f;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/util/b/f;-><init>()V

    .line 40
    invoke-virtual {v0}, Lcom/google/b/b/e;->d()V

    new-instance v2, Lcom/google/b/b/an;

    invoke-direct {v2, v0, v1}, Lcom/google/b/b/an;-><init>(Lcom/google/b/b/e;Lcom/google/b/b/k;)V

    sput-object v2, Lcom/google/android/apps/gmm/map/util/b/e;->a:Lcom/google/b/b/o;

    .line 37
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static a(Ljava/lang/Class;)Lcom/google/b/c/cv;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    .line 90
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 93
    const-class v5, Lcom/google/b/d/c;

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 94
    invoke-virtual {v1, v4}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 90
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 98
    :cond_1
    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Lcom/google/b/c/hu;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/b/c/hu",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/android/apps/gmm/map/util/b/l;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 65
    invoke-static {}, Lcom/google/b/c/ca;->n()Lcom/google/b/c/ca;

    move-result-object v4

    .line 67
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 68
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/b/e;->b(Ljava/lang/Class;)Lcom/google/b/c/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Method;

    .line 69
    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v1

    array-length v6, v1

    if-eq v6, v2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    array-length v1, v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x7d

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Method "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " has @Subscribe annotation, but requires "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " arguments.  Event handler methods must require a single argument."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    aget-object v6, v1, v3

    .line 71
    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/a/o;->a(Ljava/lang/reflect/Method;)Lcom/google/android/apps/gmm/shared/c/a/p;

    move-result-object v7

    const-class v1, Lcom/google/b/d/a;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    new-instance v1, Lcom/google/android/apps/gmm/map/util/b/l;

    invoke-direct {v1, p0, v0, v7}, Lcom/google/android/apps/gmm/map/util/b/l;-><init>(Ljava/lang/Object;Ljava/lang/reflect/Method;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    move-object v0, v1

    .line 72
    :goto_2
    invoke-interface {v4, v6, v0}, Lcom/google/b/c/hu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move v1, v3

    .line 71
    goto :goto_1

    :cond_2
    new-instance v1, Lcom/google/android/apps/gmm/map/util/b/m;

    invoke-direct {v1, p0, v0, v7}, Lcom/google/android/apps/gmm/map/util/b/m;-><init>(Ljava/lang/Object;Ljava/lang/reflect/Method;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    move-object v0, v1

    goto :goto_2

    .line 74
    :cond_3
    return-object v4
.end method

.method private static b(Ljava/lang/Class;)Lcom/google/b/c/cv;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/map/util/b/e;->a:Lcom/google/b/b/o;

    invoke-interface {v0, p0}, Lcom/google/b/b/o;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/cv;
    :try_end_0
    .catch Lcom/google/b/j/a/s; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    invoke-virtual {v0}, Lcom/google/b/j/a/s;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, v1

    check-cast v0, Ljava/lang/Throwable;

    const-class v2, Ljava/lang/Error;

    if-eqz v0, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    const-class v2, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method
