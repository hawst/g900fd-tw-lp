.class public Lcom/google/android/apps/gmm/map/internal/d/b/ai;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/b/s;


# static fields
.field private static final d:[B


# instance fields
.field a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

.field final b:Lcom/google/android/apps/gmm/map/internal/d/ac;

.field final c:Lcom/google/android/apps/gmm/shared/c/f;

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final g:Lcom/google/android/apps/gmm/map/internal/d/b/ap;

.field private final h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/map/internal/d/b/an;",
            ">;"
        }
    .end annotation
.end field

.field private final i:I

.field private final j:I

.field private final k:Lcom/google/android/apps/gmm/map/b/a/ai;

.field private final l:Lcom/google/android/apps/gmm/map/internal/d/aw;

.field private m:I

.field private n:I

.field private final o:Lcom/google/android/apps/gmm/map/util/b;

.field private final p:Lcom/google/android/apps/gmm/map/internal/d/b/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->d:[B

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;ILcom/google/android/apps/gmm/map/internal/d/b/ap;Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->m:I

    .line 103
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->n:I

    .line 286
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    .line 287
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->f:I

    .line 288
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->g:Lcom/google/android/apps/gmm/map/internal/d/b/ap;

    .line 289
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->C()I

    move-result v0

    const/16 v1, 0x100

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x20

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->i:I

    .line 290
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, p5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    const/16 v0, 0x300

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->i:I

    mul-int/lit8 v1, v1, 0xc

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->j:I

    .line 297
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->j:I

    invoke-static {v0}, Lcom/google/b/c/hj;->a(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    .line 303
    :goto_0
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/d/aw;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->l:Lcom/google/android/apps/gmm/map/internal/d/aw;

    .line 305
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 307
    new-instance v0, Lcom/google/android/apps/gmm/map/util/b;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v2

    invoke-direct {v0, v1, v2, p2}, Lcom/google/android/apps/gmm/map/util/b;-><init>(ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->o:Lcom/google/android/apps/gmm/map/util/b;

    .line 311
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, p5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 312
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->f:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->p:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 322
    :goto_1
    return-void

    .line 299
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->i:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->j:I

    .line 300
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->i:I

    invoke-static {v0}, Lcom/google/b/c/hj;->a(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    goto :goto_0

    .line 315
    :cond_1
    instance-of v0, p5, Lcom/google/android/apps/gmm/map/b/a/au;

    if-eqz v0, :cond_2

    .line 318
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->d:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->p:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    goto :goto_1

    .line 320
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->c:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->p:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    goto :goto_1
.end method

.method static a(Lcom/google/android/apps/gmm/shared/c/f;J)J
    .locals 7

    .prologue
    const-wide/16 v0, 0x0

    const-wide/16 v4, -0x1

    .line 262
    cmp-long v2, p1, v4

    if-eqz v2, :cond_0

    cmp-long v2, p1, v4

    if-eqz v2, :cond_0

    .line 264
    invoke-interface {p0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    invoke-interface {p0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    sub-long/2addr v2, v4

    add-long/2addr p1, v2

    .line 265
    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    move-wide p1, v0

    .line 271
    :cond_0
    return-wide p1
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/b/an;I)Lcom/google/android/apps/gmm/map/internal/c/bo;
    .locals 9

    .prologue
    .line 860
    if-eqz p3, :cond_2

    .line 861
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/l;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v3, p2, Lcom/google/android/apps/gmm/map/internal/d/b/an;->f:Lcom/google/android/apps/gmm/map/internal/d/b/n;

    .line 863
    iget-wide v4, v3, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    .line 862
    invoke-static {v0, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v3, p2, Lcom/google/android/apps/gmm/map/internal/d/b/an;->f:Lcom/google/android/apps/gmm/map/internal/d/b/n;

    .line 865
    iget-wide v6, v3, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 864
    invoke-static {v0, v6, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v6

    .line 866
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v8, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    move-object v3, p1

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/l;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;JJI)V

    .line 867
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v0

    iget-object v2, p2, Lcom/google/android/apps/gmm/map/internal/d/b/an;->f:Lcom/google/android/apps/gmm/map/internal/d/b/n;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    .line 870
    :cond_1
    :goto_0
    return-object v1

    :cond_2
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/l;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    invoke-direct {v1, v0, p1, v2}, Lcom/google/android/apps/gmm/map/internal/c/l;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;I)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/util/f;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/b/n;)Lcom/google/android/apps/gmm/map/internal/c/bt;
    .locals 21
    .param p1    # Lcom/google/android/apps/gmm/map/util/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/map/internal/d/b/n;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 885
    if-nez p1, :cond_1

    .line 886
    const/4 v3, 0x0

    .line 930
    :cond_0
    :goto_0
    return-object v3

    .line 888
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v2

    if-nez v2, :cond_3

    .line 889
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/l;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v4, :cond_2

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Uninitialized"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    move-object/from16 v0, p2

    invoke-direct {v2, v3, v0, v4}, Lcom/google/android/apps/gmm/map/internal/c/l;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;I)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/c/l;->a:Lcom/google/android/apps/gmm/map/internal/c/bt;

    goto :goto_0

    .line 891
    :cond_3
    if-nez p3, :cond_4

    .line 892
    const/4 v3, 0x0

    goto :goto_0

    .line 895
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 896
    move-object/from16 v0, p3

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    .line 895
    invoke-static {v2, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v6

    .line 897
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 898
    move-object/from16 v0, p3

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 897
    invoke-static {v2, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v8

    .line 902
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a(Lcom/google/android/apps/gmm/map/internal/d/b/k;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 903
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/ct;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 906
    move-object/from16 v0, p3

    iget v10, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->h:I

    .line 907
    move-object/from16 v0, p3

    iget-byte v11, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->i:B

    .line 908
    move-object/from16 v0, p3

    iget v12, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->g:I

    .line 909
    move-object/from16 v0, p3

    iget v13, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->f:I

    move-object/from16 v4, p2

    invoke-direct/range {v3 .. v13}, Lcom/google/android/apps/gmm/map/internal/c/ct;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJIBII)V

    .line 910
    move-object/from16 v0, p3

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v5, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iput v2, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    goto :goto_0

    .line 913
    :cond_5
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 914
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->b()I

    move-result v14

    .line 917
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v2

    if-ne v2, v14, :cond_7

    .line 918
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/l;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 920
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v2, :cond_6

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Uninitialized"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v10, v2, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    move-object/from16 v5, p2

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/gmm/map/internal/c/l;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;JJI)V

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/l;->a:Lcom/google/android/apps/gmm/map/internal/c/bt;

    .line 927
    :goto_1
    if-eqz v3, :cond_0

    .line 928
    move-object/from16 v0, p3

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v5, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iput v2, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    goto/16 :goto_0

    .line 922
    :cond_7
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->g:Lcom/google/android/apps/gmm/map/internal/d/b/ap;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v13

    .line 923
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v15

    .line 924
    move-object/from16 v0, p3

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->f:I

    move/from16 v20, v0

    move-object/from16 v12, p2

    move-wide/from16 v16, v6

    move-wide/from16 v18, v8

    .line 922
    invoke-interface/range {v11 .. v20}, Lcom/google/android/apps/gmm/map/internal/d/b/ap;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIIJJI)Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v3

    goto :goto_1
.end method

.method private a(JLjava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/d/b/n;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1057
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-static {p1, p2, p3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(JLjava/lang/String;)Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ab;I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v1

    .line 1058
    if-nez v1, :cond_1

    .line 1068
    :cond_0
    :goto_0
    return-object v0

    .line 1062
    :cond_1
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v1, v0, p4}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/util/f;Lcom/google/android/apps/gmm/map/internal/d/b/n;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/d/b/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1066
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v3, v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v3, :cond_0

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v3, v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v3, :cond_2

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_2
    throw v0
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/b/an;)Lcom/google/android/apps/gmm/map/internal/d/b/n;
    .locals 13

    .prologue
    .line 849
    const/4 v0, 0x0

    .line 850
    iget-object v12, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    monitor-enter v12

    .line 851
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/an;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    if-ne v1, v2, :cond_0

    .line 853
    iget-object v11, p1, Lcom/google/android/apps/gmm/map/internal/d/b/an;->f:Lcom/google/android/apps/gmm/map/internal/d/b/n;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;

    iget-object v1, v11, Lcom/google/android/apps/gmm/map/internal/d/b/n;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    iget-wide v2, v11, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    iget-wide v4, v11, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    iget v6, v11, Lcom/google/android/apps/gmm/map/internal/d/b/n;->d:I

    iget v7, v11, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    iget v8, v11, Lcom/google/android/apps/gmm/map/internal/d/b/n;->f:I

    iget v9, v11, Lcom/google/android/apps/gmm/map/internal/d/b/n;->g:I

    iget v10, v11, Lcom/google/android/apps/gmm/map/internal/d/b/n;->h:I

    iget-byte v11, v11, Lcom/google/android/apps/gmm/map/internal/d/b/n;->i:B

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/gmm/map/internal/d/b/n;-><init>(Lcom/google/android/apps/gmm/map/internal/d/b/k;JJIIIIIB)V

    .line 855
    :cond_0
    monitor-exit v12

    .line 856
    return-object v0

    .line 855
    :catchall_0
    move-exception v0

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Lcom/google/android/apps/gmm/map/util/f;Lcom/google/android/apps/gmm/map/internal/d/b/n;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/d/b/n;
    .locals 8
    .param p2    # Lcom/google/android/apps/gmm/map/internal/d/b/n;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 938
    .line 939
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a([B)Lcom/google/android/apps/gmm/map/internal/d/b/k;

    move-result-object v1

    .line 940
    if-nez v1, :cond_0

    .line 941
    const-string v1, "SDCardTileCache"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x21

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Could not extact tile header in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 962
    :goto_0
    return-object v0

    .line 944
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->a([B)Lcom/google/android/apps/gmm/map/internal/d/b/n;

    move-result-object v1

    .line 945
    if-nez v1, :cond_1

    .line 946
    const-string v1, "SDCardTileCache"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x47

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Could not unpack tile in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": total data length less than the header size"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 952
    :cond_1
    if-eqz p2, :cond_2

    .line 954
    iget-wide v2, p2, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    .line 953
    iput-wide v2, v1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    .line 956
    iget-wide v2, p2, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 955
    iput-wide v2, v1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 957
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 959
    iget v0, p2, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    .line 958
    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    :cond_2
    move-object v0, v1

    .line 962
    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/b/k;Lcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/map/internal/d/b/o;
    .locals 6

    .prologue
    .line 1244
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v0

    .line 1245
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v2, p2, v3}, Lcom/google/android/apps/gmm/map/b/a/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v2

    .line 1246
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/d/b/o;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/internal/d/b/o;-><init>()V

    .line 1247
    iput-object p1, v4, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 1248
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-static {v5, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v2

    iput-wide v2, v4, Lcom/google/android/apps/gmm/map/internal/d/b/o;->b:J

    .line 1250
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v0

    iput-wide v0, v4, Lcom/google/android/apps/gmm/map/internal/d/b/o;->c:J

    .line 1252
    return-object v4
.end method

.method private a(Lcom/google/android/apps/gmm/map/util/f;ILcom/google/android/apps/gmm/map/internal/d/b/n;)Lcom/google/android/apps/gmm/map/internal/d/b/o;
    .locals 6

    .prologue
    .line 1153
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/o;-><init>()V

    .line 1154
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->p:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 1156
    iget-wide v2, p3, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 1155
    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->b:J

    .line 1157
    iget-wide v2, p3, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->c:J

    .line 1158
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v1

    sub-int/2addr v1, p2

    .line 1159
    if-lez v1, :cond_0

    .line 1161
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 1162
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v3

    .line 1161
    invoke-static {v1, v2, p2, v3}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/b/a/ai;[BII)Lcom/google/android/apps/gmm/map/internal/c/cs;

    move-result-object v1

    .line 1163
    iget v2, v1, Lcom/google/android/apps/gmm/map/internal/c/cs;->a:I

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->g:I

    iget-byte v2, v1, Lcom/google/android/apps/gmm/map/internal/c/cs;->f:B

    iput-byte v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->i:B

    iget v2, v1, Lcom/google/android/apps/gmm/map/internal/c/cs;->b:I

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->h:I

    .line 1164
    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/cs;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->e:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1172
    :cond_0
    :goto_0
    return-object v0

    .line 1165
    :catch_0
    move-exception v0

    .line 1167
    const-string v1, "SDCardTileCache"

    const-string v2, " Error parsing vector tile data header for coords whileupdating client tile header to UWF"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1169
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(JLjava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/b/n;Z)V
    .locals 17

    .prologue
    .line 1104
    move-object/from16 v0, p5

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 1107
    const/4 v4, 0x0

    .line 1108
    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->b()I

    move-result v10

    .line 1109
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->p:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    if-eq v5, v6, :cond_1

    .line 1110
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    move-wide/from16 v0, p1

    move-object/from16 v2, p3

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(JLjava/lang/String;)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v4

    .line 1111
    if-nez v4, :cond_1

    .line 1148
    :cond_0
    :goto_0
    return-void

    .line 1116
    :cond_1
    sget-object v15, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->c:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    .line 1117
    const/4 v9, 0x0

    .line 1118
    const/4 v11, 0x0

    .line 1119
    if-eqz v4, :cond_8

    .line 1120
    sget-object v15, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    .line 1121
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    instance-of v5, v5, Lcom/google/android/apps/gmm/map/b/a/au;

    if-eqz v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->p:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 1122
    invoke-static {v5}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a(Lcom/google/android/apps/gmm/map/internal/d/b/k;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1123
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v0, v4, v10, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/util/f;ILcom/google/android/apps/gmm/map/internal/d/b/n;)Lcom/google/android/apps/gmm/map/internal/d/b/o;

    move-result-object v5

    .line 1125
    if-eqz v5, :cond_4

    .line 1126
    if-eqz p6, :cond_2

    .line 1127
    move-object/from16 v0, p5

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    iput v6, v5, Lcom/google/android/apps/gmm/map/internal/d/b/o;->e:I

    .line 1129
    :cond_2
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a()Lcom/google/android/apps/gmm/map/internal/d/b/n;

    move-result-object p5

    .line 1136
    :goto_1
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v9

    .line 1137
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v11

    move-object/from16 v14, p5

    .line 1140
    :goto_2
    if-eqz v14, :cond_3

    .line 1141
    sget-object v12, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->d:[B

    const/16 v16, 0x0

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-object/from16 v8, p3

    move-object/from16 v13, p4

    invoke-direct/range {v5 .. v16}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(JLjava/lang/String;[BII[BLcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/b/n;Lcom/google/android/apps/gmm/map/internal/d/b/ao;Lcom/google/android/apps/gmm/map/internal/d/b/an;)V

    .line 1145
    :cond_3
    if-eqz v4, :cond_0

    .line 1146
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v6, v5, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v6, :cond_0

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_0

    .line 1131
    :cond_4
    const/16 p5, 0x0

    .line 1133
    goto :goto_1

    .line 1134
    :cond_5
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->p:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->a()I

    move-result v5

    move-object/from16 v0, p5

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->a()I

    move-result v7

    if-lt v5, v7, :cond_6

    const/4 v5, 0x1

    :goto_3
    if-nez v5, :cond_7

    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    :cond_6
    const/4 v5, 0x0

    goto :goto_3

    :cond_7
    move-object/from16 v0, p5

    iput-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    goto :goto_1

    :cond_8
    move-object/from16 v14, p5

    goto :goto_2
.end method

.method private a(JLjava/lang/String;[BII[BLcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/b/n;Lcom/google/android/apps/gmm/map/internal/d/b/ao;Lcom/google/android/apps/gmm/map/internal/d/b/an;)V
    .locals 13

    .prologue
    .line 1182
    const/4 v2, 0x0

    .line 1183
    if-eqz p4, :cond_10

    .line 1184
    sub-int v2, p6, p5

    move v3, v2

    .line 1186
    :goto_0
    iget-object v11, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    monitor-enter v11

    .line 1189
    if-lez v3, :cond_2

    const/4 v2, 0x1

    move v6, v2

    .line 1190
    :goto_1
    if-eqz p11, :cond_3

    :try_start_0
    move-object/from16 v0, p11

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    move v5, v2

    .line 1191
    :goto_2
    if-eqz v5, :cond_4

    move-object/from16 v0, p11

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    .line 1192
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->e:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v2

    if-lez v2, :cond_4

    const/4 v2, 0x1

    move v4, v2

    .line 1193
    :goto_3
    iget v7, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->m:I

    if-eqz v6, :cond_5

    const/4 v2, 0x1

    :goto_4
    add-int/2addr v7, v2

    if-eqz v4, :cond_6

    const/4 v2, 0x1

    :goto_5
    sub-int/2addr v7, v2

    .line 1195
    iget v8, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->n:I

    if-eqz v6, :cond_7

    const/4 v2, 0x0

    :goto_6
    add-int/2addr v8, v2

    if-eqz v5, :cond_0

    if-eqz v4, :cond_8

    :cond_0
    const/4 v2, 0x0

    :goto_7
    sub-int v2, v8, v2

    .line 1198
    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->i:I

    if-gt v7, v4, :cond_1

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->j:I

    if-le v2, v4, :cond_9

    .line 1200
    :cond_1
    const-string v2, "SDCardTileCache"

    const-string v3, "Max pending inserts reached. Insertion of type %s for tile coords %s could not be finished, cache name: %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p10, v4, v5

    const/4 v5, 0x1

    aput-object p8, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1202
    monitor-exit v11

    .line 1239
    :goto_8
    return-void

    .line 1189
    :cond_2
    const/4 v2, 0x0

    move v6, v2

    goto :goto_1

    .line 1190
    :cond_3
    const/4 v2, 0x0

    move v5, v2

    goto :goto_2

    .line 1192
    :cond_4
    const/4 v2, 0x0

    move v4, v2

    goto :goto_3

    .line 1193
    :cond_5
    const/4 v2, 0x0

    goto :goto_4

    :cond_6
    const/4 v2, 0x0

    goto :goto_5

    .line 1195
    :cond_7
    const/4 v2, 0x1

    goto :goto_6

    :cond_8
    const/4 v2, 0x1

    goto :goto_7

    .line 1205
    :cond_9
    iput v7, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->m:I

    .line 1206
    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->n:I

    .line 1208
    if-eqz v6, :cond_f

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g:Lcom/google/android/apps/gmm/map/internal/d/b/aa;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/b/aa;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;

    move-object v9, v2

    .line 1210
    :goto_9
    iget-object v4, v9, Lcom/google/android/apps/gmm/map/internal/d/b/z;->e:Lcom/google/android/apps/gmm/map/util/f;

    .line 1211
    const/4 v2, 0x0

    .line 1212
    if-eqz p7, :cond_a

    .line 1213
    move-object/from16 v0, p7

    array-length v2, v0

    .line 1215
    :cond_a
    add-int v5, v3, v2

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/util/f;->a(I)V

    .line 1218
    if-lez v2, :cond_b

    .line 1219
    const/4 v5, 0x0

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v7

    const/4 v8, 0x0

    move-object/from16 v0, p7

    invoke-static {v0, v5, v7, v8, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1221
    :cond_b
    if-eqz v6, :cond_c

    .line 1222
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v4

    move-object/from16 v0, p4

    move/from16 v1, p5

    invoke-static {v0, v1, v4, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1225
    :cond_c
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/d/b/an;

    move-wide v4, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p10

    move-object/from16 v8, p8

    move-object/from16 v10, p9

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/gmm/map/internal/d/b/an;-><init>(JLjava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/b/ao;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/b/z;Lcom/google/android/apps/gmm/map/internal/d/b/n;)V

    .line 1227
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    move-object/from16 v0, p8

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1229
    if-eqz p11, :cond_e

    .line 1230
    move-object/from16 v0, p11

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->h:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    if-eqz v2, :cond_d

    .line 1231
    move-object/from16 v0, p11

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->h:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    iput-object v2, v3, Lcom/google/android/apps/gmm/map/internal/d/b/an;->h:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    .line 1232
    move-object/from16 v0, p11

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->g:I

    iput v2, v3, Lcom/google/android/apps/gmm/map/internal/d/b/an;->g:I

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    if-eqz v4, :cond_d

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    iput v2, v3, Lcom/google/android/apps/gmm/map/internal/d/b/z;->c:I

    .line 1235
    :cond_d
    move-object/from16 v0, p11

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    if-eqz v2, :cond_e

    .line 1236
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    move-object/from16 v0, p11

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/z;)Z

    .line 1239
    :cond_e
    monitor-exit v11

    goto/16 :goto_8

    :catchall_0
    move-exception v2

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1208
    :cond_f
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    .line 1209
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c()Lcom/google/android/apps/gmm/map/internal/d/b/z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v9

    goto :goto_9

    :cond_10
    move v3, v2

    goto/16 :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/c/bp;[B[BLcom/google/android/apps/gmm/map/internal/d/b/n;)V
    .locals 16

    .prologue
    .line 493
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->d(Lcom/google/android/apps/gmm/map/internal/c/bp;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 505
    :goto_0
    return-void

    .line 497
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/b/a/an;

    move-result-object v3

    .line 499
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    monitor-enter v15

    .line 500
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/apps/gmm/map/internal/d/b/an;

    .line 501
    if-eqz p2, :cond_1

    move-object/from16 v0, p2

    array-length v9, v0

    .line 502
    :goto_1
    iget-object v2, v3, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v6, v3, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    const/4 v8, 0x0

    sget-object v13, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    move-object/from16 v3, p0

    move-object/from16 v7, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p1

    move-object/from16 v12, p4

    invoke-direct/range {v3 .. v14}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(JLjava/lang/String;[BII[BLcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/b/n;Lcom/google/android/apps/gmm/map/internal/d/b/ao;Lcom/google/android/apps/gmm/map/internal/d/b/an;)V

    .line 505
    monitor-exit v15

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 501
    :cond_1
    const/4 v9, 0x0

    goto :goto_1
.end method

.method private a(ILjava/util/Locale;)Z
    .locals 2

    .prologue
    .line 594
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 595
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 596
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(ILjava/util/Locale;)V

    .line 597
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->g()V

    .line 598
    monitor-exit v1

    .line 599
    const/4 v0, 0x1

    .line 602
    :goto_0
    return v0

    .line 598
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 600
    :catch_0
    move-exception v0

    .line 601
    const-string v1, "SDCardTileCache"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 602
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Z
    .locals 6
    .param p2    # Ljava/lang/Long;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 1074
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    monitor-enter v1

    .line 1075
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;

    .line 1076
    if-eqz v0, :cond_4

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->b:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    if-eq v2, v3, :cond_4

    .line 1078
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->f:Lcom/google/android/apps/gmm/map/internal/d/b/n;

    .line 1079
    if-eqz p2, :cond_0

    .line 1080
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 1081
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1080
    invoke-static {v3, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    .line 1083
    :cond_0
    if-eqz p3, :cond_1

    .line 1084
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 1086
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1085
    invoke-static {v3, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v4

    .line 1084
    iput-wide v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 1088
    :cond_1
    if-eqz p4, :cond_2

    .line 1089
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    .line 1091
    :cond_2
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    if-eqz v2, :cond_3

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->f:Lcom/google/android/apps/gmm/map/internal/d/b/n;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/b/n;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v4

    if-lez v4, :cond_3

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v4

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->b()I

    move-result v5

    if-ne v4, v5, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->f:Lcom/google/android/apps/gmm/map/internal/d/b/n;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v2

    invoke-interface {v3, v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->a(Lcom/google/android/apps/gmm/map/internal/d/b/n;[B)V

    .line 1092
    :cond_3
    const/4 v0, 0x1

    monitor-exit v1

    .line 1095
    :goto_0
    return v0

    .line 1094
    :cond_4
    monitor-exit v1

    .line 1095
    const/4 v0, 0x0

    goto :goto_0

    .line 1094
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b(Lcom/google/android/apps/gmm/shared/c/f;J)J
    .locals 7

    .prologue
    const-wide/16 v0, 0x0

    const-wide/16 v4, -0x1

    .line 249
    cmp-long v2, p1, v4

    if-eqz v2, :cond_0

    cmp-long v2, p1, v4

    if-eqz v2, :cond_0

    .line 251
    invoke-interface {p0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    invoke-interface {p0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    add-long/2addr p1, v2

    .line 252
    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    move-wide p1, v0

    .line 258
    :cond_0
    return-wide p1
.end method

.method private b(Ljava/io/File;)Z
    .locals 14

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-eqz v0, :cond_0

    move v0, v7

    .line 392
    :goto_0
    return v0

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->c()J

    move-result-wide v10

    .line 353
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/b/x;->a:Lcom/google/android/apps/gmm/map/internal/d/b/x;

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/b/x;)Lcom/google/android/apps/gmm/map/internal/d/b/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v8

    .line 361
    :goto_1
    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/w;->h:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    :cond_1
    move v0, v7

    .line 363
    :goto_2
    if-eqz v0, :cond_2

    .line 365
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->f:I

    const/4 v3, -0x1

    new-instance v4, Ljava/util/Locale;

    const-string v5, ""

    invoke-direct {v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sget-object v6, Lcom/google/android/apps/gmm/map/internal/d/b/x;->a:Lcom/google/android/apps/gmm/map/internal/d/b/x;

    move-object v5, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;IILjava/util/Locale;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/b/x;)Lcom/google/android/apps/gmm/map/internal/d/b/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    .line 367
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->g()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 374
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->c()J

    move-result-wide v0

    sub-long/2addr v0, v10

    .line 375
    const-string v2, "SDCardTileCache"

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    .line 376
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    .line 377
    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/d/b/w;->i:Ljava/util/Locale;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v9, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    .line 378
    iget-object v9, v9, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-wide v10, v9, Lcom/google/android/apps/gmm/map/internal/d/b/w;->h:J

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit16 v12, v12, 0x88

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-direct {v9, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "Loaded cache: "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, " with "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " entries, data version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", locale: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms, creationTime: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v8, [Ljava/lang/Object;

    .line 375
    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 385
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/b/aj;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/map/internal/d/b/aj;-><init>(Lcom/google/android/apps/gmm/map/internal/d/b/ai;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    move v0, v7

    .line 392
    goto/16 :goto_0

    .line 355
    :catch_0
    move-exception v0

    move v0, v7

    goto/16 :goto_1

    :cond_4
    move v0, v8

    .line 361
    goto/16 :goto_2

    .line 368
    :catch_1
    move-exception v0

    .line 369
    const-string v1, "SDCardTileCache"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x10

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Creating cache: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v8

    .line 370
    goto/16 :goto_0
.end method

.method private d(Lcom/google/android/apps/gmm/map/internal/c/bp;)Z
    .locals 4

    .prologue
    .line 1402
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v0, :cond_0

    .line 1403
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1405
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/b/a/ai;->E:Z

    if-nez v0, :cond_1

    .line 1406
    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Can\'t insert a tile of type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " into SD cache"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1409
    :cond_1
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    const/16 v1, 0x15

    if-le v0, v1, :cond_2

    .line 1410
    const/4 v0, 0x0

    .line 1412
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private g()V
    .locals 6

    .prologue
    .line 608
    new-instance v1, Lcom/google/android/apps/gmm/m/a/b;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/m/a/b;-><init>()V

    .line 610
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->h:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/m/a/b;->writeLong(J)V

    .line 611
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v2

    .line 612
    iget-object v0, v1, Lcom/google/android/apps/gmm/m/a/b;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    const-string v0, "disk_creation_time_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 611
    :goto_0
    invoke-interface {v2, v3, v0}, Lcom/google/android/apps/gmm/m/d;->a([BLjava/lang/String;)I

    .line 623
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v0, v2, :cond_0

    .line 624
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/b/ak;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/map/internal/d/b/ak;-><init>(Lcom/google/android/apps/gmm/map/internal/d/b/ai;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 632
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/m/a/b;->close()V

    .line 633
    return-void

    .line 612
    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 632
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/m/a/b;->close()V

    throw v0
.end method

.method private h()J
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 641
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v1

    const-string v0, "disk_creation_time_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/m/d;->b(Ljava/lang/String;)[B

    move-result-object v0

    .line 643
    if-nez v0, :cond_1

    move-wide v0, v2

    .line 653
    :goto_1
    return-wide v0

    .line 641
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 647
    :cond_1
    new-instance v1, Lcom/google/android/apps/gmm/m/a/a;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/m/a/a;-><init>([B)V

    .line 649
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/m/a/a;->readLong()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_1

    .line 651
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v1

    const-string v0, "disk_creation_time_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/m/d;->a(Ljava/lang/String;)Z

    move-wide v0, v2

    .line 653
    goto :goto_1

    .line 651
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public final T_()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1292
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->c()J

    .line 1293
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1296
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1297
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1298
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1300
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    monitor-enter v1

    .line 1301
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;

    .line 1302
    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    if-eqz v9, :cond_2

    .line 1303
    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    iget-object v9, v9, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    if-eqz v9, :cond_1

    .line 1304
    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    iget-object v9, v9, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    .line 1306
    :cond_1
    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    iget-object v9, v9, Lcom/google/android/apps/gmm/map/internal/d/b/z;->e:Lcom/google/android/apps/gmm/map/util/f;

    if-eqz v9, :cond_2

    .line 1307
    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    iget-object v9, v9, Lcom/google/android/apps/gmm/map/internal/d/b/z;->e:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    .line 1310
    :cond_2
    sget-object v9, Lcom/google/android/apps/gmm/map/internal/d/b/am;->a:[I

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    invoke-virtual {v10}, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    goto :goto_0

    .line 1312
    :pswitch_0
    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1313
    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->h:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    if-eqz v9, :cond_0

    .line 1314
    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->h:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->d:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v9, v0}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1328
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1318
    :pswitch_1
    :try_start_1
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1321
    :pswitch_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1325
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->m:I

    .line 1326
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->n:I

    .line 1327
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1328
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1329
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 1334
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(Ljava/util/List;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-ne v0, v11, :cond_c

    move v1, v2

    .line 1341
    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v4, v3

    :goto_2
    if-ge v4, v9, :cond_4

    .line 1342
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b/z;

    .line 1343
    iget-object v10, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v10, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/z;)Z

    .line 1341
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_4
    move v4, v1

    .line 1346
    :goto_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/b/a/an;

    .line 1349
    iget-object v0, v1, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/a/c;

    iget-object v1, v1, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v6

    invoke-interface {v0, v1, v4, v12, v6}, Lcom/google/android/apps/gmm/map/internal/d/a/c;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    goto :goto_4

    .line 1337
    :catch_0
    move-exception v0

    .line 1338
    :try_start_3
    const-string v1, "SDCardTileCache"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1341
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v3

    :goto_5
    if-ge v1, v4, :cond_5

    .line 1342
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b/z;

    .line 1343
    iget-object v9, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v9, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/z;)Z

    .line 1341
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_5
    move v4, v2

    .line 1345
    goto :goto_3

    .line 1341
    :catchall_1
    move-exception v0

    move-object v1, v0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_6
    if-ge v3, v2, :cond_6

    .line 1342
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b/z;

    .line 1343
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/z;)Z

    .line 1341
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_6
    throw v1

    .line 1353
    :cond_7
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 1354
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Ljava/util/List;)I

    .line 1357
    :cond_8
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    .line 1358
    :goto_7
    if-ge v4, v5, :cond_a

    .line 1359
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;

    .line 1361
    iget v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->g:I

    if-lez v1, :cond_b

    .line 1365
    :try_start_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->b:J

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->c:Ljava/lang/String;

    iget v10, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->g:I

    invoke-virtual {v1, v8, v9, v6, v10}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(JLjava/lang/String;I)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    move-result v1

    if-ne v1, v11, :cond_b

    .line 1367
    const/4 v1, 0x2

    .line 1373
    :goto_8
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->h:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    if-eqz v6, :cond_9

    .line 1374
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->h:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->d:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 1375
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v8

    .line 1374
    invoke-interface {v6, v0, v1, v12, v8}, Lcom/google/android/apps/gmm/map/internal/d/a/c;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    .line 1358
    :cond_9
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_7

    .line 1370
    :catch_1
    move-exception v1

    move v1, v2

    goto :goto_8

    .line 1378
    :cond_a
    return-void

    :cond_b
    move v1, v3

    goto :goto_8

    :cond_c
    move v1, v3

    goto/16 :goto_1

    .line 1310
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bt;
    .locals 14

    .prologue
    .line 760
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v0, :cond_0

    .line 761
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 763
    :cond_0
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    const/16 v1, 0x15

    if-le v0, v1, :cond_1

    .line 764
    const/4 v1, 0x0

    .line 845
    :goto_0
    return-object v1

    .line 767
    :cond_1
    iget-object v12, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    monitor-enter v12

    .line 770
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;

    .line 771
    if-eqz v0, :cond_6

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    if-eqz v1, :cond_6

    .line 772
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/z;->e:Lcom/google/android/apps/gmm/map/util/f;

    .line 773
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v2

    .line 774
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v3

    .line 775
    if-nez v2, :cond_2

    .line 776
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 777
    invoke-direct {p0, p1, v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/b/an;I)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v1

    monitor-exit v12

    goto :goto_0

    .line 826
    :catchall_0
    move-exception v0

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 779
    :cond_2
    if-lez v2, :cond_6

    .line 780
    :try_start_1
    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->f:Lcom/google/android/apps/gmm/map/internal/d/b/n;

    .line 781
    iget-object v2, v13, Lcom/google/android/apps/gmm/map/internal/d/b/n;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 783
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a(Lcom/google/android/apps/gmm/map/internal/d/b/k;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 784
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/ct;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 786
    iget-wide v4, v13, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    .line 785
    invoke-static {v0, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 788
    iget-wide v6, v13, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 787
    invoke-static {v0, v6, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v6

    .line 789
    iget v8, v13, Lcom/google/android/apps/gmm/map/internal/d/b/n;->h:I

    .line 790
    iget-byte v9, v13, Lcom/google/android/apps/gmm/map/internal/d/b/n;->i:B

    .line 791
    iget v10, v13, Lcom/google/android/apps/gmm/map/internal/d/b/n;->g:I

    .line 792
    iget v11, v13, Lcom/google/android/apps/gmm/map/internal/d/b/n;->f:I

    move-object v2, p1

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/gmm/map/internal/c/ct;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJIBII)V

    .line 793
    iget v0, v13, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    .line 794
    :cond_3
    monitor-exit v12

    goto :goto_0

    .line 798
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->o:Lcom/google/android/apps/gmm/map/util/b;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->l:Lcom/google/android/apps/gmm/map/internal/d/aw;

    .line 799
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/d/aw;->c()B

    move-result v3

    .line 798
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/util/b;->a(I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v11

    .line 800
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v3

    const/4 v4, 0x0

    .line 801
    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v5

    .line 800
    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 803
    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->g:Lcom/google/android/apps/gmm/map/internal/d/b/ap;

    .line 804
    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v5

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 806
    iget-wide v6, v13, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    .line 805
    invoke-static {v2, v6, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v6

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 808
    iget-wide v8, v13, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 807
    invoke-static {v2, v8, v9}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v8

    .line 809
    iget v10, v13, Lcom/google/android/apps/gmm/map/internal/d/b/n;->f:I

    move-object v2, p1

    .line 803
    invoke-interface/range {v1 .. v10}, Lcom/google/android/apps/gmm/map/internal/d/b/ap;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIIJJI)Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v1

    .line 812
    if-eqz v1, :cond_5

    .line 813
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->f:Lcom/google/android/apps/gmm/map/internal/d/b/n;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 821
    :cond_5
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->o:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v0, v11}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    monitor-exit v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 816
    :catch_0
    move-exception v0

    .line 817
    :try_start_4
    const-string v1, "SDCardTileCache"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x25

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Could not create tile meta data in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 819
    :try_start_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->o:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v0, v11}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    monitor-exit v12

    const/4 v1, 0x0

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->o:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v1, v11}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    throw v0

    .line 826
    :cond_6
    monitor-exit v12
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 828
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-static {v1, p1}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/b/a/an;

    move-result-object v2

    .line 829
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v1, v2, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    .line 830
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v1, v2, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->l:Lcom/google/android/apps/gmm/map/internal/d/aw;

    invoke-virtual {v3, v4, v5, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(JLjava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/aw;)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v2

    .line 831
    if-nez v2, :cond_7

    .line 832
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 834
    :cond_7
    const/4 v1, 0x0

    .line 838
    :try_start_6
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/internal/d/b/an;)Lcom/google/android/apps/gmm/map/internal/d/b/n;

    move-result-object v0

    .line 837
    invoke-direct {p0, v2, v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/util/f;Lcom/google/android/apps/gmm/map/internal/d/b/n;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/d/b/n;

    move-result-object v0

    .line 839
    invoke-direct {p0, v2, p1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/util/f;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/b/n;)Lcom/google/android/apps/gmm/map/internal/c/bt;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-result-object v0

    .line 843
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v3, :cond_8

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_8
    :goto_1
    move-object v1, v0

    .line 845
    goto/16 :goto_0

    .line 840
    :catch_1
    move-exception v0

    .line 841
    :try_start_7
    const-string v3, "SDCardTileCache"

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1b

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Could not unpack tile in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ":"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v0, v4}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 843
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v3, :cond_9

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_9
    move-object v0, v1

    .line 844
    goto :goto_1

    .line 843
    :catchall_2
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v3, :cond_a

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_a
    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;I)V
    .locals 13

    .prologue
    .line 992
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 993
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->d(Lcom/google/android/apps/gmm/map/internal/c/bp;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1050
    :cond_0
    :goto_0
    return-void

    .line 997
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 998
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->p:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/internal/d/b/k;Lcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/map/internal/d/b/o;

    move-result-object v0

    .line 1000
    iput p2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->e:I

    .line 1001
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->d:[B

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->d:[B

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a()Lcom/google/android/apps/gmm/map/internal/d/b/n;

    move-result-object v0

    invoke-direct {p0, p1, v1, v2, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[B[BLcom/google/android/apps/gmm/map/internal/d/b/n;)V

    goto :goto_0

    .line 1006
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v4

    .line 1011
    const/4 v0, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 1012
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1011
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1016
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/b/a/an;

    move-result-object v7

    .line 1017
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v0, v7, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    .line 1018
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, v7, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->l:Lcom/google/android/apps/gmm/map/internal/d/aw;

    invoke-virtual {v1, v2, v3, v0, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(JLjava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/aw;)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v3

    .line 1019
    if-nez v3, :cond_3

    .line 1020
    const-string v0, "SDCardTileCache"

    const-string v1, "Could not retrieve the record for %s that should exist on disk cache"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1024
    :cond_3
    const/4 v2, 0x0

    .line 1025
    const/4 v1, 0x0

    .line 1027
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v3, v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/util/f;Lcom/google/android/apps/gmm/map/internal/d/b/n;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/d/b/n;

    move-result-object v1

    .line 1028
    invoke-direct {p0, v3, p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/util/f;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/b/n;)Lcom/google/android/apps/gmm/map/internal/c/bt;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1032
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v6, v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v6, :cond_4

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_4
    move-object v6, v1

    .line 1035
    :goto_1
    if-eqz v6, :cond_0

    .line 1039
    iput p2, v6, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    .line 1040
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 1041
    invoke-static {v1, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v2

    .line 1040
    iput-wide v2, v6, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 1042
    if-eqz v0, :cond_5

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bt;->g:Z

    if-nez v1, :cond_5

    .line 1043
    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    if-ne v0, p2, :cond_5

    .line 1044
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 1046
    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v2

    .line 1045
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v0

    .line 1044
    iput-wide v0, v6, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    .line 1048
    :cond_5
    iget-object v0, v7, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, v7, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    const/4 v7, 0x1

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(JLjava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/b/n;Z)V

    goto/16 :goto_0

    .line 1029
    :catch_0
    move-exception v0

    .line 1030
    :try_start_1
    const-string v6, "SDCardTileCache"

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x1b

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "Could not unpack tile in "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ":"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v0, v8}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1032
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v6, :cond_6

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_6
    move-object v6, v1

    move-object v0, v2

    .line 1033
    goto/16 :goto_1

    .line 1032
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v2, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v2, :cond_7

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_7
    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;JJ)V
    .locals 8

    .prologue
    .line 967
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->d(Lcom/google/android/apps/gmm/map/internal/c/bp;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 988
    :cond_0
    :goto_0
    return-void

    .line 971
    :cond_1
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 975
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/b/a/an;

    move-result-object v1

    .line 976
    iget-object v0, v1, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, v1, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v2, v3, v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(JLjava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/d/b/n;

    move-result-object v6

    .line 978
    if-eqz v6, :cond_0

    .line 981
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-static {v0, p2, p3}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v2

    iput-wide v2, v6, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    .line 983
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-static {v0, p4, p5}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v2

    iput-wide v2, v6, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 986
    iget-object v0, v1, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, v1, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    const/4 v7, 0x0

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(JLjava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/b/n;Z)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;)V
    .locals 2

    .prologue
    .line 515
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Don\'t store unencrypted tiles into SD cache."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;I)V
    .locals 9

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v0, :cond_0

    .line 430
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 432
    :cond_0
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    monitor-enter v8

    .line 433
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;

    .line 434
    if-eqz v0, :cond_4

    .line 436
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->h:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    if-eqz v1, :cond_1

    .line 437
    if-eqz p2, :cond_3

    .line 438
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/a/b;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->h:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    invoke-direct {v1, v2, p2}, Lcom/google/android/apps/gmm/map/internal/d/a/b;-><init>(Lcom/google/android/apps/gmm/map/internal/d/a/c;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    move-object p2, v1

    .line 443
    :cond_1
    :goto_0
    iget v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->g:I

    .line 444
    invoke-static {v1, p3}, Lcom/google/android/apps/gmm/map/internal/d/aj;->a(II)I

    move-result v1

    .line 443
    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->g:I

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    iput v1, v2, Lcom/google/android/apps/gmm/map/internal/d/b/z;->c:I

    .line 445
    :cond_2
    iput-object p2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->h:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    .line 453
    :goto_1
    monitor-exit v8

    return-void

    .line 440
    :cond_3
    iget-object p2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->h:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    goto :goto_0

    .line 448
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/b/a/an;

    move-result-object v4

    .line 449
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/b/an;

    iget-object v0, v4, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, v4, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    move v5, p3

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/internal/d/b/an;-><init>(JLjava/lang/String;ILcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 453
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;[B[BLcom/google/android/apps/gmm/shared/c/f;I)V
    .locals 5

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->p:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    invoke-direct {p0, v0, p4}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/internal/d/b/k;Lcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/map/internal/d/b/o;

    move-result-object v0

    .line 461
    iput p5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->f:I

    .line 465
    array-length v1, p2

    if-lez v1, :cond_2

    array-length v1, p3

    if-lez v1, :cond_2

    .line 466
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    instance-of v1, v1, Lcom/google/android/apps/gmm/map/b/a/au;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->p:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 467
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a(Lcom/google/android/apps/gmm/map/internal/d/b/k;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->p:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/b/c;->e:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 468
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 470
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-static {v1, p2, p3}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/b/a/ai;[B[B)Lcom/google/android/apps/gmm/map/internal/c/cs;

    move-result-object v1

    .line 472
    iget v2, v1, Lcom/google/android/apps/gmm/map/internal/c/cs;->a:I

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->g:I

    iget-byte v2, v1, Lcom/google/android/apps/gmm/map/internal/c/cs;->f:B

    iput-byte v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->i:B

    iget v2, v1, Lcom/google/android/apps/gmm/map/internal/c/cs;->b:I

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->h:I

    .line 473
    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/cs;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->e:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 482
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a()Lcom/google/android/apps/gmm/map/internal/d/b/n;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[B[BLcom/google/android/apps/gmm/map/internal/d/b/n;)V

    .line 483
    :goto_0
    return-void

    .line 476
    :catch_0
    move-exception v0

    const-string v0, "SDCardTileCache"

    const-string v1, " Error parsing vector tile data header for coords %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 477
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/c/bp;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 476
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/ai;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/j/ai;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v0, v1, :cond_0

    .line 400
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v1, p1, Lcom/google/android/apps/gmm/map/j/ai;->b:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->g:Lcom/google/android/apps/gmm/map/internal/d/b/aa;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/aa;->a(F)I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/util/b;->a(F)I

    .line 402
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 586
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v0, :cond_0

    .line 587
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 589
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/w;->i:Ljava/util/Locale;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(ILjava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 553
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v0, :cond_0

    .line 554
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 557
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 558
    const/4 v0, 0x1

    .line 561
    :goto_0
    return v0

    .line 559
    :catch_0
    move-exception v0

    .line 560
    const-string v1, "SDCardTileCache"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 561
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bo;)Z
    .locals 1

    .prologue
    .line 1286
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/internal/c/l;

    return v0
.end method

.method public final declared-synchronized a(Ljava/io/File;)Z
    .locals 2

    .prologue
    .line 339
    monitor-enter p0

    :try_start_0
    const-string v0, "SDCardTileCache.initialize"

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/t;->a(Ljava/lang/String;)V

    .line 340
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b(Ljava/io/File;)Z

    move-result v0

    .line 341
    const-string v1, "SDCardTileCache.initialize"

    invoke-static {v1}, Lcom/google/android/apps/gmm/shared/c/t;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342
    monitor-exit p0

    return v0

    .line 339
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/util/Locale;)Z
    .locals 2

    .prologue
    .line 577
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v0, :cond_0

    .line 578
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 580
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(ILjava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public final a_(Lcom/google/android/apps/gmm/map/internal/c/bp;)V
    .locals 6

    .prologue
    .line 521
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->d:[B

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->d:[B

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[B[BLcom/google/android/apps/gmm/shared/c/f;I)V

    .line 522
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1392
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    monitor-enter v1

    .line 1393
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    .line 1394
    monitor-exit v1

    .line 1395
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    .line 1394
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1395
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/internal/c/bp;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1266
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v0, :cond_0

    .line 1267
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1269
    :cond_0
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    const/16 v3, 0x15

    if-le v0, v3, :cond_1

    move v0, v1

    .line 1278
    :goto_0
    return v0

    .line 1272
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/b/a/an;

    move-result-object v3

    .line 1273
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v0, v3, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v0, v3, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v6, v7, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c(JLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 1274
    goto :goto_0

    .line 1276
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    monitor-enter v3

    .line 1277
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;

    .line 1278
    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    monitor-exit v3

    goto :goto_0

    .line 1279
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    move v0, v1

    .line 1278
    goto :goto_1
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v0, :cond_0

    .line 528
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    return v0
.end method

.method public final c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;
    .locals 26

    .prologue
    .line 660
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v4, :cond_0

    .line 661
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Uninitialized"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 663
    :cond_0
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    const/16 v5, 0x15

    if-le v4, v5, :cond_2

    .line 664
    const/4 v4, 0x0

    .line 754
    :cond_1
    :goto_0
    return-object v4

    .line 667
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    move-object/from16 v16, v0

    monitor-enter v16

    .line 670
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->h:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/d/b/an;

    .line 671
    if-eqz v4, :cond_5

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    if-eqz v5, :cond_5

    .line 672
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/d/b/z;->e:Lcom/google/android/apps/gmm/map/util/f;

    .line 673
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v6

    .line 674
    iget-object v7, v4, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v7

    .line 675
    if-nez v6, :cond_3

    .line 676
    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/d/b/an;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 677
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/b/an;I)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v4

    monitor-exit v16

    goto :goto_0

    .line 707
    :catchall_0
    move-exception v4

    monitor-exit v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 679
    :cond_3
    if-lez v6, :cond_5

    .line 681
    :try_start_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->o:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v7, v6}, Lcom/google/android/apps/gmm/map/util/b;->a(I)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v17

    .line 682
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v5, v7, v8, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 684
    :try_start_2
    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/d/b/an;->f:Lcom/google/android/apps/gmm/map/internal/d/b/n;

    .line 685
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->g:Lcom/google/android/apps/gmm/map/internal/d/b/ap;

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v7

    const/4 v8, 0x0

    .line 686
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 687
    iget-wide v12, v6, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    invoke-static {v10, v12, v13}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 689
    iget-wide v14, v6, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 688
    invoke-static {v12, v14, v15}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 690
    invoke-interface {v14}, Lcom/google/android/apps/gmm/map/internal/d/ac;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v14

    .line 691
    iget v15, v6, Lcom/google/android/apps/gmm/map/internal/d/b/n;->f:I

    move-object/from16 v6, p1

    .line 685
    invoke-interface/range {v5 .. v15}, Lcom/google/android/apps/gmm/map/internal/d/b/ap;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIIJJLcom/google/android/apps/gmm/map/internal/c/o;I)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v5

    .line 694
    if-eqz v5, :cond_4

    .line 695
    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v6

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/an;->f:Lcom/google/android/apps/gmm/map/internal/d/b/n;

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    iget-object v7, v6, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v8, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v7, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    iput v4, v6, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 703
    :cond_4
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->o:Lcom/google/android/apps/gmm/map/util/b;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    monitor-exit v16
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v4, v5

    goto/16 :goto_0

    .line 699
    :catch_0
    move-exception v4

    .line 700
    :try_start_4
    const-string v5, "SDCardTileCache"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x1b

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Could not unpack tile in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ":"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v4, v6}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 701
    :try_start_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->o:Lcom/google/android/apps/gmm/map/util/b;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    monitor-exit v16

    const/4 v4, 0x0

    goto/16 :goto_0

    :catchall_1
    move-exception v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->o:Lcom/google/android/apps/gmm/map/util/b;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    throw v4

    .line 707
    :cond_5
    monitor-exit v16
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 709
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/b/a/an;

    move-result-object v6

    .line 710
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v5, v6, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-object v5, v6, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v7, v8, v9, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b(JLjava/lang/String;)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v24

    .line 711
    if-nez v24, :cond_6

    .line 712
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 715
    :cond_6
    :try_start_6
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v5

    if-nez v5, :cond_9

    .line 716
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/l;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v6, :cond_8

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Uninitialized"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 750
    :catch_1
    move-exception v4

    .line 751
    :try_start_7
    const-string v5, "SDCardTileCache"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->e:Ljava/lang/String;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x1b

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Could not unpack tile in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ":"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v4, v6}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 752
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v5, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v5, :cond_7

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 716
    :cond_8
    :try_start_8
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v6, v6, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    move-object/from16 v0, p1

    invoke-direct {v4, v5, v0, v6}, Lcom/google/android/apps/gmm/map/internal/c/l;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;I)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 754
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v6, v5, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v6, :cond_1

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto/16 :goto_0

    .line 720
    :cond_9
    :try_start_9
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/internal/d/b/an;)Lcom/google/android/apps/gmm/map/internal/d/b/n;

    move-result-object v4

    .line 719
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/map/util/f;Lcom/google/android/apps/gmm/map/internal/d/b/n;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/d/b/n;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    move-result-object v25

    .line 721
    if-nez v25, :cond_b

    .line 722
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v5, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v5, :cond_a

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v4

    if-eqz v4, :cond_a

    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 726
    :cond_b
    :try_start_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 727
    move-object/from16 v0, v25

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    .line 726
    invoke-static {v4, v6, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v8

    .line 728
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 729
    move-object/from16 v0, v25

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 728
    invoke-static {v4, v6, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;J)J

    move-result-wide v10

    .line 731
    move-object/from16 v0, v25

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 732
    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->b()I

    move-result v16

    .line 735
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v4

    move/from16 v0, v16

    if-ne v4, v0, :cond_f

    .line 736
    new-instance v5, Lcom/google/android/apps/gmm/map/internal/c/l;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v4, :cond_d

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Uninitialized"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 752
    :catchall_2
    move-exception v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v6, v5, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v6, :cond_c

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v5

    if-eqz v5, :cond_c

    :cond_c
    throw v4

    .line 736
    :cond_d
    :try_start_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget v12, v4, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    move-object/from16 v7, p1

    invoke-direct/range {v5 .. v12}, Lcom/google/android/apps/gmm/map/internal/c/l;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;JJI)V

    move-object v4, v5

    .line 744
    :goto_1
    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v6, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 745
    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v5

    .line 746
    move-object/from16 v0, v25

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    .line 745
    iget-object v7, v5, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v8, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v7, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    iput v6, v5, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 754
    :cond_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v6, v5, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v6, :cond_1

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto/16 :goto_0

    .line 738
    :cond_f
    :try_start_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->g:Lcom/google/android/apps/gmm/map/internal/d/b/ap;

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v15

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 739
    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/d/ac;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v22

    .line 740
    move-object/from16 v0, v25

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/n;->f:I

    move/from16 v23, v0

    move-object/from16 v14, p1

    move-wide/from16 v18, v8

    move-wide/from16 v20, v10

    .line 738
    invoke-interface/range {v13 .. v23}, Lcom/google/android/apps/gmm/map/internal/d/b/ap;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIIJJLcom/google/android/apps/gmm/map/internal/c/o;I)Lcom/google/android/apps/gmm/map/internal/c/bo;
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    move-result-object v4

    goto :goto_1
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 544
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v0, :cond_0

    .line 545
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 547
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->h:J

    return-wide v0
.end method

.method public final e()Ljava/util/Locale;
    .locals 2

    .prologue
    .line 568
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v0, :cond_0

    .line 569
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b:Lcom/google/android/apps/gmm/map/internal/d/b/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->i:Ljava/util/Locale;

    return-object v0
.end method

.method public final declared-synchronized f()V
    .locals 4

    .prologue
    .line 413
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    if-nez v0, :cond_0

    .line 414
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 417
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->b()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 422
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 423
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 425
    :cond_1
    monitor-exit p0

    return-void

    .line 418
    :catch_0
    move-exception v0

    .line 419
    :try_start_3
    const-string v1, "SDCardTileCache"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "shutDown(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
