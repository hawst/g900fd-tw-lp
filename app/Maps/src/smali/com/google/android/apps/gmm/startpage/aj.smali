.class Lcom/google/android/apps/gmm/startpage/aj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;"
        }
    .end annotation
.end field

.field final b:Ljava/lang/String;

.field final c:Lcom/google/android/apps/gmm/startpage/d/e;

.field final d:J

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/gmm/startpage/d/e;Lcom/google/r/b/a/tf;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/startpage/d/e;",
            "Lcom/google/r/b/a/tf;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 705
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/aj;->a:Ljava/util/List;

    .line 706
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/aj;->b:Ljava/lang/String;

    .line 707
    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/aj;->c:Lcom/google/android/apps/gmm/startpage/d/e;

    .line 708
    iput-wide p5, p0, Lcom/google/android/apps/gmm/startpage/aj;->d:J

    .line 710
    invoke-static {p1}, Lcom/google/android/apps/gmm/startpage/aj;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/aj;->e:Ljava/util/List;

    .line 711
    return-void
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 714
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v2

    .line 715
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    .line 716
    iget v1, v0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    .line 717
    invoke-virtual {v2, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_0

    .line 716
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 720
    :cond_2
    invoke-virtual {v2}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method
