.class public Lcom/google/android/apps/gmm/navigation/a/b/g;
.super Lcom/google/android/apps/gmm/navigation/a/b/a;
.source "PG"


# instance fields
.field public final a:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/a/b/a;-><init>()V

    .line 53
    iput p1, p0, Lcom/google/android/apps/gmm/navigation/a/b/g;->a:I

    .line 54
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 75
    instance-of v1, p1, Lcom/google/android/apps/gmm/navigation/a/b/g;

    if-eqz v1, :cond_0

    .line 76
    check-cast p1, Lcom/google/android/apps/gmm/navigation/a/b/g;

    iget v1, p1, Lcom/google/android/apps/gmm/navigation/a/b/g;->a:I

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/a/b/g;->a:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 78
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/a/b/g;->a:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/a/b/g;->a:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "<maneuver_message id=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
