.class public Lcom/google/android/apps/gmm/directions/i/u;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method private static a(Lcom/google/android/apps/gmm/o/a/a;Lcom/google/android/apps/gmm/o/a/c;Landroid/content/res/Resources;)Lcom/google/android/apps/gmm/base/views/c/e;
    .locals 3

    .prologue
    .line 61
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/f;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/views/c/f;-><init>()V

    .line 62
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/ab;->b:[I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/o/a/a;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    const-string v0, ""

    :goto_0
    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/f;->a:Ljava/lang/CharSequence;

    .line 63
    invoke-interface {p1, p0}, Lcom/google/android/apps/gmm/o/a/c;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    sget-object v0, Lcom/google/android/apps/gmm/directions/i/ab;->b:[I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/o/a/a;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/f;->d:Ljava/lang/Runnable;

    const/4 v0, 0x1

    .line 64
    iput-boolean v0, v1, Lcom/google/android/apps/gmm/base/views/c/f;->f:Z

    .line 65
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/aa;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/gmm/directions/i/aa;-><init>(Lcom/google/android/apps/gmm/o/a/c;Lcom/google/android/apps/gmm/o/a/a;)V

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/f;->g:Ljava/util/concurrent/Callable;

    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/e;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/e;-><init>(Lcom/google/android/apps/gmm/base/views/c/f;)V

    return-object v0

    .line 62
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/l;->aW:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/l;->hB:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/l;->hD:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    sget v0, Lcom/google/android/apps/gmm/l;->hE:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    sget v0, Lcom/google/android/apps/gmm/l;->hA:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 63
    :pswitch_5
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/v;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/directions/i/v;-><init>(Lcom/google/android/apps/gmm/o/a/c;)V

    goto :goto_1

    :pswitch_6
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/w;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/directions/i/w;-><init>(Lcom/google/android/apps/gmm/o/a/c;)V

    goto :goto_1

    :pswitch_7
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/x;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/directions/i/x;-><init>(Lcom/google/android/apps/gmm/o/a/c;)V

    goto :goto_1

    :pswitch_8
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/y;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/directions/i/y;-><init>(Lcom/google/android/apps/gmm/o/a/c;)V

    goto :goto_1

    :pswitch_9
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/z;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/directions/i/z;-><init>(Lcom/google/android/apps/gmm/o/a/c;)V

    goto :goto_1

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 63
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static a(Lcom/google/b/c/cx;Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/o/a/c;Landroid/content/res/Resources;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/cx",
            "<",
            "Lcom/google/android/apps/gmm/base/views/c/e;",
            ">;",
            "Lcom/google/maps/g/a/hm;",
            "Lcom/google/android/apps/gmm/o/a/c;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/ab;->a:[I

    invoke-virtual {p1}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 50
    :goto_0
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-static {v0, p2, p3}, Lcom/google/android/apps/gmm/directions/i/u;->a(Lcom/google/android/apps/gmm/o/a/a;Lcom/google/android/apps/gmm/o/a/c;Landroid/content/res/Resources;)Lcom/google/android/apps/gmm/base/views/c/e;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 56
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->e:Lcom/google/android/apps/gmm/o/a/a;

    invoke-static {v0, p2, p3}, Lcom/google/android/apps/gmm/directions/i/u;->a(Lcom/google/android/apps/gmm/o/a/a;Lcom/google/android/apps/gmm/o/a/c;Landroid/content/res/Resources;)Lcom/google/android/apps/gmm/base/views/c/e;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 57
    return-void

    .line 43
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->a:Lcom/google/android/apps/gmm/o/a/a;

    invoke-static {v0, p2, p3}, Lcom/google/android/apps/gmm/directions/i/u;->a(Lcom/google/android/apps/gmm/o/a/a;Lcom/google/android/apps/gmm/o/a/c;Landroid/content/res/Resources;)Lcom/google/android/apps/gmm/base/views/c/e;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_0

    .line 46
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    invoke-static {v0, p2, p3}, Lcom/google/android/apps/gmm/directions/i/u;->a(Lcom/google/android/apps/gmm/o/a/a;Lcom/google/android/apps/gmm/o/a/c;Landroid/content/res/Resources;)Lcom/google/android/apps/gmm/base/views/c/e;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_0

    .line 49
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->c:Lcom/google/android/apps/gmm/o/a/a;

    invoke-static {v0, p2, p3}, Lcom/google/android/apps/gmm/directions/i/u;->a(Lcom/google/android/apps/gmm/o/a/a;Lcom/google/android/apps/gmm/o/a/c;Landroid/content/res/Resources;)Lcom/google/android/apps/gmm/base/views/c/e;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_0

    .line 41
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
