.class public Lcom/google/android/apps/gmm/place/reservation/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/reservation/b/a;


# instance fields
.field a:Ljava/lang/Runnable;

.field private final b:Lcom/google/android/apps/gmm/base/activities/c;

.field private final c:Lcom/google/maps/g/q;

.field private final d:Lcom/google/android/apps/gmm/place/reservation/aj;

.field private final e:I

.field private final f:I

.field private g:Ljava/lang/CharSequence;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/reservation/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 45
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->q()Lcom/google/maps/g/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->c:Lcom/google/maps/g/q;

    .line 46
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/aj;

    .line 47
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v2, p2, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-virtual {v2}, Lcom/google/r/b/a/ads;->h()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p2, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    :cond_0
    iget-object v2, p2, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/place/reservation/aj;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->d:Lcom/google/android/apps/gmm/place/reservation/aj;

    .line 49
    const/16 v0, 0x82

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->e:I

    .line 50
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->f:I

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->c:Lcom/google/maps/g/q;

    iget-object v1, v0, Lcom/google/maps/g/q;->f:Lcom/google/maps/g/ky;

    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/maps/g/ky;->d()Lcom/google/maps/g/ky;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, v1, Lcom/google/maps/g/ky;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->lv:I

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/reservation/a;->c:Lcom/google/maps/g/q;

    invoke-virtual {v4}, Lcom/google/maps/g/q;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->g:Ljava/lang/CharSequence;

    .line 52
    :goto_2
    return-void

    .line 51
    :cond_1
    iget-object v0, v0, Lcom/google/maps/g/q;->f:Lcom/google/maps/g/ky;

    move-object v1, v0

    goto :goto_0

    :cond_2
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object v2, v1, Lcom/google/maps/g/ky;->b:Ljava/lang/Object;

    :cond_3
    move-object v0, v2

    goto :goto_1

    :cond_4
    const-string v1, "$w"

    iget v2, p0, Lcom/google/android/apps/gmm/place/reservation/a;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "$h"

    iget v2, p0, Lcom/google/android/apps/gmm/place/reservation/a;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/place/reservation/b;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/place/reservation/b;-><init>(Lcom/google/android/apps/gmm/place/reservation/a;)V

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/k;Z)V

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/place/reservation/a;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->lv:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v2, "%s"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    new-instance v3, Landroid/text/style/ImageSpan;

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/reservation/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v3, v4, p1}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    const/16 v4, 0x12

    invoke-virtual {v1, v3, v0, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/a;->c:Lcom/google/maps/g/q;

    invoke-virtual {v3}, Lcom/google/maps/g/q;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/a;->g:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/place/reservation/b/f;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->d:Lcom/google/android/apps/gmm/place/reservation/aj;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->g:Ljava/lang/CharSequence;

    return-object v0
.end method
