.class public Lcom/google/android/apps/gmm/replay/d;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field


# instance fields
.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/replay/b",
            "<*>;>;"
        }
    .end annotation
.end field

.field final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/android/apps/gmm/replay/b",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;

    const-class v1, Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;

    const-class v2, Lcom/google/android/apps/gmm/shared/net/CannedResponseEvent;

    const-class v3, Lcom/google/android/apps/gmm/map/location/rawlocationevents/ExpectedLocationEvent;

    const-class v4, Lcom/google/android/apps/gmm/navigation/logging/PerceivedLocationEvent;

    const-class v5, Lcom/google/android/apps/gmm/navigation/navui/PretendToBePluggedInEventForTesting;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-class v8, Lcom/google/android/apps/gmm/util/replay/SetStateEvent;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-class v8, Lcom/google/android/apps/gmm/replay/UseCannedStpResponseEvent;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, Lcom/google/b/c/dn;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/replay/d;->c:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/replay/d;->a:Ljava/util/Map;

    .line 58
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/replay/d;->b:Ljava/util/Map;

    .line 59
    sget-object v0, Lcom/google/android/apps/gmm/replay/d;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 60
    iget-object v1, p0, Lcom/google/android/apps/gmm/replay/d;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "EventProfiles"

    const-string v3, "Class %s already added."

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v0, v4, v9

    invoke-static {v1, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/replay/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/replay/b;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/gmm/replay/d;->a:Ljava/util/Map;

    iget-object v4, v3, Lcom/google/android/apps/gmm/replay/b;->a:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v4, "EventProfiles"

    const-string v5, "Duplicate definition for %s in %s and %s"

    const/4 v1, 0x3

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v1, v3, Lcom/google/android/apps/gmm/replay/b;->a:Ljava/lang/String;

    aput-object v1, v6, v9

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v10

    const/4 v7, 0x2

    iget-object v1, p0, Lcom/google/android/apps/gmm/replay/d;->a:Ljava/util/Map;

    iget-object v8, v3, Lcom/google/android/apps/gmm/replay/b;->a:Ljava/lang/String;

    invoke-interface {v1, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/replay/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/replay/b;->b:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v7

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/replay/d;->a:Ljava/util/Map;

    iget-object v4, v3, Lcom/google/android/apps/gmm/replay/b;->a:Ljava/lang/String;

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/gmm/replay/d;->b:Ljava/util/Map;

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 62
    :cond_2
    return-void
.end method

.method public static a()Lcom/google/android/apps/gmm/replay/d;
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/replay/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/replay/d;-><init>()V

    return-object v0
.end method
