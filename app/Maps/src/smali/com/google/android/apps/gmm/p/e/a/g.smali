.class public Lcom/google/android/apps/gmm/p/e/a/g;
.super Lcom/google/android/apps/gmm/map/b/a/af;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/internal/c/az;

.field public final b:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final c:Lcom/google/android/apps/gmm/map/b/a/y;

.field final d:Lcom/google/android/apps/gmm/map/b/a/ae;

.field public final e:F

.field final f:Z

.field final g:Z

.field public final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/p/e/a/e;",
            ">;"
        }
    .end annotation
.end field

.field public i:[Z

.field public j:Ljava/lang/Object;

.field k:Lcom/google/android/apps/gmm/map/r/a/ae;

.field public l:Z

.field public m:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/az;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;ZZI)V
    .locals 3

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/b/a/af;-><init>()V

    .line 98
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/ae;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/a/g;->k:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 108
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    .line 109
    iput-object p2, p0, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 110
    iput-object p3, p0, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 111
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/p/e/a/g;->f:Z

    .line 112
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/p/e/a/g;->g:Z

    .line 113
    const/4 v0, 0x0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/a/g;->i:[Z

    .line 116
    invoke-static {p2, p3}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/google/android/apps/gmm/map/b/a/ae;->b(I)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/a/g;->d:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 119
    iget v0, p3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v0, v1

    iget v1, p3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v2, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/z;->a(II)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/a/g;->h:Ljava/util/ArrayList;

    .line 121
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/p/e/a/g;)Z
    .locals 9

    .prologue
    const/16 v8, 0x50

    const/16 v7, 0x10

    const/4 v6, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 318
    iget v0, p0, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    iget v3, p1, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v3, 0x43340000    # 180.0f

    cmpl-float v3, v0, v3

    if-lez v3, :cond_0

    const/high16 v3, 0x43b40000    # 360.0f

    sub-float v0, v3, v0

    :cond_0
    const/high16 v3, 0x43070000    # 135.0f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    .line 358
    :cond_1
    :goto_0
    return v2

    .line 323
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->e:I

    .line 324
    iget-object v0, p1, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->e:I

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    and-int/2addr v0, v6

    if-eqz v0, :cond_9

    move v0, v1

    .line 326
    :goto_1
    iget-object v3, p1, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    and-int/2addr v3, v6

    if-eqz v3, :cond_a

    move v3, v1

    .line 329
    :goto_2
    const/16 v6, 0x80

    if-lt v4, v6, :cond_3

    if-gt v5, v8, :cond_3

    if-nez v0, :cond_3

    if-eqz v3, :cond_1

    .line 334
    :cond_3
    const/16 v6, 0x80

    if-lt v5, v6, :cond_4

    if-gt v4, v8, :cond_4

    if-nez v0, :cond_4

    if-eqz v3, :cond_1

    .line 341
    :cond_4
    if-eqz v0, :cond_5

    if-nez v3, :cond_5

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/p/e/a/g;->g:Z

    if-nez v4, :cond_5

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/p/e/a/g;->f:Z

    if-eqz v4, :cond_1

    .line 344
    :cond_5
    if-eqz v3, :cond_6

    if-nez v0, :cond_6

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/p/e/a/g;->g:Z

    if-nez v0, :cond_6

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/p/e/a/g;->f:Z

    if-eqz v0, :cond_1

    .line 349
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    and-int/2addr v0, v7

    if-eqz v0, :cond_b

    move v0, v1

    .line 350
    :goto_3
    iget-object v3, p1, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    and-int/2addr v3, v7

    if-eqz v3, :cond_c

    move v3, v1

    .line 351
    :goto_4
    if-eqz v0, :cond_7

    if-nez v3, :cond_7

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/p/e/a/g;->g:Z

    if-nez v4, :cond_7

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/p/e/a/g;->f:Z

    if-eqz v4, :cond_1

    .line 354
    :cond_7
    if-eqz v3, :cond_8

    if-nez v0, :cond_8

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/p/e/a/g;->g:Z

    if-nez v0, :cond_8

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/p/e/a/g;->f:Z

    if-eqz v0, :cond_1

    :cond_8
    move v2, v1

    .line 358
    goto :goto_0

    :cond_9
    move v0, v2

    .line 325
    goto :goto_1

    :cond_a
    move v3, v2

    .line 326
    goto :goto_2

    :cond_b
    move v0, v2

    .line 349
    goto :goto_3

    :cond_c
    move v3, v2

    .line 350
    goto :goto_4
.end method


# virtual methods
.method public final O_()D
    .locals 4

    .prologue
    .line 173
    const-wide v0, 0x400ccccccccccccdL    # 3.6

    .line 174
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/4 v3, 0x4

    and-int/2addr v2, v3

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_1

    .line 175
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/az;->e:I

    const/16 v3, 0x40

    if-lt v2, v3, :cond_0

    .line 176
    const-wide v0, 0x402599999999999aL    # 10.8

    .line 178
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/az;->e:I

    const/16 v3, 0x80

    if-lt v2, v3, :cond_1

    .line 179
    const-wide v2, 0x401ccccccccccccdL    # 7.2

    add-double/2addr v0, v2

    .line 182
    :cond_1
    return-wide v0

    .line 174
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 1

    .prologue
    .line 375
    if-nez p1, :cond_0

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 379
    :goto_0
    return-object v0

    .line 378
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_0

    .line 381
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 1

    .prologue
    .line 386
    const/4 v0, 0x0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 370
    const/4 v0, 0x2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 363
    const-string v0, "[(%s, %s), (%s, %s)]"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 364
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 363
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
