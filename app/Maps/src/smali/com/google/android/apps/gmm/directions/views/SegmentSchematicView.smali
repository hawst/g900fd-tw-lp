.class public Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;
.super Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;
.source "PG"


# instance fields
.field public f:I

.field private g:Lcom/google/android/apps/gmm/directions/views/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 73
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->f:I

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->g:Lcom/google/android/apps/gmm/directions/views/a;

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->g:Lcom/google/android/apps/gmm/directions/views/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/directions/views/a;->c(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;)F

    move-result v4

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->getHeight()I

    move-result v0

    int-to-float v3, v0

    iget v5, p0, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->f:I

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->a(Landroid/graphics/Canvas;FFFI)V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->f:I

    invoke-virtual {p0, p1, v2, v0, v1}, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->a(Landroid/graphics/Canvas;FFI)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->g:Lcom/google/android/apps/gmm/directions/views/a;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->g:Lcom/google/android/apps/gmm/directions/views/a;

    sub-int v1, p5, p3

    int-to-float v1, v1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/gmm/directions/views/a;->a(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;F)V

    .line 68
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->onLayout(ZIIII)V

    .line 69
    return-void
.end method

.method public final setDottedLineController(ILcom/google/android/apps/gmm/directions/views/a;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->g:Lcom/google/android/apps/gmm/directions/views/a;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->g:Lcom/google/android/apps/gmm/directions/views/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/directions/views/a;->b(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;)V

    .line 50
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->g:Lcom/google/android/apps/gmm/directions/views/a;

    .line 51
    if-eqz p2, :cond_1

    .line 54
    invoke-virtual {p2, p1, p0}, Lcom/google/android/apps/gmm/directions/views/a;->a(ILcom/google/android/apps/gmm/directions/views/BaseSchematicView;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2, p0, v0}, Lcom/google/android/apps/gmm/directions/views/a;->a(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;F)V

    .line 57
    :cond_1
    return-void
.end method
