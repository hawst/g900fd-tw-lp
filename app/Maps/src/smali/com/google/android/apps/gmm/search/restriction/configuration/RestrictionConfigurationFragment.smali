.class public Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"


# instance fields
.field c:Lcom/google/android/apps/gmm/search/restriction/configuration/f;

.field d:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    .line 43
    return-void
.end method

.method public static a(Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;
    .locals 3

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;-><init>()V

    .line 57
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 58
    const-string v2, "ENABLED_RESTRICTIONS_KEY"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 59
    const-string v2, "MODEL_KEY"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 61
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;->setArguments(Landroid/os/Bundle;)V

    .line 63
    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 68
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 73
    const-string v0, "MODEL_KEY"

    .line 74
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/alh;

    .line 75
    const-string v3, "ENABLED_RESTRICTIONS_KEY"

    .line 76
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/qm;

    .line 79
    invoke-static {}, Lcom/google/r/b/a/alh;->newBuilder()Lcom/google/r/b/a/alm;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/alm;->a(Lcom/google/r/b/a/alh;)Lcom/google/r/b/a/alm;

    move-result-object v4

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v2

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 81
    invoke-static {}, Lcom/google/maps/g/gn;->g()Lcom/google/maps/g/gn;

    move-result-object v1

    .line 80
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_1
    check-cast v0, Lcom/google/maps/g/gn;

    .line 79
    invoke-virtual {v4, v0}, Lcom/google/r/b/a/alm;->a(Lcom/google/maps/g/gn;)Lcom/google/r/b/a/alm;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lcom/google/r/b/a/alm;->g()Lcom/google/n/t;

    move-result-object v4

    check-cast v4, Lcom/google/r/b/a/alh;

    .line 84
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/configuration/f;

    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_2

    move-object v1, v2

    .line 86
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    if-nez v5, :cond_3

    :goto_3
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v2

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/search/restriction/configuration/f;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;->c:Lcom/google/android/apps/gmm/search/restriction/configuration/f;

    .line 90
    return-void

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    .line 85
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto :goto_2

    .line 86
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    goto :goto_3
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 95
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    .line 100
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 101
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/search/restriction/configuration/b;

    .line 102
    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;->d:Landroid/view/View;

    .line 109
    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/configuration/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/search/restriction/configuration/c;-><init>(Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;)V

    .line 116
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;->c:Lcom/google/android/apps/gmm/search/restriction/configuration/f;

    iput-object v0, v1, Lcom/google/android/apps/gmm/search/restriction/configuration/f;->b:Ljava/lang/Runnable;

    iget-object v1, v1, Lcom/google/android/apps/gmm/search/restriction/configuration/f;->a:Lcom/google/android/apps/gmm/search/restriction/c/k;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/search/restriction/c/k;->a(Ljava/lang/Runnable;)V

    .line 117
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;->d:Landroid/view/View;

    return-object v0

    .line 104
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/search/restriction/configuration/a;

    .line 105
    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;->d:Landroid/view/View;

    goto :goto_0
.end method
