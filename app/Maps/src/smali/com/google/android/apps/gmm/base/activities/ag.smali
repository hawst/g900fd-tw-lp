.class public Lcom/google/android/apps/gmm/base/activities/ag;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Z

.field b:I

.field c:Lcom/google/android/apps/gmm/map/o/ar;

.field public d:Z

.field public e:Z

.field f:Lcom/google/android/apps/gmm/mylocation/b/h;

.field g:Lcom/google/android/apps/gmm/mylocation/b/g;

.field h:Lcom/google/android/apps/gmm/map/b/a/ai;

.field i:Z

.field j:Z

.field k:[Lcom/google/android/apps/gmm/o/a/b;

.field public l:Z

.field m:Z

.field n:Z

.field public o:Lcom/google/b/f/t;

.field p:Z

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field public r:Z

.field private s:Z

.field private t:Z


# direct methods
.method private constructor <init>(ZILcom/google/android/apps/gmm/map/o/ar;ZZZLcom/google/android/apps/gmm/mylocation/b/h;Lcom/google/android/apps/gmm/mylocation/b/g;Lcom/google/android/apps/gmm/map/b/a/ai;ZZ[Lcom/google/android/apps/gmm/o/a/b;ZZZZZLcom/google/b/f/t;Z)V
    .locals 2

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    sget-object v1, Lcom/google/b/f/t;->fX:Lcom/google/b/f/t;

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/activities/ag;->o:Lcom/google/b/f/t;

    .line 75
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/activities/ag;->q:Ljava/util/List;

    .line 98
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/activities/ag;->a:Z

    .line 99
    iput p2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->b:I

    .line 100
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/activities/ag;->c:Lcom/google/android/apps/gmm/map/o/ar;

    .line 101
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/base/activities/ag;->d:Z

    .line 102
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/base/activities/ag;->e:Z

    .line 103
    iput-boolean p6, p0, Lcom/google/android/apps/gmm/base/activities/ag;->s:Z

    .line 104
    iput-object p7, p0, Lcom/google/android/apps/gmm/base/activities/ag;->f:Lcom/google/android/apps/gmm/mylocation/b/h;

    .line 105
    iput-object p8, p0, Lcom/google/android/apps/gmm/base/activities/ag;->g:Lcom/google/android/apps/gmm/mylocation/b/g;

    .line 106
    iput-object p9, p0, Lcom/google/android/apps/gmm/base/activities/ag;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 107
    iput-boolean p10, p0, Lcom/google/android/apps/gmm/base/activities/ag;->i:Z

    .line 108
    iput-boolean p11, p0, Lcom/google/android/apps/gmm/base/activities/ag;->j:Z

    .line 109
    iput-object p12, p0, Lcom/google/android/apps/gmm/base/activities/ag;->k:[Lcom/google/android/apps/gmm/o/a/b;

    .line 110
    iput-boolean p13, p0, Lcom/google/android/apps/gmm/base/activities/ag;->l:Z

    .line 111
    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/ag;->t:Z

    .line 112
    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/ag;->n:Z

    .line 113
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/ag;->m:Z

    .line 114
    move/from16 v0, p17

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/ag;->r:Z

    .line 115
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/ag;->o:Lcom/google/b/f/t;

    .line 116
    move/from16 v0, p19

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/ag;->p:Z

    .line 117
    return-void
.end method

.method public static a()Lcom/google/android/apps/gmm/base/activities/ag;
    .locals 20

    .prologue
    .line 348
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/ag;

    const/4 v1, 0x0

    const/high16 v2, -0x80000000

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/google/android/apps/gmm/mylocation/b/h;->a:Lcom/google/android/apps/gmm/mylocation/b/h;

    sget-object v8, Lcom/google/android/apps/gmm/mylocation/b/g;->b:Lcom/google/android/apps/gmm/mylocation/b/g;

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x1

    const/4 v12, 0x0

    new-array v12, v12, [Lcom/google/android/apps/gmm/o/a/b;

    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x0

    const/16 v17, 0x1

    sget-object v18, Lcom/google/b/f/t;->fX:Lcom/google/b/f/t;

    const/16 v19, 0x0

    invoke-direct/range {v0 .. v19}, Lcom/google/android/apps/gmm/base/activities/ag;-><init>(ZILcom/google/android/apps/gmm/map/o/ar;ZZZLcom/google/android/apps/gmm/mylocation/b/h;Lcom/google/android/apps/gmm/mylocation/b/g;Lcom/google/android/apps/gmm/map/b/a/ai;ZZ[Lcom/google/android/apps/gmm/o/a/b;ZZZZZLcom/google/b/f/t;Z)V

    return-object v0
.end method

.method public static a(Lcom/google/maps/g/a/hm;)Lcom/google/android/apps/gmm/base/activities/ag;
    .locals 20

    .prologue
    .line 394
    const-string v0, "travelMode"

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 396
    :cond_0
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 397
    const/16 v19, 0x0

    .line 398
    sget-object v0, Lcom/google/android/apps/gmm/base/activities/ah;->a:[I

    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 419
    const/16 v19, 0x1

    .line 420
    new-instance v0, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 421
    new-instance v0, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->c:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 427
    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->e:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 430
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/ag;

    const/4 v1, 0x1

    const/4 v2, 0x3

    sget-object v3, Lcom/google/android/apps/gmm/map/o/ar;->a:Lcom/google/android/apps/gmm/map/o/ar;

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/apps/gmm/mylocation/b/h;->b:Lcom/google/android/apps/gmm/mylocation/b/h;

    sget-object v8, Lcom/google/android/apps/gmm/mylocation/b/g;->a:Lcom/google/android/apps/gmm/mylocation/b/g;

    sget-object v9, Lcom/google/android/apps/gmm/map/b/a/ai;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    new-array v13, v13, [Lcom/google/android/apps/gmm/o/a/b;

    .line 442
    invoke-interface {v12, v13}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [Lcom/google/android/apps/gmm/o/a/b;

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x0

    sget-object v18, Lcom/google/b/f/t;->fX:Lcom/google/b/f/t;

    invoke-direct/range {v0 .. v19}, Lcom/google/android/apps/gmm/base/activities/ag;-><init>(ZILcom/google/android/apps/gmm/map/o/ar;ZZZLcom/google/android/apps/gmm/mylocation/b/h;Lcom/google/android/apps/gmm/mylocation/b/g;Lcom/google/android/apps/gmm/map/b/a/ai;ZZ[Lcom/google/android/apps/gmm/o/a/b;ZZZZZLcom/google/b/f/t;Z)V

    return-object v0

    .line 400
    :pswitch_0
    new-instance v0, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 401
    new-instance v0, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->c:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 402
    new-instance v0, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->a:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 407
    :pswitch_1
    new-instance v0, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 408
    new-instance v0, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->c:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409
    new-instance v0, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->a:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 413
    :pswitch_2
    new-instance v0, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 414
    new-instance v0, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->c:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 415
    new-instance v0, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->a:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 398
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b()Lcom/google/android/apps/gmm/base/activities/ag;
    .locals 20

    .prologue
    .line 371
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/ag;

    const/4 v1, 0x0

    const/high16 v2, -0x80000000

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    sget-object v7, Lcom/google/android/apps/gmm/mylocation/b/h;->a:Lcom/google/android/apps/gmm/mylocation/b/h;

    sget-object v8, Lcom/google/android/apps/gmm/mylocation/b/g;->b:Lcom/google/android/apps/gmm/mylocation/b/g;

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x1

    const/4 v12, 0x0

    new-array v12, v12, [Lcom/google/android/apps/gmm/o/a/b;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1

    sget-object v18, Lcom/google/b/f/t;->fX:Lcom/google/b/f/t;

    const/16 v19, 0x0

    invoke-direct/range {v0 .. v19}, Lcom/google/android/apps/gmm/base/activities/ag;-><init>(ZILcom/google/android/apps/gmm/map/o/ar;ZZZLcom/google/android/apps/gmm/mylocation/b/h;Lcom/google/android/apps/gmm/mylocation/b/g;Lcom/google/android/apps/gmm/map/b/a/ai;ZZ[Lcom/google/android/apps/gmm/o/a/b;ZZZZZLcom/google/b/f/t;Z)V

    return-object v0
.end method

.method public static c()Lcom/google/android/apps/gmm/base/activities/ag;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 460
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->a()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v2

    .line 461
    invoke-static {}, Lcom/google/android/apps/gmm/o/a/a;->values()[Lcom/google/android/apps/gmm/o/a/a;

    move-result-object v3

    array-length v0, v3

    new-array v4, v0, [Lcom/google/android/apps/gmm/o/a/b;

    move v0, v1

    :goto_0
    array-length v5, v3

    if-ge v0, v5, :cond_0

    new-instance v5, Lcom/google/android/apps/gmm/o/a/b;

    aget-object v6, v3, v0

    invoke-direct {v5, v6, v1}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    aput-object v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object v4, v2, Lcom/google/android/apps/gmm/base/activities/ag;->k:[Lcom/google/android/apps/gmm/o/a/b;

    .line 462
    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/activities/ag;->d:Z

    .line 463
    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/activities/ag;->e:Z

    return-object v2
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 222
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v0

    .line 223
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    .line 224
    if-eqz v1, :cond_0

    .line 232
    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/o/a/c;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 234
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->r()V

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    .line 237
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget-boolean v0, v0, Lcom/google/r/b/a/ou;->O:Z

    if-eqz v0, :cond_2

    .line 239
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->r()V

    goto :goto_0

    .line 242
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/ag;->s:Z

    if-eqz v0, :cond_3

    .line 243
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, v3, v3}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;Landroid/view/View;)V

    goto :goto_0

    .line 247
    :cond_3
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->r()V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 468
    if-ne p0, p1, :cond_1

    .line 494
    :cond_0
    :goto_0
    return v0

    .line 470
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 471
    goto :goto_0

    .line 472
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 473
    goto :goto_0

    .line 475
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/ag;

    .line 476
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->a:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->a:Z

    if-ne v2, v3, :cond_c

    iget v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->b:I

    if-ne v2, v3, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->c:Lcom/google/android/apps/gmm/map/o/ar;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->c:Lcom/google/android/apps/gmm/map/o/ar;

    .line 478
    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_d

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_4
    move v2, v0

    :goto_1
    if-eqz v2, :cond_c

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->d:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->d:Z

    if-ne v2, v3, :cond_c

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->e:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->e:Z

    if-ne v2, v3, :cond_c

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->s:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->s:Z

    if-ne v2, v3, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->f:Lcom/google/android/apps/gmm/mylocation/b/h;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->f:Lcom/google/android/apps/gmm/mylocation/b/h;

    .line 482
    if-eq v2, v3, :cond_5

    if-eqz v2, :cond_e

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_5
    move v2, v0

    :goto_2
    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->g:Lcom/google/android/apps/gmm/mylocation/b/g;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->g:Lcom/google/android/apps/gmm/mylocation/b/g;

    .line 483
    if-eq v2, v3, :cond_6

    if-eqz v2, :cond_f

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_6
    move v2, v0

    :goto_3
    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 484
    if-eq v2, v3, :cond_7

    if-eqz v2, :cond_10

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    :cond_7
    move v2, v0

    :goto_4
    if-eqz v2, :cond_c

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->i:Z

    .line 485
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->i:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    if-eq v2, v3, :cond_8

    if-eqz v2, :cond_11

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    :cond_8
    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->j:Z

    .line 486
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->j:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    if-eq v2, v3, :cond_9

    if-eqz v2, :cond_12

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    :cond_9
    move v2, v0

    :goto_6
    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->k:[Lcom/google/android/apps/gmm/o/a/b;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->k:[Lcom/google/android/apps/gmm/o/a/b;

    .line 487
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->l:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->l:Z

    if-ne v2, v3, :cond_c

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->t:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->t:Z

    if-ne v2, v3, :cond_c

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->m:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->m:Z

    if-ne v2, v3, :cond_c

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->n:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->n:Z

    if-ne v2, v3, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->q:Ljava/util/List;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->q:Ljava/util/List;

    .line 492
    if-eq v2, v3, :cond_a

    if-eqz v2, :cond_13

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    :cond_a
    move v2, v0

    :goto_7
    if-eqz v2, :cond_c

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->r:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->r:Z

    if-ne v2, v3, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->o:Lcom/google/b/f/t;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->o:Lcom/google/b/f/t;

    .line 494
    if-eq v2, v3, :cond_b

    if-eqz v2, :cond_14

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    :cond_b
    move v2, v0

    :goto_8
    if-eqz v2, :cond_c

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->p:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/activities/ag;->p:Z

    if-eq v2, v3, :cond_0

    :cond_c
    move v0, v1

    goto/16 :goto_0

    :cond_d
    move v2, v1

    .line 478
    goto/16 :goto_1

    :cond_e
    move v2, v1

    .line 482
    goto/16 :goto_2

    :cond_f
    move v2, v1

    .line 483
    goto/16 :goto_3

    :cond_10
    move v2, v1

    .line 484
    goto/16 :goto_4

    :cond_11
    move v2, v1

    .line 485
    goto :goto_5

    :cond_12
    move v2, v1

    .line 486
    goto :goto_6

    :cond_13
    move v2, v1

    .line 492
    goto :goto_7

    :cond_14
    move v2, v1

    .line 494
    goto :goto_8
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 501
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->b:I

    .line 502
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->c:Lcom/google/android/apps/gmm/map/o/ar;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->d:Z

    .line 504
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->e:Z

    .line 505
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->s:Z

    .line 506
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->f:Lcom/google/android/apps/gmm/mylocation/b/h;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->g:Lcom/google/android/apps/gmm/mylocation/b/g;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->i:Z

    .line 510
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->j:Z

    .line 511
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->k:[Lcom/google/android/apps/gmm/o/a/b;

    .line 512
    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->l:Z

    .line 513
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->t:Z

    .line 514
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->m:Z

    .line 515
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->n:Z

    .line 516
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->q:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->r:Z

    .line 518
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->o:Lcom/google/b/f/t;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/ag;->p:Z

    .line 520
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 501
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
