.class public Lcom/google/android/apps/gmm/map/n/aj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/ak;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/f/o;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/n/aj;->a:Lcom/google/android/apps/gmm/map/f/o;

    .line 29
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/b/a/ba;
    .locals 7

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/aj;->a:Lcom/google/android/apps/gmm/map/f/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/f/o;->b()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/aj;->a:Lcom/google/android/apps/gmm/map/f/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/f/o;->b()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ba;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    iget-object v2, v5, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    iget-object v3, v5, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v3

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v6, 0x2

    aget-object v4, v4, v6

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v4

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/b/a/bb;->d:Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/bc;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/b/a/ba;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/r;)V

    .line 53
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 58
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "camera"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/n/aj;->a:Lcom/google/android/apps/gmm/map/f/o;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
