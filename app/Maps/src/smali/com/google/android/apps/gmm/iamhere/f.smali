.class Lcom/google/android/apps/gmm/iamhere/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/iamhere/e;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/iamhere/e;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 113
    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/iamhere/e;->g:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lcom/google/android/apps/gmm/iamhere/e;->g:Z

    .line 114
    sget-object v3, Lcom/google/android/apps/gmm/iamhere/g;->a:[I

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/iamhere/e;->g:Z

    if-eqz v4, :cond_6

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/iamhere/e;->h:Z

    if-eqz v4, :cond_3

    :cond_0
    :goto_1
    if-eqz v2, :cond_5

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/h;->b:Lcom/google/android/apps/gmm/iamhere/h;

    :goto_2
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/h;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    .line 138
    :cond_1
    :goto_3
    return-void

    :cond_2
    move v0, v2

    .line 113
    goto :goto_0

    .line 114
    :cond_3
    iget-object v4, v0, Lcom/google/android/apps/gmm/iamhere/e;->d:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, v0, Lcom/google/android/apps/gmm/iamhere/e;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    move v2, v1

    goto :goto_1

    :cond_4
    iget-object v4, v0, Lcom/google/android/apps/gmm/iamhere/e;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v1, :cond_0

    iget v0, v0, Lcom/google/android/apps/gmm/iamhere/e;->b:I

    if-nez v0, :cond_0

    move v2, v1

    goto :goto_1

    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/h;->a:Lcom/google/android/apps/gmm/iamhere/h;

    goto :goto_2

    :cond_6
    iget v1, v0, Lcom/google/android/apps/gmm/iamhere/e;->b:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-lt v1, v0, :cond_7

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/h;->d:Lcom/google/android/apps/gmm/iamhere/h;

    goto :goto_2

    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/h;->c:Lcom/google/android/apps/gmm/iamhere/h;

    goto :goto_2

    .line 116
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    iget v1, v0, Lcom/google/android/apps/gmm/iamhere/e;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/iamhere/e;->b:I

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    iget-object v1, v0, Lcom/google/android/apps/gmm/iamhere/e;->f:Lcom/google/android/apps/gmm/mylocation/d/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/e;->c:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    iget v2, v2, Lcom/google/android/apps/gmm/iamhere/e;->b:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    iget-object v0, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/ci;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/mylocation/d/a;->a(Lcom/google/android/apps/gmm/v/ci;)V

    goto :goto_3

    .line 119
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    const/4 v1, -0x1

    iput v1, v0, Lcom/google/android/apps/gmm/iamhere/e;->b:I

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/e;->f:Lcom/google/android/apps/gmm/mylocation/d/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/mylocation/d/a;->j:Lcom/google/android/apps/gmm/v/ci;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/mylocation/d/a;->a(Lcom/google/android/apps/gmm/v/ci;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/iamhere/e;->h:Z

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/e;->N_()V

    goto :goto_3

    .line 135
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/animation/Animation;->cancel()V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/e;->a:Lcom/google/android/apps/gmm/iamhere/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/iamhere/e;->e:Lcom/google/android/apps/gmm/iamhere/c/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/iamhere/i;->a(Lcom/google/android/apps/gmm/iamhere/c/o;)V

    goto/16 :goto_3

    .line 114
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/iamhere/e;->g:Z

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/f;->a:Lcom/google/android/apps/gmm/iamhere/e;

    const/4 v1, -0x1

    iput v1, v0, Lcom/google/android/apps/gmm/iamhere/e;->b:I

    .line 106
    return-void
.end method
