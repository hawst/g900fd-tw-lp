.class public abstract Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field public d:Lcom/google/android/apps/gmm/base/g/c;

.field public e:Lcom/google/maps/g/hs;

.field public f:Lcom/google/android/apps/gmm/map/b/a;

.field public g:Lcom/google/android/apps/gmm/base/views/q;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 51
    new-instance v0, Lcom/google/android/apps/gmm/base/views/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    .line 52
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;)V
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->a_()V

    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a_()V
.end method

.method public abstract b()I
.end method

.method public abstract c()I
.end method

.method public abstract d()I
.end method

.method public abstract f()Ljava/lang/Object;
.end method

.method public i()V
    .locals 3

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    if-nez v0, :cond_0

    .line 208
    :goto_0
    return-void

    .line 206
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/d;->a:[I

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget v0, v0, Lcom/google/maps/g/oj;->b:I

    invoke-static {v0}, Lcom/google/maps/g/om;->a(I)Lcom/google/maps/g/om;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/om;->g:Lcom/google/maps/g/om;

    :cond_1
    invoke-virtual {v0}, Lcom/google/maps/g/om;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    .line 207
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/views/q;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 206
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget-object v0, v0, Lcom/google/maps/g/oj;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gg;->d()Lcom/google/maps/g/gg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gg;

    iget-object v0, v0, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    const/16 v1, 0xa

    new-instance v2, Lcom/google/b/a/ab;

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->ac()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->ad()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->ae()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->af()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 105
    if-eqz p1, :cond_0

    .line 106
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "placemark"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    .line 107
    const-string v0, "rdp_entry point_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/google/maps/g/hs;->a(I)Lcom/google/maps/g/hs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->e:Lcom/google/maps/g/hs;

    .line 108
    return-void

    .line 105
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 124
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->a()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->b()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->d()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/l;->bg:I

    .line 127
    invoke-virtual {p0, v5}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->c()I

    new-instance v6, Lcom/google/android/apps/gmm/base/fragments/b;

    invoke-direct {v6, p0}, Lcom/google/android/apps/gmm/base/fragments/b;-><init>(Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;)V

    new-instance v7, Lcom/google/android/apps/gmm/base/fragments/c;

    invoke-direct {v7, p0}, Lcom/google/android/apps/gmm/base/fragments/c;-><init>(Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;)V

    .line 122
    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/gmm/base/views/q;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->a:Landroid/view/View;

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/views/q;->m()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->b:Landroid/view/View;

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->a:Landroid/view/View;

    return-object v0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 192
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    .line 194
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->f()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->f:Lcom/google/android/apps/gmm/map/b/a;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->f:Lcom/google/android/apps/gmm/map/b/a;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/ah;->a(Lcom/google/android/apps/gmm/map/b/a;)V

    .line 200
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 148
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->f()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 152
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->a()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v0

    .line 153
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/base/activities/ag;->d:Z

    .line 154
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/base/activities/ag;->e:Z

    .line 155
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/activities/ag;->r:Z

    .line 157
    new-instance v3, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 158
    iget-object v4, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v1, v4, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 159
    iget-object v4, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v2, v4, Lcom/google/android/apps/gmm/base/activities/p;->D:Z

    .line 160
    iget-object v4, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v7, v4, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v4, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v4, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 161
    iget-object v4, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v2, v4, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 162
    iget-object v4, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v7, v4, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v4, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v4, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->a:Landroid/view/View;

    .line 163
    iget-object v5, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v4, v5, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iget-object v4, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v5, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v5, v4, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->b:Landroid/view/View;

    .line 164
    iget-object v5, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v4, v5, Lcom/google/android/apps/gmm/base/activities/p;->A:Landroid/view/View;

    .line 165
    iget-object v4, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    .line 166
    iget-object v0, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v6, v0, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    .line 167
    iget-object v0, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v6, v0, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    .line 168
    iget-object v0, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/views/q;->n()V

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 177
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    .line 179
    if-eqz v0, :cond_0

    .line 181
    sget-object v2, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    invoke-virtual {v3, v0, v2, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/ag;Z)Lcom/google/android/apps/gmm/map/b/b;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->f:Lcom/google/android/apps/gmm/map/b/a;

    .line 183
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    invoke-virtual {v3, v0, v7, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 186
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->i()V

    .line 188
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 177
    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 112
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "placemark"

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 116
    :cond_0
    const-string v0, "rdp_entry point_type"

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->e:Lcom/google/maps/g/hs;

    iget v1, v1, Lcom/google/maps/g/hs;->n:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117
    return-void
.end method
