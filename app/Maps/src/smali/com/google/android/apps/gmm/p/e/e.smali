.class Lcom/google/android/apps/gmm/p/e/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/p/e/e;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/google/android/apps/gmm/map/r/a/ae;

.field b:D

.field c:[D

.field d:D

.field e:D

.field f:D

.field g:Lcom/google/android/apps/gmm/p/e/a/g;

.field h:Lcom/google/android/apps/gmm/p/e/f;

.field i:[Lcom/google/android/apps/gmm/map/r/a/ad;

.field j:D

.field k:Lcom/google/android/apps/gmm/map/b/a/y;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/r/a/ae;Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/p/e/f;Lcom/google/android/apps/gmm/shared/net/a/l;)V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 564
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/e/e;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 565
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/e;->c:[D

    .line 566
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/r/a/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/e;->i:[Lcom/google/android/apps/gmm/map/r/a/ad;

    .line 567
    iput-object p2, p0, Lcom/google/android/apps/gmm/p/e/e;->g:Lcom/google/android/apps/gmm/p/e/a/g;

    .line 568
    iput-object p3, p0, Lcom/google/android/apps/gmm/p/e/e;->h:Lcom/google/android/apps/gmm/p/e/f;

    .line 571
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/e;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 572
    iget-object v0, p2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p2, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p3, Lcom/google/android/apps/gmm/p/e/f;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/gmm/p/e/e;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;ZLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 573
    iget-object v0, p2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p2, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/e;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/e;->j:D

    .line 576
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/e;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, p2, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    float-to-double v2, v1

    iget-object v1, p3, Lcom/google/android/apps/gmm/p/e/f;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    float-to-double v0, v0

    iget-wide v4, p3, Lcom/google/android/apps/gmm/p/e/f;->a:D

    div-double/2addr v0, v4

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/p/e/a/g;->O_()D

    move-result-wide v4

    div-double/2addr v4, v8

    const-wide/16 v6, 0x0

    sub-double/2addr v0, v4

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iget-wide v4, p3, Lcom/google/android/apps/gmm/p/e/f;->e:D

    div-double/2addr v0, v4

    neg-double v4, v0

    mul-double/2addr v0, v4

    div-double/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    iget-boolean v0, p3, Lcom/google/android/apps/gmm/p/e/f;->f:Z

    if-eqz v0, :cond_1

    double-to-float v0, v2

    iget-wide v2, p3, Lcom/google/android/apps/gmm/p/e/f;->g:D

    double-to-float v1, v2

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    const/high16 v1, 0x43b40000    # 360.0f

    sub-float v0, v1, v0

    :cond_0
    float-to-double v0, v0

    iget-wide v2, p3, Lcom/google/android/apps/gmm/p/e/f;->h:D

    div-double/2addr v0, v2

    neg-double v2, v0

    mul-double/2addr v0, v2

    div-double/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    :goto_0
    mul-double/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/e;->d:D

    .line 577
    iget-object v0, p4, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v0, v0, Lcom/google/r/b/a/ou;->c:I

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    neg-int v0, v0

    int-to-double v0, v0

    const-wide v4, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/e;->e:D

    .line 578
    return-void

    .line 576
    :cond_1
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 519
    check-cast p1, Lcom/google/android/apps/gmm/p/e/e;

    iget-wide v0, p0, Lcom/google/android/apps/gmm/p/e/e;->b:D

    iget-wide v2, p1, Lcom/google/android/apps/gmm/p/e/e;->b:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/gmm/p/e/e;->b:D

    iget-wide v2, p1, Lcom/google/android/apps/gmm/p/e/e;->b:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 687
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v0, "#.##"

    invoke-direct {v1, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 688
    const-class v0, Lcom/google/android/apps/gmm/p/e/e;

    new-instance v2, Lcom/google/b/a/ah;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v0, "id"

    .line 689
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v2

    const-string v3, "likelihood"

    iget-wide v4, p0, Lcom/google/android/apps/gmm/p/e/e;->b:D

    .line 690
    cmpl-double v0, v4, v6

    if-nez v0, :cond_0

    const-string v0, "-inf"

    :goto_0
    invoke-virtual {v2, v3, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v2

    const-string v3, "emissionLikelihood"

    iget-wide v4, p0, Lcom/google/android/apps/gmm/p/e/e;->d:D

    .line 691
    cmpl-double v0, v4, v6

    if-nez v0, :cond_1

    const-string v0, "-inf"

    :goto_1
    invoke-virtual {v2, v3, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v2

    const-string v3, "transitionLikelihood"

    iget-wide v4, p0, Lcom/google/android/apps/gmm/p/e/e;->e:D

    .line 692
    cmpl-double v0, v4, v6

    if-nez v0, :cond_2

    const-string v0, "-inf"

    :goto_2
    invoke-virtual {v2, v3, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "position"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/e/e;->j:D

    .line 693
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "segment"

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/e;->g:Lcom/google/android/apps/gmm/p/e/a/g;

    .line 694
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "point"

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/e;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 695
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 696
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 690
    :cond_0
    invoke-static {v4, v5}, Ljava/lang/Math;->log10(D)D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 691
    :cond_1
    invoke-static {v4, v5}, Ljava/lang/Math;->log10(D)D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 692
    :cond_2
    invoke-static {v4, v5}, Ljava/lang/Math;->log10(D)D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
