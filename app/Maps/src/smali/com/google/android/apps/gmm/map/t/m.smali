.class public final enum Lcom/google/android/apps/gmm/map/t/m;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/t/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/t/m;",
        ">;",
        "Lcom/google/android/apps/gmm/map/t/k;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/t/m;

.field private static final synthetic b:[Lcom/google/android/apps/gmm/map/t/m;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/map/t/m;

    const-string v1, "IMAGERY"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/m;->a:Lcom/google/android/apps/gmm/map/t/m;

    .line 47
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/t/m;

    sget-object v1, Lcom/google/android/apps/gmm/map/t/m;->a:Lcom/google/android/apps/gmm/map/t/m;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/gmm/map/t/m;->b:[Lcom/google/android/apps/gmm/map/t/m;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/t/m;
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/google/android/apps/gmm/map/t/m;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/m;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/t/m;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/apps/gmm/map/t/m;->b:[Lcom/google/android/apps/gmm/map/t/m;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/t/m;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/t/m;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x2

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/t/m;->ordinal()I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/t/m;->ordinal()I

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method
