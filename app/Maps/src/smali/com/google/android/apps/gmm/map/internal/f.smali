.class Lcom/google/android/apps/gmm/map/internal/f;
.super Lcom/google/android/apps/gmm/v/e;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/bf;


# instance fields
.field a:Lcom/google/android/apps/gmm/map/internal/e;

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/android/apps/gmm/v/g;

.field private d:Lcom/google/android/apps/gmm/v/x;

.field private e:Lcom/google/android/apps/gmm/v/n;

.field private f:Lcom/google/android/apps/gmm/v/aa;

.field private g:F

.field private h:F


# direct methods
.method private declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/e;)V
    .locals 2

    .prologue
    .line 419
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->a:Lcom/google/android/apps/gmm/map/internal/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/e;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 427
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 422
    :cond_1
    :try_start_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/f;->a:Lcom/google/android/apps/gmm/map/internal/e;

    .line 424
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->c:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->c:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 419
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method a(J)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 445
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    sub-long v2, p1, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 446
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 452
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/e;->a:Lcom/google/android/apps/gmm/map/internal/e;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/e;->e:I

    if-ge v0, v1, :cond_2

    .line 453
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/e;->a:Lcom/google/android/apps/gmm/map/internal/e;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/f;->a(Lcom/google/android/apps/gmm/map/internal/e;)V

    .line 460
    :cond_1
    :goto_1
    return-void

    .line 454
    :cond_2
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/e;->a:Lcom/google/android/apps/gmm/map/internal/e;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/e;->e:I

    add-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_3

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/e;->b:Lcom/google/android/apps/gmm/map/internal/e;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/e;->e:I

    if-ge v0, v1, :cond_3

    .line 456
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/e;->b:Lcom/google/android/apps/gmm/map/internal/e;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/f;->a(Lcom/google/android/apps/gmm/map/internal/e;)V

    goto :goto_1

    .line 457
    :cond_3
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/e;->b:Lcom/google/android/apps/gmm/map/internal/e;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/e;->e:I

    add-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_1

    .line 458
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/e;->c:Lcom/google/android/apps/gmm/map/internal/e;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/f;->a(Lcom/google/android/apps/gmm/map/internal/e;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 0

    .prologue
    .line 415
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/f;->c:Lcom/google/android/apps/gmm/v/g;

    .line 416
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/n;)V
    .locals 2

    .prologue
    .line 464
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/f;->e:Lcom/google/android/apps/gmm/v/n;

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->c:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->c:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 468
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 472
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->e:Lcom/google/android/apps/gmm/v/n;

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->e:Lcom/google/android/apps/gmm/v/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/f;->g:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/f;->h:F

    sub-float/2addr v0, v1

    .line 474
    const v1, 0x3f36db6e

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/f;->e:Lcom/google/android/apps/gmm/v/n;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 475
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/f;->f:Lcom/google/android/apps/gmm/v/aa;

    new-instance v3, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    const/4 v4, 0x0

    .line 476
    iget-object v5, v3, Lcom/google/android/apps/gmm/v/cj;->a:[F

    invoke-static {v5, v6, v0, v1, v4}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->g:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/f;->g:F

    const/high16 v4, 0x3f800000    # 1.0f

    .line 477
    iget-object v5, v3, Lcom/google/android/apps/gmm/v/cj;->a:[F

    invoke-static {v5, v6, v0, v1, v4}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 475
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/cj;)V

    .line 478
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->e:Lcom/google/android/apps/gmm/v/n;

    .line 480
    :cond_0
    monitor-enter p0

    .line 481
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/b;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/f;->a:Lcom/google/android/apps/gmm/map/internal/e;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 492
    :goto_0
    monitor-exit p0

    return-void

    .line 483
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->d:Lcom/google/android/apps/gmm/v/x;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/e;->a:Lcom/google/android/apps/gmm/map/internal/e;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/e;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/x;->a(I)V

    goto :goto_0

    .line 492
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 486
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->d:Lcom/google/android/apps/gmm/v/x;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/e;->b:Lcom/google/android/apps/gmm/map/internal/e;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/e;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/x;->a(I)V

    goto :goto_0

    .line 489
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/f;->d:Lcom/google/android/apps/gmm/v/x;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/e;->c:Lcom/google/android/apps/gmm/map/internal/e;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/e;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/x;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 481
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
