.class public Lcom/google/android/apps/gmm/iamhere/d/af;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/iamhere/d/an;

.field final c:Lcom/google/android/apps/gmm/iamhere/d/au;

.field d:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/d/d;",
            ">;"
        }
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/base/a;

.field f:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/gmm/iamhere/d/af;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/af;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/iamhere/d/an;Lcom/google/android/apps/gmm/iamhere/d/au;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    .line 63
    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->c:Lcom/google/android/apps/gmm/iamhere/d/au;

    .line 64
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/iamhere/c/g;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 249
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 250
    const v1, 0x50808000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 251
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 252
    const-string v1, "notification_activity_intent_type"

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/d/at;->b:Lcom/google/android/apps/gmm/iamhere/d/at;

    .line 253
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/iamhere/d/at;->name()Ljava/lang/String;

    move-result-object v2

    .line 252
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 254
    const-string v1, "card_action_type"

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/iamhere/c/g;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 255
    return-object v0
.end method


# virtual methods
.method a(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 5

    .prologue
    const v3, -0x21524111

    .line 106
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/af;->a:Ljava/lang/String;

    const-string v0, "Loading Intent: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 107
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 109
    :try_start_0
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :goto_1
    return-void

    .line 106
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/af;->a:Ljava/lang/String;

    const-string v0, "Could not load URI: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 112
    :goto_2
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/iamhere/d/an;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/iamhere/d/an;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    iget-object v3, v3, Lcom/google/android/apps/gmm/iamhere/d/an;->c:Lcom/google/android/libraries/curvular/cg;

    iget-object v4, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    iget-object v4, v4, Lcom/google/android/apps/gmm/iamhere/d/an;->d:Lcom/google/android/libraries/curvular/cg;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/d/h;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/curvular/cg;Lcom/google/android/libraries/curvular/cg;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->d:Lcom/google/android/libraries/curvular/ae;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->d:Lcom/google/android/libraries/curvular/ae;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/iamhere/d/an;->setContentView(Landroid/view/View;)V

    goto :goto_1

    .line 111
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 236
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/af;->a:Ljava/lang/String;

    const-string v0, "Loading Url in Webview: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 237
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/iamhere/d/ak;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/iamhere/d/ak;-><init>(Lcom/google/android/apps/gmm/iamhere/d/af;Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 243
    return-void

    .line 236
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final a(Ljava/lang/String;Lcom/google/android/apps/gmm/iamhere/d/am;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->B_()Landroid/accounts/Account;

    move-result-object v1

    .line 185
    if-nez v1, :cond_0

    .line 186
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/af;->a:Ljava/lang/String;

    .line 187
    invoke-interface {p2, p1}, Lcom/google/android/apps/gmm/iamhere/d/am;->a(Ljava/lang/String;)V

    .line 189
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/af;->a:Ljava/lang/String;

    const-string v0, "Fetched current account:"

    iget-object v2, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 190
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 191
    const-string v4, "local"

    const-string v2, "appengine.google.com"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v4, "ah"

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1b

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "weblogin:service="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&continue="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    new-instance v5, Lcom/google/android/apps/gmm/iamhere/d/aj;

    invoke-direct {v5, p0, p1, p2}, Lcom/google/android/apps/gmm/iamhere/d/aj;-><init>(Lcom/google/android/apps/gmm/iamhere/d/af;Ljava/lang/String;Lcom/google/android/apps/gmm/iamhere/d/am;)V

    move-object v6, v3

    .line 190
    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 211
    return-void

    .line 189
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 191
    :cond_2
    const-string v2, "appspot.com"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "googleplex.com"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    const-string v4, "ah"

    const-string v2, "https://appengine.google.com/_ah/conflogin?continue="

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v5, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    sget v1, Lcom/google/android/apps/gmm/h;->R:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/iamhere/d/an;->setContentView(I)V

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    sget v1, Lcom/google/android/apps/gmm/g;->eE:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/iamhere/d/an;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->f:Landroid/webkit/WebView;

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/d/an;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    sget v2, Lcom/google/android/apps/gmm/g;->cq:I

    .line 135
    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/iamhere/d/an;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    .line 136
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->a()V

    .line 138
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->f:Landroid/webkit/WebView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 142
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->f:Landroid/webkit/WebView;

    new-instance v3, Lcom/google/android/apps/gmm/iamhere/d/ah;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/gmm/iamhere/d/ah;-><init>(Lcom/google/android/apps/gmm/iamhere/d/af;Lcom/google/android/apps/gmm/base/views/GmmProgressBar;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 159
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->f:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 161
    new-array v2, v4, [Landroid/view/View;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/iamhere/d/af;->f:Landroid/webkit/WebView;

    aput-object v4, v2, v3

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->b:[Landroid/view/View;

    .line 162
    if-eqz p1, :cond_0

    .line 163
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/ai;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/ai;-><init>(Lcom/google/android/apps/gmm/iamhere/d/af;)V

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/iamhere/d/af;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/iamhere/d/am;)V

    .line 167
    :goto_0
    return-void

    .line 165
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/iamhere/d/af;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
