.class public final Lcom/google/android/apps/gmm/streetview/internal/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/f;


# instance fields
.field a:Lcom/google/android/apps/gmm/v/g;

.field final b:Lcom/google/android/apps/gmm/streetview/internal/aq;

.field public c:Z

.field d:I

.field e:Lcom/google/android/apps/gmm/streetview/internal/c;

.field private f:Z

.field private g:Lcom/google/android/apps/gmm/v/cn;

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:F

.field private s:F

.field private final t:Landroid/widget/Scroller;

.field private final u:Lcom/google/android/apps/gmm/streetview/b/a;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/streetview/internal/aq;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0, v1, v1, v1}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->g:Lcom/google/android/apps/gmm/v/cn;

    .line 64
    sget-object v0, Lcom/google/android/apps/gmm/streetview/internal/c;->a:Lcom/google/android/apps/gmm/streetview/internal/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->e:Lcom/google/android/apps/gmm/streetview/internal/c;

    .line 74
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 75
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/streetview/internal/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->b:Lcom/google/android/apps/gmm/streetview/internal/aq;

    .line 76
    new-instance v0, Lcom/google/android/apps/gmm/streetview/b/a;

    iget-object v1, p2, Lcom/google/android/apps/gmm/streetview/internal/aq;->f:Lcom/google/android/apps/gmm/streetview/b/a;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/streetview/b/a;-><init>(Lcom/google/android/apps/gmm/streetview/b/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    .line 77
    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->t:Landroid/widget/Scroller;

    .line 78
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->c:Z

    .line 79
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->f:Z

    .line 80
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/v/h;
    .locals 1

    .prologue
    .line 278
    sget-object v0, Lcom/google/android/apps/gmm/v/h;->a:Lcom/google/android/apps/gmm/v/h;

    return-object v0
.end method

.method final declared-synchronized a(F)V
    .locals 9

    .prologue
    .line 187
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/streetview/internal/c;->c:Lcom/google/android/apps/gmm/streetview/internal/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->e:Lcom/google/android/apps/gmm/streetview/internal/c;

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->t:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->t:Landroid/widget/Scroller;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v3, 0x447a0000    # 1000.0f

    mul-float/2addr v3, p1

    float-to-int v3, v3

    const/4 v4, 0x0

    const/high16 v5, -0x80000000

    const v6, 0x7fffffff

    const/high16 v7, -0x80000000

    const v8, 0x7fffffff

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->s:F

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :cond_0
    monitor-exit p0

    return-void

    .line 187
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(FF)V
    .locals 9

    .prologue
    .line 171
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/streetview/internal/c;->b:Lcom/google/android/apps/gmm/streetview/internal/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->e:Lcom/google/android/apps/gmm/streetview/internal/c;

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->t:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->t:Landroid/widget/Scroller;

    const/4 v1, 0x0

    const/4 v2, 0x0

    float-to-int v3, p1

    float-to-int v4, p2

    const/high16 v5, -0x80000000

    const v6, 0x7fffffff

    const/high16 v7, -0x80000000

    const v8, 0x7fffffff

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->q:F

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->r:F

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->l:F

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    :cond_0
    monitor-exit p0

    return-void

    .line 171
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/streetview/b/a;)V
    .locals 2

    .prologue
    .line 207
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->h:F

    .line 208
    iget v0, p1, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->i:F

    .line 209
    iget v0, p1, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->l:F

    .line 210
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->n:F

    .line 211
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->o:F

    .line 212
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->p:F

    .line 213
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->d:I

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    :cond_0
    monitor-exit p0

    return-void

    .line 207
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/streetview/b/a;Lcom/google/android/apps/gmm/streetview/b/a;I)V
    .locals 4

    .prologue
    const/high16 v3, 0x43340000    # 180.0f

    const/high16 v2, 0x43b40000    # 360.0f

    .line 137
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->h:F

    .line 138
    iget v0, p2, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->j:F

    .line 141
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->j:F

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->h:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    .line 142
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->j:F

    rem-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->j:F

    .line 143
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->h:F

    rem-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->h:F

    .line 144
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->j:F

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->h:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    .line 145
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->j:F

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->h:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 146
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->j:F

    add-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->j:F

    .line 153
    :cond_0
    :goto_0
    iget v0, p1, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->i:F

    .line 154
    iget v0, p2, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->k:F

    .line 156
    iget v0, p1, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->l:F

    .line 157
    iget v0, p2, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->m:F

    .line 159
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->j:F

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->h:F

    sub-float/2addr v0, v1

    int-to-float v1, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->n:F

    .line 160
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->k:F

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->i:F

    sub-float/2addr v0, v1

    int-to-float v1, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->o:F

    .line 161
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->m:F

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->l:F

    sub-float/2addr v0, v1

    int-to-float v1, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->p:F

    .line 162
    iput p3, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->d:I

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    :cond_1
    monitor-exit p0

    return-void

    .line 148
    :cond_2
    :try_start_1
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->h:F

    add-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->h:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/v/cn;Lcom/google/android/apps/gmm/v/cj;I)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 125
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->g:Lcom/google/android/apps/gmm/v/cn;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v2, 0x3

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v1, v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v1, 0x0

    iget-object v2, p2, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->g:Lcom/google/android/apps/gmm/v/cn;

    int-to-float v1, p3

    div-float v1, v6, v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v3, 0x0

    aget v4, v2, v3

    mul-float/2addr v4, v1

    aput v4, v2, v3

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v3, 0x1

    aget v4, v2, v3

    mul-float/2addr v4, v1

    aput v4, v2, v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v2, 0x2

    aget v3, v0, v2

    mul-float/2addr v1, v3

    aput v1, v0, v2

    .line 128
    iput p3, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->d:I

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->c:Z

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    :cond_0
    monitor-exit p0

    return-void

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 2

    .prologue
    .line 266
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 268
    :cond_0
    return-void
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 7

    .prologue
    .line 250
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->h:F

    iput v1, v0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->i:F

    iput v1, v0, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->l:F

    const/high16 v2, 0x41700000    # 15.0f

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/streetview/b/a;->a(FFF)F

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    .line 258
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->c:Z

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->b:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->g:Lcom/google/android/apps/gmm/v/cn;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/aq;->d:Lcom/google/android/apps/gmm/v/cn;

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v3, 0x0

    aget v4, v2, v3

    iget-object v5, v1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    add-float/2addr v4, v5

    aput v4, v2, v3

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v3, 0x1

    aget v4, v2, v3

    iget-object v5, v1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    add-float/2addr v4, v5

    aput v4, v2, v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v2, 0x2

    aget v3, v0, v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v4, 0x2

    aget v1, v1, v4

    add-float/2addr v1, v3

    aput v1, v0, v2

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->b:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/aq;->a(Lcom/google/android/apps/gmm/streetview/b/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    monitor-exit p0

    return-void

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 230
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->e:Lcom/google/android/apps/gmm/streetview/internal/c;

    sget-object v3, Lcom/google/android/apps/gmm/streetview/internal/c;->b:Lcom/google/android/apps/gmm/streetview/internal/c;

    if-ne v2, v3, :cond_2

    .line 231
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->t:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->t:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->t:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    iget v3, v3, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->b:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v4, v4, Lcom/google/android/apps/gmm/streetview/internal/aq;->e:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->q:F

    int-to-float v1, v1

    mul-float/2addr v1, v3

    sub-float v1, v4, v1

    iget v4, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->r:F

    int-to-float v2, v2

    mul-float/2addr v2, v3

    add-float/2addr v2, v4

    invoke-static {v1}, Lcom/google/android/apps/gmm/shared/c/s;->a(F)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->h:F

    const/high16 v1, -0x3d4c0000    # -90.0f

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-static {v2, v1, v3}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FFF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->i:F

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, p0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 231
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/android/apps/gmm/streetview/internal/c;->a:Lcom/google/android/apps/gmm/streetview/internal/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->e:Lcom/google/android/apps/gmm/streetview/internal/c;

    move v0, v1

    goto :goto_0

    .line 232
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->e:Lcom/google/android/apps/gmm/streetview/internal/c;

    sget-object v3, Lcom/google/android/apps/gmm/streetview/internal/c;->c:Lcom/google/android/apps/gmm/streetview/internal/c;

    if-ne v2, v3, :cond_4

    .line 233
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->t:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->t:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->s:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->l:F

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, p0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 230
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 233
    :cond_3
    :try_start_2
    sget-object v0, Lcom/google/android/apps/gmm/streetview/internal/c;->a:Lcom/google/android/apps/gmm/streetview/internal/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->e:Lcom/google/android/apps/gmm/streetview/internal/c;

    move v0, v1

    goto :goto_0

    .line 236
    :cond_4
    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->h:F

    iget v3, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->n:F

    add-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->h:F

    .line 237
    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->i:F

    iget v3, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->o:F

    add-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->i:F

    .line 238
    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->l:F

    iget v3, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->p:F

    add-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->l:F

    .line 240
    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->d:I

    add-int/lit8 v3, v2, -0x1

    iput v3, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->d:I

    if-lez v2, :cond_5

    .line 241
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, p0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    goto :goto_0

    .line 244
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->c:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    .line 245
    goto :goto_0
.end method

.method public final declared-synchronized c()Lcom/google/android/apps/gmm/streetview/b/a;
    .locals 2

    .prologue
    .line 221
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/apps/gmm/streetview/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/b;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/streetview/b/a;-><init>(Lcom/google/android/apps/gmm/streetview/b/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
