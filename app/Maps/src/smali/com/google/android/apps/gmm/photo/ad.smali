.class public Lcom/google/android/apps/gmm/photo/ad;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/photo/a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    .line 277
    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 18

    .prologue
    .line 100
    const/4 v2, 0x5

    new-array v10, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    .line 101
    sget v3, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v10, v2

    const/4 v2, 0x1

    const/4 v3, -0x1

    .line 103
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v10, v2

    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 104
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v10, v2

    const/4 v11, 0x3

    const/4 v2, 0x4

    new-array v12, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 107
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v12, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 108
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v12, v2

    const/4 v2, 0x2

    const/high16 v3, 0x3f800000    # 1.0f

    .line 109
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v12, v2

    const/4 v13, 0x3

    const/4 v2, 0x7

    new-array v14, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 112
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v14, v2

    const/4 v3, 0x1

    const/4 v2, 0x6

    new-array v4, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v5, 0x0

    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    .line 115
    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_1

    double-to-int v6, v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v2, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->as:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x1

    .line 116
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x2

    .line 117
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ao:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x3

    .line 119
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->b()Lcom/google/android/libraries/curvular/ar;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x4

    .line 120
    sget v5, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v5}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x5

    sget v5, Lcom/google/android/apps/gmm/l;->on:I

    .line 122
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    .line 114
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v4}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v4, "android.widget.TextView"

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v14, v3

    const/4 v3, 0x2

    const/4 v2, 0x4

    new-array v4, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v5, 0x0

    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    .line 127
    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_2

    double-to-int v6, v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v2, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->as:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v5, 0x1

    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    .line 128
    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_3

    double-to-int v6, v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v2, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->an:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x2

    .line 130
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/photo/ad;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x3

    const/4 v5, 0x1

    .line 131
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    .line 126
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v4}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v4, "android.widget.LinearLayout"

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v14, v3

    const/4 v15, 0x3

    const/4 v2, 0x1

    .line 136
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 137
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v3}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/curvular/ce;

    check-cast v3, Lcom/google/android/apps/gmm/photo/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/photo/a;->h()Lcom/google/android/libraries/curvular/cf;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/f;->cJ:I

    .line 139
    sget v5, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v5}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/l;->aO:I

    .line 140
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 141
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->f()Lcom/google/android/libraries/curvular/ar;

    move-result-object v6

    .line 142
    sget v7, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v7}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-array v9, v9, [Lcom/google/android/libraries/curvular/cu;

    .line 135
    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/gmm/base/f/by;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/cu;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/apps/gmm/z/b/l;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v14, v15

    const/4 v15, 0x4

    const/4 v2, 0x1

    .line 148
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 149
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v3}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/curvular/ce;

    check-cast v3, Lcom/google/android/apps/gmm/photo/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/photo/a;->g()Lcom/google/android/libraries/curvular/cf;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/f;->dk:I

    .line 151
    sget v5, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v5}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/l;->aP:I

    .line 152
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 153
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->f()Lcom/google/android/libraries/curvular/ar;

    move-result-object v6

    .line 154
    sget v7, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v7}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-array v9, v9, [Lcom/google/android/libraries/curvular/cu;

    .line 147
    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/gmm/base/f/by;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/cu;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/apps/gmm/z/b/l;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v14, v15

    const/4 v3, 0x5

    const/4 v2, 0x3

    new-array v4, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v5, 0x0

    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    .line 160
    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_4

    double-to-int v6, v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v2, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->as:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v5, 0x1

    .line 162
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/photo/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/photo/a;->d()Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v6

    const/4 v2, 0x2

    new-array v7, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    .line 164
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v2, 0x1

    .line 165
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->m()Lcom/google/android/libraries/curvular/au;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->ao:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v7, v2

    .line 161
    const-wide/high16 v8, 0x4040000000000000L    # 32.0

    new-instance v15, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_5

    double-to-int v8, v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_4
    invoke-direct {v15, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-static {v6, v15, v7}, Lcom/google/android/apps/gmm/base/k/ao;->a(Lcom/google/android/apps/gmm/base/views/c/k;Lcom/google/android/libraries/curvular/au;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v5, 0x2

    const/4 v2, 0x5

    new-array v6, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    .line 169
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->ao:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x1

    .line 171
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->f()Lcom/google/android/libraries/curvular/ar;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x2

    .line 172
    sget v7, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v7}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v7, 0x3

    sget v2, Lcom/google/android/apps/gmm/l;->om:I

    .line 174
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v8

    const/4 v2, 0x1

    new-array v9, v2, [Ljava/lang/Object;

    const/4 v15, 0x0

    .line 175
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/photo/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/photo/a;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v15

    .line 174
    invoke-static {v8, v9}, Lcom/google/android/libraries/curvular/t;->a(Ljava/lang/CharSequence;[Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v6, v7

    const/4 v7, 0x4

    .line 178
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/photo/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/photo/a;->b()Ljava/lang/Boolean;

    move-result-object v8

    sget v2, Lcom/google/android/apps/gmm/l;->om:I

    .line 179
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v9

    const/4 v2, 0x1

    new-array v15, v2, [Ljava/lang/Object;

    const/16 v16, 0x0

    .line 180
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/photo/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/photo/a;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v15, v16

    .line 179
    invoke-static {v9, v15}, Lcom/google/android/libraries/curvular/t;->a(Ljava/lang/CharSequence;[Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    sget v2, Lcom/google/android/apps/gmm/l;->oo:I

    .line 181
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v15

    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    .line 182
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/photo/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/photo/a;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v16, v17

    .line 181
    invoke-static/range {v15 .. v16}, Lcom/google/android/libraries/curvular/t;->a(Ljava/lang/CharSequence;[Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    .line 177
    invoke-static {v8, v9, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v6, v7

    .line 168
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v6}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v6, "android.widget.TextView"

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v4, v5

    .line 159
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v4}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v4, "android.widget.LinearLayout"

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v14, v3

    const/4 v3, 0x6

    const/4 v2, 0x7

    new-array v4, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    .line 189
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->e()Lcom/google/android/libraries/curvular/au;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x1

    .line 190
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ao:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x2

    .line 191
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->i()Lcom/google/android/libraries/curvular/au;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->an:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x3

    const/4 v5, -0x2

    .line 192
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x4

    .line 194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/photo/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/photo/a;->i()Lcom/google/android/libraries/curvular/cf;

    move-result-object v6

    const/4 v2, 0x0

    if-eqz v6, :cond_0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v6, v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v6

    new-instance v2, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v2, v6}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_0
    sget-object v6, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x5

    .line 196
    sget v5, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v5}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x6

    sget v5, Lcom/google/android/apps/gmm/l;->kY:I

    .line 197
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    .line 188
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v4}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v4, "android.widget.TextView"

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v14, v3

    .line 111
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v14}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.LinearLayout"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v12, v13

    .line 106
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v12}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.ScrollView"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v10, v11

    const/4 v9, 0x4

    sget v2, Lcom/google/android/apps/gmm/l;->bg:I

    .line 203
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->kH:I

    .line 204
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 205
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/photo/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/photo/a;->e()Lcom/google/android/libraries/curvular/cf;

    move-result-object v4

    .line 206
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v5}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/libraries/curvular/ce;

    check-cast v5, Lcom/google/android/apps/gmm/photo/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/photo/a;->f()Lcom/google/android/libraries/curvular/cf;

    move-result-object v5

    const/4 v6, 0x1

    .line 207
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v7, 0x0

    .line 208
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const/4 v8, 0x0

    new-array v8, v8, [Lcom/google/android/libraries/curvular/cu;

    .line 202
    invoke-static/range {v2 .. v8}, Lcom/google/android/apps/gmm/base/f/ad;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/cf;Ljava/lang/Boolean;Ljava/lang/Boolean;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v10, v9

    .line 100
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v10}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.LinearLayout"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    return-object v2

    .line 115
    :cond_1
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v6, v6, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    .line 127
    :cond_2
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v6, v6, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    .line 128
    :cond_3
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v6, v6, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    .line 160
    :cond_4
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v6, v6, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3

    .line 161
    :cond_5
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v8, v8, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_4
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 3

    .prologue
    .line 97
    check-cast p2, Lcom/google/android/apps/gmm/photo/a;

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Lcom/google/android/apps/gmm/photo/a;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v0, v1, :cond_0

    const-class v1, Lcom/google/android/apps/gmm/photo/af;

    new-instance v2, Lcom/google/android/apps/gmm/photo/ae;

    invoke-direct {v2, p0, p2, v0}, Lcom/google/android/apps/gmm/photo/ae;-><init>(Lcom/google/android/apps/gmm/photo/ad;Lcom/google/android/apps/gmm/photo/a;I)V

    invoke-virtual {p4, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
