.class Lcom/google/android/apps/gmm/car/g;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/f;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/f;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/g;->a:Lcom/google/android/apps/gmm/car/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/car/a/e;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 136
    sget-object v0, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/apps/gmm/car/a/e;->a:Lcom/google/android/apps/gmm/car/a/f;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onNavigationFocusRequestEvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/a/e;->a:Lcom/google/android/apps/gmm/car/a/f;

    sget-object v1, Lcom/google/android/apps/gmm/car/a/f;->a:Lcom/google/android/apps/gmm/car/a/f;

    if-eq v0, v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/g;->a:Lcom/google/android/apps/gmm/car/f;

    iget-object v0, p1, Lcom/google/android/apps/gmm/car/a/e;->a:Lcom/google/android/apps/gmm/car/a/f;

    sget-object v2, Lcom/google/android/apps/gmm/car/a/f;->b:Lcom/google/android/apps/gmm/car/a/f;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/car/f;->a(Lcom/google/android/apps/gmm/car/f;Z)V

    .line 140
    :cond_0
    return-void

    .line 138
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/l/n;)V
    .locals 5
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 144
    iget-object v0, p1, Lcom/google/android/apps/gmm/l/n;->a:Landroid/content/Intent;

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/g;->a:Lcom/google/android/apps/gmm/car/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/gmm/car/GmmCarProjectionService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 146
    sget-object v1, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Starting car projection service with intent "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/g;->a:Lcom/google/android/apps/gmm/car/f;

    :try_start_0
    iget-object v2, v1, Lcom/google/android/apps/gmm/car/f;->b:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Lcom/google/android/gms/car/ao; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    iget-object v3, v1, Lcom/google/android/apps/gmm/car/f;->d:Lcom/google/android/apps/gmm/car/e;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/car/e;->a()Lcom/google/android/gms/car/d;

    move-result-object v3

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/f;->f:Lcom/google/android/gms/common/api/o;

    invoke-interface {v3, v1, v0}, Lcom/google/android/gms/car/d;->a(Lcom/google/android/gms/common/api/o;Landroid/content/Intent;)V

    monitor-exit v2

    .line 148
    :goto_0
    return-void

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Lcom/google/android/gms/car/ao; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Exception sending startCarActivity intent "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Exception sending startCarActivity intent "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_2
    move-exception v0

    sget-object v1, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Exception sending startCarActivity intent "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 152
    sget-object v1, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;

    .line 157
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/g;->a:Lcom/google/android/apps/gmm/car/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/f;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 158
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/g;->a:Lcom/google/android/apps/gmm/car/f;

    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v3, :cond_1

    :goto_0
    iput-boolean v0, v2, Lcom/google/android/apps/gmm/car/f;->p:Z

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g;->a:Lcom/google/android/apps/gmm/car/f;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/f;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g;->a:Lcom/google/android/apps/gmm/car/f;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/f;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g;->a:Lcom/google/android/apps/gmm/car/f;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/f;->n:Z

    if-nez v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g;->a:Lcom/google/android/apps/gmm/car/f;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/f;->n:Z

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g;->a:Lcom/google/android/apps/gmm/car/f;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/car/f;->a(Lcom/google/android/apps/gmm/car/f;Z)V

    .line 163
    :cond_0
    monitor-exit v1

    return-void

    .line 158
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
