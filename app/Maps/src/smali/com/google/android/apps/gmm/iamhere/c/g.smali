.class public final enum Lcom/google/android/apps/gmm/iamhere/c/g;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/iamhere/c/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/iamhere/c/g;

.field public static final enum b:Lcom/google/android/apps/gmm/iamhere/c/g;

.field public static final enum c:Lcom/google/android/apps/gmm/iamhere/c/g;

.field public static final enum d:Lcom/google/android/apps/gmm/iamhere/c/g;

.field public static final enum e:Lcom/google/android/apps/gmm/iamhere/c/g;

.field public static final enum f:Lcom/google/android/apps/gmm/iamhere/c/g;

.field public static final enum g:Lcom/google/android/apps/gmm/iamhere/c/g;

.field public static final enum h:Lcom/google/android/apps/gmm/iamhere/c/g;

.field public static final enum i:Lcom/google/android/apps/gmm/iamhere/c/g;

.field private static final synthetic j:[Lcom/google/android/apps/gmm/iamhere/c/g;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 121
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/g;

    const-string v1, "GMM_PLACE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/iamhere/c/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/g;->a:Lcom/google/android/apps/gmm/iamhere/c/g;

    .line 129
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/g;

    const-string v1, "GMM_TRANSIT_TIMETABLE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/iamhere/c/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/g;->b:Lcom/google/android/apps/gmm/iamhere/c/g;

    .line 138
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/g;

    const-string v1, "GMM_DIRECTIONS"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/iamhere/c/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/g;->c:Lcom/google/android/apps/gmm/iamhere/c/g;

    .line 146
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/g;

    const-string v1, "DOWNLOAD"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/iamhere/c/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/g;->d:Lcom/google/android/apps/gmm/iamhere/c/g;

    .line 155
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/g;

    const-string v1, "GENERIC_PUBLIC_URL"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/iamhere/c/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/g;->e:Lcom/google/android/apps/gmm/iamhere/c/g;

    .line 165
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/g;

    const-string v1, "GENERIC_GAIA_URL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/c/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/g;->f:Lcom/google/android/apps/gmm/iamhere/c/g;

    .line 175
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/g;

    const-string v1, "GENERIC_GAIA_SYSTEM"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/c/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/g;->g:Lcom/google/android/apps/gmm/iamhere/c/g;

    .line 187
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/g;

    const-string v1, "GENERIC_SYSTEM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/c/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/g;->h:Lcom/google/android/apps/gmm/iamhere/c/g;

    .line 206
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/g;

    const-string v1, "GENERIC_IN_PLACE_ACTION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/c/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/g;->i:Lcom/google/android/apps/gmm/iamhere/c/g;

    .line 114
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/gmm/iamhere/c/g;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/g;->a:Lcom/google/android/apps/gmm/iamhere/c/g;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/g;->b:Lcom/google/android/apps/gmm/iamhere/c/g;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/g;->c:Lcom/google/android/apps/gmm/iamhere/c/g;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/g;->d:Lcom/google/android/apps/gmm/iamhere/c/g;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/g;->e:Lcom/google/android/apps/gmm/iamhere/c/g;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/g;->f:Lcom/google/android/apps/gmm/iamhere/c/g;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/g;->g:Lcom/google/android/apps/gmm/iamhere/c/g;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/g;->h:Lcom/google/android/apps/gmm/iamhere/c/g;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/g;->i:Lcom/google/android/apps/gmm/iamhere/c/g;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/g;->j:[Lcom/google/android/apps/gmm/iamhere/c/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/iamhere/c/g;
    .locals 1

    .prologue
    .line 114
    const-class v0, Lcom/google/android/apps/gmm/iamhere/c/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/g;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/iamhere/c/g;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/g;->j:[Lcom/google/android/apps/gmm/iamhere/c/g;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/iamhere/c/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/iamhere/c/g;

    return-object v0
.end method
