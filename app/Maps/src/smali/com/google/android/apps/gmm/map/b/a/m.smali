.class public final Lcom/google/android/apps/gmm/map/b/a/m;
.super Lcom/google/android/apps/gmm/map/b/a/af;
.source "PG"


# instance fields
.field public a:[Lcom/google/android/apps/gmm/map/b/a/y;

.field final b:Lcom/google/android/apps/gmm/map/b/a/ae;


# direct methods
.method protected constructor <init>([Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 3

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/b/a/af;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ae;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/b/a/ae;->a([Lcom/google/android/apps/gmm/map/b/a/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/m;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 20
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/m;
    .locals 2

    .prologue
    .line 32
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/b/a/y;

    .line 33
    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 34
    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 35
    const/4 v1, 0x2

    aput-object p3, v0, v1

    .line 36
    const/4 v1, 0x3

    aput-object p2, v0, v1

    .line 37
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/m;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/m;-><init>([Lcom/google/android/apps/gmm/map/b/a/y;)V

    return-object v1
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 120
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v0, v0, v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v3, v3, v1

    invoke-static {v0, v3, p1}, Lcom/google/android/apps/gmm/map/b/a/z;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 124
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v3, v3, v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v4, v4, v5

    invoke-static {v3, v4, p1}, Lcom/google/android/apps/gmm/map/b/a/z;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 125
    add-int/lit8 v0, v0, 0x1

    .line 127
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v3, v3, v5

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v4, v4, v6

    invoke-static {v3, v4, p1}, Lcom/google/android/apps/gmm/map/b/a/z;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 128
    add-int/lit8 v0, v0, 0x1

    .line 130
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v3, v3, v6

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v4, v4, v2

    invoke-static {v3, v4, p1}, Lcom/google/android/apps/gmm/map/b/a/z;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 131
    add-int/lit8 v0, v0, 0x1

    .line 133
    :cond_2
    if-ne v0, v1, :cond_3

    :goto_1
    return v1

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x4

    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/b/a/af;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/m;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/af;->c()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 151
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 146
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/af;->b()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 147
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/b/a/m;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 151
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final c()Lcom/google/android/apps/gmm/map/b/a/ae;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/m;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 102
    if-ne p0, p1, :cond_0

    .line 103
    const/4 v0, 0x1

    .line 109
    :goto_0
    return v0

    .line 105
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/b/a/m;

    if-eqz v0, :cond_1

    .line 106
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/m;

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 109
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x5

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
