.class public Lcom/google/android/apps/gmm/map/l/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:[F

.field public b:[F

.field public c:Z

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/l/b;-><init>(I)V

    .line 40
    return-void
.end method

.method private constructor <init>(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/l/b;->a(I)V

    .line 45
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/l/b;->c:Z

    .line 46
    iput v0, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    .line 47
    return-void
.end method


# virtual methods
.method public final a(FLcom/google/android/apps/gmm/map/b/a/ay;)I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 174
    iget v2, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    if-ge v2, v3, :cond_0

    move v0, v1

    .line 186
    :goto_0
    return v0

    .line 178
    :cond_0
    cmpg-float v2, p1, v0

    if-gtz v2, :cond_1

    .line 179
    invoke-virtual {p0, v1, p2}, Lcom/google/android/apps/gmm/map/l/b;->a(ILcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    move v0, v1

    .line 180
    goto :goto_0

    .line 182
    :cond_1
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_2

    .line 183
    iget v0, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/gmm/map/l/b;->a(ILcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 184
    iget v0, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v0, v0, -0x2

    goto :goto_0

    .line 186
    :cond_2
    iget v1, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    if-ge v1, v3, :cond_3

    :goto_1
    mul-float/2addr v0, p1

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/gmm/map/l/b;->b(FLcom/google/android/apps/gmm/map/b/a/ay;)I

    move-result v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/l/b;->b()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    iget v1, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_1
.end method

.method public final a(ILcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    shl-int/lit8 v1, p1, 0x1

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    shl-int/lit8 v2, p1, 0x1

    add-int/lit8 v2, v2, 0x1

    aget v1, v1, v2

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v1, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 84
    return-object p2
.end method

.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 137
    iput v0, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    .line 138
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/l/b;->c:Z

    .line 139
    return-void
.end method

.method public final a(FF)V
    .locals 2

    .prologue
    .line 74
    iget v0, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/l/b;->a(I)V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    iget v1, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    shl-int/lit8 v1, v1, 0x1

    aput p1, v0, v1

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    iget v1, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, 0x1

    aput p2, v0, v1

    .line 77
    iget v0, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/l/b;->c:Z

    .line 79
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    if-nez v0, :cond_2

    move v0, v1

    .line 55
    :goto_0
    if-ge v0, p1, :cond_1

    .line 56
    shl-int/lit8 v0, v0, 0x1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 57
    shl-int/lit8 v0, v0, 0x1

    new-array v0, v0, [F

    .line 58
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    if-eqz v2, :cond_0

    .line 59
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    array-length v3, v3

    invoke-static {v2, v1, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 61
    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    .line 63
    :cond_1
    return-void

    .line 54
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method public final b(I)F
    .locals 6

    .prologue
    .line 123
    iget v0, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Lcom/google/b/a/aq;->a(II)I

    .line 125
    shl-int/lit8 v0, p1, 0x1

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    aget v1, v1, v0

    .line 127
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    add-int/lit8 v3, v0, 0x1

    aget v2, v2, v3

    .line 128
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    add-int/lit8 v4, v0, 0x2

    aget v3, v3, v4

    .line 129
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    add-int/lit8 v0, v0, 0x3

    aget v0, v4, v0

    .line 130
    sub-float/2addr v0, v2

    float-to-double v4, v0

    sub-float v0, v3, v1

    float-to-double v0, v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public b(FLcom/google/android/apps/gmm/map/b/a/ay;)I
    .locals 7

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/l/b;->b()V

    .line 200
    iget v0, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v1, v0, -0x1

    .line 201
    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v2, v1, -0x1

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    add-int/lit8 v3, v0, 0x1

    aget v2, v2, v3

    cmpg-float v2, v2, p1

    if-gez v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    aget v2, v2, v0

    sub-float/2addr v1, v2

    .line 204
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    aget v2, v2, v0

    sub-float v2, p1, v2

    .line 205
    div-float v1, v2, v1

    .line 207
    shl-int/lit8 v2, v0, 0x1

    .line 208
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    aget v3, v3, v2

    .line 209
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    add-int/lit8 v5, v2, 0x1

    aget v4, v4, v5

    .line 210
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    add-int/lit8 v6, v2, 0x2

    aget v5, v5, v6

    .line 211
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    add-int/lit8 v2, v2, 0x3

    aget v2, v6, v2

    .line 212
    sub-float/2addr v5, v3

    mul-float/2addr v5, v1

    add-float/2addr v3, v5

    sub-float/2addr v2, v4

    mul-float/2addr v1, v2

    add-float/2addr v1, v4

    iput v3, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v1, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 213
    return v0
.end method

.method public b()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    .line 142
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/l/b;->c:Z

    if-eqz v0, :cond_0

    .line 159
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    array-length v0, v0

    iget v2, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    if-ge v0, v2, :cond_2

    .line 146
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v0, v2

    move v0, v1

    .line 150
    :goto_1
    iget v2, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    if-ge v0, v2, :cond_3

    .line 151
    add-int/lit8 v2, v0, -0x1

    shl-int/lit8 v2, v2, 0x1

    .line 152
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    aget v3, v3, v2

    .line 153
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    add-int/lit8 v5, v2, 0x1

    aget v4, v4, v5

    .line 154
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    add-int/lit8 v6, v2, 0x2

    aget v5, v5, v6

    .line 155
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    add-int/lit8 v2, v2, 0x3

    aget v2, v6, v2

    .line 156
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    add-int/lit8 v8, v0, -0x1

    aget v7, v7, v8

    sub-float v3, v5, v3

    float-to-double v8, v3

    sub-float/2addr v2, v4

    float-to-double v2, v2

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    double-to-float v2, v2

    add-float/2addr v2, v7

    aput v2, v6, v0

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 158
    :cond_3
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/l/b;->c:Z

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 369
    if-ne p0, p1, :cond_1

    move v1, v2

    .line 384
    :cond_0
    :goto_0
    return v1

    .line 371
    :cond_1
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/l/b;

    if-eqz v0, :cond_0

    .line 375
    check-cast p1, Lcom/google/android/apps/gmm/map/l/b;

    .line 376
    iget v0, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/l/b;->d:I

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 379
    :goto_1
    iget v3, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    shl-int/lit8 v3, v3, 0x1

    if-ge v0, v3, :cond_2

    .line 380
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    aget v3, v3, v0

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    aget v4, v4, v0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    .line 379
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    .line 384
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 389
    const/4 v1, 0x1

    .line 391
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    shl-int/lit8 v2, v2, 0x1

    if-ge v0, v2, :cond_0

    .line 392
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    aget v2, v2, v0

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    .line 393
    mul-int/lit8 v1, v1, 0x1f

    ushr-int/lit8 v3, v2, 0x10

    xor-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 391
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 395
    :cond_0
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 400
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 401
    const-string v0, "{"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    if-ge v0, v2, :cond_1

    .line 403
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    shl-int/lit8 v3, v0, 0x1

    aget v2, v2, v3

    .line 404
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/l/b;->a:[F

    shl-int/lit8 v4, v0, 0x1

    add-int/lit8 v4, v4, 0x1

    aget v3, v3, v4

    .line 405
    const-string v4, "("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    iget v2, p0, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 407
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 410
    :cond_1
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
