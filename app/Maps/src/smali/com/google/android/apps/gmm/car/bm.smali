.class public Lcom/google/android/apps/gmm/car/bm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public d:Lcom/google/android/apps/gmm/base/g/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public e:Lcom/google/android/apps/gmm/directions/a/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public f:Lcom/google/android/apps/gmm/map/r/a/ap;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/r/a/ap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/android/apps/gmm/base/g/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/r/a/ap;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 52
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/bm;->a:Ljava/lang/String;

    .line 53
    invoke-direct {p0, p3, p4}, Lcom/google/android/apps/gmm/car/bm;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    iput-object p5, p0, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    .line 55
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/car/bm;
    .locals 6

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->e()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    .line 60
    new-instance v0, Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v3

    .line 61
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-virtual {v4}, Lcom/google/r/b/a/ads;->h()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/bm;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/base/g/c;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 336
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_4

    :cond_0
    move v3, v2

    :goto_0
    if-eqz v3, :cond_5

    .line 337
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    if-eqz v1, :cond_3

    move-object p2, v0

    :cond_3
    iput-object p2, p0, Lcom/google/android/apps/gmm/car/bm;->b:Ljava/lang/String;

    .line 338
    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bm;->c:Ljava/lang/String;

    .line 343
    :goto_1
    return-void

    :cond_4
    move v3, v1

    .line 336
    goto :goto_0

    .line 340
    :cond_5
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/bm;->b:Ljava/lang/String;

    .line 341
    if-eqz p2, :cond_6

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_7

    :cond_6
    move v1, v2

    :cond_7
    if-eqz v1, :cond_8

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bm;->c:Ljava/lang/String;

    goto :goto_1

    :cond_8
    move-object v0, p2

    goto :goto_2
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/car/bn;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    if-nez v0, :cond_0

    .line 237
    sget-object v0, Lcom/google/android/apps/gmm/car/bn;->a:Lcom/google/android/apps/gmm/car/bn;

    .line 249
    :goto_0
    return-object v0

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 242
    sget-object v0, Lcom/google/android/apps/gmm/car/bn;->c:Lcom/google/android/apps/gmm/car/bn;

    goto :goto_0

    .line 245
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->o()Z

    move-result v0

    if-nez v0, :cond_2

    .line 246
    sget-object v0, Lcom/google/android/apps/gmm/car/bn;->d:Lcom/google/android/apps/gmm/car/bn;

    goto :goto_0

    .line 249
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/car/bn;->b:Lcom/google/android/apps/gmm/car/bn;

    goto :goto_0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/map/r/a/ao;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 273
    if-ltz p1, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    .line 274
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/bm;->b()I

    move-result v2

    if-ge p1, v2, :cond_2

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 275
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    if-ltz p1, :cond_4

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v0, v0

    if-gt v0, p1, :cond_5

    :cond_4
    const/4 v0, 0x0

    :goto_2
    return-object v0

    :cond_5
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, p1

    if-nez v0, :cond_6

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    new-instance v3, Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/map/r/a/ao;-><init>(Lcom/google/maps/g/a/hu;Lcom/google/android/apps/gmm/map/r/a/e;)V

    aput-object v3, v2, p1

    :cond_6
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, p1

    goto :goto_2
.end method

.method public final b()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 256
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v1

    if-nez v1, :cond_1

    .line 263
    :cond_0
    :goto_0
    return v0

    .line 259
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    .line 260
    if-eqz v1, :cond_0

    .line 263
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 294
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    .line 295
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->e()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 296
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_a

    :cond_1
    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 297
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bm;->b:Ljava/lang/String;

    .line 299
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_b

    :cond_4
    move v0, v1

    :goto_1
    if-nez v0, :cond_6

    .line 300
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    if-nez v0, :cond_5

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    :cond_5
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bm;->c:Ljava/lang/String;

    .line 307
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bm;->c:Ljava/lang/String;

    if-nez v0, :cond_9

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/c;->c:Ljava/lang/String;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_c

    :cond_7
    move v0, v1

    :goto_2
    if-nez v0, :cond_d

    move v0, v1

    :goto_3
    if-eqz v0, :cond_9

    .line 308
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    if-nez v1, :cond_8

    iget-object v1, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-virtual {v1}, Lcom/google/r/b/a/ads;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    :cond_8
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/car/bm;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :cond_9
    return-void

    :cond_a
    move v0, v2

    .line 296
    goto :goto_0

    :cond_b
    move v0, v2

    .line 299
    goto :goto_1

    :cond_c
    move v0, v2

    .line 307
    goto :goto_2

    :cond_d
    move v0, v2

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 372
    new-instance v2, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "query"

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bm;->a:Ljava/lang/String;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "title"

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bm;->b:Ljava/lang/String;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "subtitle"

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bm;->c:Ljava/lang/String;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "placemark"

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v1, "directionsFetcher"

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    if-nez v3, :cond_4

    const-string v0, "null"

    :goto_0
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v0, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v1, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    new-instance v4, Lcom/google/b/a/ak;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "error"

    invoke-interface {v3}, Lcom/google/android/apps/gmm/directions/a/c;->d()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/google/b/a/al;

    invoke-direct {v6}, Lcom/google/b/a/al;-><init>()V

    iget-object v7, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v6, v7, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v6, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "complete"

    invoke-interface {v3}, Lcom/google/android/apps/gmm/directions/a/c;->c()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/google/b/a/al;

    invoke-direct {v6}, Lcom/google/b/a/al;-><init>()V

    iget-object v7, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v6, v7, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v6, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "valid"

    invoke-interface {v3}, Lcom/google/android/apps/gmm/directions/a/c;->o()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/google/b/a/al;

    invoke-direct {v6}, Lcom/google/b/a/al;-><init>()V

    iget-object v7, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v6, v7, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v6, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "prefetch"

    invoke-interface {v3}, Lcom/google/android/apps/gmm/directions/a/c;->n()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/google/b/a/al;

    invoke-direct {v6}, Lcom/google/b/a/al;-><init>()V

    iget-object v7, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v6, v7, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v6, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "user"

    invoke-interface {v3}, Lcom/google/android/apps/gmm/directions/a/c;->e()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/google/b/a/al;

    invoke-direct {v6}, Lcom/google/b/a/al;-><init>()V

    iget-object v7, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v6, v7, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Ljava/lang/String;

    iput-object v0, v6, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "loading"

    invoke-interface {v3}, Lcom/google/android/apps/gmm/directions/a/c;->b()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/google/b/a/al;

    invoke-direct {v6}, Lcom/google/b/a/al;-><init>()V

    iget-object v7, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v6, v7, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    check-cast v0, Ljava/lang/String;

    iput-object v0, v6, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "initializing"

    invoke-interface {v3}, Lcom/google/android/apps/gmm/directions/a/c;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_c
    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "waypoint"

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
