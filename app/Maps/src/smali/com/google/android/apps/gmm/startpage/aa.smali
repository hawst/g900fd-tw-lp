.class public Lcom/google/android/apps/gmm/startpage/aa;
.super Lcom/google/android/apps/gmm/shared/net/e;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/shared/net/e",
        "<",
        "Lcom/google/r/b/a/xo;",
        "Lcom/google/r/b/a/xt;",
        ">;"
    }
.end annotation


# static fields
.field private static final I:Ljava/lang/String;


# instance fields
.field final A:Lcom/google/android/apps/gmm/startpage/d/e;

.field B:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field C:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field D:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/acy;",
            ">;"
        }
    .end annotation
.end field

.field E:Lcom/google/android/apps/gmm/startpage/ae;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field volatile F:Lcom/google/r/b/a/xt;

.field private G:Z

.field private H:Z

.field public a:Lcom/google/android/apps/gmm/map/b/a/r;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final b:Landroid/accounts/Account;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final c:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/o/h/a/dq;",
            ">;"
        }
    .end annotation
.end field

.field d:Z

.field e:Z

.field f:Lcom/google/o/h/a/eq;

.field final g:Lcom/google/android/apps/gmm/startpage/af;

.field public h:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public i:Lcom/google/o/b/a/v;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field j:Ljava/util/List;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/location/ActivityRecognitionResult;",
            ">;"
        }
    .end annotation
.end field

.field k:Lcom/google/o/h/a/en;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field l:Lcom/google/o/h/a/ed;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field m:Lcom/google/o/h/a/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field n:Z

.field o:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field t:I

.field u:I

.field v:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field w:Lcom/google/maps/g/a/al;

.field x:Landroid/content/res/Resources;

.field y:Lcom/google/android/apps/gmm/map/b/a/l;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field z:Lcom/google/android/apps/gmm/map/b/a/l;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 154
    const-class v0, Lcom/google/android/apps/gmm/startpage/aa;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/startpage/aa;->I:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/accounts/Account;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/startpage/af;Lcom/google/android/apps/gmm/startpage/d/e;Z)V
    .locals 2
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/o/h/a/dq;",
            ">;",
            "Lcom/google/android/apps/gmm/startpage/af;",
            "Lcom/google/android/apps/gmm/startpage/d/e;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 223
    sget-object v0, Lcom/google/r/b/a/el;->bG:Lcom/google/r/b/a/el;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/shared/net/e;-><init>(Lcom/google/r/b/a/el;)V

    .line 104
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/startpage/aa;->n:Z

    .line 106
    iput v1, p0, Lcom/google/android/apps/gmm/startpage/aa;->t:I

    .line 107
    iput v1, p0, Lcom/google/android/apps/gmm/startpage/aa;->u:I

    .line 146
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->D:Ljava/util/List;

    .line 151
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/startpage/aa;->G:Z

    .line 224
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/aa;->b:Landroid/accounts/Account;

    .line 225
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p2

    check-cast v0, Lcom/google/b/c/cv;

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    .line 226
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p3, Lcom/google/android/apps/gmm/startpage/af;

    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/aa;->g:Lcom/google/android/apps/gmm/startpage/af;

    .line 227
    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p4, Lcom/google/android/apps/gmm/startpage/d/e;

    iput-object p4, p0, Lcom/google/android/apps/gmm/startpage/aa;->A:Lcom/google/android/apps/gmm/startpage/d/e;

    .line 228
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/startpage/aa;->H:Z

    .line 229
    invoke-virtual {p2}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 230
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "must have at least one uiType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/google/b/c/cv;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 233
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "uiType must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235
    :cond_4
    return-void
.end method

.method public static a(Landroid/accounts/Account;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/startpage/af;Lcom/google/android/apps/gmm/startpage/d/e;Z)Lcom/google/android/apps/gmm/startpage/ac;
    .locals 7
    .param p0    # Landroid/accounts/Account;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/o/h/a/dq;",
            ">;",
            "Lcom/google/android/apps/gmm/startpage/af;",
            "Lcom/google/android/apps/gmm/startpage/d/e;",
            "Z)",
            "Lcom/google/android/apps/gmm/startpage/ac;"
        }
    .end annotation

    .prologue
    .line 243
    new-instance v6, Lcom/google/android/apps/gmm/startpage/ac;

    new-instance v0, Lcom/google/android/apps/gmm/startpage/aa;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/aa;-><init>(Landroid/accounts/Account;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/startpage/af;Lcom/google/android/apps/gmm/startpage/d/e;Z)V

    invoke-direct {v6, v0}, Lcom/google/android/apps/gmm/startpage/ac;-><init>(Lcom/google/android/apps/gmm/startpage/aa;)V

    return-object v6
.end method

.method public static a(ZLandroid/content/res/Resources;)Lcom/google/e/a/a/a/b;
    .locals 2
    .param p1    # Landroid/content/res/Resources;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 418
    .line 419
    invoke-static {p1}, Lcom/google/android/apps/gmm/startpage/aa;->a(Landroid/content/res/Resources;)Lcom/google/o/h/a/ce;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/h/a/ce;->g()Lcom/google/n/t;

    move-result-object v0

    sget-object v1, Lcom/google/o/h/a/a/b;->s:Lcom/google/e/a/a/a/d;

    .line 418
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/res/Resources;)Lcom/google/o/h/a/ce;
    .locals 8
    .param p0    # Landroid/content/res/Resources;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 386
    invoke-static {}, Lcom/google/o/h/a/bz;->newBuilder()Lcom/google/o/h/a/ce;

    move-result-object v1

    .line 389
    invoke-static {}, Lcom/google/android/apps/gmm/cardui/a;->a()V

    .line 391
    sget-object v0, Lcom/google/android/apps/gmm/cardui/f/a;->b:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/o/h/a/ce;->c()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bw;

    iget-object v3, v1, Lcom/google/o/h/a/ce;->c:Ljava/util/List;

    iget v0, v0, Lcom/google/o/h/a/bw;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 393
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/cardui/f/a;->a:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/o/h/a/ce;->d()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bu;

    iget-object v3, v1, Lcom/google/o/h/a/ce;->d:Ljava/util/List;

    iget v0, v0, Lcom/google/o/h/a/bu;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 396
    :cond_1
    sget-object v0, Lcom/google/o/h/a/ct;->b:Lcom/google/o/h/a/ct;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v2, v1, Lcom/google/o/h/a/ce;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/o/h/a/ce;->a:I

    iget v0, v0, Lcom/google/o/h/a/ct;->c:I

    iput v0, v1, Lcom/google/o/h/a/ce;->b:I

    .line 398
    sget-object v0, Lcom/google/android/apps/gmm/util/b/l;->a:Lcom/google/android/apps/gmm/util/b/ac;

    .line 400
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/util/b/ac;->a()Ljava/util/List;

    move-result-object v0

    .line 398
    invoke-virtual {v1}, Lcom/google/o/h/a/ce;->i()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    iget-object v3, v1, Lcom/google/o/h/a/ce;->e:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    invoke-direct {v4}, Lcom/google/n/ao;-><init>()V

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v4, Lcom/google/n/ao;->d:Z

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 403
    :cond_3
    sget-object v0, Lcom/google/o/h/a/cr;->a:Lcom/google/o/h/a/cr;

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/ce;->a(Lcom/google/o/h/a/cr;)Lcom/google/o/h/a/ce;

    .line 404
    sget-object v0, Lcom/google/o/h/a/cr;->b:Lcom/google/o/h/a/cr;

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/ce;->a(Lcom/google/o/h/a/cr;)Lcom/google/o/h/a/ce;

    .line 405
    sget-object v0, Lcom/google/o/h/a/cr;->c:Lcom/google/o/h/a/cr;

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/ce;->a(Lcom/google/o/h/a/cr;)Lcom/google/o/h/a/ce;

    .line 407
    if-nez p0, :cond_4

    invoke-static {}, Lcom/google/o/h/a/cn;->d()Lcom/google/o/h/a/cn;

    move-result-object v0

    .line 408
    :goto_3
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 407
    :cond_4
    invoke-static {}, Lcom/google/o/h/a/cn;->newBuilder()Lcom/google/o/h/a/cp;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/gmm/cardui/e/d;->a(Landroid/content/res/Resources;)I

    move-result v2

    iget v3, v0, Lcom/google/o/h/a/cp;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v0, Lcom/google/o/h/a/cp;->a:I

    iput v2, v0, Lcom/google/o/h/a/cp;->b:I

    invoke-static {p0}, Lcom/google/android/apps/gmm/cardui/e/d;->b(Landroid/content/res/Resources;)I

    move-result v2

    iget v3, v0, Lcom/google/o/h/a/cp;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v0, Lcom/google/o/h/a/cp;->a:I

    iput v2, v0, Lcom/google/o/h/a/cp;->c:I

    invoke-virtual {v0}, Lcom/google/o/h/a/cp;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/cn;

    goto :goto_3

    .line 408
    :cond_5
    iget-object v2, v1, Lcom/google/o/h/a/ce;->g:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/ce;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, v1, Lcom/google/o/h/a/ce;->a:I

    .line 409
    iget v0, v1, Lcom/google/o/h/a/ce;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, v1, Lcom/google/o/h/a/ce;->a:I

    iput-boolean v6, v1, Lcom/google/o/h/a/ce;->f:Z

    .line 411
    return-object v1
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/b/a/r;D)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 701
    if-ne p0, p1, :cond_1

    .line 709
    :cond_0
    :goto_0
    return v0

    .line 704
    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    move v0, v1

    .line 705
    goto :goto_0

    .line 707
    :cond_3
    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/r;->a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/b/a/r;)D

    move-result-wide v2

    .line 708
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/r;->c()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/r;->c()D

    move-result-wide v6

    add-double/2addr v4, v6

    sub-double/2addr v4, v2

    .line 709
    div-double/2addr v2, v4

    cmpl-double v2, v2, p2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/google/o/b/a/v;Lcom/google/o/b/a/v;D)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/16 v5, 0x800

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 724
    if-ne p0, p1, :cond_0

    move v0, v3

    .line 739
    :goto_0
    return v0

    .line 727
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    :cond_1
    move v0, v4

    .line 728
    goto :goto_0

    .line 732
    :cond_2
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x800

    if-ne v0, v5, :cond_5

    move v0, v3

    :goto_1
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/o/b/a/v;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/h;->d()Lcom/google/o/b/a/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/h;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/o/b/a/h;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    move-object v1, v0

    :goto_2
    iget v0, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x800

    if-ne v0, v5, :cond_7

    move v0, v3

    :goto_3
    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/o/b/a/v;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/h;->d()Lcom/google/o/b/a/h;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/h;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/o/b/a/h;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v2

    :cond_3
    if-eq v1, v2, :cond_4

    if-eqz v1, :cond_8

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_4
    move v0, v3

    :goto_4
    if-nez v0, :cond_9

    move v0, v4

    .line 733
    goto :goto_0

    :cond_5
    move v0, v4

    .line 732
    goto :goto_1

    :cond_6
    move-object v1, v2

    goto :goto_2

    :cond_7
    move v0, v4

    goto :goto_3

    :cond_8
    move v0, v4

    goto :goto_4

    .line 737
    :cond_9
    iget-object v0, p0, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/l;->d()Lcom/google/o/b/a/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/o/b/a/l;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    .line 738
    iget-object v0, p1, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/l;->d()Lcom/google/o/b/a/l;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/o/b/a/l;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    .line 736
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/p;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)D

    move-result-wide v0

    .line 739
    cmpg-double v0, v0, p2

    if-gez v0, :cond_a

    move v0, v3

    goto :goto_0

    :cond_a
    move v0, v4

    goto :goto_0
.end method

.method private h()Lcom/google/o/h/a/sp;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 424
    invoke-static {}, Lcom/google/o/h/a/sm;->newBuilder()Lcom/google/o/h/a/sp;

    move-result-object v0

    sget-object v1, Lcom/google/o/h/a/sq;->a:Lcom/google/o/h/a/sq;

    .line 425
    invoke-virtual {v0, v1}, Lcom/google/o/h/a/sp;->a(Lcom/google/o/h/a/sq;)Lcom/google/o/h/a/sp;

    move-result-object v0

    sget-object v1, Lcom/google/o/h/a/sq;->b:Lcom/google/o/h/a/sq;

    .line 426
    invoke-virtual {v0, v1}, Lcom/google/o/h/a/sp;->a(Lcom/google/o/h/a/sq;)Lcom/google/o/h/a/sp;

    move-result-object v0

    sget-object v1, Lcom/google/o/h/a/sq;->c:Lcom/google/o/h/a/sq;

    .line 427
    invoke-virtual {v0, v1}, Lcom/google/o/h/a/sp;->a(Lcom/google/o/h/a/sq;)Lcom/google/o/h/a/sp;

    move-result-object v0

    sget-object v1, Lcom/google/o/h/a/sq;->f:Lcom/google/o/h/a/sq;

    .line 428
    invoke-virtual {v0, v1}, Lcom/google/o/h/a/sp;->a(Lcom/google/o/h/a/sq;)Lcom/google/o/h/a/sp;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/startpage/aa;->H:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/aa;->x:Landroid/content/res/Resources;

    .line 429
    invoke-static {v1}, Lcom/google/android/apps/gmm/startpage/aa;->a(Landroid/content/res/Resources;)Lcom/google/o/h/a/ce;

    move-result-object v1

    iget-object v2, v0, Lcom/google/o/h/a/sp;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/o/h/a/ce;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit16 v1, v1, 0x2000

    iput v1, v0, Lcom/google/o/h/a/sp;->a:I

    .line 430
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/aa;->m:Lcom/google/o/h/a/d;

    if-eqz v1, :cond_1

    .line 434
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/aa;->m:Lcom/google/o/h/a/d;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v2, v0, Lcom/google/o/h/a/sp;->e:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit16 v1, v1, 0x4000

    iput v1, v0, Lcom/google/o/h/a/sp;->a:I

    .line 436
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/aa;->l:Lcom/google/o/h/a/ed;

    if-eqz v1, :cond_3

    .line 437
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/aa;->l:Lcom/google/o/h/a/ed;

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v2, v0, Lcom/google/o/h/a/sp;->b:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/o/h/a/sp;->a:I

    .line 439
    :cond_3
    const/high16 v1, 0x4020000

    iget v2, v0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/o/h/a/sp;->a:I

    iput v1, v0, Lcom/google/o/h/a/sp;->c:I

    .line 440
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/aa;->w:Lcom/google/maps/g/a/al;

    if-eqz v1, :cond_4

    .line 441
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/aa;->w:Lcom/google/maps/g/a/al;

    invoke-virtual {v0, v1}, Lcom/google/o/h/a/sp;->a(Lcom/google/maps/g/a/al;)Lcom/google/o/h/a/sp;

    .line 443
    :cond_4
    return-object v0
.end method

.method private o()Lcom/google/o/h/a/dh;
    .locals 4

    .prologue
    .line 498
    invoke-static {}, Lcom/google/o/h/a/de;->newBuilder()Lcom/google/o/h/a/dh;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    .line 499
    invoke-virtual {v1}, Lcom/google/o/h/a/dh;->c()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/dq;

    iget-object v3, v1, Lcom/google/o/h/a/dh;->b:Ljava/util/List;

    iget v0, v0, Lcom/google/o/h/a/dq;->s:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->g:Lcom/google/android/apps/gmm/startpage/af;

    .line 500
    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/af;->e:Lcom/google/o/h/a/di;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v2, v1, Lcom/google/o/h/a/dh;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/o/h/a/dh;->a:I

    iget v0, v0, Lcom/google/o/h/a/di;->e:I

    iput v0, v1, Lcom/google/o/h/a/dh;->c:I

    .line 502
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->h:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 503
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->h:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v2, v1, Lcom/google/o/h/a/dh;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v1, Lcom/google/o/h/a/dh;->a:I

    iput-object v0, v1, Lcom/google/o/h/a/dh;->d:Ljava/lang/Object;

    .line 505
    :cond_3
    return-object v1
.end method


# virtual methods
.method protected final L_()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/xt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 331
    sget-object v0, Lcom/google/r/b/a/xt;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method protected final M_()J
    .locals 2

    .prologue
    .line 313
    const-wide/16 v0, 0x2710

    return-wide v0
.end method

.method protected final S_()Lcom/google/b/a/ak;
    .locals 5

    .prologue
    .line 595
    invoke-super {p0}, Lcom/google/android/apps/gmm/shared/net/e;->S_()Lcom/google/b/a/ak;

    move-result-object v1

    const-string v0, "viewport"

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/aa;->a:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 596
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "uiTypes"

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    .line 597
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "fetchType"

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/aa;->g:Lcom/google/android/apps/gmm/startpage/af;

    .line 598
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "myLocation"

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    .line 599
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "directionContext"

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/aa;->k:Lcom/google/o/h/a/en;

    .line 600
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "listener"

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/aa;->E:Lcom/google/android/apps/gmm/startpage/ae;

    .line 601
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    return-object v1
.end method

.method protected final synthetic a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 3

    .prologue
    .line 78
    check-cast p1, Lcom/google/r/b/a/xt;

    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/aa;->F:Lcom/google/r/b/a/xt;

    sget-object v1, Lcom/google/android/apps/gmm/startpage/ab;->a:[I

    iget-object v0, p1, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mj;->h()Lcom/google/o/h/a/mj;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mj;

    iget v0, v0, Lcom/google/o/h/a/mj;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/mo;->a(I)Lcom/google/o/h/a/mo;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    :cond_0
    invoke-virtual {v0}, Lcom/google/o/h/a/mo;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/gmm/startpage/aa;->I:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mj;->h()Lcom/google/o/h/a/mj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mj;

    iget v0, v0, Lcom/google/o/h/a/mj;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/mo;->a(I)Lcom/google/o/h/a/mo;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    :cond_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Error code is returned:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->l:Lcom/google/android/apps/gmm/shared/net/k;

    :goto_0
    return-object v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->i:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 322
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->g:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->i:Lcom/google/android/apps/gmm/shared/net/k;

    if-ne p1, v0, :cond_1

    .line 324
    :cond_0
    const/4 v0, 0x0

    .line 326
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/shared/net/e;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final al_()Z
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->b:Landroid/accounts/Account;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    sget-object v1, Lcom/google/o/h/a/dq;->m:Lcom/google/o/h/a/dq;

    .line 298
    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    sget-object v1, Lcom/google/o/h/a/dq;->l:Lcom/google/o/h/a/dq;

    .line 299
    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    sget-object v1, Lcom/google/o/h/a/dq;->j:Lcom/google/o/h/a/dq;

    .line 300
    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final synthetic b()Lcom/google/n/at;
    .locals 12

    .prologue
    .line 78
    invoke-static {}, Lcom/google/o/h/a/mf;->newBuilder()Lcom/google/o/h/a/mh;

    move-result-object v1

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {}, Lcom/google/o/h/a/rg;->newBuilder()Lcom/google/o/h/a/ri;

    move-result-object v4

    invoke-static {}, Lcom/google/o/h/a/rk;->newBuilder()Lcom/google/o/h/a/rm;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, v2

    iget v8, v5, Lcom/google/o/h/a/rm;->a:I

    or-int/lit8 v8, v8, 0x1

    iput v8, v5, Lcom/google/o/h/a/rm;->a:I

    iput-wide v6, v5, Lcom/google/o/h/a/rm;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    iget v2, v5, Lcom/google/o/h/a/rm;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v5, Lcom/google/o/h/a/rm;->a:I

    iput v0, v5, Lcom/google/o/h/a/rm;->c:I

    iget-object v0, v4, Lcom/google/o/h/a/ri;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/o/h/a/rm;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    iget v0, v4, Lcom/google/o/h/a/ri;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v4, Lcom/google/o/h/a/ri;->a:I

    iget-object v0, v1, Lcom/google/o/h/a/mh;->c:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/o/h/a/ri;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/mh;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v1, Lcom/google/o/h/a/mh;->a:I

    invoke-static {}, Lcom/google/o/h/a/kz;->newBuilder()Lcom/google/o/h/a/lb;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->a:Lcom/google/android/apps/gmm/map/b/a/r;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/d/a/a/hp;->newBuilder()Lcom/google/d/a/a/hr;

    move-result-object v0

    invoke-static {}, Lcom/google/d/a/a/hp;->newBuilder()Lcom/google/d/a/a/hr;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/startpage/aa;->a:Lcom/google/android/apps/gmm/map/b/a/r;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    const-wide v6, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v4, v6

    double-to-int v4, v4

    iget v5, v0, Lcom/google/d/a/a/hr;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v0, Lcom/google/d/a/a/hr;->a:I

    iput v4, v0, Lcom/google/d/a/a/hr;->b:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/startpage/aa;->a:Lcom/google/android/apps/gmm/map/b/a/r;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    const-wide v6, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v4, v6

    double-to-int v4, v4

    iget v5, v0, Lcom/google/d/a/a/hr;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v0, Lcom/google/d/a/a/hr;->a:I

    iput v4, v0, Lcom/google/d/a/a/hr;->c:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/startpage/aa;->a:Lcom/google/android/apps/gmm/map/b/a/r;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    const-wide v6, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v4, v6

    double-to-int v4, v4

    iget v5, v3, Lcom/google/d/a/a/hr;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v3, Lcom/google/d/a/a/hr;->a:I

    iput v4, v3, Lcom/google/d/a/a/hr;->b:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/startpage/aa;->a:Lcom/google/android/apps/gmm/map/b/a/r;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    const-wide v6, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v4, v6

    double-to-int v4, v4

    iget v5, v3, Lcom/google/d/a/a/hr;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v3, Lcom/google/d/a/a/hr;->a:I

    iput v4, v3, Lcom/google/d/a/a/hr;->c:I

    invoke-static {}, Lcom/google/d/a/a/ju;->newBuilder()Lcom/google/d/a/a/jw;

    move-result-object v4

    iget-object v5, v4, Lcom/google/d/a/a/jw;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/d/a/a/hr;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v5, Lcom/google/n/ao;->d:Z

    iget v0, v4, Lcom/google/d/a/a/jw;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v4, Lcom/google/d/a/a/jw;->a:I

    iget-object v0, v4, Lcom/google/d/a/a/jw;->c:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/d/a/a/hr;->g()Lcom/google/n/t;

    move-result-object v3

    iget-object v5, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v3, 0x0

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    iget v0, v4, Lcom/google/d/a/a/jw;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v4, Lcom/google/d/a/a/jw;->a:I

    iget-object v0, v2, Lcom/google/o/h/a/lb;->b:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/d/a/a/jw;->g()Lcom/google/n/t;

    move-result-object v3

    iget-object v4, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v3, 0x0

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/o/h/a/lb;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/o/h/a/lb;->a:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    iget v0, v0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    iget-object v0, v0, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/l;->d()Lcom/google/o/b/a/l;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/o/b/a/l;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_4

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->c()Lcom/google/d/a/a/hp;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v3, v2, Lcom/google/o/h/a/lb;->c:Lcom/google/n/ao;

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/o/h/a/lb;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v2, Lcom/google/o/h/a/lb;->a:I

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    iget v0, v0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_6

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    iget v0, v0, Lcom/google/o/b/a/v;->i:F

    float-to-double v4, v0

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    double-to-int v0, v4

    const v3, 0x1869f

    if-eq v0, v3, :cond_5

    iget v3, v2, Lcom/google/o/h/a/lb;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, v2, Lcom/google/o/h/a/lb;->a:I

    iput v0, v2, Lcom/google/o/h/a/lb;->d:I

    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->j:Ljava/util/List;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-static {}, Lcom/google/o/h/a/lc;->newBuilder()Lcom/google/o/h/a/le;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v7

    iget v8, v6, Lcom/google/o/h/a/le;->a:I

    or-int/lit8 v8, v8, 0x2

    iput v8, v6, Lcom/google/o/h/a/le;->a:I

    iput v7, v6, Lcom/google/o/h/a/le;->b:I

    const/4 v7, 0x2

    invoke-virtual {v0, v7}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v7

    iget v8, v6, Lcom/google/o/h/a/le;->a:I

    or-int/lit8 v8, v8, 0x4

    iput v8, v6, Lcom/google/o/h/a/le;->a:I

    iput v7, v6, Lcom/google/o/h/a/le;->c:I

    const/4 v7, 0x3

    invoke-virtual {v0, v7}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v7

    iget v8, v6, Lcom/google/o/h/a/le;->a:I

    or-int/lit8 v8, v8, 0x8

    iput v8, v6, Lcom/google/o/h/a/le;->a:I

    iput v7, v6, Lcom/google/o/h/a/le;->d:I

    const/4 v7, 0x5

    invoke-virtual {v0, v7}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v7

    iget v8, v6, Lcom/google/o/h/a/le;->a:I

    or-int/lit8 v8, v8, 0x10

    iput v8, v6, Lcom/google/o/h/a/le;->a:I

    iput v7, v6, Lcom/google/o/h/a/le;->e:I

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b()J

    move-result-wide v8

    sub-long v8, v4, v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    long-to-int v0, v8

    iget v7, v6, Lcom/google/o/h/a/le;->a:I

    or-int/lit8 v7, v7, 0x20

    iput v7, v6, Lcom/google/o/h/a/le;->a:I

    iput v0, v6, Lcom/google/o/h/a/le;->f:I

    invoke-virtual {v2}, Lcom/google/o/h/a/lb;->c()V

    iget-object v0, v2, Lcom/google/o/h/a/lb;->e:Ljava/util/List;

    invoke-virtual {v6}, Lcom/google/o/h/a/le;->g()Lcom/google/n/t;

    move-result-object v6

    new-instance v7, Lcom/google/n/ao;

    invoke-direct {v7}, Lcom/google/n/ao;-><init>()V

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v6, 0x0

    iput-object v6, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v6, 0x1

    iput-boolean v6, v7, Lcom/google/n/ao;->d:Z

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_7
    iget-object v0, v1, Lcom/google/o/h/a/mh;->f:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/o/h/a/lb;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/mh;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v1, Lcom/google/o/h/a/mh;->a:I

    invoke-direct {p0}, Lcom/google/android/apps/gmm/startpage/aa;->o()Lcom/google/o/h/a/dh;

    move-result-object v0

    iget-object v2, v1, Lcom/google/o/h/a/mh;->g:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/h/a/dh;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/mh;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v1, Lcom/google/o/h/a/mh;->a:I

    invoke-static {}, Lcom/google/o/h/a/id;->newBuilder()Lcom/google/o/h/a/if;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/aa;->y:Lcom/google/android/apps/gmm/map/b/a/l;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/aa;->y:Lcom/google/android/apps/gmm/map/b/a/l;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget v4, v0, Lcom/google/o/h/a/if;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/o/h/a/if;->a:I

    iput-wide v2, v0, Lcom/google/o/h/a/if;->b:J

    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/aa;->z:Lcom/google/android/apps/gmm/map/b/a/l;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/aa;->z:Lcom/google/android/apps/gmm/map/b/a/l;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget v4, v0, Lcom/google/o/h/a/if;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v0, Lcom/google/o/h/a/if;->a:I

    iput-wide v2, v0, Lcom/google/o/h/a/if;->c:J

    :cond_9
    iget-object v2, v1, Lcom/google/o/h/a/mh;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/h/a/if;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/mh;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, v1, Lcom/google/o/h/a/mh;->a:I

    invoke-direct {p0}, Lcom/google/android/apps/gmm/startpage/aa;->h()Lcom/google/o/h/a/sp;

    move-result-object v0

    iget-object v2, v1, Lcom/google/o/h/a/mh;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/h/a/sp;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/mh;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/o/h/a/mh;->a:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->k:Lcom/google/o/h/a/en;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->k:Lcom/google/o/h/a/en;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    iget-object v2, v1, Lcom/google/o/h/a/mh;->h:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/mh;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, v1, Lcom/google/o/h/a/mh;->a:I

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->A:Lcom/google/android/apps/gmm/startpage/d/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/d/e;->b:Ljava/lang/String;

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_4
    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->A:Lcom/google/android/apps/gmm/startpage/d/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/d/e;->b:Ljava/lang/String;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_4

    :cond_d
    iget v2, v1, Lcom/google/o/h/a/mh;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, v1, Lcom/google/o/h/a/mh;->a:I

    iput-object v0, v1, Lcom/google/o/h/a/mh;->j:Ljava/lang/Object;

    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->B:Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->B:Ljava/lang/String;

    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    iget v2, v1, Lcom/google/o/h/a/mh;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, v1, Lcom/google/o/h/a/mh;->a:I

    iput-object v0, v1, Lcom/google/o/h/a/mh;->k:Ljava/lang/Object;

    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->C:Ljava/lang/String;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->C:Ljava/lang/String;

    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_11
    iget v2, v1, Lcom/google/o/h/a/mh;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, v1, Lcom/google/o/h/a/mh;->a:I

    iput-object v0, v1, Lcom/google/o/h/a/mh;->l:Ljava/lang/Object;

    :cond_12
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->n:Z

    if-eqz v0, :cond_14

    invoke-static {}, Lcom/google/o/h/a/ot;->d()Lcom/google/o/h/a/ot;

    move-result-object v0

    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_13
    iget-object v2, v1, Lcom/google/o/h/a/mh;->m:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/mh;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, v1, Lcom/google/o/h/a/mh;->a:I

    :cond_14
    invoke-static {}, Lcom/google/r/b/a/xo;->newBuilder()Lcom/google/r/b/a/xr;

    move-result-object v0

    iget-object v2, v0, Lcom/google/r/b/a/xr;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/o/h/a/mh;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/r/b/a/xr;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/r/b/a/xr;->a:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/aa;->D:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/r/b/a/xr;->a(Ljava/lang/Iterable;)Lcom/google/r/b/a/xr;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->o:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->o:Lcom/google/e/a/a/a/b;

    invoke-static {}, Lcom/google/r/b/a/adi;->d()Lcom/google/r/b/a/adi;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_15

    :goto_5
    check-cast v0, Lcom/google/r/b/a/adi;

    if-nez v0, :cond_16

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_15
    move-object v0, v1

    goto :goto_5

    :cond_16
    iget-object v1, v2, Lcom/google/r/b/a/xr;->c:Lcom/google/n/ao;

    iget-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/r/b/a/xr;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v2, Lcom/google/r/b/a/xr;->a:I

    :cond_17
    iget v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->t:I

    if-lez v0, :cond_1a

    iget v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->t:I

    invoke-static {v0}, Lcom/google/android/apps/gmm/search/bc;->a(I)Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-static {}, Lcom/google/maps/a/m;->d()Lcom/google/maps/a/m;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_18

    :goto_6
    check-cast v0, Lcom/google/maps/a/m;

    if-nez v0, :cond_19

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_18
    move-object v0, v1

    goto :goto_6

    :cond_19
    iget-object v1, v2, Lcom/google/r/b/a/xr;->d:Lcom/google/n/ao;

    iget-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/r/b/a/xr;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v2, Lcom/google/r/b/a/xr;->a:I

    :cond_1a
    iget v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->u:I

    if-lez v0, :cond_1d

    iget v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->u:I

    invoke-static {v0}, Lcom/google/android/apps/gmm/search/bc;->a(I)Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-static {}, Lcom/google/maps/a/m;->d()Lcom/google/maps/a/m;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_1b

    :goto_7
    check-cast v0, Lcom/google/maps/a/m;

    if-nez v0, :cond_1c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1b
    move-object v0, v1

    goto :goto_7

    :cond_1c
    iget-object v1, v2, Lcom/google/r/b/a/xr;->e:Lcom/google/n/ao;

    iget-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/r/b/a/xr;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, v2, Lcom/google/r/b/a/xr;->a:I

    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->v:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_20

    invoke-static {}, Lcom/google/maps/g/lh;->newBuilder()Lcom/google/maps/g/lj;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->v:Lcom/google/e/a/a/a/b;

    invoke-static {}, Lcom/google/maps/g/gn;->g()Lcom/google/maps/g/gn;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_1e

    :goto_8
    check-cast v0, Lcom/google/maps/g/gn;

    if-nez v0, :cond_1f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1e
    move-object v0, v1

    goto :goto_8

    :cond_1f
    iget-object v1, v3, Lcom/google/maps/g/lj;->b:Lcom/google/n/ao;

    iget-object v4, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, v3, Lcom/google/maps/g/lj;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v3, Lcom/google/maps/g/lj;->a:I

    iget-object v0, v2, Lcom/google/r/b/a/xr;->f:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/maps/g/lj;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/r/b/a/xr;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, v2, Lcom/google/r/b/a/xr;->a:I

    :cond_20
    invoke-static {}, Lcom/google/maps/g/hy;->newBuilder()Lcom/google/maps/g/ia;

    move-result-object v0

    const/4 v1, 0x1

    iget v3, v0, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, v0, Lcom/google/maps/g/ia;->a:I

    iput-boolean v1, v0, Lcom/google/maps/g/ia;->f:Z

    iget-object v1, v2, Lcom/google/r/b/a/xr;->g:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/r/b/a/xr;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, v2, Lcom/google/r/b/a/xr;->a:I

    invoke-virtual {v2}, Lcom/google/r/b/a/xr;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/xo;

    return-object v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 305
    invoke-super {p0}, Lcom/google/android/apps/gmm/shared/net/e;->f()V

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->E:Lcom/google/android/apps/gmm/startpage/ae;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->E:Lcom/google/android/apps/gmm/startpage/ae;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/startpage/ae;->a(Lcom/google/android/apps/gmm/startpage/aa;)V

    .line 309
    :cond_0
    return-void
.end method

.method public final declared-synchronized g()Z
    .locals 1

    .prologue
    .line 317
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return v0
.end method

.method protected final j()Landroid/accounts/Account;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->b:Landroid/accounts/Account;

    return-object v0
.end method

.method protected declared-synchronized onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 291
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/aa;->E:Lcom/google/android/apps/gmm/startpage/ae;

    invoke-interface {v0, p0, p1}, Lcom/google/android/apps/gmm/startpage/ae;->a(Lcom/google/android/apps/gmm/startpage/aa;Lcom/google/android/apps/gmm/shared/net/k;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    monitor-exit p0

    return-void

    .line 291
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
