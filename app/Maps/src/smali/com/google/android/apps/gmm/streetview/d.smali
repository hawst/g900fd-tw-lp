.class Lcom/google/android/apps/gmm/streetview/d;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/d;->a:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/d;->a:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;->b:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    .line 243
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->m:Z

    if-eqz v0, :cond_1

    .line 244
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/d;->a:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;

    iget-object v3, v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;->b:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/d;->a:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;->b:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->a:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b(Z)V

    .line 248
    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 245
    goto :goto_0

    :cond_1
    move v1, v2

    .line 248
    goto :goto_1
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/d;->a:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;->b:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    if-nez v0, :cond_0

    .line 237
    :goto_0
    return v2

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/d;->a:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;->b:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->a()Z

    move-result v3

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/d;->a:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;->b:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->a(Z)V

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/d;->a:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;->b:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->f:Lcom/google/android/apps/gmm/util/h;

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/d;->a:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;->b:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;

    .line 232
    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->a:Z

    if-nez v0, :cond_1

    move v0, v1

    .line 231
    :goto_1
    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/util/h;->a(Z)V

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/d;->a:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;->b:Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;

    sget-object v5, Lcom/google/b/f/t;->eY:Lcom/google/b/f/t;

    new-instance v6, Lcom/google/android/apps/gmm/z/b/n;

    if-eqz v3, :cond_2

    sget-object v0, Lcom/google/r/b/a/a;->b:Lcom/google/r/b/a/a;

    :goto_2
    invoke-direct {v6, v0}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    new-array v0, v1, [Lcom/google/b/f/cq;

    sget-object v3, Lcom/google/b/f/t;->eX:Lcom/google/b/f/t;

    aput-object v3, v0, v2

    invoke-virtual {v4, v5, v6, v0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->a(Lcom/google/b/f/cq;Lcom/google/android/apps/gmm/z/b/n;[Lcom/google/b/f/cq;)V

    move v2, v1

    .line 237
    goto :goto_0

    :cond_1
    move v0, v2

    .line 232
    goto :goto_1

    .line 234
    :cond_2
    sget-object v0, Lcom/google/r/b/a/a;->c:Lcom/google/r/b/a/a;

    goto :goto_2
.end method
