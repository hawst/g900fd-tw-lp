.class public abstract Lcom/google/android/apps/gmm/shared/net/e;
.super Lcom/google/android/apps/gmm/shared/net/i;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q::",
        "Lcom/google/n/at;",
        "S::",
        "Lcom/google/n/at;",
        ">",
        "Lcom/google/android/apps/gmm/shared/net/i;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/r/b/a/el;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/shared/net/i;-><init>(Lcom/google/r/b/a/el;)V

    .line 28
    return-void
.end method


# virtual methods
.method public abstract L_()Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<TS;>;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)",
            "Lcom/google/android/apps/gmm/shared/net/k;"
        }
    .end annotation
.end method

.method public final a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 4

    .prologue
    .line 54
    invoke-static {p1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Ljava/io/DataInput;)Ljava/io/InputStream;

    move-result-object v1

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/e;->L_()Lcom/google/n/ax;

    move-result-object v0

    .line 58
    :try_start_0
    invoke-interface {v0, v1}, Lcom/google/n/ax;->b(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    .line 59
    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 60
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->j:Lcom/google/android/apps/gmm/shared/net/k;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 68
    :goto_0
    return-object v0

    .line 66
    :cond_0
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 68
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/shared/net/e;->a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    :try_start_1
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->j:Lcom/google/android/apps/gmm/shared/net/k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 66
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method public final a(Ljava/io/DataOutput;)V
    .locals 2

    .prologue
    .line 32
    instance-of v0, p1, Ljava/io/OutputStream;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 33
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/e;->b()Lcom/google/n/at;

    move-result-object v0

    .line 34
    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeInt(I)V

    .line 35
    check-cast p1, Ljava/io/OutputStream;

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Ljava/io/OutputStream;)V

    .line 36
    return-void
.end method

.method public abstract b()Lcom/google/n/at;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TQ;"
        }
    .end annotation
.end method
