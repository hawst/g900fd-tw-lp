.class public Lcom/google/android/apps/gmm/startpage/f/s;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/startpage/e/k;


# instance fields
.field private a:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/f/s;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 18
    return-void
.end method

.method private a(Lcom/google/b/f/t;)Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/s;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/s;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    .line 34
    invoke-static {p1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 33
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/s;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->u()Lcom/google/android/apps/gmm/prefetchcache/api/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/a;->c()V

    .line 37
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/b/f/t;->ck:Lcom/google/b/f/t;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/startpage/f/s;->a(Lcom/google/b/f/t;)Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/b/f/t;->fp:Lcom/google/b/f/t;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/startpage/f/s;->a(Lcom/google/b/f/t;)Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    return-object v0
.end method
