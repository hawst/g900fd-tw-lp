.class public Lcom/google/android/apps/gmm/navigation/navui/d/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/navui/d/p;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/google/android/apps/gmm/base/a;

.field final c:Lcom/google/android/apps/gmm/navigation/navui/d/o;

.field final d:Landroid/content/res/Resources;

.field final e:Landroid/content/Context;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field public h:Lcom/google/android/apps/gmm/navigation/g/b/k;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public i:Lcom/google/android/apps/gmm/navigation/g/b/k;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field j:I

.field k:I

.field l:Lcom/google/android/apps/gmm/directions/f/a/b;

.field public m:Z

.field n:Z

.field o:Z

.field public final p:Ljava/lang/Object;

.field private final q:Z

.field private final r:Ljava/lang/String;

.field private final s:Ljava/lang/String;

.field private final t:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/navigation/navui/d/o;Z)V
    .locals 2

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/d/n;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/d/n;-><init>(Lcom/google/android/apps/gmm/navigation/navui/d/m;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->p:Ljava/lang/Object;

    .line 118
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->b:Lcom/google/android/apps/gmm/base/a;

    .line 119
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/navigation/navui/d/o;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->c:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    .line 120
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->q:Z

    .line 121
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->d:Landroid/content/res/Resources;

    .line 122
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->e:Landroid/content/Context;

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->d:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/l;->ms:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->r:Ljava/lang/String;

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->d:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/l;->mr:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->s:Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->d:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/l;->mt:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->t:Ljava/lang/String;

    .line 126
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/google/b/f/b/a/ah;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 226
    sget-object v0, Lcom/google/b/f/b/a/ah;->d:Lcom/google/b/f/b/a/ah;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/b/f/b/a/ah;->c:Lcom/google/b/f/b/a/ah;

    if-ne p1, v0, :cond_1

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    new-array v1, v1, [Lcom/google/android/apps/gmm/z/b/a;

    new-instance v2, Lcom/google/android/apps/gmm/z/b;

    iget v3, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->j:I

    .line 229
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->k:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v5

    invoke-direct {v2, p1, v3, v4, v5}, Lcom/google/android/apps/gmm/z/b;-><init>(Lcom/google/b/f/b/a/ah;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/apps/gmm/shared/c/f;)V

    aput-object v2, v1, v6

    .line 228
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    .line 234
    :goto_0
    return-void

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    new-array v1, v1, [Lcom/google/android/apps/gmm/z/b/a;

    new-instance v2, Lcom/google/android/apps/gmm/z/b;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->b:Lcom/google/android/apps/gmm/base/a;

    .line 232
    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/google/android/apps/gmm/z/b;-><init>(Lcom/google/b/f/b/a/ah;Lcom/google/android/apps/gmm/shared/c/f;)V

    aput-object v2, v1, v6

    .line 231
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 155
    sget-object v0, Lcom/google/b/f/b/a/ah;->d:Lcom/google/b/f/b/a/ah;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a(Lcom/google/b/f/b/a/ah;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->c:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/o;->a()V

    .line 156
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 171
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    if-nez v0, :cond_1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    sget-object v0, Lcom/google/b/f/b/a/ah;->c:Lcom/google/b/f/b/a/ah;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a(Lcom/google/b/f/b/a/ah;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v0, v0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ao;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v3

    sget-object v4, Lcom/google/r/b/a/tf;->b:Lcom/google/r/b/a/tf;

    invoke-interface {v3, v4, v0}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/r/b/a/tf;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/gmm/navigation/j/a/e;

    invoke-direct {v3, v2, v1}, Lcom/google/android/apps/gmm/navigation/j/a/e;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;Z)V

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->c:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/o;->a()V

    .line 172
    :cond_1
    const/4 v0, 0x0

    return-object v0

    .line 171
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->n:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->q:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
