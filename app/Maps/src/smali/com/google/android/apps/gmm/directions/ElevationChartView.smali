.class public Lcom/google/android/apps/gmm/directions/ElevationChartView;
.super Landroid/view/View;
.source "PG"


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field private final e:Lcom/google/android/apps/gmm/directions/bb;

.field private final f:Lcom/google/android/apps/gmm/directions/b;

.field private final g:Lcom/google/android/apps/gmm/directions/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/gmm/directions/ElevationChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/directions/ElevationChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/ElevationChartView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 60
    sget v1, Lcom/google/android/apps/gmm/d;->i:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 61
    sget v2, Lcom/google/android/apps/gmm/d;->k:I

    .line 62
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget v3, Lcom/google/android/apps/gmm/e;->U:I

    .line 63
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 61
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iput-object v4, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->a:Landroid/graphics/Paint;

    .line 64
    sget v2, Lcom/google/android/apps/gmm/d;->z:I

    .line 65
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget v3, Lcom/google/android/apps/gmm/e;->U:I

    .line 66
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 64
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 67
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->b:Landroid/graphics/Paint;

    .line 68
    sget v1, Lcom/google/android/apps/gmm/d;->h:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->c:Landroid/graphics/Paint;

    .line 69
    sget v1, Lcom/google/android/apps/gmm/d;->z:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->d:Landroid/graphics/Paint;

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->d:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/gmm/e;->bA:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/directions/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->f:Lcom/google/android/apps/gmm/directions/b;

    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/directions/bd;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/bd;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->g:Lcom/google/android/apps/gmm/directions/b;

    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/directions/bb;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->f:Lcom/google/android/apps/gmm/directions/b;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->g:Lcom/google/android/apps/gmm/directions/b;

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->b:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->a:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->d:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->c:Landroid/graphics/Paint;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/bb;-><init>(Lcom/google/android/apps/gmm/directions/b;Lcom/google/android/apps/gmm/directions/b;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->e:Lcom/google/android/apps/gmm/directions/bb;

    .line 75
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    .line 76
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/ElevationChartView;->setWillNotDraw(Z)V

    .line 77
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 150
    return-void
.end method

.method public onRtlPropertiesChanged(I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 158
    invoke-super {p0, p1}, Landroid/view/View;->onRtlPropertiesChanged(I)V

    .line 159
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->e:Lcom/google/android/apps/gmm/directions/bb;

    if-ne p1, v0, :cond_0

    :goto_0
    iput-boolean v0, v1, Lcom/google/android/apps/gmm/directions/bb;->p:Z

    .line 161
    return-void

    .line 159
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 134
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->e:Lcom/google/android/apps/gmm/directions/bb;

    invoke-virtual {v0, v1, v1, p1, p2}, Lcom/google/android/apps/gmm/directions/bb;->setBounds(IIII)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/ElevationChartView;->e:Lcom/google/android/apps/gmm/directions/bb;

    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/ElevationChartView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/ElevationChartView;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/ElevationChartView;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/ElevationChartView;->getPaddingBottom()I

    move-result v4

    .line 136
    iput v1, v0, Lcom/google/android/apps/gmm/directions/bb;->j:I

    iput v2, v0, Lcom/google/android/apps/gmm/directions/bb;->k:I

    iput v3, v0, Lcom/google/android/apps/gmm/directions/bb;->l:I

    iput v4, v0, Lcom/google/android/apps/gmm/directions/bb;->m:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/bb;->a()V

    .line 139
    return-void
.end method
