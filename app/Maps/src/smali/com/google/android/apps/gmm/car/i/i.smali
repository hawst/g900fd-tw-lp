.class public final Lcom/google/android/apps/gmm/car/i/i;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/car/bg;

.field final c:Lcom/google/android/apps/gmm/map/util/b/g;

.field final d:Lcom/google/android/apps/gmm/car/i/h;

.field e:Lcom/google/android/apps/gmm/map/r/b/a;

.field f:Lcom/google/android/apps/gmm/car/i/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/car/i/k",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/car/bm;",
            ">;>;"
        }
    .end annotation
.end field

.field final g:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/gmm/car/i/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/i/i;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/car/bg;Lcom/google/android/apps/gmm/car/c;Lcom/google/android/apps/gmm/map/util/b/g;Landroid/view/LayoutInflater;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Lcom/google/android/apps/gmm/car/i/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/i/j;-><init>(Lcom/google/android/apps/gmm/car/i/i;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/i;->g:Ljava/lang/Object;

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/i/i;->b:Lcom/google/android/apps/gmm/car/bg;

    .line 42
    iput-object p3, p0, Lcom/google/android/apps/gmm/car/i/i;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/car/i/h;

    invoke-virtual {p4}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/google/android/apps/gmm/car/i/h;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/car/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/i;->d:Lcom/google/android/apps/gmm/car/i/h;

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/i;->g:Ljava/lang/Object;

    invoke-interface {p3, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 46
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/i;->f:Lcom/google/android/apps/gmm/car/i/k;

    if-nez v0, :cond_1

    .line 90
    :cond_0
    return-void

    .line 80
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/car/i/i;->a:Ljava/lang/String;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/i;->f:Lcom/google/android/apps/gmm/car/i/k;

    .line 82
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/i/i;->f:Lcom/google/android/apps/gmm/car/i/k;

    .line 83
    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/i/k;->b()V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/i;->f:Lcom/google/android/apps/gmm/car/i/k;

    if-eqz v0, :cond_0

    .line 88
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Tried to start a search while it was being canceled."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
