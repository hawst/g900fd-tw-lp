.class public Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# instance fields
.field a:Landroid/support/v4/view/ViewPager;

.field b:Lcom/google/android/apps/gmm/base/views/HeaderView;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/android/apps/gmm/util/h;

.field e:Lcom/google/android/apps/gmm/photo/y;

.field f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field g:I

.field m:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/r/b/a/aje;",
            ">;"
        }
    .end annotation
.end field

.field n:Lcom/google/android/apps/gmm/base/g/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field o:I

.field p:I

.field q:I

.field r:Z

.field private s:Landroid/view/View;

.field private t:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 81
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->f:Ljava/util/Map;

    .line 96
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->q:I

    .line 596
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;
    .locals 2

    .prologue
    .line 140
    .line 142
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->J()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    const/4 v1, 0x0

    .line 144
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 140
    invoke-static {p0, v0, p1, v1}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/base/g/c;Ljava/lang/Integer;)Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/base/g/c;Ljava/lang/Integer;)Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;
    .locals 4
    .param p2    # Lcom/google/android/apps/gmm/base/g/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/r/b/a/aje;",
            ">;",
            "Lcom/google/android/apps/gmm/base/g/c;",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;"
        }
    .end annotation

    .prologue
    .line 109
    new-instance v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;-><init>()V

    .line 110
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 111
    const-string v2, "photos"

    invoke-virtual {p0, v1, v2, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 112
    if-eqz p3, :cond_0

    .line 113
    const-string v2, "initialIndex"

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 115
    :cond_0
    if-eqz p2, :cond_1

    .line 116
    const-string v2, "placemark"

    invoke-virtual {p0, v1, v2, p2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 118
    :cond_1
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 119
    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 339
    iget v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->q:I

    iget v1, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->g:I

    if-lt v0, v1, :cond_0

    .line 340
    const-string v0, ""

    .line 342
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->jJ:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->q:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected final b(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 582
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 583
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/a;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/a;-><init>(Landroid/content/Context;)V

    .line 584
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->a()Ljava/lang/String;

    move-result-object v0

    .line 585
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 586
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_3

    .line 587
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->q:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    invoke-virtual {v2}, Lcom/google/b/c/cv;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->q:I

    if-ltz v0, :cond_1

    .line 588
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    iget v2, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->q:I

    invoke-virtual {v0, v2}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aje;

    iget-object v0, v0, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aja;->d()Lcom/google/r/b/a/aja;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aja;

    iget-object v0, v0, Lcom/google/r/b/a/aja;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_4

    .line 591
    :cond_1
    :goto_1
    iget-object v0, v1, Lcom/google/android/apps/gmm/shared/c/c/a;->a:Ljava/lang/StringBuffer;

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 593
    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->b(Landroid/view/View;)V

    .line 594
    return-void

    .line 586
    :cond_3
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_0

    .line 588
    :cond_4
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 149
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "photos"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/cv;

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "placemark"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->n:Lcom/google/android/apps/gmm/base/g/c;

    .line 154
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->q:I

    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "initialIndex"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "initialIndex"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->q:I

    .line 159
    :cond_0
    if-eqz p1, :cond_2

    .line 160
    const-string v0, "focusItemIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    const-string v0, "focusItemIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->q:I

    .line 164
    :cond_1
    const-string v0, "totalPhotos"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 165
    const-string v0, "totalPhotos"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->g:I

    .line 168
    :cond_2
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 211
    sget v0, Lcom/google/android/apps/gmm/i;->a:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 213
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->r:Z

    if-nez v0, :cond_0

    .line 214
    sget v0, Lcom/google/android/apps/gmm/g;->ez:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 216
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->t:Z

    if-nez v0, :cond_1

    .line 217
    sget v0, Lcom/google/android/apps/gmm/g;->cH:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 219
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 11

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 231
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 259
    :goto_0
    return v0

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->d:Lcom/google/android/apps/gmm/util/h;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/util/h;->a(Z)V

    .line 235
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 236
    sget v3, Lcom/google/android/apps/gmm/g;->cH:I

    if-ne v0, v3, :cond_2

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    new-array v4, v4, [Lcom/google/b/f/cq;

    sget-object v5, Lcom/google/b/f/t;->ex:Lcom/google/b/f/t;

    aput-object v5, v4, v1

    sget-object v1, Lcom/google/b/f/t;->cL:Lcom/google/b/f/t;

    aput-object v1, v4, v2

    .line 239
    iput-object v4, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 241
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 238
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->n:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v3

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->n:Lcom/google/android/apps/gmm/base/g/c;

    .line 246
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v4

    iget v5, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->q:I

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    if-eqz v0, :cond_1

    if-ltz v5, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-ge v5, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    invoke-virtual {v0, v5}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aje;

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/google/r/b/a/aje;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/hg;

    invoke-virtual {v1}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, v0, Lcom/google/r/b/a/aje;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 247
    :goto_1
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 248
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->startActivity(Landroid/content/Intent;)V

    move v0, v2

    .line 249
    goto/16 :goto_0

    .line 245
    :cond_1
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "/maps/place"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "cid"

    iget-wide v6, v4, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-static {v6, v7}, Lcom/google/b/h/c;->a(J)Lcom/google/b/h/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/b/h/c;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "view"

    const-string v4, "feature"

    invoke-virtual {v0, v1, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "mcsrc"

    const-string v4, "photo"

    invoke-virtual {v0, v1, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "start"

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "num"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v4, "http"

    invoke-virtual {v1, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "www.google.com"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "/maps/photos/flagImage"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "hl"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "latlng"

    iget-wide v6, v3, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v8, v3, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v10, 0x31

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "photofeatureurl"

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_1

    .line 250
    :cond_2
    sget v3, Lcom/google/android/apps/gmm/g;->ez:I

    if-ne v0, v3, :cond_3

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    new-array v4, v4, [Lcom/google/b/f/cq;

    sget-object v5, Lcom/google/b/f/t;->fn:Lcom/google/b/f/t;

    aput-object v5, v4, v1

    sget-object v1, Lcom/google/b/f/t;->cL:Lcom/google/b/f/t;

    aput-object v1, v4, v2

    .line 253
    iput-object v4, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 255
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 252
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->n:Lcom/google/android/apps/gmm/base/g/c;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;Z)V

    move v0, v2

    .line 257
    goto/16 :goto_0

    .line 259
    :cond_3
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 331
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->d:Lcom/google/android/apps/gmm/util/h;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->cancel()V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/util/h;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 336
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 223
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->d:Lcom/google/android/apps/gmm/util/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/util/h;->a(Z)V

    .line 227
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 172
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->n:Lcom/google/android/apps/gmm/base/g/c;

    if-nez v0, :cond_0

    move v0, v1

    .line 175
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    invoke-virtual {v3}, Lcom/google/b/c/cv;->size()I

    move-result v3

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->g:I

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->n:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 177
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->d:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->r:Z

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->n:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->t:Z

    .line 180
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->c:Ljava/util/List;

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->c:Ljava/util/List;

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/util/h;->a(Landroid/app/Fragment;Ljava/util/Collection;)Lcom/google/android/apps/gmm/util/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->d:Lcom/google/android/apps/gmm/util/h;

    .line 182
    new-instance v0, Lcom/google/android/apps/gmm/base/views/aa;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/base/views/aa;-><init>(Landroid/content/Context;)V

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/views/aa;->d:Landroid/app/Fragment;

    const v3, 0x103006b

    iput v3, v0, Lcom/google/android/apps/gmm/base/views/aa;->c:I

    sget v3, Lcom/google/android/apps/gmm/h;->aj:I

    iput v3, v0, Lcom/google/android/apps/gmm/base/views/aa;->b:I

    new-instance v3, Lcom/google/android/apps/gmm/base/views/HeaderView;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;-><init>(Lcom/google/android/apps/gmm/base/views/aa;)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->b:Lcom/google/android/apps/gmm/base/views/HeaderView;

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->b:Lcom/google/android/apps/gmm/base/views/HeaderView;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/views/HeaderView;->setTitle(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/apps/gmm/h;->af:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v3, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    sget v0, Lcom/google/android/apps/gmm/g;->cj:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->a:Landroid/support/v4/view/ViewPager;

    new-instance v0, Lcom/google/android/apps/gmm/photo/y;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/photo/y;-><init>(Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->e:Lcom/google/android/apps/gmm/photo/y;

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->a:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->e:Lcom/google/android/apps/gmm/photo/y;

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/ag;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->a:Landroid/support/v4/view/ViewPager;

    new-instance v4, Lcom/google/android/apps/gmm/photo/w;

    invoke-direct {v4, p0}, Lcom/google/android/apps/gmm/photo/w;-><init>(Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;)V

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/bz;)V

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v4, v0, Landroid/graphics/Point;->x:I

    iput v4, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->o:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    iput v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->p:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->a:Landroid/support/v4/view/ViewPager;

    iget v4, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->q:I

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->s:Landroid/view/View;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->s:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->k:Landroid/view/View;

    .line 184
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 185
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v2, v3, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->b:Lcom/google/android/apps/gmm/base/views/HeaderView;

    iget-object v4, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->s:Landroid/view/View;

    .line 186
    invoke-virtual {v3, v4, v2}, Lcom/google/android/apps/gmm/base/views/HeaderView;->a(Landroid/view/View;Z)Landroid/view/View;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v3, v4, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v2, v3, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 187
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v3, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 188
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v3, Lcom/google/android/apps/gmm/base/activities/p;->O:Lcom/google/android/apps/gmm/base/a/a;

    .line 189
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v5, v3, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v2, v3, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 190
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    new-instance v1, Lcom/google/android/apps/gmm/photo/v;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/photo/v;-><init>(Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;)V

    .line 191
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 197
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->b:Lcom/google/android/apps/gmm/base/views/HeaderView;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->n:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->I()V

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/c;->s:Lcom/google/r/b/a/ade;

    if-nez v3, :cond_1

    move v0, v1

    goto/16 :goto_0

    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->s:Lcom/google/r/b/a/ade;

    iget v0, v0, Lcom/google/r/b/a/ade;->f:I

    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 177
    goto/16 :goto_1

    :cond_3
    move v0, v1

    .line 178
    goto/16 :goto_2
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 204
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 205
    const-string v0, "focusItemIndex"

    iget v1, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 206
    const-string v0, "totalPhotos"

    iget v1, p0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->g:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 207
    return-void
.end method
