.class public Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;
.super Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;
.source "PG"


# instance fields
.field c:Lcom/google/android/apps/gmm/share/e;

.field d:Landroid/widget/LinearLayout;

.field e:[Lcom/google/android/apps/gmm/share/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field f:Ljava/lang/String;

.field g:Z

.field private h:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ[Lcom/google/android/apps/gmm/share/a/a;)Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 142
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 143
    const-string v3, "intent"

    .line 144
    new-instance v4, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND"

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "text/plain"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p3, :cond_0

    const-string v0, "android.intent.extra.SUBJECT"

    invoke-virtual {v4, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "\n"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "\n"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p5, :cond_3

    const-string v0, "urlFormatMessageId"

    invoke-virtual {v4, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_3
    const-string v0, "isRoute"

    invoke-virtual {v4, v0, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 143
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 145
    const-string v0, "callback"

    invoke-virtual {p1, v2, v0, p7}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 146
    const-string v0, "url"

    invoke-virtual {v2, v0, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v0, "isShortened"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 148
    new-instance v0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;-><init>()V

    .line 150
    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 151
    return-object v0

    :cond_4
    move v0, v1

    .line 144
    goto :goto_0
.end method


# virtual methods
.method protected final a()Landroid/view/View;
    .locals 3

    .prologue
    .line 84
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->d:Landroid/widget/LinearLayout;

    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/share/d;

    iget-object v2, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->d:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->c:Lcom/google/android/apps/gmm/share/e;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->d:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 94
    const-string v0, "intent"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 95
    :cond_0
    const-string v0, "callback"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 100
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v3, "callback"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 101
    array-length v3, v0

    const-class v4, [Lcom/google/android/apps/gmm/share/a/a;

    invoke-static {v0, v3, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/share/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->e:[Lcom/google/android/apps/gmm/share/a/a;

    .line 103
    const-string v0, "intent"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->h:Landroid/content/Intent;

    .line 104
    const-string v0, "isShortened"

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->g:Z

    .line 105
    const-string v0, "url"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->f:Ljava/lang/String;

    .line 106
    new-instance v0, Lcom/google/android/apps/gmm/share/f;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->h:Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->e:[Lcom/google/android/apps/gmm/share/a/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->f:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/share/f;-><init>(Landroid/content/Context;Landroid/content/Intent;[Lcom/google/android/apps/gmm/share/a/a;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->c:Lcom/google/android/apps/gmm/share/e;

    .line 107
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0

    .line 100
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    .line 106
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto :goto_1
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 112
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->onResume()V

    .line 113
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->g:Z

    if-eqz v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 116
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 117
    invoke-static {}, Lcom/google/r/b/a/xb;->newBuilder()Lcom/google/r/b/a/xd;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->f:Ljava/lang/String;

    .line 118
    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 116
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_1

    .line 118
    :cond_2
    iget v3, v1, Lcom/google/r/b/a/xd;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/google/r/b/a/xd;->a:I

    iput-object v2, v1, Lcom/google/r/b/a/xd;->b:Ljava/lang/Object;

    const/4 v2, 0x0

    .line 119
    iget v3, v1, Lcom/google/r/b/a/xd;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v1, Lcom/google/r/b/a/xd;->a:I

    iput v2, v1, Lcom/google/r/b/a/xd;->c:I

    .line 120
    invoke-virtual {v1}, Lcom/google/r/b/a/xd;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/xb;

    .line 121
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/share/k;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/share/k;-><init>(Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 133
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "callback"

    iget-object v2, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->e:[Lcom/google/android/apps/gmm/share/a/a;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 135
    const-string v0, "intent"

    iget-object v1, p0, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->h:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 136
    return-void

    .line 134
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method
