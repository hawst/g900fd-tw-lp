.class public abstract Lcom/google/android/apps/gmm/base/placelists/a/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private e:Lcom/google/android/apps/gmm/search/c/a;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/placelists/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->b:Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->c:Ljava/util/List;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/search/c/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/search/c/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->e:Lcom/google/android/apps/gmm/search/c/a;

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I

    .line 67
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/base/g/c;Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/g/c;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;>;)I"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 177
    monitor-enter p0

    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    .line 178
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 179
    if-nez v0, :cond_0

    move v0, v1

    .line 189
    :goto_0
    monitor-exit p0

    return v0

    .line 183
    :cond_0
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    .line 184
    :try_start_1
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 185
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/base/g/c;->b(Lcom/google/android/apps/gmm/base/g/c;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 186
    goto :goto_0

    .line 183
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 189
    goto :goto_0

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 165
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 166
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 167
    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/b/l;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 169
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->f()Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v1

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/g/g;->s:Lcom/google/android/apps/gmm/z/b/l;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object p2

    .line 172
    :cond_0
    invoke-virtual {p1, p2}, Lcom/google/android/apps/gmm/x/o;->b(Ljava/io/Serializable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    monitor-exit p0

    return-void

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 0

    .prologue
    .line 266
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 267
    return-void
.end method

.method private declared-synchronized writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 262
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    monitor-exit p0

    return-void

    .line 262
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()I
    .locals 1

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/base/g/c;)I
    .locals 1

    .prologue
    .line 197
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(Lcom/google/android/apps/gmm/base/g/c;Ljava/util/List;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)Lcom/google/android/apps/gmm/base/g/c;
    .locals 1

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/b/a/j;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 112
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 114
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 115
    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    .line 116
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 119
    :cond_1
    monitor-exit p0

    return-object v2

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ILcom/google/android/apps/gmm/base/g/c;)V
    .locals 1

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/base/g/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    monitor-exit p0

    return-void

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ILcom/google/android/apps/gmm/base/placelists/a/b;)V
    .locals 1

    .prologue
    .line 134
    monitor-enter p0

    if-nez p2, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 135
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/base/placelists/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    monitor-exit p0

    return-void
.end method

.method public abstract a(Lcom/google/android/apps/gmm/base/placelists/a/a;Z)V
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/search/c/a;)V
    .locals 1

    .prologue
    .line 312
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->e:Lcom/google/android/apps/gmm/search/c/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    monitor-exit p0

    return-void

    .line 312
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 273
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 274
    monitor-exit p0

    return-void

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    .line 74
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_0

    .line 76
    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 80
    :cond_1
    :try_start_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 81
    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v3

    .line 82
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->c:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 84
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 88
    :cond_3
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            "Lcom/google/e/a/a/a/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 296
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_3

    .line 297
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v3

    .line 298
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 299
    if-eqz v0, :cond_0

    .line 303
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/g/c;->Y()Lcom/google/android/apps/gmm/hotels/a/c;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/g/c;->Y()Lcom/google/android/apps/gmm/hotels/a/c;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/hotels/a/c;->a:Lcom/google/e/a/a/a/b;

    const/4 v4, 0x7

    invoke-static {v1, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v1

    .line 304
    :goto_2
    const/4 v4, 0x7

    iget-object v5, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v4, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 306
    :cond_0
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/base/g/c;->a(Lcom/google/e/a/a/a/b;)V

    .line 307
    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b(ILcom/google/android/apps/gmm/base/g/c;)V

    .line 296
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 303
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const-string v1, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 309
    :cond_3
    monitor-exit p0

    return-void

    .line 296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/base/g/c;)I
    .locals 1

    .prologue
    .line 205
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->c:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(Lcom/google/android/apps/gmm/base/g/c;Ljava/util/List;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/map/b/a/j;)I
    .locals 10

    .prologue
    const/4 v1, -0x1

    .line 211
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-wide v6, p1, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v6, v8

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)Lcom/google/android/apps/gmm/x/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(ILcom/google/android/apps/gmm/base/g/c;)V
    .locals 1

    .prologue
    .line 155
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/base/g/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    monitor-exit p0

    return-void

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()I
    .locals 1

    .prologue
    .line 215
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(I)Lcom/google/android/apps/gmm/base/g/c;
    .locals 1

    .prologue
    .line 127
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 5

    .prologue
    .line 237
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(Lcom/google/android/apps/gmm/base/g/c;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I

    .line 238
    iget v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I

    if-gez v0, :cond_0

    .line 239
    sget-object v0, Lcom/google/android/apps/gmm/base/placelists/a/a;->a:Ljava/lang/String;

    const-string v1, "Placemark %s should exist in mapPlacemarkRefs"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 240
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v4

    aput-object v4, v2, v3

    .line 239
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    :cond_0
    monitor-exit p0

    return-void

    .line 237
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(I)Lcom/google/android/apps/gmm/x/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 2

    .prologue
    .line 219
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(Lcom/google/android/apps/gmm/base/g/c;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 361
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(Lcom/google/android/apps/gmm/base/g/c;)I

    move-result v3

    .line 363
    const/4 v0, -0x1

    if-ne v3, v0, :cond_0

    .line 365
    invoke-static {p1}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v2

    .line 366
    iget v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I

    add-int/lit8 v0, v0, 0x1

    .line 367
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    invoke-interface {v3, v0, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move v6, v1

    move v1, v0

    move v0, v6

    .line 403
    :goto_0
    iput v1, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 404
    monitor-exit p0

    return v0

    .line 373
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    .line 374
    iget v4, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I

    if-ne v4, v3, :cond_1

    .line 376
    iget v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I

    move v1, v0

    move v0, v2

    .line 377
    goto :goto_0

    .line 378
    :cond_1
    iget v4, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I

    add-int/lit8 v5, v3, 0x1

    if-ne v4, v5, :cond_2

    move v0, v2

    move v1, v3

    .line 382
    goto :goto_0

    .line 383
    :cond_2
    iget v2, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I

    if-ge v2, v3, :cond_3

    .line 386
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 387
    iget v2, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I

    add-int/lit8 v2, v2, 0x1

    .line 388
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    invoke-interface {v3, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move v0, v1

    move v1, v2

    .line 389
    goto :goto_0

    .line 390
    :cond_3
    iget v2, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I

    if-le v2, v3, :cond_4

    .line 394
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 395
    iget v2, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I

    .line 396
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    invoke-interface {v3, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move v0, v1

    move v1, v2

    .line 397
    goto :goto_0

    .line 400
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 361
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Lcom/google/android/apps/gmm/base/g/c;
    .locals 1

    .prologue
    .line 223
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c(I)Lcom/google/android/apps/gmm/base/g/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e(I)V
    .locals 1

    .prologue
    .line 227
    monitor-enter p0

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    if-ltz p1, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    .line 228
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 227
    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 228
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 229
    :cond_2
    :try_start_1
    iput p1, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 230
    monitor-exit p0

    return-void
.end method

.method public final f()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    .line 251
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 252
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    .line 253
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 252
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 256
    :cond_2
    return-object v2
.end method

.method public final declared-synchronized g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized h()Z
    .locals 1

    .prologue
    .line 284
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return v0
.end method

.method public final declared-synchronized i()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 316
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->e:Lcom/google/android/apps/gmm/search/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized j()Z
    .locals 1

    .prologue
    .line 320
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return v0
.end method

.method public final declared-synchronized k()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 408
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b()I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 409
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_2

    .line 410
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    .line 411
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 409
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 413
    :cond_2
    monitor-exit p0

    return-object v2
.end method

.method public declared-synchronized l()V
    .locals 1

    .prologue
    .line 420
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 422
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/placelists/a/a;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 423
    monitor-exit p0

    return-void

    .line 420
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
