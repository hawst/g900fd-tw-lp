.class public final enum Lcom/google/android/apps/gmm/p/b/h;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/p/b/h;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/p/b/h;

.field public static final enum b:Lcom/google/android/apps/gmm/p/b/h;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/p/b/h;


# instance fields
.field public c:Lcom/google/android/apps/gmm/p/b/l;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/p/b/h;

    const-string v1, "SLOW"

    sget-object v2, Lcom/google/android/apps/gmm/p/b/l;->b:Lcom/google/android/apps/gmm/p/b/l;

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/p/b/h;-><init>(Ljava/lang/String;ILcom/google/android/apps/gmm/p/b/l;)V

    sput-object v0, Lcom/google/android/apps/gmm/p/b/h;->a:Lcom/google/android/apps/gmm/p/b/h;

    .line 33
    new-instance v0, Lcom/google/android/apps/gmm/p/b/h;

    const-string v1, "FAST"

    sget-object v2, Lcom/google/android/apps/gmm/p/b/l;->c:Lcom/google/android/apps/gmm/p/b/l;

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/p/b/h;-><init>(Ljava/lang/String;ILcom/google/android/apps/gmm/p/b/l;)V

    sput-object v0, Lcom/google/android/apps/gmm/p/b/h;->b:Lcom/google/android/apps/gmm/p/b/h;

    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/gmm/p/b/h;

    sget-object v1, Lcom/google/android/apps/gmm/p/b/h;->a:Lcom/google/android/apps/gmm/p/b/h;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/p/b/h;->b:Lcom/google/android/apps/gmm/p/b/h;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/p/b/h;->d:[Lcom/google/android/apps/gmm/p/b/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/apps/gmm/p/b/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/p/b/l;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput-object p3, p0, Lcom/google/android/apps/gmm/p/b/h;->c:Lcom/google/android/apps/gmm/p/b/l;

    .line 39
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/p/b/h;
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/gmm/p/b/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/b/h;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/p/b/h;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/apps/gmm/p/b/h;->d:[Lcom/google/android/apps/gmm/p/b/h;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/p/b/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/p/b/h;

    return-object v0
.end method
