.class public Lcom/google/android/apps/gmm/search/d/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/e/a;


# instance fields
.field a:Ljava/lang/Boolean;

.field public b:Lcom/google/android/apps/gmm/map/r/b/a;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/view/View;

.field private final f:Landroid/view/View$OnClickListener;

.field private final g:Landroid/content/res/Resources;

.field private final h:Lcom/google/android/apps/gmm/base/g/a;

.field private final i:Z

.field private j:Ljava/lang/Float;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Lcom/google/r/b/a/aje;

.field private n:Ljava/lang/CharSequence;

.field private o:Ljava/lang/CharSequence;

.field private p:Ljava/lang/CharSequence;

.field private q:Ljava/lang/CharSequence;

.field private r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/a;Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->a:Ljava/lang/Boolean;

    .line 84
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/d/a;->c:Landroid/content/Context;

    .line 85
    iput-object p3, p0, Lcom/google/android/apps/gmm/search/d/a;->e:Landroid/view/View;

    .line 86
    iput-object p4, p0, Lcom/google/android/apps/gmm/search/d/a;->f:Landroid/view/View$OnClickListener;

    .line 87
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->d:Lcom/google/android/apps/gmm/x/o;

    .line 88
    iput-object p2, p0, Lcom/google/android/apps/gmm/search/d/a;->h:Lcom/google/android/apps/gmm/base/g/a;

    .line 89
    invoke-virtual {p3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->g:Landroid/content/res/Resources;

    .line 90
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/search/d/a;->a(Lcom/google/android/apps/gmm/base/g/a;)V

    .line 91
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.telephony"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/d/a;->i:Z

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/c;Landroid/view/View;Landroid/view/View$OnClickListener;Lcom/google/android/apps/gmm/map/r/b/a;)V
    .locals 8
    .param p5    # Lcom/google/android/apps/gmm/map/r/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/d/a;->a:Ljava/lang/Boolean;

    .line 65
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/d/a;->c:Landroid/content/Context;

    .line 66
    iput-object p3, p0, Lcom/google/android/apps/gmm/search/d/a;->e:Landroid/view/View;

    .line 67
    iput-object p4, p0, Lcom/google/android/apps/gmm/search/d/a;->f:Landroid/view/View$OnClickListener;

    .line 68
    iput-object p5, p0, Lcom/google/android/apps/gmm/search/d/a;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 69
    invoke-static {p2}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->d:Lcom/google/android/apps/gmm/x/o;

    .line 70
    iget-object v0, p2, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->h:Lcom/google/android/apps/gmm/base/g/a;

    .line 71
    invoke-virtual {p3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->g:Landroid/content/res/Resources;

    .line 72
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->W()Lcom/google/r/b/a/aje;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->m:Lcom/google/r/b/a/aje;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->v()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->j:Ljava/lang/Float;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->s()I

    move-result v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->j:Ljava/lang/Float;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->j:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v5, 0x40600000    # 3.5f

    cmpl-float v0, v0, v5

    if-lez v0, :cond_6

    move v0, v2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    if-lez v4, :cond_0

    const-string v0, "(%d)"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/d/a;->l:Ljava/lang/String;

    :cond_1
    :goto_2
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->u()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_8

    :cond_2
    move v0, v2

    :goto_3
    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->l:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    :cond_3
    move v0, v2

    :goto_4
    if-nez v0, :cond_b

    const-string v0, " \u00b7 "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->k:Ljava/lang/String;

    :goto_6
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/d/a;->w()V

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/d/a;->x()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->q:Ljava/lang/CharSequence;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->q:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_c

    move v0, v2

    :goto_7
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-static {}, Landroid/support/v4/f/a;->a()Landroid/support/v4/f/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/f/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "  \u2022  "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/d/a;->q:Ljava/lang/CharSequence;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->n:Ljava/lang/CharSequence;

    .line 73
    :cond_4
    :goto_8
    if-eqz p2, :cond_e

    :goto_9
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/search/d/a;->r:Z

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->h:Lcom/google/android/apps/gmm/base/g/a;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/search/d/a;->a(Lcom/google/android/apps/gmm/base/g/a;)V

    .line 75
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.telephony"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/d/a;->i:Z

    .line 76
    return-void

    .line 72
    :cond_5
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->v()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move v0, v3

    goto/16 :goto_1

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->j:Ljava/lang/Float;

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->l:Ljava/lang/String;

    goto/16 :goto_2

    :cond_8
    move v0, v3

    goto/16 :goto_3

    :cond_9
    move v0, v3

    goto/16 :goto_4

    :cond_a
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_b
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->k:Ljava/lang/String;

    goto/16 :goto_6

    :cond_c
    move v0, v3

    goto/16 :goto_7

    :cond_d
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/d/a;->n:Ljava/lang/CharSequence;

    goto :goto_8

    :cond_e
    move v2, v3

    .line 73
    goto :goto_9
.end method

.method private a(Lcom/google/android/apps/gmm/base/g/a;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 240
    if-nez p1, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/a;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    :cond_2
    move v0, v3

    :goto_1
    if-eqz v0, :cond_4

    .line 244
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/a;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_a

    :cond_3
    move v0, v3

    :goto_2
    if-nez v0, :cond_7

    .line 245
    :cond_4
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 247
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/a;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_b

    :cond_5
    move v0, v3

    :goto_3
    if-eqz v0, :cond_c

    const-string v0, ""

    .line 248
    :goto_4
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/g/a;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_d

    :cond_6
    move v1, v3

    :goto_5
    if-eqz v1, :cond_e

    const-string v1, ""

    :goto_6
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 246
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 249
    iput-object v4, p0, Lcom/google/android/apps/gmm/search/d/a;->p:Ljava/lang/CharSequence;

    .line 251
    :cond_7
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/a;->h:Ljava/lang/String;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_f

    :cond_8
    move v0, v3

    :goto_7
    if-nez v0, :cond_0

    .line 252
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 253
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/d/a;->g:Landroid/content/res/Resources;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/util/a;->a(Landroid/text/SpannableStringBuilder;Landroid/content/res/Resources;Z)V

    .line 254
    const-string v1, "  "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 255
    invoke-static {}, Landroid/support/v4/f/a;->a()Landroid/support/v4/f/a;

    move-result-object v1

    .line 256
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/g/a;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 255
    invoke-virtual {v1, v2}, Landroid/support/v4/f/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 257
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->o:Ljava/lang/CharSequence;

    goto/16 :goto_0

    :cond_9
    move v0, v2

    .line 243
    goto/16 :goto_1

    :cond_a
    move v0, v2

    .line 244
    goto/16 :goto_2

    :cond_b
    move v0, v2

    .line 247
    goto/16 :goto_3

    :cond_c
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/g/a;->f:Ljava/lang/String;

    goto/16 :goto_4

    :cond_d
    move v1, v2

    .line 248
    goto :goto_5

    :cond_e
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/g/a;->g:Ljava/lang/String;

    goto :goto_6

    :cond_f
    move v0, v2

    .line 251
    goto :goto_7
.end method


# virtual methods
.method public final A()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->d:Lcom/google/android/apps/gmm/x/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    if-nez v0, :cond_1

    .line 333
    :cond_0
    const/4 v0, 0x0

    .line 341
    :goto_0
    return-object v0

    .line 336
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->d:Lcom/google/android/apps/gmm/x/o;

    .line 337
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->d:Lcom/google/android/apps/gmm/x/o;

    .line 338
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/g/a;->k:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/b/f/t;->en:Lcom/google/b/f/t;

    :goto_1
    aput-object v0, v2, v3

    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 341
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 338
    :cond_2
    sget-object v0, Lcom/google/b/f/t;->eg:Lcom/google/b/f/t;

    goto :goto_1
.end method

.method public final B()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->f:Landroid/view/View$OnClickListener;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/d/a;->e:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final C()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->d:Lcom/google/android/apps/gmm/x/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    if-nez v0, :cond_1

    .line 357
    :cond_0
    const/4 v0, 0x0

    .line 363
    :goto_0
    return-object v0

    .line 360
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->d:Lcom/google/android/apps/gmm/x/o;

    .line 361
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->ee:Lcom/google/b/f/t;

    aput-object v3, v1, v2

    .line 362
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 363
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0
.end method

.method public final D()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4

    .prologue
    .line 368
    sget-object v0, Lcom/google/b/f/t;->O:Lcom/google/b/f/t;

    .line 369
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/d/a;->h:Lcom/google/android/apps/gmm/base/g/a;

    .line 370
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/g/a;->p:Lcom/google/android/apps/gmm/z/b/l;

    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 371
    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->h:Lcom/google/android/apps/gmm/base/g/a;

    .line 372
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->i:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    .line 373
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final E()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->h:Lcom/google/android/apps/gmm/base/g/a;

    .line 379
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->p:Lcom/google/android/apps/gmm/z/b/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->ed:Lcom/google/b/f/t;

    aput-object v3, v1, v2

    .line 380
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/d/a;->h:Lcom/google/android/apps/gmm/base/g/a;

    .line 381
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/g/a;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    .line 382
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 105
    const-string v0, "%.1f"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/d/a;->j:Ljava/lang/Float;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->j:Ljava/lang/Float;

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->j:Ljava/lang/Float;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->j:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v1, 0x40600000    # 3.5f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->p:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->p:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->p:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->o:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->o:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->o:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->h:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->g:Landroid/content/res/Resources;

    .line 149
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Landroid/content/res/Configuration;->isLayoutSizeAtLeast(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->g:Landroid/content/res/Resources;

    .line 150
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v3, 0x140

    if-lt v0, v3, :cond_3

    :cond_0
    move v0, v2

    .line 152
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/search/d/a;->i:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/d/a;->h:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/g/a;->r:Ljava/lang/String;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_4

    :cond_1
    move v3, v2

    :goto_1
    if-nez v3, :cond_5

    move v3, v2

    :goto_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/d/a;->m:Lcom/google/r/b/a/aje;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/d/a;->m:Lcom/google/r/b/a/aje;

    .line 153
    invoke-virtual {v3}, Lcom/google/r/b/a/aje;->g()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_6

    :cond_2
    move v3, v2

    :goto_3
    if-nez v3, :cond_7

    if-eqz v0, :cond_7

    .line 152
    :goto_4
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    .line 150
    goto :goto_0

    :cond_4
    move v3, v1

    .line 152
    goto :goto_1

    :cond_5
    move v3, v1

    goto :goto_2

    :cond_6
    move v3, v1

    .line 153
    goto :goto_3

    :cond_7
    move v2, v1

    goto :goto_4
.end method

.method public final j()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->m:Lcom/google/r/b/a/aje;

    if-nez v0, :cond_0

    .line 161
    const/4 v0, 0x0

    .line 165
    :goto_0
    return-object v0

    .line 164
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/k;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->m:Lcom/google/r/b/a/aje;

    invoke-virtual {v0}, Lcom/google/r/b/a/aje;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->m:Lcom/google/r/b/a/aje;

    .line 165
    iget v0, v0, Lcom/google/r/b/a/aje;->h:I

    invoke-static {v0}, Lcom/google/r/b/a/ajh;->a(I)Lcom/google/r/b/a/ajh;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/r/b/a/ajh;->a:Lcom/google/r/b/a/ajh;

    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/b/a;->a(Lcom/google/r/b/a/ajh;)Lcom/google/android/apps/gmm/util/webimageview/b;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;I)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final k()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->h:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 172
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/d/a;->c:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 173
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    .line 178
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    const-string v3, "tel: "

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->h:Lcom/google/android/apps/gmm/base/g/a;

    .line 179
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->r:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 181
    const/4 v0, 0x0

    return-object v0

    .line 179
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final m()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 186
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/search/d/a;->i:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/d/a;->h:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/g/a;->r:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final n()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->n:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->n:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->n:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final p()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->q:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->q:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->q:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final r()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 216
    iget-object v2, p0, Lcom/google/android/apps/gmm/search/d/a;->l:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final s()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 226
    iget-object v2, p0, Lcom/google/android/apps/gmm/search/d/a;->k:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final u()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/d/a;->i()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/d/a;->m()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()V
    .locals 5

    .prologue
    .line 308
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/d/a;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/d/a;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->d:Lcom/google/android/apps/gmm/x/o;

    .line 309
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/d/a;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/base/g/c;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->q:Ljava/lang/CharSequence;

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->q:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->q:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/d/a;->x()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    const-string v0, " \u00b7 "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/d/a;->q:Ljava/lang/CharSequence;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->q:Ljava/lang/CharSequence;

    .line 313
    :cond_0
    return-void
.end method

.method public final x()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->j:Ljava/lang/Float;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->j:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v3, 0x40600000    # 3.5f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/d/a;->r()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/d/a;->t()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final y()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/d/a;->r:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final z()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/a;->f:Landroid/view/View$OnClickListener;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/d/a;->e:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    const/4 v0, 0x0

    return-object v0
.end method
