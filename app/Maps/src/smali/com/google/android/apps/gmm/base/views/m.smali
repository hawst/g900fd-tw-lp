.class Lcom/google/android/apps/gmm/base/views/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/views/CardListView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/views/CardListView;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/m;->a:Lcom/google/android/apps/gmm/base/views/CardListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/m;->a:Lcom/google/android/apps/gmm/base/views/CardListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/CardListView;->b:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/m;->a:Lcom/google/android/apps/gmm/base/views/CardListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/CardListView;->b:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/m;->a:Lcom/google/android/apps/gmm/base/views/CardListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/CardListView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$OnScrollListener;

    .line 98
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    goto :goto_0

    .line 100
    :cond_1
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/m;->a:Lcom/google/android/apps/gmm/base/views/CardListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/CardListView;->b:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/m;->a:Lcom/google/android/apps/gmm/base/views/CardListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/CardListView;->b:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/m;->a:Lcom/google/android/apps/gmm/base/views/CardListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/CardListView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$OnScrollListener;

    .line 87
    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    goto :goto_0

    .line 89
    :cond_1
    return-void
.end method
