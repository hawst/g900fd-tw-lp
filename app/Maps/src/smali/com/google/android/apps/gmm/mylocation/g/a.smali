.class public Lcom/google/android/apps/gmm/mylocation/g/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/b/a/y;

.field public b:Lcom/google/android/apps/gmm/map/b/a/y;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/b/b;",
            ">;"
        }
    .end annotation
.end field

.field public d:F

.field public e:I

.field public f:Lcom/google/android/apps/gmm/map/b/a/y;

.field public g:Z

.field public h:F

.field public i:F

.field public j:F

.field public k:F

.field public l:Z

.field private m:Lcom/google/android/apps/gmm/map/indoor/d/f;

.field private n:Z

.field private o:Lcom/google/android/apps/gmm/map/b/a/y;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->l:Z

    .line 88
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mylocation/g/a;->a()V

    .line 92
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/y;FI)V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->l:Z

    .line 88
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 99
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iput p2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    iput p3, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    .line 100
    return-void

    .line 99
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 255
    iput-object v1, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 256
    iput-object v1, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 257
    iput v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    .line 258
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    .line 259
    iput-object v1, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 260
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    .line 261
    iput-object v1, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->m:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 262
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->n:Z

    .line 263
    iput v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->h:F

    .line 264
    iput v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->i:F

    .line 265
    iput v3, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->k:F

    .line 266
    iput v3, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    .line 267
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->l:Z

    .line 268
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/mylocation/g/a;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 227
    if-nez p1, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mylocation/g/a;->a()V

    .line 246
    :goto_0
    return-void

    .line 230
    :cond_0
    iget-object v2, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    iget v4, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    if-nez v2, :cond_1

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v3, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    iput v4, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    .line 231
    iget-object v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 234
    iget-object v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_2

    :goto_2
    iput-object v1, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 237
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    .line 238
    iget-object v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->m:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->m:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 239
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->n:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->n:Z

    .line 240
    iget v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->h:F

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->h:F

    .line 241
    iget v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->i:F

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->i:F

    .line 242
    iget v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->k:F

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->k:F

    .line 243
    iget v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    .line 244
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->l:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->l:Z

    goto :goto_0

    .line 230
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;)V

    goto :goto_1

    .line 234
    :cond_2
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;)V

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/bd;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 368
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 338
    goto :goto_0

    .line 346
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 347
    :goto_2
    iget v3, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v4

    double-to-int v4, v4

    mul-int/2addr v3, v4

    .line 349
    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v5, v3

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v6, v3

    iput v5, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v6, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v4, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 350
    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p1, v4}, Lcom/google/android/apps/gmm/map/b/a/bd;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v2

    .line 351
    goto :goto_1

    .line 346
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_2

    .line 354
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v5, v3

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v0, v3

    iput v5, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v0, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v4, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/bd;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    .line 356
    goto :goto_1

    .line 359
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/bd;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    .line 360
    goto :goto_1

    .line 363
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_6

    .line 364
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/bd;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 365
    goto :goto_1

    :cond_6
    move v0, v1

    .line 368
    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 281
    if-ne p0, p1, :cond_1

    .line 294
    :cond_0
    :goto_0
    return v0

    .line 283
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 284
    goto :goto_0

    .line 285
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 286
    goto :goto_0

    .line 288
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/mylocation/g/a;

    .line 289
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_8

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_4
    move v2, v0

    :goto_1
    if-eqz v2, :cond_7

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    iget v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_7

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    iget v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    if-ne v2, v3, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 292
    if-eq v2, v3, :cond_5

    if-eqz v2, :cond_9

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_5
    move v2, v0

    :goto_2
    if-eqz v2, :cond_7

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    if-ne v2, v3, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->m:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->m:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 294
    if-eq v2, v3, :cond_6

    if-eqz v2, :cond_a

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_6
    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->n:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->n:Z

    if-ne v2, v3, :cond_7

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->h:F

    iget v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->h:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_7

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->i:F

    iget v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->i:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_7

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->k:F

    iget v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->k:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_7

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    iget v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_7

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->l:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->l:Z

    if-eq v2, v3, :cond_0

    :cond_7
    move v0, v1

    goto/16 :goto_0

    :cond_8
    move v2, v1

    .line 289
    goto :goto_1

    :cond_9
    move v2, v1

    .line 292
    goto :goto_2

    :cond_a
    move v2, v1

    .line 294
    goto :goto_3
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 306
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    .line 307
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->m:Lcom/google/android/apps/gmm/map/indoor/d/f;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->h:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->i:F

    .line 308
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->k:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->l:Z

    .line 309
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 306
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_0

    .line 315
    const-string v0, "Invalid point"

    .line 334
    :goto_0
    return-object v0

    .line 317
    :cond_0
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    .line 318
    const-string v0, "@"

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->k()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 319
    const-string v0, "Accuracy"

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_4

    .line 321
    const-string v0, "Accuracy point"

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->k()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 323
    :cond_4
    const-string v0, "Use angle"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 324
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    if-eqz v0, :cond_7

    .line 325
    const-string v0, "Angle"

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 327
    :cond_7
    const-string v0, "Brightness"

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->k:F

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 328
    const-string v0, "Height"

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->h:F

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 329
    const-string v0, "Level"

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->m:Lcom/google/android/apps/gmm/map/indoor/d/f;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 330
    const-string v0, "IsLevelObscured"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->n:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 331
    const-string v0, "Staleness"

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->i:F

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 332
    const-string v0, "ScalingFactor"

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 333
    const-string v0, "isCurrentlyDisplayed"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/mylocation/g/a;->l:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 334
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
