.class public Lcom/google/android/apps/gmm/map/internal/c/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/bu;


# instance fields
.field public final a:Landroid/accounts/Account;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/accounts/Account;)V
    .locals 0
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/b;->a:Landroid/accounts/Account;

    .line 27
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bv;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->e:Lcom/google/android/apps/gmm/map/internal/c/bv;

    return-object v0
.end method

.method public final a(Lcom/google/e/a/a/a/b;)V
    .locals 0

    .prologue
    .line 47
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ai;)Z
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/c/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/bu;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/c/b;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 61
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/google/android/apps/gmm/map/internal/c/b;

    if-nez v1, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v0

    .line 64
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/b;

    .line 65
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/b;->a:Landroid/accounts/Account;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/b;->a:Landroid/accounts/Account;

    if-eq v1, v2, :cond_2

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/b;->a:Landroid/accounts/Account;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/b;->a:Landroid/accounts/Account;

    invoke-virtual {v0}, Landroid/accounts/Account;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/b;->a:Landroid/accounts/Account;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/b;->a:Landroid/accounts/Account;

    invoke-virtual {v0}, Landroid/accounts/Account;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
