.class Lcom/google/android/apps/gmm/search/y;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/search/SearchListFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/search/SearchListFragment;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/y;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/j/t;)V
    .locals 5
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 267
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/g/a;

    if-nez v0, :cond_1

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/y;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 274
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    check-cast v0, Lcom/google/android/apps/gmm/map/g/a;

    .line 275
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/g/a;->f:Z

    if-eqz v1, :cond_0

    .line 279
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/y;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/search/al;

    iget-object v1, v1, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x1f

    const/16 v3, 0x18

    invoke-virtual {v1, v2, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 280
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/y;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v2

    sget-object v3, Lcom/google/r/b/a/tf;->e:Lcom/google/r/b/a/tf;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/y;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    .line 281
    iget-object v1, v1, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/search/al;

    iget-object v1, v1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/ap;->o()Ljava/lang/String;

    move-result-object v1

    .line 280
    invoke-interface {v2, v3, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/r/b/a/tf;Ljava/lang/String;)V

    .line 284
    :cond_2
    new-instance v1, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 285
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/g/g;->a(Lcom/google/android/apps/gmm/map/g/a;)Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v0

    .line 287
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    .line 288
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-boolean v4, v1, Lcom/google/android/apps/gmm/base/g/i;->h:Z

    .line 289
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    .line 291
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/y;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/search/ap;->d(Lcom/google/android/apps/gmm/base/g/c;)Z

    .line 292
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/y;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/SearchListFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->b:Lcom/google/android/apps/gmm/base/fragments/a/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    if-ne v0, v2, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/SearchListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    :cond_3
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/search/SearchFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/search/SearchFragment;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->a(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V

    goto/16 :goto_0
.end method
