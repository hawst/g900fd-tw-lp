.class public Lcom/google/android/apps/gmm/prefetchcache/v;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/u;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

.field private final c:Lcom/google/android/apps/gmm/prefetchcache/api/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/gmm/prefetchcache/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/prefetchcache/v;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/prefetchcache/api/c;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 34
    iput-object p2, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->c:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    .line 35
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/libraries/curvular/cf;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    :goto_0
    return-object v7

    .line 55
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/l;->jz:I

    if-ne p1, v0, :cond_1

    .line 56
    sget-object v1, Lcom/google/b/f/t;->ca:Lcom/google/b/f/t;

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->c:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->a()Lcom/google/r/b/a/a/af;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/prefetchcache/EditAreaDialogFragment;->a(Lcom/google/r/b/a/a/af;)Lcom/google/android/apps/gmm/prefetchcache/EditAreaDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1, v7}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    goto :goto_0

    .line 58
    :cond_1
    sget v0, Lcom/google/android/apps/gmm/l;->jA:I

    if-ne p1, v0, :cond_2

    .line 59
    sget-object v1, Lcom/google/b/f/t;->cc:Lcom/google/b/f/t;

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->u()Lcom/google/android/apps/gmm/prefetchcache/api/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->c:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    .line 61
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/api/a;->b(Lcom/google/android/apps/gmm/prefetchcache/api/c;)V

    goto :goto_0

    .line 62
    :cond_2
    sget v0, Lcom/google/android/apps/gmm/l;->jy:I

    if-ne p1, v0, :cond_3

    .line 63
    sget-object v1, Lcom/google/b/f/t;->bW:Lcom/google/b/f/t;

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->c:Lcom/google/android/apps/gmm/prefetchcache/api/c;

    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/e;

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/gmm/l;->jq:I

    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v4, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    new-array v2, v9, [Ljava/lang/Object;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v5, v1, v4}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    iget-object v1, v5, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v4, v1, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v1, v5, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    aput-object v5, v2, v8

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v1

    const-string v2, "%s"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/google/android/apps/gmm/l;->jy:I

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->bg:I

    new-instance v3, Lcom/google/android/apps/gmm/prefetchcache/x;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/prefetchcache/x;-><init>(Lcom/google/android/apps/gmm/prefetchcache/v;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->jy:I

    new-instance v3, Lcom/google/android/apps/gmm/prefetchcache/w;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/gmm/prefetchcache/w;-><init>(Lcom/google/android/apps/gmm/prefetchcache/v;Lcom/google/android/apps/gmm/prefetchcache/api/c;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 66
    :cond_3
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/v;->b:Ljava/lang/String;

    const-string v2, "Clicked unknown menu item: "

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-array v2, v8, [Ljava/lang/Object;

    .line 66
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 67
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    sget v0, Lcom/google/android/apps/gmm/l;->jz:I

    .line 44
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->jA:I

    .line 45
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->jy:I

    .line 46
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 43
    invoke-static {v0, v1, v2}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method a(Lcom/google/b/f/t;)V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/v;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    .line 74
    invoke-static {p1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 73
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 75
    return-void
.end method
