.class public Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/support/v4/view/bz;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final a:I

.field public final b:I

.field public c:Landroid/support/v4/view/ViewPager;

.field d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/google/android/apps/gmm/directions/views/m;

.field public f:Lcom/google/android/apps/gmm/directions/views/l;

.field public g:Ljava/util/Calendar;

.field public h:I

.field public i:I

.field private j:Landroid/widget/ImageButton;

.field private k:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 121
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 125
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 128
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/n;->ao:[I

    invoke-virtual {v0, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 132
    sget v3, Lcom/google/android/apps/gmm/n;->ap:I

    const/16 v4, 0x1e

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a:I

    .line 133
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 136
    const/4 v0, 0x3

    iget v3, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a:I

    if-gt v0, v3, :cond_0

    move v0, v1

    :goto_0
    iget v3, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a:I

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x50

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "numberOfPageBuffers should be greater than or equal to 3. But it was "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 141
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    .line 142
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->h:I

    .line 143
    iput v2, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->i:I

    .line 144
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    .line 145
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a:I

    if-ltz v0, :cond_2

    :goto_1
    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->d:Ljava/util/ArrayList;

    move v1, v2

    .line 146
    :goto_2
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a:I

    if-ge v1, v0, :cond_4

    .line 147
    sget v0, Lcom/google/android/apps/gmm/h;->t:I

    const/4 v3, 0x0

    invoke-static {p1, v0, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 149
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 154
    :cond_4
    sget v0, Lcom/google/android/apps/gmm/h;->s:I

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 155
    sget v0, Lcom/google/android/apps/gmm/g;->R:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->c:Landroid/support/v4/view/ViewPager;

    .line 156
    sget v0, Lcom/google/android/apps/gmm/g;->co:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->j:Landroid/widget/ImageButton;

    .line 158
    sget v0, Lcom/google/android/apps/gmm/g;->aF:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->k:Landroid/widget/ImageButton;

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->c:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/google/android/apps/gmm/directions/views/k;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/directions/views/k;-><init>(Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/ag;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/bz;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->c:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 186
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a()V

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->j:Landroid/widget/ImageButton;

    invoke-static {v0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/widget/ImageView;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->k:Landroid/widget/ImageButton;

    invoke-static {v0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/widget/ImageView;)V

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->j:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->k:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    return-void
.end method

.method private a(Ljava/util/Calendar;Landroid/widget/TextView;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 327
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x18016

    invoke-static {v0, p1, v2, v1}, Lcom/google/android/apps/gmm/shared/c/c/b;->a(Landroid/content/Context;Ljava/util/Calendar;ZI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 328
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x16

    invoke-static {v0, p1, v2, v1}, Lcom/google/android/apps/gmm/shared/c/c/b;->b(Landroid/content/Context;Ljava/util/Calendar;ZI)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 329
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 333
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->d:Ljava/util/ArrayList;

    iget v3, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a(Ljava/util/Calendar;Landroid/widget/TextView;)V

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->c:Landroid/support/v4/view/ViewPager;

    iget v2, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 337
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->h:I

    .line 338
    iput v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->i:I

    move v2, v1

    .line 340
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a:I

    if-ge v2, v0, :cond_1

    .line 342
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    if-eq v2, v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 347
    const/4 v1, 0x5

    iget v3, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    sub-int v3, v2, v3

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->add(II)V

    .line 348
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a(Ljava/util/Calendar;Landroid/widget/TextView;)V

    .line 340
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 352
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->invalidate()V

    .line 353
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 216
    iput p1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->h:I

    .line 217
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->i:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->h:I

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->h:I

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b()V

    .line 218
    :cond_0
    return-void
.end method

.method public final a(IF)V
    .locals 0

    .prologue
    .line 223
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->f:Lcom/google/android/apps/gmm/directions/views/l;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->f:Lcom/google/android/apps/gmm/directions/views/l;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    const/4 v4, 0x5

    .line 365
    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 364
    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/directions/views/l;->a(III)V

    .line 367
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 210
    iput p1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->i:I

    .line 211
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->i:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->h:I

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->h:I

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b()V

    .line 212
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x2

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->j:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    .line 200
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->i:I

    if-ne v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->h:I

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    invoke-virtual {v1, v4, v0}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->c:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 206
    :cond_1
    :goto_0
    return-void

    .line 201
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->k:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_4

    .line 202
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->i:I

    if-ne v0, v3, :cond_3

    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->h:I

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    invoke-virtual {v1, v4, v0}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->c:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 204
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->e:Lcom/google/android/apps/gmm/directions/views/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->e:Lcom/google/android/apps/gmm/directions/views/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/directions/views/m;->a_(III)V

    goto :goto_0
.end method

.method public final setDate(III)V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-ne p3, v0, :cond_0

    .line 240
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    .line 238
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a()V

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b()V

    goto :goto_0
.end method
