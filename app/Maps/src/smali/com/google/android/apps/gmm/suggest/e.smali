.class public Lcom/google/android/apps/gmm/suggest/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/suggest/a/b;
.implements Lcom/google/android/apps/gmm/suggest/n;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/base/a;

.field private c:Lcom/google/android/apps/gmm/shared/net/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/n",
            "<",
            "Lcom/google/android/apps/gmm/shared/net/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/android/apps/gmm/suggest/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/suggest/e;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;)V
    .locals 5

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/apps/gmm/suggest/e;->b:Lcom/google/android/apps/gmm/base/a;

    .line 47
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/n;

    const/16 v1, 0x64

    .line 48
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v2

    .line 49
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/n;-><init>(ILcom/google/android/apps/gmm/shared/net/r;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/shared/c/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/e;->c:Lcom/google/android/apps/gmm/shared/net/n;

    .line 50
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/android/apps/gmm/suggest/e/a;Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/suggest/e/b;Lcom/google/android/apps/gmm/suggest/d/g;)Lcom/google/android/apps/gmm/shared/net/af;
    .locals 11
    .param p3    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/map/b/a/r;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/android/apps/gmm/suggest/e/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 35
    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p3, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    sget-object v1, Lcom/google/maps/a/a/a;->a:Lcom/google/e/a/a/a/d;

    invoke-virtual {v0, v1}, Lcom/google/e/a/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/e;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l_()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual/range {p6 .. p6}, Lcom/google/android/apps/gmm/suggest/d/g;->a()V

    invoke-virtual/range {p6 .. p6}, Lcom/google/android/apps/gmm/suggest/d/g;->c()V

    sget-object v0, Lcom/google/android/apps/gmm/suggest/e;->a:Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_2
    if-nez p1, :cond_3

    const/4 v10, 0x2

    :goto_1
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "MapsSuggestClientId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/google/android/apps/gmm/suggest/f;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/suggest/e/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lcom/google/android/apps/gmm/suggest/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/e;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/e;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v1

    if-nez v1, :cond_5

    const/4 v8, 0x0

    :goto_2
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p5

    move-object v5, p0

    move-object/from16 v6, p6

    move-object v9, p4

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/gmm/suggest/c;-><init>(Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/android/apps/gmm/suggest/e/a;Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/suggest/e/b;Lcom/google/android/apps/gmm/suggest/n;Lcom/google/android/apps/gmm/suggest/d/g;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/b/a/r;I)V

    :goto_3
    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/o;->a:Lcom/google/android/apps/gmm/suggest/e/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/suggest/e/a;->a:Ljava/lang/String;

    if-nez v1, :cond_6

    const/4 v1, 0x0

    :goto_4
    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/e;->c:Lcom/google/android/apps/gmm/shared/net/n;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/net/n;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/suggest/f;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/suggest/e/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    const/4 v10, 0x2

    goto :goto_1

    :pswitch_0
    const/16 v10, 0x1a

    goto :goto_1

    :pswitch_1
    const/16 v10, 0x1b

    goto :goto_1

    :pswitch_2
    const/16 v10, 0x1c

    goto :goto_1

    :pswitch_3
    const/16 v10, 0x1d

    goto :goto_1

    :pswitch_4
    new-instance v0, Lcom/google/android/apps/gmm/suggest/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/e;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/e;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v1

    if-nez v1, :cond_4

    const/4 v6, 0x0

    :goto_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/e;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->o()Lcom/google/r/b/a/apq;

    move-result-object v1

    iget v8, v1, Lcom/google/r/b/a/apq;->d:I

    move-object v1, p1

    move-object v2, p2

    move-object v3, p0

    move-object/from16 v4, p6

    move-object v7, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/suggest/a;-><init>(Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/android/apps/gmm/suggest/e/a;Lcom/google/android/apps/gmm/suggest/n;Lcom/google/android/apps/gmm/suggest/d/g;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/b/a/r;I)V

    goto :goto_3

    :cond_4
    invoke-interface {v1}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v6

    goto :goto_5

    :cond_5
    invoke-interface {v1}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v8

    goto :goto_2

    :cond_6
    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/o;->a:Lcom/google/android/apps/gmm/suggest/e/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/suggest/e/a;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x64

    if-le v1, v2, :cond_7

    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/o;->a:Lcom/google/android/apps/gmm/suggest/e/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/suggest/e/a;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x4c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "shouldn\'t send suggest request because query is too long: length="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    goto :goto_4

    :cond_7
    const/4 v1, 0x1

    goto :goto_4

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/suggest/m;)V
    .locals 6

    .prologue
    .line 146
    invoke-interface {p1}, Lcom/google/android/apps/gmm/suggest/m;->e()I

    move-result v0

    .line 147
    if-ltz v0, :cond_0

    .line 148
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/e;->c:Lcom/google/android/apps/gmm/shared/net/n;

    iget-object v2, v1, Lcom/google/android/apps/gmm/shared/net/n;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iput v0, v1, Lcom/google/android/apps/gmm/shared/net/n;->b:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    sget-object v1, Lcom/google/android/apps/gmm/suggest/e;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Suggest request interval has been updated to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/e;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/suggest/b/a;

    .line 153
    invoke-interface {p1}, Lcom/google/android/apps/gmm/suggest/m;->b()Lcom/google/android/apps/gmm/suggest/e/c;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/apps/gmm/suggest/m;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/apps/gmm/suggest/m;->d()Lcom/google/b/c/cv;

    move-result-object v4

    .line 154
    invoke-interface {p1}, Lcom/google/android/apps/gmm/suggest/m;->aj_()Lcom/google/android/apps/gmm/suggest/d/g;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/suggest/b/a;-><init>(Lcom/google/android/apps/gmm/suggest/e/c;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/gmm/suggest/d/g;)V

    .line 152
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 155
    return-void

    .line 148
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(Lcom/google/android/apps/gmm/suggest/m;)V
    .locals 4

    .prologue
    .line 159
    invoke-interface {p1}, Lcom/google/android/apps/gmm/suggest/m;->aj_()Lcom/google/android/apps/gmm/suggest/d/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/g;->c()V

    .line 161
    const-string v0, "Suggest request failed: [%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/suggest/m;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 162
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e;->a:Ljava/lang/String;

    .line 163
    return-void
.end method
