.class public Lcom/google/android/apps/gmm/navigation/navui/t;
.super Lcom/google/android/apps/gmm/navigation/navui/r;
.source "PG"


# instance fields
.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/navigation/c/h;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/directions/a/d;)V
    .locals 5

    .prologue
    const/high16 v4, 0x42400000    # 48.0f

    const/high16 v3, 0x3f000000    # 0.5f

    .line 305
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/gmm/navigation/navui/r;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/navigation/c/h;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/directions/a/d;)V

    .line 307
    invoke-interface {p3}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 309
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 310
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    .line 313
    const/high16 v2, 0x43080000    # 136.0f

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/apps/gmm/navigation/navui/t;->f:I

    .line 316
    mul-float v2, v4, v1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/apps/gmm/navigation/navui/t;->g:I

    .line 320
    const/high16 v2, 0x42b00000    # 88.0f

    mul-float/2addr v1, v2

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/gmm/navigation/navui/t;->h:I

    .line 322
    const/high16 v1, 0x42880000    # 68.0f

    mul-float/2addr v1, v0

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/gmm/navigation/navui/t;->i:I

    .line 324
    mul-float/2addr v0, v4

    add-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/navui/t;->j:I

    .line 326
    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/t;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    .line 348
    :goto_0
    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v0, v3, :cond_3

    :cond_0
    move v0, v2

    .line 350
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v3

    .line 351
    iget-boolean v4, v3, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    .line 352
    iget-boolean v5, v3, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    .line 355
    if-eqz v4, :cond_5

    .line 356
    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/t;->j:I

    :goto_2
    move v3, v0

    .line 362
    :goto_3
    if-eqz v5, :cond_7

    .line 363
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/t;->h:I

    .line 370
    :goto_4
    new-instance v4, Lcom/google/android/apps/gmm/directions/f/a/c;

    invoke-direct {v4, v1, v1, v0, v3}, Lcom/google/android/apps/gmm/directions/f/a/c;-><init>(IIII)V

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/t;->d:Lcom/google/android/apps/gmm/navigation/c/h;

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/c/a;->f:Lcom/google/android/apps/gmm/directions/f/a/c;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/gmm/directions/f/a/c;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    iput-object v4, v0, Lcom/google/android/apps/gmm/navigation/c/a;->f:Lcom/google/android/apps/gmm/directions/f/a/c;

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/apps/gmm/navigation/c/a;->a(ZZZ)V

    .line 372
    :cond_1
    return-void

    .line 346
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/t;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    .line 347
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    goto :goto_0

    :cond_3
    move v0, v1

    .line 348
    goto :goto_1

    :cond_4
    move v0, v1

    .line 356
    goto :goto_2

    .line 358
    :cond_5
    if-eqz v0, :cond_6

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/t;->i:I

    :goto_5
    move v3, v0

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_5

    .line 364
    :cond_7
    if-eqz v4, :cond_8

    .line 365
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/t;->g:I

    goto :goto_4

    .line 367
    :cond_8
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/t;->f:I

    goto :goto_4
.end method


# virtual methods
.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 330
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/navigation/navui/r;->a(Landroid/content/res/Configuration;)V

    .line 331
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/t;->a(Z)V

    .line 332
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V
    .locals 2

    .prologue
    .line 337
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/navigation/navui/r;->a(Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V

    .line 338
    if-eqz p2, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    iget-object v1, p2, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v0, v1, :cond_1

    .line 339
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/t;->a(Z)V

    .line 341
    :cond_1
    return-void
.end method
