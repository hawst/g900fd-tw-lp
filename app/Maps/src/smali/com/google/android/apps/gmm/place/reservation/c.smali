.class public Lcom/google/android/apps/gmm/place/reservation/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/reservation/b/b;


# instance fields
.field a:Ljava/util/Calendar;

.field private b:Lcom/google/android/apps/gmm/place/reservation/f;

.field private c:Ljava/util/Date;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;)V
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/f;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/place/reservation/f;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/apps/gmm/place/reservation/c;-><init>(Lcom/google/android/apps/gmm/place/reservation/f;Ljava/util/Date;Ljava/util/Date;)V

    .line 77
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/place/reservation/f;Ljava/util/Date;Ljava/util/Date;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/place/reservation/f;

    iput-object p1, p0, Lcom/google/android/apps/gmm/place/reservation/c;->b:Lcom/google/android/apps/gmm/place/reservation/f;

    .line 82
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    .line 83
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, p2

    check-cast v0, Ljava/util/Date;

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/16 v0, 0xb

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xc

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xd

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xe

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->c:Ljava/util/Date;

    .line 84
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 86
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->c:Ljava/util/Date;

    invoke-virtual {p3, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->c:Ljava/util/Date;

    .line 87
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long v0, v2, v0

    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v0

    long-to-int v0, v0

    const/16 v1, 0x3c

    if-lt v0, v1, :cond_4

    .line 88
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    invoke-virtual {v0, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 92
    :goto_0
    return-void

    .line 90
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    invoke-virtual {v0, p3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->b:Lcom/google/android/apps/gmm/place/reservation/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/reservation/f;->a:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const v1, 0x80013

    invoke-static {v0, v2, v3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 114
    const/16 v0, 0x2f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 119
    const/16 v0, 0x3b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/Integer;
    .locals 5

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->c:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long v0, v2, v0

    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Integer;
    .locals 6

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 137
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 138
    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    long-to-int v0, v2

    add-int/2addr v0, v1

    .line 139
    div-int/lit8 v0, v0, 0x1e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/util/Date;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public final h()Landroid/widget/NumberPicker$OnValueChangeListener;
    .locals 1

    .prologue
    .line 149
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/reservation/d;-><init>(Lcom/google/android/apps/gmm/place/reservation/c;)V

    return-object v0
.end method

.method public final i()Landroid/widget/NumberPicker$OnValueChangeListener;
    .locals 1

    .prologue
    .line 160
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/reservation/e;-><init>(Lcom/google/android/apps/gmm/place/reservation/c;)V

    return-object v0
.end method

.method public final j()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v7, 0x3c

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->d:Ljava/util/List;

    if-nez v0, :cond_1

    .line 176
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->d:Ljava/util/List;

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/c;->c:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 181
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v7, :cond_0

    .line 182
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/c;->d:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/c;->b:Lcom/google/android/apps/gmm/place/reservation/f;

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    iget-object v3, v3, Lcom/google/android/apps/gmm/place/reservation/f;->a:Landroid/content/Context;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    const v6, 0x80012

    invoke-static {v3, v4, v5, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    const/4 v3, 0x6

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 181
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->d:Ljava/util/List;

    return-object v0
.end method

.method public final k()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v7, 0x30

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->e:Ljava/util/List;

    if-nez v0, :cond_1

    .line 193
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->e:Ljava/util/List;

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/c;->c:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 198
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v7, :cond_0

    .line 199
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/c;->e:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/c;->b:Lcom/google/android/apps/gmm/place/reservation/f;

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    iget-object v3, v3, Lcom/google/android/apps/gmm/place/reservation/f;->a:Landroid/content/Context;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    const/16 v3, 0xc

    const/16 v4, 0x1e

    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 198
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->a:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/c;->e:Ljava/util/List;

    return-object v0
.end method
