.class public Lcom/google/android/apps/gmm/base/l/ac;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/y;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/google/android/apps/gmm/base/activities/c;

.field public final c:Lcom/google/j/d/a/e;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/x/o;Lcom/google/j/d/a/e;)V
    .locals 0
    .param p2    # Lcom/google/android/apps/gmm/x/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/j/d/a/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/activities/c;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/j/d/a/e;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 29
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/l/ac;->a:Lcom/google/android/apps/gmm/x/o;

    .line 30
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/l/ac;->c:Lcom/google/j/d/a/e;

    .line 31
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ac;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ac;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    if-nez v0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return-object v4

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v1

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ac;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ac;->c:Lcom/google/j/d/a/e;

    if-eqz v0, :cond_2

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ac;->c:Lcom/google/j/d/a/e;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/j/d/a/e;)V

    .line 63
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ac;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v2, Lcom/google/j/d/a/w;->b:Lcom/google/j/d/a/w;

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/l/ac;->b()Lcom/google/b/f/t;

    move-result-object v3

    .line 63
    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ac;->a:Lcom/google/android/apps/gmm/x/o;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/l/ac;->c:Lcom/google/j/d/a/e;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/j/d/a/e;)V

    goto :goto_0
.end method

.method public b()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/google/b/f/t;->cR:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final synthetic c()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ac;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->j:Lcom/google/android/apps/gmm/base/g/e;

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/g/c;->a(Lcom/google/android/apps/gmm/base/g/c;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->G()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ac;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 83
    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x0

    .line 88
    :goto_0
    return-object v0

    .line 86
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/l/ac;->b()Lcom/google/b/f/t;

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 88
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return-object v0
.end method
