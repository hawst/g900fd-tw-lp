.class public Lcom/google/android/apps/gmm/startpage/d/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/o/h/a/hb;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/google/o/h/a/gx;

.field public final d:Lcom/google/android/apps/gmm/startpage/d/e;


# direct methods
.method public constructor <init>(Lcom/google/o/h/a/hb;Ljava/lang/String;Lcom/google/o/h/a/gx;Lcom/google/android/apps/gmm/startpage/d/e;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/d/a;->a:Lcom/google/o/h/a/hb;

    .line 19
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/d/a;->b:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/d/a;->c:Lcom/google/o/h/a/gx;

    .line 21
    iput-object p4, p0, Lcom/google/android/apps/gmm/startpage/d/a;->d:Lcom/google/android/apps/gmm/startpage/d/e;

    .line 22
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 47
    instance-of v2, p1, Lcom/google/android/apps/gmm/startpage/d/a;

    if-nez v2, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v0

    .line 50
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/startpage/d/a;

    .line 51
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/d/a;->a:Lcom/google/o/h/a/hb;

    iget-object v3, p1, Lcom/google/android/apps/gmm/startpage/d/a;->a:Lcom/google/o/h/a/hb;

    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_2
    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/d/a;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/startpage/d/a;->b:Ljava/lang/String;

    .line 52
    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_3
    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/d/a;->c:Lcom/google/o/h/a/gx;

    iget-object v3, p1, Lcom/google/android/apps/gmm/startpage/d/a;->c:Lcom/google/o/h/a/gx;

    .line 53
    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_8

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_4
    move v2, v1

    :goto_3
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/d/a;->d:Lcom/google/android/apps/gmm/startpage/d/e;

    iget-object v3, p1, Lcom/google/android/apps/gmm/startpage/d/a;->d:Lcom/google/android/apps/gmm/startpage/d/e;

    .line 54
    if-eq v2, v3, :cond_5

    if-eqz v2, :cond_9

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_5
    move v2, v1

    :goto_4
    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_6
    move v2, v0

    .line 51
    goto :goto_1

    :cond_7
    move v2, v0

    .line 52
    goto :goto_2

    :cond_8
    move v2, v0

    .line 53
    goto :goto_3

    :cond_9
    move v2, v0

    .line 54
    goto :goto_4
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/d/a;->a:Lcom/google/o/h/a/hb;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/d/a;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/d/a;->c:Lcom/google/o/h/a/gx;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/d/a;->d:Lcom/google/android/apps/gmm/startpage/d/e;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
