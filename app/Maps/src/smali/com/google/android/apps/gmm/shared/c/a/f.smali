.class public Lcom/google/android/apps/gmm/shared/c/a/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# instance fields
.field final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field final b:Ljava/lang/String;

.field final c:Lcom/google/android/apps/gmm/map/c/a/a;

.field final d:Lcom/google/android/apps/gmm/shared/c/a/p;

.field final e:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;)V
    .locals 1
    .param p3    # Lcom/google/android/apps/gmm/map/c/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 119
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/shared/c/a/f;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;I)V

    .line 120
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;I)V
    .locals 2
    .param p3    # Lcom/google/android/apps/gmm/map/c/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/f;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 129
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/c/a/f;->b:Ljava/lang/String;

    .line 130
    iput-object p3, p0, Lcom/google/android/apps/gmm/shared/c/a/f;->c:Lcom/google/android/apps/gmm/map/c/a/a;

    .line 131
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/c/a/f;->d:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 132
    iput p4, p0, Lcom/google/android/apps/gmm/shared/c/a/f;->e:I

    .line 133
    return-void
.end method


# virtual methods
.method public newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 6

    .prologue
    .line 137
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/a/f;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/c/a/f;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 138
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "-pool-thread-"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/c/a/f;->d:Lcom/google/android/apps/gmm/shared/c/a/p;

    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/c/a/f;->c:Lcom/google/android/apps/gmm/map/c/a/a;

    iget v5, p0, Lcom/google/android/apps/gmm/shared/c/a/f;->e:I

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/shared/c/a/d;-><init>(Ljava/lang/Runnable;Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;I)V

    .line 142
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/d;->setDaemon(Z)V

    .line 143
    return-object v0
.end method
