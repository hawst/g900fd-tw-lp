.class public Lcom/google/android/apps/gmm/p/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static final a:Lcom/google/android/apps/gmm/shared/c/a/p;

.field private static final f:Landroid/net/Uri;

.field private static final g:[Ljava/lang/String;

.field private static final h:[Ljava/lang/String;


# instance fields
.field public final b:Lcom/google/android/apps/gmm/base/a;

.field public c:Z

.field volatile d:Z

.field public volatile e:Landroid/location/LocationManager;

.field private final i:Lcom/google/android/apps/gmm/p/b/b;

.field private final j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    const-string v0, "content://com.google.settings/partner"

    .line 39
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/p/d;->f:Landroid/net/Uri;

    .line 41
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "value"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/gmm/p/d;->g:[Ljava/lang/String;

    .line 42
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "use_location_for_services"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/gmm/p/d;->h:[Ljava/lang/String;

    .line 44
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    sput-object v0, Lcom/google/android/apps/gmm/p/d;->a:Lcom/google/android/apps/gmm/shared/c/a/p;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/p/b/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/p/b/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/d;->i:Lcom/google/android/apps/gmm/p/b/b;

    .line 58
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/d;->b:Lcom/google/android/apps/gmm/base/a;

    .line 59
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.telephony"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/d;->j:Z

    .line 60
    return-void
.end method

.method private a()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 140
    :try_start_0
    sget-object v1, Lcom/google/android/apps/gmm/p/d;->f:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/gmm/p/d;->g:[Ljava/lang/String;

    const-string v3, "name=?"

    sget-object v4, Lcom/google/android/apps/gmm/p/d;->h:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 142
    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 143
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    move-object v0, v6

    .line 148
    :goto_0
    if-eqz v1, :cond_0

    .line 149
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 152
    :cond_0
    :goto_1
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 146
    :catch_0
    move-exception v0

    move-object v0, v6

    :goto_2
    if-eqz v0, :cond_2

    .line 149
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_1

    .line 148
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v6, :cond_1

    .line 149
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 148
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_3

    .line 146
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_2

    :cond_2
    move-object v0, v6

    goto :goto_1

    :cond_3
    move-object v0, v6

    goto :goto_0
.end method

.method private b()Lcom/google/android/apps/gmm/p/b/d;
    .locals 2

    .prologue
    .line 158
    :try_start_0
    const-string v0, "gps"

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/d;->e:Landroid/location/LocationManager;

    invoke-virtual {v1}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d;->e:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    .line 165
    :goto_1
    return-object v0

    .line 158
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 159
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->d:Lcom/google/android/apps/gmm/p/b/d;

    goto :goto_1

    .line 163
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->b:Lcom/google/android/apps/gmm/p/b/d;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 165
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->a:Lcom/google/android/apps/gmm/p/b/d;

    goto :goto_1
.end method

.method private c()Lcom/google/android/apps/gmm/p/b/d;
    .locals 2

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/d;->j:Z

    if-nez v0, :cond_0

    .line 172
    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->b:Lcom/google/android/apps/gmm/p/b/d;

    .line 183
    :goto_0
    return-object v0

    .line 176
    :cond_0
    :try_start_0
    const-string v0, "network"

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/d;->e:Landroid/location/LocationManager;

    invoke-virtual {v1}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d;->e:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    goto :goto_0

    .line 176
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 177
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->d:Lcom/google/android/apps/gmm/p/b/d;

    goto :goto_0

    .line 181
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->b:Lcom/google/android/apps/gmm/p/b/d;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 183
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->a:Lcom/google/android/apps/gmm/p/b/d;

    goto :goto_0
.end method

.method private d()Lcom/google/android/apps/gmm/p/b/d;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 200
    :try_start_0
    const-string v0, "network"

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/d;->e:Landroid/location/LocationManager;

    invoke-virtual {v2}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d;->e:Landroid/location/LocationManager;

    const-string v2, "network"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 202
    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->d:Lcom/google/android/apps/gmm/p/b/d;

    .line 226
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 200
    goto :goto_0

    .line 205
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->b:Lcom/google/android/apps/gmm/p/b/d;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 208
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->a:Lcom/google/android/apps/gmm/p/b/d;

    goto :goto_1

    .line 211
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d;->b:Lcom/google/android/apps/gmm/base/a;

    .line 212
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "wifi"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 213
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v2

    .line 214
    const/4 v3, 0x3

    if-eq v2, v3, :cond_3

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 215
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    goto :goto_1

    .line 220
    :cond_4
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 221
    const-string v3, "isScanAlwaysAvailable"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 222
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 226
    :goto_2
    if-eqz v0, :cond_5

    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    goto :goto_1

    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_2

    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->d:Lcom/google/android/apps/gmm/p/b/d;

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 92
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/d;->d:Z

    if-eqz v0, :cond_4

    .line 96
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/d;->c:Z

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/d;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 97
    sget-object v2, Lcom/google/android/apps/gmm/p/b/d;->e:Lcom/google/android/apps/gmm/p/b/d;

    .line 98
    sget-object v1, Lcom/google/android/apps/gmm/p/b/d;->e:Lcom/google/android/apps/gmm/p/b/d;

    .line 99
    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->e:Lcom/google/android/apps/gmm/p/b/d;

    .line 105
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/p/d;->i:Lcom/google/android/apps/gmm/p/b/b;

    iget-object v6, v3, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v7, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-eq v6, v7, :cond_0

    iget-object v3, v3, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v6, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v3, v6, :cond_6

    :cond_0
    move v3, v5

    .line 106
    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/gmm/p/d;->i:Lcom/google/android/apps/gmm/p/b/b;

    monitor-enter v6

    .line 107
    :try_start_0
    iget-object v7, p0, Lcom/google/android/apps/gmm/p/d;->i:Lcom/google/android/apps/gmm/p/b/b;

    iput-object v2, v7, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    .line 108
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/d;->i:Lcom/google/android/apps/gmm/p/b/b;

    iput-object v1, v2, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    .line 109
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/d;->i:Lcom/google/android/apps/gmm/p/b/b;

    iput-object v0, v1, Lcom/google/android/apps/gmm/p/b/b;->c:Lcom/google/android/apps/gmm/p/b/d;

    .line 110
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d;->i:Lcom/google/android/apps/gmm/p/b/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v2, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-eq v1, v2, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v1, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v0, v1, :cond_2

    :cond_1
    move v4, v5

    :cond_2
    if-nez v4, :cond_3

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/location/a;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/location/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 116
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/p/b/f;

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/d;->i:Lcom/google/android/apps/gmm/p/b/b;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/p/b/f;-><init>(Lcom/google/android/apps/gmm/p/b/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/p/d;->a:Lcom/google/android/apps/gmm/shared/c/a/p;

    const-wide/16 v2, 0x1388

    invoke-interface {v0, p0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 119
    :cond_4
    return-void

    .line 101
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/d;->b()Lcom/google/android/apps/gmm/p/b/d;

    move-result-object v2

    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/d;->c()Lcom/google/android/apps/gmm/p/b/d;

    move-result-object v1

    .line 103
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/d;->d()Lcom/google/android/apps/gmm/p/b/d;

    move-result-object v0

    goto :goto_0

    :cond_6
    move v3, v4

    .line 105
    goto :goto_1

    .line 110
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
