.class public Lcom/google/android/apps/gmm/navigation/f/c;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/commonui/c;
.implements Lcom/google/android/apps/gmm/navigation/f/q;


# instance fields
.field a:Lcom/google/android/apps/gmm/navigation/f/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field b:Lcom/google/android/libraries/curvular/ae;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/navigation/commonui/c/l;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/android/libraries/curvular/ae;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/navigation/commonui/c/e;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/android/libraries/curvular/ae;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/navigation/f/c/a;",
            ">;"
        }
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/navigation/f/b/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private f:Lcom/google/android/apps/gmm/navigation/commonui/l;

.field private g:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 320
    new-instance v0, Lcom/google/android/apps/gmm/navigation/f/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/f/h;-><init>(Lcom/google/android/apps/gmm/navigation/f/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->g:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 196
    new-instance v1, Lcom/google/android/apps/gmm/navigation/f/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/navigation/f/e;-><init>(Lcom/google/android/apps/gmm/navigation/f/c;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/navigation/f/g;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/navigation/f/g;-><init>(Lcom/google/android/apps/gmm/navigation/f/c;Ljava/lang/Runnable;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 204
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 255
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/f/b/a;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 247
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/f/c;->e:Lcom/google/android/apps/gmm/navigation/f/b/a;

    .line 248
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/f/b/a;->e:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/f/c;->e:Lcom/google/android/apps/gmm/navigation/f/b/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/f/b/a;->e:Lcom/google/android/apps/gmm/navigation/g/b/d;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/d;->d:Lcom/google/maps/g/a/hm;

    new-instance v4, Lcom/google/android/apps/gmm/base/activities/x;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/base/activities/x;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/f/c;->b:Lcom/google/android/libraries/curvular/ae;

    iget-object v5, v5, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v5, v4, Lcom/google/android/apps/gmm/base/activities/x;->a:Landroid/view/View;

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/activities/x;->b:Ljava/lang/Runnable;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/activities/x;->b:Ljava/lang/Runnable;

    invoke-interface {v5}, Ljava/lang/Runnable;->run()V

    :cond_0
    new-instance v5, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    iget-object v6, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v1, v6, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    iget-object v6, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v8, v6, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v6, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v6, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v6, v7, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    iget-object v6, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v1, v6, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    iget-object v6, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v0, v6, Lcom/google/android/apps/gmm/base/activities/p;->D:Z

    iget-object v0, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    iget-object v0, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->b:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iget-object v2, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iget-object v0, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v2, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->f:Lcom/google/android/apps/gmm/navigation/commonui/l;

    iget-object v2, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->a:Lcom/google/android/apps/gmm/navigation/f/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/f/b;->c:Lcom/google/android/apps/gmm/navigation/f/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/d/a;->d()Lcom/google/android/apps/gmm/map/b/a/f;

    move-result-object v0

    iget-object v2, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    iget-object v0, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v4, v0, Lcom/google/android/apps/gmm/base/activities/p;->F:Lcom/google/android/apps/gmm/base/activities/x;

    invoke-static {v3}, Lcom/google/android/apps/gmm/base/activities/ag;->a(Lcom/google/maps/g/a/hm;)Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v0

    iget-object v2, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iget-object v2, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v8, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eqz v0, :cond_4

    :goto_2
    iget-object v2, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->d:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    sget v2, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->a:I

    invoke-virtual {v5, v0, v2}, Lcom/google/android/apps/gmm/base/activities/w;->a(Landroid/view/View;I)Lcom/google/android/apps/gmm/base/activities/w;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/f/c;->a:Lcom/google/android/apps/gmm/navigation/f/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/f/b;->d:Lcom/google/android/apps/gmm/navigation/f/c/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/f/c/a;->l()Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/activities/p;->m:Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/f/c;->c:Lcom/google/android/libraries/curvular/ae;

    iget-object v2, v2, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/activities/p;->C:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->f:Lcom/google/android/apps/gmm/navigation/commonui/l;

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/commonui/l;->a:Z

    .line 251
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 248
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 249
    goto/16 :goto_1

    :cond_4
    move-object v0, v2

    goto :goto_2
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/f/c;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 238
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->f:Lcom/google/android/apps/gmm/navigation/commonui/l;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/navigation/commonui/l;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(ZZ)V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->O()Lcom/google/android/apps/gmm/aa/c/a/a;

    move-result-object v0

    .line 209
    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/aa/c/a/a;->a(ZZ)V

    .line 210
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/f/c;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/c;->a()V

    .line 217
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->b()V

    .line 224
    new-instance v1, Lcom/google/android/apps/gmm/navigation/f/f;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/navigation/f/f;-><init>(Lcom/google/android/apps/gmm/navigation/f/c;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/navigation/f/g;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/navigation/f/g;-><init>(Lcom/google/android/apps/gmm/navigation/f/c;Ljava/lang/Runnable;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 230
    return-void
.end method

.method public final bridge synthetic d()Lcom/google/android/apps/gmm/navigation/commonui/f;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->a:Lcom/google/android/apps/gmm/navigation/f/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/f/b;->a:Lcom/google/android/apps/gmm/navigation/f/s;

    return-object v0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 183
    sget-object v0, Lcom/google/b/f/t;->bA:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final k()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    :cond_0
    :goto_0
    return v1

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->e:Lcom/google/android/apps/gmm/navigation/f/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v0, v2, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->a:Lcom/google/android/apps/gmm/navigation/f/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/f/b;->a:Lcom/google/android/apps/gmm/navigation/f/s;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/navigation/f/r;->a(Ljava/lang/Float;)V

    goto :goto_0

    .line 163
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 171
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/f/c;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/c;->a()V

    goto :goto_0
.end method

.method public final m()Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 1

    .prologue
    .line 188
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->g:Lcom/google/android/apps/gmm/feedback/a/d;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 21

    .prologue
    .line 96
    invoke-super/range {p0 .. p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 98
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/f/c;->a:Lcom/google/android/apps/gmm/navigation/f/b;

    if-nez v2, :cond_4

    .line 99
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v3, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    check-cast v2, Lcom/google/android/libraries/curvular/bd;

    .line 100
    const-class v3, Lcom/google/android/apps/gmm/navigation/f/a/b;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/f/c;->b:Lcom/google/android/libraries/curvular/ae;

    .line 101
    const-class v3, Lcom/google/android/apps/gmm/navigation/commonui/a/b;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/f/c;->c:Lcom/google/android/libraries/curvular/ae;

    .line 102
    const-class v3, Lcom/google/android/apps/gmm/navigation/f/a/a;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/f/c;->d:Lcom/google/android/libraries/curvular/ae;

    .line 104
    new-instance v17, Lcom/google/android/apps/gmm/navigation/f/d;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/navigation/f/d;-><init>(Lcom/google/android/apps/gmm/navigation/f/c;)V

    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/f/c;->d:Lcom/google/android/libraries/curvular/ae;

    .line 115
    iget-object v2, v2, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 114
    invoke-static {v2}, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->a(Landroid/view/View;)Ljava/lang/Runnable;

    move-result-object v18

    .line 118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    move-object/from16 v19, v0

    .line 117
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v3

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_1
    move-object/from16 v16, v2

    check-cast v16, Lcom/google/android/apps/gmm/base/a;

    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    if-nez v2, :cond_2

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_2
    move-object v11, v2

    check-cast v11, Lcom/google/android/apps/gmm/map/MapFragment;

    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_3
    move-object v13, v2

    check-cast v13, Lcom/google/android/apps/gmm/base/j/b;

    new-instance v10, Lcom/google/android/apps/gmm/navigation/f/s;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v3}, Lcom/google/android/apps/gmm/navigation/f/s;-><init>(Lcom/google/android/apps/gmm/navigation/f/q;Lcom/google/android/apps/gmm/map/util/b/g;)V

    move-object/from16 v0, v20

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/google/android/apps/gmm/navigation/f/a;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v7, v0, v1}, Lcom/google/android/apps/gmm/navigation/f/a;-><init>(Lcom/google/android/apps/gmm/navigation/f/q;Lcom/google/android/apps/gmm/base/a;)V

    move-object/from16 v0, v20

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/gmm/navigation/c/f;

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v5, v11, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-interface {v13}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/apps/gmm/mylocation/b/i;->d()Lcom/google/android/apps/gmm/mylocation/b/f;

    move-result-object v6

    const/4 v8, 0x1

    invoke-interface/range {v16 .. v16}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v9

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/gmm/navigation/c/f;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/mylocation/b/f;Lcom/google/android/apps/gmm/navigation/d/a;ZLcom/google/android/apps/gmm/shared/net/a/b;)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v8, Lcom/google/android/apps/gmm/navigation/f/k;

    invoke-interface/range {v16 .. v16}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v12

    iget-object v14, v11, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-interface {v13}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v15

    move-object/from16 v9, p0

    move-object v11, v3

    move-object v13, v2

    invoke-direct/range {v8 .. v15}, Lcom/google/android/apps/gmm/navigation/f/k;-><init>(Lcom/google/android/apps/gmm/navigation/f/q;Lcom/google/android/apps/gmm/navigation/f/r;Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/Context;Lcom/google/android/apps/gmm/navigation/c/f;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/o/a/c;)V

    move-object/from16 v0, v20

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v11, Lcom/google/android/apps/gmm/navigation/f/l;

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Lcom/google/android/apps/gmm/navigation/f/l;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    move-object/from16 v0, v20

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/google/android/apps/gmm/navigation/f/c/c;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/navigation/f/c/c;-><init>()V

    move-object/from16 v0, p0

    iput-object v0, v4, Lcom/google/android/apps/gmm/navigation/f/c/c;->a:Lcom/google/android/apps/gmm/navigation/f/q;

    iput-object v10, v4, Lcom/google/android/apps/gmm/navigation/f/c/c;->b:Lcom/google/android/apps/gmm/navigation/f/r;

    move-object/from16 v0, v16

    iput-object v0, v4, Lcom/google/android/apps/gmm/navigation/f/c/c;->c:Lcom/google/android/apps/gmm/base/a;

    sget v3, Lcom/google/android/apps/gmm/g;->ax:I

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iput-object v3, v4, Lcom/google/android/apps/gmm/navigation/f/c/c;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    move-object/from16 v0, v17

    iput-object v0, v4, Lcom/google/android/apps/gmm/navigation/f/c/c;->e:Lcom/google/android/libraries/curvular/ag;

    move-object/from16 v0, v18

    iput-object v0, v4, Lcom/google/android/apps/gmm/navigation/f/c/c;->f:Ljava/lang/Runnable;

    new-instance v14, Lcom/google/android/apps/gmm/navigation/f/c/b;

    invoke-direct {v14, v4}, Lcom/google/android/apps/gmm/navigation/f/c/b;-><init>(Lcom/google/android/apps/gmm/navigation/f/c/c;)V

    move-object/from16 v0, v20

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v8, Lcom/google/android/apps/gmm/navigation/f/b;

    move-object/from16 v9, v20

    move-object v12, v7

    move-object v13, v2

    invoke-direct/range {v8 .. v14}, Lcom/google/android/apps/gmm/navigation/f/b;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/navigation/f/s;Lcom/google/android/apps/gmm/navigation/f/l;Lcom/google/android/apps/gmm/navigation/f/a;Lcom/google/android/apps/gmm/navigation/c/f;Lcom/google/android/apps/gmm/navigation/f/c/b;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/apps/gmm/navigation/f/c;->a:Lcom/google/android/apps/gmm/navigation/f/b;

    .line 120
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/f/c;->a:Lcom/google/android/apps/gmm/navigation/f/b;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/navigation/f/b;->a(Landroid/os/Bundle;)V

    .line 122
    new-instance v3, Lcom/google/android/apps/gmm/navigation/f/m;

    .line 123
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v4, 0x3

    invoke-direct {v3, v2, v4}, Lcom/google/android/apps/gmm/navigation/f/m;-><init>(Lcom/google/android/apps/gmm/base/activities/c;I)V

    .line 124
    iget-object v2, v3, Lcom/google/android/apps/gmm/navigation/f/m;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->aT:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v4

    iget-object v2, v3, Lcom/google/android/apps/gmm/navigation/f/m;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    sget-object v5, Lcom/google/android/apps/gmm/shared/b/c;->aT:Lcom/google/android/apps/gmm/shared/b/c;

    add-int/lit8 v6, v4, 0x1

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_5
    add-int/lit8 v2, v4, 0x1

    iget v4, v3, Lcom/google/android/apps/gmm/navigation/f/m;->b:I

    if-le v2, v4, :cond_7

    iget-object v2, v3, Lcom/google/android/apps/gmm/navigation/f/m;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->aU:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, v3, Lcom/google/android/apps/gmm/navigation/f/m;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->aU:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v5, 0x1

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_6
    iget-object v2, v3, Lcom/google/android/apps/gmm/navigation/f/m;->c:Lcom/google/b/a/aa;

    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, v3, Lcom/google/android/apps/gmm/navigation/f/m;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, -0x21524111

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/l;->pg:I

    new-instance v6, Lcom/google/android/apps/gmm/navigation/f/p;

    invoke-direct {v6, v3}, Lcom/google/android/apps/gmm/navigation/f/p;-><init>(Lcom/google/android/apps/gmm/navigation/f/m;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/l;->iX:I

    new-instance v6, Lcom/google/android/apps/gmm/navigation/f/o;

    invoke-direct {v6, v3}, Lcom/google/android/apps/gmm/navigation/f/o;-><init>(Lcom/google/android/apps/gmm/navigation/f/m;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/b/a/aa;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    :cond_7
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 177
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->a:Lcom/google/android/apps/gmm/navigation/f/b;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/navigation/f/b;->a(Landroid/content/res/Configuration;)V

    .line 179
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 80
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 82
    new-instance v0, Lcom/google/android/apps/gmm/navigation/commonui/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/commonui/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->f:Lcom/google/android/apps/gmm/navigation/commonui/l;

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/commonui/i;

    .line 88
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    const-class v3, Lcom/google/android/apps/gmm/navigation/f/c;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/navigation/commonui/i;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/Object;)V

    .line 87
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Lcom/google/android/apps/gmm/base/activities/n;)V

    .line 89
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 150
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->a:Lcom/google/android/apps/gmm/navigation/f/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/f/b;->G_()V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/f/c;->g:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 153
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 129
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->T:Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    .line 140
    if-eqz v0, :cond_0

    .line 141
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->e:Z

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/f/c;->g:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->a:Lcom/google/android/apps/gmm/navigation/f/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/f/b;->b()V

    .line 146
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c;->a:Lcom/google/android/apps/gmm/navigation/f/b;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/navigation/f/b;->b(Landroid/os/Bundle;)V

    .line 76
    return-void
.end method
