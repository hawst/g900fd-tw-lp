.class public Lcom/google/android/apps/gmm/navigation/base/a;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field a:Lcom/google/android/apps/gmm/base/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/base/a;->a:Lcom/google/android/apps/gmm/base/a;

    .line 28
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/navigation/g/a/b;)V
    .locals 5

    .prologue
    .line 54
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    .line 58
    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v2, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-eq v1, v2, :cond_0

    .line 67
    :goto_0
    return-void

    .line 62
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 63
    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v2, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/r/a/ae;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;)V

    sget-object v0, Lcom/google/android/apps/gmm/directions/f/c/e;->a:Lcom/google/android/apps/gmm/directions/f/c/e;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/base/a;->a:Lcom/google/android/apps/gmm/base/a;

    .line 65
    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/base/a;->a:Lcom/google/android/apps/gmm/base/a;

    .line 66
    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v4

    .line 62
    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/apps/gmm/directions/d/c;->a(Lcom/google/android/apps/gmm/map/r/a/ae;Lcom/google/android/apps/gmm/directions/f/c/e;JLcom/google/android/apps/gmm/shared/b/a;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/navigation/g/a/h;)V
    .locals 0
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/navigation/base/a;->a(Lcom/google/android/apps/gmm/navigation/g/a/b;)V

    .line 38
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/i;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/a;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/d/c;->a(Lcom/google/android/apps/gmm/shared/b/a;)V

    .line 49
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/o;)V
    .locals 0
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/navigation/base/a;->a(Lcom/google/android/apps/gmm/navigation/g/a/b;)V

    .line 43
    return-void
.end method
