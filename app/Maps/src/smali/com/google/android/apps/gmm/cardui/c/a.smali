.class public Lcom/google/android/apps/gmm/cardui/c/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/o/h/a/br;

.field final b:Lcom/google/o/h/a/lh;

.field public final c:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final d:Lcom/google/r/b/a/tf;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public e:Lcom/google/android/apps/gmm/cardui/e/c;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/startpage/d/b;Lcom/google/o/h/a/br;)V
    .locals 3

    .prologue
    .line 33
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/d/b;->a:Lcom/google/o/h/a/lh;

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/d/b;->b:Ljava/lang/String;

    .line 34
    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/d/b;->c:Lcom/google/r/b/a/tf;

    .line 33
    invoke-direct {p0, v0, p2, v1, v2}, Lcom/google/android/apps/gmm/cardui/c/a;-><init>(Lcom/google/o/h/a/lh;Lcom/google/o/h/a/br;Ljava/lang/String;Lcom/google/r/b/a/tf;)V

    .line 35
    return-void
.end method

.method private constructor <init>(Lcom/google/o/h/a/lh;Lcom/google/o/h/a/br;Ljava/lang/String;Lcom/google/r/b/a/tf;)V
    .locals 0
    .param p4    # Lcom/google/r/b/a/tf;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/apps/gmm/cardui/c/a;->b:Lcom/google/o/h/a/lh;

    .line 43
    iput-object p2, p0, Lcom/google/android/apps/gmm/cardui/c/a;->a:Lcom/google/o/h/a/br;

    .line 44
    iput-object p3, p0, Lcom/google/android/apps/gmm/cardui/c/a;->c:Ljava/lang/String;

    .line 45
    iput-object p4, p0, Lcom/google/android/apps/gmm/cardui/c/a;->d:Lcom/google/r/b/a/tf;

    .line 46
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/startpage/a/c;)Ljava/util/List;
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/startpage/a/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/startpage/a/c;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/a;->a:Lcom/google/o/h/a/br;

    invoke-virtual {v0}, Lcom/google/o/h/a/br;->d()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/c/a;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/c/a;->d:Lcom/google/r/b/a/tf;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/util/b/m;->a(Ljava/util/List;Ljava/lang/String;Lcom/google/r/b/a/tf;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/cardui/c/a;Ljava/lang/String;Lcom/google/r/b/a/tf;)V
    .locals 4
    .param p3    # Lcom/google/r/b/a/tf;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 71
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 72
    iget-object v0, p1, Lcom/google/android/apps/gmm/cardui/c/a;->a:Lcom/google/o/h/a/br;

    invoke-virtual {v0}, Lcom/google/o/h/a/br;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    .line 73
    new-instance v3, Lcom/google/android/apps/gmm/util/b/m;

    invoke-direct {v3, v0, p2, p3}, Lcom/google/android/apps/gmm/util/b/m;-><init>(Lcom/google/o/h/a/iv;Ljava/lang/String;Lcom/google/r/b/a/tf;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/a;->e:Lcom/google/android/apps/gmm/cardui/e/c;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/cardui/e/c;->a(Ljava/util/List;)V

    .line 76
    return-void
.end method
