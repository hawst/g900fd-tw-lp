.class public Lcom/google/android/apps/gmm/map/indoor/d/g;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/indoor/d/g;->a:Ljava/util/List;

    .line 19
    return-void
.end method

.method public static a(Lcom/google/maps/b/a/ag;)Lcom/google/android/apps/gmm/map/indoor/d/g;
    .locals 4

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/maps/b/a/ag;->a:Lcom/google/maps/b/a/cs;

    iget v1, v0, Lcom/google/maps/b/a/cs;->b:I

    .line 49
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v2

    .line 50
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 52
    invoke-virtual {p0, v0}, Lcom/google/maps/b/a/ag;->b(I)Lcom/google/maps/b/a/af;

    move-result-object v3

    .line 51
    invoke-static {v3}, Lcom/google/android/apps/gmm/map/indoor/d/f;->a(Lcom/google/maps/b/a/af;)Lcom/google/android/apps/gmm/map/indoor/d/f;

    move-result-object v3

    .line 53
    if-eqz v3, :cond_0

    .line 54
    invoke-virtual {v2, v3}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 50
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/d/g;

    invoke-virtual {v2}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/indoor/d/g;-><init>(Ljava/util/List;)V

    return-object v0
.end method
