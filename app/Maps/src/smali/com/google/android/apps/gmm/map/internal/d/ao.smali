.class Lcom/google/android/apps/gmm/map/internal/d/ao;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/internal/d/an;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/an;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/ao;->a:Lcom/google/android/apps/gmm/map/internal/d/an;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 19

    .prologue
    .line 149
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 151
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/d/ao;->a:Lcom/google/android/apps/gmm/map/internal/d/an;

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v10, v1

    check-cast v10, Lcom/google/android/apps/gmm/map/internal/d/ar;

    iget-object v1, v10, Lcom/google/android/apps/gmm/map/internal/d/ar;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/as;->t:Lcom/google/android/apps/gmm/map/internal/c/bp;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-static {v10, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/an;->a(Lcom/google/android/apps/gmm/map/internal/d/ar;ILcom/google/android/apps/gmm/map/internal/c/bo;)V

    goto :goto_0

    :cond_1
    iget-object v1, v10, Lcom/google/android/apps/gmm/map/internal/d/ar;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->w:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v1

    iget-object v2, v13, Lcom/google/android/apps/gmm/map/internal/d/an;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v2, v13, Lcom/google/android/apps/gmm/map/internal/d/an;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bo;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v10, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/an;->a(Lcom/google/android/apps/gmm/map/internal/d/ar;ILcom/google/android/apps/gmm/map/internal/c/bo;)V

    :goto_1
    const/4 v1, 0x1

    :goto_2
    if-nez v1, :cond_0

    iget-object v6, v13, Lcom/google/android/apps/gmm/map/internal/d/an;->s:Ljava/util/List;

    if-eqz v6, :cond_0

    iget-object v1, v10, Lcom/google/android/apps/gmm/map/internal/d/ar;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->w:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v14

    iget-object v15, v13, Lcom/google/android/apps/gmm/map/internal/d/an;->r:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->clear()V

    iget v1, v14, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    const/high16 v2, 0x40000000    # 2.0f

    shr-int v1, v2, v1

    int-to-float v1, v1

    const/high16 v2, 0x41f00000    # 30.0f

    div-float/2addr v1, v2

    mul-float v7, v1, v1

    const/4 v1, 0x0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    move v3, v1

    :goto_3
    if-ge v3, v8, :cond_6

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/google/android/apps/gmm/map/k/a;

    iget-object v9, v2, Lcom/google/android/apps/gmm/map/k/a;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v14}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v11

    move v5, v1

    :goto_4
    if-ge v5, v11, :cond_11

    invoke-interface {v15, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/k/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/k/a;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v9, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->d(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    cmpg-float v1, v1, v7

    if-gez v1, :cond_5

    const/4 v1, 0x1

    :goto_5
    if-nez v1, :cond_2

    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    invoke-static {v10, v2, v1}, Lcom/google/android/apps/gmm/map/internal/d/an;->a(Lcom/google/android/apps/gmm/map/internal/d/ar;ILcom/google/android/apps/gmm/map/internal/c/bo;)V

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    :cond_5
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_4

    :cond_6
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, v13, Lcom/google/android/apps/gmm/map/internal/d/an;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v1, v14}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a_(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v10, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/an;->a(Lcom/google/android/apps/gmm/map/internal/d/ar;ILcom/google/android/apps/gmm/map/internal/c/bo;)V

    goto/16 :goto_0

    :cond_7
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v2

    if-ltz v2, :cond_8

    const/4 v1, 0x1

    :goto_6
    if-nez v1, :cond_9

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_8
    const/4 v1, 0x0

    goto :goto_6

    :cond_9
    new-instance v16, Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    move v12, v1

    :goto_7
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v1

    if-ge v12, v1, :cond_10

    invoke-interface {v15, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/google/android/apps/gmm/map/k/a;

    const/4 v1, 0x1

    new-array v0, v1, [Lcom/google/android/apps/gmm/map/internal/c/a;

    move-object/from16 v17, v0

    const/4 v9, 0x0

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/a;

    iget-object v2, v11, Lcom/google/android/apps/gmm/map/k/a;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;IFLcom/google/android/apps/gmm/map/b/a/y;FFF)V

    aput-object v1, v17, v9

    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/aa;

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/p;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/an;->q:[Ljava/lang/String;

    iget-object v5, v11, Lcom/google/android/apps/gmm/map/k/a;->d:Lcom/google/android/apps/gmm/map/k/b;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/k/b;->ordinal()I

    move-result v5

    aget-object v4, v4, v5

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/apps/gmm/map/internal/c/p;-><init>(Ljava/lang/String;II)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(ILcom/google/android/apps/gmm/map/internal/c/p;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bi;ILjava/lang/String;F)V

    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/z;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/d;->b:Lcom/google/android/apps/gmm/map/internal/c/d;

    move-object/from16 v0, v18

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/z;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/d;)V

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/ap;->a:[I

    iget-object v2, v11, Lcom/google/android/apps/gmm/map/k/a;->d:Lcom/google/android/apps/gmm/map/k/b;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/k/b;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    const/4 v1, 0x0

    :goto_8
    const/4 v2, 0x0

    iget-object v4, v11, Lcom/google/android/apps/gmm/map/k/a;->d:Lcom/google/android/apps/gmm/map/k/b;

    sget-object v5, Lcom/google/android/apps/gmm/map/k/b;->a:Lcom/google/android/apps/gmm/map/k/b;

    if-ne v4, v5, :cond_a

    sget-object v2, Lcom/google/b/f/t;->gd:Lcom/google/b/f/t;

    :cond_a
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/an;->l()Lcom/google/android/apps/gmm/map/internal/c/ao;

    move-result-object v4

    iput-object v14, v4, Lcom/google/android/apps/gmm/map/internal/c/ao;->d:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v5, v11, Lcom/google/android/apps/gmm/map/k/a;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/ao;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v17

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/internal/c/ao;->i:[Lcom/google/android/apps/gmm/map/internal/c/a;

    iput-object v3, v4, Lcom/google/android/apps/gmm/map/internal/c/ao;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    const/16 v3, 0x64

    iput v3, v4, Lcom/google/android/apps/gmm/map/internal/c/ao;->q:I

    iget v3, v11, Lcom/google/android/apps/gmm/map/k/a;->e:I

    iput v3, v4, Lcom/google/android/apps/gmm/map/internal/c/ao;->r:I

    const/16 v3, 0x1010

    iput v3, v4, Lcom/google/android/apps/gmm/map/internal/c/ao;->u:I

    iget-object v3, v11, Lcom/google/android/apps/gmm/map/k/a;->a:Ljava/lang/String;

    iput-object v3, v4, Lcom/google/android/apps/gmm/map/internal/c/ao;->w:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/c/e;->c:Lcom/google/android/apps/gmm/map/internal/c/e;

    iput-object v3, v4, Lcom/google/android/apps/gmm/map/internal/c/ao;->x:Lcom/google/android/apps/gmm/map/internal/c/e;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_c

    :cond_b
    const/4 v3, 0x1

    :goto_9
    if-nez v3, :cond_d

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :pswitch_1
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_8

    :pswitch_2
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_8

    :cond_c
    const/4 v3, 0x0

    goto :goto_9

    :cond_d
    iput-object v1, v4, Lcom/google/android/apps/gmm/map/internal/c/ao;->E:Ljava/lang/Integer;

    iget-object v1, v11, Lcom/google/android/apps/gmm/map/k/a;->d:Lcom/google/android/apps/gmm/map/k/b;

    sget-object v3, Lcom/google/android/apps/gmm/map/k/b;->a:Lcom/google/android/apps/gmm/map/k/b;

    if-ne v1, v3, :cond_f

    const/4 v1, 0x1

    :goto_a
    iput-boolean v1, v4, Lcom/google/android/apps/gmm/map/internal/c/ao;->F:Z

    iput-object v2, v4, Lcom/google/android/apps/gmm/map/internal/c/ao;->D:Lcom/google/b/f/cq;

    iget-object v1, v11, Lcom/google/android/apps/gmm/map/k/a;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, v11, Lcom/google/android/apps/gmm/map/k/a;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-object v1, v4, Lcom/google/android/apps/gmm/map/internal/c/ao;->f:Lcom/google/android/apps/gmm/map/b/a/j;

    :cond_e
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/c/ao;->a()Lcom/google/android/apps/gmm/map/internal/c/an;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v12, 0x1

    move v12, v1

    goto/16 :goto_7

    :cond_f
    const/4 v1, 0x0

    goto :goto_a

    :cond_10
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/co;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/internal/c/co;-><init>()V

    iput-object v14, v1, Lcom/google/android/apps/gmm/map/internal/c/co;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v2, v13, Lcom/google/android/apps/gmm/map/internal/d/an;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    iput-object v2, v1, Lcom/google/android/apps/gmm/map/internal/c/co;->i:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, v16

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/co;->e:Ljava/util/List;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/co;->a()Lcom/google/android/apps/gmm/map/internal/c/cm;

    move-result-object v1

    iget-object v2, v13, Lcom/google/android/apps/gmm/map/internal/d/an;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v2, v14, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;)V

    const/4 v2, 0x0

    invoke-static {v10, v2, v1}, Lcom/google/android/apps/gmm/map/internal/d/an;->a(Lcom/google/android/apps/gmm/map/internal/d/ar;ILcom/google/android/apps/gmm/map/internal/c/bo;)V

    goto/16 :goto_0

    :cond_11
    move v1, v4

    goto/16 :goto_5

    .line 149
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    .line 151
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
