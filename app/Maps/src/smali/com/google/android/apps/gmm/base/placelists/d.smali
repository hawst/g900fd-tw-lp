.class Lcom/google/android/apps/gmm/base/placelists/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/hotels/a/g;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/placelists/d;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/d;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 163
    if-eqz v0, :cond_0

    .line 165
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/d;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    sget v2, Lcom/google/android/apps/gmm/l;->iU:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 164
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->a(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/CharSequence;)V

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/d;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/hotels/a/h;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/hotels/a/h;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 169
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            "Lcom/google/e/a/a/a/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/d;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(Ljava/util/Map;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/d;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Z)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/d;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/hotels/a/h;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/hotels/a/h;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 158
    return-void
.end method
