.class Lcom/google/android/apps/gmm/map/internal/d/c/b/g;
.super Landroid/graphics/drawable/PictureDrawable;
.source "PG"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:F

.field private final d:F


# direct methods
.method constructor <init>(Landroid/graphics/Picture;FF)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 546
    invoke-direct {p0, p1}, Landroid/graphics/drawable/PictureDrawable;-><init>(Landroid/graphics/Picture;)V

    .line 548
    cmpl-float v0, p2, v1

    if-lez v0, :cond_0

    .line 549
    float-to-int v0, p2

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->a:I

    .line 550
    invoke-virtual {p1}, Landroid/graphics/Picture;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v0, p2, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->c:F

    .line 556
    :goto_0
    cmpl-float v0, p3, v1

    if-lez v0, :cond_1

    .line 557
    float-to-int v0, p3

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->b:I

    .line 558
    invoke-virtual {p1}, Landroid/graphics/Picture;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float v0, p3, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->d:F

    .line 563
    :goto_1
    return-void

    .line 552
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Picture;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->a:I

    .line 553
    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->c:F

    goto :goto_0

    .line 560
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Picture;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->b:I

    .line 561
    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->d:F

    goto :goto_1
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 574
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 575
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->c:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->d:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 576
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->getPicture()Landroid/graphics/Picture;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawPicture(Landroid/graphics/Picture;)V

    .line 577
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 578
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 582
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;

    .line 583
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->getPicture()Landroid/graphics/Picture;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->getPicture()Landroid/graphics/Picture;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 569
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->b:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 566
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->a:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 588
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/g;->getPicture()Landroid/graphics/Picture;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
