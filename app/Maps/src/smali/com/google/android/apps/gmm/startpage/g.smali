.class public Lcom/google/android/apps/gmm/startpage/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/r/a/a;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/base/activities/c;

.field private final c:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/prefetchcache/api/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/android/apps/gmm/base/l/a/t;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private e:Lcom/google/android/apps/gmm/startpage/e/k;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/gmm/startpage/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/startpage/g;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 54
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 55
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->p()Lcom/google/android/apps/gmm/prefetchcache/api/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/f;->b()Ljava/util/List;

    move-result-object v0

    .line 54
    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/g;->c:Lcom/google/b/c/cv;

    .line 56
    return-void
.end method


# virtual methods
.method public final a(Z)Lcom/google/android/apps/gmm/r/a/a;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 75
    iput-object v5, p0, Lcom/google/android/apps/gmm/startpage/g;->d:Lcom/google/android/apps/gmm/base/l/a/t;

    .line 76
    iput-object v5, p0, Lcom/google/android/apps/gmm/startpage/g;->e:Lcom/google/android/apps/gmm/startpage/e/k;

    .line 78
    sget-object v1, Lcom/google/android/apps/gmm/startpage/h;->a:[I

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->e()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/startpage/i;->c:Lcom/google/android/apps/gmm/startpage/i;

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/i;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 102
    :goto_1
    return-object p0

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/g;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    if-nez p1, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/startpage/i;->a:Lcom/google/android/apps/gmm/startpage/i;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l_()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/g;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetchcache/api/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/startpage/i;->a:Lcom/google/android/apps/gmm/startpage/i;

    goto :goto_0

    :cond_3
    if-nez p1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l_()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/startpage/i;->b:Lcom/google/android/apps/gmm/startpage/i;

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/startpage/i;->c:Lcom/google/android/apps/gmm/startpage/i;

    goto :goto_0

    .line 80
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/startpage/g;->a:Ljava/lang/String;

    const-string v0, "ManualOfflineCache is enabled: %d areas"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/startpage/g;->c:Lcom/google/b/c/cv;

    invoke-virtual {v3}, Lcom/google/b/c/cv;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 81
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/g;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v2

    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetchcache/api/c;

    .line 84
    if-eqz p1, :cond_6

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->d()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 85
    :cond_6
    new-instance v3, Lcom/google/android/apps/gmm/base/l/x;

    iget-object v4, p0, Lcom/google/android/apps/gmm/startpage/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v3, v4, v0, v5, p1}, Lcom/google/android/apps/gmm/base/l/x;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/prefetchcache/api/c;Lcom/google/android/apps/gmm/base/l/a/u;Z)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 95
    :cond_7
    new-instance v0, Lcom/google/android/apps/gmm/base/l/z;

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v2, v5, v1, p1}, Lcom/google/android/apps/gmm/base/l/z;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/l/a/ab;Ljava/util/List;Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/g;->d:Lcom/google/android/apps/gmm/base/l/a/t;

    goto/16 :goto_1

    .line 100
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/startpage/g;->a:Ljava/lang/String;

    .line 101
    new-instance v0, Lcom/google/android/apps/gmm/startpage/f/s;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/startpage/f/s;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/g;->e:Lcom/google/android/apps/gmm/startpage/e/k;

    goto/16 :goto_1

    .line 78
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()Lcom/google/android/apps/gmm/startpage/e/j;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/base/l/a/t;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/g;->d:Lcom/google/android/apps/gmm/base/l/a/t;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/startpage/e/k;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/g;->e:Lcom/google/android/apps/gmm/startpage/e/k;

    return-object v0
.end method
