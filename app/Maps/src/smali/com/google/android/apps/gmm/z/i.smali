.class public Lcom/google/android/apps/gmm/z/i;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:I

.field private static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/android/apps/gmm/z/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/z/i;->b:Ljava/lang/String;

    .line 47
    sget v0, Lcom/google/android/apps/gmm/g;->ex:I

    sput v0, Lcom/google/android/apps/gmm/z/i;->a:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;)Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 122
    sget v0, Lcom/google/android/apps/gmm/z/i;->a:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/z/a;Landroid/view/View;)Lcom/google/android/apps/gmm/z/b/l;
    .locals 9
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 204
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    :goto_0
    return-object v3

    .line 209
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/z/i;->a:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/z/b/l;

    .line 210
    if-eqz v0, :cond_1

    .line 211
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/z/a;->a(Lcom/google/android/apps/gmm/z/b/l;)V

    move-object v3, v0

    .line 212
    goto :goto_0

    .line 216
    :cond_1
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 217
    check-cast p1, Landroid/view/ViewGroup;

    .line 218
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 219
    const/4 v0, 0x0

    move v2, v0

    move-object v1, v3

    :goto_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 221
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v7

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v8

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 228
    invoke-virtual {p1, v0, v4, v3}, Landroid/view/ViewGroup;->getChildVisibleRect(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    .line 229
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v5

    if-lez v5, :cond_4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    if-lez v5, :cond_4

    .line 230
    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a;Landroid/view/View;)Lcom/google/android/apps/gmm/z/b/l;

    .line 231
    if-nez v1, :cond_4

    .line 233
    sget v5, Lcom/google/android/apps/gmm/z/i;->a:I

    invoke-virtual {v0, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/z/b/l;

    .line 234
    if-eqz v0, :cond_4

    iget-object v5, v0, Lcom/google/android/apps/gmm/z/b/l;->e:Lcom/google/r/b/a/tf;

    if-eqz v5, :cond_4

    .line 219
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1

    :cond_2
    move-object v1, v3

    :cond_3
    move-object v3, v1

    .line 241
    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method public static a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/b/f/b/a/cp;
    .locals 2

    .prologue
    .line 126
    sget-object v0, Lcom/google/android/apps/gmm/z/j;->a:[I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 136
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 128
    :pswitch_0
    sget-object v0, Lcom/google/b/f/b/a/cp;->a:Lcom/google/b/f/b/a/cp;

    goto :goto_0

    .line 130
    :pswitch_1
    sget-object v0, Lcom/google/b/f/b/a/cp;->b:Lcom/google/b/f/b/a/cp;

    goto :goto_0

    .line 132
    :pswitch_2
    sget-object v0, Lcom/google/b/f/b/a/cp;->c:Lcom/google/b/f/b/a/cp;

    goto :goto_0

    .line 134
    :pswitch_3
    sget-object v0, Lcom/google/b/f/b/a/cp;->d:Lcom/google/b/f/b/a/cp;

    goto :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 54
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;Lcom/google/android/apps/gmm/z/b/l;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/z/b/l;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 114
    sget v0, Lcom/google/android/apps/gmm/z/i;->a:I

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 115
    return-void
.end method

.method public static a(Landroid/view/View;Lcom/google/android/apps/gmm/z/b/o;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 161
    invoke-interface {p1}, Lcom/google/android/apps/gmm/z/b/o;->f_()Lcom/google/b/f/t;

    move-result-object v2

    .line 162
    if-eqz p0, :cond_0

    .line 165
    sget v0, Lcom/google/android/apps/gmm/z/i;->a:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/z/b/l;

    .line 166
    if-nez v0, :cond_1

    .line 167
    invoke-static {v2}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/z/i;->a:I

    invoke-virtual {p0, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    array-length v1, v1

    if-gtz v1, :cond_3

    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-eq v1, v2, :cond_0

    .line 169
    const-string v1, "Fragment\'s root view must be attached UE3 params for page VE.Fragment=%s, params=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 171
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object v0, v2, v3

    .line 170
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 169
    invoke-static {}, Lcom/google/b/a/bc;->a()Ljava/util/Random;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    move-result v1

    const v2, 0x3a83126f    # 0.001f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 168
    :cond_3
    iget-object v1, v0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    aget-object v1, v1, v4

    goto :goto_1
.end method

.method public static a(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 145
    invoke-static {}, Lcom/google/b/a/bc;->a()Ljava/util/Random;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    const v1, 0x3a83126f    # 0.001f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 147
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 150
    :cond_0
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/z/b/n;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/z/b/l;)V
    .locals 7

    .prologue
    .line 65
    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/z/b/n;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/z/b/l;I)V

    .line 67
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/z/b/n;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/z/b/l;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78
    iget v0, p3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->g:F

    iget v3, p4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->g:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 79
    sget-object v0, Lcom/google/b/f/b/a/cn;->b:Lcom/google/b/f/b/a/cn;

    .line 86
    :goto_1
    invoke-static {}, Lcom/google/b/f/b/a/ck;->newBuilder()Lcom/google/b/f/b/a/cm;

    move-result-object v3

    .line 87
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    .line 78
    goto :goto_0

    .line 80
    :cond_1
    iget v0, p4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->g:F

    iget v3, p3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->g:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    .line 81
    sget-object v0, Lcom/google/b/f/b/a/cn;->a:Lcom/google/b/f/b/a/cn;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 80
    goto :goto_2

    .line 83
    :cond_3
    sget-object v0, Lcom/google/b/f/b/a/cn;->c:Lcom/google/b/f/b/a/cn;

    goto :goto_1

    .line 87
    :cond_4
    iget v4, v3, Lcom/google/b/f/b/a/cm;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v3, Lcom/google/b/f/b/a/cm;->a:I

    iget v0, v0, Lcom/google/b/f/b/a/cn;->d:I

    iput v0, v3, Lcom/google/b/f/b/a/cm;->c:I

    .line 88
    invoke-static {p3}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/b/f/b/a/cp;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/b/f/b/a/cm;->a(Lcom/google/b/f/b/a/cp;)Lcom/google/b/f/b/a/cm;

    move-result-object v0

    .line 89
    invoke-static {p4}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/b/f/b/a/cp;

    move-result-object v3

    if-nez v3, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget v4, v0, Lcom/google/b/f/b/a/cm;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v0, Lcom/google/b/f/b/a/cm;->a:I

    iget v3, v3, Lcom/google/b/f/b/a/cp;->e:I

    iput v3, v0, Lcom/google/b/f/b/a/cm;->d:I

    sget-object v3, Lcom/google/b/f/b/a/cr;->a:Lcom/google/b/f/b/a/cr;

    .line 90
    invoke-virtual {v0, v3}, Lcom/google/b/f/b/a/cm;->a(Lcom/google/b/f/b/a/cr;)Lcom/google/b/f/b/a/cm;

    move-result-object v0

    .line 92
    if-ltz p6, :cond_6

    .line 93
    iget v3, v0, Lcom/google/b/f/b/a/cm;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/google/b/f/b/a/cm;->a:I

    iput p6, v0, Lcom/google/b/f/b/a/cm;->b:I

    .line 97
    :cond_6
    invoke-static {p5}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    new-array v1, v1, [Lcom/google/b/f/cq;

    aput-object p2, v1, v2

    .line 98
    iput-object v1, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 99
    invoke-virtual {v0}, Lcom/google/b/f/b/a/cm;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ck;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/z/b/m;->a(Lcom/google/b/f/b/a/ck;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 96
    invoke-interface {p0, p1, v0}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 101
    return-void
.end method
