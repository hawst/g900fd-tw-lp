.class public final enum Lcom/google/android/apps/gmm/startpage/af;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/startpage/af;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/startpage/af;

.field public static final enum b:Lcom/google/android/apps/gmm/startpage/af;

.field public static final enum c:Lcom/google/android/apps/gmm/startpage/af;

.field public static final enum d:Lcom/google/android/apps/gmm/startpage/af;

.field private static final synthetic f:[Lcom/google/android/apps/gmm/startpage/af;


# instance fields
.field final e:Lcom/google/o/h/a/di;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 173
    new-instance v0, Lcom/google/android/apps/gmm/startpage/af;

    const-string v1, "FETCH_ON_DEMAND"

    sget-object v2, Lcom/google/o/h/a/di;->a:Lcom/google/o/h/a/di;

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/startpage/af;-><init>(Ljava/lang/String;ILcom/google/o/h/a/di;)V

    sput-object v0, Lcom/google/android/apps/gmm/startpage/af;->a:Lcom/google/android/apps/gmm/startpage/af;

    .line 174
    new-instance v0, Lcom/google/android/apps/gmm/startpage/af;

    const-string v1, "GMM_PREFETCH_ON_STARTUP"

    sget-object v2, Lcom/google/o/h/a/di;->b:Lcom/google/o/h/a/di;

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/startpage/af;-><init>(Ljava/lang/String;ILcom/google/o/h/a/di;)V

    sput-object v0, Lcom/google/android/apps/gmm/startpage/af;->b:Lcom/google/android/apps/gmm/startpage/af;

    .line 175
    new-instance v0, Lcom/google/android/apps/gmm/startpage/af;

    const-string v1, "GMM_PREFETCH_ON_RENDER"

    sget-object v2, Lcom/google/o/h/a/di;->c:Lcom/google/o/h/a/di;

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/startpage/af;-><init>(Ljava/lang/String;ILcom/google/o/h/a/di;)V

    sput-object v0, Lcom/google/android/apps/gmm/startpage/af;->c:Lcom/google/android/apps/gmm/startpage/af;

    .line 176
    new-instance v0, Lcom/google/android/apps/gmm/startpage/af;

    const-string v1, "SPONTANEOUS_FETCH"

    sget-object v2, Lcom/google/o/h/a/di;->d:Lcom/google/o/h/a/di;

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/gmm/startpage/af;-><init>(Ljava/lang/String;ILcom/google/o/h/a/di;)V

    sput-object v0, Lcom/google/android/apps/gmm/startpage/af;->d:Lcom/google/android/apps/gmm/startpage/af;

    .line 172
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/startpage/af;

    sget-object v1, Lcom/google/android/apps/gmm/startpage/af;->a:Lcom/google/android/apps/gmm/startpage/af;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/startpage/af;->b:Lcom/google/android/apps/gmm/startpage/af;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/startpage/af;->c:Lcom/google/android/apps/gmm/startpage/af;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/startpage/af;->d:Lcom/google/android/apps/gmm/startpage/af;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/gmm/startpage/af;->f:[Lcom/google/android/apps/gmm/startpage/af;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/o/h/a/di;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/o/h/a/di;",
            ")V"
        }
    .end annotation

    .prologue
    .line 180
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 181
    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/af;->e:Lcom/google/o/h/a/di;

    .line 182
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/startpage/af;
    .locals 1

    .prologue
    .line 172
    const-class v0, Lcom/google/android/apps/gmm/startpage/af;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/startpage/af;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/startpage/af;
    .locals 1

    .prologue
    .line 172
    sget-object v0, Lcom/google/android/apps/gmm/startpage/af;->f:[Lcom/google/android/apps/gmm/startpage/af;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/startpage/af;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/startpage/af;

    return-object v0
.end method
