.class public Lcom/google/android/apps/gmm/directions/option/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/views/a/e;


# instance fields
.field final a:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field b:I

.field private final c:Lcom/google/android/apps/gmm/directions/f/b/l;

.field private final d:Landroid/content/Context;

.field private final e:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/widget/RadioGroup$OnCheckedChangeListener;Lcom/google/android/apps/gmm/directions/f/b/l;ILandroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/option/d;->a:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    .line 32
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/option/d;->c:Lcom/google/android/apps/gmm/directions/f/b/l;

    .line 33
    iput p3, p0, Lcom/google/android/apps/gmm/directions/option/d;->b:I

    .line 34
    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/option/d;->d:Landroid/content/Context;

    .line 35
    invoke-static {p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/option/d;->e:Landroid/view/LayoutInflater;

    .line 36
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 40
    sget v0, Lcom/google/android/apps/gmm/h;->o:I

    return v0
.end method

.method public final a(Landroid/view/View;)Lcom/google/android/apps/gmm/base/views/a/f;
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lcom/google/android/apps/gmm/directions/option/f;

    check-cast p1, Landroid/widget/RadioGroup;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/directions/option/f;-><init>(Landroid/widget/RadioGroup;)V

    .line 80
    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/a/f;)V
    .locals 6

    .prologue
    .line 45
    check-cast p1, Lcom/google/android/apps/gmm/directions/option/f;

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/option/d;->c:Lcom/google/android/apps/gmm/directions/f/b/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/b/l;->a:Lcom/google/android/apps/gmm/directions/f/b/m;

    .line 47
    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/f/b/m;->l:[Lcom/google/android/apps/gmm/directions/f/b/q;

    .line 48
    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 49
    :cond_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/directions/option/f;->a:Landroid/widget/RadioGroup;

    iget v0, v0, Lcom/google/android/apps/gmm/directions/f/b/m;->m:I

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->setId(I)V

    .line 52
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/option/f;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v0

    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_1

    .line 53
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/option/d;->e:Landroid/view/LayoutInflater;

    sget v3, Lcom/google/android/apps/gmm/h;->n:I

    iget-object v4, p1, Lcom/google/android/apps/gmm/directions/option/f;->a:Landroid/widget/RadioGroup;

    const/4 v5, 0x1

    invoke-virtual {v1, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/option/f;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    array-length v1, v2

    if-lt v0, v1, :cond_2

    .line 59
    iget-object v1, p1, Lcom/google/android/apps/gmm/directions/option/f;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 58
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 62
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    array-length v0, v2

    if-ge v1, v0, :cond_3

    .line 63
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/option/f;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 64
    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setId(I)V

    .line 65
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/option/d;->d:Landroid/content/Context;

    aget-object v4, v2, v1

    invoke-interface {v4}, Lcom/google/android/apps/gmm/directions/f/b/q;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 62
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 67
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/option/f;->a:Landroid/widget/RadioGroup;

    iget v1, p0, Lcom/google/android/apps/gmm/directions/option/d;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 68
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/option/f;->a:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/google/android/apps/gmm/directions/option/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/directions/option/e;-><init>(Lcom/google/android/apps/gmm/directions/option/d;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 75
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/google/android/apps/gmm/directions/option/m;->d:Lcom/google/android/apps/gmm/directions/option/m;

    iget v0, v0, Lcom/google/android/apps/gmm/directions/option/m;->e:I

    return v0
.end method
