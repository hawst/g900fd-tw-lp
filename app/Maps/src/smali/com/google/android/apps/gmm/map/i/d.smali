.class public Lcom/google/android/apps/gmm/map/i/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/r;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/google/android/apps/gmm/map/b/a/q;

.field public final c:Lcom/google/android/apps/gmm/map/b/a/r;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/q;)V
    .locals 3
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/r;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/q;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/i/d;->a:Ljava/util/List;

    .line 37
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/i/d;->a(Ljava/util/List;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/d;->c:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 39
    if-nez p2, :cond_1

    .line 40
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/r;

    .line 41
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/r/a/r;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 42
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/r/a/r;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->c()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->c()I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(II)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object p2

    .line 49
    :cond_1
    const-string v0, "No destination or usable PolylineMapData."

    if-nez p2, :cond_2

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 50
    :cond_2
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/i/d;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 51
    return-void
.end method

.method private static a(Ljava/util/List;)Lcom/google/android/apps/gmm/map/b/a/r;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/r;",
            ">;)",
            "Lcom/google/android/apps/gmm/map/b/a/r;"
        }
    .end annotation

    .prologue
    .line 66
    const/4 v0, 0x1

    .line 67
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/s;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/s;-><init>()V

    .line 68
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/r;

    .line 69
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/r/a/r;->h()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 70
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/r/a/r;->b()Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_0

    .line 75
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v6, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/s;->a(DD)Lcom/google/android/apps/gmm/map/b/a/s;

    .line 78
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {v2, v4, v5, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/s;->a(DD)Lcom/google/android/apps/gmm/map/b/a/s;

    .line 79
    const/4 v0, 0x0

    move v1, v0

    .line 80
    goto :goto_0

    .line 81
    :cond_1
    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/s;->a()Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    goto :goto_1
.end method
