.class public final Lcom/google/android/apps/gmm/map/o/b/a;
.super Lcom/google/android/apps/gmm/map/o/b/b;
.source "PG"


# instance fields
.field public a:F

.field public b:F

.field public c:F

.field public d:F

.field public final e:Lcom/google/android/apps/gmm/map/b/a/ay;

.field public final f:Lcom/google/android/apps/gmm/map/o/b/i;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/o/b/b;-><init>()V

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->e:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->f:Lcom/google/android/apps/gmm/map/o/b/i;

    .line 35
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/o/b/b;-><init>()V

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->e:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/o/b/i;-><init>(Lcom/google/android/apps/gmm/map/o/b/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->f:Lcom/google/android/apps/gmm/map/o/b/i;

    .line 39
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/o/b/a;->a(FFFF)V

    .line 40
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->e:Lcom/google/android/apps/gmm/map/b/a/ay;

    return-object v0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->f:Lcom/google/android/apps/gmm/map/o/b/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/o/b/i;->a(I)Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v0

    return-object v0
.end method

.method public final a(FFFF)V
    .locals 9

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    .line 47
    iput p1, p0, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    .line 48
    iput p2, p0, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    .line 49
    iput p3, p0, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    .line 50
    iput p4, p0, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    .line 52
    add-float v0, p1, p3

    mul-float v2, v0, v7

    .line 53
    add-float v0, p2, p4

    mul-float v3, v0, v7

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->e:Lcom/google/android/apps/gmm/map/b/a/ay;

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->f:Lcom/google/android/apps/gmm/map/o/b/i;

    if-eqz v0, :cond_0

    .line 57
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/b/a;->f:Lcom/google/android/apps/gmm/map/o/b/i;

    const-wide/16 v4, 0x0

    sub-float v0, p3, p1

    mul-float v6, v0, v7

    sub-float v0, p4, p2

    mul-float/2addr v7, v0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/o/b/i;->a(FFDFFZ)V

    .line 65
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z
    .locals 3

    .prologue
    .line 130
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 131
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 132
    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    cmpg-float v2, v2, v0

    if-gtz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    cmpg-float v0, v1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/b/a;)Z
    .locals 2

    .prologue
    .line 124
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/b/i;)Z
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->f:Lcom/google/android/apps/gmm/map/o/b/i;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/o/b/i;->a(Lcom/google/android/apps/gmm/map/o/b/i;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/o/b/a;)Z
    .locals 2

    .prologue
    .line 136
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 165
    if-ne p0, p1, :cond_1

    .line 172
    :cond_0
    :goto_0
    return v0

    .line 168
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/o/b/a;

    if-eqz v2, :cond_3

    .line 169
    check-cast p1, Lcom/google/android/apps/gmm/map/o/b/a;

    .line 170
    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 172
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 154
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 157
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 159
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 177
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x4c

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "AABB[["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
