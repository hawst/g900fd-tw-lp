.class public Lcom/google/android/apps/gmm/suggest/SuggestFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/activities/y;


# static fields
.field static final b:Ljava/lang/String;


# instance fields
.field private a:Lcom/google/android/apps/gmm/shared/net/i;

.field public final c:Lcom/google/android/apps/gmm/suggest/l;

.field d:Lcom/google/android/apps/gmm/startpage/m;

.field public e:Lcom/google/android/apps/gmm/suggest/g/c;

.field private final f:Lcom/google/android/apps/gmm/suggest/d;

.field private final g:Lcom/google/android/apps/gmm/startpage/d/d;

.field private final m:Lcom/google/android/apps/gmm/suggest/d/d;

.field private n:Lcom/google/android/apps/gmm/base/activities/p;

.field private o:Landroid/widget/SearchView$OnQueryTextListener;

.field private final p:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const-class v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 127
    new-instance v0, Lcom/google/android/apps/gmm/suggest/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/suggest/g;-><init>(Lcom/google/android/apps/gmm/suggest/SuggestFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->p:Ljava/lang/Object;

    .line 155
    new-instance v0, Lcom/google/android/apps/gmm/suggest/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/suggest/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    .line 156
    new-instance v0, Lcom/google/android/apps/gmm/suggest/d/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/suggest/d/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->m:Lcom/google/android/apps/gmm/suggest/d/d;

    .line 157
    new-instance v0, Lcom/google/android/apps/gmm/suggest/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->m:Lcom/google/android/apps/gmm/suggest/d/d;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/suggest/d;-><init>(Lcom/google/android/apps/gmm/suggest/l;Lcom/google/android/apps/gmm/suggest/d/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->f:Lcom/google/android/apps/gmm/suggest/d;

    .line 158
    new-instance v0, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/startpage/d/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 159
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/suggest/k;Landroid/app/Fragment;Landroid/app/Fragment;)Lcom/google/android/apps/gmm/suggest/SuggestFragment;
    .locals 1
    .param p2    # Landroid/app/Fragment;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Landroid/app/Fragment;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Landroid/app/Fragment;",
            ":",
            "Lcom/google/android/apps/gmm/cardui/a/d;",
            "S:",
            "Landroid/app/Fragment;",
            ":",
            "Lcom/google/android/apps/gmm/suggest/a/a;",
            ">(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/android/apps/gmm/suggest/k;",
            "TC;TS;)",
            "Lcom/google/android/apps/gmm/suggest/SuggestFragment;"
        }
    .end annotation

    .prologue
    .line 172
    new-instance v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;-><init>()V

    .line 173
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/suggest/k;Landroid/app/Fragment;Landroid/app/Fragment;)V

    .line 174
    return-object v0
.end method

.method private a(Landroid/app/Fragment;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 591
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b:Ljava/lang/String;

    new-instance v2, Ljava/io/PrintWriter;

    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;Z)V

    invoke-virtual {v0, v1, v5, v2, v5}, Landroid/app/FragmentManager;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 593
    sget-object v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 595
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v2

    .line 599
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "cardui_action_delegate"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 600
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x66

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Unknown fragment seems loaded:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", stack-count="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fragmentIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", argument.keySet()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 593
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 601
    return-void
.end method

.method private static a(Landroid/os/Bundle;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/android/apps/gmm/suggest/a/a;)V
    .locals 2

    .prologue
    .line 202
    if-eqz p1, :cond_0

    move-object v0, p1

    .line 203
    check-cast v0, Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "cardui_action_delegate"

    check-cast p1, Landroid/app/Fragment;

    invoke-virtual {v0, p0, v1, p1}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    .line 206
    :cond_0
    if-eqz p2, :cond_1

    move-object v0, p2

    .line 207
    check-cast v0, Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "suggest_action_listener"

    check-cast p2, Landroid/app/Fragment;

    invoke-virtual {v0, p0, v1, p2}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    .line 210
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/suggest/SuggestFragment;Lcom/google/android/apps/gmm/suggest/d/b;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Lcom/google/android/apps/gmm/suggest/d/b;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/suggest/SuggestFragment;Lcom/google/android/apps/gmm/suggest/e/a;)V
    .locals 3

    .prologue
    .line 81
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/a;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/gmm/suggest/l;->d:Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Lcom/google/android/apps/gmm/suggest/e/a;)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b()V

    sget-object v1, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Input query was updated: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/suggest/d/b;Ljava/lang/String;)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 405
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/b;->a:Lcom/google/android/apps/gmm/suggest/d/b;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Suggestion click should be separately handled!"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 407
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->h()Z

    move-result v0

    const-string v1, "SuggestFragment state does not allow submitting of query, only clicking suggestions!"

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 410
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/suggest/d/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    .line 411
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/suggest/l;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/suggest/l;->o()Lcom/google/b/c/cv;

    move-result-object v4

    const/4 v5, -0x1

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/suggest/d/a;-><init>(Lcom/google/android/apps/gmm/suggest/d/b;Ljava/lang/String;Ljava/lang/String;Lcom/google/b/c/cv;I)V

    .line 413
    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->m:Lcom/google/android/apps/gmm/suggest/d/d;

    .line 414
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    .line 413
    invoke-virtual {v2, v0, v3, v1}, Lcom/google/android/apps/gmm/suggest/d/d;->a(Lcom/google/android/apps/gmm/suggest/d/a;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/suggest/d/e;

    move-result-object v1

    .line 416
    if-nez v1, :cond_5

    const/4 v0, 0x0

    .line 419
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c()Lcom/google/android/apps/gmm/suggest/a/a;

    move-result-object v2

    .line 420
    if-eqz v2, :cond_4

    .line 421
    new-instance v3, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    .line 422
    iget-object v4, p1, Lcom/google/android/apps/gmm/suggest/d/b;->e:Lcom/google/b/f/t;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v3

    .line 423
    iget-object v4, p1, Lcom/google/android/apps/gmm/suggest/d/b;->f:Lcom/google/b/f/cj;

    if-eqz v4, :cond_3

    iget-object v5, v3, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v5, v4}, Lcom/google/maps/g/ia;->a(Lcom/google/b/f/cj;)Lcom/google/maps/g/ia;

    .line 424
    :cond_3
    invoke-virtual {v3, p2}, Lcom/google/android/apps/gmm/z/b/f;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v3

    .line 425
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/bf;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    .line 426
    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    .line 427
    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/suggest/l;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/suggest/a/a;->a(Ljava/lang/String;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V

    .line 429
    :cond_4
    return-void

    .line 417
    :cond_5
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/suggest/d/e;->b()Lcom/google/b/f/bf;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/suggest/e/a;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 656
    iput-object v3, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a:Lcom/google/android/apps/gmm/shared/net/i;

    .line 657
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 677
    :cond_0
    :goto_0
    return-void

    .line 660
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/a;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    .line 661
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 664
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v0

    if-nez v0, :cond_5

    .line 665
    :goto_2
    if-nez v3, :cond_6

    .line 666
    sget-object v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b:Ljava/lang/String;

    goto :goto_0

    .line 660
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 664
    :cond_5
    sget-object v1, Lcom/google/maps/a/a/a;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v3

    goto :goto_2

    .line 669
    :cond_6
    new-instance v6, Lcom/google/android/apps/gmm/suggest/d/g;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/suggest/d/g;-><init>()V

    .line 670
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->m:Lcom/google/android/apps/gmm/suggest/d/d;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/suggest/d/d;->a(Lcom/google/android/apps/gmm/suggest/d/g;)V

    .line 671
    invoke-virtual {v6, p1}, Lcom/google/android/apps/gmm/suggest/d/g;->a(Lcom/google/android/apps/gmm/suggest/e/a;)V

    .line 673
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 674
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->n_()Lcom/google/android/apps/gmm/suggest/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/suggest/l;->a()Lcom/google/android/apps/gmm/suggest/e/c;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    .line 675
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/suggest/l;->d()Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v4

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    .line 676
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/suggest/l;->f()Lcom/google/android/apps/gmm/suggest/e/b;

    move-result-object v5

    move-object v2, p1

    .line 674
    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/gmm/suggest/a/b;->a(Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/android/apps/gmm/suggest/e/a;Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/suggest/e/b;Lcom/google/android/apps/gmm/suggest/d/g;)Lcom/google/android/apps/gmm/shared/net/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a:Lcom/google/android/apps/gmm/shared/net/i;

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 218
    if-nez p1, :cond_0

    move v0, v2

    .line 232
    :goto_0
    return v0

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "suggest_fragment_state"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 222
    instance-of v1, v0, Lcom/google/android/apps/gmm/suggest/l;

    if-nez v1, :cond_1

    move v0, v2

    .line 223
    goto :goto_0

    .line 225
    :cond_1
    const-string v1, "suggest_fragment_odelay_content_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    .line 226
    instance-of v3, v1, Lcom/google/android/apps/gmm/startpage/d/d;

    if-nez v3, :cond_2

    move v0, v2

    .line 227
    goto :goto_0

    .line 230
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    check-cast v0, Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(Lcom/google/android/apps/gmm/suggest/l;)V

    .line 231
    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/startpage/d/d;)V

    .line 232
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 432
    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private i()Lcom/google/android/apps/gmm/cardui/a/d;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 608
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "cardui_action_delegate"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 618
    :goto_0
    return-object v0

    .line 611
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 612
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "cardui_action_delegate"

    invoke-virtual {v0, v2, v3}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 613
    if-eqz v0, :cond_1

    instance-of v2, v0, Lcom/google/android/apps/gmm/cardui/a/d;

    if-nez v2, :cond_1

    .line 615
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Landroid/app/Fragment;)V

    move-object v0, v1

    .line 616
    goto :goto_0

    .line 618
    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/cardui/a/d;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 387
    sget v0, Lcom/google/android/apps/gmm/h;->ax:I

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/String;)Lcom/google/android/apps/gmm/suggest/e/a;
    .locals 4

    .prologue
    .line 344
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->codePointCount(II)I

    move-result v1

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    .line 347
    new-instance v0, Lcom/google/android/apps/gmm/suggest/e/a;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/gmm/suggest/e/a;-><init>(Ljava/lang/String;IJ)V

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/a;->a()V

    .line 538
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->n:Lcom/google/android/apps/gmm/base/activities/p;

    .line 539
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 392
    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 393
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 394
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(Ljava/lang/String;)V

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 396
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/b;->c:Lcom/google/android/apps/gmm/suggest/d/b;

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Lcom/google/android/apps/gmm/suggest/d/b;Ljava/lang/String;)V

    .line 399
    :cond_1
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_7

    .line 332
    sget v4, Lcom/google/android/apps/gmm/g;->K:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a()Lcom/google/o/h/a/dq;

    move-result-object v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getView()Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v0, :cond_6

    move v0, v2

    :goto_2
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 336
    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->f:Lcom/google/android/apps/gmm/suggest/d;

    iget-object v4, v0, Lcom/google/android/apps/gmm/suggest/d;->a:Landroid/view/View;

    if-eqz v4, :cond_3

    iget-object v4, v0, Lcom/google/android/apps/gmm/suggest/d;->e:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/suggest/l;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_8

    :cond_2
    :goto_4
    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/d;->b:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/d;->e:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/suggest/l;->d(Z)V

    .line 337
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 331
    goto :goto_0

    :cond_5
    move v0, v2

    .line 332
    goto :goto_1

    :cond_6
    move v0, v3

    goto :goto_2

    .line 334
    :cond_7
    sget v0, Lcom/google/android/apps/gmm/g;->K:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getView()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_8
    move v1, v2

    .line 336
    goto :goto_4
.end method

.method public final b(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/suggest/k;Landroid/app/Fragment;Landroid/app/Fragment;)V
    .locals 3
    .param p3    # Landroid/app/Fragment;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Landroid/app/Fragment;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Landroid/app/Fragment;",
            ":",
            "Lcom/google/android/apps/gmm/cardui/a/d;",
            "S:",
            "Landroid/app/Fragment;",
            ":",
            "Lcom/google/android/apps/gmm/suggest/a/a;",
            ">(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/android/apps/gmm/suggest/k;",
            "TC;TS;)V"
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    iget-object v1, p2, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/suggest/l;->a(Lcom/google/android/apps/gmm/suggest/l;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    iget-object v1, p2, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/startpage/d/d;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->a()Lcom/google/android/apps/gmm/suggest/e/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/suggest/e/c;->a:Lcom/google/android/apps/gmm/suggest/e/c;

    if-ne v0, v1, :cond_0

    .line 187
    sget-object v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b:Ljava/lang/String;

    const-string v1, "InputSource should not be UNKNOWN"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 191
    const-string v1, "suggest_fragment_state"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 192
    const-string v1, "suggest_fragment_odelay_content_state"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 193
    check-cast p3, Lcom/google/android/apps/gmm/cardui/a/d;

    check-cast p4, Lcom/google/android/apps/gmm/suggest/a/a;

    invoke-static {v0, p3, p4}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Landroid/os/Bundle;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/android/apps/gmm/suggest/a/a;)V

    .line 194
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->setArguments(Landroid/os/Bundle;)V

    .line 195
    return-void
.end method

.method public c()Lcom/google/android/apps/gmm/suggest/a/a;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 626
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "suggest_action_listener"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 636
    :goto_0
    return-object v0

    .line 629
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 630
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "suggest_action_listener"

    invoke-virtual {v0, v2, v3}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 631
    if-eqz v0, :cond_1

    instance-of v2, v0, Lcom/google/android/apps/gmm/suggest/a/a;

    if-nez v2, :cond_1

    .line 633
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Landroid/app/Fragment;)V

    move-object v0, v1

    .line 634
    goto :goto_0

    .line 636
    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/suggest/a/a;

    goto :goto_0
.end method

.method public f_()Lcom/google/b/f/t;
    .locals 2

    .prologue
    .line 641
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->a()Lcom/google/android/apps/gmm/suggest/e/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/suggest/e/c;->c:Lcom/google/android/apps/gmm/suggest/e/c;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->a()Lcom/google/android/apps/gmm/suggest/e/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/suggest/e/c;->d:Lcom/google/android/apps/gmm/suggest/e/c;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 642
    sget-object v0, Lcom/google/b/f/t;->aP:Lcom/google/b/f/t;

    .line 644
    :goto_1
    return-object v0

    .line 641
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 644
    :cond_2
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->f_()Lcom/google/b/f/t;

    move-result-object v0

    goto :goto_1
.end method

.method public final m()Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 1

    .prologue
    .line 693
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->h:Lcom/google/android/apps/gmm/feedback/a/d;

    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 237
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onAttach(Landroid/app/Activity;)V

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->f:Lcom/google/android/apps/gmm/suggest/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iput-object v1, v0, Lcom/google/android/apps/gmm/suggest/d;->h:Lcom/google/android/apps/gmm/base/activities/c;

    .line 239
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 243
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 246
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247
    sget-object v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x35

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "SuggestFragmentState loaded from savedInstanceState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    sget-object v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x33

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "OdelayContentState loaded from savedInstanceState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    :cond_0
    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/suggest/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/suggest/h;-><init>(Lcom/google/android/apps/gmm/suggest/SuggestFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->o:Landroid/widget/SearchView$OnQueryTextListener;

    .line 317
    new-instance v0, Lcom/google/android/apps/gmm/suggest/g/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/suggest/g/c;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->e:Lcom/google/android/apps/gmm/suggest/g/c;

    .line 319
    new-instance v3, Lcom/google/android/apps/gmm/startpage/f/i;

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/startpage/f/i;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/d/d;)V

    .line 321
    new-instance v0, Lcom/google/android/apps/gmm/startpage/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 322
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 323
    invoke-direct {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->i()Lcom/google/android/apps/gmm/cardui/a/d;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/m;-><init>(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/f/i;Lcom/google/android/apps/gmm/cardui/a/d;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    .line 324
    return-void

    .line 249
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    sget-object v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "SuggestFragmentState loaded from argument: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    sget-object v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x29

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "OdelayContentState loaded from argument: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 364
    sget v0, Lcom/google/android/apps/gmm/h;->Q:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 366
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v1, Lcom/google/android/libraries/curvular/bd;

    .line 368
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 369
    iget-object v5, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->f:Lcom/google/android/apps/gmm/suggest/d;

    sget v2, Lcom/google/android/apps/gmm/g;->dB:I

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v2

    sget v7, Lcom/google/android/apps/gmm/g;->dB:I

    if-ne v2, v7, :cond_1

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    sget v2, Lcom/google/android/apps/gmm/g;->dC:I

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iget-object v2, v5, Lcom/google/android/apps/gmm/suggest/d;->h:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v8, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast v2, Lcom/google/android/libraries/curvular/bd;

    const-class v8, Lcom/google/android/apps/gmm/suggest/c/b;

    invoke-virtual {v2, v8, v7}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    iput-object v6, v5, Lcom/google/android/apps/gmm/suggest/d;->a:Landroid/view/View;

    iput-object v7, v5, Lcom/google/android/apps/gmm/suggest/d;->b:Landroid/view/View;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v8, v5, Lcom/google/android/apps/gmm/suggest/d;->e:Lcom/google/android/apps/gmm/suggest/l;

    iget-object v8, v8, Lcom/google/android/apps/gmm/suggest/l;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v5, v2, v8}, Lcom/google/android/apps/gmm/suggest/d;->a(Ljava/util/List;Lcom/google/android/apps/gmm/x/o;)V

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    const/16 v2, 0x8

    invoke-virtual {v7, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v5, Lcom/google/android/apps/gmm/suggest/d;->e:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/suggest/l;->d(Z)V

    .line 371
    sget v2, Lcom/google/android/apps/gmm/g;->K:I

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 372
    const-class v3, Lcom/google/android/apps/gmm/suggest/c/d;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 373
    new-instance v1, Lcom/google/android/apps/gmm/suggest/i;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/gmm/suggest/i;-><init>(Lcom/google/android/apps/gmm/suggest/SuggestFragment;Landroid/view/View;)V

    .line 380
    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/startpage/m;->a(Lcom/google/android/libraries/curvular/ag;)V

    .line 383
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 574
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onDestroy()V

    .line 575
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->n:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v0, :cond_0

    .line 578
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->n:Lcom/google/android/apps/gmm/base/activities/p;

    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    .line 580
    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 437
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onDestroyView()V

    .line 438
    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 548
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/a;->b()V

    .line 551
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    .line 552
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/m;->d()V

    .line 553
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->f:Lcom/google/android/apps/gmm/suggest/d;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/d;->b:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v1, v1, Lcom/google/android/apps/gmm/suggest/d;->b:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 554
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->f:Lcom/google/android/apps/gmm/suggest/d;

    iput-object v3, v1, Lcom/google/android/apps/gmm/suggest/d;->d:Lcom/google/android/apps/gmm/suggest/b;

    .line 555
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->f:Lcom/google/android/apps/gmm/suggest/d;

    iput-object v3, v1, Lcom/google/android/apps/gmm/suggest/d;->f:Lcom/google/android/apps/gmm/suggest/a/a;

    .line 556
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->p:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 559
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(I)V

    .line 564
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->e()V

    .line 565
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->O()Lcom/google/android/apps/gmm/aa/c/a/a;

    .line 570
    return-void

    .line 559
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 455
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 458
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    .line 460
    if-eqz v0, :cond_0

    .line 461
    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(Lcom/google/android/apps/gmm/map/b/a/r;)V

    .line 462
    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/r;)V

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    .line 468
    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->p:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 469
    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->f:Lcom/google/android/apps/gmm/suggest/d;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/suggest/d;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/google/android/apps/gmm/suggest/d;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 470
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/m;->b()V

    .line 471
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->f:Lcom/google/android/apps/gmm/suggest/d;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c()Lcom/google/android/apps/gmm/suggest/a/a;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/suggest/d;->f:Lcom/google/android/apps/gmm/suggest/a/a;

    .line 473
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 476
    new-instance v2, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    const/4 v3, 0x0

    .line 477
    iget-object v4, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v3, v4, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v3, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 478
    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 479
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/google/android/apps/gmm/base/activities/z;->a:Lcom/google/android/apps/gmm/base/activities/z;

    :goto_0
    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->u:Lcom/google/android/apps/gmm/base/activities/z;

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    .line 481
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->j()I

    move-result v0

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    .line 482
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->g:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 483
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    .line 484
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 485
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 486
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->e:Lcom/google/android/apps/gmm/suggest/g/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->o:Landroid/widget/SearchView$OnQueryTextListener;

    iput-object v3, v0, Lcom/google/android/apps/gmm/suggest/g/c;->a:Landroid/widget/SearchView$OnQueryTextListener;

    .line 487
    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->e:Lcom/google/android/apps/gmm/suggest/g/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    :cond_2
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/suggest/g/c;->a(Ljava/lang/String;)V

    .line 488
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->e:Lcom/google/android/apps/gmm/suggest/g/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/suggest/l;->k()I

    move-result v3

    iput v3, v0, Lcom/google/android/apps/gmm/suggest/g/c;->n:I

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->e:Lcom/google/android/apps/gmm/suggest/g/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/startpage/d/d;->b()Z

    move-result v3

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/suggest/g/c;->o:Z

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_7

    :cond_3
    move v0, v1

    :goto_1
    if-nez v0, :cond_4

    .line 492
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->e:Lcom/google/android/apps/gmm/suggest/g/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/suggest/l;->c()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/apps/gmm/suggest/g/c;->j:Ljava/lang/String;

    .line 494
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->l()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 495
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->e:Lcom/google/android/apps/gmm/suggest/g/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/suggest/l;->m()I

    move-result v3

    iput v3, v0, Lcom/google/android/apps/gmm/suggest/g/c;->k:I

    .line 496
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->e:Lcom/google/android/apps/gmm/suggest/g/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    .line 497
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/suggest/l;->n()I

    move-result v3

    .line 496
    iput v3, v0, Lcom/google/android/apps/gmm/suggest/g/c;->m:I

    .line 499
    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/base/activities/a/a;->b:Lcom/google/android/apps/gmm/base/activities/a/a;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->t:Lcom/google/android/apps/gmm/base/activities/a/a;

    .line 500
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->e:Lcom/google/android/apps/gmm/suggest/g/c;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->f:Lcom/google/android/apps/gmm/base/l/ai;

    .line 501
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 502
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->n:Lcom/google/android/apps/gmm/base/activities/p;

    .line 503
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->n:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 505
    new-instance v0, Lcom/google/android/apps/gmm/suggest/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/suggest/j;-><init>(Lcom/google/android/apps/gmm/suggest/SuggestFragment;)V

    .line 514
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->f:Lcom/google/android/apps/gmm/suggest/d;

    iput-object v0, v1, Lcom/google/android/apps/gmm/suggest/d;->d:Lcom/google/android/apps/gmm/suggest/b;

    .line 530
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/suggest/e/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Lcom/google/android/apps/gmm/suggest/e/a;)V

    .line 531
    return-void

    .line 479
    :cond_6
    sget-object v0, Lcom/google/android/apps/gmm/base/activities/z;->b:Lcom/google/android/apps/gmm/base/activities/z;

    goto/16 :goto_0

    .line 491
    :cond_7
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 584
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 585
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "suggest_fragment_state"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 586
    const-string v0, "suggest_fragment_odelay_content_state"

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 587
    invoke-direct {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->i()Lcom/google/android/apps/gmm/cardui/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c()Lcom/google/android/apps/gmm/suggest/a/a;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Landroid/os/Bundle;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/android/apps/gmm/suggest/a/a;)V

    .line 588
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 442
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onStart()V

    .line 444
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b()V

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->m:Lcom/google/android/apps/gmm/suggest/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/d;->a()V

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->m:Lcom/google/android/apps/gmm/suggest/d/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/suggest/l;->p()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/suggest/d/d;->a(Z)V

    .line 450
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->m:Lcom/google/android/apps/gmm/suggest/d/d;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/suggest/d/d;->a(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 451
    return-void
.end method
