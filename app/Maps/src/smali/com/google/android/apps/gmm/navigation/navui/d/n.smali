.class Lcom/google/android/apps/gmm/navigation/navui/d/n;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/navui/d/m;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/d/m;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/j/ae;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-boolean v1, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->n:Z

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->c:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/o;->b()V

    .line 355
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/a;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    sget-object v1, Lcom/google/b/f/b/a/ah;->f:Lcom/google/b/f/b/a/ah;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a(Lcom/google/b/f/b/a/ah;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->c:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/o;->a()V

    .line 346
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 10
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 277
    sget-object v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a:Ljava/lang/String;

    .line 278
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_1

    move v0, v3

    :goto_0
    if-nez v0, :cond_2

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    sget-object v1, Lcom/google/b/f/b/a/ah;->i:Lcom/google/b/f/b/a/ah;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    if-nez v2, :cond_0

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a(Lcom/google/b/f/b/a/ah;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->c:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/o;->a()V

    .line 339
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v4

    .line 278
    goto :goto_0

    .line 282
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 283
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->f:Z

    if-eqz v2, :cond_4

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    sget-object v1, Lcom/google/b/f/b/a/ah;->h:Lcom/google/b/f/b/a/ah;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    if-nez v2, :cond_0

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a(Lcom/google/b/f/b/a/ah;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->c:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/o;->a()V

    goto :goto_1

    .line 287
    :cond_4
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->e:Z

    if-eqz v2, :cond_5

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    sget-object v1, Lcom/google/b/f/b/a/ah;->g:Lcom/google/b/f/b/a/ah;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    if-nez v2, :cond_0

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a(Lcom/google/b/f/b/a/ah;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->c:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/o;->a()V

    goto :goto_1

    .line 293
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/d/m;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-nez v2, :cond_a

    .line 294
    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget v5, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->c:I

    if-gez v5, :cond_6

    move-object v2, v1

    :goto_2
    if-nez v2, :cond_7

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    sget-object v1, Lcom/google/b/f/b/a/ah;->a:Lcom/google/b/f/b/a/ah;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    if-nez v2, :cond_0

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a(Lcom/google/b/f/b/a/ah;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->c:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/o;->a()V

    goto :goto_1

    .line 294
    :cond_6
    iget-object v5, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->c:I

    aget-object v2, v5, v2

    goto :goto_2

    .line 299
    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v6, v5, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v5, v5, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v5, v6, v5

    iput-object v5, v2, Lcom/google/android/apps/gmm/navigation/navui/d/m;->h:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 300
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget v6, v5, Lcom/google/android/apps/gmm/navigation/g/b/i;->c:I

    if-gez v6, :cond_8

    :goto_3
    iput-object v1, v2, Lcom/google/android/apps/gmm/navigation/navui/d/m;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 302
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/d/m;->d:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/apps/gmm/l;->oC:I

    new-array v6, v3, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    .line 303
    iget-object v7, v7, Lcom/google/android/apps/gmm/navigation/navui/d/m;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v7, v7, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    aput-object v7, v6, v4

    .line 302
    invoke-virtual {v2, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/gmm/navigation/navui/d/m;->f:Ljava/lang/String;

    .line 306
    new-instance v1, Lcom/google/android/apps/gmm/navigation/g/b/a;

    .line 307
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/b;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/d/m;->h:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/navui/d/m;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    invoke-direct {v1, v0, v2, v5}, Lcom/google/android/apps/gmm/navigation/g/b/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/navigation/g/b/k;Lcom/google/android/apps/gmm/navigation/g/b/k;)V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/a;->a:I

    iput v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->j:I

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/a;->b:I

    iput v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->k:I

    .line 310
    iget-boolean v0, v1, Lcom/google/android/apps/gmm/navigation/g/b/a;->d:Z

    if-nez v0, :cond_9

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    sget-object v1, Lcom/google/b/f/b/a/ah;->a:Lcom/google/b/f/b/a/ah;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    if-nez v2, :cond_0

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->o:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a(Lcom/google/b/f/b/a/ah;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->c:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/o;->a()V

    goto/16 :goto_1

    .line 300
    :cond_8
    iget-object v1, v5, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget v5, v5, Lcom/google/android/apps/gmm/navigation/g/b/i;->c:I

    aget-object v1, v1, v5

    goto :goto_3

    .line 314
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/d/m;->d:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/apps/gmm/l;->mw:I

    new-array v6, v3, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    .line 315
    iget-object v7, v7, Lcom/google/android/apps/gmm/navigation/navui/d/m;->e:Landroid/content/Context;

    .line 316
    iget v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/a;->c:I

    sget-object v8, Lcom/google/android/apps/gmm/shared/c/c/m;->c:Lcom/google/android/apps/gmm/shared/c/c/m;

    .line 315
    invoke-static {v7, v1, v8}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;)Landroid/text/Spanned;

    move-result-object v1

    aput-object v1, v6, v4

    .line 314
    invoke-virtual {v2, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->g:Ljava/lang/String;

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->h:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 319
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/navui/d/m;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 321
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    aget-object v2, v2, v5

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 322
    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    new-instance v6, Lcom/google/android/apps/gmm/directions/f/a/b;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/directions/f/a/b;-><init>()V

    const/4 v7, 0x2

    new-array v7, v7, [Lcom/google/android/apps/gmm/map/r/a/w;

    aput-object v0, v7, v4

    aput-object v1, v7, v3

    .line 323
    invoke-static {v3, v7}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(I[Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/map/r/a/ae;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/apps/gmm/directions/f/a/b;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    sget-object v0, Lcom/google/android/apps/gmm/map/i/r;->a:Lcom/google/android/apps/gmm/map/i/r;

    .line 324
    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/directions/f/a/b;->a(Lcom/google/android/apps/gmm/map/i/s;)Lcom/google/android/apps/gmm/directions/f/a/b;

    move-result-object v0

    .line 325
    iput-boolean v3, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->f:Z

    .line 326
    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 327
    iput-boolean v3, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->k:Z

    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/v;->d:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 328
    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->l:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 322
    iput-object v0, v5, Lcom/google/android/apps/gmm/navigation/navui/d/m;->l:Lcom/google/android/apps/gmm/directions/f/a/b;

    .line 333
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->l:Lcom/google/android/apps/gmm/directions/f/a/b;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->m:Z

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->c:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/navui/d/m;->l:Lcom/google/android/apps/gmm/directions/f/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/d/o;->a(Lcom/google/android/apps/gmm/directions/f/a/b;)V

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/n;->a:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->c:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/o;->b()V

    goto/16 :goto_1
.end method
