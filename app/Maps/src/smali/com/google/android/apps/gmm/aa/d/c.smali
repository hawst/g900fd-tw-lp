.class Lcom/google/android/apps/gmm/aa/d/c;
.super Landroid/speech/tts/UtteranceProgressListener;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/aa/d/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/aa/d/a;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/d/c;->a:Lcom/google/android/apps/gmm/aa/d/a;

    invoke-direct {p0}, Landroid/speech/tts/UtteranceProgressListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDone(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 210
    sget-object v0, Lcom/google/android/apps/gmm/aa/d/a;->a:Ljava/lang/String;

    const-string v0, "UtteranceProgressListener.onDone() for utterance id "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 211
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/c;->a:Lcom/google/android/apps/gmm/aa/d/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/aa/d/a;->c(Ljava/lang/String;)V

    .line 212
    return-void

    .line 210
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onError(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 216
    sget-object v0, Lcom/google/android/apps/gmm/aa/d/a;->a:Ljava/lang/String;

    const-string v0, "UtteranceProgressListener.onError() for utterance id "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 217
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/c;->a:Lcom/google/android/apps/gmm/aa/d/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/aa/d/a;->b(Ljava/lang/String;)V

    .line 218
    return-void

    .line 216
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStart(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 222
    sget-object v0, Lcom/google/android/apps/gmm/aa/d/a;->a:Ljava/lang/String;

    const-string v0, "UtteranceProgressListener.onStart() for utterance id "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 223
    :goto_0
    return-void

    .line 222
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
