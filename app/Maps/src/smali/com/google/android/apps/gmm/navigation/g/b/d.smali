.class public Lcom/google/android/apps/gmm/navigation/g/b/d;
.super Lcom/google/android/apps/gmm/navigation/g/b/b;
.source "PG"


# instance fields
.field public final d:Lcom/google/maps/g/a/hm;

.field public final e:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/g/b/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/g/b/e;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/navigation/g/b/b;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/c;)V

    .line 29
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/e;->d:Lcom/google/maps/g/a/hm;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/d;->d:Lcom/google/maps/g/a/hm;

    .line 30
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/e;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/d;->e:Ljava/lang/String;

    .line 31
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/e;->f:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/d;->f:Ljava/util/List;

    .line 32
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/g/b/d;->a()Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "requestedTravelMode"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/d;->d:Lcom/google/maps/g/a/hm;

    .line 56
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "currentRoadName"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/d;->e:Ljava/lang/String;

    .line 57
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
