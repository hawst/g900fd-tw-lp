.class public Lcom/google/android/apps/gmm/navigation/navui/a/a;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/navigation/navui/d/j;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/google/android/libraries/curvular/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/am",
            "<",
            "Lcom/google/android/apps/gmm/navigation/navui/d/j;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/google/android/libraries/curvular/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/am",
            "<",
            "Lcom/google/android/apps/gmm/navigation/navui/d/j;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lcom/google/android/libraries/curvular/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/am",
            "<",
            "Lcom/google/android/apps/gmm/navigation/navui/d/j;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 167
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/a/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/navui/a/b;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/a/a;->a:Lcom/google/android/libraries/curvular/am;

    .line 183
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/navui/a/c;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/a/a;->b:Lcom/google/android/libraries/curvular/am;

    .line 198
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/a/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/navui/a/d;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/a/a;->c:Lcom/google/android/libraries/curvular/am;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method

.method private static varargs a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 5

    .prologue
    const/4 v4, -0x2

    .line 155
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v1, 0x0

    .line 156
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 157
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/libraries/curvular/z;->c:Landroid/graphics/Typeface;

    .line 159
    sget-object v3, Lcom/google/android/libraries/curvular/g;->U:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 160
    sget v2, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/d;->aq:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/base/k/am;

    invoke-direct {v4, v2, v3}, Lcom/google/android/apps/gmm/base/k/am;-><init>(Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aq;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    .line 155
    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/br;->a([Lcom/google/android/libraries/curvular/cu;[Lcom/google/android/libraries/curvular/cu;)[Lcom/google/android/libraries/curvular/cu;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/k/aa;->v([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/content/res/Configuration;)Z
    .locals 2

    .prologue
    .line 57
    iget v0, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Landroid/content/res/Configuration;)Z
    .locals 2

    .prologue
    .line 57
    iget v0, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 14

    .prologue
    .line 64
    .line 65
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/commonui/c/a;

    const/4 v1, 0x6

    new-array v2, v1, [Lcom/google/android/libraries/curvular/cu;

    const/4 v3, 0x0

    .line 66
    iget-object v1, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/libraries/curvular/bb;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/curvular/bb;-><init>(Lcom/google/android/libraries/curvular/ay;)V

    iput-object v1, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/a/a;->p()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/libraries/curvular/br;->b(Lcom/google/android/libraries/curvular/am;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    const-wide/high16 v6, 0x4051000000000000L    # 68.0

    .line 67
    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v1

    if-eqz v1, :cond_1

    double-to-int v6, v6

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x2

    iput v6, v1, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v5, v1}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v1, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    const-wide/high16 v6, 0x4048000000000000L    # 48.0

    .line 68
    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v1

    if-eqz v1, :cond_2

    double-to-int v6, v6

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x2

    iput v6, v1, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v8, v1}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v1, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 66
    invoke-static {v4, v5, v1}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v3, 0x1

    .line 71
    iget-object v1, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/curvular/ce;

    check-cast v1, Lcom/google/android/apps/gmm/navigation/commonui/c/a;

    invoke-static {v1}, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->a(Lcom/google/android/apps/gmm/navigation/commonui/c/a;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v3, 0x2

    .line 72
    iget-object v1, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/curvular/ce;

    check-cast v1, Lcom/google/android/apps/gmm/navigation/commonui/c/a;

    invoke-static {v1}, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->c(Lcom/google/android/apps/gmm/navigation/commonui/c/a;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v3, 0x3

    .line 73
    iget-object v1, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/curvular/ce;

    check-cast v1, Lcom/google/android/apps/gmm/navigation/commonui/c/a;

    invoke-static {v1}, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->b(Lcom/google/android/apps/gmm/navigation/commonui/c/a;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v3, 0x4

    .line 76
    const/16 v1, 0x9

    new-array v4, v1, [Lcom/google/android/libraries/curvular/cu;

    const/4 v1, 0x0

    const/4 v5, -0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x1

    const/4 v5, -0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x2

    const/16 v5, 0x14

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, v5, v7, v8}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x3

    const/16 v5, 0xf

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, v5, v7, v8}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x4

    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->f()Lcom/google/android/libraries/curvular/b;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x5

    sget-object v5, Lcom/google/android/apps/gmm/navigation/navui/a/a;->b:Lcom/google/android/libraries/curvular/am;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/am;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x6

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v5, 0x7

    const/4 v1, 0x2

    new-array v6, v1, [Lcom/google/android/libraries/curvular/cu;

    const/4 v7, 0x0

    iget-object v1, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/curvular/ce;

    check-cast v1, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->q()Landroid/text/Spanned;

    move-result-object v1

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v6, v7

    const/4 v7, 0x1

    const-wide/high16 v8, 0x403a000000000000L    # 26.0

    new-instance v10, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v1

    if-eqz v1, :cond_3

    double-to-int v8, v8

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x2

    iput v8, v1, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v10, v1}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v1, Lcom/google/android/apps/gmm/base/k/j;->o:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v1, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v6, v7

    invoke-static {v6}, Lcom/google/android/apps/gmm/navigation/navui/a/a;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v1

    aput-object v1, v4, v5

    const/16 v5, 0x8

    const/4 v1, 0x2

    new-array v6, v1, [Lcom/google/android/libraries/curvular/cu;

    const/4 v7, 0x0

    iget-object v1, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/curvular/ce;

    check-cast v1, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->r()Landroid/text/Spanned;

    move-result-object v1

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v6, v7

    const/4 v7, 0x1

    const-wide/high16 v8, 0x4032000000000000L    # 18.0

    new-instance v10, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v1

    if-eqz v1, :cond_4

    double-to-int v8, v8

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x2

    iput v8, v1, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v10, v1}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v1, Lcom/google/android/apps/gmm/base/k/j;->o:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v1, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v6, v7

    invoke-static {v6}, Lcom/google/android/apps/gmm/navigation/navui/a/a;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v1

    aput-object v1, v4, v5

    new-instance v1, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v1, v4}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v4, "android.widget.LinearLayout"

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v3, 0x5

    .line 81
    const/4 v1, 0x4

    new-array v4, v1, [Lcom/google/android/libraries/curvular/cu;

    const/4 v1, 0x0

    sget-object v5, Lcom/google/android/apps/gmm/navigation/navui/a/a;->a:Lcom/google/android/libraries/curvular/am;

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x1

    const/16 v5, 0xd

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, v5, v7, v8}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x2

    sget-object v5, Lcom/google/android/apps/gmm/navigation/navui/a/a;->c:Lcom/google/android/libraries/curvular/am;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/am;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v5, 0x3

    const-wide/high16 v6, 0x4038000000000000L    # 24.0

    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v1

    if-eqz v1, :cond_5

    double-to-int v6, v6

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x2

    iput v6, v1, Landroid/util/TypedValue;->data:I

    :goto_4
    invoke-direct {v8, v1}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v1, Lcom/google/android/apps/gmm/base/k/j;->o:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v1, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v4}, Lcom/google/android/apps/gmm/navigation/navui/a/a;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v1

    aput-object v1, v2, v3

    .line 64
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->a(Lcom/google/android/apps/gmm/navigation/commonui/c/a;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v1

    .line 84
    const/16 v0, 0x9

    new-array v2, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const/4 v3, -0x2

    .line 85
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const/4 v3, -0x1

    .line 86
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const/4 v3, 0x3

    .line 87
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v5, v1, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    const/4 v6, 0x0

    invoke-direct {v4, v3, v5, v6}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x3

    const-wide/high16 v4, 0x4038000000000000L    # 24.0

    .line 88
    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v4, v5}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_6

    double-to-int v4, v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v5, 0xffffff

    and-int/2addr v4, v5

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Landroid/util/TypedValue;->data:I

    :goto_5
    invoke-direct {v6, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bm:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v3, 0x4

    const-wide/high16 v4, 0x4030000000000000L    # 16.0

    .line 89
    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v4, v5}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_7

    double-to-int v4, v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v5, 0xffffff

    and-int/2addr v4, v5

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Landroid/util/TypedValue;->data:I

    :goto_6
    invoke-direct {v6, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bh:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x5

    const/4 v3, 0x0

    .line 90
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bt:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x6

    const/4 v3, 0x0

    .line 91
    sget-object v4, Lcom/google/android/libraries/curvular/g;->C:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x7

    const/4 v3, 0x0

    .line 92
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->u:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0x8

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/a/a;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    .line 84
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v2, "android.widget.ListView"

    sget-object v3, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->d()Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->a(Lcom/google/android/libraries/curvular/cs;Lcom/google/android/libraries/curvular/cs;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    .line 67
    :cond_1
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v8

    sget-object v1, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v1}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x12

    iput v6, v1, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    .line 68
    :cond_2
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v10

    sget-object v1, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v1}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x12

    iput v6, v1, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    .line 76
    :cond_3
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v12

    sget-object v1, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v1}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x12

    iput v8, v1, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    :cond_4
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v12

    sget-object v1, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v1}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x12

    iput v8, v1, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3

    .line 81
    :cond_5
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v10

    sget-object v1, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v1}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x12

    iput v6, v1, Landroid/util/TypedValue;->data:I

    goto/16 :goto_4

    .line 88
    :cond_6
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v4, v8

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v5, 0xffffff

    and-int/2addr v4, v5

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x11

    iput v4, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_5

    .line 89
    :cond_7
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v4, v8

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v5, 0xffffff

    and-int/2addr v4, v5

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x11

    iput v4, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_6
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 5

    .prologue
    .line 57
    check-cast p2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->v()Lcom/google/android/apps/gmm/navigation/navui/d/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->a()Ljava/util/List;

    move-result-object v2

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    if-nez v1, :cond_0

    const-class v4, Lcom/google/android/apps/gmm/navigation/navui/a/s;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    invoke-virtual {p4, v4, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->a()Lcom/google/android/apps/gmm/directions/h/h;

    move-result-object v0

    const-class v4, Lcom/google/android/apps/gmm/navigation/navui/a/q;

    invoke-virtual {p4, v4, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    const-class v4, Lcom/google/android/apps/gmm/navigation/navui/a/r;

    invoke-virtual {p4, v4, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_1

    :cond_1
    return-void
.end method
