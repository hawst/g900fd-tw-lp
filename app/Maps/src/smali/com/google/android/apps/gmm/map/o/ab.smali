.class Lcom/google/android/apps/gmm/map/o/ab;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/o/ac;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/o/d;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/ae;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/apps/gmm/v/ad;

.field private final e:Lcom/google/android/apps/gmm/map/util/b/g;

.field private final f:Lcom/google/android/apps/gmm/shared/net/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/net/a/b;)V
    .locals 4

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/ab;->c:Ljava/util/List;

    .line 65
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/ab;->d:Lcom/google/android/apps/gmm/v/ad;

    .line 66
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/o/ab;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 67
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/o/ab;->f:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 69
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/ab;->b:Ljava/util/Set;

    .line 70
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ac;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/gmm/map/o/ac;-><init>(Lcom/google/android/apps/gmm/map/o/ab;Lcom/google/android/apps/gmm/map/util/b/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/ab;->a:Lcom/google/android/apps/gmm/map/o/ac;

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/ab;->a:Lcom/google/android/apps/gmm/map/o/ac;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 72
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/map/t/ac;Z)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 118
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/ab;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 119
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 120
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/ae;

    .line 121
    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/ae;->a:Lcom/google/android/apps/gmm/map/o/d;

    .line 122
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/ab;->b:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4, p1, p2}, Lcom/google/android/apps/gmm/map/o/d;->a(Lcom/google/android/apps/gmm/map/t/ac;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 123
    iget v4, v0, Lcom/google/android/apps/gmm/map/o/ae;->b:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/android/apps/gmm/map/o/ae;->b:I

    .line 124
    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/o/ae;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 126
    :cond_1
    :try_start_1
    iget v4, v0, Lcom/google/android/apps/gmm/map/o/ae;->b:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v0, Lcom/google/android/apps/gmm/map/o/ae;->b:I

    .line 127
    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/o/ae;->c:Z

    .line 128
    iget v0, v0, Lcom/google/android/apps/gmm/map/o/ae;->b:I

    if-gez v0, :cond_0

    .line 129
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 135
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/ab;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/d;

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/ab;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_a

    .line 139
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/ab;->c:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/o/ae;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/o/ae;->a:Lcom/google/android/apps/gmm/map/o/d;

    if-ne v1, v0, :cond_4

    move v1, v2

    .line 145
    :goto_3
    if-nez v1, :cond_3

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/map/o/d;->a(Lcom/google/android/apps/gmm/map/t/ac;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 146
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/ab;->c:Ljava/util/List;

    new-instance v4, Lcom/google/android/apps/gmm/map/o/ae;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/map/o/ae;-><init>(Lcom/google/android/apps/gmm/map/o/d;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 138
    :cond_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 150
    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/ab;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    move v4, v3

    :goto_4
    if-ge v4, v6, :cond_7

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/ae;

    iget-boolean v7, v0, Lcom/google/android/apps/gmm/map/o/ae;->c:Z

    if-eqz v7, :cond_9

    if-eqz v1, :cond_6

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/o/ae;->a(Lcom/google/android/apps/gmm/map/o/ae;)I

    move-result v7

    if-gez v7, :cond_9

    :cond_6
    :goto_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move-object v1, v0

    goto :goto_4

    .line 151
    :cond_7
    if-eqz v1, :cond_8

    .line 152
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/o/ae;->a:Lcom/google/android/apps/gmm/map/o/d;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/ab;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, p1, v3}, Lcom/google/android/apps/gmm/map/o/d;->a(Lcom/google/android/apps/gmm/map/t/ac;Lcom/google/android/apps/gmm/map/util/b/g;)V

    .line 155
    const/4 v0, 0x0

    iput v0, v1, Lcom/google/android/apps/gmm/map/o/ae;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v2

    .line 159
    :goto_6
    monitor-exit p0

    return v0

    :cond_8
    move v0, v3

    goto :goto_6

    :cond_9
    move-object v0, v1

    goto :goto_5

    :cond_a
    move v1, v3

    goto :goto_3
.end method

.method private declared-synchronized b()V
    .locals 3

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/ab;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/d;

    .line 89
    const/16 v2, 0x20

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 91
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/ab;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/ab;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/o/ab;->b()V

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/ab;->d:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/ab;->a:Lcom/google/android/apps/gmm/map/o/ac;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    monitor-exit p0

    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/o/d;)V
    .locals 1

    .prologue
    .line 75
    monitor-enter p0

    const/16 v0, 0x20

    :try_start_0
    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/o/d;->a(I)V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/ab;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit p0

    return-void

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Lcom/google/android/apps/gmm/map/t/ac;Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 102
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    long-to-double v2, v2

    .line 103
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/ab;->f:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 104
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/net/a/b;->d()Lcom/google/android/apps/gmm/shared/net/a/t;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget-boolean v4, v4, Lcom/google/r/b/a/aqg;->p:Z

    if-nez v4, :cond_0

    .line 106
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/o/ab;->a(Lcom/google/android/apps/gmm/map/t/ac;Z)Z

    move-result v0

    .line 108
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    long-to-double v4, v4

    sub-double v2, v4, v2

    .line 109
    const-string v1, "LabelPicker"

    const-string v4, "Tap detection for %d candidates in %d labels took %4.2f microseconds"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/o/ab;->c:Ljava/util/List;

    .line 110
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/o/ab;->b:Ljava/util/Set;

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-wide v8, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v5, v6

    .line 109
    invoke-static {v1, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    monitor-exit p0

    return v0

    :cond_0
    move v0, v1

    .line 104
    goto :goto_0

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/map/o/d;)V
    .locals 1

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/ab;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 81
    const/16 v0, 0x20

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    monitor-exit p0

    return-void

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
