.class public Lcom/google/android/apps/gmm/place/c/g;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/place/i/j;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 22

    .prologue
    .line 81
    const/16 v4, 0xa

    new-array v5, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v4, 0x0

    .line 82
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v4

    const/4 v4, 0x1

    .line 84
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->c()Lcom/google/android/libraries/curvular/b;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->as:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v4

    const/4 v4, 0x2

    const/4 v6, 0x5

    .line 86
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->bH:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v4

    const/4 v4, 0x3

    .line 87
    sget v6, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v6}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v4

    const/4 v4, 0x4

    .line 88
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->c()Lcom/google/android/libraries/curvular/ar;

    move-result-object v6

    aput-object v6, v5, v4

    const/4 v4, 0x5

    const/4 v6, 0x2

    .line 89
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->aD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v4

    const/4 v4, 0x6

    const/4 v6, 0x0

    .line 90
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ae:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v4

    const/4 v6, 0x7

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 91
    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v4

    if-eqz v4, :cond_1

    double-to-int v8, v8

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x2

    iput v8, v4, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v7, v4}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v4, Lcom/google/android/libraries/curvular/g;->aw:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v5, v6

    const/16 v4, 0x8

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    .line 92
    sget-object v7, Lcom/google/android/libraries/curvular/g;->O:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v4

    const/16 v6, 0x9

    .line 93
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->e()Ljava/lang/String;

    move-result-object v4

    sget-object v7, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v5, v6

    .line 81
    new-instance v4, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v4, v5}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v5, "android.widget.TextView"

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v5

    .line 97
    const/4 v4, 0x6

    new-array v6, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v7, 0x0

    .line 98
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->h()Ljava/lang/Boolean;

    move-result-object v4

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    const/16 v9, 0x8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    sget-object v10, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    invoke-static {v4, v8, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v6, v7

    const/4 v7, 0x1

    const-wide/high16 v8, 0x4010000000000000L    # 4.0

    .line 99
    new-instance v10, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v4

    if-eqz v4, :cond_2

    double-to-int v8, v8

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v4, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v10, v4}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v6, v7

    const/4 v4, 0x2

    .line 100
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v4

    const/4 v4, 0x3

    .line 102
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->f()Lcom/google/android/libraries/curvular/ar;

    move-result-object v7

    aput-object v7, v6, v4

    const/4 v4, 0x4

    .line 103
    sget v7, Lcom/google/android/apps/gmm/d;->az:I

    invoke-static {v7}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v4

    const/4 v7, 0x5

    .line 104
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->j()Ljava/lang/String;

    move-result-object v4

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v6, v7

    .line 97
    new-instance v4, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v4, v6}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v6, "android.widget.TextView"

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v6

    .line 108
    const/4 v4, 0x2

    new-array v7, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v8, 0x0

    .line 109
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->h()Ljava/lang/Boolean;

    move-result-object v4

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    sget-object v10, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    const/16 v10, 0x8

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    sget-object v11, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    invoke-static {v4, v9, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v7, v8

    const/4 v8, 0x1

    .line 111
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->i()Ljava/lang/Float;

    move-result-object v4

    sget-object v9, Lcom/google/android/apps/gmm/base/k/j;->v:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v9, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v7, v8

    .line 108
    invoke-static {v7}, Lcom/google/android/apps/gmm/base/k/aa;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v7

    .line 115
    const/4 v4, 0x5

    new-array v8, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v9, 0x0

    .line 116
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->h()Ljava/lang/Boolean;

    move-result-object v4

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    sget-object v11, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    const/16 v11, 0x8

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    sget-object v12, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    invoke-static {v4, v10, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v8, v9

    const/4 v9, 0x1

    const-wide/high16 v10, 0x4010000000000000L    # 4.0

    .line 117
    new-instance v12, Lcom/google/android/libraries/curvular/b;

    invoke-static {v10, v11}, Lcom/google/b/g/a;->a(D)Z

    move-result v4

    if-eqz v4, :cond_3

    double-to-int v10, v10

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v10, v11

    shl-int/lit8 v10, v10, 0x8

    or-int/lit8 v10, v10, 0x1

    iput v10, v4, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v12, v4}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v8, v9

    const/4 v4, 0x2

    .line 119
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->f()Lcom/google/android/libraries/curvular/ar;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v4, 0x3

    .line 120
    sget v9, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v9}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v9

    sget-object v10, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v9, 0x4

    .line 121
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->l()Ljava/lang/String;

    move-result-object v4

    sget-object v10, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v8, v9

    .line 115
    new-instance v4, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v4, v8}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v8, "android.widget.TextView"

    sget-object v9, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v8

    .line 125
    const/4 v4, 0x4

    new-array v9, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v10, 0x0

    .line 126
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->D()Ljava/lang/Boolean;

    move-result-object v4

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    sget-object v12, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    const/16 v12, 0x8

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    invoke-static {v4, v11, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v9, v10

    const/4 v4, 0x1

    .line 128
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->f()Lcom/google/android/libraries/curvular/ar;

    move-result-object v10

    aput-object v10, v9, v4

    const/4 v4, 0x2

    .line 129
    sget v10, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v10}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v10

    sget-object v11, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v9, v4

    const/4 v10, 0x3

    .line 130
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->n()Ljava/lang/String;

    move-result-object v4

    sget-object v11, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v9, v10

    .line 125
    new-instance v4, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v4, v9}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v9, "android.widget.TextView"

    sget-object v10, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v9

    .line 134
    const/4 v4, 0x6

    new-array v10, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v4, 0x0

    .line 135
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v11

    sget-object v12, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    aput-object v11, v10, v4

    const/4 v4, 0x1

    const/4 v11, 0x1

    .line 137
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    sget-object v12, Lcom/google/android/libraries/curvular/g;->aD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    aput-object v11, v10, v4

    const/4 v4, 0x2

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    .line 138
    sget-object v12, Lcom/google/android/libraries/curvular/g;->O:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    aput-object v11, v10, v4

    const/4 v4, 0x3

    .line 139
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->f()Lcom/google/android/libraries/curvular/ar;

    move-result-object v11

    aput-object v11, v10, v4

    const/4 v4, 0x4

    .line 140
    sget v11, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v11}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v11

    sget-object v12, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    aput-object v11, v10, v4

    const/4 v11, 0x5

    .line 141
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->x()Ljava/lang/String;

    move-result-object v4

    sget-object v12, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v10, v11

    .line 134
    new-instance v4, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v4, v10}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v10, "android.widget.TextView"

    sget-object v11, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    invoke-virtual {v4, v10}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v10

    .line 147
    const/4 v4, 0x7

    new-array v11, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v12, 0x0

    .line 149
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->v()Ljava/lang/Boolean;

    move-result-object v4

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    const/4 v14, 0x4

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    invoke-static {v4, v13, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v11, v12

    const/4 v4, 0x1

    .line 150
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v4

    const/4 v4, 0x2

    .line 152
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->f()Lcom/google/android/libraries/curvular/ar;

    move-result-object v12

    aput-object v12, v11, v4

    const/4 v4, 0x3

    const/4 v12, 0x1

    .line 153
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->aD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v4

    const/4 v4, 0x4

    sget-object v12, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    .line 154
    sget-object v13, Lcom/google/android/libraries/curvular/g;->O:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v4

    const/4 v4, 0x5

    .line 155
    sget v12, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v12}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v4

    const/4 v12, 0x6

    .line 156
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->u()Ljava/lang/CharSequence;

    move-result-object v4

    sget-object v13, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v11, v12

    .line 147
    new-instance v4, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v4, v11}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v11, "android.widget.TextView"

    sget-object v12, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    invoke-virtual {v4, v11}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v11

    .line 161
    const/16 v4, 0x8

    new-array v12, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v4, 0x0

    const/4 v13, -0x1

    .line 164
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    aput-object v13, v12, v4

    const/4 v4, 0x1

    const/4 v13, -0x2

    .line 165
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    aput-object v13, v12, v4

    const/4 v13, 0x2

    .line 167
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->y()Ljava/lang/String;

    move-result-object v4

    sget-object v14, Lcom/google/android/libraries/curvular/g;->w:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v12, v13

    const/4 v4, 0x3

    .line 169
    sget-object v13, Lcom/google/android/apps/gmm/base/h/c;->b:Lcom/google/android/apps/gmm/base/h/c;

    sget-object v14, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    aput-object v13, v12, v4

    const/4 v13, 0x4

    .line 170
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->C()Lcom/google/android/libraries/curvular/cf;

    move-result-object v14

    const/4 v4, 0x0

    if-eqz v14, :cond_0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-static {v14, v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v14

    new-instance v4, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v4, v14}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_0
    sget-object v14, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v12, v13

    const/4 v13, 0x5

    .line 171
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->B()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v4

    sget-object v14, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v14, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v12, v13

    const/4 v4, 0x6

    const/16 v13, 0x30

    .line 173
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    aput-object v13, v12, v4

    const/4 v13, 0x7

    const/4 v4, 0x6

    new-array v14, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v4, 0x0

    const/4 v15, -0x2

    .line 176
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v14, v4

    const/4 v4, 0x1

    const/4 v15, -0x2

    .line 177
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v14, v4

    const/4 v15, 0x2

    const-wide/high16 v16, 0x4038000000000000L    # 24.0

    .line 179
    new-instance v18, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/b/g/a;->a(D)Z

    move-result v4

    if-eqz v4, :cond_4

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    iput v0, v4, Landroid/util/TypedValue;->data:I

    :goto_3
    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v14, v15

    const/4 v15, 0x3

    const-wide/high16 v16, 0x4038000000000000L    # 24.0

    .line 180
    new-instance v18, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/b/g/a;->a(D)Z

    move-result v4

    if-eqz v4, :cond_5

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    iput v0, v4, Landroid/util/TypedValue;->data:I

    :goto_4
    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v14, v15

    const/4 v15, 0x4

    const-wide/high16 v16, 0x4038000000000000L    # 24.0

    .line 181
    new-instance v18, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/b/g/a;->a(D)Z

    move-result v4

    if-eqz v4, :cond_6

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    iput v0, v4, Landroid/util/TypedValue;->data:I

    :goto_5
    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bm:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v14, v15

    const/4 v15, 0x5

    .line 183
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->z()Lcom/google/android/libraries/curvular/aw;

    move-result-object v4

    sget-object v16, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v14, v15

    .line 175
    new-instance v4, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v4, v14}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v14, "android.widget.ImageView"

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    invoke-virtual {v4, v14}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v4

    aput-object v4, v12, v13

    .line 162
    new-instance v4, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v4, v12}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v12, "android.widget.FrameLayout"

    sget-object v13, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    invoke-virtual {v4, v12}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v12

    .line 188
    const/4 v4, 0x7

    new-array v13, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v4, 0x0

    const/4 v14, -0x2

    .line 190
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v4

    const/4 v14, 0x1

    const-wide/high16 v16, 0x4052000000000000L    # 72.0

    .line 195
    new-instance v15, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/b/g/a;->a(D)Z

    move-result v4

    if-eqz v4, :cond_7

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    iput v0, v4, Landroid/util/TypedValue;->data:I

    :goto_6
    invoke-direct {v15, v4}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v4, Lcom/google/android/libraries/curvular/g;->aJ:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v13, v14

    const/4 v4, 0x2

    const/4 v14, 0x1

    .line 198
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v4

    const/4 v14, 0x3

    .line 200
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->A()Ljava/lang/String;

    move-result-object v4

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v13, v14

    const/4 v4, 0x4

    .line 201
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->f()Lcom/google/android/libraries/curvular/ar;

    move-result-object v14

    aput-object v14, v13, v4

    const/4 v4, 0x5

    const/4 v14, 0x1

    .line 202
    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bC:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v4

    const/4 v4, 0x6

    .line 203
    sget v14, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v4

    .line 189
    new-instance v4, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v4, v13}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v13, "android.widget.TextView"

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    invoke-virtual {v4, v13}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v13

    .line 206
    const/16 v4, 0x10

    new-array v14, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v4, 0x0

    const/4 v15, -0x1

    .line 207
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v14, v4

    const/4 v4, 0x1

    const/4 v15, -0x2

    .line 208
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v14, v4

    const/4 v15, 0x2

    const-wide/high16 v16, 0x4057000000000000L    # 92.0

    .line 210
    new-instance v18, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/b/g/a;->a(D)Z

    move-result v4

    if-eqz v4, :cond_8

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    iput v0, v4, Landroid/util/TypedValue;->data:I

    :goto_7
    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v4, Lcom/google/android/libraries/curvular/g;->aH:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v14, v15

    const/4 v4, 0x3

    .line 212
    sget v15, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v15}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v14, v4

    const/4 v4, 0x4

    .line 213
    sget-object v15, Lcom/google/android/apps/gmm/base/h/c;->b:Lcom/google/android/apps/gmm/base/h/c;

    sget-object v16, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v14, v4

    const/4 v15, 0x5

    .line 214
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->p()Lcom/google/android/libraries/curvular/cg;

    move-result-object v4

    sget-object v16, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v14, v15

    const/4 v15, 0x6

    .line 215
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v4

    sget-object v16, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    move-object/from16 v0, v16

    invoke-static {v0, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v14, v15

    const/4 v4, 0x7

    const/4 v15, 0x3

    new-array v15, v15, [Lcom/google/android/libraries/curvular/cu;

    const/16 v16, 0x0

    const/16 v17, 0x14

    .line 218
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    new-instance v18, Lcom/google/android/libraries/curvular/i;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move-object/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static/range {v17 .. v18}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    const/16 v17, 0xa

    .line 219
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    new-instance v18, Lcom/google/android/libraries/curvular/i;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move-object/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static/range {v17 .. v18}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x2

    const/16 v17, 0x10

    .line 220
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    new-instance v18, Lcom/google/android/libraries/curvular/i;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    iget-object v0, v13, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move-object/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static/range {v17 .. v18}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v17

    aput-object v17, v15, v16

    .line 217
    invoke-virtual {v5, v15}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v15

    aput-object v15, v14, v4

    const/16 v4, 0x8

    const/4 v15, 0x2

    new-array v15, v15, [Lcom/google/android/libraries/curvular/cu;

    const/16 v16, 0x0

    const/16 v17, 0x14

    .line 224
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    new-instance v18, Lcom/google/android/libraries/curvular/i;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move-object/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static/range {v17 .. v18}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    const/16 v17, 0x3

    .line 225
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    new-instance v18, Lcom/google/android/libraries/curvular/i;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    iget-object v0, v5, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move-object/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static/range {v17 .. v18}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v17

    aput-object v17, v15, v16

    .line 223
    invoke-virtual {v6, v15}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v15

    aput-object v15, v14, v4

    const/16 v4, 0x9

    const/4 v15, 0x3

    new-array v15, v15, [Lcom/google/android/libraries/curvular/cu;

    const/16 v16, 0x0

    const/16 v17, 0x11

    .line 229
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    new-instance v18, Lcom/google/android/libraries/curvular/i;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    iget-object v0, v6, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move-object/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static/range {v17 .. v18}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    const/16 v17, 0x6

    .line 230
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    new-instance v18, Lcom/google/android/libraries/curvular/i;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    iget-object v0, v6, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move-object/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static/range {v17 .. v18}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x2

    const/16 v17, 0x8

    .line 231
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    new-instance v18, Lcom/google/android/libraries/curvular/i;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    iget-object v0, v6, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move-object/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static/range {v17 .. v18}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v17

    aput-object v17, v15, v16

    .line 228
    invoke-virtual {v7, v15}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v15

    aput-object v15, v14, v4

    const/16 v4, 0xa

    const/4 v15, 0x2

    new-array v15, v15, [Lcom/google/android/libraries/curvular/cu;

    const/16 v16, 0x0

    const/16 v17, 0x11

    .line 235
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    new-instance v18, Lcom/google/android/libraries/curvular/i;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    iget-object v7, v7, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v19

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v7, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v18

    invoke-static {v7, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v15, v16

    const/4 v7, 0x1

    const/16 v16, 0x3

    .line 236
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    new-instance v17, Lcom/google/android/libraries/curvular/i;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v16

    iget-object v0, v5, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v16

    move-object/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v16, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static/range {v16 .. v17}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v15, v7

    .line 234
    invoke-virtual {v8, v15}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v7

    aput-object v7, v14, v4

    const/16 v4, 0xb

    const/4 v7, 0x2

    new-array v7, v7, [Lcom/google/android/libraries/curvular/cu;

    const/4 v15, 0x0

    const/16 v16, 0x11

    .line 240
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    new-instance v17, Lcom/google/android/libraries/curvular/i;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v16

    iget-object v8, v8, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v16

    move/from16 v2, v18

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    invoke-static {v8, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v7, v15

    const/4 v8, 0x1

    const/4 v15, 0x3

    .line 241
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    new-instance v16, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    iget-object v5, v5, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v0, v15, v5, v1}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v7, v8

    .line 239
    invoke-virtual {v9, v7}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v5

    aput-object v5, v14, v4

    const/16 v4, 0xc

    const/4 v5, 0x3

    new-array v5, v5, [Lcom/google/android/libraries/curvular/cu;

    const/4 v7, 0x0

    const/16 v8, 0x14

    .line 245
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v9, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-direct {v9, v8, v15, v0}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x1

    const/4 v8, 0x3

    .line 246
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v9, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iget-object v6, v6, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    const/4 v15, 0x0

    invoke-direct {v9, v8, v6, v15}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v7

    const/4 v6, 0x2

    const/16 v7, 0x10

    .line 247
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v9, v13, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    const/4 v15, 0x0

    invoke-direct {v8, v7, v9, v15}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v7, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v5, v6

    .line 244
    invoke-virtual {v10, v5}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v5

    aput-object v5, v14, v4

    const/16 v4, 0xd

    const/4 v5, 0x3

    new-array v5, v5, [Lcom/google/android/libraries/curvular/cu;

    const/4 v6, 0x0

    const/16 v7, 0x14

    .line 251
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v9, 0x0

    const/4 v15, 0x0

    invoke-direct {v8, v7, v9, v15}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v7, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const/4 v7, 0x3

    .line 252
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v9, v10, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    const/4 v15, 0x0

    invoke-direct {v8, v7, v9, v15}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v7, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    .line 257
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->c()Lcom/google/android/libraries/curvular/b;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->an:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v5, v6

    .line 250
    invoke-virtual {v11, v5}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v5

    aput-object v5, v14, v4

    const/16 v4, 0xe

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/libraries/curvular/cu;

    const/4 v6, 0x0

    const/16 v7, 0x15

    .line 261
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v9, 0x0

    const/4 v15, 0x0

    invoke-direct {v8, v7, v9, v15}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v7, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v5, v6

    .line 260
    invoke-virtual {v12, v5}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v5

    aput-object v5, v14, v4

    const/16 v5, 0xf

    const/4 v4, 0x3

    new-array v6, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v4, 0x0

    const/16 v7, 0x15

    .line 265
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v9, 0x0

    const/4 v12, 0x0

    invoke-direct {v8, v7, v9, v12}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v7, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v4

    const/4 v7, 0x1

    .line 271
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->h()Ljava/lang/Boolean;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/j;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/j;->D()Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v8, v4}, Lcom/google/android/libraries/curvular/br;->b(Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    const/4 v8, 0x4

    .line 273
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v9, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iget-object v10, v10, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    const/4 v12, 0x0

    invoke-direct {v9, v8, v10, v12}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    const/4 v9, 0x4

    .line 276
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    new-instance v10, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iget-object v11, v11, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    const/4 v12, 0x0

    invoke-direct {v10, v9, v11, v12}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v9, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    .line 270
    invoke-static {v4, v8, v9}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v6, v7

    const/4 v4, 0x2

    .line 279
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->c()Lcom/google/android/libraries/curvular/b;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->an:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v4

    .line 264
    invoke-virtual {v13, v6}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v4

    aput-object v4, v14, v5

    .line 206
    new-instance v4, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v4, v14}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v5, "android.widget.RelativeLayout"

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v4

    return-object v4

    .line 91
    :cond_1
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v10

    sget-object v4, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v4}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x12

    iput v8, v4, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    .line 99
    :cond_2
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v12

    sget-object v4, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v4}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v4, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    .line 117
    :cond_3
    const-wide/high16 v14, 0x4060000000000000L    # 128.0

    mul-double/2addr v10, v14

    sget-object v4, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v10, v11, v4}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v10

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v10, v11

    shl-int/lit8 v10, v10, 0x8

    or-int/lit8 v10, v10, 0x11

    iput v10, v4, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    .line 179
    :cond_4
    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    mul-double v16, v16, v20

    sget-object v4, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v4}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v16

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x11

    move/from16 v0, v16

    iput v0, v4, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3

    .line 180
    :cond_5
    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    mul-double v16, v16, v20

    sget-object v4, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v4}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v16

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x11

    move/from16 v0, v16

    iput v0, v4, Landroid/util/TypedValue;->data:I

    goto/16 :goto_4

    .line 181
    :cond_6
    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    mul-double v16, v16, v20

    sget-object v4, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v4}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v16

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x11

    move/from16 v0, v16

    iput v0, v4, Landroid/util/TypedValue;->data:I

    goto/16 :goto_5

    .line 195
    :cond_7
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v16, v16, v18

    sget-object v4, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v4}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v16

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x11

    move/from16 v0, v16

    iput v0, v4, Landroid/util/TypedValue;->data:I

    goto/16 :goto_6

    .line 210
    :cond_8
    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    mul-double v16, v16, v20

    sget-object v4, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v4}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v16

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x11

    move/from16 v0, v16

    iput v0, v4, Landroid/util/TypedValue;->data:I

    goto/16 :goto_7
.end method
