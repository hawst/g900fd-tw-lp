.class public Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/base/activities/c;

.field b:Z

.field c:Lcom/google/android/apps/gmm/tutorial/b;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/tutorial/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 56
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    iput-object p1, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    iput-object p1, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    iput-object p1, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 67
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Lcom/google/android/apps/gmm/tutorial/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Lcom/google/android/apps/gmm/tutorial/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->b:Z

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->d:Ljava/util/List;

    .line 72
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 73
    iget-object v2, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->d:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/tutorial/c;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/gmm/tutorial/c;-><init>(Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;Landroid/view/View;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 75
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->c:Lcom/google/android/apps/gmm/tutorial/b;

    .line 76
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 184
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 186
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->c:Lcom/google/android/apps/gmm/tutorial/b;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->c:Lcom/google/android/apps/gmm/tutorial/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/tutorial/b;->b()V

    .line 189
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 15

    .prologue
    .line 80
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 81
    const/4 v2, 0x0

    .line 82
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_2

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->d:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/tutorial/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/tutorial/c;->a:Landroid/view/View;

    .line 84
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 85
    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 86
    const/4 v3, 0x0

    aget v3, v0, v3

    .line 87
    const/4 v5, 0x1

    aget v5, v0, v5

    .line 88
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->getLocationOnScreen([I)V

    .line 89
    const/4 v6, 0x0

    aget v6, v0, v6

    sub-int v6, v3, v6

    .line 90
    const/4 v3, 0x1

    aget v0, v0, v3

    sub-int v3, v5, v0

    .line 91
    instance-of v0, v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 92
    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget v2, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->h:I

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v3

    aget v3, v5, v3

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getTop()I

    move-result v0

    add-int/2addr v0, v2

    move v2, v0

    move-object v3, v1

    .line 97
    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int v5, v6, v0

    .line 98
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int v1, v2, v0

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->d:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/tutorial/c;

    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7, v6, v2, v5, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v7, v0, Lcom/google/android/apps/gmm/tutorial/c;->b:Landroid/graphics/Rect;

    .line 82
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move-object v2, v3

    goto :goto_0

    .line 94
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v5

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v7, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    if-eqz v7, :cond_1

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->m:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    :goto_2
    if-ne v5, v0, :cond_3

    if-eqz v2, :cond_3

    move-object v0, v2

    .line 95
    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget v3, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->h:I

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v7, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v5

    aget v5, v7, v5

    sub-int/2addr v3, v5

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getTop()I

    move-result v0

    add-int/2addr v0, v3

    int-to-double v8, v0

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->a:Lcom/google/android/apps/gmm/base/activities/c;

    const/16 v3, 0x40

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v0

    int-to-double v12, v0

    mul-double/2addr v10, v12

    sub-double/2addr v8, v10

    double-to-int v0, v8

    move-object v3, v2

    move v2, v0

    goto :goto_1

    .line 94
    :cond_1
    const/4 v0, -0x1

    goto :goto_2

    .line 101
    :cond_2
    return-void

    :cond_3
    move v14, v3

    move-object v3, v2

    move v2, v14

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 105
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/tutorial/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/c;->b:Landroid/graphics/Rect;

    .line 107
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    :goto_1
    return v2

    .line 105
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 111
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
