.class public Lcom/google/android/apps/gmm/mylocation/d/g;
.super Lcom/google/android/apps/gmm/mylocation/d/f;
.source "PG"


# static fields
.field static final d:Lcom/google/android/apps/gmm/map/b/a/y;


# instance fields
.field private e:Lcom/google/android/apps/gmm/mylocation/d/d;

.field private f:Lcom/google/android/apps/gmm/mylocation/d/d;

.field private g:Lcom/google/android/apps/gmm/map/t/q;

.field private h:Lcom/google/android/apps/gmm/map/t/q;

.field private i:Lcom/google/android/apps/gmm/mylocation/d/d;

.field private j:Lcom/google/android/apps/gmm/mylocation/d/d;

.field private k:Lcom/google/android/apps/gmm/mylocation/d/d;

.field private l:Lcom/google/android/apps/gmm/mylocation/d/a;

.field private m:Lcom/google/android/apps/gmm/mylocation/d/a;

.field private n:F

.field private o:Lcom/google/android/apps/gmm/v/cd;

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/v;Z)V
    .locals 7

    .prologue
    .line 103
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/mylocation/d/f;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/v;ZZ)V

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->p:Z

    .line 105
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;FFLcom/google/android/apps/gmm/map/b/a/y;FFLcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;FF)V
    .locals 4

    .prologue
    .line 167
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-ne p1, v1, :cond_9

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->k:Lcom/google/android/apps/gmm/mylocation/d/d;

    const/4 v2, 0x0

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_0
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 169
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->p:Z

    .line 176
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->c:Z

    if-eqz v1, :cond_3

    .line 177
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-ne p3, v1, :cond_b

    .line 178
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->l:Lcom/google/android/apps/gmm/mylocation/d/a;

    const/4 v2, 0x0

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 179
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->i:Lcom/google/android/apps/gmm/mylocation/d/d;

    const/4 v2, 0x0

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_2
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 190
    :cond_3
    :goto_1
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-ne p6, v1, :cond_e

    .line 198
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    const/4 v2, 0x0

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_4
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 199
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    const/4 v2, 0x0

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_5
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 221
    :goto_2
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-ne p9, v1, :cond_13

    .line 222
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->g:Lcom/google/android/apps/gmm/map/t/q;

    const/4 v2, 0x0

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_6

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_6
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 223
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->h:Lcom/google/android/apps/gmm/map/t/q;

    const/4 v2, 0x0

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_7

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_7
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 232
    :goto_3
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-ne p11, v1, :cond_18

    .line 233
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->j:Lcom/google/android/apps/gmm/mylocation/d/d;

    const/4 v2, 0x0

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_8

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_8
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 240
    :goto_4
    return-void

    .line 171
    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->k:Lcom/google/android/apps/gmm/mylocation/d/d;

    const/16 v2, 0xff

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_a

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_a
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 172
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->k:Lcom/google/android/apps/gmm/mylocation/d/d;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->k:Lcom/google/android/apps/gmm/mylocation/d/d;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(F)V

    .line 174
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->p:Z

    goto/16 :goto_0

    .line 181
    :cond_b
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->l:Lcom/google/android/apps/gmm/mylocation/d/a;

    const/16 v2, 0xff

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_c

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_c
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 185
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->l:Lcom/google/android/apps/gmm/mylocation/d/a;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/gmm/mylocation/d/a;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 186
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->l:Lcom/google/android/apps/gmm/mylocation/d/a;

    invoke-virtual {v1, p4}, Lcom/google/android/apps/gmm/mylocation/d/a;->a(F)V

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->i:Lcom/google/android/apps/gmm/mylocation/d/d;

    const/16 v2, 0xff

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_d

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_d
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->i:Lcom/google/android/apps/gmm/mylocation/d/d;

    invoke-virtual {v1, p6}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 189
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->i:Lcom/google/android/apps/gmm/mylocation/d/d;

    invoke-virtual {v1, p7}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(F)V

    goto/16 :goto_1

    .line 201
    :cond_e
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    const/16 v2, 0xff

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_f

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_f
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 202
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    invoke-virtual {v1, p6}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 203
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    invoke-virtual {v1, p7}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(F)V

    .line 204
    const/4 v1, 0x0

    cmpl-float v1, p8, v1

    if-nez v1, :cond_11

    .line 205
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    const/4 v2, 0x0

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_10

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_10
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    goto/16 :goto_2

    .line 207
    :cond_11
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    const/16 v2, 0xff

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_12

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_12
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 208
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    invoke-virtual {v1, p6}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 209
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    invoke-virtual {v1, p7}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(F)V

    .line 214
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->o:Lcom/google/android/apps/gmm/v/cd;

    invoke-virtual {v1, p8, p8, p8, p8}, Lcom/google/android/apps/gmm/v/cd;->a(FFFF)V

    goto/16 :goto_2

    .line 225
    :cond_13
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->g:Lcom/google/android/apps/gmm/map/t/q;

    const/16 v2, 0xff

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_14

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_14
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 226
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->g:Lcom/google/android/apps/gmm/map/t/q;

    invoke-virtual {v1, p9}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 227
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->g:Lcom/google/android/apps/gmm/map/t/q;

    iget-boolean v2, v1, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v2, :cond_15

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_15
    iput p10, v1, Lcom/google/android/apps/gmm/map/t/q;->d:F

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/map/t/q;->f:Z

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    .line 228
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->h:Lcom/google/android/apps/gmm/map/t/q;

    const/16 v2, 0xff

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_16

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_16
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 229
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->h:Lcom/google/android/apps/gmm/map/t/q;

    invoke-virtual {v1, p9}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 230
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->h:Lcom/google/android/apps/gmm/map/t/q;

    iget-boolean v2, v1, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v2, :cond_17

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_17
    iput p10, v1, Lcom/google/android/apps/gmm/map/t/q;->d:F

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/map/t/q;->f:Z

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    goto/16 :goto_3

    .line 235
    :cond_18
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->j:Lcom/google/android/apps/gmm/mylocation/d/d;

    const/16 v2, 0xff

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_19

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_19
    int-to-byte v2, v2

    iput-byte v2, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 236
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->j:Lcom/google/android/apps/gmm/mylocation/d/d;

    invoke-virtual {v1, p11}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 237
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->j:Lcom/google/android/apps/gmm/mylocation/d/d;

    move/from16 v0, p12

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(F)V

    .line 238
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->j:Lcom/google/android/apps/gmm/mylocation/d/d;

    iget-boolean v2, v1, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v2, :cond_1a

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1a
    move/from16 v0, p13

    iput v0, v1, Lcom/google/android/apps/gmm/map/t/q;->g:F

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    goto/16 :goto_4
.end method

.method private a(Lcom/google/android/apps/gmm/mylocation/d/d;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 151
    new-instance v0, Lcom/google/android/apps/gmm/map/t/y;

    new-instance v1, Lcom/google/android/apps/gmm/map/t/s;

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->n:F

    .line 152
    iget v3, p1, Lcom/google/android/apps/gmm/mylocation/d/d;->k:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/t/s;-><init>(F)V

    sget-object v2, Lcom/google/android/apps/gmm/map/j/aa;->a:Lcom/google/android/apps/gmm/map/j/aa;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-direct {v0, v1, v2, v3, p2}, Lcom/google/android/apps/gmm/map/t/y;-><init>(Lcom/google/android/apps/gmm/map/t/x;Lcom/google/android/apps/gmm/map/j/s;Lcom/google/android/apps/gmm/map/util/b/g;Ljava/lang/String;)V

    .line 151
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 154
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/res/Resources;Z)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x4

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->a:Lcom/google/android/apps/gmm/mylocation/d/e;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->c:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(Z)Lcom/google/android/apps/gmm/map/t/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->g:Lcom/google/android/apps/gmm/map/t/q;

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->a:Lcom/google/android/apps/gmm/mylocation/d/e;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->c:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/mylocation/d/e;->b(Z)Lcom/google/android/apps/gmm/map/t/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->h:Lcom/google/android/apps/gmm/map/t/q;

    .line 117
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->a:Lcom/google/android/apps/gmm/mylocation/d/e;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->c:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/gmm/f;->fY:I

    :goto_0
    const-string v2, "Throbbing effect under the blue dot"

    sget-object v3, Lcom/google/android/apps/gmm/map/t/l;->F:Lcom/google/android/apps/gmm/map/t/l;

    invoke-virtual {v1, v0, v2, v6, v3}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(ILjava/lang/String;ILcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/mylocation/d/d;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/v/cd;

    const/4 v2, 0x2

    invoke-direct {v1, v2, v6}, Lcom/google/android/apps/gmm/v/cd;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/v/ai;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    sget-object v1, Lcom/google/android/apps/gmm/v/aj;->j:Lcom/google/android/apps/gmm/v/aj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/v/aj;)Lcom/google/android/apps/gmm/v/ai;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/cd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->o:Lcom/google/android/apps/gmm/v/cd;

    .line 119
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->a:Lcom/google/android/apps/gmm/mylocation/d/e;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->c:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/apps/gmm/f;->ga:I

    :goto_1
    const-string v2, "Direction pointer around the blue dot"

    const/4 v3, 0x5

    sget-object v4, Lcom/google/android/apps/gmm/map/t/l;->F:Lcom/google/android/apps/gmm/map/t/l;

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(ILjava/lang/String;ILcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/mylocation/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->j:Lcom/google/android/apps/gmm/mylocation/d/d;

    .line 120
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->a:Lcom/google/android/apps/gmm/mylocation/d/e;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->c:Z

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/gmm/f;->gb:I

    :goto_2
    const-string v2, "MyLocation stale blue dot"

    sget-object v3, Lcom/google/android/apps/gmm/map/t/l;->F:Lcom/google/android/apps/gmm/map/t/l;

    invoke-virtual {v1, v0, v2, v6, v3}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(ILjava/lang/String;ILcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/mylocation/d/d;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->bx:Lcom/google/b/f/t;

    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->k:Lcom/google/android/apps/gmm/mylocation/d/d;

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->a:Lcom/google/android/apps/gmm/mylocation/d/e;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->c:Z

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/apps/gmm/f;->fW:I

    :goto_3
    const-string v2, "MyLocation blue dot"

    sget-object v3, Lcom/google/android/apps/gmm/map/t/l;->F:Lcom/google/android/apps/gmm/map/t/l;

    invoke-virtual {v1, v0, v2, v6, v3}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(ILjava/lang/String;ILcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/mylocation/d/d;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->br:Lcom/google/b/f/t;

    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    sget v1, Lcom/google/android/apps/gmm/e;->az:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iget v0, v0, Lcom/google/android/apps/gmm/mylocation/d/d;->k:I

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->n:F

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    const-string v1, "my location"

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/mylocation/d/g;->a(Lcom/google/android/apps/gmm/mylocation/d/d;Ljava/lang/String;)V

    .line 132
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->c:Z

    if-eqz v0, :cond_4

    .line 133
    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->a:Lcom/google/android/apps/gmm/mylocation/d/e;

    new-instance v0, Lcom/google/android/apps/gmm/v/ar;

    iget-object v1, v3, Lcom/google/android/apps/gmm/mylocation/d/e;->b:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/apps/gmm/f;->fX:I

    iget-object v3, v3, Lcom/google/android/apps/gmm/mylocation/d/e;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/v/ar;-><init>(Landroid/content/res/Resources;ILcom/google/android/apps/gmm/v/ao;ZZ)V

    const-string v1, "iah entrypoint icon entity"

    sget-object v2, Lcom/google/android/apps/gmm/mylocation/d/b;->b:Lcom/google/android/apps/gmm/mylocation/d/b;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(Lcom/google/android/apps/gmm/v/ar;Ljava/lang/String;Lcom/google/android/apps/gmm/mylocation/d/b;)Lcom/google/android/apps/gmm/mylocation/d/a;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->bs:Lcom/google/b/f/t;

    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->l:Lcom/google/android/apps/gmm/mylocation/d/a;

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->a:Lcom/google/android/apps/gmm/mylocation/d/e;

    sget v1, Lcom/google/android/apps/gmm/f;->fZ:I

    const-string v2, "MyLocation halo blue dot"

    sget-object v3, Lcom/google/android/apps/gmm/map/t/l;->F:Lcom/google/android/apps/gmm/map/t/l;

    invoke-virtual {v0, v1, v2, v6, v3}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(ILjava/lang/String;ILcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/mylocation/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->i:Lcom/google/android/apps/gmm/mylocation/d/d;

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->k:Lcom/google/android/apps/gmm/mylocation/d/d;

    const-string v1, "my location stale point"

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/mylocation/d/g;->a(Lcom/google/android/apps/gmm/mylocation/d/d;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->h:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->g:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->i:Lcom/google/android/apps/gmm/mylocation/d/d;

    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->l:Lcom/google/android/apps/gmm/mylocation/d/a;

    iget-object v6, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->j:Lcom/google/android/apps/gmm/mylocation/d/d;

    iget-object v7, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->k:Lcom/google/android/apps/gmm/mylocation/d/d;

    invoke-static/range {v0 .. v7}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    .line 146
    :goto_4
    return-object v0

    .line 117
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/f;->e:I

    goto/16 :goto_0

    .line 119
    :cond_1
    sget v0, Lcom/google/android/apps/gmm/f;->aU:I

    goto/16 :goto_1

    .line 120
    :cond_2
    sget v0, Lcom/google/android/apps/gmm/f;->bn:I

    goto/16 :goto_2

    .line 121
    :cond_3
    sget v0, Lcom/google/android/apps/gmm/f;->d:I

    goto/16 :goto_3

    .line 146
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->h:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->g:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->j:Lcom/google/android/apps/gmm/mylocation/d/d;

    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->k:Lcom/google/android/apps/gmm/mylocation/d/d;

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    invoke-static/range {v0 .. v5}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_4
.end method

.method public final a(Lcom/google/android/apps/gmm/mylocation/g/a;Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 30

    .prologue
    .line 248
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_1

    .line 249
    sget-object v3, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object v11, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v12, 0x0

    sget-object v13, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v15}, Lcom/google/android/apps/gmm/mylocation/d/g;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;FFLcom/google/android/apps/gmm/map/b/a/y;FFLcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;FF)V

    .line 287
    :goto_1
    return-void

    .line 248
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 258
    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->i:F

    sub-float/2addr v2, v3

    const v3, 0x3dcccccd    # 0.1f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    const/4 v2, 0x1

    .line 259
    :goto_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/gmm/mylocation/d/g;->n:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/mylocation/d/g;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    if-nez v3, :cond_3

    const/4 v3, 0x0

    :goto_3
    mul-float/2addr v4, v3

    .line 264
    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    int-to-double v6, v3

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v3, :cond_4

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    :goto_4
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-float v0, v6

    move/from16 v26, v0

    .line 265
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    if-eqz v3, :cond_5

    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    neg-float v15, v3

    .line 266
    :goto_5
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    .line 269
    if-eqz v2, :cond_6

    .line 271
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    sget-object v5, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object v11, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v12, 0x0

    sget-object v13, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v14, 0x0

    move-object/from16 v2, p0

    .line 270
    invoke-direct/range {v2 .. v15}, Lcom/google/android/apps/gmm/mylocation/d/g;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;FFLcom/google/android/apps/gmm/map/b/a/y;FFLcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;FF)V

    goto :goto_1

    .line 258
    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    .line 259
    :cond_3
    iget v3, v3, Lcom/google/android/apps/gmm/mylocation/d/d;->k:I

    int-to-float v3, v3

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    mul-float/2addr v3, v5

    goto :goto_3

    .line 264
    :cond_4
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_4

    .line 265
    :cond_5
    const/4 v15, 0x0

    goto :goto_5

    .line 279
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/d/g;->m:Lcom/google/android/apps/gmm/mylocation/d/a;

    if-nez v2, :cond_7

    const/16 v21, 0x0

    .line 280
    :goto_6
    sget-object v17, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    const/16 v18, 0x0

    .line 282
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v19, v0

    .line 283
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->k:F

    move/from16 v24, v0

    .line 284
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v2, :cond_8

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v25, v0

    :goto_7
    if-eqz v3, :cond_9

    .line 285
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v27, v0

    :goto_8
    if-eqz v3, :cond_a

    move/from16 v28, v4

    :goto_9
    move-object/from16 v16, p0

    move/from16 v20, v4

    move/from16 v23, v4

    move/from16 v29, v15

    .line 280
    invoke-direct/range {v16 .. v29}, Lcom/google/android/apps/gmm/mylocation/d/g;->a(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;FFLcom/google/android/apps/gmm/map/b/a/y;FFLcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;FF)V

    goto/16 :goto_1

    .line 279
    :cond_7
    iget v2, v2, Lcom/google/android/apps/gmm/mylocation/d/d;->k:I

    int-to-float v2, v2

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v2, v5

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    mul-float v21, v2, v5

    goto :goto_6

    .line 284
    :cond_8
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v25, v0

    goto :goto_7

    .line 285
    :cond_9
    sget-object v27, Lcom/google/android/apps/gmm/mylocation/d/g;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_8

    :cond_a
    const/16 v28, 0x0

    goto :goto_9
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 291
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/d/g;->p:Z

    return v0
.end method
