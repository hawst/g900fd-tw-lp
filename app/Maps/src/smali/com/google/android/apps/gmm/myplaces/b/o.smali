.class Lcom/google/android/apps/gmm/myplaces/b/o;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/myplaces/b/ae;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/myplaces/b/ae",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/myplaces/c/b;

.field final synthetic b:Lcom/google/android/apps/gmm/myplaces/c/h;

.field final synthetic c:J

.field final synthetic d:J

.field final synthetic e:Lcom/google/e/a/a/a/b;

.field final synthetic f:Lcom/google/android/apps/gmm/myplaces/b/n;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/myplaces/b/n;Lcom/google/android/apps/gmm/myplaces/c/b;Lcom/google/android/apps/gmm/myplaces/c/h;JJLcom/google/e/a/a/a/b;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->f:Lcom/google/android/apps/gmm/myplaces/b/n;

    iput-object p2, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    iput-object p3, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->b:Lcom/google/android/apps/gmm/myplaces/c/h;

    iput-wide p4, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->c:J

    iput-wide p6, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->d:J

    iput-object p8, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->e:Lcom/google/e/a/a/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()Ljava/lang/Void;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 178
    new-instance v2, Lcom/google/android/apps/gmm/myplaces/b/ad;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/myplaces/b/ad;-><init>()V

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/myplaces/c/b;->a()I

    move-result v0

    iput v0, v2, Lcom/google/android/apps/gmm/myplaces/b/ad;->a:I

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->b:Lcom/google/android/apps/gmm/myplaces/c/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/myplaces/c/h;->a:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/apps/gmm/myplaces/b/ad;->b:Ljava/lang/String;

    .line 181
    iget-wide v4, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->c:J

    iput-wide v4, v2, Lcom/google/android/apps/gmm/myplaces/b/ad;->c:J

    .line 182
    iget-wide v4, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->d:J

    iput-wide v4, v2, Lcom/google/android/apps/gmm/myplaces/b/ad;->d:J

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    iget-object v3, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->e:Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/myplaces/c/b;->d(Lcom/google/e/a/a/a/b;)Lcom/google/b/h/c;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/b/h/c;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    iput-object v0, v2, Lcom/google/android/apps/gmm/myplaces/b/ad;->e:Ljava/lang/Long;

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    iget-object v3, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->e:Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/myplaces/c/b;->e(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/u;

    move-result-object v3

    .line 186
    if-eqz v3, :cond_1

    iget v0, v3, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_1
    iput-object v0, v2, Lcom/google/android/apps/gmm/myplaces/b/ad;->f:Ljava/lang/Integer;

    .line 187
    if-eqz v3, :cond_2

    iget v0, v3, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_2
    iput-object v0, v2, Lcom/google/android/apps/gmm/myplaces/b/ad;->g:Ljava/lang/Integer;

    .line 188
    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/myplaces/b/ad;->h:Z

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->e:Lcom/google/e/a/a/a/b;

    iput-object v0, v2, Lcom/google/android/apps/gmm/myplaces/b/ad;->i:Lcom/google/e/a/a/a/b;

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/o;->f:Lcom/google/android/apps/gmm/myplaces/b/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Lcom/google/android/apps/gmm/myplaces/b/ad;)V

    .line 192
    return-object v1

    :cond_0
    move-object v0, v1

    .line 184
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 186
    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 187
    goto :goto_2
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/google/android/apps/gmm/myplaces/b/o;->b()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
