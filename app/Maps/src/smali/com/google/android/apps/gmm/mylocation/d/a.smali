.class public Lcom/google/android/apps/gmm/mylocation/d/a;
.super Lcom/google/android/apps/gmm/mylocation/d/d;
.source "PG"


# static fields
.field private static final A:Lcom/google/android/apps/gmm/v/m;

.field private static final z:Lcom/google/android/apps/gmm/v/cd;


# instance fields
.field public final i:Lcom/google/android/apps/gmm/mylocation/d/b;

.field public final j:Lcom/google/android/apps/gmm/v/ci;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/apps/gmm/v/cd;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/v/cd;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/d/a;->z:Lcom/google/android/apps/gmm/v/cd;

    .line 17
    new-instance v0, Lcom/google/android/apps/gmm/v/m;

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/v/m;-><init>(II)V

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/d/a;->A:Lcom/google/android/apps/gmm/v/m;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/ar;Lcom/google/android/apps/gmm/mylocation/d/b;)V
    .locals 3

    .prologue
    .line 40
    iget v0, p2, Lcom/google/android/apps/gmm/v/ar;->c:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/mylocation/d/d;-><init>(Lcom/google/android/apps/gmm/map/t/k;I)V

    .line 41
    iput-object p3, p0, Lcom/google/android/apps/gmm/mylocation/d/a;->i:Lcom/google/android/apps/gmm/mylocation/d/b;

    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/v/ci;

    const/4 v1, 0x4

    const/4 v2, 0x1

    invoke-direct {v0, p2, v1, v2}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;IZ)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/a;->j:Lcom/google/android/apps/gmm/v/ci;

    .line 44
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/ci;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 47
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_0
    move v0, v1

    :goto_0
    sget v2, Lcom/google/android/apps/gmm/v/aa;->s:I

    if-ge v0, v2, :cond_3

    const/4 v2, 0x1

    shl-int/2addr v2, v0

    iget-byte v3, p0, Lcom/google/android/apps/gmm/v/aa;->v:B

    and-int/2addr v2, v3

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    aget-object v3, v2, v0

    move v2, v1

    :goto_1
    array-length v4, v3

    if-ge v2, v4, :cond_2

    aget-object v4, v3, v2

    const/4 v5, 0x0

    aput-object v5, v3, v2

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v5, :cond_1

    if-eqz v4, :cond_1

    iget v5, v4, Lcom/google/android/apps/gmm/v/ai;->s:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v4, Lcom/google/android/apps/gmm/v/ai;->s:I

    iget-object v5, p0, Lcom/google/android/apps/gmm/v/aa;->x:Lcom/google/android/apps/gmm/v/ad;

    sget-object v6, Lcom/google/android/apps/gmm/v/ab;->a:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/gmm/v/ai;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iput-byte v1, p0, Lcom/google/android/apps/gmm/v/aa;->v:B

    .line 48
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/mylocation/d/a;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 49
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/d/a;->z:Lcom/google/android/apps/gmm/v/cd;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/mylocation/d/a;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 50
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/d/a;->A:Lcom/google/android/apps/gmm/v/m;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/mylocation/d/a;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 51
    return-void
.end method
