.class public Lcom/google/android/apps/gmm/base/views/d/g;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lcom/google/android/apps/gmm/base/views/d/l;

.field private static final b:Lcom/google/android/apps/gmm/base/views/d/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 334
    new-instance v0, Lcom/google/android/apps/gmm/base/views/d/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/d/l;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/d/g;->a:Lcom/google/android/apps/gmm/base/views/d/l;

    .line 338
    new-instance v0, Lcom/google/android/apps/gmm/base/views/d/o;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/d/o;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/d/g;->b:Lcom/google/android/apps/gmm/base/views/d/o;

    return-void
.end method

.method public static a(Landroid/content/Context;F)I
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 2

    .prologue
    .line 245
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private static a(Landroid/text/Spannable;)Landroid/text/Spannable;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 226
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v1, Landroid/text/style/URLSpan;

    invoke-interface {p0, v2, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 227
    array-length v3, v0

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 228
    invoke-interface {p0, v4}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 229
    invoke-interface {p0, v4}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    .line 230
    invoke-interface {p0, v4}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 231
    new-instance v7, Lcom/google/android/apps/gmm/base/views/d/n;

    invoke-virtual {v4}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, v4}, Lcom/google/android/apps/gmm/base/views/d/n;-><init>(Ljava/lang/String;)V

    .line 232
    invoke-interface {p0, v7, v5, v6, v2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 227
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 234
    :cond_0
    return-object p0
.end method

.method public static a(Landroid/view/View;Lcom/google/b/a/ar;)Landroid/view/View;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/google/b/a/ar",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 466
    invoke-interface {p1, p0}, Lcom/google/b/a/ar;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479
    :goto_0
    return-object p0

    .line 469
    :cond_0
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 470
    check-cast p0, Landroid/view/ViewGroup;

    .line 471
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_2

    .line 472
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 473
    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/view/View;Lcom/google/b/a/ar;)Landroid/view/View;

    move-result-object v0

    .line 474
    if-eqz v0, :cond_1

    move-object p0, v0

    .line 475
    goto :goto_0

    .line 471
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 479
    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 143
    invoke-static {p0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 144
    if-eqz p1, :cond_0

    .line 145
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 149
    new-instance v0, Lcom/google/android/apps/gmm/base/views/d/h;

    const-wide/16 v2, 0x3e8

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/google/android/apps/gmm/base/views/d/h;-><init>(Landroid/view/View;JLjava/lang/Runnable;)V

    .line 157
    const-string v0, "input_method"

    .line 158
    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 159
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;II)V
    .locals 3

    .prologue
    .line 486
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 487
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 488
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->jE:I

    new-instance v2, Lcom/google/android/apps/gmm/base/views/d/k;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/views/d/k;-><init>()V

    .line 489
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 495
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 496
    return-void
.end method

.method public static a(Landroid/text/Spannable;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 210
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 211
    new-instance v1, Lcom/google/android/apps/gmm/base/views/d/j;

    invoke-direct {v1, p2}, Lcom/google/android/apps/gmm/base/views/d/j;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0, v2, v2, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)Z

    .line 217
    if-nez p3, :cond_0

    .line 218
    invoke-static {p0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/text/Spannable;)Landroid/text/Spannable;

    .line 220
    :cond_0
    return-void
.end method

.method public static a(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 404
    instance-of v0, p0, Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 405
    check-cast p0, Landroid/widget/ListView;

    invoke-virtual {p0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 411
    :cond_0
    :goto_0
    return-void

    .line 406
    :cond_1
    instance-of v0, p0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_2

    .line 407
    check-cast p0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    goto :goto_0

    .line 408
    :cond_2
    if-eqz p0, :cond_0

    .line 409
    invoke-virtual {p0, v1, v1}, Landroid/view/View;->scrollTo(II)V

    goto :goto_0
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 181
    const-string v0, "%s %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/views/d/i;

    invoke-direct {v1, p2}, Lcom/google/android/apps/gmm/base/views/d/i;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0, v4, v4, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V

    if-nez p3, :cond_0

    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spannable;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/text/Spannable;)Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    :cond_0
    return-void
.end method

.method public static a(Landroid/app/Activity;)Z
    .locals 3

    .prologue
    .line 130
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 131
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 132
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    .line 133
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 134
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 135
    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Landroid/app/DialogFragment;Ljava/lang/String;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 421
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 422
    const/4 v0, 0x0

    .line 426
    :goto_0
    return v0

    .line 424
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 425
    invoke-virtual {p1, v0, p2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 426
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Landroid/view/View;ZIII)Z
    .locals 6

    .prologue
    .line 353
    neg-int v2, p2

    sget-object v5, Lcom/google/android/apps/gmm/base/views/d/g;->a:Lcom/google/android/apps/gmm/base/views/d/l;

    move-object v0, p0

    move v1, p1

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/view/View;ZIIILcom/google/android/apps/gmm/base/views/d/m;)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/view/View;ZIIILcom/google/android/apps/gmm/base/views/d/m;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    .line 379
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    move-object v6, p0

    .line 380
    check-cast v6, Landroid/view/ViewGroup;

    .line 381
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v8

    .line 382
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v9

    .line 383
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 385
    add-int/lit8 v0, v0, -0x1

    move v7, v0

    :goto_0
    if-ltz v7, :cond_2

    .line 388
    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 389
    add-int v2, p3, v8

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    if-lt v2, v3, :cond_1

    add-int v2, p3, v8

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v3

    if-ge v2, v3, :cond_1

    add-int v2, p4, v9

    .line 390
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    if-lt v2, v3, :cond_1

    add-int v2, p4, v9

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v3

    if-ge v2, v3, :cond_1

    add-int v2, p3, v8

    .line 392
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v3, v2, v3

    add-int v2, p4, v9

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int v4, v2, v4

    move v2, p2

    move-object v5, p5

    .line 391
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/view/View;ZIIILcom/google/android/apps/gmm/base/views/d/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397
    :cond_0
    :goto_1
    return v1

    .line 385
    :cond_1
    add-int/lit8 v0, v7, -0x1

    move v7, v0

    goto :goto_0

    .line 397
    :cond_2
    if-eqz p1, :cond_3

    invoke-interface {p5, p0, p2}, Lcom/google/android/apps/gmm/base/views/d/m;->a(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;)Z
    .locals 2

    .prologue
    .line 436
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    .line 437
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    const/4 v0, 0x0

    .line 441
    :goto_0
    return v0

    .line 440
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, p0, v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    .line 441
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;Landroid/app/Fragment;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/app/Fragment;",
            ":",
            "Lcom/google/android/apps/gmm/base/fragments/a/b;",
            ">(",
            "Lcom/google/android/apps/gmm/base/activities/c;",
            "Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;",
            "TT;)Z"
        }
    .end annotation

    .prologue
    .line 452
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    .line 453
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454
    const/4 v0, 0x0

    .line 457
    :goto_0
    return v0

    .line 456
    :cond_0
    check-cast p2, Lcom/google/android/apps/gmm/base/fragments/a/b;

    invoke-virtual {p1, p0, p2}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    .line 457
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;F)I
    .locals 1

    .prologue
    .line 272
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    div-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;I)I
    .locals 3

    .prologue
    .line 260
    int-to-float v0, p1

    const/4 v1, 0x2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v1, v0, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/View;ZIII)Z
    .locals 6

    .prologue
    .line 368
    neg-int v2, p2

    sget-object v5, Lcom/google/android/apps/gmm/base/views/d/g;->b:Lcom/google/android/apps/gmm/base/views/d/o;

    move-object v0, p0

    move v1, p1

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/view/View;ZIIILcom/google/android/apps/gmm/base/views/d/m;)Z

    move-result v0

    return v0
.end method
