.class public abstract Lcom/google/android/apps/gmm/map/b/a/bd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public f:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(I)Lcom/google/android/apps/gmm/map/b/a/y;
.end method

.method public abstract a(I[Lcom/google/android/apps/gmm/map/b/a/y;)V
.end method

.method public a(Lcom/google/android/apps/gmm/map/b/a/af;)Z
    .locals 12

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/bd;->b()Lcom/google/android/apps/gmm/map/b/a/bc;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/af;->c()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 103
    invoke-virtual {p1, v1}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/b/a/bd;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 104
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/b/a/bd;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/android/apps/gmm/map/b/a/af;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/bd;->d()I

    move-result v7

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/af;->b()I

    move-result v8

    if-eqz v7, :cond_0

    if-nez v8, :cond_2

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_6

    :cond_1
    :goto_1
    return v0

    :cond_2
    const/4 v2, 0x2

    new-array v9, v2, [Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/af;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    move v6, v1

    :goto_2
    if-ge v6, v7, :cond_5

    invoke-virtual {p0, v6, v9}, Lcom/google/android/apps/gmm/map/b/a/bd;->a(I[Lcom/google/android/apps/gmm/map/b/a/y;)V

    move v2, v1

    move-object v3, v4

    :goto_3
    if-ge v2, v8, :cond_4

    invoke-virtual {p1, v2}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    aget-object v10, v9, v1

    aget-object v11, v9, v0

    invoke-static {v10, v11, v3, v5}, Lcom/google/android/apps/gmm/map/b/a/z;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v2, v0

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    move-object v3, v5

    goto :goto_3

    :cond_4
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_2

    :cond_5
    move v2, v1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public abstract a(Lcom/google/android/apps/gmm/map/b/a/y;)Z
.end method

.method public b()Lcom/google/android/apps/gmm/map/b/a/bc;
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/bd;->c()Lcom/google/android/apps/gmm/map/b/a/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/af;->c()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/bc;-><init>(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    return-object v1
.end method

.method public abstract c()Lcom/google/android/apps/gmm/map/b/a/af;
.end method

.method public abstract d()I
.end method
