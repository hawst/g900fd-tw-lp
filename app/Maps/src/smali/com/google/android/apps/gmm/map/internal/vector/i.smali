.class Lcom/google/android/apps/gmm/map/internal/vector/i;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Z

.field b:Lcom/google/android/apps/gmm/map/internal/vector/h;

.field private c:Z

.field private d:I

.field private e:Z

.field private f:Z


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/vector/h;)V
    .locals 1

    .prologue
    .line 1973
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p1, Lcom/google/android/apps/gmm/map/internal/vector/h;->a:Z

    .line 1974
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->b:Lcom/google/android/apps/gmm/map/internal/vector/h;

    if-ne v0, p1, :cond_0

    .line 1975
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->b:Lcom/google/android/apps/gmm/map/internal/vector/h;

    .line 1977
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1978
    monitor-exit p0

    return-void

    .line 1973
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2031
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->e:Z

    if-nez v2, :cond_3

    .line 2032
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/i;->c()V

    .line 2033
    const/16 v2, 0x1f01

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v3

    .line 2034
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->d:I

    const/high16 v4, 0x20000

    if-ge v2, v4, :cond_0

    .line 2035
    const-string v2, "Q3Dimension MSM7500 "

    .line 2036
    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    move v2, v1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->a:Z

    .line 2037
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 2040
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->a:Z

    if-eqz v2, :cond_1

    const-string v2, "Adreno"

    .line 2041
    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->f:Z

    .line 2048
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2050
    :cond_3
    monitor-exit p0

    return-void

    :cond_4
    move v2, v0

    .line 2036
    goto :goto_0

    .line 2031
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 2022
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 2026
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/i;->c()V

    .line 2027
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2026
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method c()V
    .locals 3

    .prologue
    const/high16 v2, 0x20000

    const/4 v1, 0x1

    .line 2053
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->c:Z

    if-nez v0, :cond_1

    .line 2057
    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->d:I

    .line 2058
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->d:I

    if-lt v0, v2, :cond_0

    .line 2059
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->a:Z

    .line 2065
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/i;->c:Z

    .line 2067
    :cond_1
    return-void
.end method
