.class public Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Landroid/widget/TimePicker$OnTimeChangedListener;
.implements Lcom/google/android/apps/gmm/directions/views/l;
.implements Lcom/google/android/apps/gmm/directions/views/m;


# instance fields
.field c:Lcom/google/android/apps/gmm/directions/bs;

.field d:Lcom/google/android/apps/gmm/directions/br;

.field private e:Lcom/google/r/b/a/afz;

.field private f:Lcom/google/android/apps/gmm/directions/option/h;

.field private final g:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    .line 131
    new-instance v0, Lcom/google/android/apps/gmm/directions/bo;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/directions/bo;-><init>(Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->g:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final F_()Z
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    return v0
.end method

.method protected final a(Z)Lcom/google/android/apps/gmm/base/fragments/g;
    .locals 1

    .prologue
    .line 224
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/g;->c:Lcom/google/android/apps/gmm/base/fragments/g;

    return-object v0
.end method

.method public final a(III)V
    .locals 4

    .prologue
    .line 347
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 348
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 349
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/directions/bs;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 350
    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    .line 352
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/apps/gmm/directions/bs;->b:J

    .line 353
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->i()V

    .line 354
    return-void
.end method

.method public final a_(III)V
    .locals 2

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 310
    :goto_0
    return-void

    .line 308
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 309
    :goto_1
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->a(III)Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;

    move-result-object v1

    .line 308
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_1
.end method

.method i()V
    .locals 8

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 367
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    if-nez v0, :cond_0

    .line 405
    :goto_0
    return-void

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/br;->a:Landroid/widget/RadioGroup;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/bs;->a:Lcom/google/maps/g/a/hc;

    if-nez v0, :cond_4

    sget v0, Lcom/google/android/apps/gmm/g;->V:I

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    .line 376
    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/bs;->a:Lcom/google/maps/g/a/hc;

    sget-object v3, Lcom/google/maps/g/a/hc;->d:Lcom/google/maps/g/a/hc;

    if-ne v0, v3, :cond_5

    move v0, v1

    .line 378
    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v5, v3, Lcom/google/android/apps/gmm/directions/br;->e:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_6

    move v3, v4

    :goto_3
    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 380
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v5, v3, Lcom/google/android/apps/gmm/directions/br;->h:Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

    if-eqz v0, :cond_7

    move v3, v4

    :goto_4
    invoke-virtual {v5, v3}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->setVisibility(I)V

    .line 381
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v3, v3, Lcom/google/android/apps/gmm/directions/br;->g:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    move v4, v2

    :cond_1
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/directions/bs;->b:J

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/directions/f/d/c;->b(J)Ljava/util/Calendar;

    move-result-object v3

    .line 385
    const/16 v0, 0xb

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 386
    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 389
    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v5, v5, Lcom/google/android/apps/gmm/directions/br;->f:Landroid/widget/TimePicker;

    invoke-virtual {v5}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-eq v5, v0, :cond_2

    .line 390
    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v5, v5, Lcom/google/android/apps/gmm/directions/br;->f:Landroid/widget/TimePicker;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 394
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/br;->f:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v4, :cond_3

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/br;->f:Landroid/widget/TimePicker;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 398
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/br;->h:Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

    invoke-virtual {v3, v1}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/4 v6, 0x5

    .line 399
    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 398
    invoke-virtual {v0, v4, v5, v6}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->setDate(III)V

    .line 401
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v4

    .line 402
    invoke-virtual {v4}, Ljava/util/Calendar;->clear()V

    .line 403
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_8

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/br;->i:Landroid/widget/ImageButton;

    invoke-static {v4, v3}, Lcom/google/android/apps/gmm/shared/c/c/b;->d(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v3

    if-nez v3, :cond_9

    :goto_6
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_0

    .line 373
    :cond_4
    sget-object v5, Lcom/google/android/apps/gmm/directions/bq;->a:[I

    invoke-virtual {v0}, Lcom/google/maps/g/a/hc;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    sget v0, Lcom/google/android/apps/gmm/g;->V:I

    goto/16 :goto_1

    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/g;->V:I

    goto/16 :goto_1

    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/g;->o:I

    goto/16 :goto_1

    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/g;->be:I

    goto/16 :goto_1

    :cond_5
    move v0, v2

    .line 376
    goto/16 :goto_2

    :cond_6
    move v3, v2

    .line 378
    goto/16 :goto_3

    :cond_7
    move v3, v2

    .line 380
    goto/16 :goto_4

    .line 403
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_5

    :cond_9
    move v1, v2

    .line 404
    goto :goto_6

    .line 373
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2

    .prologue
    .line 314
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    sget v0, Lcom/google/android/apps/gmm/g;->V:I

    if-ne p2, v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    :goto_0
    iput-object v0, v1, Lcom/google/android/apps/gmm/directions/bs;->a:Lcom/google/maps/g/a/hc;

    .line 316
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->i()V

    .line 317
    return-void

    .line 314
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/g;->o:I

    if-ne p2, v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/hc;->c:Lcom/google/maps/g/a/hc;

    goto :goto_0

    :cond_1
    sget v0, Lcom/google/android/apps/gmm/g;->be:I

    if-ne p2, v0, :cond_2

    sget-object v0, Lcom/google/maps/g/a/hc;->d:Lcom/google/maps/g/a/hc;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 280
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/br;->i:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    .line 288
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v0

    .line 289
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    iput-wide v0, v2, Lcom/google/android/apps/gmm/directions/bs;->b:J

    .line 290
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->i()V

    goto :goto_0

    .line 288
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_1

    .line 291
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/br;->j:Landroid/widget/Button;

    if-ne p1, v0, :cond_c

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/br;->f:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->clearFocus()V

    .line 294
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->e:Lcom/google/r/b/a/afz;

    invoke-static {v4}, Lcom/google/r/b/a/afz;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v5

    iget v0, v4, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    move v0, v2

    :goto_2
    if-eqz v0, :cond_5

    iget-object v0, v4, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agt;->d()Lcom/google/r/b/a/agt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agt;

    invoke-static {v0}, Lcom/google/r/b/a/agt;->a(Lcom/google/r/b/a/agt;)Lcom/google/r/b/a/agw;

    move-result-object v0

    :goto_3
    iget-object v4, v3, Lcom/google/android/apps/gmm/directions/bs;->a:Lcom/google/maps/g/a/hc;

    if-nez v4, :cond_7

    iget v3, v0, Lcom/google/r/b/a/agw;->a:I

    and-int/lit8 v3, v3, -0x5

    iput v3, v0, Lcom/google/r/b/a/agw;->a:I

    iput-wide v8, v0, Lcom/google/r/b/a/agw;->d:J

    sget-object v3, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    if-nez v3, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    invoke-static {}, Lcom/google/r/b/a/agt;->newBuilder()Lcom/google/r/b/a/agw;

    move-result-object v0

    goto :goto_3

    :cond_6
    iget v4, v0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/r/b/a/agw;->a:I

    iget v3, v3, Lcom/google/maps/g/a/hc;->i:I

    iput v3, v0, Lcom/google/r/b/a/agw;->b:I

    :goto_4
    sget-object v3, Lcom/google/maps/g/a/fu;->b:Lcom/google/maps/g/a/fu;

    if-nez v3, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget-object v4, v3, Lcom/google/android/apps/gmm/directions/bs;->a:Lcom/google/maps/g/a/hc;

    sget-object v6, Lcom/google/maps/g/a/hc;->d:Lcom/google/maps/g/a/hc;

    if-ne v4, v6, :cond_9

    iget v4, v0, Lcom/google/r/b/a/agw;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, v0, Lcom/google/r/b/a/agw;->a:I

    iput-wide v8, v0, Lcom/google/r/b/a/agw;->d:J

    iget-object v3, v3, Lcom/google/android/apps/gmm/directions/bs;->a:Lcom/google/maps/g/a/hc;

    if-nez v3, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget v4, v0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/r/b/a/agw;->a:I

    iget v3, v3, Lcom/google/maps/g/a/hc;->i:I

    iput v3, v0, Lcom/google/r/b/a/agw;->b:I

    goto :goto_4

    :cond_9
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v6, v3, Lcom/google/android/apps/gmm/directions/bs;->b:J

    invoke-static {v6, v7}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(J)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    iget v4, v0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v0, Lcom/google/r/b/a/agw;->a:I

    iput-wide v6, v0, Lcom/google/r/b/a/agw;->d:J

    iget-object v3, v3, Lcom/google/android/apps/gmm/directions/bs;->a:Lcom/google/maps/g/a/hc;

    if-nez v3, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    iget v4, v0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/r/b/a/agw;->a:I

    iget v3, v3, Lcom/google/maps/g/a/hc;->i:I

    iput v3, v0, Lcom/google/r/b/a/agw;->b:I

    goto :goto_4

    :cond_b
    iget v4, v0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v0, Lcom/google/r/b/a/agw;->a:I

    iget v3, v3, Lcom/google/maps/g/a/fu;->c:I

    iput v3, v0, Lcom/google/r/b/a/agw;->c:I

    iget-object v3, v5, Lcom/google/r/b/a/agb;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/r/b/a/agw;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v3, Lcom/google/n/ao;->d:Z

    iget v0, v5, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v5, Lcom/google/r/b/a/agb;->a:I

    invoke-virtual {v5}, Lcom/google/r/b/a/agb;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    .line 295
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->f:Lcom/google/android/apps/gmm/directions/option/h;

    sget-object v2, Lcom/google/b/f/t;->aG:Lcom/google/b/f/t;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/directions/option/h;->a(Lcom/google/r/b/a/afz;Lcom/google/b/f/t;)V

    .line 297
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->dismiss()V

    goto/16 :goto_0

    .line 298
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/br;->k:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 299
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->dismiss()V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 160
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 163
    const-string v0, "default options"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->e:Lcom/google/r/b/a/afz;

    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v3, "listener fragment"

    .line 165
    invoke-virtual {v0, v2, v3}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/option/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->f:Lcom/google/android/apps/gmm/directions/option/h;

    .line 167
    if-eqz p1, :cond_0

    .line 168
    const-string v0, "state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/bs;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    .line 173
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->g:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 174
    return-void

    .line 170
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->e:Lcom/google/r/b/a/afz;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_2
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/directions/bs;->a(Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/directions/bs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_2

    .line 173
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v1, v2

    .line 180
    :goto_0
    sget v0, Lcom/google/android/apps/gmm/h;->u:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 182
    new-instance v4, Lcom/google/android/apps/gmm/directions/br;

    invoke-direct {v4, p0, v0}, Lcom/google/android/apps/gmm/directions/br;-><init>(Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;Landroid/view/View;)V

    iput-object v4, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    .line 184
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v4, v4, Lcom/google/android/apps/gmm/directions/br;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v4, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 185
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v4, v4, Lcom/google/android/apps/gmm/directions/br;->f:Landroid/widget/TimePicker;

    invoke-virtual {v4, p0}, Landroid/widget/TimePicker;->setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V

    .line 186
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v4, v4, Lcom/google/android/apps/gmm/directions/br;->f:Landroid/widget/TimePicker;

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/br;->i:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/br;->h:Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

    iput-object p0, v1, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->e:Lcom/google/android/apps/gmm/directions/views/m;

    .line 189
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/br;->h:Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

    iput-object p0, v1, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->f:Lcom/google/android/apps/gmm/directions/views/l;

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/br;->j:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/br;->k:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 179
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 193
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_4

    :goto_1
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/shared/net/a/h;->f:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/br;->d:Landroid/widget/RadioButton;

    if-eqz v1, :cond_6

    :goto_3
    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setVisibility(I)V

    if-nez v1, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/br;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v2

    sget v3, Lcom/google/android/apps/gmm/g;->be:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/br;->a:Landroid/widget/RadioGroup;

    sget v3, Lcom/google/android/apps/gmm/g;->V:I

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->check(I)V

    :cond_2
    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/br;->c:Landroid/widget/RadioButton;

    sget v2, Lcom/google/android/apps/gmm/f;->gB:I

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setBackgroundResource(I)V

    .line 195
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->i()V

    .line 197
    sget v1, Lcom/google/android/apps/gmm/l;->fD:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->a(Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    .line 200
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/br;->b:Landroid/widget/RadioButton;

    invoke-static {v1}, Lcom/google/android/apps/gmm/util/r;->b(Landroid/view/View;)V

    .line 201
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/br;->c:Landroid/widget/RadioButton;

    invoke-static {v1}, Lcom/google/android/apps/gmm/util/r;->b(Landroid/view/View;)V

    .line 202
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/br;->d:Landroid/widget/RadioButton;

    invoke-static {v1}, Lcom/google/android/apps/gmm/util/r;->b(Landroid/view/View;)V

    .line 205
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/br;->a:Landroid/widget/RadioGroup;

    new-instance v2, Lcom/google/android/apps/gmm/directions/bp;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/directions/bp;-><init>(Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->post(Ljava/lang/Runnable;)Z

    .line 219
    return-object v0

    .line 193
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    goto :goto_1

    :cond_5
    move v1, v3

    goto :goto_2

    :cond_6
    const/16 v3, 0x8

    goto :goto_3
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->g:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 275
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onDestroy()V

    .line 276
    return-void

    .line 273
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    .line 268
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onDestroyView()V

    .line 269
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 259
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 261
    const-string v0, "state"

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 262
    return-void
.end method

.method public onTimeChanged(Landroid/widget/TimePicker;II)V
    .locals 9

    .prologue
    const/16 v8, 0xc

    const/16 v5, 0xb

    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v4, 0x0

    .line 321
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 322
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 323
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/directions/bs;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 327
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 325
    invoke-static {p2, p3, v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/b;->a(IIII)I

    move-result v1

    .line 329
    invoke-virtual {v0, v5, p2}, Ljava/util/Calendar;->set(II)V

    .line 330
    invoke-virtual {v0, v8, p3}, Ljava/util/Calendar;->set(II)V

    .line 331
    const/16 v2, 0xd

    invoke-virtual {v0, v2, v4}, Ljava/util/Calendar;->set(II)V

    .line 332
    const/16 v2, 0xe

    invoke-virtual {v0, v2, v4}, Ljava/util/Calendar;->set(II)V

    .line 334
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/apps/gmm/directions/bs;->b:J

    .line 336
    const/4 v0, 0x1

    if-ne v1, v0, :cond_2

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/br;->h:Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

    iget v1, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->i:I

    if-ne v1, v6, :cond_0

    iget v1, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->h:I

    iget v2, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    sub-int/2addr v1, v2

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    invoke-virtual {v2, v7, v1}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b()V

    :cond_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->c:Landroid/support/v4/view/ViewPager;

    iget v0, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 342
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->i()V

    .line 343
    return-void

    .line 338
    :cond_2
    const/4 v0, -0x1

    if-ne v1, v0, :cond_1

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/br;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/br;->h:Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

    iget v1, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->i:I

    if-ne v1, v6, :cond_3

    iget v1, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->h:I

    iget v2, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    sub-int/2addr v1, v2

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->g:Ljava/util/Calendar;

    invoke-virtual {v2, v7, v1}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->a()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b()V

    :cond_3
    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->c:Landroid/support/v4/view/ViewPager;

    iget v0, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->b:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method
