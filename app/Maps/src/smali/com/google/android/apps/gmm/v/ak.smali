.class public Lcom/google/android/apps/gmm/v/ak;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public a:Lcom/google/android/apps/gmm/v/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/v/am",
            "<TT;>;"
        }
    .end annotation
.end field

.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TT;",
            "Lcom/google/android/apps/gmm/v/am",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private c:Lcom/google/android/apps/gmm/v/al;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/v/ak",
            "<TT;>.com/google/android/apps/gmm/v/al;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Z)V
    .locals 2

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    if-eqz p1, :cond_0

    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ak;->b:Ljava/util/Map;

    .line 110
    new-instance v0, Lcom/google/android/apps/gmm/v/al;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/v/al;-><init>(Lcom/google/android/apps/gmm/v/ak;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ak;->c:Lcom/google/android/apps/gmm/v/al;

    .line 112
    new-instance v0, Lcom/google/android/apps/gmm/v/am;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/v/am;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ak;->a:Lcom/google/android/apps/gmm/v/am;

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ak;->a:Lcom/google/android/apps/gmm/v/am;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ak;->a:Lcom/google/android/apps/gmm/v/am;

    iput-object v1, v0, Lcom/google/android/apps/gmm/v/am;->b:Lcom/google/android/apps/gmm/v/am;

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ak;->a:Lcom/google/android/apps/gmm/v/am;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ak;->a:Lcom/google/android/apps/gmm/v/am;

    iput-object v1, v0, Lcom/google/android/apps/gmm/v/am;->c:Lcom/google/android/apps/gmm/v/am;

    .line 115
    return-void

    .line 108
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ak;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/am;

    .line 148
    if-nez v0, :cond_0

    .line 149
    const/4 v0, 0x0

    .line 155
    :goto_0
    return v0

    .line 152
    :cond_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/v/am;->c:Lcom/google/android/apps/gmm/v/am;

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/am;->b:Lcom/google/android/apps/gmm/v/am;

    iput-object v2, v1, Lcom/google/android/apps/gmm/v/am;->b:Lcom/google/android/apps/gmm/v/am;

    .line 153
    iget-object v1, v0, Lcom/google/android/apps/gmm/v/am;->b:Lcom/google/android/apps/gmm/v/am;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/am;->c:Lcom/google/android/apps/gmm/v/am;

    iput-object v0, v1, Lcom/google/android/apps/gmm/v/am;->c:Lcom/google/android/apps/gmm/v/am;

    .line 155
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ak;->c:Lcom/google/android/apps/gmm/v/al;

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/al;->b:Lcom/google/android/apps/gmm/v/ak;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ak;->a:Lcom/google/android/apps/gmm/v/am;

    iput-object v1, v0, Lcom/google/android/apps/gmm/v/al;->a:Lcom/google/android/apps/gmm/v/am;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ak;->c:Lcom/google/android/apps/gmm/v/al;

    return-object v0
.end method
