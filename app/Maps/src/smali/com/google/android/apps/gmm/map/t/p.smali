.class public Lcom/google/android/apps/gmm/map/t/p;
.super Lcom/google/android/apps/gmm/v/aa;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/b/a;
.implements Lcom/google/android/apps/gmm/map/b/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/v/aa;",
        "Lcom/google/android/apps/gmm/map/b/a;",
        "Lcom/google/android/apps/gmm/map/b/f",
        "<",
        "Lcom/google/android/apps/gmm/v/aa;",
        ">;"
    }
.end annotation


# instance fields
.field public final b:I

.field public final c:Lcom/google/android/apps/gmm/map/t/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/google/android/apps/gmm/map/t/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/t/k;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/aa;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/t/p;->c:Lcom/google/android/apps/gmm/map/t/k;

    .line 51
    const/4 v0, 0x1

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/t/k;->a()I

    move-result v1

    shl-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/p;->b:I

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/p;->m:Lcom/google/android/apps/gmm/v/cj;

    .line 53
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/v/aa;
    .locals 0

    .prologue
    .line 118
    return-object p0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/aj;)Lcom/google/android/apps/gmm/v/ai;
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/p;->b:I

    invoke-super {p0, p1, v0}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/aj;I)Lcom/google/android/apps/gmm/v/ai;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/aj;I)Lcom/google/android/apps/gmm/v/ai;
    .locals 1

    .prologue
    .line 98
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ai;)V
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/p;->b:I

    invoke-super {p0, p1, v0}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ai;I)V

    .line 68
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ai;I)V
    .locals 1

    .prologue
    .line 93
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/co;)V
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/p;->b:I

    invoke-super {p0, p1, v0}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/co;I)V

    .line 86
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/co;I)V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
