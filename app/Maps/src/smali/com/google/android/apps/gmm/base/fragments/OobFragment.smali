.class public abstract Lcom/google/android/apps/gmm/base/fragments/OobFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# instance fields
.field private a:Z

.field private b:Lcom/google/android/apps/gmm/util/j;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Landroid/view/View;
.end method

.method public abstract b()V
.end method

.method public c()Lcom/google/android/apps/gmm/z/b/o;
    .locals 0

    .prologue
    .line 114
    return-object p0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->k()Z

    .line 61
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->a:Z

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->finish()V

    .line 66
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->a:Z

    return v0

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->A()Lcom/google/android/apps/gmm/t/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/t/a/a;->c()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 72
    if-eqz p1, :cond_0

    .line 73
    :goto_0
    const-string v0, "exitsOnBackPressed"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->a:Z

    .line 74
    new-instance v0, Lcom/google/android/apps/gmm/util/j;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/util/j;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->b:Lcom/google/android/apps/gmm/util/j;

    .line 75
    return-void

    .line 72
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 96
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->b:Lcom/google/android/apps/gmm/util/j;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/util/j;->b:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/util/j;->b:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/j;->c:Landroid/app/Activity;

    iget v0, v0, Lcom/google/android/apps/gmm/util/j;->a:I

    invoke-virtual {v1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 101
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 79
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->b:Lcom/google/android/apps/gmm/util/j;

    const/4 v1, 0x7

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/util/j;->b:Z

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/util/j;->c:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v2

    iput v2, v0, Lcom/google/android/apps/gmm/util/j;->a:I

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/util/j;->b:Z

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/util/j;->c:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 85
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 86
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->a()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    const/4 v1, 0x0

    .line 88
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    const/4 v1, 0x0

    .line 89
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->c()Lcom/google/android/apps/gmm/z/b/o;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 91
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 92
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 106
    const-string v0, "exitsOnBackPressed"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 107
    return-void
.end method
