.class public final enum Lcom/google/android/apps/gmm/l/ae;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/l/ae;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/l/ae;

.field public static final enum b:Lcom/google/android/apps/gmm/l/ae;

.field public static final enum c:Lcom/google/android/apps/gmm/l/ae;

.field public static final enum d:Lcom/google/android/apps/gmm/l/ae;

.field public static final enum e:Lcom/google/android/apps/gmm/l/ae;

.field private static final synthetic h:[Lcom/google/android/apps/gmm/l/ae;


# instance fields
.field public final f:Ljava/lang/String;

.field final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 618
    new-instance v0, Lcom/google/android/apps/gmm/l/ae;

    const-string v1, "DIRECTIONS_WIDGET"

    const-string v2, "dw"

    const-string v3, "com.google.android.apps.gmm.appwidget"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/apps/gmm/l/ae;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/l/ae;->a:Lcom/google/android/apps/gmm/l/ae;

    .line 619
    new-instance v0, Lcom/google/android/apps/gmm/l/ae;

    const-string v1, "NAVIGATE_NFC_BEAM"

    const-string v2, "n"

    const-string v3, "com.android.nfc"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/gmm/l/ae;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/l/ae;->b:Lcom/google/android/apps/gmm/l/ae;

    .line 620
    new-instance v0, Lcom/google/android/apps/gmm/l/ae;

    const-string v1, "NAVIGATE_CARD"

    const-string v2, "r"

    const-string v3, "com.google.android.googlequicksearchbox/android.intent.action.ASSIST"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/gmm/l/ae;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/l/ae;->c:Lcom/google/android/apps/gmm/l/ae;

    .line 621
    new-instance v0, Lcom/google/android/apps/gmm/l/ae;

    const-string v1, "GWS_CLICK"

    const-string v2, "s"

    const-string v3, ""

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/apps/gmm/l/ae;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/l/ae;->d:Lcom/google/android/apps/gmm/l/ae;

    .line 622
    new-instance v0, Lcom/google/android/apps/gmm/l/ae;

    const-string v1, "FOZZY_PARSE"

    const-string v2, "fp"

    const-string v3, "com.google.android.apps.gmm.voice.actions.parsing.Parser"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/apps/gmm/l/ae;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/l/ae;->e:Lcom/google/android/apps/gmm/l/ae;

    .line 617
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/gmm/l/ae;

    sget-object v1, Lcom/google/android/apps/gmm/l/ae;->a:Lcom/google/android/apps/gmm/l/ae;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/l/ae;->b:Lcom/google/android/apps/gmm/l/ae;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/l/ae;->c:Lcom/google/android/apps/gmm/l/ae;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/l/ae;->d:Lcom/google/android/apps/gmm/l/ae;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/gmm/l/ae;->e:Lcom/google/android/apps/gmm/l/ae;

    aput-object v1, v0, v8

    sput-object v0, Lcom/google/android/apps/gmm/l/ae;->h:[Lcom/google/android/apps/gmm/l/ae;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 627
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 628
    iput-object p3, p0, Lcom/google/android/apps/gmm/l/ae;->f:Ljava/lang/String;

    .line 629
    iput-object p4, p0, Lcom/google/android/apps/gmm/l/ae;->g:Ljava/lang/String;

    .line 630
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/apps/gmm/l/ae;
    .locals 5
    .param p0    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 641
    invoke-static {}, Lcom/google/android/apps/gmm/l/ae;->values()[Lcom/google/android/apps/gmm/l/ae;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 642
    iget-object v4, v0, Lcom/google/android/apps/gmm/l/ae;->f:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 646
    :goto_1
    return-object v0

    .line 641
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 646
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/l/ae;
    .locals 1

    .prologue
    .line 617
    const-class v0, Lcom/google/android/apps/gmm/l/ae;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/l/ae;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/l/ae;
    .locals 1

    .prologue
    .line 617
    sget-object v0, Lcom/google/android/apps/gmm/l/ae;->h:[Lcom/google/android/apps/gmm/l/ae;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/l/ae;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/l/ae;

    return-object v0
.end method
