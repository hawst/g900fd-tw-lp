.class public Lcom/google/android/apps/gmm/place/riddler/d/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/riddler/d/o;


# instance fields
.field private final a:Landroid/app/DialogFragment;

.field private final b:Landroid/app/Activity;

.field private final c:Lcom/google/android/apps/gmm/shared/net/a/b;


# direct methods
.method public constructor <init>(Landroid/app/DialogFragment;Landroid/app/Activity;Lcom/google/android/apps/gmm/shared/net/a/b;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/riddler/d/p;->a:Landroid/app/DialogFragment;

    .line 29
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/riddler/d/p;->b:Landroid/app/Activity;

    .line 30
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/riddler/d/p;->c:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 31
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/cf;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/p;->a:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 40
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/p;->a:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/p;->c:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/util/q;->g(Lcom/google/android/apps/gmm/shared/net/a/b;)Ljava/lang/String;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/p;->b:Landroid/app/Activity;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 61
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->dL:Lcom/google/b/f/t;

    aput-object v3, v1, v2

    .line 62
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 63
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 72
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->dM:Lcom/google/b/f/t;

    aput-object v3, v1, v2

    .line 73
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 74
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method
