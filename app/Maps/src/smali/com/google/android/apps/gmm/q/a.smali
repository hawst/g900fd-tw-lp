.class public Lcom/google/android/apps/gmm/q/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/q/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public b:Z

.field public c:Lcom/google/android/apps/gmm/q/c;

.field private final d:Lcom/google/android/apps/gmm/map/util/b/g;

.field private e:Lcom/google/android/apps/gmm/map/j/ae;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/q/a;->b:Z

    .line 37
    sget-object v0, Lcom/google/android/apps/gmm/q/c;->a:Lcom/google/android/apps/gmm/q/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/q/a;->c:Lcom/google/android/apps/gmm/q/c;

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/gmm/q/a;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/q/a;->b()V

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/q/d;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/q/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/q/a;->a:Lcom/google/android/apps/gmm/q/d;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/q/a;->a:Lcom/google/android/apps/gmm/q/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/q/d;->b()V

    .line 59
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/q/a;->a:Lcom/google/android/apps/gmm/q/d;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/q/a;->a:Lcom/google/android/apps/gmm/q/d;

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/q/a;->a:Lcom/google/android/apps/gmm/q/d;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/q/d;->a(Lcom/google/android/apps/gmm/q/a;)V

    .line 63
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/q/a;->b()V

    .line 64
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 82
    sget-object v2, Lcom/google/android/apps/gmm/q/b;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/gmm/q/a;->c:Lcom/google/android/apps/gmm/q/c;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/q/c;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 89
    iget-object v2, p0, Lcom/google/android/apps/gmm/q/a;->a:Lcom/google/android/apps/gmm/q/d;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/q/a;->a:Lcom/google/android/apps/gmm/q/d;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/q/d;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    .line 86
    goto :goto_0

    :cond_0
    move v0, v1

    .line 89
    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/q/a;->a()Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/q/a;->b:Z

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/j/ae;->a(ZZ)Lcom/google/android/apps/gmm/map/j/ae;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/google/android/apps/gmm/q/a;->e:Lcom/google/android/apps/gmm/map/j/ae;

    if-eq v0, v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/gmm/q/a;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 98
    iput-object v0, p0, Lcom/google/android/apps/gmm/q/a;->e:Lcom/google/android/apps/gmm/map/j/ae;

    .line 100
    :cond_0
    return-void
.end method
