.class Lcom/google/android/apps/gmm/place/au;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/place/aq;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/aq;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/au;->a:Lcom/google/android/apps/gmm/place/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 153
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/au;->a:Lcom/google/android/apps/gmm/place/aq;

    iget-object v0, v2, Lcom/google/android/apps/gmm/place/aq;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_0

    iget-object v1, v2, Lcom/google/android/apps/gmm/place/aq;->g:Lcom/google/android/apps/gmm/map/r/a/ap;

    if-nez v1, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->M()Lcom/google/android/apps/gmm/base/g/f;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/base/g/f;->d:Lcom/google/android/apps/gmm/base/g/f;

    if-ne v0, v1, :cond_2

    iget-object v0, v2, Lcom/google/android/apps/gmm/place/aq;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    iget-object v2, v2, Lcom/google/android/apps/gmm/place/aq;->g:Lcom/google/android/apps/gmm/map/r/a/ap;

    sget-object v5, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    move-object v4, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    invoke-interface/range {v0 .. v8}, Lcom/google/android/apps/gmm/directions/a/f;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/directions/a/g;Ljava/lang/String;Lcom/google/maps/g/hy;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/place/aq;->a:Ljava/lang/String;

    const-string v1, "Depart button should not be clicked except from station place page."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
