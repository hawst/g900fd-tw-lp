.class Lcom/google/android/apps/gmm/util/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic b:Lcom/google/android/apps/gmm/util/b;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/util/b;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/google/android/apps/gmm/util/e;->b:Lcom/google/android/apps/gmm/util/b;

    iput-object p2, p0, Lcom/google/android/apps/gmm/util/e;->a:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/e;->a:Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 161
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/e;->b:Lcom/google/android/apps/gmm/util/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/gmm/util/e;->a:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :goto_0
    return-void

    .line 163
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/util/b;->a:Ljava/lang/String;

    const-string v0, "Problem while starting activity. action: "

    iget-object v1, p0, Lcom/google/android/apps/gmm/util/e;->a:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
