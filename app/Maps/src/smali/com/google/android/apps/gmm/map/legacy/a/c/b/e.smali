.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;


# static fields
.field private static final b:F

.field private static final c:Lcom/google/android/apps/gmm/v/bd;


# instance fields
.field a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/k;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/k;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

.field private g:Lcom/google/android/apps/gmm/map/b/a/y;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 69
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0, v1, v1, v1}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/cn;->a()Lcom/google/android/apps/gmm/v/cn;

    .line 74
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/cn;->a()Lcom/google/android/apps/gmm/v/cn;

    .line 79
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0, v2, v1, v1}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/cn;->a()Lcom/google/android/apps/gmm/v/cn;

    .line 84
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0, v1, v2, v1}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/cn;->a()Lcom/google/android/apps/gmm/v/cn;

    .line 90
    const/high16 v0, 0x3ec00000    # 0.375f

    sput v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->b:F

    .line 132
    new-instance v0, Lcom/google/android/apps/gmm/v/bd;

    invoke-direct {v0, v2, v2}, Lcom/google/android/apps/gmm/v/bd;-><init>(FF)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->c:Lcom/google/android/apps/gmm/v/bd;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/f;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v5, 0xa

    const/4 v2, 0x0

    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->d:Ljava/util/List;

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->e:Ljava/util/List;

    .line 173
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    move v1, v2

    .line 242
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 243
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->d:Ljava/util/List;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/f;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/f;->a:I

    .line 244
    iget v4, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 243
    invoke-static {v0, v5, v2, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 246
    :cond_0
    :goto_1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 247
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->e:Ljava/util/List;

    .line 248
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/g;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/g;->a:I

    const/4 v3, 0x1

    .line 249
    iget v4, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 247
    invoke-static {v0, v5, v3, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 251
    :cond_1
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/map/t/e;Lcom/google/android/apps/gmm/v/ck;Lcom/google/android/apps/gmm/map/c/a;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "[",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/internal/c/cp;",
            "Lcom/google/android/apps/gmm/map/t/e;",
            "Lcom/google/android/apps/gmm/v/ck;",
            "Lcom/google/android/apps/gmm/map/c/a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;"
        }
    .end annotation

    .prologue
    .line 197
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 199
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 200
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 202
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v5

    .line 203
    instance-of v4, v5, Lcom/google/android/apps/gmm/map/internal/c/bc;

    if-eqz v4, :cond_5

    move-object v4, v5

    .line 204
    check-cast v4, Lcom/google/android/apps/gmm/map/internal/c/bc;

    .line 205
    invoke-interface {v9}, Ljava/util/List;->clear()V

    invoke-interface {v10}, Ljava/util/List;->clear()V

    const/4 v6, 0x0

    move v7, v6

    :goto_0
    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/c/bc;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v7, v6, :cond_1

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/c/bc;->a:Ljava/util/List;

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/gmm/map/l/p;

    new-instance v11, Lcom/google/android/apps/gmm/map/legacy/a/c/b/f;

    invoke-direct {v11}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/f;-><init>()V

    iget v12, v6, Lcom/google/android/apps/gmm/map/l/p;->e:I

    iget v12, v6, Lcom/google/android/apps/gmm/map/l/p;->e:I

    mul-int/lit8 v12, v12, 0x3

    iput v12, v11, Lcom/google/android/apps/gmm/map/legacy/a/c/b/f;->a:I

    iget-object v12, v6, Lcom/google/android/apps/gmm/map/l/p;->d:[F

    if-nez v12, :cond_0

    :goto_1
    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_0

    :cond_0
    iget-object v6, v6, Lcom/google/android/apps/gmm/map/l/p;->d:[F

    array-length v6, v6

    div-int/lit8 v6, v6, 0x3

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    move v7, v6

    :goto_2
    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/c/bc;->b:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v7, v6, :cond_2

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/c/bc;->b:Ljava/util/List;

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/gmm/map/l/a;

    new-instance v11, Lcom/google/android/apps/gmm/map/legacy/a/c/b/g;

    invoke-direct {v11}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/g;-><init>()V

    iget-object v12, v6, Lcom/google/android/apps/gmm/map/l/a;->b:[I

    array-length v12, v12

    div-int/lit8 v12, v12, 0x2

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/l/a;->a:[F

    array-length v6, v6

    div-int/lit8 v6, v6, 0x3

    iput v6, v11, Lcom/google/android/apps/gmm/map/legacy/a/c/b/g;->a:I

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_2

    .line 207
    :cond_2
    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/internal/c/m;->j()[I

    move-result-object v6

    array-length v7, v6

    const/4 v5, 0x0

    :goto_3
    if-ge v5, v7, :cond_4

    aget v11, v6, v5

    .line 208
    if-ltz v11, :cond_3

    move-object/from16 v0, p1

    array-length v12, v0

    if-ge v11, v12, :cond_3

    .line 209
    aget-object v11, p1, v11

    move-object/from16 v0, p7

    invoke-interface {v0, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 207
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 213
    :cond_4
    invoke-interface {v8, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    :cond_5
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    .line 219
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v11

    .line 220
    iget-object v4, v11, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v4

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v12, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v12

    mul-double/2addr v4, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    .line 221
    new-instance v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v9, v10}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/List;Ljava/util/List;)V

    .line 223
    invoke-interface/range {p5 .. p5}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v4

    .line 224
    if-eqz v4, :cond_6

    .line 226
    :cond_6
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Lcom/google/android/apps/gmm/map/internal/c/bc;

    .line 227
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/c/bc;->g()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v5

    iget-object v4, v11, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, v11, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v7, v11, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v7, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v4, v7

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    if-nez v4, :cond_8

    const/4 v4, 0x0

    :goto_4
    if-eqz v4, :cond_7

    const/4 v7, 0x1

    if-le v4, v7, :cond_9

    const/4 v4, 0x1

    :goto_5
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    aget v5, v5, v4

    const/16 v4, 0xa0

    invoke-static {v5}, Landroid/graphics/Color;->alpha(I)I

    move-result v7

    invoke-static {v7, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v5}, Landroid/graphics/Color;->red(I)I

    move-result v7

    invoke-static {v5}, Landroid/graphics/Color;->green(I)I

    move-result v9

    invoke-static {v5}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    invoke-static {v4, v7, v9, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v9

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    rsub-int/lit8 v10, v10, 0x12

    invoke-static {v7, v10}, Ljava/lang/Math;->max(II)I

    move-result v7

    int-to-double v14, v7

    invoke-static {v4, v5, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-float v10, v4

    const/4 v4, 0x0

    move v5, v4

    :goto_6
    iget-object v4, v6, Lcom/google/android/apps/gmm/map/internal/c/bc;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v5, v4, :cond_c

    iget-object v4, v6, Lcom/google/android/apps/gmm/map/internal/c/bc;->a:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/l/p;

    const-string v7, "triangle mesh"

    if-nez v4, :cond_a

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_8
    iget-object v4, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    array-length v4, v4

    goto :goto_4

    :cond_9
    const/4 v4, 0x0

    goto :goto_5

    :cond_a
    iget-object v7, v4, Lcom/google/android/apps/gmm/map/l/p;->a:Lcom/google/android/apps/gmm/v/cn;

    iget-object v7, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v13, 0x2

    aget v13, v7, v13

    iget-object v7, v4, Lcom/google/android/apps/gmm/map/l/p;->b:Lcom/google/android/apps/gmm/v/cn;

    iget-object v7, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v14, 0x2

    aget v7, v7, v14

    iget-object v14, v4, Lcom/google/android/apps/gmm/map/l/p;->a:Lcom/google/android/apps/gmm/v/cn;

    iget-object v14, v14, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v15, 0x2

    aget v14, v14, v15

    sub-float v14, v7, v14

    iget v7, v4, Lcom/google/android/apps/gmm/map/l/p;->e:I

    if-eqz v7, :cond_b

    iget v7, v4, Lcom/google/android/apps/gmm/map/l/p;->e:I

    mul-int/lit8 v15, v7, 0x3

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    move-object/from16 v16, v0

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/l/p;->d:[F

    move-object/from16 v17, v0

    const/4 v4, 0x0

    move v7, v4

    :goto_7
    if-ge v7, v15, :cond_b

    mul-int/lit8 v18, v7, 0x3

    add-int/lit8 v4, v18, 0x2

    aget v4, v16, v4

    sub-float/2addr v4, v13

    div-float v19, v4, v14

    iget-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    aget v20, v16, v18

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    add-int/lit8 v21, v18, 0x1

    aget v21, v16, v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    add-int/lit8 v22, v18, 0x2

    aget v22, v16, v22

    mul-float v22, v22, v10

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v20

    iput v0, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v0, v21

    iput v0, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v0, v22

    iput v0, v4, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->d:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v0, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    aget v4, v17, v18

    add-int/lit8 v20, v18, 0x1

    aget v20, v17, v20

    add-int/lit8 v18, v18, 0x2

    aget v18, v17, v18

    const/high16 v21, -0x1000000

    and-int v21, v21, v9

    mul-float v22, v4, v4

    mul-float v20, v20, v20

    add-float v20, v20, v22

    mul-float v22, v18, v18

    add-float v20, v20, v22

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v20, v0

    div-float v4, v4, v20

    div-float v18, v18, v20

    const/high16 v20, 0x3f800000    # 1.0f

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v22, 0x3f800000    # 1.0f

    add-float v18, v18, v22

    const/high16 v22, 0x40000000    # 2.0f

    div-float v18, v18, v22

    move/from16 v0, v20

    move/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v18

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v20, v20, v18

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const v22, 0x3f333333    # 0.7f

    mul-float v20, v20, v22

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v19, v0

    const/high16 v22, 0x3f800000    # 1.0f

    sub-float v22, v22, v19

    sget v23, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->b:F

    mul-float v22, v22, v23

    add-float v19, v19, v22

    mul-float v19, v19, v20

    const-wide/16 v22, 0x0

    const-wide v24, 0x3fb999999999999aL    # 0.1

    float-to-double v0, v4

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    invoke-static/range {v22 .. v25}, Ljava/lang/Math;->max(DD)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-float v4, v0

    add-float v4, v4, v19

    add-float v4, v4, v18

    const/high16 v18, 0x3f800000    # 1.0f

    move/from16 v0, v18

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v4

    const/16 v18, 0xff

    const/high16 v19, 0x437f0000    # 255.0f

    mul-float v19, v19, v4

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->min(II)I

    move-result v18

    const/16 v19, 0xff

    const/high16 v20, 0x437f0000    # 255.0f

    mul-float v20, v20, v4

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->min(II)I

    move-result v19

    const/16 v20, 0xff

    const/high16 v22, 0x437f0000    # 255.0f

    mul-float v4, v4, v22

    float-to-int v4, v4

    move/from16 v0, v20

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    shl-int/lit8 v18, v18, 0x10

    or-int v18, v18, v21

    shl-int/lit8 v19, v19, 0x8

    or-int v18, v18, v19

    or-int v18, v18, v4

    iget-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->d:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(II)V

    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto/16 :goto_7

    :cond_b
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_6

    :cond_c
    const/4 v4, 0x0

    move v7, v4

    :goto_8
    iget-object v4, v6, Lcom/google/android/apps/gmm/map/internal/c/bc;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v7, v4, :cond_7

    iget-object v4, v6, Lcom/google/android/apps/gmm/map/internal/c/bc;->b:Ljava/util/List;

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/l/a;

    const-string v5, "edge mesh"

    if-nez v4, :cond_d

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_d
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/l/a;->a:[F

    array-length v5, v5

    div-int/lit8 v13, v5, 0x3

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/l/a;->b:[I

    array-length v5, v5

    div-int/lit8 v14, v5, 0x2

    iget-object v15, v4, Lcom/google/android/apps/gmm/map/l/a;->a:[F

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/l/a;->b:[I

    move-object/from16 v16, v0

    iget-object v5, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->e:Ljava/util/List;

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b()I

    move-result v17

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/l/a;->c:Lcom/google/android/apps/gmm/v/cn;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    move-object/from16 v18, v0

    const/16 v19, 0x2

    aget v18, v18, v19

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/l/a;->d:Lcom/google/android/apps/gmm/v/cn;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    move-object/from16 v19, v0

    const/16 v20, 0x2

    aget v19, v19, v20

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/l/a;->c:Lcom/google/android/apps/gmm/v/cn;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/16 v20, 0x2

    aget v4, v4, v20

    sub-float v19, v19, v4

    const/4 v4, 0x0

    :goto_9
    if-ge v4, v13, :cond_e

    mul-int/lit8 v20, v4, 0x3

    iget-object v0, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v21, v0

    aget v22, v15, v20

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v22, v0

    add-int/lit8 v23, v20, 0x1

    aget v23, v15, v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    add-int/lit8 v24, v20, 0x2

    aget v24, v15, v24

    mul-float v24, v24, v10

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v0, v23

    move-object/from16 v1, v21

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v0, v24

    move-object/from16 v1, v21

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget-object v0, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    add-int/lit8 v20, v20, 0x2

    aget v20, v15, v20

    sub-float v20, v20, v18

    div-float v20, v20, v19

    const/high16 v21, -0x1000000

    and-int v21, v21, v9

    invoke-static {v9}, Landroid/graphics/Color;->red(I)I

    move-result v22

    invoke-static {v9}, Landroid/graphics/Color;->green(I)I

    move-result v23

    invoke-static {v9}, Landroid/graphics/Color;->blue(I)I

    move-result v24

    const/high16 v25, 0x3f400000    # 0.75f

    const/high16 v26, 0x3f800000    # 1.0f

    sub-float v26, v26, v20

    sget v27, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->b:F

    mul-float v26, v26, v27

    add-float v20, v20, v26

    mul-float v20, v20, v25

    const/16 v25, 0xff

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    mul-float v22, v22, v20

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->round(F)I

    move-result v22

    move/from16 v0, v25

    move/from16 v1, v22

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v22

    const/16 v25, 0xff

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v20

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->round(F)I

    move-result v23

    move/from16 v0, v25

    move/from16 v1, v23

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v23

    const/16 v25, 0xff

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v20, v20, v24

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    move/from16 v0, v25

    move/from16 v1, v20

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v20

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v20

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(II)V

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_9

    :cond_e
    const/4 v4, 0x0

    :goto_a
    if-ge v4, v14, :cond_f

    shl-int/lit8 v13, v4, 0x1

    add-int/lit8 v15, v13, 0x1

    aget v13, v16, v13

    add-int v13, v13, v17

    invoke-virtual {v5, v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c(I)V

    aget v13, v16, v15

    add-int v13, v13, v17

    invoke-virtual {v5, v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->c(I)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    :cond_f
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto/16 :goto_8

    .line 229
    :cond_10
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    rem-int/lit8 v4, v4, 0x2

    shl-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    rem-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    int-to-float v4, v4

    new-instance v6, Lcom/google/android/apps/gmm/v/bd;

    invoke-direct {v6, v4, v4}, Lcom/google/android/apps/gmm/v/bd;-><init>(FF)V

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    rsub-int/lit8 v7, v7, 0x12

    int-to-double v8, v7

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-float v7, v4

    const/4 v4, 0x0

    move v5, v4

    :goto_b
    iget-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->d:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v5, v4, :cond_14

    iget-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->d:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k()I

    move-result v4

    if-lez v4, :cond_13

    iget-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->d:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v8, 0x4

    const/4 v9, 0x1

    invoke-virtual {v4, v8, v9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v4

    iput-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    new-instance v4, Lcom/google/android/apps/gmm/map/t/ar;

    sget-object v8, Lcom/google/android/apps/gmm/map/t/l;->l:Lcom/google/android/apps/gmm/map/t/l;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v8, v0, v9, v10}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0xb

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Building3D "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v4, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    iget-object v8, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    invoke-virtual {v4, v8}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    new-instance v8, Lcom/google/android/apps/gmm/v/z;

    const/16 v9, 0x201

    invoke-direct {v8, v9}, Lcom/google/android/apps/gmm/v/z;-><init>(I)V

    invoke-virtual {v4, v8}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    new-instance v8, Lcom/google/android/apps/gmm/v/m;

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-direct {v8, v9, v10}, Lcom/google/android/apps/gmm/v/m;-><init>(II)V

    invoke-virtual {v4, v8}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    iput v7, v4, Lcom/google/android/apps/gmm/map/t/ar;->e:F

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/t/ar;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    invoke-virtual {v4, v6}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/v/ck;->a(Lcom/google/android/apps/gmm/v/cl;)V

    move-object/from16 v0, p6

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v8, 0x0

    iget-boolean v9, v4, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v9, :cond_11

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_11
    int-to-byte v8, v8

    iput-byte v8, v4, Lcom/google/android/apps/gmm/v/aa;->w:B

    new-instance v4, Lcom/google/android/apps/gmm/map/t/ar;

    sget-object v8, Lcom/google/android/apps/gmm/map/t/l;->m:Lcom/google/android/apps/gmm/map/t/l;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v8, v0, v9, v10}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0xb

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Building3D "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v4, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    iget-object v8, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    invoke-virtual {v4, v8}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    new-instance v8, Lcom/google/android/apps/gmm/v/z;

    const/16 v9, 0x203

    invoke-direct {v8, v9}, Lcom/google/android/apps/gmm/v/z;-><init>(I)V

    invoke-virtual {v4, v8}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    new-instance v8, Lcom/google/android/apps/gmm/v/m;

    const/16 v9, 0x302

    const/16 v10, 0x303

    invoke-direct {v8, v9, v10}, Lcom/google/android/apps/gmm/v/m;-><init>(II)V

    invoke-virtual {v4, v8}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    invoke-virtual {v4, v6}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    iput v7, v4, Lcom/google/android/apps/gmm/map/t/ar;->e:F

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/t/ar;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/v/ck;->a(Lcom/google/android/apps/gmm/v/cl;)V

    move-object/from16 v0, p6

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v8, 0x0

    iget-boolean v9, v4, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v9, :cond_12

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_12
    int-to-byte v8, v8

    iput-byte v8, v4, Lcom/google/android/apps/gmm/v/aa;->w:B

    :cond_13
    iget-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->d:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    iget-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->d:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v4, v5, v8}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_b

    :cond_14
    const/4 v4, 0x0

    move v5, v4

    :goto_c
    iget-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v5, v4, :cond_17

    iget-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->e:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k()I

    move-result v4

    if-lez v4, :cond_16

    iget-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->e:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-virtual {v4, v6, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v4

    iput-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    new-instance v4, Lcom/google/android/apps/gmm/map/t/ar;

    sget-object v6, Lcom/google/android/apps/gmm/map/t/l;->m:Lcom/google/android/apps/gmm/map/t/l;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v6, v0, v7, v8}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static/range {p0 .. p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x13

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Building3D "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " outline"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    iget-object v6, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    invoke-virtual {v4, v6}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    new-instance v6, Lcom/google/android/apps/gmm/v/x;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/v/x;-><init>()V

    invoke-virtual {v4, v6}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    new-instance v6, Lcom/google/android/apps/gmm/v/z;

    const/16 v7, 0x201

    invoke-direct {v6, v7}, Lcom/google/android/apps/gmm/v/z;-><init>(I)V

    invoke-virtual {v4, v6}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    sget-object v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->c:Lcom/google/android/apps/gmm/v/bd;

    const/high16 v7, 0x40200000    # 2.5f

    iput v7, v6, Lcom/google/android/apps/gmm/v/bd;->a:F

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/t/ar;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    sget-object v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->c:Lcom/google/android/apps/gmm/v/bd;

    invoke-virtual {v4, v6}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/v/ck;->a(Lcom/google/android/apps/gmm/v/cl;)V

    const/4 v6, 0x0

    iget-boolean v7, v4, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v7, :cond_15

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_15
    int-to-byte v6, v6

    iput-byte v6, v4, Lcom/google/android/apps/gmm/v/aa;->w:B

    :cond_16
    iget-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->e:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    iget-object v4, v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->e:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_c

    .line 230
    :cond_17
    return-object v12
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-nez v0, :cond_0

    .line 594
    const/4 v0, 0x0

    .line 597
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-nez v0, :cond_0

    .line 604
    const/4 v0, 0x0

    .line 606
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/e;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    add-int/lit8 v0, v0, 0x0

    goto :goto_0
.end method
