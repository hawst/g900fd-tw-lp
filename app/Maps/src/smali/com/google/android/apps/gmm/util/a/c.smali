.class abstract Lcom/google/android/apps/gmm/util/a/c;
.super Lcom/google/android/apps/gmm/util/a/d;
.source "PG"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# instance fields
.field private a:Lcom/google/android/apps/gmm/util/a/a;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/android/apps/gmm/util/a/a;)V
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/google/android/apps/gmm/util/a/d;-><init>()V

    .line 177
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p2, Lcom/google/android/apps/gmm/util/a/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/util/a/c;->a:Lcom/google/android/apps/gmm/util/a/a;

    .line 178
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/apps/gmm/util/a/c;->b:Landroid/view/View;

    .line 179
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/c;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/c;->b:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 186
    const/4 v0, 0x0

    .line 188
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/c;->b:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 218
    invoke-super {p0}, Lcom/google/android/apps/gmm/util/a/d;->b()V

    .line 219
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/c;->b:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 224
    iput-object v1, p0, Lcom/google/android/apps/gmm/util/a/c;->a:Lcom/google/android/apps/gmm/util/a/a;

    .line 225
    iput-object v1, p0, Lcom/google/android/apps/gmm/util/a/c;->b:Landroid/view/View;

    .line 226
    invoke-super {p0}, Lcom/google/android/apps/gmm/util/a/d;->c()V

    .line 227
    return-void
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 195
    iget-object v2, p0, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    if-eqz v2, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    move v2, v1

    :goto_1
    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v2, v0

    goto :goto_0

    :cond_1
    move v2, v0

    goto :goto_1

    .line 196
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/util/a/c;->d:Z

    if-nez v2, :cond_3

    move v2, v1

    :goto_2
    if-nez v2, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    move v2, v0

    goto :goto_2

    .line 198
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/util/a/c;->a:Lcom/google/android/apps/gmm/util/a/a;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/gmm/util/a/a;->a(Lcom/google/android/apps/gmm/util/a/d;)V

    .line 200
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/util/a/c;->d:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    if-eqz v2, :cond_7

    move v2, v1

    :goto_3
    if-eqz v2, :cond_6

    :cond_5
    move v0, v1

    :cond_6
    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_7
    move v2, v0

    goto :goto_3

    .line 201
    :cond_8
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 207
    iget-object v2, p0, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    .line 208
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/util/a/c;->d:Z

    if-nez v2, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    goto :goto_1

    .line 210
    :cond_3
    invoke-super {p0}, Lcom/google/android/apps/gmm/util/a/d;->b()V

    .line 212
    iget-object v2, p0, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    if-eqz v2, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    :goto_3
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_3

    .line 213
    :cond_6
    return-void
.end method
