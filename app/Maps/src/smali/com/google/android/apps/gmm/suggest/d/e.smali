.class public Lcom/google/android/apps/gmm/suggest/d/e;
.super Lcom/google/android/apps/gmm/z/b/a;
.source "PG"


# static fields
.field static final a:[I


# instance fields
.field final b:J

.field private final h:J

.field private final i:Lcom/google/android/apps/gmm/suggest/d/a;

.field private final j:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/d/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/gmm/suggest/d/e;->a:[I

    return-void

    :array_0
    .array-data 4
        0x64
        0xc8
        0x12c
        0x190
        0x1f4
        0x2bc
        0x3e8
        0x5dc
        0x7d0
        0x7fffffff
    .end array-data
.end method

.method public constructor <init>(JJLcom/google/android/apps/gmm/suggest/d/a;Ljava/util/List;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/google/android/apps/gmm/suggest/d/a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/suggest/d/g;",
            ">;",
            "Lcom/google/android/apps/gmm/shared/c/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p7}, Lcom/google/android/apps/gmm/z/b/a;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 51
    iput-wide p1, p0, Lcom/google/android/apps/gmm/suggest/d/e;->b:J

    .line 52
    iput-wide p3, p0, Lcom/google/android/apps/gmm/suggest/d/e;->h:J

    .line 53
    iput-object p5, p0, Lcom/google/android/apps/gmm/suggest/d/e;->i:Lcom/google/android/apps/gmm/suggest/d/a;

    .line 54
    invoke-static {p6}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/e;->j:Lcom/google/b/c/cv;

    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    .line 57
    iget-object v1, p5, Lcom/google/android/apps/gmm/suggest/d/a;->c:Ljava/lang/String;

    .line 56
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/b/f;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    .line 57
    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    .line 56
    iput-object v0, p0, Lcom/google/android/apps/gmm/z/b/a;->e:Lcom/google/maps/g/hy;

    .line 58
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/suggest/e/d;I)Lcom/google/r/b/a/zi;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/e/d;->o:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_1

    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/e/d;->o:Lcom/google/e/a/a/a/b;

    .line 79
    invoke-static {}, Lcom/google/r/b/a/zi;->d()Lcom/google/r/b/a/zi;

    move-result-object v1

    .line 78
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_1
    check-cast v0, Lcom/google/r/b/a/zi;

    .line 79
    invoke-static {}, Lcom/google/r/b/a/zi;->newBuilder()Lcom/google/r/b/a/zk;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/zk;->a(Lcom/google/r/b/a/zi;)Lcom/google/r/b/a/zk;

    move-result-object v0

    .line 85
    :goto_2
    iget v1, v0, Lcom/google/r/b/a/zk;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/r/b/a/zk;->a:I

    iput p1, v0, Lcom/google/r/b/a/zk;->b:I

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/e/d;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    move v1, v2

    :goto_3
    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/e/d;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, v0, Lcom/google/r/b/a/zk;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v0, Lcom/google/r/b/a/zk;->a:I

    iput v1, v0, Lcom/google/r/b/a/zk;->c:I

    .line 90
    :cond_0
    invoke-virtual {v0}, Lcom/google/r/b/a/zk;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/zi;

    return-object v0

    :cond_1
    move v0, v3

    .line 75
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 78
    goto :goto_1

    .line 81
    :cond_3
    invoke-static {}, Lcom/google/r/b/a/zi;->newBuilder()Lcom/google/r/b/a/zk;

    move-result-object v0

    goto :goto_2

    :cond_4
    move v1, v3

    .line 87
    goto :goto_3
.end method

.method private static b(Lcom/google/android/apps/gmm/suggest/e/d;I)Lcom/google/b/f/bx;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 95
    invoke-static {}, Lcom/google/b/f/bx;->newBuilder()Lcom/google/b/f/bz;

    move-result-object v4

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/e/d;->o:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_3

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/e/d;->o:Lcom/google/e/a/a/a/b;

    .line 101
    invoke-static {}, Lcom/google/r/b/a/zi;->d()Lcom/google/r/b/a/zi;

    move-result-object v1

    .line 100
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_4

    :goto_1
    check-cast v0, Lcom/google/r/b/a/zi;

    .line 103
    iget v1, v0, Lcom/google/r/b/a/zi;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_5

    move v1, v2

    :goto_2
    if-eqz v1, :cond_0

    .line 104
    iget v1, v0, Lcom/google/r/b/a/zi;->c:I

    iget v5, v4, Lcom/google/b/f/bz;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v4, Lcom/google/b/f/bz;->a:I

    iput v1, v4, Lcom/google/b/f/bz;->c:I

    .line 106
    :cond_0
    iget-object v0, v0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    invoke-virtual {v4, v0}, Lcom/google/b/f/bz;->a(Ljava/lang/Iterable;)Lcom/google/b/f/bz;

    .line 110
    :cond_1
    iget v0, v4, Lcom/google/b/f/bz;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v4, Lcom/google/b/f/bz;->a:I

    iput p1, v4, Lcom/google/b/f/bz;->b:I

    .line 114
    iget v0, v4, Lcom/google/b/f/bz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_6

    move v0, v2

    :goto_3
    if-nez v0, :cond_2

    .line 115
    iget v0, v4, Lcom/google/b/f/bz;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v4, Lcom/google/b/f/bz;->a:I

    iput v3, v4, Lcom/google/b/f/bz;->c:I

    .line 117
    :cond_2
    invoke-virtual {v4}, Lcom/google/b/f/bz;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/bx;

    return-object v0

    :cond_3
    move v0, v3

    .line 96
    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 100
    goto :goto_1

    :cond_5
    move v1, v3

    .line 103
    goto :goto_2

    :cond_6
    move v0, v3

    .line 114
    goto :goto_3
.end method


# virtual methods
.method public final a()Lcom/google/r/b/a/zd;
    .locals 10
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 227
    invoke-static {}, Lcom/google/r/b/a/zd;->newBuilder()Lcom/google/r/b/a/zf;

    move-result-object v4

    const/4 v0, 0x4

    .line 228
    iget v1, v4, Lcom/google/r/b/a/zf;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v4, Lcom/google/r/b/a/zf;->a:I

    iput v0, v4, Lcom/google/r/b/a/zf;->b:I

    .line 229
    iget v0, v4, Lcom/google/r/b/a/zf;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, v4, Lcom/google/r/b/a/zf;->a:I

    iput v2, v4, Lcom/google/r/b/a/zf;->h:I

    iget-wide v0, p0, Lcom/google/android/apps/gmm/suggest/d/e;->h:J

    iget-wide v6, p0, Lcom/google/android/apps/gmm/suggest/d/e;->b:J

    sub-long/2addr v0, v6

    long-to-int v0, v0

    .line 230
    iget v1, v4, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, v4, Lcom/google/r/b/a/zf;->a:I

    iput v0, v4, Lcom/google/r/b/a/zf;->i:I

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/e;->i:Lcom/google/android/apps/gmm/suggest/d/a;

    .line 234
    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/d/a;->a:Lcom/google/android/apps/gmm/suggest/d/b;

    iget v0, v0, Lcom/google/android/apps/gmm/suggest/d/b;->d:I

    invoke-static {v0}, Lcom/google/r/b/a/zg;->a(I)Lcom/google/r/b/a/zg;

    move-result-object v0

    .line 235
    if-eqz v0, :cond_1

    .line 236
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v1, v4, Lcom/google/r/b/a/zf;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v4, Lcom/google/r/b/a/zf;->a:I

    iget v0, v0, Lcom/google/r/b/a/zg;->d:I

    iput v0, v4, Lcom/google/r/b/a/zf;->c:I

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/e;->i:Lcom/google/android/apps/gmm/suggest/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/d/a;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v1, v4, Lcom/google/r/b/a/zf;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v4, Lcom/google/r/b/a/zf;->a:I

    iput-object v0, v4, Lcom/google/r/b/a/zf;->d:Ljava/lang/Object;

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/e;->i:Lcom/google/android/apps/gmm/suggest/d/a;

    iget v0, v0, Lcom/google/android/apps/gmm/suggest/d/a;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    move v0, v3

    :goto_0
    if-eqz v0, :cond_6

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/e;->i:Lcom/google/android/apps/gmm/suggest/d/a;

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/d/a;->d:I

    iget-object v5, v0, Lcom/google/android/apps/gmm/suggest/d/a;->e:Lcom/google/b/c/cv;

    invoke-virtual {v5}, Lcom/google/b/c/cv;->size()I

    move-result v5

    invoke-static {v1, v5}, Lcom/google/b/a/aq;->a(II)I

    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/d/a;->e:Lcom/google/b/c/cv;

    iget v0, v0, Lcom/google/android/apps/gmm/suggest/d/a;->d:I

    invoke-virtual {v1, v0}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/suggest/e/d;

    .line 241
    if-eqz v0, :cond_6

    .line 242
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/d/e;->i:Lcom/google/android/apps/gmm/suggest/d/a;

    .line 244
    iget v1, v1, Lcom/google/android/apps/gmm/suggest/d/a;->d:I

    .line 243
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/suggest/d/e;->a(Lcom/google/android/apps/gmm/suggest/e/d;I)Lcom/google/r/b/a/zi;

    move-result-object v1

    .line 242
    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v0, v2

    .line 239
    goto :goto_0

    .line 242
    :cond_4
    iget-object v5, v4, Lcom/google/r/b/a/zf;->e:Lcom/google/n/ao;

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v5, Lcom/google/n/ao;->d:Z

    iget v1, v4, Lcom/google/r/b/a/zf;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v4, Lcom/google/r/b/a/zf;->a:I

    .line 245
    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/e/d;->n:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 246
    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/e/d;->n:Ljava/lang/String;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget v1, v4, Lcom/google/r/b/a/zf;->a:I

    const/high16 v5, 0x40000

    or-int/2addr v1, v5

    iput v1, v4, Lcom/google/r/b/a/zf;->a:I

    iput-object v0, v4, Lcom/google/r/b/a/zf;->t:Ljava/lang/Object;

    .line 250
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/e;->i:Lcom/google/android/apps/gmm/suggest/d/a;

    iget-object v5, v0, Lcom/google/android/apps/gmm/suggest/d/a;->e:Lcom/google/b/c/cv;

    move v1, v2

    .line 251
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 253
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/suggest/e/d;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/suggest/d/e;->a(Lcom/google/android/apps/gmm/suggest/e/d;I)Lcom/google/r/b/a/zi;

    move-result-object v0

    .line 252
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    invoke-virtual {v4}, Lcom/google/r/b/a/zf;->c()V

    iget-object v6, v4, Lcom/google/r/b/a/zf;->f:Ljava/util/List;

    new-instance v7, Lcom/google/n/ao;

    invoke-direct {v7}, Lcom/google/n/ao;-><init>()V

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v7, Lcom/google/n/ao;->d:Z

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 256
    :cond_8
    new-instance v1, Lcom/google/android/apps/gmm/suggest/d/f;

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/e;->j:Lcom/google/b/c/cv;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/suggest/d/f;-><init>(Lcom/google/android/apps/gmm/suggest/d/e;Ljava/util/List;)V

    .line 257
    iget v0, v1, Lcom/google/android/apps/gmm/suggest/d/f;->a:I

    iget v5, v4, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v5, v5, 0x100

    iput v5, v4, Lcom/google/r/b/a/zf;->a:I

    iput v0, v4, Lcom/google/r/b/a/zf;->j:I

    iget v0, v1, Lcom/google/android/apps/gmm/suggest/d/f;->b:I

    .line 258
    iget v5, v4, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v5, v5, 0x200

    iput v5, v4, Lcom/google/r/b/a/zf;->a:I

    iput v0, v4, Lcom/google/r/b/a/zf;->k:I

    iget v0, v1, Lcom/google/android/apps/gmm/suggest/d/f;->c:I

    .line 259
    iget v5, v4, Lcom/google/r/b/a/zf;->a:I

    const v6, 0x8000

    or-int/2addr v5, v6

    iput v5, v4, Lcom/google/r/b/a/zf;->a:I

    iput v0, v4, Lcom/google/r/b/a/zf;->q:I

    iget v0, v1, Lcom/google/android/apps/gmm/suggest/d/f;->d:I

    .line 260
    iget v5, v4, Lcom/google/r/b/a/zf;->a:I

    const/high16 v6, 0x10000

    or-int/2addr v5, v6

    iput v5, v4, Lcom/google/r/b/a/zf;->a:I

    iput v0, v4, Lcom/google/r/b/a/zf;->r:I

    iget v0, v1, Lcom/google/android/apps/gmm/suggest/d/f;->e:I

    if-lez v0, :cond_9

    move v0, v3

    .line 261
    :goto_2
    iget v3, v4, Lcom/google/r/b/a/zf;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, v4, Lcom/google/r/b/a/zf;->a:I

    iput-boolean v0, v4, Lcom/google/r/b/a/zf;->g:Z

    iget v0, v1, Lcom/google/android/apps/gmm/suggest/d/f;->f:I

    .line 262
    iget v3, v4, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, v4, Lcom/google/r/b/a/zf;->a:I

    iput v0, v4, Lcom/google/r/b/a/zf;->l:I

    iget v0, v1, Lcom/google/android/apps/gmm/suggest/d/f;->g:I

    .line 263
    iget v3, v4, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, v4, Lcom/google/r/b/a/zf;->a:I

    iput v0, v4, Lcom/google/r/b/a/zf;->m:I

    iget v0, v1, Lcom/google/android/apps/gmm/suggest/d/f;->h:I

    .line 264
    iget v3, v4, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, v4, Lcom/google/r/b/a/zf;->a:I

    iput v0, v4, Lcom/google/r/b/a/zf;->n:I

    iget v0, v1, Lcom/google/android/apps/gmm/suggest/d/f;->i:I

    .line 265
    iget v3, v4, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, v4, Lcom/google/r/b/a/zf;->a:I

    iput v0, v4, Lcom/google/r/b/a/zf;->o:I

    iget v0, v1, Lcom/google/android/apps/gmm/suggest/d/f;->j:I

    .line 266
    iget v3, v4, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, v4, Lcom/google/r/b/a/zf;->a:I

    iput v0, v4, Lcom/google/r/b/a/zf;->p:I

    .line 267
    :goto_3
    iget-object v0, v1, Lcom/google/android/apps/gmm/suggest/d/f;->k:[I

    array-length v0, v0

    if-ge v2, v0, :cond_a

    .line 268
    iget-object v0, v1, Lcom/google/android/apps/gmm/suggest/d/f;->k:[I

    aget v0, v0, v2

    invoke-virtual {v4}, Lcom/google/r/b/a/zf;->d()V

    iget-object v3, v4, Lcom/google/r/b/a/zf;->s:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 267
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_9
    move v0, v2

    .line 260
    goto :goto_2

    .line 270
    :cond_a
    iget-object v0, v1, Lcom/google/android/apps/gmm/suggest/d/f;->l:Lcom/google/n/f;

    if-eqz v0, :cond_c

    .line 271
    iget-object v0, v1, Lcom/google/android/apps/gmm/suggest/d/f;->l:Lcom/google/n/f;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    iget v1, v4, Lcom/google/r/b/a/zf;->a:I

    const/high16 v2, 0x80000

    or-int/2addr v1, v2

    iput v1, v4, Lcom/google/r/b/a/zf;->a:I

    iput-object v0, v4, Lcom/google/r/b/a/zf;->u:Lcom/google/n/f;

    .line 273
    :cond_c
    invoke-virtual {v4}, Lcom/google/r/b/a/zf;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/zd;

    return-object v0
.end method

.method protected final a(Lcom/google/r/b/a/apf;)V
    .locals 3

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/z/b/a;->a(Lcom/google/r/b/a/apf;)V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/d/e;->a()Lcom/google/r/b/a/zd;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p1, Lcom/google/r/b/a/apf;->i:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/r/b/a/apf;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p1, Lcom/google/r/b/a/apf;->a:I

    .line 66
    return-void
.end method

.method public final b()Lcom/google/b/f/bf;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 280
    invoke-static {}, Lcom/google/b/f/bf;->newBuilder()Lcom/google/b/f/bi;

    move-result-object v4

    const-string v0, "maps-gmm-android"

    .line 281
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v1, v4, Lcom/google/b/f/bi;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v4, Lcom/google/b/f/bi;->a:I

    iput-object v0, v4, Lcom/google/b/f/bi;->c:Ljava/lang/Object;

    .line 282
    iget v0, v4, Lcom/google/b/f/bi;->a:I

    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    iput v0, v4, Lcom/google/b/f/bi;->a:I

    iput v2, v4, Lcom/google/b/f/bi;->i:I

    iget-wide v0, p0, Lcom/google/android/apps/gmm/suggest/d/e;->h:J

    iget-wide v6, p0, Lcom/google/android/apps/gmm/suggest/d/e;->b:J

    sub-long/2addr v0, v6

    long-to-int v0, v0

    .line 283
    iget v1, v4, Lcom/google/b/f/bi;->a:I

    const/high16 v5, 0x20000

    or-int/2addr v1, v5

    iput v1, v4, Lcom/google/b/f/bi;->a:I

    iput v0, v4, Lcom/google/b/f/bi;->j:I

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/e;->i:Lcom/google/android/apps/gmm/suggest/d/a;

    .line 287
    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/d/a;->a:Lcom/google/android/apps/gmm/suggest/d/b;

    iget v0, v0, Lcom/google/android/apps/gmm/suggest/d/b;->d:I

    invoke-static {v0}, Lcom/google/b/f/br;->a(I)Lcom/google/b/f/br;

    move-result-object v0

    .line 288
    if-eqz v0, :cond_1

    .line 289
    invoke-virtual {v4, v0}, Lcom/google/b/f/bi;->a(Lcom/google/b/f/br;)Lcom/google/b/f/bi;

    .line 291
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/e;->i:Lcom/google/android/apps/gmm/suggest/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/d/a;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v1, v4, Lcom/google/b/f/bi;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, v4, Lcom/google/b/f/bi;->a:I

    iput-object v0, v4, Lcom/google/b/f/bi;->e:Ljava/lang/Object;

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/e;->i:Lcom/google/android/apps/gmm/suggest/d/a;

    iget v0, v0, Lcom/google/android/apps/gmm/suggest/d/a;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    move v0, v3

    :goto_0
    if-eqz v0, :cond_5

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/e;->i:Lcom/google/android/apps/gmm/suggest/d/a;

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/d/a;->d:I

    iget-object v5, v0, Lcom/google/android/apps/gmm/suggest/d/a;->e:Lcom/google/b/c/cv;

    invoke-virtual {v5}, Lcom/google/b/c/cv;->size()I

    move-result v5

    invoke-static {v1, v5}, Lcom/google/b/a/aq;->a(II)I

    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/d/a;->e:Lcom/google/b/c/cv;

    iget v0, v0, Lcom/google/android/apps/gmm/suggest/d/a;->d:I

    invoke-virtual {v1, v0}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/suggest/e/d;

    .line 294
    if-eqz v0, :cond_5

    .line 295
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/d/e;->i:Lcom/google/android/apps/gmm/suggest/d/a;

    .line 296
    iget v1, v1, Lcom/google/android/apps/gmm/suggest/d/a;->d:I

    .line 295
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/suggest/d/e;->b(Lcom/google/android/apps/gmm/suggest/e/d;I)Lcom/google/b/f/bx;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v0, v2

    .line 292
    goto :goto_0

    .line 295
    :cond_4
    iget-object v1, v4, Lcom/google/b/f/bi;->f:Lcom/google/n/ao;

    iget-object v5, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v1, Lcom/google/n/ao;->d:Z

    iget v0, v4, Lcom/google/b/f/bi;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, v4, Lcom/google/b/f/bi;->a:I

    .line 299
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/e;->i:Lcom/google/android/apps/gmm/suggest/d/a;

    iget-object v5, v0, Lcom/google/android/apps/gmm/suggest/d/a;->e:Lcom/google/b/c/cv;

    move v1, v2

    .line 300
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 302
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/suggest/e/d;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/suggest/d/e;->b(Lcom/google/android/apps/gmm/suggest/e/d;I)Lcom/google/b/f/bx;

    move-result-object v0

    .line 301
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    invoke-virtual {v4}, Lcom/google/b/f/bi;->c()V

    iget-object v6, v4, Lcom/google/b/f/bi;->g:Ljava/util/List;

    new-instance v7, Lcom/google/n/ao;

    invoke-direct {v7}, Lcom/google/n/ao;-><init>()V

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v7, Lcom/google/n/ao;->d:Z

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 305
    :cond_7
    new-instance v0, Lcom/google/android/apps/gmm/suggest/d/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/d/e;->j:Lcom/google/b/c/cv;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/suggest/d/f;-><init>(Lcom/google/android/apps/gmm/suggest/d/e;Ljava/util/List;)V

    .line 306
    iget v1, v0, Lcom/google/android/apps/gmm/suggest/d/f;->a:I

    iget v5, v4, Lcom/google/b/f/bi;->a:I

    const/high16 v6, 0x40000

    or-int/2addr v5, v6

    iput v5, v4, Lcom/google/b/f/bi;->a:I

    iput v1, v4, Lcom/google/b/f/bi;->k:I

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/d/f;->b:I

    .line 307
    iget v5, v4, Lcom/google/b/f/bi;->a:I

    const/high16 v6, 0x80000

    or-int/2addr v5, v6

    iput v5, v4, Lcom/google/b/f/bi;->a:I

    iput v1, v4, Lcom/google/b/f/bi;->l:I

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/d/f;->c:I

    .line 308
    iget v5, v4, Lcom/google/b/f/bi;->a:I

    const/high16 v6, -0x80000000

    or-int/2addr v5, v6

    iput v5, v4, Lcom/google/b/f/bi;->a:I

    iput v1, v4, Lcom/google/b/f/bi;->r:I

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/d/f;->d:I

    .line 309
    iget v5, v4, Lcom/google/b/f/bi;->b:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v4, Lcom/google/b/f/bi;->b:I

    iput v1, v4, Lcom/google/b/f/bi;->s:I

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/d/f;->e:I

    if-lez v1, :cond_8

    move v2, v3

    .line 310
    :cond_8
    iget v1, v4, Lcom/google/b/f/bi;->a:I

    const v5, 0x8000

    or-int/2addr v1, v5

    iput v1, v4, Lcom/google/b/f/bi;->a:I

    iput-boolean v2, v4, Lcom/google/b/f/bi;->h:Z

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/d/f;->f:I

    .line 311
    iget v2, v4, Lcom/google/b/f/bi;->a:I

    const/high16 v5, 0x200000

    or-int/2addr v2, v5

    iput v2, v4, Lcom/google/b/f/bi;->a:I

    iput v1, v4, Lcom/google/b/f/bi;->m:I

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/d/f;->g:I

    .line 312
    iget v2, v4, Lcom/google/b/f/bi;->a:I

    const/high16 v5, 0x8000000

    or-int/2addr v2, v5

    iput v2, v4, Lcom/google/b/f/bi;->a:I

    iput v1, v4, Lcom/google/b/f/bi;->n:I

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/d/f;->h:I

    .line 313
    iget v2, v4, Lcom/google/b/f/bi;->a:I

    const/high16 v5, 0x10000000

    or-int/2addr v2, v5

    iput v2, v4, Lcom/google/b/f/bi;->a:I

    iput v1, v4, Lcom/google/b/f/bi;->o:I

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/d/f;->i:I

    .line 314
    iget v2, v4, Lcom/google/b/f/bi;->a:I

    const/high16 v5, 0x20000000

    or-int/2addr v2, v5

    iput v2, v4, Lcom/google/b/f/bi;->a:I

    iput v1, v4, Lcom/google/b/f/bi;->p:I

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/d/f;->j:I

    .line 315
    iget v2, v4, Lcom/google/b/f/bi;->a:I

    const/high16 v5, 0x40000000    # 2.0f

    or-int/2addr v2, v5

    iput v2, v4, Lcom/google/b/f/bi;->a:I

    iput v1, v4, Lcom/google/b/f/bi;->q:I

    .line 316
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/f;->a()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    iget v2, v4, Lcom/google/b/f/bi;->b:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v4, Lcom/google/b/f/bi;->b:I

    iput-object v1, v4, Lcom/google/b/f/bi;->t:Ljava/lang/Object;

    .line 317
    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/d/f;->m:Lcom/google/b/f/bj;

    if-eqz v1, :cond_b

    .line 318
    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/d/f;->m:Lcom/google/b/f/bj;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    iget-object v1, v4, Lcom/google/b/f/bi;->d:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v1, Lcom/google/n/ao;->d:Z

    iget v0, v4, Lcom/google/b/f/bi;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, v4, Lcom/google/b/f/bi;->a:I

    .line 320
    :cond_b
    invoke-virtual {v4}, Lcom/google/b/f/bi;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/bf;

    return-object v0
.end method
