.class public Lcom/google/android/apps/gmm/map/o/a/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/b/a/ay;

.field final b:Lcom/google/android/apps/gmm/map/b/a/ay;

.field final c:Lcom/google/android/apps/gmm/map/o/b/a;

.field final d:Lcom/google/android/apps/gmm/map/b/a/ab;

.field private final e:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final f:[F

.field private final g:Lcom/google/android/apps/gmm/map/o/b/a;

.field private final h:Lcom/google/android/apps/gmm/map/b/a/ay;

.field private final i:Lcom/google/android/apps/gmm/map/b/a/ay;

.field private final j:Lcom/google/android/apps/gmm/map/b/a/ay;

.field private final k:Lcom/google/android/apps/gmm/map/b/a/ay;

.field private final l:Lcom/google/android/apps/gmm/map/b/a/ay;

.field private final m:[[Lcom/google/android/apps/gmm/map/b/a/ay;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/o/b/a;Lcom/google/android/apps/gmm/map/b/a/ab;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v2, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 20
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->f:[F

    .line 21
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0, v3, v3}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0, v3, v3}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 23
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-direct {v0, v3, v3, v3, v3}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->g:Lcom/google/android/apps/gmm/map/o/b/a;

    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0, v3, v3}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->h:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 47
    const-string v0, "screenBounds"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/map/o/b/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->c:Lcom/google/android/apps/gmm/map/o/b/a;

    .line 48
    const-string v0, "polyline"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 49
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    if-lt v0, v5, :cond_2

    move v0, v1

    :goto_0
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 51
    :cond_3
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    iget v4, p1, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->i:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    iget v4, p1, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget v4, p1, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->k:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 54
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget v4, p1, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->l:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 55
    const/4 v0, 0x4

    new-array v0, v0, [[Lcom/google/android/apps/gmm/map/b/a/ay;

    new-array v3, v5, [Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/d;->i:Lcom/google/android/apps/gmm/map/b/a/ay;

    aput-object v4, v3, v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/d;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    aput-object v4, v3, v1

    aput-object v3, v0, v2

    new-array v3, v5, [Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/d;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    aput-object v4, v3, v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/d;->l:Lcom/google/android/apps/gmm/map/b/a/ay;

    aput-object v4, v3, v1

    aput-object v3, v0, v1

    new-array v3, v5, [Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/d;->l:Lcom/google/android/apps/gmm/map/b/a/ay;

    aput-object v4, v3, v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/d;->k:Lcom/google/android/apps/gmm/map/b/a/ay;

    aput-object v4, v3, v1

    aput-object v3, v0, v5

    const/4 v3, 0x3

    new-array v4, v5, [Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/a/d;->k:Lcom/google/android/apps/gmm/map/b/a/ay;

    aput-object v5, v4, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/a/d;->i:Lcom/google/android/apps/gmm/map/b/a/ay;

    aput-object v2, v4, v1

    aput-object v4, v0, v3

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->m:[[Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 57
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;ZLcom/google/android/apps/gmm/map/b/a/ay;)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 245
    .line 246
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 247
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/o/a/d;->m:[[Lcom/google/android/apps/gmm/map/b/a/ay;

    move v5, v3

    move v1, v3

    :goto_0
    const/4 v4, 0x4

    if-ge v5, v4, :cond_4

    aget-object v4, v6, v5

    .line 248
    aget-object v7, v4, v3

    aget-object v4, v4, v2

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/o/a/d;->h:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-static {p1, p2, v7, v4, v8}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 249
    if-eqz p3, :cond_0

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->h:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v1, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v1, p4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move v0, v2

    .line 266
    :goto_1
    return v0

    .line 254
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/d;->h:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v7, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v8, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    sub-float/2addr v7, v8

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v8, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    sub-float/2addr v4, v8

    mul-float/2addr v7, v7

    mul-float/2addr v4, v4

    add-float/2addr v4, v7

    .line 255
    if-eqz v1, :cond_1

    cmpg-float v7, v4, v0

    if-gez v7, :cond_2

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->h:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v7, p4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move v0, v4

    .line 260
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 261
    const/4 v4, 0x2

    if-eq v1, v4, :cond_4

    .line 262
    :cond_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 266
    :cond_4
    if-lez v1, :cond_5

    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v3

    goto :goto_1
.end method


# virtual methods
.method a(Lcom/google/android/apps/gmm/map/f/o;ILcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)I
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 184
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/d;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v1, v1

    div-int/lit8 v1, v1, 0x3

    add-int/lit8 v1, v1, -0x1

    if-ne p2, v1, :cond_1

    .line 185
    iget v0, p3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v0, p3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 215
    :cond_0
    :goto_0
    return p2

    .line 190
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/a/d;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 191
    const/4 v1, 0x0

    :goto_1
    const/4 v3, 0x5

    if-ge v1, v3, :cond_5

    .line 192
    add-int/lit8 v3, p2, 0x1

    invoke-virtual {p0, p1, v3, v2}, Lcom/google/android/apps/gmm/map/o/a/d;->a(Lcom/google/android/apps/gmm/map/f/o;ILcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v3

    if-nez v3, :cond_2

    move p2, v0

    .line 193
    goto :goto_0

    .line 196
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a/d;->c:Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 199
    invoke-virtual {p0, p3, v2, p4}, Lcom/google/android/apps/gmm/map/o/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v1

    if-nez v1, :cond_0

    move p2, v0

    .line 202
    goto :goto_0

    .line 206
    :cond_3
    add-int/lit8 v3, p2, 0x1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/d;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v4, v4

    div-int/lit8 v4, v4, 0x3

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_4

    .line 207
    iget v0, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v0, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 208
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 211
    :cond_4
    add-int/lit8 p2, p2, 0x1

    .line 212
    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v3, p3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v3, p3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 191
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    move p2, v0

    .line 215
    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ay;)I
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, -0x1

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {p0, p1, v0, v3}, Lcom/google/android/apps/gmm/map/o/a/d;->a(Lcom/google/android/apps/gmm/map/f/o;ILcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->c:Lcom/google/android/apps/gmm/map/o/b/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v1, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v1, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/d;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, -0x1

    .line 109
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 85
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a/d;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v3, v3

    div-int/lit8 v3, v3, 0x3

    add-int/lit8 v3, v3, -0x2

    :goto_2
    if-ltz v3, :cond_5

    .line 88
    if-eqz v0, :cond_1

    .line 89
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/d;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v6, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 92
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {p0, p1, v3, v4}, Lcom/google/android/apps/gmm/map/o/a/d;->a(Lcom/google/android/apps/gmm/map/f/o;ILcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v4

    if-nez v4, :cond_3

    move v0, v2

    .line 85
    :cond_2
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .line 97
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/d;->c:Lcom/google/android/apps/gmm/map/o/b/a;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/a/d;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 98
    invoke-direct {p0, v4, v5, v1, p2}, Lcom/google/android/apps/gmm/map/o/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;ZLcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v0, v3

    .line 99
    goto :goto_0

    .line 104
    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/a/d;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {p0, v4, v5, p2}, Lcom/google/android/apps/gmm/map/o/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v0, v3

    .line 105
    goto :goto_0

    .line 109
    :cond_5
    const/4 v0, -0x1

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_1
.end method

.method a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 275
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/d;->g:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v3, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v4, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iget v4, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v5, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 276
    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iget v5, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v6, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 275
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/o/b/a;->a(FFFF)V

    .line 277
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/d;->c:Lcom/google/android/apps/gmm/map/o/b/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/a/d;->g:Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/o/b/a;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 278
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/apps/gmm/map/o/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;ZLcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method a(Lcom/google/android/apps/gmm/map/f/o;ILcom/google/android/apps/gmm/map/b/a/ay;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 223
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/a/d;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a/d;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, p2, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 224
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/a/d;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a/d;->f:[F

    invoke-virtual {p1, v2, v3}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v2

    if-nez v2, :cond_0

    .line 228
    :goto_0
    return v0

    .line 227
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/a/d;->f:[F

    aget v0, v2, v0

    float-to-int v0, v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/a/d;->f:[F

    aget v2, v2, v1

    float-to-int v2, v2

    int-to-float v2, v2

    iput v0, p3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v2, p3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move v0, v1

    .line 228
    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 283
    if-ne p0, p1, :cond_1

    .line 290
    :cond_0
    :goto_0
    return v0

    .line 286
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/o/a/d;

    if-nez v2, :cond_2

    move v0, v1

    .line 287
    goto :goto_0

    .line 289
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/o/a/d;

    .line 290
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/a/d;->c:Lcom/google/android/apps/gmm/map/o/b/a;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/o/a/d;->c:Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/o/b/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/a/d;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/o/a/d;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
