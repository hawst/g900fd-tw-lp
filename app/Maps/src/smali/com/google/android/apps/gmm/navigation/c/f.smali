.class public Lcom/google/android/apps/gmm/navigation/c/f;
.super Lcom/google/android/apps/gmm/navigation/c/a;
.source "PG"


# instance fields
.field private l:Ljava/util/List;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/g/b/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/mylocation/b/f;Lcom/google/android/apps/gmm/navigation/d/a;ZLcom/google/android/apps/gmm/shared/net/a/b;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct/range {p0 .. p7}, Lcom/google/android/apps/gmm/navigation/c/a;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/mylocation/b/f;Lcom/google/android/apps/gmm/navigation/d/a;ZLcom/google/android/apps/gmm/shared/net/a/b;)V

    .line 46
    return-void
.end method


# virtual methods
.method public final G_()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/google/android/apps/gmm/navigation/c/a;->G_()V

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/f;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 60
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/f/t;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 65
    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/f/t;->a:Lcom/google/android/apps/gmm/navigation/f/b/a;

    .line 70
    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/f/b/a;->e:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/c/f;->d()V

    .line 77
    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->e:Lcom/google/android/apps/gmm/mylocation/b/f;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Lcom/google/android/apps/gmm/map/s/a;)V

    .line 84
    :goto_1
    return-void

    .line 70
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 81
    :cond_1
    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/f/b/a;->e:Lcom/google/android/apps/gmm/navigation/g/b/d;

    .line 82
    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/d;->f:Ljava/util/List;

    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/c/f;->l:Ljava/util/List;

    .line 83
    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/d;->d:Lcom/google/maps/g/a/hm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/b;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/navigation/c/f;->a(Lcom/google/android/apps/gmm/navigation/commonui/b/a;Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/b/a;)V

    goto :goto_1
.end method

.method protected final a(ZZ)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 88
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/f;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/f;->l:Ljava/util/List;

    if-nez v1, :cond_2

    :cond_0
    const/4 v0, 0x0

    .line 89
    :goto_0
    if-eqz v0, :cond_1

    .line 90
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/navigation/c/f;->a(ZZLcom/google/android/apps/gmm/map/f/a/a;)V

    .line 92
    :cond_1
    return-void

    .line 88
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/f;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    new-array v2, v1, [Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/f;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v4

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/f;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    aput-object v1, v2, v0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/f;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/f;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/h;->a:Lcom/google/android/apps/gmm/map/r/a/ap;

    add-int/lit8 v3, v1, 0x1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    aput-object v0, v2, v3

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/ae;->b([Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/c/f;->e()Lcom/google/android/apps/gmm/navigation/c/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/c/f;->c:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/c/f;->k:Landroid/graphics/Rect;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/navigation/c/a/b;->a(Lcom/google/android/apps/gmm/map/b/a/ae;Landroid/util/DisplayMetrics;Landroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Lcom/google/android/apps/gmm/navigation/c/a;->b()V

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/f;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 54
    return-void
.end method
