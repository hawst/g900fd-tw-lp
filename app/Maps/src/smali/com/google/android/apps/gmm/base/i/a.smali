.class public Lcom/google/android/apps/gmm/base/i/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:I

.field static final b:I

.field static final c:I

.field static final d:I

.field static final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final k:I

.field private static final l:I

.field private static final m:I


# instance fields
.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/i/c;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:I

.field private final n:Landroid/content/Context;

.field private final o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    sget v0, Lcom/google/android/apps/gmm/h;->H:I

    sput v0, Lcom/google/android/apps/gmm/base/i/a;->k:I

    .line 77
    sget v0, Lcom/google/android/apps/gmm/h;->P:I

    sput v0, Lcom/google/android/apps/gmm/base/i/a;->a:I

    .line 78
    sget v0, Lcom/google/android/apps/gmm/h;->N:I

    sput v0, Lcom/google/android/apps/gmm/base/i/a;->b:I

    .line 79
    sget v0, Lcom/google/android/apps/gmm/h;->M:I

    sput v0, Lcom/google/android/apps/gmm/base/i/a;->c:I

    .line 80
    sget v0, Lcom/google/android/apps/gmm/h;->O:I

    sput v0, Lcom/google/android/apps/gmm/base/i/a;->d:I

    .line 81
    sget v0, Lcom/google/android/apps/gmm/h;->G:I

    sput v0, Lcom/google/android/apps/gmm/base/i/a;->l:I

    .line 82
    sget v0, Lcom/google/android/apps/gmm/base/b/e;->a:I

    sput v0, Lcom/google/android/apps/gmm/base/i/a;->m:I

    .line 85
    const-class v0, Lcom/google/android/apps/gmm/base/f/u;

    sput-object v0, Lcom/google/android/apps/gmm/base/i/a;->e:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/i/a;-><init>(Landroid/content/Context;Z)V

    .line 131
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    .line 122
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/i/a;->g:Z

    .line 123
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/i/a;->h:Z

    .line 124
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/i/a;->i:Z

    .line 125
    sget v0, Lcom/google/android/apps/gmm/base/i/a;->k:I

    iput v0, p0, Lcom/google/android/apps/gmm/base/i/a;->j:I

    .line 140
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/gmm/base/i/a;->n:Landroid/content/Context;

    .line 141
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/base/i/a;->o:Z

    .line 142
    return-void
.end method

.method private b()Lcom/google/android/apps/gmm/base/i/c;
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/i/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/i/c;->a:Lcom/google/android/apps/gmm/base/i/e;

    sget-object v1, Lcom/google/android/apps/gmm/base/i/e;->b:Lcom/google/android/apps/gmm/base/i/e;

    if-eq v0, v1, :cond_1

    .line 331
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot set header - last item is not list card"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 333
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/i/c;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/b/b;
    .locals 22

    .prologue
    .line 356
    new-instance v21, Lcom/google/android/apps/gmm/base/b/b;

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/i/a;->o:Z

    move-object/from16 v0, v21

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/b/b;-><init>(Z)V

    .line 357
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/i/a;->n:Landroid/content/Context;

    const-string v2, "layout_inflater"

    .line 358
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Landroid/view/LayoutInflater;

    .line 359
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/i/a;->n:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object/from16 v17, v1

    check-cast v17, Lcom/google/android/libraries/curvular/bd;

    .line 362
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/base/i/a;->g:Z

    .line 364
    const/4 v2, 0x0

    .line 366
    const/4 v1, 0x0

    move/from16 v19, v1

    move/from16 v20, v3

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    move/from16 v0, v19

    if-ge v0, v1, :cond_a

    .line 367
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    move/from16 v0, v19

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/google/android/apps/gmm/base/i/c;

    .line 368
    const/4 v1, 0x0

    .line 369
    sget-object v3, Lcom/google/android/apps/gmm/base/i/b;->a:[I

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/i/c;->a:Lcom/google/android/apps/gmm/base/i/e;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/i/e;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 407
    :cond_1
    :goto_1
    if-nez v1, :cond_c

    .line 408
    const-string v1, "CardListBuilder"

    const-string v3, "Missing an adapter (type=%s)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/google/android/apps/gmm/base/i/c;->a:Lcom/google/android/apps/gmm/base/i/e;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/i/e;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 409
    new-instance v1, Lcom/google/android/apps/gmm/base/b/l;

    sget v3, Lcom/google/android/apps/gmm/base/i/a;->k:I

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/google/android/apps/gmm/base/b/l;-><init>(Landroid/view/View;)V

    move-object v3, v1

    .line 412
    :goto_2
    if-eqz v20, :cond_2

    .line 418
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/gmm/base/i/a;->j:I

    if-eqz v1, :cond_9

    sget v1, Lcom/google/android/apps/gmm/base/i/a;->k:I

    .line 419
    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 423
    :goto_3
    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/apps/gmm/base/i/d;

    invoke-direct {v5, v1, v3}, Lcom/google/android/apps/gmm/base/i/d;-><init>(Landroid/view/View;Landroid/widget/ListAdapter;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/gmm/base/b/b;->a(Lcom/google/android/libraries/curvular/bk;Landroid/widget/ListAdapter;)V

    .line 426
    :cond_2
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/i/c;->q:Lcom/google/android/libraries/curvular/bk;

    move-object/from16 v0, v21

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/base/b/b;->a(Lcom/google/android/libraries/curvular/bk;Landroid/widget/ListAdapter;)V

    .line 366
    add-int/lit8 v1, v19, 0x1

    move/from16 v19, v1

    move/from16 v20, v2

    goto :goto_0

    .line 371
    :pswitch_0
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/i/c;->b:Landroid/view/View;

    if-nez v3, :cond_3

    .line 372
    sget v3, Lcom/google/android/apps/gmm/base/i/a;->l:I

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/google/android/apps/gmm/base/i/c;->b:Landroid/view/View;

    .line 374
    :cond_3
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/i/c;->b:Landroid/view/View;

    .line 375
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/i/c;->b:Landroid/view/View;

    if-eqz v3, :cond_1

    .line 378
    new-instance v1, Lcom/google/android/apps/gmm/base/b/l;

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/i/c;->b:Landroid/view/View;

    move-object/from16 v0, v18

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/base/i/c;->p:Z

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/base/b/l;-><init>(Landroid/view/View;Z)V

    .line 379
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/base/i/a;->h:Z

    goto :goto_1

    .line 383
    :pswitch_1
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/i/c;->c:Landroid/widget/ListAdapter;

    if-eqz v3, :cond_1

    .line 384
    new-instance v15, Lcom/google/android/apps/gmm/base/b/h;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/i/a;->n:Landroid/content/Context;

    move-object/from16 v0, v17

    invoke-direct {v15, v1, v0}, Lcom/google/android/apps/gmm/base/b/h;-><init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/bd;)V

    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/i/c;->c:Landroid/widget/ListAdapter;

    .line 385
    iput-object v1, v15, Lcom/google/android/apps/gmm/base/b/h;->b:Landroid/widget/ListAdapter;

    move-object/from16 v0, v18

    iget v1, v0, Lcom/google/android/apps/gmm/base/i/c;->e:I

    move-object/from16 v0, v18

    iget v2, v0, Lcom/google/android/apps/gmm/base/i/c;->g:I

    move-object/from16 v0, v18

    iget v3, v0, Lcom/google/android/apps/gmm/base/i/c;->h:I

    move-object/from16 v0, v18

    iget v4, v0, Lcom/google/android/apps/gmm/base/i/c;->f:I

    .line 386
    iput v1, v15, Lcom/google/android/apps/gmm/base/b/h;->n:I

    iput v2, v15, Lcom/google/android/apps/gmm/base/b/h;->k:I

    iput v3, v15, Lcom/google/android/apps/gmm/base/b/h;->l:I

    iput v4, v15, Lcom/google/android/apps/gmm/base/b/h;->m:I

    move-object/from16 v0, v18

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/i/c;->d:Z

    if-eqz v1, :cond_6

    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/i/c;->i:Ljava/lang/Class;

    move-object v2, v1

    .line 387
    :goto_4
    if-eqz v2, :cond_7

    const/4 v1, 0x1

    :goto_5
    iput-boolean v1, v15, Lcom/google/android/apps/gmm/base/b/h;->e:Z

    iget-boolean v1, v15, Lcom/google/android/apps/gmm/base/b/h;->e:Z

    if-eqz v1, :cond_4

    iput-object v2, v15, Lcom/google/android/apps/gmm/base/b/h;->d:Ljava/lang/Class;

    :cond_4
    move-object/from16 v0, v18

    iget v1, v0, Lcom/google/android/apps/gmm/base/i/c;->k:I

    move-object/from16 v0, v18

    iget v2, v0, Lcom/google/android/apps/gmm/base/i/c;->l:I

    move-object/from16 v0, v18

    iget v3, v0, Lcom/google/android/apps/gmm/base/i/c;->m:I

    move-object/from16 v0, v18

    iget v4, v0, Lcom/google/android/apps/gmm/base/i/c;->n:I

    .line 388
    iput v1, v15, Lcom/google/android/apps/gmm/base/b/h;->f:I

    iput v2, v15, Lcom/google/android/apps/gmm/base/b/h;->g:I

    iput v3, v15, Lcom/google/android/apps/gmm/base/b/h;->h:I

    iput v4, v15, Lcom/google/android/apps/gmm/base/b/h;->i:I

    move-object/from16 v0, v18

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/i/c;->o:Z

    .line 389
    iput-boolean v1, v15, Lcom/google/android/apps/gmm/base/b/h;->j:Z

    .line 390
    iget-object v1, v15, Lcom/google/android/apps/gmm/base/b/h;->b:Landroid/widget/ListAdapter;

    if-nez v1, :cond_5

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, v15, Lcom/google/android/apps/gmm/base/b/h;->a:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v1, v15, Lcom/google/android/apps/gmm/base/b/h;->b:Landroid/widget/ListAdapter;

    :cond_5
    new-instance v1, Lcom/google/android/apps/gmm/base/b/e;

    iget-object v2, v15, Lcom/google/android/apps/gmm/base/b/h;->a:Landroid/content/Context;

    iget-object v3, v15, Lcom/google/android/apps/gmm/base/b/h;->b:Landroid/widget/ListAdapter;

    iget-object v4, v15, Lcom/google/android/apps/gmm/base/b/h;->c:Lcom/google/android/libraries/curvular/bd;

    iget v5, v15, Lcom/google/android/apps/gmm/base/b/h;->n:I

    iget v6, v15, Lcom/google/android/apps/gmm/base/b/h;->k:I

    iget v7, v15, Lcom/google/android/apps/gmm/base/b/h;->l:I

    iget v8, v15, Lcom/google/android/apps/gmm/base/b/h;->m:I

    iget-object v9, v15, Lcom/google/android/apps/gmm/base/b/h;->d:Ljava/lang/Class;

    iget-boolean v10, v15, Lcom/google/android/apps/gmm/base/b/h;->e:Z

    iget v11, v15, Lcom/google/android/apps/gmm/base/b/h;->f:I

    iget v12, v15, Lcom/google/android/apps/gmm/base/b/h;->g:I

    iget v13, v15, Lcom/google/android/apps/gmm/base/b/h;->h:I

    iget v14, v15, Lcom/google/android/apps/gmm/base/b/h;->i:I

    iget-boolean v15, v15, Lcom/google/android/apps/gmm/base/b/h;->j:Z

    invoke-direct/range {v1 .. v15}, Lcom/google/android/apps/gmm/base/b/e;-><init>(Landroid/content/Context;Landroid/widget/ListAdapter;Lcom/google/android/libraries/curvular/bd;IIIILjava/lang/Class;ZIIIIZ)V

    .line 391
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/base/i/a;->h:Z

    goto/16 :goto_1

    .line 386
    :cond_6
    const/4 v1, 0x0

    move-object v2, v1

    goto :goto_4

    .line 387
    :cond_7
    const/4 v1, 0x0

    goto :goto_5

    .line 395
    :pswitch_2
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/i/c;->c:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_8

    .line 396
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/i/c;->c:Landroid/widget/ListAdapter;

    .line 397
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/base/i/a;->h:Z

    goto/16 :goto_1

    .line 401
    :cond_8
    new-instance v1, Lcom/google/android/apps/gmm/base/b/l;

    sget v3, Lcom/google/android/apps/gmm/base/i/a;->k:I

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/google/android/apps/gmm/base/b/l;-><init>(Landroid/view/View;)V

    goto/16 :goto_1

    .line 419
    :cond_9
    new-instance v1, Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/i/a;->n:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    goto/16 :goto_3

    .line 429
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/i/a;->i:Z

    if-eqz v1, :cond_b

    .line 430
    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/apps/gmm/base/b/l;

    sget v3, Lcom/google/android/apps/gmm/base/i/a;->k:I

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/base/b/l;-><init>(Landroid/view/View;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/base/b/b;->a(Lcom/google/android/libraries/curvular/bk;Landroid/widget/ListAdapter;)V

    .line 433
    :cond_b
    return-object v21

    :cond_c
    move-object v3, v1

    goto/16 :goto_2

    .line 369
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/view/View;)Lcom/google/android/apps/gmm/base/i/a;
    .locals 2

    .prologue
    .line 168
    new-instance v0, Lcom/google/android/apps/gmm/base/i/c;

    sget-object v1, Lcom/google/android/apps/gmm/base/i/e;->a:Lcom/google/android/apps/gmm/base/i/e;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/i/c;-><init>(Lcom/google/android/apps/gmm/base/i/e;)V

    .line 169
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Landroid/view/View;

    iput-object p1, v0, Lcom/google/android/apps/gmm/base/i/c;->b:Landroid/view/View;

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    return-object p0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/b/b;)Lcom/google/android/apps/gmm/base/i/a;
    .locals 3

    .prologue
    .line 193
    if-nez p1, :cond_0

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Trying to add an empty adapter at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 196
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/base/i/c;

    sget-object v1, Lcom/google/android/apps/gmm/base/i/e;->c:Lcom/google/android/apps/gmm/base/i/e;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/i/c;-><init>(Lcom/google/android/apps/gmm/base/i/e;)V

    .line 197
    iput-object p1, v0, Lcom/google/android/apps/gmm/base/i/c;->c:Landroid/widget/ListAdapter;

    .line 198
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    return-object p0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;)Lcom/google/android/apps/gmm/base/i/a;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 269
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_2

    move v2, v0

    .line 271
    :goto_0
    if-eqz v2, :cond_3

    iget-boolean v2, p1, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->f:Z

    if-eqz v2, :cond_3

    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/i/a;->b()Lcom/google/android/apps/gmm/base/i/c;

    move-result-object v2

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/base/i/c;->d:Z

    .line 273
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->getPaddingTop()I

    move-result v2

    .line 274
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->getPaddingRight()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->getPaddingBottom()I

    move-result v4

    .line 272
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/i/a;->b()Lcom/google/android/apps/gmm/base/i/c;

    move-result-object v5

    iput v0, v5, Lcom/google/android/apps/gmm/base/i/c;->k:I

    iput v2, v5, Lcom/google/android/apps/gmm/base/i/c;->l:I

    iput v3, v5, Lcom/google/android/apps/gmm/base/i/c;->m:I

    iput v4, v5, Lcom/google/android/apps/gmm/base/i/c;->n:I

    .line 275
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->g:Z

    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/i/a;->b()Lcom/google/android/apps/gmm/base/i/c;

    move-result-object v2

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/base/i/c;->o:Z

    .line 276
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->i:Z

    if-eqz v0, :cond_1

    .line 277
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/i/a;->b()Lcom/google/android/apps/gmm/base/i/c;

    move-result-object v0

    iput v1, v0, Lcom/google/android/apps/gmm/base/i/c;->e:I

    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/i/a;->b()Lcom/google/android/apps/gmm/base/i/c;

    move-result-object v0

    iput v1, v0, Lcom/google/android/apps/gmm/base/i/c;->g:I

    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/i/a;->b()Lcom/google/android/apps/gmm/base/i/c;

    move-result-object v0

    iput v1, v0, Lcom/google/android/apps/gmm/base/i/c;->h:I

    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/i/a;->b()Lcom/google/android/apps/gmm/base/i/c;

    move-result-object v0

    iput v1, v0, Lcom/google/android/apps/gmm/base/i/c;->f:I

    .line 279
    :cond_1
    return-object p0

    :cond_2
    move v2, v1

    .line 269
    goto :goto_0

    :cond_3
    move v0, v1

    .line 271
    goto :goto_1
.end method
