.class public Lcom/google/android/apps/gmm/streetview/internal/ak;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/streetview/internal/x;
.implements Lcom/google/android/apps/gmm/v/f;


# instance fields
.field private final A:Lcom/google/android/apps/gmm/streetview/internal/aq;

.field private B:Z

.field private C:Lcom/google/android/apps/gmm/streetview/internal/an;

.field private final D:Lcom/google/android/apps/gmm/streetview/internal/ag;

.field final a:Z

.field public b:I

.field final c:Lcom/google/android/apps/gmm/v/ad;

.field d:Lcom/google/android/apps/gmm/v/g;

.field final e:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/streetview/internal/ba;",
            "Lcom/google/android/apps/gmm/v/ci;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/google/android/apps/gmm/streetview/internal/t;

.field g:Lcom/google/android/apps/gmm/streetview/internal/z;

.field h:Lcom/google/android/apps/gmm/v/ci;

.field i:Z

.field final j:Lcom/google/android/apps/gmm/map/c/a;

.field k:Z

.field final l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/streetview/internal/ba;",
            ">;"
        }
    .end annotation
.end field

.field private final m:I

.field private final n:Lcom/google/android/apps/gmm/streetview/internal/b;

.field private final o:Lcom/google/android/apps/gmm/v/cp;

.field private final p:Lcom/google/android/apps/gmm/streetview/internal/ac;

.field private q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/streetview/internal/am;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation
.end field

.field private u:I

.field private v:Lcom/google/android/apps/gmm/v/ci;

.field private w:Lcom/google/android/apps/gmm/streetview/internal/w;

.field private x:Lcom/google/android/apps/gmm/streetview/internal/w;

.field private y:Z

.field private z:Lcom/google/android/apps/gmm/streetview/internal/y;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/ad;ILcom/google/android/apps/gmm/streetview/internal/b;Lcom/google/android/apps/gmm/streetview/internal/ac;Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 3
    .param p5    # Lcom/google/android/apps/gmm/map/util/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/android/apps/gmm/v/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->q:Ljava/util/ArrayList;

    .line 88
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->r:Ljava/util/ArrayList;

    .line 89
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->s:Ljava/util/ArrayList;

    .line 91
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->t:Ljava/util/ArrayList;

    .line 94
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->u:I

    .line 104
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->i:Z

    .line 105
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->y:Z

    .line 119
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->B:Z

    .line 120
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->k:Z

    .line 121
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->l:Ljava/util/Set;

    .line 186
    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/al;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/streetview/internal/al;-><init>(Lcom/google/android/apps/gmm/streetview/internal/ak;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->D:Lcom/google/android/apps/gmm/streetview/internal/ag;

    .line 160
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->c:Lcom/google/android/apps/gmm/v/ad;

    .line 161
    iput p2, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->m:I

    .line 162
    iput-object p4, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->p:Lcom/google/android/apps/gmm/streetview/internal/ac;

    .line 163
    iput-object p3, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->n:Lcom/google/android/apps/gmm/streetview/internal/b;

    .line 164
    iget-object v1, p3, Lcom/google/android/apps/gmm/streetview/internal/b;->b:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->A:Lcom/google/android/apps/gmm/streetview/internal/aq;

    .line 166
    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    iget v1, v1, Lcom/google/android/apps/gmm/v/ao;->e:I

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->a:Z

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->A:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/aq;->e:Landroid/util/DisplayMetrics;

    .line 171
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit16 v1, v1, 0x200

    add-int/lit8 v1, v1, 0x2

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit16 v0, v0, 0x200

    add-int/lit8 v0, v0, 0x2

    mul-int/2addr v1, v0

    .line 174
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->a:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x100

    .line 176
    :goto_0
    shl-int/lit8 v1, v1, 0x2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 177
    new-instance v1, Lcom/google/android/apps/gmm/map/util/a/e;

    const-string v2, "Streetview texture cache"

    invoke-direct {v1, v0, v2, p5}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 180
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->a:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x32

    :goto_1
    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->b:I

    .line 182
    new-instance v0, Lcom/google/android/apps/gmm/v/cr;

    invoke-direct {v0, p6}, Lcom/google/android/apps/gmm/v/cr;-><init>(Lcom/google/android/apps/gmm/v/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->o:Lcom/google/android/apps/gmm/v/cp;

    .line 183
    iput-object p7, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->j:Lcom/google/android/apps/gmm/map/c/a;

    .line 184
    return-void

    .line 174
    :cond_1
    const/16 v0, 0x40

    goto :goto_0

    .line 180
    :cond_2
    const/16 v0, 0xf

    goto :goto_1
.end method

.method private static a(IIII)Lcom/google/android/apps/gmm/v/ax;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v7, 0x3f000000    # 0.5f

    .line 497
    .line 499
    const/high16 v0, 0x3f800000    # 1.0f

    .line 502
    sub-int v5, p2, p3

    move v3, v2

    move v4, v2

    move v2, v0

    move v0, v1

    .line 505
    :goto_0
    if-ge v0, v5, :cond_0

    .line 506
    mul-float/2addr v4, v7

    and-int/lit8 v6, p0, 0x1

    int-to-float v6, v6

    mul-float/2addr v6, v7

    add-float/2addr v4, v6

    .line 507
    mul-float/2addr v3, v7

    and-int/lit8 v6, p1, 0x1

    int-to-float v6, v6

    mul-float/2addr v6, v7

    add-float/2addr v3, v6

    .line 508
    shr-int/lit8 p0, p0, 0x1

    .line 509
    shr-int/lit8 p1, p1, 0x1

    .line 510
    mul-float/2addr v2, v7

    .line 505
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 513
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/v/ax;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/ax;-><init>()V

    .line 514
    iget-object v5, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    aput v2, v5, v1

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v5, 0x4

    aput v2, v1, v5

    .line 515
    iget-object v1, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v2, 0x6

    aput v4, v1, v2

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v2, 0x7

    aput v3, v1, v2

    .line 516
    return-object v0
.end method

.method private c()Z
    .locals 17

    .prologue
    .line 390
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 391
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->h:Lcom/google/android/apps/gmm/v/ci;

    if-nez v1, :cond_2

    .line 393
    const/4 v1, 0x0

    .line 479
    :goto_1
    return v1

    .line 396
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 397
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 398
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 400
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->A:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v3, v1, Lcom/google/android/apps/gmm/streetview/internal/aq;->e:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v3, v3

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/aq;->f:Lcom/google/android/apps/gmm/streetview/b/a;

    iget v1, v1, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    div-float/2addr v3, v1

    const/4 v1, 0x0

    :goto_2
    iget-object v4, v2, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    array-length v4, v4

    if-ge v1, v4, :cond_4

    iget-object v4, v2, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v4, v4, v1

    iget v5, v4, Lcom/google/android/apps/gmm/streetview/internal/n;->c:I

    int-to-float v5, v5

    iget v4, v4, Lcom/google/android/apps/gmm/streetview/internal/n;->d:F

    const/high16 v6, 0x43b40000    # 360.0f

    mul-float/2addr v4, v6

    div-float v4, v5, v4

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    cmpl-float v4, v4, v3

    if-ltz v4, :cond_3

    move v9, v1

    .line 401
    :goto_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->u:I

    if-ltz v1, :cond_5

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->u:I

    if-eq v9, v1, :cond_5

    .line 403
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->u:I

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    aget-object v2, v1, v2

    .line 404
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->u:I

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v1, v1, v3

    iget v3, v1, Lcom/google/android/apps/gmm/streetview/internal/n;->a:I

    .line 405
    const/4 v1, 0x0

    :goto_4
    array-length v4, v2

    if-ge v1, v4, :cond_5

    .line 406
    aget-object v4, v2, v1

    .line 407
    div-int v5, v1, v3

    .line 408
    rem-int v6, v1, v3

    .line 409
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->s:Ljava/util/ArrayList;

    new-instance v8, Lcom/google/android/apps/gmm/streetview/internal/am;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->h:Lcom/google/android/apps/gmm/v/ci;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->u:I

    const/4 v12, 0x0

    .line 410
    invoke-static {v6, v5, v11, v12}, Lcom/google/android/apps/gmm/streetview/internal/ak;->a(IIII)Lcom/google/android/apps/gmm/v/ax;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v8, v0, v4, v10, v5}, Lcom/google/android/apps/gmm/streetview/internal/am;-><init>(Lcom/google/android/apps/gmm/streetview/internal/ak;Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/ci;Lcom/google/android/apps/gmm/v/ax;)V

    .line 409
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 411
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->r:Ljava/util/ArrayList;

    aget-object v5, v2, v1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 405
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 400
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    iget-object v1, v2, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    move v9, v1

    goto :goto_3

    .line 414
    :cond_5
    move-object/from16 v0, p0

    iput v9, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->u:I

    .line 417
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    aget-object v1, v1, v9

    array-length v1, v1

    .line 418
    new-array v10, v1, [I

    .line 420
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->A:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v1, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    if-eqz v10, :cond_6

    const/4 v1, 0x1

    :goto_5
    if-nez v1, :cond_7

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :cond_6
    const/4 v1, 0x0

    goto :goto_5

    :cond_7
    array-length v1, v10

    iget-object v3, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    aget-object v3, v3, v9

    array-length v3, v3

    if-lt v1, v3, :cond_8

    const/4 v1, 0x1

    :goto_6
    if-nez v1, :cond_9

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :cond_8
    const/4 v1, 0x0

    goto :goto_6

    :cond_9
    iget-object v1, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    aget-object v1, v1, v9

    array-length v11, v1

    iget-object v1, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->f:Lcom/google/android/apps/gmm/v/cj;

    iget-object v2, v2, Lcom/google/android/apps/gmm/streetview/internal/aq;->g:Lcom/google/android/apps/gmm/v/cj;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v4, 0x0

    const/16 v5, 0x10

    invoke-static {v2, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->f:Lcom/google/android/apps/gmm/v/cj;

    iget-object v1, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->f:Lcom/google/android/apps/gmm/v/cj;

    iget-object v5, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->e:Lcom/google/android/apps/gmm/v/cj;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v2, 0x0

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v4, 0x0

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget-object v1, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->f:Lcom/google/android/apps/gmm/v/cj;

    iget-object v2, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->h:Lcom/google/android/apps/gmm/v/cn;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    iget-object v3, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->h:Lcom/google/android/apps/gmm/v/cn;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    iget-object v4, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->h:Lcom/google/android/apps/gmm/v/cn;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v5, 0x0

    invoke-static {v1, v5, v2, v3, v4}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    iget-object v1, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->f:Lcom/google/android/apps/gmm/v/cj;

    sget-object v6, Lcom/google/android/apps/gmm/streetview/internal/y;->a:Lcom/google/android/apps/gmm/v/cn;

    iget-object v2, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v2, v2, Lcom/google/android/apps/gmm/streetview/internal/t;->q:F

    neg-float v3, v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v2, 0x0

    iget-object v4, v6, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    iget-object v5, v6, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v8, 0x1

    aget v5, v5, v8

    iget-object v6, v6, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v8, 0x2

    aget v6, v6, v8

    invoke-static/range {v1 .. v6}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v1, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->f:Lcom/google/android/apps/gmm/v/cj;

    iget-object v2, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->g:[F

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x10

    invoke-static {v1, v3, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->g:[F

    const/4 v2, 0x0

    iget-object v3, v7, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v3, v3, v9

    iget-object v3, v3, Lcom/google/android/apps/gmm/streetview/internal/n;->g:[F

    const/4 v4, 0x0

    const/4 v7, 0x0

    move v5, v11

    move-object v6, v10

    move v8, v11

    invoke-static/range {v1 .. v8}, Landroid/opengl/Visibility;->frustumCullSpheres([FI[FII[III)I

    move-result v11

    .line 422
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->B:Z

    if-eqz v1, :cond_a

    .line 423
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->k:Z

    .line 427
    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    aget-object v12, v1, v9

    .line 428
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v1, v1, v9

    iget v13, v1, Lcom/google/android/apps/gmm/streetview/internal/n;->a:I

    .line 429
    const/4 v2, 0x0

    .line 430
    const/4 v1, 0x0

    move v3, v1

    move v1, v2

    :goto_7
    array-length v2, v12

    if-ge v3, v2, :cond_13

    .line 431
    aget-object v14, v12, v3

    .line 432
    div-int v6, v3, v13

    .line 433
    rem-int v8, v3, v13

    .line 435
    if-ge v1, v11, :cond_12

    aget v2, v10, v1

    if-ne v2, v3, :cond_12

    .line 436
    add-int/lit8 v2, v1, 0x1

    .line 439
    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/ba;

    invoke-direct {v1, v8, v6, v9}, Lcom/google/android/apps/gmm/streetview/internal/ba;-><init>(III)V

    .line 440
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_b

    .line 441
    new-instance v4, Lcom/google/android/apps/gmm/streetview/internal/z;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget-object v5, v5, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    invoke-direct {v4, v5, v8, v6, v9}, Lcom/google/android/apps/gmm/streetview/internal/z;-><init>(Ljava/lang/String;III)V

    .line 443
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->p:Lcom/google/android/apps/gmm/streetview/internal/ac;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->D:Lcom/google/android/apps/gmm/streetview/internal/ag;

    const/4 v15, 0x1

    invoke-virtual {v5, v7, v4, v15}, Lcom/google/android/apps/gmm/streetview/internal/ac;->a(Lcom/google/android/apps/gmm/streetview/internal/ag;Lcom/google/android/apps/gmm/streetview/internal/z;Z)Z

    .line 444
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->B:Z

    if-eqz v4, :cond_b

    .line 446
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->l:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 451
    :cond_b
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    monitor-enter v15

    move v4, v9

    move v5, v6

    move v7, v8

    .line 452
    :goto_8
    if-lez v4, :cond_10

    :try_start_0
    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/ba;

    invoke-direct {v1, v7, v5, v4}, Lcom/google/android/apps/gmm/streetview/internal/ba;-><init>(III)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    if-eqz v16, :cond_f

    move-object v4, v1

    .line 454
    :goto_9
    iget v1, v4, Lcom/google/android/apps/gmm/streetview/internal/ba;->a:I

    if-nez v1, :cond_11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->h:Lcom/google/android/apps/gmm/v/ci;

    move-object v5, v1

    .line 455
    :goto_a
    sget-object v1, Lcom/google/android/apps/gmm/v/aj;->b:Lcom/google/android/apps/gmm/v/aj;

    const/4 v7, 0x1

    .line 456
    invoke-virtual {v14, v1, v7}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/aj;I)Lcom/google/android/apps/gmm/v/ai;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/v/ci;

    .line 457
    if-eq v1, v5, :cond_c

    .line 458
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->s:Ljava/util/ArrayList;

    new-instance v7, Lcom/google/android/apps/gmm/streetview/internal/am;

    .line 459
    iget v4, v4, Lcom/google/android/apps/gmm/streetview/internal/ba;->a:I

    invoke-static {v8, v6, v9, v4}, Lcom/google/android/apps/gmm/streetview/internal/ak;->a(IIII)Lcom/google/android/apps/gmm/v/ax;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v14, v5, v4}, Lcom/google/android/apps/gmm/streetview/internal/am;-><init>(Lcom/google/android/apps/gmm/streetview/internal/ak;Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/ci;Lcom/google/android/apps/gmm/v/ax;)V

    .line 458
    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 461
    :cond_c
    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463
    iget-byte v1, v14, Lcom/google/android/apps/gmm/v/aa;->w:B

    if-nez v1, :cond_d

    .line 464
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->q:Ljava/util/ArrayList;

    invoke-virtual {v1, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_d
    move v1, v2

    .line 430
    :cond_e
    :goto_b
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_7

    .line 452
    :cond_f
    add-int/lit8 v1, v4, -0x1

    shr-int/lit8 v4, v5, 0x1

    shr-int/lit8 v5, v7, 0x1

    move v7, v5

    move v5, v4

    move v4, v1

    goto :goto_8

    :cond_10
    :try_start_1
    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/ba;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-direct {v1, v4, v5, v7}, Lcom/google/android/apps/gmm/streetview/internal/ba;-><init>(III)V

    move-object v4, v1

    goto :goto_9

    .line 454
    :cond_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/v/ci;

    move-object v5, v1

    goto :goto_a

    .line 461
    :catchall_0
    move-exception v1

    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 468
    :cond_12
    iget-byte v2, v14, Lcom/google/android/apps/gmm/v/aa;->w:B

    if-eqz v2, :cond_e

    .line 470
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->s:Ljava/util/ArrayList;

    new-instance v4, Lcom/google/android/apps/gmm/streetview/internal/am;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->h:Lcom/google/android/apps/gmm/v/ci;

    const/4 v7, 0x0

    .line 471
    invoke-static {v8, v6, v9, v7}, Lcom/google/android/apps/gmm/streetview/internal/ak;->a(IIII)Lcom/google/android/apps/gmm/v/ax;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v14, v5, v6}, Lcom/google/android/apps/gmm/streetview/internal/am;-><init>(Lcom/google/android/apps/gmm/streetview/internal/ak;Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/ci;Lcom/google/android/apps/gmm/v/ax;)V

    .line 470
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 472
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 477
    :cond_13
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->B:Z

    .line 479
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_15

    :cond_14
    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_15
    const/4 v1, 0x0

    goto/16 :goto_1
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/v/h;
    .locals 1

    .prologue
    .line 588
    sget-object v0, Lcom/google/android/apps/gmm/v/h;->c:Lcom/google/android/apps/gmm/v/h;

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/streetview/internal/t;Lcom/google/android/apps/gmm/v/cn;Lcom/google/android/apps/gmm/streetview/internal/an;)V
    .locals 14
    .param p3    # Lcom/google/android/apps/gmm/streetview/internal/an;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 229
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->C:Lcom/google/android/apps/gmm/streetview/internal/an;

    .line 230
    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 230
    :cond_0
    :try_start_1
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/streetview/internal/t;

    move-object v1, v0

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    .line 232
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->h:Lcom/google/android/apps/gmm/v/ci;

    .line 233
    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/z;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/streetview/internal/z;-><init>(Ljava/lang/String;III)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->g:Lcom/google/android/apps/gmm/streetview/internal/z;

    .line 234
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->p:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->D:Lcom/google/android/apps/gmm/streetview/internal/ag;

    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->g:Lcom/google/android/apps/gmm/streetview/internal/z;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/streetview/internal/ac;->a(Lcom/google/android/apps/gmm/streetview/internal/ag;Lcom/google/android/apps/gmm/streetview/internal/z;Z)Z

    .line 236
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->i:Z

    .line 239
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    if-eqz v1, :cond_3

    .line 240
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    if-eqz v1, :cond_3

    .line 241
    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    iget-object v2, v2, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 242
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    iget-object v2, v2, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    aget-object v3, v2, v1

    .line 243
    const/4 v2, 0x0

    :goto_1
    array-length v4, v3

    if-ge v2, v4, :cond_2

    .line 244
    aget-object v4, v3, v2

    .line 245
    iget-byte v5, v4, Lcom/google/android/apps/gmm/v/aa;->w:B

    if-eqz v5, :cond_1

    .line 246
    iget-object v5, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->t:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 243
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 248
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->c:Lcom/google/android/apps/gmm/v/ad;

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v6, Lcom/google/android/apps/gmm/v/af;

    const/4 v7, 0x0

    invoke-direct {v6, v4, v7}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto :goto_2

    .line 241
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 255
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/a/e;->d()V

    .line 257
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->a:Z

    if-eqz v1, :cond_4

    iget-object v1, p1, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;

    if-eqz v1, :cond_4

    .line 259
    iget-object v1, p1, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;

    const/high16 v2, 0x42c80000    # 100.0f

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/streetview/internal/d;->c(F)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 260
    iget-object v1, p1, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;

    iget v4, v1, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    .line 261
    iget-object v1, p1, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;

    iget v5, v1, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I

    .line 262
    new-instance v1, Lcom/google/android/apps/gmm/v/ar;

    const/4 v3, 0x1

    iget-object v6, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->c:Lcom/google/android/apps/gmm/v/ad;

    iget-object v6, v6, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/v/ar;-><init>(Ljava/nio/ByteBuffer;IIILcom/google/android/apps/gmm/v/ao;Z)V

    .line 263
    new-instance v2, Lcom/google/android/apps/gmm/v/ci;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->v:Lcom/google/android/apps/gmm/v/ci;

    .line 268
    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v1, v1, Lcom/google/android/apps/gmm/streetview/internal/t;->o:I

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_4
    if-nez v1, :cond_6

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 265
    :cond_4
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->v:Lcom/google/android/apps/gmm/v/ci;

    goto :goto_3

    .line 268
    :cond_5
    const/4 v1, 0x0

    goto :goto_4

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v1, v1, Lcom/google/android/apps/gmm/streetview/internal/t;->d:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v2, v2, Lcom/google/android/apps/gmm/streetview/internal/t;->e:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v3, v3, Lcom/google/android/apps/gmm/streetview/internal/t;->f:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v4, v4, Lcom/google/android/apps/gmm/streetview/internal/t;->g:I

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(IIII)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v2, v2, Lcom/google/android/apps/gmm/streetview/internal/t;->z:I

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    new-instance v2, Lcom/google/android/apps/gmm/streetview/internal/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v4, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->m:I

    move-object/from16 v0, p2

    invoke-direct {v2, v3, v4, v1, v0}, Lcom/google/android/apps/gmm/streetview/internal/y;-><init>(Lcom/google/android/apps/gmm/streetview/internal/t;IILcom/google/android/apps/gmm/v/cn;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->u:I

    const/4 v1, 0x0

    :goto_5
    if-ge v1, v5, :cond_c

    const/4 v2, 0x0

    :goto_6
    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v3, v3, v1

    iget v3, v3, Lcom/google/android/apps/gmm/streetview/internal/n;->b:I

    if-ge v2, v3, :cond_b

    const/4 v3, 0x0

    :goto_7
    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    iget-object v4, v4, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v4, v4, v1

    iget v4, v4, Lcom/google/android/apps/gmm/streetview/internal/n;->a:I

    if-ge v3, v4, :cond_a

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    invoke-virtual {v4, v3, v2, v1}, Lcom/google/android/apps/gmm/streetview/internal/y;->a(III)Lcom/google/android/apps/gmm/v/aa;

    move-result-object v6

    new-instance v7, Lcom/google/android/apps/gmm/streetview/internal/ax;

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->v:Lcom/google/android/apps/gmm/v/ci;

    if-eqz v4, :cond_9

    const-class v4, Lcom/google/android/apps/gmm/streetview/internal/ay;

    :goto_8
    const/4 v8, -0x1

    invoke-direct {v7, v4, v8}, Lcom/google/android/apps/gmm/streetview/internal/ax;-><init>(Ljava/lang/Class;I)V

    const/4 v4, 0x0

    invoke-static {v3, v2, v1, v4}, Lcom/google/android/apps/gmm/streetview/internal/ak;->a(IIII)Lcom/google/android/apps/gmm/v/ax;

    move-result-object v4

    iget-object v8, v7, Lcom/google/android/apps/gmm/streetview/internal/ax;->a:Lcom/google/android/apps/gmm/v/ax;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v9, 0x0

    iget-object v8, v8, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v10, 0x0

    const/16 v11, 0x9

    invoke-static {v4, v9, v8, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v4, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->m:I

    invoke-virtual {v6, v7, v4}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ai;I)V

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->v:Lcom/google/android/apps/gmm/v/ci;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    iget-object v4, v4, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v4, v4, v1

    int-to-float v8, v3

    iget v9, v4, Lcom/google/android/apps/gmm/streetview/internal/n;->d:F

    mul-float/2addr v8, v9

    iget v9, v4, Lcom/google/android/apps/gmm/streetview/internal/n;->d:F

    add-float/2addr v9, v8

    int-to-float v10, v2

    iget v11, v4, Lcom/google/android/apps/gmm/streetview/internal/n;->e:F

    mul-float/2addr v10, v11

    iget v4, v4, Lcom/google/android/apps/gmm/streetview/internal/n;->e:F

    add-float/2addr v4, v10

    new-instance v11, Lcom/google/android/apps/gmm/v/ax;

    invoke-direct {v11}, Lcom/google/android/apps/gmm/v/ax;-><init>()V

    sub-float/2addr v9, v8

    iget-object v12, v11, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v13, 0x0

    aput v9, v12, v13

    iget-object v9, v11, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v12, 0x6

    aput v8, v9, v12

    sub-float/2addr v4, v10

    iget-object v8, v11, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v9, 0x4

    aput v4, v8, v9

    iget-object v4, v11, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v8, 0x7

    aput v10, v4, v8

    iget-object v4, v7, Lcom/google/android/apps/gmm/streetview/internal/ax;->b:Lcom/google/android/apps/gmm/v/ax;

    iget-object v7, v11, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v8, 0x0

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v9, 0x0

    const/16 v10, 0x9

    invoke-static {v7, v8, v4, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->v:Lcom/google/android/apps/gmm/v/ci;

    iget v7, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->m:I

    invoke-virtual {v6, v4, v7}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ai;I)V

    :cond_7
    const/4 v4, 0x0

    iget-boolean v7, v6, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v7, :cond_8

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_8
    int-to-byte v4, v4

    iput-byte v4, v6, Lcom/google/android/apps/gmm/v/aa;->w:B

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->c:Lcom/google/android/apps/gmm/v/ad;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v7, Lcom/google/android/apps/gmm/v/af;

    const/4 v8, 0x1

    invoke-direct {v7, v6, v8}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v4, v7}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_9
    const-class v4, Lcom/google/android/apps/gmm/streetview/internal/az;

    goto :goto_8

    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_6

    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_5

    :cond_c
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->d:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->d:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, p0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 271
    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->j:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/map/j/h;

    new-instance v3, Lcom/google/android/apps/gmm/map/j/i;

    sget-object v4, Lcom/google/r/b/a/a;->h:Lcom/google/r/b/a/a;

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/map/j/i;-><init>(Lcom/google/r/b/a/a;)V

    sget-object v4, Lcom/google/b/f/bc;->l:Lcom/google/b/f/bc;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/map/j/h;-><init>(Lcom/google/android/apps/gmm/map/j/i;Lcom/google/b/f/cq;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 275
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->B:Z

    .line 276
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->l:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 277
    monitor-exit p0

    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/streetview/internal/w;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->w:Lcom/google/android/apps/gmm/streetview/internal/w;

    if-ne p1, v0, :cond_5

    .line 319
    iget-object v0, p1, Lcom/google/android/apps/gmm/streetview/internal/w;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    .line 320
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->c:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v3, v0, v5}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto :goto_0

    .line 322
    :cond_0
    iput-object v7, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->w:Lcom/google/android/apps/gmm/streetview/internal/w;

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->C:Lcom/google/android/apps/gmm/streetview/internal/an;

    if-eqz v0, :cond_1

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->C:Lcom/google/android/apps/gmm/streetview/internal/an;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/streetview/internal/an;->b()V

    .line 329
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->c:Lcom/google/android/apps/gmm/v/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v1, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v1, p1, v5}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->w:Lcom/google/android/apps/gmm/streetview/internal/w;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->x:Lcom/google/android/apps/gmm/streetview/internal/w;

    if-nez v0, :cond_4

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->n:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/b;->b:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/aq;->d:Lcom/google/android/apps/gmm/v/cn;

    iget-object v2, v1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aput v4, v2, v5

    iget-object v2, v1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aput v4, v2, v6

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v2, 0x2

    aput v4, v1, v2

    iget v1, v0, Lcom/google/android/apps/gmm/streetview/internal/b;->d:I

    if-gtz v1, :cond_2

    iput v6, v0, Lcom/google/android/apps/gmm/streetview/internal/b;->d:I

    :cond_2
    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/b;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 333
    :cond_3
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->y:Z

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->d:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->d:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 336
    :cond_4
    return-void

    .line 326
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->x:Lcom/google/android/apps/gmm/streetview/internal/w;

    if-ne p1, v0, :cond_1

    .line 327
    iput-object v7, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->x:Lcom/google/android/apps/gmm/streetview/internal/w;

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 570
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->d:Lcom/google/android/apps/gmm/v/g;

    .line 574
    sget-object v0, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 575
    return-void
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 14

    .prologue
    .line 547
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->y:Z

    if-eqz v0, :cond_4

    .line 548
    iget-object v9, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    iget-object v0, v9, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    iget-object v0, v9, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    iget-object v0, v9, Lcom/google/android/apps/gmm/streetview/internal/y;->h:Lcom/google/android/apps/gmm/v/cn;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v5, 0x0

    aput v1, v4, v5

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v4, 0x1

    aput v2, v1, v4

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v1, 0x2

    aput v3, v0, v1

    iget-object v0, v9, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/t;->d:I

    iget-object v1, v9, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v1, v1, Lcom/google/android/apps/gmm/streetview/internal/t;->e:I

    iget-object v2, v9, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v2, v2, Lcom/google/android/apps/gmm/streetview/internal/t;->f:I

    iget-object v3, v9, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v3, v3, Lcom/google/android/apps/gmm/streetview/internal/t;->g:I

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(IIII)I

    move-result v0

    iget-object v1, v9, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v1, v1, Lcom/google/android/apps/gmm/streetview/internal/t;->z:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v10

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v10, :cond_2

    const/4 v0, 0x0

    move v7, v0

    :goto_1
    iget-object v0, v9, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v0, v0, v6

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->b:I

    if-ge v7, v0, :cond_1

    const/4 v0, 0x0

    move v8, v0

    :goto_2
    iget-object v0, v9, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v0, v0, v6

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->a:I

    if-ge v8, v0, :cond_0

    iget-object v0, v9, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v0, v0, v6

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->d:F

    int-to-float v1, v8

    mul-float/2addr v0, v1

    const/high16 v1, 0x43b40000    # 360.0f

    mul-float/2addr v0, v1

    invoke-virtual {v9, v8, v7, v6}, Lcom/google/android/apps/gmm/streetview/internal/y;->a(III)Lcom/google/android/apps/gmm/v/aa;

    move-result-object v11

    new-instance v12, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v12}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iget-object v1, v9, Lcom/google/android/apps/gmm/streetview/internal/y;->e:Lcom/google/android/apps/gmm/v/cj;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v2, 0x0

    iget-object v3, v12, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v4, 0x0

    const/16 v5, 0x10

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v5, Lcom/google/android/apps/gmm/streetview/internal/y;->a:Lcom/google/android/apps/gmm/v/cn;

    iget-object v1, v9, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v1, v1, Lcom/google/android/apps/gmm/streetview/internal/t;->q:F

    neg-float v1, v1

    sub-float v2, v1, v0

    iget-object v0, v12, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v1, 0x0

    iget-object v3, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    iget-object v4, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v13, 0x1

    aget v4, v4, v13

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v13, 0x2

    aget v5, v5, v13

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    invoke-virtual {v11, v12}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/cj;)V

    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_2

    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 549
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->d:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->d:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 550
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->y:Z

    .line 552
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/streetview/internal/am;

    .line 553
    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/am;->a:Lcom/google/android/apps/gmm/v/aa;

    iget-object v3, v0, Lcom/google/android/apps/gmm/streetview/internal/am;->b:Lcom/google/android/apps/gmm/v/ci;

    iget v4, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->m:I

    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ai;I)V

    .line 554
    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/am;->a:Lcom/google/android/apps/gmm/v/aa;

    sget-object v3, Lcom/google/android/apps/gmm/v/aj;->j:Lcom/google/android/apps/gmm/v/aj;

    const/4 v4, 0x1

    .line 555
    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/aj;I)Lcom/google/android/apps/gmm/v/ai;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/streetview/internal/ax;

    .line 556
    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/am;->c:Lcom/google/android/apps/gmm/v/ax;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/ax;->a:Lcom/google/android/apps/gmm/v/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v3, 0x0

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v4, 0x0

    const/16 v5, 0x9

    invoke-static {v0, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 547
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 560
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    .line 561
    const/4 v2, 0x0

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_6

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_6
    int-to-byte v2, v2

    iput-byte v2, v0, Lcom/google/android/apps/gmm/v/aa;->w:B

    goto :goto_4

    .line 563
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    .line 564
    const/16 v2, 0xff

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_8

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_8
    int-to-byte v2, v2

    iput-byte v2, v0, Lcom/google/android/apps/gmm/v/aa;->w:B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 566
    :cond_9
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 521
    monitor-enter p0

    .line 522
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->h:Lcom/google/android/apps/gmm/v/ci;

    if-eqz v0, :cond_8

    .line 523
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->i:Z

    if-eqz v0, :cond_6

    .line 524
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->x:Lcom/google/android/apps/gmm/streetview/internal/w;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->x:Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/w;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->c:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 521
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 524
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->c:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->x:Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->w:Lcom/google/android/apps/gmm/streetview/internal/w;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->w:Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/w;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->c:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->c:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->w:Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->c:Lcom/google/android/apps/gmm/v/ad;

    iget-object v8, v0, Lcom/google/android/apps/gmm/v/ad;->c:Lcom/google/android/apps/gmm/v/i;

    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->t:Ljava/util/ArrayList;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->b:I

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/streetview/internal/w;-><init>(Ljava/util/Collection;FFILcom/google/android/apps/gmm/streetview/internal/x;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->w:Lcom/google/android/apps/gmm/streetview/internal/w;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->t:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->c:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->w:Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->w:Lcom/google/android/apps/gmm/streetview/internal/w;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v8, v0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/w;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->b:I

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/streetview/internal/w;-><init>(Ljava/util/Collection;FFILcom/google/android/apps/gmm/streetview/internal/x;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->x:Lcom/google/android/apps/gmm/streetview/internal/w;

    move v1, v7

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->z:Lcom/google/android/apps/gmm/streetview/internal/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    aget-object v3, v0, v1

    move v2, v7

    :goto_3
    array-length v0, v3

    if-ge v2, v0, :cond_4

    aget-object v0, v3, v2

    sget-object v4, Lcom/google/android/apps/gmm/v/aj;->j:Lcom/google/android/apps/gmm/v/aj;

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/aj;I)Lcom/google/android/apps/gmm/v/ai;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/r;

    const/4 v4, 0x0

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/v/r;->a(F)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->x:Lcom/google/android/apps/gmm/streetview/internal/w;

    aget-object v4, v3, v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/w;->a:Ljava/util/Collection;

    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->c:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->x:Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->x:Lcom/google/android/apps/gmm/streetview/internal/w;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v8, v0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 525
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->i:Z

    .line 528
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->y:Z

    if-nez v0, :cond_8

    .line 529
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/internal/ak;->c()Z

    move-result v0

    .line 533
    :goto_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->o:Lcom/google/android/apps/gmm/v/cp;

    .line 534
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->d:Lcom/google/android/apps/gmm/v/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->o:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {v1, p0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 537
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ak;->y:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_7

    move v0, v6

    .line 541
    :cond_7
    monitor-exit p0

    return v0

    :cond_8
    move v0, v7

    goto :goto_4
.end method
