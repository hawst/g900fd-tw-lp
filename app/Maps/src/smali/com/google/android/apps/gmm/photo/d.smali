.class public Lcom/google/android/apps/gmm/photo/d;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field final a:Landroid/net/Uri;

.field final b:Z

.field private final d:Landroid/content/Context;

.field private e:Lcom/google/android/apps/gmm/map/b/a/q;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/gmm/photo/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/photo/d;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 80
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/google/android/apps/gmm/photo/d;-><init>(Landroid/net/Uri;Lcom/google/android/apps/gmm/map/b/a/q;Landroid/content/Context;Z)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/apps/gmm/map/b/a/q;Landroid/content/Context;Z)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/photo/d;->f:Z

    .line 87
    iput-object p1, p0, Lcom/google/android/apps/gmm/photo/d;->a:Landroid/net/Uri;

    .line 88
    iput-object p3, p0, Lcom/google/android/apps/gmm/photo/d;->d:Landroid/content/Context;

    .line 89
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/photo/d;->b:Z

    .line 90
    iput-object p2, p0, Lcom/google/android/apps/gmm/photo/d;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 91
    return-void
.end method

.method private static a(D)I
    .locals 4

    .prologue
    .line 340
    const/4 v0, 0x1

    .line 341
    :goto_0
    int-to-double v2, v0

    cmpg-double v1, v2, p0

    if-gez v1, :cond_0

    .line 343
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 345
    :cond_0
    return v0
.end method

.method public static a(Ljava/util/List;Landroid/content/Context;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/photo/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-wide/16 v4, 0x5

    int-to-long v6, v1

    add-long/2addr v4, v6

    div-int/lit8 v0, v1, 0xa

    int-to-long v0, v0

    add-long/2addr v0, v4

    const-wide/32 v4, 0x7fffffff

    cmp-long v3, v0, v4

    if-lez v3, :cond_2

    const v0, 0x7fffffff

    :goto_1
    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 98
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 99
    new-instance v3, Lcom/google/android/apps/gmm/photo/d;

    invoke-direct {v3, v0, p1}, Lcom/google/android/apps/gmm/photo/d;-><init>(Landroid/net/Uri;Landroid/content/Context;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 97
    :cond_2
    const-wide/32 v4, -0x80000000

    cmp-long v3, v0, v4

    if-gez v3, :cond_3

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_3
    long-to-int v0, v0

    goto :goto_1

    .line 101
    :cond_4
    return-object v2
.end method

.method private b()[I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 188
    .line 190
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/d;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/d;->a:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 191
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 192
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 193
    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 195
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    aput v4, v2, v3

    const/4 v3, 0x1

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    aput v0, v2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    if-eqz v1, :cond_0

    .line 198
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_0
    return-object v2

    .line 197
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 198
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v0
.end method

.method private c()I
    .locals 9

    .prologue
    const/16 v6, 0xb4

    const/16 v7, 0x5a

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v8, 0x0

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/d;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 397
    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 399
    :try_start_0
    new-instance v0, Landroid/media/ExifInterface;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/photo/d;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 400
    const-string v1, "Orientation"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v8

    .line 436
    :goto_0
    return v0

    .line 403
    :pswitch_1
    const/16 v0, 0x10e

    goto :goto_0

    :pswitch_2
    move v0, v6

    .line 405
    goto :goto_0

    :pswitch_3
    move v0, v7

    .line 407
    goto :goto_0

    .line 411
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/photo/d;->c:Ljava/lang/String;

    move v0, v8

    .line 412
    goto :goto_0

    .line 414
    :cond_0
    const-string v1, "content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 415
    new-array v2, v2, [Ljava/lang/String;

    const-string v0, "orientation"

    aput-object v0, v2, v8

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/d;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/d;->a:Landroid/net/Uri;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 417
    if-eqz v1, :cond_3

    .line 419
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 420
    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 421
    if-eqz v0, :cond_1

    if-eq v0, v7, :cond_1

    if-eq v0, v6, :cond_1

    const/16 v2, 0x10e

    if-eq v0, v2, :cond_1

    .line 422
    sget-object v2, Lcom/google/android/apps/gmm/photo/d;->c:Ljava/lang/String;

    const-string v2, "Unexpected photo orientation:%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v8

    goto :goto_0

    .line 425
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 431
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/photo/d;->c:Ljava/lang/String;

    move v0, v8

    .line 432
    goto :goto_0

    .line 428
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 435
    :cond_4
    sget-object v1, Lcom/google/android/apps/gmm/photo/d;->c:Ljava/lang/String;

    const-string v1, "Unexpected scheme:%s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v0, v8

    .line 436
    goto :goto_0

    .line 400
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private d()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 443
    .line 444
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/d;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 446
    const-string v1, "content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 447
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v4

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/d;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/d;->a:Landroid/net/Uri;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 451
    if-nez v1, :cond_1

    .line 467
    :cond_0
    :goto_0
    return-object v3

    .line 456
    :cond_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 457
    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 458
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 461
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 463
    :cond_3
    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/d;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 139
    invoke-direct {p0}, Lcom/google/android/apps/gmm/photo/d;->b()[I

    move-result-object v0

    aget v0, v0, v2

    .line 143
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/d;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/photo/d;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v7

    .line 144
    :try_start_1
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    div-int/2addr v0, p1

    int-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    if-le v0, v6, :cond_0

    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    const/4 v0, 0x0

    invoke-static {v7, v0, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    .line 146
    :cond_1
    :goto_0
    if-eqz v7, :cond_2

    .line 147
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    :cond_2
    return-object v0

    .line 144
    :cond_3
    :try_start_2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-direct {p0}, Lcom/google/android/apps/gmm/photo/d;->c()I

    move-result v2

    const/16 v3, 0x5a

    if-eq v2, v3, :cond_4

    const/16 v3, 0x10e

    if-ne v2, v3, :cond_7

    :cond_4
    int-to-float v1, v2

    invoke-virtual {v5, v1}, Landroid/graphics/Matrix;->preRotate(F)Z

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    :cond_5
    :goto_1
    if-le v1, p1, :cond_6

    int-to-float v2, p1

    int-to-float v1, v1

    div-float v1, v2, v1

    invoke-virtual {v5, v1, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    :cond_6
    invoke-virtual {v5}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v1

    goto :goto_0

    :cond_7
    const/16 v3, 0xb4

    if-ne v2, v3, :cond_5

    int-to-float v2, v2

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->preRotate(F)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 146
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_2
    if-eqz v1, :cond_8

    .line 147
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_8
    throw v0

    .line 146
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public final a()Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 8
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/d;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/photo/d;->f:Z

    if-eqz v0, :cond_1

    .line 357
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/photo/d;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 389
    :goto_0
    return-object v3

    .line 360
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/photo/d;->f:Z

    .line 361
    invoke-direct {p0}, Lcom/google/android/apps/gmm/photo/d;->d()Ljava/lang/String;

    move-result-object v0

    .line 362
    if-nez v0, :cond_6

    .line 363
    new-array v2, v2, [Ljava/lang/String;

    const-string v0, "latitude"

    aput-object v0, v2, v4

    const-string v0, "longitude"

    aput-object v0, v2, v1

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/d;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/d;->a:Landroid/net/Uri;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 366
    if-eqz v1, :cond_5

    .line 368
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 369
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_3

    .line 370
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 372
    :cond_3
    :try_start_1
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    float-to-double v2, v2

    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    float-to-double v4, v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/d;->e:Lcom/google/android/apps/gmm/map/b/a/q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 389
    :cond_5
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/photo/d;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    goto :goto_0

    .line 375
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 380
    :cond_6
    :try_start_2
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, v0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 381
    const/4 v0, 0x2

    new-array v0, v0, [F

    .line 382
    invoke-virtual {v1, v0}, Landroid/media/ExifInterface;->getLatLong([F)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 383
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    const/4 v2, 0x0

    aget v2, v0, v2

    float-to-double v4, v2

    const/4 v2, 0x1

    aget v0, v0, v2

    float-to-double v6, v0

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/photo/d;->e:Lcom/google/android/apps/gmm/map/b/a/q;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 386
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method b(I)[B
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 255
    invoke-direct {p0}, Lcom/google/android/apps/gmm/photo/d;->b()[I

    move-result-object v0

    .line 256
    aget v2, v0, v2

    aget v0, v0, v3

    mul-int/2addr v0, v2

    .line 258
    if-eq p1, v5, :cond_0

    if-gt v0, p1, :cond_3

    .line 259
    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/d;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/d;->a:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 262
    const/16 v0, 0x1000

    :try_start_0
    new-array v0, v0, [B

    .line 263
    :goto_0
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-eq v3, v5, :cond_1

    .line 265
    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 268
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 269
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v0

    .line 268
    :cond_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 269
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 271
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 279
    :cond_2
    :goto_1
    return-object v0

    .line 275
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/d;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/photo/d;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 276
    int-to-double v2, v0

    int-to-double v4, p1

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/StrictMath;->sqrt(D)D

    move-result-wide v2

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/photo/d;->a(D)I

    move-result v2

    iput v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x4b

    invoke-virtual {v0, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 278
    if-eqz v1, :cond_2

    .line 279
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    goto :goto_1

    .line 278
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_4

    .line 279
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_4
    throw v0
.end method
