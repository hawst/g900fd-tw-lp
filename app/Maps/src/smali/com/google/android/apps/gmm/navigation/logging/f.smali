.class public Lcom/google/android/apps/gmm/navigation/logging/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/shared/net/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/shared/net/c",
        "<",
        "Lcom/google/r/b/a/vc;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/logging/e;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/logging/e;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/logging/f;->a:Lcom/google/android/apps/gmm/navigation/logging/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 10

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x0

    .line 103
    check-cast p1, Lcom/google/r/b/a/vc;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/d;->b()Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v0

    if-nez v0, :cond_1

    iget-boolean v0, p1, Lcom/google/r/b/a/vc;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/f;->a:Lcom/google/android/apps/gmm/navigation/logging/e;

    iput-object p1, v0, Lcom/google/android/apps/gmm/navigation/logging/e;->b:Lcom/google/r/b/a/vc;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/f;->a:Lcom/google/android/apps/gmm/navigation/logging/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/logging/e;->g:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    sget-object v1, Lcom/google/r/b/a/el;->cp:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/vg;

    sget-object v3, Lcom/google/r/b/a/vk;->PARSER:Lcom/google/n/ax;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/logging/f;->a:Lcom/google/android/apps/gmm/navigation/logging/e;

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/logging/e;->b:Lcom/google/r/b/a/vc;

    iget v5, v5, Lcom/google/r/b/a/vc;->e:I

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v6, v5

    move v5, v4

    move v9, v4

    invoke-interface/range {v0 .. v9}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;ZZJIZ)V

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/navigation/logging/e;->a:Ljava/lang/String;

    const-string v1, "ReportTrackParametersResponseProto: enabled=%s, collection_interval_ms=%d,location_max_age_s=%d, start_scrubbing_distance_m=%d,end_scrubbing_distance_m=%d"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    iget-boolean v3, p1, Lcom/google/r/b/a/vc;->b:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget v4, p1, Lcom/google/r/b/a/vc;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    iget v3, p1, Lcom/google/r/b/a/vc;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    const/4 v3, 0x3

    iget v4, p1, Lcom/google/r/b/a/vc;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget v4, p1, Lcom/google/r/b/a/vc;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/navigation/logging/e;->a:Ljava/lang/String;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/d;->b()Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x37

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "REPORT_TRACK_PARAMETERS_REQUEST failed with error code="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
