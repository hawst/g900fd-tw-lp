.class public Lcom/google/android/apps/gmm/streetview/internal/ax;
.super Lcom/google/android/apps/gmm/v/bp;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/r;


# instance fields
.field a:Lcom/google/android/apps/gmm/v/ax;

.field b:Lcom/google/android/apps/gmm/v/ax;

.field private c:[F

.field private final d:[F


# direct methods
.method public constructor <init>(Ljava/lang/Class;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/streetview/internal/az;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/high16 v3, 0x437f0000    # 255.0f

    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/v/bp;-><init>(Ljava/lang/Class;)V

    .line 23
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/v/ax;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/ax;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->a:Lcom/google/android/apps/gmm/v/ax;

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/v/ax;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/ax;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->b:Lcom/google/android/apps/gmm/v/ax;

    .line 32
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->d:[F

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    const/4 v1, 0x0

    invoke-static {p2}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    const/4 v1, 0x1

    invoke-static {p2}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    const/4 v1, 0x2

    invoke-static {p2}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    const/4 v1, 0x3

    invoke-static {p2}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 41
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    const/4 v1, 0x3

    aput p1, v0, v1

    .line 85
    return-void
.end method

.method public final a(FFFF)V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    const/4 v1, 0x3

    aput p4, v0, v1

    .line 80
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/high16 v3, 0x437f0000    # 255.0f

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    const/4 v1, 0x1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    const/4 v1, 0x2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    const/4 v1, 0x3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 67
    return-void
.end method

.method protected final a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V
    .locals 5

    .prologue
    const/16 v4, 0x9

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 90
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/v/bp;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/streetview/internal/az;

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/az;->c:I

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->a:Lcom/google/android/apps/gmm/v/ax;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->d:[F

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    invoke-static {v0, v2, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/streetview/internal/az;

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/az;->d:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->d:[F

    invoke-static {v0, v3, v2, v1, v2}, Landroid/opengl/GLES20;->glUniformMatrix3fv(IIZ[FI)V

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->t:Lcom/google/android/apps/gmm/v/bo;

    instance-of v0, v0, Lcom/google/android/apps/gmm/streetview/internal/ay;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/streetview/internal/ay;

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/ay;->a:I

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->b:Lcom/google/android/apps/gmm/v/ax;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->d:[F

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    invoke-static {v0, v2, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/streetview/internal/ay;

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/ay;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->d:[F

    invoke-static {v0, v3, v2, v1, v2}, Landroid/opengl/GLES20;->glUniformMatrix3fv(IIZ[FI)V

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    if-eqz v0, :cond_1

    .line 109
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ax;->c:[F

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glVertexAttrib4fv(I[FI)V

    .line 111
    :cond_1
    return-void
.end method
