.class public Lcom/google/android/apps/gmm/map/t/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/t/x;


# instance fields
.field private final a:F

.field private final b:Lcom/google/android/apps/gmm/map/b/a/ab;


# direct methods
.method public constructor <init>(FLcom/google/android/apps/gmm/map/b/a/ab;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput p1, p0, Lcom/google/android/apps/gmm/map/t/w;->a:F

    .line 21
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/t/w;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 22
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/t/ac;Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 13

    .prologue
    .line 26
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/w;->a:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/w;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v1

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/t/ac;->c:Lcom/google/android/apps/gmm/map/f/o;

    const/high16 v4, 0x3f800000    # 1.0f

    iget v5, v3, Lcom/google/android/apps/gmm/map/f/o;->g:F

    mul-float/2addr v4, v5

    iget v5, v3, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v5

    div-float v3, v4, v3

    mul-float/2addr v3, v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/b/a/ae;->b(I)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v1

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/t/ac;->a:Lcom/google/android/apps/gmm/map/b/a/m;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v1, p1, Lcom/google/android/apps/gmm/map/t/ac;->e:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    add-float/2addr v0, v1

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    mul-float v3, v0, v0

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    const/4 v5, 0x1

    aget v1, v1, v5

    const/high16 v5, 0x41200000    # 10.0f

    mul-float/2addr v1, v5

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-direct {v4, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v7, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    const/4 v1, 0x0

    const/4 v0, 0x0

    move v12, v0

    move v0, v1

    move v1, v12

    :goto_1
    iget-object v9, v2, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v9, v9

    div-int/lit8 v9, v9, 0x3

    if-ge v1, v9, :cond_3

    invoke-virtual {v2, v1, v6}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    iget-object v9, p1, Lcom/google/android/apps/gmm/map/t/ac;->c:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v10, p1, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    invoke-virtual {v9, v6, v10}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, p1, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    const/4 v10, 0x0

    aget v9, v9, v10

    const/high16 v10, 0x41200000    # 10.0f

    mul-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    iget-object v10, p1, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    const/4 v11, 0x1

    aget v10, v10, v11

    const/high16 v11, 0x41200000    # 10.0f

    mul-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    iput v9, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v10, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v9, 0x0

    iput v9, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    if-eqz v0, :cond_1

    invoke-static {v8, v7, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v0, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v0, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v0, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v0, v7, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v0, v8, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    const/4 v0, 0x1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/ad;Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 13

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v2, 0x1

    const/high16 v12, 0x41200000    # 10.0f

    const/4 v1, 0x0

    .line 31
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/w;->a:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/w;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->a()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/apps/gmm/map/t/ad;->a:Lcom/google/android/apps/gmm/map/f/o;

    const/high16 v6, 0x3f800000    # 1.0f

    iget v7, v5, Lcom/google/android/apps/gmm/map/f/o;->g:F

    mul-float/2addr v6, v7

    iget v7, v5, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v7

    div-float v5, v6, v5

    mul-float/2addr v5, v0

    div-float/2addr v5, v8

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/ae;->b(I)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/apps/gmm/map/t/ad;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    div-float/2addr v0, v8

    mul-float/2addr v0, v12

    mul-float v4, v0, v0

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/t/ad;->c:[F

    aget v0, v0, v1

    mul-float/2addr v0, v12

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v6, p1, Lcom/google/android/apps/gmm/map/t/ad;->c:[F

    aget v6, v6, v2

    mul-float/2addr v6, v12

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-direct {v5, v0, v6}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v7, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v9, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    move v0, v1

    :goto_1
    iget-object v10, v3, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v10, v10

    div-int/lit8 v10, v10, 0x3

    if-ge v0, v10, :cond_0

    invoke-virtual {v3, v0, v7}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    iget-object v10, p1, Lcom/google/android/apps/gmm/map/t/ad;->a:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v11, p1, Lcom/google/android/apps/gmm/map/t/ad;->d:[F

    invoke-virtual {v10, v7, v11}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    iget-object v10, p1, Lcom/google/android/apps/gmm/map/t/ad;->d:[F

    aget v10, v10, v1

    mul-float/2addr v10, v12

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    iget-object v11, p1, Lcom/google/android/apps/gmm/map/t/ad;->d:[F

    aget v11, v11, v2

    mul-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    iput v10, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v11, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v8, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    if-lez v0, :cond_2

    invoke-static {v9, v8, v5, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v10

    cmpg-float v10, v10, v4

    if-gtz v10, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    iget v10, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v10, v9, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v10, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v10, v9, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v10, v8, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v10, v9, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
