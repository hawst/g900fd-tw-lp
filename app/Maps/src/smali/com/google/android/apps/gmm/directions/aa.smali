.class public Lcom/google/android/apps/gmm/directions/aa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/a/c;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/base/a;

.field c:Lcom/google/android/apps/gmm/directions/ad;

.field d:Lcom/google/android/apps/gmm/directions/f/a;

.field e:Ljava/lang/String;

.field f:Z

.field private final g:Landroid/content/res/Resources;

.field private h:Lcom/google/android/apps/gmm/map/r/a/f;

.field private i:Lcom/google/maps/g/a/z;

.field private j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/google/android/apps/gmm/directions/aa;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/aa;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    sget-object v0, Lcom/google/android/apps/gmm/directions/ad;->a:Lcom/google/android/apps/gmm/directions/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/aa;->f:Z

    .line 72
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/aa;->b:Lcom/google/android/apps/gmm/base/a;

    .line 73
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/aa;->g:Landroid/content/res/Resources;

    .line 74
    return-void
.end method

.method private q()V
    .locals 3

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->D_()Lcom/google/android/apps/gmm/map/i/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 327
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/e;->a:Lcom/google/r/b/a/agl;

    iget-object v1, v1, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/directions/ac;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/directions/ac;-><init>(Lcom/google/android/apps/gmm/directions/aa;)V

    .line 326
    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/map/i/a/a;->a(Ljava/util/Collection;Lcom/google/android/apps/gmm/map/i/a/b;)V

    .line 350
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/android/apps/gmm/map/r/a/f;Z)V
    .locals 1

    .prologue
    .line 357
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    .line 358
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 359
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/directions/aa;->f:Z

    .line 360
    sget-object v0, Lcom/google/android/apps/gmm/directions/ad;->b:Lcom/google/android/apps/gmm/directions/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;

    .line 361
    invoke-direct {p0}, Lcom/google/android/apps/gmm/directions/aa;->q()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 365
    monitor-exit p0

    return-void

    .line 357
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/directions/f/a;Ljava/lang/String;)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 260
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    .line 261
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/aa;->j:Ljava/lang/String;

    .line 262
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/f/a;->h:Lcom/google/maps/g/hy;

    if-eqz v1, :cond_1

    iget-boolean v1, v1, Lcom/google/maps/g/hy;->i:Z

    :goto_0
    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/aa;->f:Z

    .line 263
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    new-instance v5, Lcom/google/android/apps/gmm/directions/ab;

    invoke-direct {v5, p0}, Lcom/google/android/apps/gmm/directions/ab;-><init>(Lcom/google/android/apps/gmm/directions/aa;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->g:Landroid/content/res/Resources;

    sget-object v0, Lcom/google/r/b/a/acy;->m:Lcom/google/r/b/a/acy;

    sget-object v2, Lcom/google/r/b/a/acy;->n:Lcom/google/r/b/a/acy;

    sget-object v3, Lcom/google/r/b/a/acy;->s:Lcom/google/r/b/a/acy;

    invoke-static {v0, v2, v3}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v3

    new-instance v0, Lcom/google/android/apps/gmm/directions/d/a;

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/aa;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-static {v4}, Lcom/google/android/apps/gmm/directions/d/a;->a(Lcom/google/android/apps/gmm/map/c/a;)Ljava/util/List;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/d/a;-><init>(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/r/b/a/aha;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/directions/d/b;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/aa;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->l_()Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/directions/ad;->d:Lcom/google/android/apps/gmm/directions/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/directions/b/b;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/directions/b/b;-><init>(Lcom/google/android/apps/gmm/directions/a/c;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    move v1, v0

    .line 262
    goto :goto_0

    .line 263
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/aa;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    sget-object v0, Lcom/google/android/apps/gmm/directions/ad;->b:Lcom/google/android/apps/gmm/directions/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 260
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/directions/f/a;ZLcom/google/android/apps/gmm/map/r/a/e;)V
    .locals 1

    .prologue
    .line 273
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    .line 274
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/directions/aa;->f:Z

    .line 275
    sget-object v0, Lcom/google/android/apps/gmm/directions/ad;->b:Lcom/google/android/apps/gmm/directions/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;

    .line 276
    invoke-virtual {p0, p3}, Lcom/google/android/apps/gmm/directions/aa;->a(Lcom/google/android/apps/gmm/map/r/a/e;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    monitor-exit p0

    return-void

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Lcom/google/android/apps/gmm/map/r/a/e;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;

    sget-object v1, Lcom/google/android/apps/gmm/directions/ad;->b:Lcom/google/android/apps/gmm/directions/ad;

    if-ne v0, v1, :cond_0

    move v0, v3

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v0, v4

    goto :goto_0

    .line 286
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/directions/aa;->a:Ljava/lang/String;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "directionsRequestComplete for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    if-eqz p1, :cond_b

    .line 289
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->a:Lcom/google/r/b/a/agl;

    iget-object v1, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v1, :cond_5

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_1
    iget v0, v0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_2
    if-eqz v0, :cond_2

    .line 290
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->a:Lcom/google/r/b/a/agl;

    iget-object v1, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v1, :cond_7

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, Lcom/google/r/b/a/afn;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->e:Ljava/lang/String;

    .line 293
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/a;->e:Lcom/google/b/c/cv;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/r/a/as;->a(I)I

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 294
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/f/a;->e:Lcom/google/b/c/cv;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/r/a/as;->a(I)I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 295
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v2, v2, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v5, 0x8

    if-ne v2, v5, :cond_8

    move v2, v3

    :goto_4
    if-eqz v2, :cond_9

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v2, v2, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v2}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_3
    if-eqz v2, :cond_9

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v2, v2, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v2}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_4
    :goto_5
    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/aa;->i:Lcom/google/maps/g/a/z;

    .line 296
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/aa;->i:Lcom/google/maps/g/a/z;

    sget-object v5, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    if-ne v2, v5, :cond_a

    .line 297
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v2, v2, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/r/a/as;->a(I)I

    .line 298
    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/aa;->g:Landroid/content/res/Resources;

    .line 299
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v2, v2, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/jm;

    invoke-static {v5, v0, v2}, Lcom/google/android/apps/gmm/map/r/a/as;->a(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/maps/g/a/jm;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v2

    .line 300
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/aa;->g:Landroid/content/res/Resources;

    .line 301
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/jm;

    invoke-static {v4, v1, v0}, Lcom/google/android/apps/gmm/map/r/a/as;->a(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/maps/g/a/jm;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    move-object v0, v2

    .line 306
    :goto_6
    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/g;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/r/a/g;-><init>()V

    .line 307
    iput-object p1, v2, Lcom/google/android/apps/gmm/map/r/a/g;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    .line 308
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/directions/f/a;->a:Lcom/google/maps/g/a/hm;

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/g;->b:Lcom/google/maps/g/a/hm;

    .line 309
    iput-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/g;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 310
    iput-object v1, v2, Lcom/google/android/apps/gmm/map/r/a/g;->d:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/a;->b:Lcom/google/r/b/a/afz;

    iput-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/g;->e:Lcom/google/r/b/a/afz;

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->j:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/g;->f:Ljava/lang/String;

    .line 313
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/f;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/r/a/f;-><init>(Lcom/google/android/apps/gmm/map/r/a/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->D_()Lcom/google/android/apps/gmm/map/i/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/e;->a:Lcom/google/r/b/a/agl;

    iget-object v1, v1, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/directions/ac;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/directions/ac;-><init>(Lcom/google/android/apps/gmm/directions/aa;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/map/i/a/a;->a(Ljava/util/Collection;Lcom/google/android/apps/gmm/map/i/a/b;)V

    .line 323
    :goto_7
    return-void

    .line 289
    :cond_5
    iget-object v0, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto/16 :goto_1

    :cond_6
    move v0, v4

    goto/16 :goto_2

    .line 290
    :cond_7
    iget-object v0, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto/16 :goto_3

    :cond_8
    move v2, v4

    .line 295
    goto/16 :goto_4

    :cond_9
    sget-object v2, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    goto :goto_5

    .line 303
    :cond_a
    sget-object v2, Lcom/google/android/apps/gmm/directions/aa;->a:Ljava/lang/String;

    const-string v2, "Error, status is not SUCCESS, but %s"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/aa;->i:Lcom/google/maps/g/a/z;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_6

    .line 318
    :cond_b
    sget-object v0, Lcom/google/android/apps/gmm/directions/aa;->a:Ljava/lang/String;

    .line 320
    sget-object v0, Lcom/google/android/apps/gmm/directions/ad;->e:Lcom/google/android/apps/gmm/directions/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/directions/b/b;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/directions/b/b;-><init>(Lcom/google/android/apps/gmm/directions/a/c;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_7
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;

    sget-object v1, Lcom/google/android/apps/gmm/directions/ad;->a:Lcom/google/android/apps/gmm/directions/ad;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;

    sget-object v1, Lcom/google/android/apps/gmm/directions/ad;->b:Lcom/google/android/apps/gmm/directions/ad;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;

    sget-object v1, Lcom/google/android/apps/gmm/directions/ad;->c:Lcom/google/android/apps/gmm/directions/ad;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;

    sget-object v1, Lcom/google/android/apps/gmm/directions/ad;->d:Lcom/google/android/apps/gmm/directions/ad;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;

    sget-object v1, Lcom/google/android/apps/gmm/directions/ad;->e:Lcom/google/android/apps/gmm/directions/ad;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized e()Z
    .locals 1

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/aa;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()Lcom/google/android/apps/gmm/directions/f/a;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/a;->e:Lcom/google/b/c/cv;

    .line 121
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/r/a/as;->a(I)I

    .line 122
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/f;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 131
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/f;->d:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 140
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/b/c/cv;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/f;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 146
    :goto_0
    if-nez v0, :cond_3

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/a;->e:Lcom/google/b/c/cv;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/r/a/as;->a(I)I

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    move-object v1, v0

    .line 149
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/f;->d:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 150
    :goto_2
    if-nez v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/a;->e:Lcom/google/b/c/cv;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/r/a/as;->a(I)I

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 153
    :cond_0
    invoke-static {v1, v0}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, v2

    .line 145
    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 149
    goto :goto_2

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method

.method public final k()Lcom/google/maps/g/a/be;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 159
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    if-nez v1, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-object v0

    .line 162
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    .line 163
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v2, v2, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 164
    const/4 v2, 0x0

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v3, v3

    if-gt v3, v2, :cond_2

    :goto_1
    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/f/d/h;->e(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/be;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, v2

    if-nez v0, :cond_3

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-direct {v4, v0, v1}, Lcom/google/android/apps/gmm/map/r/a/ao;-><init>(Lcom/google/maps/g/a/hu;Lcom/google/android/apps/gmm/map/r/a/e;)V

    aput-object v4, v3, v2

    :cond_3
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, v2

    goto :goto_1
.end method

.method public final l()Lcom/google/maps/g/a/hm;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/f;->a()Lcom/google/maps/g/a/hm;

    move-result-object v0

    .line 177
    :goto_0
    return-object v0

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    if-eqz v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/a;->a:Lcom/google/maps/g/a/hm;

    goto :goto_0

    .line 177
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Lcom/google/android/apps/gmm/map/r/a/f;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/a;->h:Lcom/google/maps/g/hy;

    .line 189
    if-eqz v0, :cond_0

    .line 190
    iget-boolean v0, v0, Lcom/google/maps/g/hy;->i:Z

    .line 192
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 198
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->h:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 199
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_0
    if-eqz v0, :cond_3

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_1
    :goto_1
    sget-object v3, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    return v0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public final declared-synchronized p()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 379
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;

    sget-object v2, Lcom/google/android/apps/gmm/directions/ad;->c:Lcom/google/android/apps/gmm/directions/ad;

    if-ne v1, v2, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/aa;->f:Z

    if-nez v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/aa;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/r/b/a/tf;->b:Lcom/google/r/b/a/tf;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/aa;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/r/b/a/tf;Ljava/lang/String;)V

    .line 385
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/aa;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 386
    monitor-exit p0

    return-void

    .line 379
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
