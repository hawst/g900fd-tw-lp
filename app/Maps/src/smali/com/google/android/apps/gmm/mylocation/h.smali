.class public Lcom/google/android/apps/gmm/mylocation/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/mylocation/f;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 57
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/util/g;)Lcom/google/android/apps/gmm/mylocation/b/d;
    .locals 7
    .param p1    # Lcom/google/android/apps/gmm/util/g;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->i:Lcom/google/android/apps/gmm/util/b;

    .line 75
    iget-object v2, v0, Lcom/google/android/apps/gmm/util/b;->c:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/util/b;->c:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    if-eqz v2, :cond_1

    .line 76
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->e:Lcom/google/android/apps/gmm/mylocation/b/d;

    .line 142
    :goto_1
    return-object v0

    :cond_0
    move v2, v1

    .line 75
    goto :goto_0

    .line 79
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/p/b/a;->e()Lcom/google/android/apps/gmm/p/b/b;

    move-result-object v2

    .line 86
    sget-object v4, Lcom/google/android/apps/gmm/mylocation/j;->a:[I

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/p/b/d;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    .line 142
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->a:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto :goto_1

    .line 88
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->d:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto :goto_1

    .line 91
    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/gmm/l;->gm:I

    .line 92
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->gi:I

    .line 93
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/mylocation/i;

    invoke-direct {v2, p1}, Lcom/google/android/apps/gmm/mylocation/i;-><init>(Lcom/google/android/apps/gmm/util/g;)V

    .line 94
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/util/b;->a(Landroid/app/Dialog;)V

    .line 103
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->f:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto :goto_1

    .line 107
    :pswitch_2
    new-instance v6, Landroid/content/Intent;

    const-string v2, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v6, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 108
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-ge v2, v4, :cond_2

    const-string v2, "KeyLimePie"

    sget-object v4, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v2, v3

    :goto_2
    if-eqz v2, :cond_4

    .line 110
    sget v3, Lcom/google/android/apps/gmm/l;->go:I

    sget v2, Lcom/google/android/apps/gmm/l;->gk:I

    sget v5, Lcom/google/android/apps/gmm/l;->mW:I

    iget-object v4, v0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/util/b;->a(ZLcom/google/android/apps/gmm/util/g;ILjava/lang/CharSequence;ILandroid/content/Intent;)V

    .line 126
    :goto_3
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->f:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto :goto_1

    :cond_3
    move v2, v1

    .line 108
    goto :goto_2

    .line 118
    :cond_4
    sget v3, Lcom/google/android/apps/gmm/l;->gn:I

    sget v2, Lcom/google/android/apps/gmm/l;->gj:I

    sget v5, Lcom/google/android/apps/gmm/l;->mW:I

    iget-object v4, v0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/util/b;->a(ZLcom/google/android/apps/gmm/util/g;ILjava/lang/CharSequence;ILandroid/content/Intent;)V

    goto :goto_3

    .line 131
    :pswitch_3
    new-instance v6, Landroid/content/Intent;

    const-string v2, "com.google.android.gsf.GOOGLE_LOCATION_SETTINGS"

    invoke-direct {v6, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 132
    sget v3, Lcom/google/android/apps/gmm/l;->gp:I

    sget v2, Lcom/google/android/apps/gmm/l;->gl:I

    sget v5, Lcom/google/android/apps/gmm/l;->mW:I

    iget-object v4, v0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/util/b;->a(ZLcom/google/android/apps/gmm/util/g;ILjava/lang/CharSequence;ILandroid/content/Intent;)V

    .line 138
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->f:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto/16 :goto_1

    .line 86
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->e()Lcom/google/android/apps/gmm/p/b/b;

    move-result-object v4

    .line 65
    iget-object v0, v4, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v3, Lcom/google/android/apps/gmm/p/b/d;->d:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 66
    :goto_0
    iget-object v3, v4, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->d:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v3, v5, :cond_3

    move v3, v1

    .line 67
    :goto_1
    iget-object v4, v4, Lcom/google/android/apps/gmm/p/b/b;->c:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->d:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v4, v5, :cond_4

    move v4, v1

    .line 68
    :goto_2
    if-nez v0, :cond_0

    if-nez v3, :cond_0

    if-eqz v4, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 65
    goto :goto_0

    :cond_3
    move v3, v2

    .line 66
    goto :goto_1

    :cond_4
    move v4, v2

    .line 67
    goto :goto_2
.end method


# virtual methods
.method public final a(ZZZLcom/google/android/apps/gmm/mylocation/g;)V
    .locals 10
    .param p4    # Lcom/google/android/apps/gmm/mylocation/g;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 152
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0, p4}, Lcom/google/android/apps/gmm/mylocation/h;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/util/g;)Lcom/google/android/apps/gmm/mylocation/b/d;

    move-result-object v0

    .line 154
    :goto_0
    invoke-interface {p4, v0}, Lcom/google/android/apps/gmm/mylocation/g;->a(Lcom/google/android/apps/gmm/mylocation/b/d;)V

    .line 155
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->i:Lcom/google/android/apps/gmm/util/b;

    iget-object v2, v0, Lcom/google/android/apps/gmm/util/b;->c:Landroid/app/Dialog;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/gmm/util/b;->c:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v7

    :goto_1
    if-eqz v2, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->e:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/p/b/a;->e()Lcom/google/android/apps/gmm/p/b/b;

    move-result-object v4

    iget-object v2, v4, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    iget-object v3, v4, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->b:Lcom/google/android/apps/gmm/p/b/d;

    if-eq v2, v5, :cond_3

    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->a:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v2, v5, :cond_5

    :cond_3
    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->b:Lcom/google/android/apps/gmm/p/b/d;

    if-eq v3, v5, :cond_4

    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->a:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v3, v5, :cond_5

    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->d:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto :goto_0

    :cond_5
    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->e:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v2, v5, :cond_6

    new-instance v6, Landroid/content/Intent;

    const-string v2, "com.google.android.gsf.GOOGLE_LOCATION_SETTINGS"

    invoke-direct {v6, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/google/android/apps/gmm/l;->gp:I

    sget v2, Lcom/google/android/apps/gmm/l;->gl:I

    sget v5, Lcom/google/android/apps/gmm/l;->mW:I

    iget-object v4, v0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v2, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/util/b;->a(ZLcom/google/android/apps/gmm/util/g;ILjava/lang/CharSequence;ILandroid/content/Intent;)V

    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->f:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto :goto_0

    :cond_6
    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->d:Lcom/google/android/apps/gmm/p/b/d;

    if-eq v2, v5, :cond_7

    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->b:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v2, v5, :cond_9

    :cond_7
    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->d:Lcom/google/android/apps/gmm/p/b/d;

    if-eq v3, v5, :cond_8

    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->b:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v3, v5, :cond_9

    :cond_8
    new-instance v6, Landroid/content/Intent;

    const-string v2, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v6, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/google/android/apps/gmm/l;->gp:I

    sget v2, Lcom/google/android/apps/gmm/l;->gl:I

    sget v5, Lcom/google/android/apps/gmm/l;->mW:I

    iget-object v4, v0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v2, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/util/b;->a(ZLcom/google/android/apps/gmm/util/g;ILjava/lang/CharSequence;ILandroid/content/Intent;)V

    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->f:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto/16 :goto_0

    :cond_9
    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-eq v2, v5, :cond_a

    if-eqz p1, :cond_b

    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->b:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto/16 :goto_0

    :cond_a
    if-eqz p2, :cond_b

    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->b:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto/16 :goto_0

    :cond_b
    new-instance v5, Lcom/google/android/apps/gmm/mylocation/k;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/mylocation/k;-><init>()V

    invoke-virtual {v0, v7, p4, v5}, Lcom/google/android/apps/gmm/util/b;->a(ZLcom/google/android/apps/gmm/util/g;Lcom/google/android/apps/gmm/util/f;)Z

    move-result v5

    if-eqz v5, :cond_c

    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->g:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto/16 :goto_0

    :cond_c
    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v5}, Lcom/google/android/apps/gmm/mylocation/h;->a(Lcom/google/android/apps/gmm/base/activities/c;)Z

    move-result v5

    if-eqz v5, :cond_1a

    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->d:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v2, v5, :cond_13

    move v2, v7

    :goto_2
    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->d:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v3, v5, :cond_14

    move v3, v7

    :goto_3
    iget-object v4, v4, Lcom/google/android/apps/gmm/p/b/b;->c:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v5, Lcom/google/android/apps/gmm/p/b/d;->d:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v4, v5, :cond_15

    move v4, v7

    :goto_4
    new-instance v6, Landroid/content/Intent;

    if-nez v2, :cond_d

    if-eqz v3, :cond_16

    :cond_d
    const-string v5, "android.settings.LOCATION_SOURCE_SETTINGS"

    :goto_5
    invoke-direct {v6, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v9, Lcom/google/android/apps/gmm/l;->of:I

    invoke-virtual {v8, v9}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x13

    if-ge v8, v9, :cond_e

    const-string v8, "KeyLimePie"

    sget-object v9, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_f

    :cond_e
    move v1, v7

    :cond_f
    if-eqz v1, :cond_17

    if-nez v2, :cond_10

    if-eqz v3, :cond_11

    :cond_10
    const-string v1, "<br/>"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->hK:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->hY:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_11
    if-eqz v4, :cond_12

    const-string v1, "<br/>"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->hK:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->ie:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_12
    :goto_6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    sget v3, Lcom/google/android/apps/gmm/l;->hq:I

    sget v5, Lcom/google/android/apps/gmm/l;->mW:I

    move v1, v7

    move-object v2, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/util/b;->a(ZLcom/google/android/apps/gmm/util/g;ILjava/lang/CharSequence;ILandroid/content/Intent;)V

    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->g:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto/16 :goto_0

    :cond_13
    move v2, v1

    goto/16 :goto_2

    :cond_14
    move v3, v1

    goto/16 :goto_3

    :cond_15
    move v4, v1

    goto/16 :goto_4

    :cond_16
    const-string v5, "android.settings.WIFI_SETTINGS"

    goto/16 :goto_5

    :cond_17
    if-eqz v2, :cond_18

    const-string v1, "<br/>"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->hK:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->ic:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_18
    if-eqz v3, :cond_19

    const-string v1, "<br/>"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->hK:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->id:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    :cond_19
    if-eqz v4, :cond_12

    const-string v1, "<br/>"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->hK:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->ie:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    :cond_1a
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->a:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto/16 :goto_0
.end method
