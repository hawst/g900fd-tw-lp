.class public Lcom/google/android/apps/gmm/cardui/a/ad;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    return-void
.end method

.method private static a(Lcom/google/o/h/a/qf;)Lcom/google/android/apps/gmm/base/g/c;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 68
    new-instance v4, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 69
    iget-object v0, p0, Lcom/google/o/h/a/qf;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    iget-object v1, v4, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/g/i;->c:Lcom/google/maps/a/e;

    .line 70
    iget-object v0, p0, Lcom/google/o/h/a/qf;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    :goto_0
    iget-object v1, v4, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    iput-object v0, v1, Lcom/google/android/apps/gmm/base/g/i;->b:Ljava/lang/String;

    .line 71
    iput-boolean v2, v4, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    .line 72
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_1
    if-eqz v0, :cond_7

    .line 76
    const-string v0, ""

    iget-object v1, v4, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/g/i;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/o/h/a/qf;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/g/g;->n:Ljava/lang/String;

    .line 77
    sget-object v1, Lcom/google/android/apps/gmm/cardui/a/ae;->a:[I

    iget v0, p0, Lcom/google/o/h/a/qf;->g:I

    invoke-static {v0}, Lcom/google/o/h/a/nj;->a(I)Lcom/google/o/h/a/nj;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/o/h/a/nj;->a:Lcom/google/o/h/a/nj;

    :cond_1
    invoke-virtual {v0}, Lcom/google/o/h/a/nj;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 90
    :goto_2
    invoke-virtual {p0}, Lcom/google/o/h/a/qf;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_8

    :cond_2
    move v0, v3

    :goto_3
    if-nez v0, :cond_3

    .line 91
    invoke-virtual {p0}, Lcom/google/o/h/a/qf;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/g/g;->o:Ljava/lang/String;

    .line 94
    :cond_3
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    return-object v0

    .line 70
    :cond_4
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    iput-object v1, p0, Lcom/google/o/h/a/qf;->c:Ljava/lang/Object;

    :cond_5
    move-object v0, v1

    goto :goto_0

    :cond_6
    move v0, v2

    .line 72
    goto :goto_1

    .line 79
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/base/g/e;->a:Lcom/google/android/apps/gmm/base/g/e;

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/g/g;->t:Lcom/google/android/apps/gmm/base/g/e;

    goto :goto_2

    .line 82
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/base/g/e;->b:Lcom/google/android/apps/gmm/base/g/e;

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/g/g;->t:Lcom/google/android/apps/gmm/base/g/e;

    goto :goto_2

    .line 88
    :cond_7
    invoke-virtual {p0}, Lcom/google/o/h/a/qf;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v4, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/g/i;->a:Ljava/lang/String;

    goto :goto_2

    :cond_8
    move v0, v2

    .line 90
    goto :goto_3

    .line 77
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 37
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->u:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/qf;->h()Lcom/google/o/h/a/qf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/qf;

    .line 45
    iget v1, v0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_1

    move v1, v2

    :goto_1
    if-eqz v1, :cond_3

    .line 48
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->h()Lcom/google/android/apps/gmm/cardui/c;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/cardui/b/b;->c:Lcom/google/android/apps/gmm/cardui/b/b;

    .line 51
    :goto_2
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->k()Lcom/google/android/apps/gmm/cardui/b/c;

    move-result-object v2

    .line 54
    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/a/ad;->a(Lcom/google/o/h/a/qf;)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v3

    .line 55
    iget-object v0, v0, Lcom/google/o/h/a/qf;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mr;->h()Lcom/google/o/h/a/mr;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mr;

    .line 52
    invoke-interface {v2, v1, v3, v0}, Lcom/google/android/apps/gmm/cardui/b/c;->a(Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/o/h/a/mr;)Landroid/app/Fragment;

    move-result-object v0

    .line 56
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto :goto_0

    :cond_1
    move v1, v3

    .line 45
    goto :goto_1

    .line 50
    :cond_2
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->h()Lcom/google/android/apps/gmm/cardui/c;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/cardui/c;->h:Lcom/google/android/apps/gmm/cardui/b/b;

    goto :goto_2

    .line 58
    :cond_3
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, v5, v2}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    .line 60
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->D()Lcom/google/android/apps/gmm/place/b/b;

    move-result-object v1

    .line 62
    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/a/ad;->a(Lcom/google/o/h/a/qf;)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    invoke-interface {v1, v0, v3, v5, v5}, Lcom/google/android/apps/gmm/place/b/b;->a(Lcom/google/android/apps/gmm/base/g/c;ZLcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    sget-object v0, Lcom/google/o/h/a/g;->w:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 30
    sget-object v0, Lcom/google/o/h/a/g;->G:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 33
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    const/high16 v1, 0x80000

    .line 24
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
