.class public Lcom/google/android/apps/gmm/z/b/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:F

.field public c:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    .line 32
    iput v1, p0, Lcom/google/android/apps/gmm/z/b/d;->b:F

    .line 33
    iput v1, p0, Lcom/google/android/apps/gmm/z/b/d;->c:F

    .line 34
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 52
    iget v0, p0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    .line 53
    iget v0, p0, Lcom/google/android/apps/gmm/z/b/d;->b:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/gmm/z/b/d;->b:F

    .line 54
    iget v0, p0, Lcom/google/android/apps/gmm/z/b/d;->c:F

    mul-float v1, p1, p1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/z/b/d;->c:F

    .line 55
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 80
    iget v1, p0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    if-nez v1, :cond_0

    move v1, v0

    .line 81
    :goto_0
    iget v2, p0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    if-nez v2, :cond_1

    move v2, v0

    .line 82
    :goto_1
    const-string v0, "FLOAT_STATISTICS_TRACKER"

    new-instance v3, Lcom/google/b/a/ak;

    invoke-direct {v3, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "N"

    iget v4, p0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    .line 83
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v4, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 80
    :cond_0
    iget v1, p0, Lcom/google/android/apps/gmm/z/b/d;->b:F

    iget v2, p0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    goto :goto_0

    .line 81
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/apps/gmm/z/b/d;->c:F

    mul-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/gmm/z/b/d;->b:F

    iget v3, p0, Lcom/google/android/apps/gmm/z/b/d;->b:F

    mul-float/2addr v2, v3

    sub-float/2addr v0, v2

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iget v0, p0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    int-to-double v4, v0

    div-double/2addr v2, v4

    double-to-float v0, v2

    move v2, v0

    goto :goto_1

    .line 83
    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "SUM"

    iget v4, p0, Lcom/google/android/apps/gmm/z/b/d;->b:F

    .line 84
    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v4, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "SUM_SQUARES"

    iget v4, p0, Lcom/google/android/apps/gmm/z/b/d;->c:F

    .line 85
    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v4, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "AVG"

    .line 86
    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/google/b/a/al;

    invoke-direct {v4}, Lcom/google/b/a/al;-><init>()V

    iget-object v5, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v4, v5, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v4, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v4, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "DEV"

    .line 87
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 88
    invoke-virtual {v3}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
