.class public Lcom/google/android/apps/gmm/startpage/c/r;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/startpage/e/a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/cs;
    .locals 22

    .prologue
    .line 60
    const/4 v2, 0x6

    new-array v3, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 61
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x1

    const/4 v4, -0x2

    .line 62
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x2

    const/4 v4, 0x1

    .line 64
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x3

    const/4 v4, 0x3

    .line 65
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->aj:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v4, 0x4

    const/4 v2, 0x3

    new-array v5, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v6, 0x0

    .line 67
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/a;->d()Ljava/lang/Integer;

    move-result-object v2

    sget-object v7, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v6, 0x1

    const/4 v2, 0x4

    new-array v7, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v8, -0x1

    .line 69
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v2, 0x1

    const/4 v8, -0x1

    .line 70
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v2, 0x2

    sget-object v8, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    .line 71
    sget-object v9, Lcom/google/android/libraries/curvular/g;->bs:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v8, 0x3

    .line 72
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/a;->e()Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v2

    sget-object v9, Lcom/google/android/apps/gmm/base/k/j;->aj:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v9, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v7, v8

    .line 68
    invoke-static {v7}, Lcom/google/android/apps/gmm/base/k/aa;->C([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v6, 0x2

    .line 73
    const/4 v2, 0x2

    new-array v7, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v8, 0x1

    const/4 v2, 0x3

    new-array v9, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v10, 0x0

    const-wide v12, 0x4069400000000000L    # 202.0

    new-instance v11, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_0

    double-to-int v12, v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v13, 0xffffff

    and-int/2addr v12, v13

    shl-int/lit8 v12, v12, 0x8

    or-int/lit8 v12, v12, 0x1

    iput v12, v2, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v11, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v9, v10

    const/4 v10, 0x1

    const/4 v2, 0x5

    new-array v11, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    sget v12, Lcom/google/android/apps/gmm/e;->ao:I

    invoke-static {v12}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x1

    sget v12, Lcom/google/android/apps/gmm/e;->an:I

    invoke-static {v12}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x2

    const/16 v12, 0x15

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x3

    sget-object v12, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    sget-object v13, Lcom/google/android/libraries/curvular/g;->bs:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v12, 0x4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/a;->f()Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v2

    sget-object v13, Lcom/google/android/apps/gmm/base/k/j;->aj:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v13, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    invoke-static {v11}, Lcom/google/android/apps/gmm/base/k/aa;->C([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v9, v10

    const/4 v10, 0x2

    const/16 v2, 0x9

    new-array v11, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x1

    const/16 v12, 0x14

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    new-instance v13, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct {v13, v12, v14, v15}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v12, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x2

    const/16 v12, 0xc

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    new-instance v13, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct {v13, v12, v14, v15}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v12, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x3

    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x4

    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v12, 0x5

    const-wide/high16 v14, 0x4026000000000000L    # 11.0

    new-instance v13, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_1

    double-to-int v14, v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v15, 0xffffff

    and-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x8

    or-int/lit8 v14, v14, 0x1

    iput v14, v2, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v13, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bh:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x6

    const-class v13, Lcom/google/android/apps/gmm/startpage/c/s;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/a;->g()Lcom/google/android/apps/gmm/startpage/e/b;

    move-result-object v2

    new-instance v14, Lcom/google/android/libraries/curvular/ao;

    const/4 v15, 0x0

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    invoke-direct {v14, v13, v2}, Lcom/google/android/libraries/curvular/ao;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ah;)V

    aput-object v14, v11, v12

    const/4 v12, 0x7

    const/4 v2, 0x2

    new-array v13, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v14, 0x0

    const-wide/high16 v16, 0x403e000000000000L    # 30.0

    new-instance v15, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_2

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    iput v0, v2, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v15, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    const/4 v2, 0x1

    const/4 v14, -0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v13}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v13, "android.view.View"

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v12

    const/16 v12, 0x8

    const-class v13, Lcom/google/android/apps/gmm/startpage/c/s;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/a;->h()Lcom/google/android/apps/gmm/startpage/e/b;

    move-result-object v2

    new-instance v14, Lcom/google/android/libraries/curvular/ao;

    const/4 v15, 0x0

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    invoke-direct {v14, v13, v2}, Lcom/google/android/libraries/curvular/ao;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ah;)V

    aput-object v14, v11, v12

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v11}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v11, "android.widget.LinearLayout"

    sget-object v12, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    invoke-virtual {v2, v11}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v11

    const/4 v2, 0x3

    new-array v12, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v13, 0x2

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    new-instance v14, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    iget-object v15, v11, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-direct {v14, v13, v15, v0}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v13, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    aput-object v13, v12, v2

    const/4 v2, 0x1

    const/16 v13, 0x15

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    new-instance v14, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-direct {v14, v13, v15, v0}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v13, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    aput-object v13, v12, v2

    const/4 v2, 0x2

    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    aput-object v13, v12, v2

    const/4 v2, 0x4

    new-array v13, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->j()Lcom/google/android/libraries/curvular/ar;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x1

    sget v14, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v14, 0x2

    const-wide/high16 v16, 0x4038000000000000L    # 24.0

    new-instance v15, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_3

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x2

    move/from16 v0, v16

    iput v0, v2, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v15, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    const/4 v14, 0x3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/a;->c()Ljava/lang/CharSequence;

    move-result-object v2

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    invoke-static {v13}, Lcom/google/android/apps/gmm/base/k/aa;->t([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    invoke-virtual {v2, v12}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v12

    const/4 v2, 0x4

    new-array v13, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/16 v14, 0x14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    new-instance v15, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v15, v14, v0, v1}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v14, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x1

    const/4 v14, 0x2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    new-instance v15, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    iget-object v0, v11, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v15, v14, v0, v1}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v14, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x2

    const/16 v14, 0x10

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    new-instance v15, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    iget-object v0, v12, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v15, v14, v0, v1}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v14, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x3

    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/16 v2, 0xb

    new-array v14, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v15, -0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v14, v2

    const/4 v2, 0x1

    const/4 v15, -0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v14, v2

    const/4 v2, 0x2

    const/4 v15, 0x5

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->bH:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v14, v2

    const/4 v2, 0x3

    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->j()Lcom/google/android/libraries/curvular/ar;

    move-result-object v15

    aput-object v15, v14, v2

    const/4 v2, 0x4

    const/high16 v15, 0x3f800000    # 1.0f

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    sget-object v16, Lcom/google/android/apps/gmm/base/k/j;->J:Lcom/google/android/apps/gmm/base/k/j;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v14, v2

    const/4 v2, 0x5

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->aD:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v14, v2

    const/4 v2, 0x6

    sget-object v15, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    sget-object v16, Lcom/google/android/libraries/curvular/g;->O:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v14, v2

    const/4 v15, 0x7

    const-wide/high16 v16, 0x4038000000000000L    # 24.0

    new-instance v18, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_4

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x2

    move/from16 v0, v16

    iput v0, v2, Landroid/util/TypedValue;->data:I

    :goto_4
    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bR:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v14, v15

    const/16 v2, 0x8

    sget v15, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v15}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v14, v2

    const/16 v15, 0x9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/a;->b()Ljava/lang/CharSequence;

    move-result-object v2

    sget-object v16, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v14, v15

    const/16 v2, 0xa

    sget v15, Lcom/google/android/apps/gmm/e;->ap:I

    invoke-static {v15}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v14, v2

    invoke-static {v14}, Lcom/google/android/apps/gmm/base/k/aa;->v([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    invoke-virtual {v2, v13}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v13

    const/4 v2, 0x4

    new-array v14, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/a;->a()Ljava/lang/Boolean;

    move-result-object v2

    const/16 v16, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    const/16 v17, 0x8

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    sget-object v18, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v2, v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v14, v15

    const/4 v2, 0x1

    aput-object v12, v14, v2

    const/4 v2, 0x2

    aput-object v13, v14, v2

    const/4 v2, 0x3

    aput-object v11, v14, v2

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v14}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v11, "android.widget.RelativeLayout"

    sget-object v12, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    invoke-virtual {v2, v11}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v9, v10

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v9}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v9, "android.widget.FrameLayout"

    sget-object v10, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v7, v8

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v7}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v7, "android.widget.LinearLayout"

    sget-object v8, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v5, v6

    .line 66
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v5}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v5, "android.widget.FrameLayout"

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v4, 0x5

    const-class v5, Lcom/google/android/apps/gmm/startpage/c/c;

    .line 76
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/a;->i()Lcom/google/android/apps/gmm/startpage/e/e;

    move-result-object v2

    .line 74
    new-instance v6, Lcom/google/android/libraries/curvular/ao;

    const/4 v7, 0x0

    invoke-static {v7, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    invoke-direct {v6, v5, v2}, Lcom/google/android/libraries/curvular/ao;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ah;)V

    aput-object v6, v3, v4

    .line 60
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v3}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.LinearLayout"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    return-object v2

    .line 73
    :cond_0
    const-wide/high16 v14, 0x4060000000000000L    # 128.0

    mul-double/2addr v12, v14

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v12, v13, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v13, 0xffffff

    and-int/2addr v12, v13

    shl-int/lit8 v12, v12, 0x8

    or-int/lit8 v12, v12, 0x11

    iput v12, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    :cond_1
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v14, v14, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v14, v15, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v15, 0xffffff

    and-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x8

    or-int/lit8 v14, v14, 0x11

    iput v14, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    :cond_2
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v16, v16, v18

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v16

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x11

    move/from16 v0, v16

    iput v0, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    :cond_3
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v16, v16, v18

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v16

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x12

    move/from16 v0, v16

    iput v0, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3

    :cond_4
    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    mul-double v16, v16, v20

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v16

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x12

    move/from16 v0, v16

    iput v0, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_4
.end method
