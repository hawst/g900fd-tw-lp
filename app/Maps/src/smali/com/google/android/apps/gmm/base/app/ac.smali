.class public Lcom/google/android/apps/gmm/base/app/ac;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;>;"
        }
    .end annotation
.end field

.field static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 68
    const-class v0, Lcom/google/android/apps/gmm/base/support/f;

    const-class v1, Lcom/google/android/apps/gmm/base/f/au;

    const-class v2, Lcom/google/android/apps/gmm/base/f/bb;

    const-class v3, Lcom/google/android/apps/gmm/place/c/f;

    const-class v4, Lcom/google/android/apps/gmm/place/review/b/a;

    const-class v5, Lcom/google/android/apps/gmm/base/f/av;

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Class;

    const-class v7, Lcom/google/android/apps/gmm/base/f/g;

    aput-object v7, v6, v9

    const-class v7, Lcom/google/android/apps/gmm/place/c/u;

    aput-object v7, v6, v10

    const-class v7, Lcom/google/android/apps/gmm/place/c/q;

    aput-object v7, v6, v11

    const-class v7, Lcom/google/android/apps/gmm/place/c/e;

    aput-object v7, v6, v12

    const-class v7, Lcom/google/android/apps/gmm/place/c/v;

    aput-object v7, v6, v13

    .line 69
    invoke-static/range {v0 .. v6}, Lcom/google/b/c/dn;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v7

    .line 83
    sput-object v7, Lcom/google/android/apps/gmm/base/app/ac;->a:Ljava/util/Set;

    const-class v0, Lcom/google/android/apps/gmm/search/k;

    const-class v1, Lcom/google/android/apps/gmm/suggest/c/c;

    const-class v2, Lcom/google/android/apps/gmm/base/f/bo;

    const-class v3, Lcom/google/android/apps/gmm/startpage/c/t;

    const-class v4, Lcom/google/android/apps/gmm/navigation/navui/a/f;

    const-class v5, Lcom/google/android/apps/gmm/directions/c/u;

    const/16 v6, 0x8

    new-array v6, v6, [Ljava/lang/Class;

    const-class v8, Lcom/google/android/apps/gmm/directions/c/u;

    aput-object v8, v6, v9

    const-class v8, Lcom/google/android/apps/gmm/directions/c/l;

    aput-object v8, v6, v10

    const-class v8, Lcom/google/android/apps/gmm/directions/c/l;

    aput-object v8, v6, v11

    const-class v8, Lcom/google/android/apps/gmm/directions/c/c;

    aput-object v8, v6, v12

    const-class v8, Lcom/google/android/apps/gmm/navigation/navui/a/e;

    aput-object v8, v6, v13

    const/4 v8, 0x5

    const-class v9, Lcom/google/android/apps/gmm/directions/c/j;

    aput-object v9, v6, v8

    const/4 v8, 0x6

    const-class v9, Lcom/google/android/apps/gmm/navigation/navui/a/a;

    aput-object v9, v6, v8

    const/4 v8, 0x7

    const-class v9, Lcom/google/android/apps/gmm/directions/c/s;

    aput-object v9, v6, v8

    .line 85
    invoke-static/range {v0 .. v6}, Lcom/google/b/c/dn;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    .line 84
    const-string v1, "set1"

    if-nez v7, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v1, "set2"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {v0, v7}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v1

    new-instance v2, Lcom/google/b/c/jq;

    invoke-direct {v2, v7, v1, v0}, Lcom/google/b/c/jq;-><init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    sput-object v2, Lcom/google/android/apps/gmm/base/app/ac;->b:Ljava/util/Set;

    .line 83
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/libraries/curvular/bd;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Lcom/google/android/libraries/curvular/cd;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/cd;-><init>(Landroid/content/Context;)V

    .line 111
    new-instance v1, Lcom/google/android/libraries/curvular/bd;

    invoke-direct {v1, p0, v0}, Lcom/google/android/libraries/curvular/bd;-><init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/cc;)V

    .line 117
    return-object v1
.end method

.method public static a(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/shared/c/a/j;)V
    .locals 6

    .prologue
    .line 126
    new-instance v0, Lcom/google/android/apps/gmm/base/app/ad;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/app/ad;-><init>(Lcom/google/android/libraries/curvular/bd;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x7

    .line 133
    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 126
    invoke-interface {p1, v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 135
    return-void
.end method
