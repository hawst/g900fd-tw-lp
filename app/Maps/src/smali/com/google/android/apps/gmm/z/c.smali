.class public Lcom/google/android/apps/gmm/z/c;
.super Lcom/google/android/apps/gmm/z/b/e;
.source "PG"


# instance fields
.field private final a:[Lcom/google/b/f/t;

.field private final h:I


# direct methods
.method public varargs constructor <init>(ILcom/google/android/apps/gmm/shared/c/f;[Lcom/google/b/f/t;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/z/b/e;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 29
    iput-object p3, p0, Lcom/google/android/apps/gmm/z/c;->a:[Lcom/google/b/f/t;

    .line 30
    iput p1, p0, Lcom/google/android/apps/gmm/z/c;->h:I

    .line 31
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/b/f/b/a/bc;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 43
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/z/b/e;->a(Lcom/google/b/f/b/a/bc;)V

    .line 44
    invoke-static {}, Lcom/google/b/f/b/a/cg;->newBuilder()Lcom/google/b/f/b/a/ci;

    move-result-object v2

    .line 45
    iget v0, p0, Lcom/google/android/apps/gmm/z/c;->h:I

    iget v1, v2, Lcom/google/b/f/b/a/ci;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v2, Lcom/google/b/f/b/a/ci;->a:I

    iput v0, v2, Lcom/google/b/f/b/a/ci;->c:I

    .line 46
    iget-object v3, p0, Lcom/google/android/apps/gmm/z/c;->a:[Lcom/google/b/f/t;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 47
    iget v0, v0, Lcom/google/b/f/t;->gy:I

    invoke-static {}, Lcom/google/b/f/cm;->newBuilder()Lcom/google/b/f/co;

    move-result-object v5

    iget v6, v5, Lcom/google/b/f/co;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v5, Lcom/google/b/f/co;->a:I

    iput v0, v5, Lcom/google/b/f/co;->b:I

    invoke-virtual {v5}, Lcom/google/b/f/co;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v2}, Lcom/google/b/f/b/a/ci;->c()V

    iget-object v5, v2, Lcom/google/b/f/b/a/ci;->b:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    invoke-direct {v6}, Lcom/google/n/ao;-><init>()V

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v8, v6, Lcom/google/n/ao;->d:Z

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 50
    :cond_1
    iget-object v0, p1, Lcom/google/b/f/b/a/bc;->i:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/b/f/b/a/ci;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v8, v0, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p1, Lcom/google/b/f/b/a/bc;->a:I

    .line 51
    invoke-static {p1}, Lcom/google/android/apps/gmm/z/b/b;->a(Lcom/google/b/f/b/a/bc;)Lcom/google/b/f/b/a/bc;

    .line 54
    return-void
.end method
