.class public Lcom/google/android/apps/gmm/map/t/v;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/t/x;


# static fields
.field private static final a:Lcom/google/android/apps/gmm/map/b/a/y;

.field private static final b:Lcom/google/android/apps/gmm/map/b/a/y;

.field private static final c:Lcom/google/android/apps/gmm/map/b/a/y;


# instance fields
.field private final d:[Lcom/google/android/apps/gmm/map/internal/c/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/v;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/v;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/v;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/h;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/h;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/c/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/v;->d:[Lcom/google/android/apps/gmm/map/internal/c/h;

    .line 26
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/t/ac;Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 19

    .prologue
    .line 30
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/t/v;->d:[Lcom/google/android/apps/gmm/map/internal/c/h;

    array-length v11, v10

    const/4 v2, 0x0

    move v9, v2

    :goto_0
    if-ge v9, v11, :cond_c

    aget-object v2, v10, v9

    .line 31
    iget-object v12, v2, Lcom/google/android/apps/gmm/map/internal/c/h;->d:Lcom/google/android/apps/gmm/map/b/a/ax;

    const/4 v2, 0x0

    move v8, v2

    :goto_1
    iget-object v2, v12, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v2, :cond_0

    iget-object v2, v12, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    if-lez v2, :cond_0

    iget-object v2, v12, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    :goto_2
    if-ge v8, v2, :cond_a

    sget-object v2, Lcom/google/android/apps/gmm/map/t/v;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    sget-object v3, Lcom/google/android/apps/gmm/map/t/v;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    sget-object v4, Lcom/google/android/apps/gmm/map/t/v;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v12, v8, v2, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    sget-object v2, Lcom/google/android/apps/gmm/map/t/v;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    sget-object v3, Lcom/google/android/apps/gmm/map/t/v;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    sget-object v4, Lcom/google/android/apps/gmm/map/t/v;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/t/ac;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    const/4 v13, 0x0

    aget v7, v7, v13

    const/high16 v13, 0x41200000    # 10.0f

    mul-float/2addr v7, v13

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    const/4 v14, 0x1

    aget v13, v13, v14

    const/high16 v14, 0x41200000    # 10.0f

    mul-float/2addr v13, v14

    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    move-result v13

    iput v7, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v13, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v7, 0x0

    iput v7, v6, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/t/ac;->c:Lcom/google/android/apps/gmm/map/f/o;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    invoke-virtual {v6, v2, v7}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_b

    .line 32
    const/4 v2, 0x1

    .line 35
    :goto_5
    return v2

    .line 31
    :cond_0
    iget-object v2, v12, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x9

    goto :goto_2

    :cond_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/ac;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    const/high16 v7, 0x41200000    # 10.0f

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    const/4 v13, 0x1

    aget v7, v7, v13

    const/high16 v13, 0x41200000    # 10.0f

    mul-float/2addr v7, v13

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    iput v6, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v7, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v6, 0x0

    iput v6, v2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/ac;->c:Lcom/google/android/apps/gmm/map/f/o;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    invoke-virtual {v2, v3, v6}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    :cond_2
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/ac;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    const/4 v6, 0x0

    aget v3, v3, v6

    const/high16 v6, 0x41200000    # 10.0f

    mul-float/2addr v3, v6

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    const/high16 v7, 0x41200000    # 10.0f

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v6, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v3, 0x0

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/ac;->c:Lcom/google/android/apps/gmm/map/f/o;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    invoke-virtual {v2, v4, v3}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x0

    goto :goto_3

    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/ac;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    const/high16 v4, 0x41200000    # 10.0f

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    const/4 v6, 0x1

    aget v4, v4, v6

    const/high16 v6, 0x41200000    # 10.0f

    mul-float/2addr v4, v6

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v4, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v3, 0x0

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    const/high16 v2, 0x41200000    # 10.0f

    mul-float v3, v5, v2

    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/apps/gmm/map/t/ac;->e:F

    const/high16 v4, 0x41200000    # 10.0f

    mul-float v7, v2, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v2

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/t/ac;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/t/ac;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/t/ac;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/t/ac;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v14

    iget v15, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v0, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v16, v0

    iget v0, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v17, v0

    iget v0, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v18, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->min(II)I

    move-result v17

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->min(II)I

    move-result v16

    sub-int v16, v16, v14

    div-int/lit8 v17, v2, 0x2

    sub-int v16, v16, v17

    move/from16 v0, v16

    if-lt v15, v0, :cond_4

    iget v15, v13, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v0, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v16, v0

    iget v0, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v17, v0

    iget v0, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v18, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I

    move-result v17

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->max(II)I

    move-result v16

    add-int v16, v16, v14

    div-int/lit8 v17, v2, 0x2

    add-int v16, v16, v17

    move/from16 v0, v16

    if-gt v15, v0, :cond_4

    iget v15, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v0, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v16, v0

    iget v0, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v17, v0

    iget v0, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move/from16 v18, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->min(II)I

    move-result v17

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->min(II)I

    move-result v16

    sub-int v16, v16, v14

    div-int/lit8 v17, v2, 0x2

    sub-int v16, v16, v17

    move/from16 v0, v16

    if-lt v15, v0, :cond_4

    iget v13, v13, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int/2addr v4, v14

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    if-gt v13, v2, :cond_4

    const/4 v2, 0x1

    :goto_6
    if-nez v2, :cond_5

    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_4
    const/4 v2, 0x0

    goto :goto_6

    :cond_5
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/t/ac;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/t/ac;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/t/ac;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/t/ac;->a(FLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;F)Z

    move-result v2

    if-nez v2, :cond_6

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/t/ac;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/t/ac;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/t/ac;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/t/ac;->a(FLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;F)Z

    move-result v2

    if-nez v2, :cond_6

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/t/ac;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/t/ac;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/t/ac;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/t/ac;->a(FLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;F)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_6
    const/4 v2, 0x1

    goto/16 :goto_3

    :cond_7
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/ac;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/t/ac;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/t/ac;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/t/ac;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v6, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v7, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v6, v7

    iget v7, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v13, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v7, v13

    mul-int/2addr v6, v7

    iget v7, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v13, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v7, v13

    iget v13, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v14, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v13, v14

    mul-int/2addr v7, v13

    add-int/2addr v6, v7

    int-to-double v6, v6

    iget v13, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v14, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v13, v14

    iget v14, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v15, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v14, v15

    mul-int/2addr v13, v14

    iget v14, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v4, v14, v4

    iget v14, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v15, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v14, v15

    mul-int/2addr v4, v14

    add-int/2addr v4, v13

    int-to-double v14, v4

    iget v4, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v13, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v13

    iget v13, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v0, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move/from16 v16, v0

    sub-int v13, v13, v16

    mul-int/2addr v4, v13

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v13, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v3, v13

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, v5

    mul-int/2addr v2, v3

    add-int/2addr v2, v4

    int-to-double v2, v2

    sub-double v4, v6, v14

    sub-double/2addr v4, v2

    const-wide/16 v16, 0x0

    cmpg-double v13, v16, v14

    if-gtz v13, :cond_8

    cmpg-double v13, v14, v6

    if-gtz v13, :cond_8

    const-wide/16 v14, 0x0

    cmpg-double v13, v14, v2

    if-gtz v13, :cond_8

    cmpg-double v2, v2, v6

    if-gtz v2, :cond_8

    const-wide/16 v2, 0x0

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_8

    cmpg-double v2, v4, v6

    if-gtz v2, :cond_8

    const/4 v2, 0x1

    goto/16 :goto_3

    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_9
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto/16 :goto_1

    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 30
    :cond_b
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto/16 :goto_0

    .line 35
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_5
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/ad;Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method
