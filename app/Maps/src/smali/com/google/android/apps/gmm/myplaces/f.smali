.class Lcom/google/android/apps/gmm/myplaces/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/myplaces/h;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/myplaces/c;

.field private final b:Z

.field private final c:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/apps/gmm/map/internal/d/b;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/myplaces/c;ZLcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/map/internal/d/b;)V
    .locals 0
    .param p4    # Lcom/google/android/apps/gmm/map/internal/d/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/map/internal/d/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 201
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/f;->a:Lcom/google/android/apps/gmm/myplaces/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/myplaces/f;->b:Z

    .line 203
    iput-object p3, p0, Lcom/google/android/apps/gmm/myplaces/f;->c:Lcom/google/android/apps/gmm/x/o;

    .line 204
    iput-object p4, p0, Lcom/google/android/apps/gmm/myplaces/f;->d:Lcom/google/android/apps/gmm/map/internal/d/b;

    .line 205
    return-void
.end method


# virtual methods
.method public final a(ZLjava/lang/String;Lcom/google/e/a/a/a/b;Lcom/google/e/a/a/a/b;)V
    .locals 9
    .param p3    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 210
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 211
    if-eqz p1, :cond_6

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/f;->a:Lcom/google/android/apps/gmm/myplaces/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/myplaces/c;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j_()Lcom/google/android/apps/gmm/myplaces/a/a;

    move-result-object v1

    .line 213
    invoke-interface {v1, p2}, Lcom/google/android/apps/gmm/myplaces/a/a;->a(Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/f;->c:Lcom/google/android/apps/gmm/x/o;

    .line 216
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->f()Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/myplaces/f;->b:Z

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v4, Lcom/google/android/apps/gmm/base/g/i;->e:Ljava/lang/Boolean;

    .line 217
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-boolean v7, v3, Lcom/google/android/apps/gmm/base/g/i;->f:Z

    .line 218
    iget-object v3, p0, Lcom/google/android/apps/gmm/myplaces/f;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/x/o;->b(Ljava/io/Serializable;)V

    .line 221
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/myplaces/f;->b:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/f;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-interface {v1, v3, v0}, Lcom/google/android/apps/gmm/myplaces/a/a;->a(ZLcom/google/android/apps/gmm/base/g/c;)V

    .line 225
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/myplaces/f;->b:Z

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/f;->a:Lcom/google/android/apps/gmm/myplaces/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/myplaces/c;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/f;->c:Lcom/google/android/apps/gmm/x/o;

    .line 227
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/f;->a:Lcom/google/android/apps/gmm/myplaces/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/myplaces/c;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/gmm/startpage/b/a;

    sget-object v4, Lcom/google/android/apps/gmm/startpage/b/b;->c:Lcom/google/android/apps/gmm/startpage/b/b;

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/b/a;-><init>(Lcom/google/android/apps/gmm/base/g/c;Ljava/lang/String;Ljava/lang/Integer;Lcom/google/android/apps/gmm/startpage/b/b;Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 226
    invoke-interface {v8, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/f;->d:Lcom/google/android/apps/gmm/map/internal/d/b;

    if-eqz v0, :cond_2

    .line 231
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/f;->d:Lcom/google/android/apps/gmm/map/internal/d/b;

    iget-object v0, p3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v7}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_4

    move v0, v7

    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p3, v7}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v7

    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/a;->k:Landroid/os/Handler;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/a;->k:Landroid/os/Handler;

    const/16 v3, 0x8

    new-instance v4, Lcom/google/b/a/an;

    invoke-direct {v4, v1, p3}, Lcom/google/b/a/an;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 233
    :cond_2
    if-eqz p4, :cond_3

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/f;->a:Lcom/google/android/apps/gmm/myplaces/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/myplaces/c;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/j/ah;

    invoke-direct {v1, v6, p4}, Lcom/google/android/apps/gmm/map/j/ah;-><init>(ZLcom/google/e/a/a/a/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 245
    :cond_3
    :goto_2
    return-void

    :cond_4
    move v0, v6

    .line 231
    goto :goto_0

    :cond_5
    move v0, v6

    goto :goto_1

    .line 238
    :cond_6
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/c;->a:Ljava/lang/String;

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/f;->a:Lcom/google/android/apps/gmm/myplaces/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/myplaces/c;->c()V

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/f;->a:Lcom/google/android/apps/gmm/myplaces/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/myplaces/c;->a(Lcom/google/android/apps/gmm/myplaces/c;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/f;->a:Lcom/google/android/apps/gmm/myplaces/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/myplaces/c;->b(Lcom/google/android/apps/gmm/myplaces/c;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->gA:I

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 242
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2
.end method
