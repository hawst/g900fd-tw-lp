.class public Lcom/google/android/apps/gmm/v/ar;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/v/as;

.field b:Lcom/google/android/apps/gmm/v/at;

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field final g:Z

.field private h:Landroid/graphics/Bitmap;

.field private i:Ljava/nio/ByteBuffer;

.field private j:Lcom/google/android/apps/gmm/v/s;

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private l:I


# direct methods
.method public constructor <init>(IIZ)V
    .locals 0

    .prologue
    .line 316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 317
    iput p1, p0, Lcom/google/android/apps/gmm/v/ar;->c:I

    .line 318
    iput p2, p0, Lcom/google/android/apps/gmm/v/ar;->d:I

    .line 319
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/v/ar;->g:Z

    .line 320
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/google/android/apps/gmm/v/ao;Z)V
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/google/android/apps/gmm/v/ar;-><init>(Landroid/content/res/Resources;ILcom/google/android/apps/gmm/v/ao;Z)V

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;ILcom/google/android/apps/gmm/v/ao;Z)V
    .locals 6

    .prologue
    .line 115
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/v/ar;-><init>(Landroid/content/res/Resources;ILcom/google/android/apps/gmm/v/ao;ZZ)V

    .line 116
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;ILcom/google/android/apps/gmm/v/ao;ZZ)V
    .locals 4

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    invoke-static {p1, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->c:I

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->d:I

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/v/ar;->c:I

    iget v2, p0, Lcom/google/android/apps/gmm/v/ar;->d:I

    mul-int/2addr v1, v2

    if-ne v0, v1, :cond_4

    iget v0, p0, Lcom/google/android/apps/gmm/v/ar;->c:I

    if-eqz v0, :cond_4

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 148
    if-eqz v0, :cond_0

    .line 149
    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    if-nez p5, :cond_1

    if-eqz p3, :cond_2

    .line 159
    iget-object v0, p3, Lcom/google/android/apps/gmm/v/ao;->a:Lcom/google/android/apps/gmm/v/ap;

    if-eqz p4, :cond_5

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/v/ap;->e:Z

    :goto_0
    if-nez v0, :cond_2

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, p4}, Lcom/google/android/apps/gmm/v/ar;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    .line 162
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->e:I

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->f:I

    .line 165
    if-eqz p4, :cond_3

    .line 166
    iget v0, p0, Lcom/google/android/apps/gmm/v/ar;->e:I

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->c:I

    .line 167
    iget v0, p0, Lcom/google/android/apps/gmm/v/ar;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->d:I

    .line 170
    :cond_3
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/v/ar;->g:Z

    .line 171
    return-void

    .line 152
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x78

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "The image resource["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] has experienced a decoding error. BitmapFactory.decodeResource yielded a bitmap with a null Config."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_5
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/v/ap;->d:Z

    goto :goto_0
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;Lcom/google/android/apps/gmm/v/ao;Z)V
    .locals 1

    .prologue
    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->c:I

    .line 242
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->d:I

    .line 244
    if-eqz p2, :cond_2

    iget-object v0, p2, Lcom/google/android/apps/gmm/v/ao;->a:Lcom/google/android/apps/gmm/v/ap;

    if-eqz p3, :cond_1

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/v/ap;->e:Z

    :goto_0
    if-nez v0, :cond_2

    .line 245
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    invoke-static {p1, v0, p3}, Lcom/google/android/apps/gmm/v/ar;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    .line 250
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->e:I

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->f:I

    .line 252
    if-eqz p3, :cond_0

    .line 253
    iget v0, p0, Lcom/google/android/apps/gmm/v/ar;->e:I

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->c:I

    .line 254
    iget v0, p0, Lcom/google/android/apps/gmm/v/ar;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->d:I

    .line 256
    :cond_0
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/v/ar;->g:Z

    .line 257
    return-void

    .line 244
    :cond_1
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/v/ap;->d:Z

    goto :goto_0

    .line 247
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/as;IIIIZ)V
    .locals 0

    .prologue
    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/ar;->a:Lcom/google/android/apps/gmm/v/as;

    .line 192
    iput p2, p0, Lcom/google/android/apps/gmm/v/ar;->c:I

    .line 193
    iput p3, p0, Lcom/google/android/apps/gmm/v/ar;->d:I

    .line 194
    iput p4, p0, Lcom/google/android/apps/gmm/v/ar;->e:I

    .line 195
    iput p5, p0, Lcom/google/android/apps/gmm/v/ar;->f:I

    .line 196
    iput-boolean p6, p0, Lcom/google/android/apps/gmm/v/ar;->g:Z

    .line 197
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/at;IIZ)V
    .locals 0

    .prologue
    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/ar;->b:Lcom/google/android/apps/gmm/v/at;

    .line 215
    iput p2, p0, Lcom/google/android/apps/gmm/v/ar;->c:I

    .line 216
    iput p3, p0, Lcom/google/android/apps/gmm/v/ar;->d:I

    .line 217
    iput p2, p0, Lcom/google/android/apps/gmm/v/ar;->e:I

    .line 218
    iput p3, p0, Lcom/google/android/apps/gmm/v/ar;->f:I

    .line 219
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/v/ar;->g:Z

    .line 220
    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;IIILcom/google/android/apps/gmm/v/ao;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 261
    iput p2, p0, Lcom/google/android/apps/gmm/v/ar;->l:I

    .line 262
    iput p3, p0, Lcom/google/android/apps/gmm/v/ar;->c:I

    .line 263
    iput p4, p0, Lcom/google/android/apps/gmm/v/ar;->d:I

    .line 264
    if-eqz p5, :cond_2

    iget-object v0, p5, Lcom/google/android/apps/gmm/v/ao;->a:Lcom/google/android/apps/gmm/v/ap;

    if-eqz p6, :cond_1

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/v/ap;->e:Z

    :goto_0
    if-nez v0, :cond_2

    .line 265
    invoke-static {p3, v1}, Lcom/google/android/apps/gmm/v/ar;->a(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->e:I

    .line 266
    invoke-static {p4, v1}, Lcom/google/android/apps/gmm/v/ar;->a(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->f:I

    .line 267
    iget v3, p0, Lcom/google/android/apps/gmm/v/ar;->e:I

    iget v4, p0, Lcom/google/android/apps/gmm/v/ar;->f:I

    move-object v0, p1

    move v1, p3

    move v2, p4

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/v/ar;->a(Ljava/nio/ByteBuffer;IIIII)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->i:Ljava/nio/ByteBuffer;

    .line 274
    :goto_1
    if-eqz p6, :cond_0

    .line 275
    iget v0, p0, Lcom/google/android/apps/gmm/v/ar;->e:I

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->c:I

    .line 276
    iget v0, p0, Lcom/google/android/apps/gmm/v/ar;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->d:I

    .line 278
    :cond_0
    iput-boolean p6, p0, Lcom/google/android/apps/gmm/v/ar;->g:Z

    .line 279
    return-void

    .line 264
    :cond_1
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/v/ap;->d:Z

    goto :goto_0

    .line 270
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/ar;->i:Ljava/nio/ByteBuffer;

    .line 271
    iput p4, p0, Lcom/google/android/apps/gmm/v/ar;->f:I

    .line 272
    iput p3, p0, Lcom/google/android/apps/gmm/v/ar;->e:I

    goto :goto_1
.end method

.method public constructor <init>(Ljava/util/List;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    .line 300
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->k:Ljava/util/List;

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->c:I

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->d:I

    .line 303
    iget v0, p0, Lcom/google/android/apps/gmm/v/ar;->c:I

    if-eqz v0, :cond_0

    add-int/lit8 v3, v0, -0x1

    and-int/2addr v0, v3

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 304
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/v/ar;->d:I

    if-eqz v0, :cond_2

    add-int/lit8 v3, v0, -0x1

    and-int/2addr v0, v3

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 305
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->e:I

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ar;->f:I

    .line 307
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/v/ar;->g:Z

    .line 308
    return-void
.end method

.method public static a(II)I
    .locals 0

    .prologue
    .line 552
    .line 553
    :goto_0
    if-ge p1, p0, :cond_0

    .line 554
    shl-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 556
    :cond_0
    return p1
.end method

.method public static a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 569
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v2, v1, -0x1

    and-int/2addr v1, v2

    if-nez v1, :cond_0

    move v1, v0

    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v2, v1, -0x1

    and-int/2addr v1, v2

    if-nez v1, :cond_1

    move v1, v0

    :goto_1
    if-eqz v1, :cond_2

    .line 606
    :goto_2
    return-object p0

    :cond_0
    move v1, v3

    .line 569
    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    .line 573
    :cond_2
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 574
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    move v2, v0

    .line 575
    :goto_3
    if-ge v2, v4, :cond_3

    shl-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_3
    move v1, v0

    .line 576
    :goto_4
    if-ge v1, v5, :cond_4

    shl-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 578
    :cond_4
    invoke-static {v2, v1, p1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 579
    invoke-virtual {v0, v3}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 580
    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 581
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 582
    if-eqz p2, :cond_5

    .line 585
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8, v3, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v3, v3, v2, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v6, p0, v8, v4, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    move-object p0, v0

    .line 587
    goto :goto_2

    .line 591
    :cond_5
    invoke-virtual {v6, p0, v8, v8, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 594
    if-le v2, v4, :cond_6

    .line 595
    new-instance v8, Landroid/graphics/Rect;

    add-int/lit8 v9, v4, -0x1

    invoke-direct {v8, v9, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v9, Landroid/graphics/Rect;

    add-int/lit8 v10, v4, 0x1

    invoke-direct {v9, v4, v3, v10, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v6, p0, v8, v9, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 598
    :cond_6
    if-le v1, v5, :cond_7

    .line 599
    new-instance v8, Landroid/graphics/Rect;

    add-int/lit8 v9, v5, -0x1

    invoke-direct {v8, v3, v9, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v9, Landroid/graphics/Rect;

    add-int/lit8 v10, v5, 0x1

    invoke-direct {v9, v3, v5, v4, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v6, p0, v8, v9, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 602
    :cond_7
    if-le v2, v4, :cond_8

    if-le v1, v5, :cond_8

    .line 603
    new-instance v1, Landroid/graphics/Rect;

    add-int/lit8 v2, v4, -0x1

    add-int/lit8 v3, v5, -0x1

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v2, Landroid/graphics/Rect;

    add-int/lit8 v3, v4, 0x1

    add-int/lit8 v8, v5, 0x1

    invoke-direct {v2, v4, v5, v3, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v6, p0, v1, v2, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_8
    move-object p0, v0

    .line 606
    goto :goto_2
.end method

.method private static a(Ljava/nio/ByteBuffer;IIIII)Ljava/nio/ByteBuffer;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 629
    if-eqz p1, :cond_0

    add-int/lit8 v2, p1, -0x1

    and-int/2addr v2, p1

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    if-eqz p2, :cond_1

    add-int/lit8 v2, p2, -0x1

    and-int/2addr v2, p2

    if-nez v2, :cond_1

    :goto_1
    if-eqz v0, :cond_2

    .line 651
    :goto_2
    return-object p0

    :cond_0
    move v2, v1

    .line 629
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 633
    :cond_2
    mul-int v0, p5, p3

    mul-int/2addr v0, p4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    move v4, v1

    .line 636
    :goto_3
    if-ge v4, p2, :cond_5

    move v3, v1

    .line 637
    :goto_4
    if-ge v3, p1, :cond_4

    move v2, v1

    .line 638
    :goto_5
    if-ge v2, p5, :cond_3

    .line 639
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 638
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 637
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 642
    :cond_4
    sub-int v2, p3, p1

    .line 643
    invoke-static {v0, v2, p5}, Lcom/google/android/apps/gmm/v/ar;->a(Ljava/nio/ByteBuffer;II)V

    .line 636
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 646
    :cond_5
    sub-int v2, p4, p2

    .line 647
    :goto_6
    if-ge v1, v2, :cond_6

    .line 648
    invoke-static {v0, p3, p5}, Lcom/google/android/apps/gmm/v/ar;->a(Ljava/nio/ByteBuffer;II)V

    .line 647
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_6
    move-object p0, v0

    .line 651
    goto :goto_2
.end method

.method public static a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Landroid/graphics/Bitmap$Config;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 341
    .line 342
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 343
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 344
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    move v2, v3

    .line 351
    :goto_0
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5}, Landroid/graphics/Canvas;-><init>()V

    .line 352
    new-instance v6, Landroid/graphics/Rect;

    add-int v7, v0, v2

    invoke-direct {v6, v3, v2, v1, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 353
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7, v3, v3, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 355
    invoke-static {v1, v0, p1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 356
    invoke-virtual {v5, v8}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 357
    const/4 v9, 0x0

    invoke-virtual {v5, p0, v6, v7, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 358
    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    add-int/2addr v2, v0

    .line 360
    if-ne v1, v10, :cond_0

    if-eq v0, v10, :cond_1

    .line 361
    :cond_0
    div-int/lit8 v1, v1, 0x2

    invoke-static {v10, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 364
    div-int/lit8 v0, v0, 0x2

    invoke-static {v10, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    .line 366
    :cond_1
    return-object v4
.end method

.method private static a(Ljava/nio/ByteBuffer;II)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 613
    move v2, v1

    :goto_0
    if-ge v2, p1, :cond_1

    move v0, v1

    .line 614
    :goto_1
    if-ge v0, p2, :cond_0

    .line 615
    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 614
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 613
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 618
    :cond_1
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z
    .locals 9

    .prologue
    const v7, 0x8363

    const/4 v8, 0x0

    const/16 v2, 0x1907

    const/16 v0, 0xde1

    const/4 v1, 0x0

    .line 461
    iget-boolean v3, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    if-eqz v3, :cond_4

    .line 462
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ar;->a:Lcom/google/android/apps/gmm/v/as;

    if-eqz v3, :cond_1

    .line 463
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ar;->a:Lcom/google/android/apps/gmm/v/as;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/v/as;->a()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    .line 467
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_5

    .line 471
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-static {v0, v1, v2, v1}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 473
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ar;->k:Ljava/util/List;

    if-eqz v2, :cond_2

    move v3, v1

    .line 474
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ar;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 475
    add-int/lit8 v4, v3, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ar;->k:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-static {v0, v4, v2, v1}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 474
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 464
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ar;->b:Lcom/google/android/apps/gmm/v/at;

    if-eqz v3, :cond_0

    .line 465
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ar;->b:Lcom/google/android/apps/gmm/v/at;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/v/at;->a()Lcom/google/android/apps/gmm/v/s;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/gmm/v/ar;->j:Lcom/google/android/apps/gmm/v/s;

    goto :goto_0

    .line 479
    :cond_2
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_3

    .line 484
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/v/be;->e(I)V

    .line 486
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->a:Lcom/google/android/apps/gmm/v/as;

    if-eqz v0, :cond_4

    .line 488
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->a:Lcom/google/android/apps/gmm/v/as;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/v/as;->a(Landroid/graphics/Bitmap;)V

    .line 489
    iput-object v8, p0, Lcom/google/android/apps/gmm/v/ar;->h:Landroid/graphics/Bitmap;

    .line 532
    :cond_4
    :goto_2
    const/4 v0, 0x1

    return v0

    .line 491
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ar;->i:Ljava/nio/ByteBuffer;

    if-eqz v3, :cond_6

    .line 493
    iget v3, p0, Lcom/google/android/apps/gmm/v/ar;->l:I

    packed-switch v3, :pswitch_data_0

    .line 507
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad number of channels in ImageData buffer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 495
    :pswitch_0
    const/16 v2, 0x1909

    .line 510
    :goto_3
    :pswitch_1
    iget v3, p0, Lcom/google/android/apps/gmm/v/ar;->e:I

    iget v4, p0, Lcom/google/android/apps/gmm/v/ar;->f:I

    const/16 v7, 0x1401

    iget-object v8, p0, Lcom/google/android/apps/gmm/v/ar;->i:Ljava/nio/ByteBuffer;

    move v5, v1

    move v6, v2

    invoke-static/range {v0 .. v8}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    goto :goto_2

    .line 498
    :pswitch_2
    const/16 v2, 0x190a

    .line 499
    goto :goto_3

    .line 504
    :pswitch_3
    const/16 v2, 0x1908

    .line 505
    goto :goto_3

    .line 519
    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ar;->j:Lcom/google/android/apps/gmm/v/s;

    if-eqz v3, :cond_7

    .line 520
    iget-object v6, p0, Lcom/google/android/apps/gmm/v/ar;->j:Lcom/google/android/apps/gmm/v/s;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/16 v0, 0xde1

    const/16 v3, 0x1907

    const v4, 0x8363

    :try_start_0
    new-instance v5, Ljava/io/ByteArrayInputStream;

    iget-object v6, v6, Lcom/google/android/apps/gmm/v/s;->c:[B

    invoke-direct {v5, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static/range {v0 .. v5}, Landroid/opengl/ETC1Util;->loadTexture(IIIIILjava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 521
    :goto_4
    iput-object v8, p0, Lcom/google/android/apps/gmm/v/ar;->j:Lcom/google/android/apps/gmm/v/s;

    .line 522
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ar;->b:Lcom/google/android/apps/gmm/v/at;

    if-eqz v0, :cond_4

    .line 527
    iput-object v8, p0, Lcom/google/android/apps/gmm/v/ar;->j:Lcom/google/android/apps/gmm/v/s;

    goto :goto_2

    .line 530
    :cond_7
    iget v3, p0, Lcom/google/android/apps/gmm/v/ar;->c:I

    iget v4, p0, Lcom/google/android/apps/gmm/v/ar;->d:I

    move v5, v1

    move v6, v2

    invoke-static/range {v0 .. v8}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_4

    .line 493
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
