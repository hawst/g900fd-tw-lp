.class public Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;
.super Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 39
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;->b:Z

    if-eqz v0, :cond_3

    move v2, v6

    .line 44
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/tutorial/c;

    .line 46
    iget-object v3, v0, Lcom/google/android/apps/gmm/tutorial/c;->a:Landroid/view/View;

    instance-of v3, v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/google/android/apps/gmm/tutorial/c;->b:Landroid/graphics/Rect;

    if-eqz v3, :cond_1

    .line 47
    const/4 v2, 0x1

    .line 48
    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/c;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 52
    :goto_1
    if-eqz v2, :cond_2

    .line 53
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/d;->aC:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 55
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v3, v2

    int-to-float v4, v0

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 61
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_3

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;->d:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/tutorial/c;

    .line 68
    iget-object v1, v0, Lcom/google/android/apps/gmm/tutorial/c;->a:Landroid/view/View;

    instance-of v1, v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/tutorial/c;->b:Landroid/graphics/Rect;

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, v0, Lcom/google/android/apps/gmm/tutorial/c;->b:Landroid/graphics/Rect;

    .line 72
    iget v2, v1, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v3, v1, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 73
    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/c;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 74
    iget v0, v1, Landroid/graphics/Rect;->left:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, v1, Landroid/graphics/Rect;->top:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 66
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 44
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 57
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/d;->aC:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_2

    .line 78
    :cond_3
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 81
    return-void

    :cond_4
    move v0, v6

    move v2, v6

    goto :goto_1
.end method
