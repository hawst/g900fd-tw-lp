.class public Lcom/google/android/apps/gmm/car/h/r;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/h/p;


# instance fields
.field public a:Lcom/google/android/apps/gmm/car/bm;

.field b:I

.field c:Ljava/util/Collection;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/car/h/q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/bm;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/car/bm;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/h/r;->a:Lcom/google/android/apps/gmm/car/bm;

    .line 34
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/r;->a:Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bm;->b()I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/map/r/a/ao;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/r;->a:Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/car/bm;->a(I)Lcom/google/android/apps/gmm/map/r/a/ao;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/car/h/q;)V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/r;->c:Ljava/util/Collection;

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/r;->c:Ljava/util/Collection;

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/r;->c:Ljava/util/Collection;

    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 99
    return-void
.end method

.method a(Z)V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/r;->c:Ljava/util/Collection;

    if-nez v0, :cond_1

    .line 128
    :cond_0
    return-void

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/r;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/h/q;

    .line 126
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/car/h/q;->a(Z)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/google/android/apps/gmm/car/h/r;->b:I

    return v0
.end method

.method public final b(I)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 56
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/h/r;->a:Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/gmm/car/bm;->a(I)Lcom/google/android/apps/gmm/map/r/a/ao;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/directions/f/d/h;->e(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/be;

    move-result-object v2

    .line 59
    if-nez v2, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v3, v2, Lcom/google/maps/g/a/be;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_2

    :goto_1
    if-eqz v1, :cond_0

    iget v0, v2, Lcom/google/maps/g/a/be;->b:I

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1
.end method

.method public final b(Lcom/google/android/apps/gmm/car/h/q;)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/r;->c:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 104
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 105
    :cond_0
    return-void
.end method

.method public final c(I)I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/r;->a:Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/car/bm;->a(I)Lcom/google/android/apps/gmm/map/r/a/ao;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    .line 68
    :goto_0
    iget v1, v0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    move v1, v3

    :goto_1
    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v1, :cond_2

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v1

    :goto_2
    iget v1, v1, Lcom/google/maps/g/a/ai;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_3

    move v1, v3

    :goto_3
    if-eqz v1, :cond_5

    .line 69
    iget-object v1, v0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v1, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v0

    :goto_4
    iget v0, v0, Lcom/google/maps/g/a/ai;->b:I

    :goto_5
    return v0

    .line 67
    :cond_0
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_0

    :cond_1
    move v1, v2

    .line 68
    goto :goto_1

    :cond_2
    iget-object v1, v0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3

    .line 69
    :cond_4
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_5
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/car/h/r;->b:I

    .line 118
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/h/r;->a(Z)V

    .line 119
    return-void
.end method

.method public final d(I)Lcom/google/maps/g/a/fz;
    .locals 3

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/r;->a:Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/car/bm;->a(I)Lcom/google/android/apps/gmm/map/r/a/ao;

    move-result-object v1

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v2, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v2, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_0
    iget-object v2, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    if-nez v2, :cond_2

    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v0

    :goto_1
    iget v0, v0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_6

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_3
    iget-object v1, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    if-nez v1, :cond_5

    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v0

    :goto_4
    iget v0, v0, Lcom/google/maps/g/a/au;->d:I

    invoke-static {v0}, Lcom/google/maps/g/a/fz;->a(I)Lcom/google/maps/g/a/fz;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    :cond_0
    :goto_5
    return-object v0

    :cond_1
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_3

    :cond_5
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    goto :goto_4

    :cond_6
    sget-object v0, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    goto :goto_5
.end method

.method public final e(I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 84
    if-ltz p1, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    .line 85
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/h/r;->a:Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/bm;->b()I

    move-result v2

    if-ge p1, v2, :cond_2

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 86
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/car/h/r;->b:I

    if-ne p1, v0, :cond_4

    .line 91
    :goto_2
    return-void

    .line 89
    :cond_4
    iput p1, p0, Lcom/google/android/apps/gmm/car/h/r;->b:I

    .line 90
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/car/h/r;->a(Z)V

    goto :goto_2
.end method
