.class public final enum Lcom/google/android/apps/gmm/p/b/d;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/p/b/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/p/b/d;

.field public static final enum b:Lcom/google/android/apps/gmm/p/b/d;

.field public static final enum c:Lcom/google/android/apps/gmm/p/b/d;

.field public static final enum d:Lcom/google/android/apps/gmm/p/b/d;

.field public static final enum e:Lcom/google/android/apps/gmm/p/b/d;

.field private static final synthetic f:[Lcom/google/android/apps/gmm/p/b/d;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/p/b/d;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/p/b/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/p/b/d;->a:Lcom/google/android/apps/gmm/p/b/d;

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/p/b/d;

    const-string v1, "HARDWARE_MISSING"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/p/b/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/p/b/d;->b:Lcom/google/android/apps/gmm/p/b/d;

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/p/b/d;

    const-string v1, "ENABLED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/p/b/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/p/b/d;

    const-string v1, "DISABLED_BY_SETTING"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/p/b/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/p/b/d;->d:Lcom/google/android/apps/gmm/p/b/d;

    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/p/b/d;

    const-string v1, "DISABLED_BY_SECURITY"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/p/b/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/p/b/d;->e:Lcom/google/android/apps/gmm/p/b/d;

    .line 24
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/gmm/p/b/d;

    sget-object v1, Lcom/google/android/apps/gmm/p/b/d;->a:Lcom/google/android/apps/gmm/p/b/d;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/p/b/d;->b:Lcom/google/android/apps/gmm/p/b/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/p/b/d;->d:Lcom/google/android/apps/gmm/p/b/d;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/p/b/d;->e:Lcom/google/android/apps/gmm/p/b/d;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/gmm/p/b/d;->f:[Lcom/google/android/apps/gmm/p/b/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/p/b/d;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/gmm/p/b/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/b/d;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/p/b/d;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->f:[Lcom/google/android/apps/gmm/p/b/d;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/p/b/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/p/b/d;

    return-object v0
.end method
