.class public final Lcom/google/android/apps/gmm/car/i/z;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/map/util/b/a/a;

.field final c:Lcom/google/android/apps/gmm/map/t;

.field final d:Lcom/google/android/apps/gmm/shared/c/f;

.field final e:Lcom/google/android/apps/gmm/suggest/a/b;

.field f:Ljava/lang/String;

.field g:Lcom/google/android/apps/gmm/car/i/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/car/i/k",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/d;",
            ">;>;"
        }
    .end annotation
.end field

.field final h:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/gmm/car/i/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/i/z;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/a/a;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/suggest/a/b;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    new-instance v0, Lcom/google/android/apps/gmm/car/i/aa;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/i/aa;-><init>(Lcom/google/android/apps/gmm/car/i/z;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/z;->h:Ljava/lang/Object;

    .line 47
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/map/util/b/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/z;->b:Lcom/google/android/apps/gmm/map/util/b/a/a;

    .line 48
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/map/t;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/i/z;->c:Lcom/google/android/apps/gmm/map/t;

    .line 49
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/shared/c/f;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/i/z;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 50
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p4, Lcom/google/android/apps/gmm/suggest/a/b;

    iput-object p4, p0, Lcom/google/android/apps/gmm/car/i/z;->e:Lcom/google/android/apps/gmm/suggest/a/b;

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/z;->h:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->d(Ljava/lang/Object;)V

    .line 53
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/z;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 112
    :cond_0
    return-void

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/z;->g:Lcom/google/android/apps/gmm/car/i/k;

    .line 103
    iput-object v1, p0, Lcom/google/android/apps/gmm/car/i/z;->f:Ljava/lang/String;

    .line 104
    iput-object v1, p0, Lcom/google/android/apps/gmm/car/i/z;->g:Lcom/google/android/apps/gmm/car/i/k;

    .line 105
    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/i/k;->b()V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/z;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 110
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Tried to start a search while it was being canceled."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
