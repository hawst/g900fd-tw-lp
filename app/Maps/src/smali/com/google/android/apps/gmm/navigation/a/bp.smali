.class public Lcom/google/android/apps/gmm/navigation/a/bp;
.super Lcom/google/android/apps/gmm/navigation/a/a;
.source "PG"


# instance fields
.field private final b:Landroid/os/Vibrator;

.field private final c:[J


# direct methods
.method public constructor <init>(Landroid/os/Vibrator;[J)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/a/a;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/bp;->b:Landroid/os/Vibrator;

    .line 27
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/bp;->c:[J

    .line 28
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/bp;->a:Z

    .line 33
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/a/b;)V
    .locals 3

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/bp;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bp;->b:Landroid/os/Vibrator;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/bp;->c:[J

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 39
    if-eqz p1, :cond_1

    .line 40
    invoke-interface {p1, p0}, Lcom/google/android/apps/gmm/navigation/a/b;->a(Lcom/google/android/apps/gmm/navigation/a/a;)V

    .line 42
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/a/c;)V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method
