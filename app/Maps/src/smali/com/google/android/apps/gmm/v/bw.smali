.class public Lcom/google/android/apps/gmm/v/bw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/v/bz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/v/bz",
            "<*>;"
        }
    .end annotation
.end field

.field final b:I

.field final c:I

.field final d:Lcom/google/android/apps/gmm/v/ad;

.field public final e:Lcom/google/android/apps/gmm/v/bg;

.field final f:Lcom/google/android/apps/gmm/v/aa;

.field final g:Landroid/graphics/Canvas;

.field final h:Ljava/util/concurrent/Semaphore;

.field i:Landroid/graphics/Bitmap;

.field j:I

.field volatile k:Z

.field volatile l:Z

.field private final m:Lcom/google/android/apps/gmm/v/as;

.field private n:I

.field private o:I

.field private p:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/bz;Lcom/google/android/apps/gmm/v/ad;III)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/v/bz",
            "<*>;",
            "Lcom/google/android/apps/gmm/v/ad;",
            "III)V"
        }
    .end annotation

    .prologue
    const/16 v9, 0x2601

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Lcom/google/android/apps/gmm/v/bx;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/v/bx;-><init>(Lcom/google/android/apps/gmm/v/bw;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bw;->m:Lcom/google/android/apps/gmm/v/as;

    .line 92
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 95
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v8}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bw;->h:Ljava/util/concurrent/Semaphore;

    .line 127
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/bw;->a:Lcom/google/android/apps/gmm/v/bz;

    .line 128
    iput-object p2, p0, Lcom/google/android/apps/gmm/v/bw;->d:Lcom/google/android/apps/gmm/v/ad;

    .line 129
    iput p3, p0, Lcom/google/android/apps/gmm/v/bw;->b:I

    .line 130
    iput p4, p0, Lcom/google/android/apps/gmm/v/bw;->c:I

    .line 132
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bw;->g:Landroid/graphics/Canvas;

    .line 133
    new-instance v7, Lcom/google/android/apps/gmm/v/bg;

    new-instance v0, Lcom/google/android/apps/gmm/v/ar;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bw;->m:Lcom/google/android/apps/gmm/v/as;

    move v2, p3

    move v3, p4

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/v/ar;-><init>(Lcom/google/android/apps/gmm/v/as;IIIIZ)V

    invoke-direct {v7, v0, p5}, Lcom/google/android/apps/gmm/v/bg;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    iput-object v7, p0, Lcom/google/android/apps/gmm/v/bw;->e:Lcom/google/android/apps/gmm/v/bg;

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bw;->e:Lcom/google/android/apps/gmm/v/bg;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_0
    iput v9, v0, Lcom/google/android/apps/gmm/v/ci;->h:I

    iput v9, v0, Lcom/google/android/apps/gmm/v/ci;->i:I

    iput-boolean v8, v0, Lcom/google/android/apps/gmm/v/ci;->j:Z

    .line 139
    new-instance v0, Lcom/google/android/apps/gmm/v/aa;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/aa;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bw;->f:Lcom/google/android/apps/gmm/v/aa;

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bw;->f:Lcom/google/android/apps/gmm/v/aa;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1
    int-to-byte v1, v6

    iput-byte v1, v0, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bw;->f:Lcom/google/android/apps/gmm/v/aa;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bw;->e:Lcom/google/android/apps/gmm/v/bg;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ai;I)V

    .line 143
    iput v6, p0, Lcom/google/android/apps/gmm/v/bw;->n:I

    .line 144
    iput v6, p0, Lcom/google/android/apps/gmm/v/bw;->o:I

    .line 145
    iput v6, p0, Lcom/google/android/apps/gmm/v/bw;->p:I

    .line 146
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/v/bw;->k:Z

    .line 147
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/v/bw;->l:Z

    .line 148
    return-void
.end method


# virtual methods
.method final a(Z)Landroid/graphics/Bitmap;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 217
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bw;->h:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/bw;->l:Z

    .line 219
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/bw;->k:Z

    if-nez v0, :cond_2

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bw;->i:Landroid/graphics/Bitmap;

    .line 221
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/gmm/v/bw;->i:Landroid/graphics/Bitmap;

    .line 222
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/v/bw;->k:Z

    .line 224
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/v/bw;->l:Z

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    move-object v0, v1

    .line 228
    :goto_1
    return-object v0

    .line 224
    :cond_1
    const/4 v2, 0x0

    :try_start_1
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/v/bw;->l:Z

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/bw;->h:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public a(II)Lcom/google/android/apps/gmm/v/by;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 157
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/v/bw;->b:I

    if-gt p1, v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/v/bw;->c:I

    if-le p2, v1, :cond_1

    .line 158
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x26

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "width: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :cond_1
    iget v1, p0, Lcom/google/android/apps/gmm/v/bw;->o:I

    add-int/2addr v1, p2

    iget v2, p0, Lcom/google/android/apps/gmm/v/bw;->c:I

    if-le v1, v2, :cond_3

    .line 180
    :cond_2
    :goto_0
    return-object v0

    .line 165
    :cond_3
    iget v1, p0, Lcom/google/android/apps/gmm/v/bw;->n:I

    add-int/2addr v1, p1

    iget v2, p0, Lcom/google/android/apps/gmm/v/bw;->b:I

    if-le v1, v2, :cond_4

    .line 166
    iget v1, p0, Lcom/google/android/apps/gmm/v/bw;->o:I

    iget v2, p0, Lcom/google/android/apps/gmm/v/bw;->p:I

    add-int/2addr v1, v2

    add-int/2addr v1, p2

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/v/bw;->c:I

    if-gt v1, v2, :cond_2

    .line 169
    iput v3, p0, Lcom/google/android/apps/gmm/v/bw;->n:I

    .line 170
    iget v0, p0, Lcom/google/android/apps/gmm/v/bw;->o:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/bw;->p:I

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/v/bw;->o:I

    .line 171
    iput v3, p0, Lcom/google/android/apps/gmm/v/bw;->p:I

    .line 174
    :cond_4
    new-instance v0, Lcom/google/android/apps/gmm/v/by;

    iget v2, p0, Lcom/google/android/apps/gmm/v/bw;->n:I

    iget v3, p0, Lcom/google/android/apps/gmm/v/bw;->o:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/bw;->n:I

    add-int v4, v1, p1

    iget v1, p0, Lcom/google/android/apps/gmm/v/bw;->o:I

    add-int v5, v1, p2

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/v/by;-><init>(Lcom/google/android/apps/gmm/v/bw;IIII)V

    .line 177
    iget v1, p0, Lcom/google/android/apps/gmm/v/bw;->n:I

    add-int/lit8 v2, p1, 0x1

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/v/bw;->n:I

    .line 178
    iget v1, p0, Lcom/google/android/apps/gmm/v/bw;->p:I

    invoke-static {v1, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/v/bw;->p:I

    .line 179
    iget v1, p0, Lcom/google/android/apps/gmm/v/bw;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/gmm/v/bw;->j:I

    goto :goto_0
.end method

.method protected a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 260
    iput v0, p0, Lcom/google/android/apps/gmm/v/bw;->n:I

    .line 261
    iput v0, p0, Lcom/google/android/apps/gmm/v/bw;->o:I

    .line 262
    iput v0, p0, Lcom/google/android/apps/gmm/v/bw;->p:I

    .line 263
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .prologue
    .line 331
    const-string v0, "TextureAtlas: [, texture: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bw;->e:Lcom/google/android/apps/gmm/v/bg;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/v/bw;->n:I

    iget v3, p0, Lcom/google/android/apps/gmm/v/bw;->o:I

    iget v4, p0, Lcom/google/android/apps/gmm/v/bw;->p:I

    iget v5, p0, Lcom/google/android/apps/gmm/v/bw;->j:I

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x5b

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startX: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startY: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lineHeight: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", refCount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
