.class public Lcom/google/android/apps/gmm/z/b/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/shared/c/f;

.field public final c:J

.field public d:Lcom/google/o/b/a/v;

.field public e:Lcom/google/maps/g/hy;

.field public f:Ljava/lang/String;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/b/f/cm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/z/b/a;->c:J

    .line 45
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/b/a;->a:Lcom/google/android/apps/gmm/shared/c/f;

    .line 46
    return-void
.end method


# virtual methods
.method public a(Lcom/google/r/b/a/apf;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 86
    iget-wide v0, p0, Lcom/google/android/apps/gmm/z/b/a;->c:J

    iget v2, p1, Lcom/google/r/b/a/apf;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p1, Lcom/google/r/b/a/apf;->a:I

    iput-wide v0, p1, Lcom/google/r/b/a/apf;->d:J

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/a;->d:Lcom/google/o/b/a/v;

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/a;->d:Lcom/google/o/b/a/v;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/r/b/a/apf;->c()V

    iget-object v1, p1, Lcom/google/r/b/a/apf;->c:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    invoke-direct {v2}, Lcom/google/n/ao;-><init>()V

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v2, Lcom/google/n/ao;->d:Z

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/a;->e:Lcom/google/maps/g/hy;

    if-eqz v0, :cond_2

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/a;->e:Lcom/google/maps/g/hy;

    invoke-virtual {p1, v0}, Lcom/google/r/b/a/apf;->a(Lcom/google/maps/g/hy;)Lcom/google/r/b/a/apf;

    .line 93
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/a;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/a;->f:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget v1, p1, Lcom/google/r/b/a/apf;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p1, Lcom/google/r/b/a/apf;->a:I

    iput-object v0, p1, Lcom/google/r/b/a/apf;->e:Ljava/lang/Object;

    .line 96
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/a;->g:Ljava/util/List;

    if-eqz v0, :cond_6

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    .line 98
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    invoke-virtual {p1}, Lcom/google/r/b/a/apf;->d()V

    iget-object v2, p1, Lcom/google/r/b/a/apf;->f:Ljava/util/List;

    new-instance v3, Lcom/google/n/ao;

    invoke-direct {v3}, Lcom/google/n/ao;-><init>()V

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v3, Lcom/google/n/ao;->d:Z

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 101
    :cond_6
    return-void
.end method

.method public c()J
    .locals 4

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/a;->a:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->c()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/z/b/a;->c:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public final d()Lcom/google/r/b/a/apd;
    .locals 3

    .prologue
    .line 107
    invoke-static {}, Lcom/google/r/b/a/apd;->newBuilder()Lcom/google/r/b/a/apf;

    move-result-object v0

    .line 108
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/z/b/a;->a(Lcom/google/r/b/a/apf;)V

    .line 109
    invoke-virtual {v0}, Lcom/google/r/b/a/apf;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/apd;

    .line 112
    iget v1, v0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 113
    :cond_1
    return-object v0
.end method
